/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

// Enable or disable test-automation
var _enableUiAutomation = true;

// Checks if the application is launched in the CEF test-automation host
function _uiAutomationAvailable() {
    return (_enableUiAutomation && typeof window.uiAutomationInterface != 'undefined');
}

// Calls C# Dynalean.UBuy.WapperUI.UiAutomationInterface.TriggerConfiguratorEvent(string eventCode, string eventArgs)
function triggerConfiguratorEvent(eventCode, eventArgs) {
    if (!_uiAutomationAvailable())
        return;

    window.uiAutomationInterface.triggerConfiguratorEvent(eventCode, eventArgs);
}
// Calls C# Dynalean.UBuy.WapperUI.UiAutomationInterface.TriggerDomEvent(string eventCode)
function triggerDomEvent(eventCode, eventArgs) {
    if (!_uiAutomationAvailable())
        return;

    window.uiAutomationInterface.triggerDomEvent(eventCode, eventArgs);
}

// Calls C# Dynalean.UBuy.WapperUI.UiAutomationInterface.triggerAjaxEvent(string eventCode, int time)
function triggerAjaxEvent(eventCode, time) {
    if (!_uiAutomationAvailable())
        return;

    window.uiAutomationInterface.triggerAjaxEvent(eventCode, time);
}

// Calls C# Dynalean.UBuy.WapperUI.UiAutomationInterface.TriggerCheckoutEvent(string eventCode, string eventArgs)
function triggerCheckoutEvent(eventCode, eventArgs) {
    if (!_uiAutomationAvailable())
        return;

    window.uiAutomationInterface.triggerCheckoutEvent(eventCode, eventArgs);
}
// Calls C# Dynalean.UBuy.WapperUI.UiAutomationInterface.RegisterValue(string jqueryExpression, string value)
function getElementValue(jqueryExpression) {
    if (!_uiAutomationAvailable())
        return;

    var value = jQuery(jqueryExpression).html();
    window.uiAutomationInterface.registerValue(jqueryExpression, value);
}
jQuery(document).ready(function () {
    triggerDomEvent('document.ready', document.URL);
});