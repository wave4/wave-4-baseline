/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function ($) {
    var root = window;

    Inlife = function (websiteCode) {
        var inlifeController = '/configurator/inlife/';
        this.formData = {};
        this.websites = {};
        this.websiteCode = websiteCode;
        this.customer = {};
        this.multisimIntervalID = null;
        this.simSwapIntervalID = null;
        this.performMultisimPollingCount = 200;
        this.performSimSwapPollingCount = 200;

        this.localeString = function(x, sep, grp) {
            x = parseFloat(x).toFixed(2);
            var sx = (''+x).split('.'), s = '', i, j;
            sep || (sep = ' '); // default seperator
            grp || grp === 0 || (grp = 3); // default grouping
            i = sx[0].length;
            while (i > grp) {
                j = i - grp;
                s = sep + sx[0].slice(j, i) + s;
                i = j;
            }
            s = sx[0].slice(0, i) + s;
            sx[0] = s;
            return sx.join(',')
        };

        this.updateInlifeData = function(data) {
            if (data) {
                $('#inlife-wrapper .main-screen').html(data).removeClass('hidden');
                $('#inlife-wrapper .loading-extras').addClass('hidden');
            }
        };

        this.retrieveSubsAddonsData = function(ctnCode) {
            var self = this;
            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
            if (ctnCode) {
                $('#inlife-wrapper .main-screen').empty();
                $('#inlife-wrapper .loading-extras').removeClass('hidden');
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: {'ctn_code': ctnCode},
                    url: inlifeController + 'getInlifeCtnData',
                    success: function (data) {
                        if (data.error) {
                            showModalError(data.message);
                        } else if (!data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .main-screen').html(data.html);
                                $('#inlife-wrapper .loading-extras').addClass('hidden');
                            }
                        } else {
                            console.log('Failed to get ctn details');
                        }
                    },
                    error: function () {
                        console.log('Failed to get ctn details');
                    }
                });
            } else {
                showModalError('Invalid CTN provided');
                $(location).attr('href', '/');
            }
            window.setPageOverrideLoading(false);
        };

        this.confirmAddAddon = function(obj) {
            var data = {
                'block': 'inlife_confirm',
                'action': 'add',
                'modal': true,
                'simulate': true
            };

            // Add the fields from the form
            $.extend(data, $(obj).data());

            this.getInlifeBlock(data);

            return false;
        };

        this.quoteAddon = function(obj) {
            var self = this;
            var formData = $(obj).data();
            self.formData = formData;

            var callback = function() {
                var data = {
                    'block': 'partials/quote_addon',
                    'action': 'add',
                    'simulate': true,
                    'edit': true
                };

                // Add the fields from the form
                $.extend(data, formData);

                self.getInlifeBlock(data);
            };

            self.inlifeLoad('add', 'loading', 'Retrieve quote', '', callback);

            return false;
        };

        this.addInlifeAddon = function(obj, direct, order_id, sim) {
            var self = this;
            var formData = {};

            if (obj) {
                var form = $(obj).is( "form" ) ? $(obj) : $(obj).parents('form').first();
                if (form.length) {
                    formData = form.serializeObject();
                }
            }
            formData['order_id'] = order_id;
            formData['sim'] = sim;
            $.extend(formData, this.formData);

            var callback = function() {
                window.setPageOverrideLoading && window.setPageOverrideLoading(true);
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: formData,
                    url: inlifeController + 'addInlifeAddon',
                    success: function (data) {
                        if (data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            }
                        } else if (!data.error) {
                            self.inlifeLoad('add','success',formData.addonName, '');
                        } else {
                            console.log('Unknown response returned');
                        }
                        window.setPageOverrideLoading(false);
                    },
                    error: function () {
                        console.log('Failed to process requested action');
                        window.setPageOverrideLoading(false);
                    }
                });
            };

            if (direct != 'undefined' && direct == true) {
                if (callback && typeof callback == 'function') {
                    setTimeout(callback, 300);
                }
            } else {
                this.inlifeLoad('add', 'loading', formData.addonName, '', callback);
            }

            return false;
        };

        this.quoteRemoveAddon = function(obj) {
            var self = this;
            var formData = $(obj).data();
            self.formData = formData;

            var callback = function() {
                var data = {
                    'block': 'partials/quote_addon',
                    'action': 'remove',
                    'simulate': true,
                    'edit': true
                };

                // Add the fields from the form
                $.extend(data, formData);

                self.getInlifeBlock(data);
            };

            self.inlifeLoad('remove', 'loading', 'Retrieve quote', '', callback);

            return false;
        };

        this.confirmRemoveAddon = function(obj) {
            var data = {
                'block': 'inlife_confirm',
                'action': 'remove',
                'modal': true
            };

            // Add the fields from the form
            $.extend(data, $(obj).data());

            this.getInlifeBlock(data);

            return false;
        };

        this.removeInlifeAddon = function(obj) {
            var self = this;
            var form = $(obj).parents('form').first();
            var formData = form.serializeObject();

            $.extend(formData, self.formData);

            var callback = function() {
                window.setPageOverrideLoading && window.setPageOverrideLoading(true);
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: formData,
                    url: inlifeController + 'removeInlifeAddon',
                    success: function (data) {
                        if (data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            }
                        } else if (!data.error) {
                            self.inlifeLoad('remove', 'success', formData.addonName);
                        } else {
                            console.log('Unknown response returned');
                        }
                        window.setPageOverrideLoading(false);
                    },
                    error: function () {
                        console.log('Failed to process requested action');
                        window.setPageOverrideLoading(false);
                    }
                });
            };

            this.inlifeLoad('remove', 'loading', formData.addonName, '', callback);

            return false;
        };

        this.inlifeLoad = function(action, section, name, current, callback) {
            $('#inlife-confirm').modal('hide');

            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-wrapper .section, .eligible-priceplans, .edit-sim, #inlife-edit-block').addClass('hidden');
            this.getInlifeBlock({
                'block': 'inlife_action',
                'action': action,
                'section': section,
                'name': name,
                'current': current
            }, callback);
        };

        this.inlifeConfirm = function(action, code, name, current) {
            this.getInlifeBlock({
                'block': 'inlife_confirm',
                'action': action,
                'code': code,
                'name': name,
                'modal': true,
                'current': current
            });
        };

        this.getInlifeBlock = function(data, callback) {
            var self = this;
            var is_modal = (data.modal == true) ? true : false;
            var is_edit = (data.edit == true) ? true : false;
            var section = data.section;
            $('#inlife-confirm').remove();

            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
            $.ajax({
                async: (data.simulate == true) ? true : false,
                type: 'POST',
                data: data,
                url: inlifeController + 'getInlifeBlock',
                success: function (data) {
                    if (data.error) {
                        showModalError(data.message);
                    } else if (!data.error) {
                        if(is_modal) {
                            $('#inlife-wrapper #inlife-modal').html(data.html);
                            $('#inlife-confirm').appendTo('body').modal('show');
                        } else {
                            if (is_edit) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            } else {
                                if (section == 'loading') {
                                    $('#inlife-edit-block').html(data.html).removeClass('hidden');
                                } else {
                                    self.updateInlifeData(data.html);
                                }
                            }
                            if (callback && typeof callback == 'function') {
                                setTimeout(callback, 300);
                            }
                        }
                    } else {
                        console.log('Failed to get requested block');
                    }
                    window.setPageOverrideLoading(false);
                },
                error: function () {
                    console.log('Failed to get requested block');
                    window.setPageOverrideLoading(false);
                }
            });
        };

        this.cancelEditAddon = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').removeClass('hidden');
            $('#inlife-edit-block').empty().addClass('hidden');
        };

        /** ================ Garant addons ================ **/
        this.editGarant = function(obj) {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').addClass('hidden');
            this.getInlifeBlock({
                'block': 'partials/edit_garant',
                'edit': true,
                'billingOfferCode': $(obj).data('billingOfferCode')
            });
        };

        this.cancelEditGarant = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').removeClass('hidden');
            $('#inlife-edit-block').empty().addClass('hidden');
        };

        this.quoteGarant = function(obj) {
            var self = this;
            var formData = $(obj.form).serializeObject();
            self.formData = formData;

            if (obj.validator && obj.validator.validate()) {
                var callback = function() {
                    var data = {
                        'block': 'partials/quote_garant',
                        'action': 'add',
                        'simulate': true,
                        'edit': true
                    };

                    // Add the fields from the form
                    $.extend(data, formData);

                    self.getInlifeBlock(data);
                };

                self.inlifeLoad('add', 'loading', 'Retrieve quote', '', callback);
            }
            return false;
        };

        this.showGarantOptions = function(obj) {
            obj = $(obj);
            var val = obj.val();
            var parent = obj.parent();
            parent.find('.ins-cat-options').hide();
            parent.find('.ins-cat-' + val).show();
        };
        
        this.setInsuranceCategory = function(value) {
            obj = $('#insurance_category');
            obj.val(value);
        };
        
        /** ================ Change subscription ================ **/
        this.editSubscription = function(assignedProductId) {
            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
            var is_modal = false;
            var data = {'ctn_code': assignedProductId};
            $('#inlife-confirm').remove();
            $('.loading-extras').addClass('hidden');
            $('.loading-priceplans').removeClass('hidden');
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-wrapper .section, .home-wrapper .main-screen').addClass('hidden');
            $.ajax({
                async: true,
                type: 'POST',
                data: data,
                url: inlifeController + 'getEligiblePricePlans',
                success: function (data) {
                    $('.loading-priceplans').addClass('hidden');

                    if(is_modal) {
                        $('#inlife-wrapper #inlife-modal').html(data.html);
                        $('#inlife-confirm').appendTo('body').modal('show');
                    } else {
                        inlifeWrapper.find('.eligible-priceplans').html(data.html).removeClass('hidden');
                    }
                },
                error: function () {
                    console.log('Failed to get requested block');
                }
            });
            window.setPageOverrideLoading(false);

        };

        this.cancelEditSubscription = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-wrapper .section, .home-wrapper .main-screen').removeClass('hidden');
            inlifeWrapper.find('.eligible-priceplans').addClass('hidden');
        };

        this.confirmSubscription = function(obj) {
            var data = {
                'block': 'inlife_confirm',
                'action': 'subscription',
                'base_price_plan_code': $(obj).data('base_price_plan_code'),
                'product_offer_code': $(obj).data('product_offer_code'),
                'name': $(obj).data('base_price_plan_name'),
                'current': $(obj).data('current'),
                'modal': true
            };

            this.getInlifeBlock(data);
        };

        this.saveSubscription = function(obj) {
            var self = this;
            var form = $(obj).parents('form').first();
            var formData = form.serializeObject();

            var callback = function() {
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: form.serializeObject(),
                    url: inlifeController + 'updateInlifeSubscription',
                    success: function (data) {
                        if (data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            }
                        } else if (!data.error) {
                            self.inlifeLoad('subscription', 'success', formData.name, formData.current);
                        } else {
                            console.log('Unknown response returned');
                        }
                    },
                    error: function () {
                        console.log('Failed to process requested action');
                    }
                });
            };

            self.inlifeLoad('subscription', 'loading', formData.name, formData.current, callback);
        };

        /** ================ Change CTN ================ **/
        this.setPhoneNumber = function(simulate, current) {
            var self = this;
            var modal = $('#search-inlife-phone-number');
            var number = modal.find('input[name=ctn-phone-number]').filter(':checked').data('phone-number');

            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
            if(number) {
                modal.modal('hide');
                var postData = {'new_ctn': number, 'current': current};

                if (!simulate) {
                    $.extend(postData, self.formData);
                }

                postData['simulate'] = simulate;
                var callback = function() {
                    $.ajax({
                        async: true,
                        type: 'POST',
                        data: postData,
                        url: inlifeController + 'updateProductSettings',
                        success: function (data) {

                            if (simulate) {
                                if (data.html) {
                                    $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                    $('#inlife-edit-block').html(data.html).removeClass('hidden');
                                }
                                if (data.data) {
                                    self.formData = data.data;
                                }
                            } else {
                                if (data.error) {
                                    if (data.html) {
                                        $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                        $('#inlife-edit-block').html(data.html).removeClass('hidden');
                                    }
                                } else if (!data.error) {
                                    self.inlifeLoad('changeCtn', 'success', postData.new_ctn, postData.current);
                                } else {
                                    console.log('Unknown response returned');
                                }
                            }
                            window.setPageOverrideLoading(false);
                        },
                        error: function () {
                            console.log('Failed to process requested action');
                            window.setPageOverrideLoading(false);
                        }
                    });
                };
                this.inlifeLoad('changeCtn', 'loading', number, '', callback);
            } else {
                modal.find('#ctn-modal-error').html(Translator.translate('Must select a number'));
            }

            return false;
        };

        this.editCtn = function(lastCtn) {
            $.ajax({
                async: true,
                type: 'POST',
                data: {'last_ctn': lastCtn},
                url: inlifeController + 'retrieveNumberList',
                success: function (data) {
                    if (data.error) {
                        showModalError(data.message);
                    } else {
                        var ctns = '';
                        data.numbers.each(function (id) {
                            ctns += '<tr><td><div class="radio"><label><input type="radio" name="ctn-phone-number" value="1" data-phone-number="' + id + '"> ' + id + '</label></div></td></tr>';
                        });

                        var modal = $('#search-inlife-phone-number');
                        var ctnTable = modal.find('table#ctn-content tbody');
                        modal.find('#ctn-modal-error').html('');
                        if (typeof lastCtn == 'undefined') {
                            ctnTable.html(ctns);
                            modal.appendTo('body').modal();
                        } else {
                            ctnTable.append(ctns);
                        }
                    }
                },
                error: function () {
                    var modal = $('#search-inlife-phone-number');
                    modal.find('#ctn-modal-error').html('Failed to get numbers list');
                    console.log('Failed to get numbers list');
                }
            });
        };

        this.loadMoreNumbers = function() {
            var modal = $('#search-inlife-phone-number');
            var lastCtn = modal.find('table#ctn-content tbody tr input').last().data('phone-number');
            this.editCtn(lastCtn);
        };

        /** ================ Change SIM ================ **/
        this.editSim = function(ctn) {
            window.setPageOverrideLoading(true);
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.loading-extras, .home-wrapper .section, .home-wrapper .main-screen').addClass('hidden');
            $('.loading-edit-sim').removeClass('hidden').delay( 800 );

            $.ajax({
                async: true,
                type: 'POST',
                data: {'ctn': ctn},
                url: inlifeController + 'editSim',
                success: function (data) {
                    $('#inlife-edit-block').html(data.html).removeClass('hidden');
                    window.setPageOverrideLoading(false);
                    $('.loading-edit-sim').addClass('hidden');
                },
                error: function () {
                    $('.loading-edit-sim').addClass('hidden');
                    console.log('Failed to get requested block');
                    window.setPageOverrideLoading(false);
                }
            });
        };

        this.cancelEditSim = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-wrapper .section, .home-wrapper .main-screen').removeClass('hidden');
            inlifeWrapper.find('.edit-sim, #inlife-edit-block').addClass('hidden');
        };

        this.confirmSimSwap = function(obj) {
            var formData = $(obj.form).serializeObject();
            this.formData = formData;

            if ($.trim(formData.sim_id) != '') {
                $('#simcard-select').removeClass('validation-failed required-entry').addClass('validation-passed');
                $('#advice-required-entry-simcard-select').fadeOut();
            }

            if (obj.validator && obj.validator.validate()) {
                var data = {
                    'block': 'inlife_confirm',
                    'action': 'simswap',
                    'modal': true
                };

                // Add the fields from the form
                $.extend(data, formData);

                this.getInlifeBlock(data);
            }
            return false;
        };

        this.createSimSwapOrder = function(obj) {
            var self = this;
            var form = $(obj).parents('form').first();
            var data = form.serializeObject();

            $.extend(data, self.formData);
            self.formData = data;

            var callback = function() {
                window.setPageOverrideLoading && window.setPageOverrideLoading(true);
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: data,
                    url: inlifeController + 'createSimSwapOrder',
                    success: function (data) {
                        if (data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            }
                        } else if (!data.error) {
                            self.doSimSwapPolling(data.polling_id, data.order_id);
                            self.simSwapIntervalID = setInterval(function () {
                                self.doSimSwapPolling(data.polling_id, data.order_id);
                            }, 10000);
                        } else {
                            console.log('Unknown response returned');
                        }
                    },
                    error: function () {
                        console.log('Failed to process requested action');
                    }
                });
            };

            self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', callback);
            return false;
        };

        this.doSimSwapPolling = function(id, order_id) {
            var self = this;
            --self.performSimSwapPollingCount;
            if (self.simSwapIntervalID !== false) {

                $.ajax({
                    async: true,
                    type: 'POST',
                    data: { 'polling_id': id },
                    url: inlifeController + 'orderPolling',
                    success: function (data) {
                        if (data.error == true) {
                            self.simSwapIntervalID = false;
                            // error
                            showModalError(data.message, data.serviceError, self.cancelEditSim);
                        } else if (data.error == false) {
                            if (data.hasOwnProperty('performed') && data.performed == true) {
                                self.simSwapIntervalID = false;
                                if (order_id) {
                                    // success
                                    self.saveSimSwap(order_id, data.sim)
                                } else {
                                    self.inlifeLoad('simswap', 'success', 'SIM Swap', '');
                                }
                            }
                        } else {
                            console.log('error occured / no response received');
                        }
                    }
                });

            }

            if (self.performSimSwapPollingCount == 0) {
                // polling failed
                clearInterval(self.simSwapIntervalID);
                self.simSwapIntervalID = false;
            }
            return false;
        };

        this.simSwapAction = function(obj) {
            var self = this;
            self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', '');

            var form = $(obj).parents('form').first();
            var data = form.serializeObject();

            $.extend(data, self.formData);
            self.formData = data;

            self.saveSimSwap(null, null);
        };

        this.saveSimSwap = function(order_id, sim) {
            var self = this;
            var polling_id = false;

            window.setPageOverrideLoading && window.setPageOverrideLoading(true);

            var callback = function(polling_id) {
                self.doSimSwapPolling(polling_id, null);
                self.simSwapIntervalID = setInterval(function () {
                    self.doSimSwapPolling(polling_id, null);
                }, 10000);
            };

            $.ajax({
                async: true,
                type: 'POST',
                data: $.extend(self.formData, {'order_id': order_id, 'sim': sim}),
                url: inlifeController + 'simSwap',
                success: function (data) {
                    if (data.error) {
                        if (data.html) {
                            $('#inlife-wrapper .inlife-loading').addClass('hidden');
                            $('#inlife-edit-block').html(data.html).removeClass('hidden');
                        }
                    } else if (!data.error) {
                        if (! data.polling_id) {
                            self.inlifeLoad('simswap', 'success', 'SIM Swap', '');
                        } else {
                            self.inlifeLoad('simswap', 'loading', 'SIM Swap', '', callback(data.polling_id));
                            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
                        }
                    } else {
                        console.log('Unknown response returned');
                    }
                },
                error: function () {
                    console.log('Failed to process requested action');
                }
            });
        };

        this.selectSim  = function(obj) {
            obj = $(obj);
            var name = obj.children('span:first-child').text();
            var cost = obj.children('span:last-child').text();
            var parent = obj.parents('.btn-group');
            parent.find('.dropdown-toggle .text').text(name + ' ' + cost);
            parent.removeClass('open');

            var costContainer = $('.edit-sim .cost-container');
            costContainer.removeClass('hidden');
            costContainer.find('.name').text(name);
            costContainer.find('.sim-price').text(cost);
            costContainer.find('.total').text(cost);
            costContainer.find('.total-excl').text(obj.data('price-excl'));
            costContainer.find('[name="sim_name"]').val(name);
            costContainer.find('[name="sim_id"]').val(obj.data('sim-id'));

            if ($.trim(obj.data('sim-id')) != '') {
                $('#simcard-select').removeClass('validation-failed required-entry').addClass('validation-passed');
                $('#advice-required-entry-simcard-select').fadeOut();
            }
            $('#inlife-cash-amount').text(cost);
        };

        /** ================ Multisim addons ================ **/
        this.editMultisim = function(obj) {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').addClass('hidden');
            this.getInlifeBlock({
                'block': 'partials/edit_multisim',
                'edit': true,
                'billingOfferCode': $(obj).data('billingOfferCode')
            });
        };

        this.cancelEditMultisim = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').removeClass('hidden');
            inlifeWrapper.find('.multisim, #inlife-edit-block').addClass('hidden');
        };

        this.createMultisimOrder = function(obj) {
            var self = this;
            var form = $(obj).is( "form" ) ? $(obj) : $(obj).parents('form').first();
            var formData = form.serializeObject();
            $.extend(formData, self.formData);

            var callback = function() {
                window.setPageOverrideLoading && window.setPageOverrideLoading(true);
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: formData,
                    url: inlifeController + 'createMultiSimOrder',
                    success: function (data) {
                        if (data.error) {
                            if (data.html) {
                                $('#inlife-wrapper .inlife-loading').addClass('hidden');
                                $('#inlife-edit-block').html(data.html).removeClass('hidden');
                            }
                        } else if (!data.error) {
                            self.doMultisimPolling(data.polling_id, data.order_id);
                            self.multisimIntervalID = setInterval(function () {
                                self.doMultisimPolling(data.polling_id, data.order_id);
                            }, 10000);

                        } else {
                            console.log('Unknown response returned');
                        }
                    },
                    error: function () {
                        console.log('Failed to process requested action');
                    }
                });
            };

            self.inlifeLoad('add', 'loading', 'MultiSIM', '' , callback);

            return false;
        };

        this.doMultisimPolling = function(id, order_id) {
            var self = this;
            --self.performMultisimPollingCount;
            if (self.multisimIntervalID !== false) {

                $.ajax({
                    async: true,
                    type: 'POST',
                    data: { 'polling_id': id },
                    url: inlifeController + 'orderPolling',
                    success: function (data) {
                        if (data.error == true) {
                            self.multisimIntervalID = false;
                            // error
                            showModalError(data.message, data.serviceError, self.cancelEditMultisim);
                        } else if (data.error == false) {
                            if (data.hasOwnProperty('performed') && data.performed == true) {
                                self.multisimIntervalID = false;
                                // success
                                self.addInlifeAddon(false, true, order_id, data['sim']);
                            }
                        } else {
                            console.log('error occured / no response received');
                        }
                    }
                });

            }

            if (self.performMultisimPollingCount == 0) {
                // polling failed
                clearInterval(self.multisimIntervalID);
                self.multisimIntervalID = false;
            }
            return false;
        };

        this.quoteMultisim = function(obj) {
            var self = this;
            var formData = $(obj.form).serializeObject();
            self.formData = formData;

            if (obj.validator && obj.validator.validate()) {
                var callback = function() {
                    var data = {
                        'block': 'partials/quote_multisim',
                        'action': 'add',
                        'simulate': true,
                        'edit': true
                    };

                    // Add the fields from the form
                    $.extend(data, formData);

                    self.getInlifeBlock(data);
                };

                self.inlifeLoad('add', 'loading', 'Retrieve quote', '', callback);
            }
            return false;
        };

        this.toggleDelivery = function(value) {
            $('#inlife-wrapper .delivery-method').addClass('hidden');
            $('#delivery-method-'+value).removeClass('hidden');
            this.setPaymentOptions(value);
        }

        this.toggleMultisimAddon = function(obj) {
            var prevDiv = $(obj).parent().prev().toggleClass('hidden');
            $(obj).toggleClass('show');

            if ($(obj).hasClass('show')) {
                $(obj).text($(obj).data('hide-text'));
                $(obj).parent().find('input[type="checkbox"]').prop('checked', true);
                prevDiv.find('select[name^="prefix_multisim"]').removeClass('validation-passed validation-failed').addClass('validate-select');
                prevDiv.find('input[name^="postfix_multisim"]').removeClass('validation-passed validation-failed').addClass('validate-sim required-entry');
            } else {
                $(obj).text($(obj).data('show-text'));
                $(obj).parent().find('input[type="checkbox"]').prop('checked', false);
                prevDiv.find('select[name^="prefix_multisim"]').removeClass('validate-select validation-passed validation-failed');
                prevDiv.find('input[name^="postfix_multisim"]').removeClass('validate-sim required-entry validation-passed validation-failed');
            }

            var total = 0;
            $('.multisim .multi-details:not(.hidden)').each(function(index, elem){
                total += $(elem).data('oc-price');
            });
            total = '\u20AC ' + inlife.localeString(total, '.');
            $('.multisim-amount .amount').text(total);
        };

        this.setPaymentOptions = function(deliveryOption) {
            var self = this;
            var paymentOptions = {
                "deliver" : {
                    "cashondelivery": true, // Disable 'Reimbursement' for hardware only orders ?!
                    "checkmo": true
                },
                "pickup" : {
                    "payinstore": true,
                    "checkmo": true
                }
            };

            var optionsText = {
                "cashondelivery" : Translator.translate('Reimbursement'),
                "checkmo" : Translator.translate('Invoice'),
                "payinstore" : Translator.translate('Pay in store')
            };

            if (self.websiteCode == self.websites.retail
                || self.websiteCode == self.websites.belcompany
            ) {

                paymentOptions["direct"] = {
                    "payinstore": true,
                    "checkmo": true
                };
            }

            if (! self.customer.isBusiness) {
                if (paymentOptions[deliveryOption].hasOwnProperty("checkmo")) {
                    paymentOptions[deliveryOption]["checkmo"] = false;
                }
            }

            var paymentDropdown = $('[id="payment[method]"]');
            paymentDropdown.find('option').not('.choose').detach();

            if(paymentOptions.hasOwnProperty(deliveryOption)) {
                $.each(paymentOptions[deliveryOption], function(index, check) {
                    if(check == true) {
                        paymentDropdown.append($('<option />').val(index).text(optionsText[index]));
                    } else {
                        paymentDropdown.find('option[value="'+index+'"]').detach();
                    }
                });
            }
        };

        /** ================ VOM addons ================ **/
        this.editVom = function(obj, connected) {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').addClass('hidden');
            var data = {
                'block': 'partials/edit_vom',
                'edit': true,
                'connected_vom': connected
            };
            var formData = $(obj).data();
            this.formData = formData;
            $.extend(data, formData);
            this.getInlifeBlock(data);
        };

        this.cancelEditVom = function() {
            var inlifeWrapper =  $('#inlife-wrapper');
            inlifeWrapper.find('.home-screen, .home-wrapper .section').removeClass('hidden');
            inlifeWrapper.find('.vom, #inlife-edit-block').addClass('hidden');
        };

        this.confirmVom = function(obj) {
            if ( $(obj.form).find('[name="fixed[porting]"]:checked').val() == 0 ) {
                this.inlifeConfirm('vom', 'VastOpMobiel', 'VastOpMobiel', '');
            } else {
                if (obj.validator && obj.validator.validate()) {
                    var data = {
                        'block': 'inlife_confirm',
                        'action': 'vom',
                        'code': 'VastOpMobiel', // TODO: get addon code
                        'name': 'VastOpMobiel', // TODO: get addon name
                        'modal': true
                    }

                    // Add the fields from the form
                    $.extend(data, $(obj.form).serializeObject());

                    this.getInlifeBlock(data);
                }
            }
            return false;
        };

        this.quoteVom = function(obj) {
            var self = this;
            var formData = $(obj.form).serializeObject();

            delete self.formData.fixed;
            $.extend(formData, self.formData);
            self.formData = formData;

            if (obj.validator && obj.validator.validate()) {
                var callback = function() {
                    var data = {
                        'block': 'partials/quote_vom',
                        'action': 'add',
                        'simulate': true,
                        'edit': true
                    }

                    // Add the fields from the form
                    $.extend(data, formData);

                    self.getInlifeBlock(data);
                };

                self.inlifeLoad('add', 'loading', 'Retrieve quote', '', callback);
            } else {
                var failed_elements = Form.getElements(obj.form).findAll(function(elm){return $(elm).hasClass('validation-failed')}),
                    foundFixedAddressError = [],
                    foundContractAddressError = [];

                failed_elements.each(function(value) {
                    var str = value.id;
                    if (str.search(/\[fixed_address\]\[otherAddress\]/) != -1) {
                        var matches = str.match(/fixed\[(\d)\](.*)/);
                        foundFixedAddressError[matches[1]] = value;
                    }
                    if (str.search(/\[address_provider\]\[otherAddress\]/) != -1) {
                        var matches = str.match(/fixed\[(\d)\](.*)/);
                        foundContractAddressError[matches[1]] = value;
                    }
                });

                $.each(foundFixedAddressError, function() {
                    $(this).parents('.address-container').find('.editOtherAddress').trigger('click').parent().removeClass('hidden');
                })

                $.each(foundContractAddressError, function() {
                    $(this).parents('.address-container').find('.editOtherAddress').trigger('click').parent().removeClass('hidden');
                })
            }
            return false;
        };

        this.saveVom = function(obj) {
            var self = this;
            var form = $('form#saveVom');

            $.ajax({
                async: true,
                type: 'POST',
                data: form.serializeObject(),
                url: inlifeController + 'addInlifeAddon',
                success: function (data) {
                    if (data.error) {
                        showModalError(data.message, data.serviceError, self.cancelEditVom);
                    } else if (!data.error) {
                        self.inlifeLoad('add', 'loading', 'VastOpMobiel', '');
                    } else {
                        console.log('Unknown response returned');
                    }
                },
                error: function () {
                    console.log('Failed to process requested action');
                }
            });
        };

        this.vomValidationRules = {
            "0" : { // new number (not porting)
                "fixed[#][area_code]" : "required-entry validate-no-fixed-numbers"
            },
            "1" : { // existing number (porting)
                "fixed[#][number]" : "required-entry validate-fixed-number",
                "fixed[#][current_provider]" : "validate-select",
                "fixed[#][porting_date]" : "required-entry validate-date-nl",
                "fixed[#][customer_firstname]" : "required-entry validate-name",
                "fixed[#][customer_lastname]" : "required-entry validate-name validate-name-length",
                "fixed[#][customer_organization]" : "", // todo: mark as required only if business customer 'required-entry'
                "fixed[#][customer_contract_end_date]" : "validate-date-nl"
            }
        };

        this.validateVomAddress = function(obj) {
            var addressContainer = $(obj).parents('.address-container');
            var container = addressContainer.find('.extra.otherAddress');

            if ($(obj).val() == 'otherAddress') {
                container.find('.otherAddress input[id*="street"]').addClass('required-entry');
                container.find('.otherAddress input[id*="houseno"]').addClass('validate-number required-entry');
                container.find('.otherAddress input[id*="postcode"]').addClass('required-entry nl-postcode');
                container.find('.otherAddress input[id*="city"]').addClass('required-entry');
            } else {
                container.find('.otherAddress input[id*="street"]').removeClass('required-entry validation-failed').next('.validation-advice').remove();
                container.find('.otherAddress input[id*="houseno"]').removeClass('validate-number required-entry validation-failed').next('.validation-advice').remove();
                container.find('.otherAddress input[id*="postcode"]').removeClass('required-entry nl-postcode validation-failed').next('.validation-advice').remove();
                container.find('.otherAddress input[id*="city"]').removeClass('required-entry validation-failed').next('.validation-advice').remove();
            }
        };

        this.switchVomPorting = function(obj) {
            var inlifeWrapper =  $('#inlife-wrapper');
            $(obj).parents('.vom-block').find('.fixed-porting').addClass('hidden');
            $(obj).parents('.radio').next('.fixed-porting').removeClass('hidden')
            var porting = $(obj).val();
            var index = $(obj).data('index');

            $.each(this.vomValidationRules[porting], function(key, rule) {
                key = key.replace('#', index);
                $('[id="{0}"]'.format(key)).addClass(rule);
            });
            $.each(this.vomValidationRules[Math.abs(porting-1)], function(key, rule) {
                key = key.replace('#', index);
                $('[id="{0}"]'.format(key)).removeClass(rule).removeClass('validation-failed').next('.validation-advice').remove();
            });

        };

        this.loadMoreVoms = function(obj, fax) {
            var index = $(obj).data('index');
            var data = {
                'block': 'partials/vom_block',
                'index': index,
                'modal': false,
                'cancel': true,
                'loadFax': fax
            };

            $.ajax({
                async: true,
                type: 'POST',
                data: data,
                url: inlifeController + 'getInlifeBlock',
                success: function (data) {
                    if (data.error) {
                        console.log(data.error);
                    } else if (!data.error) {
                        if (data.html) {
                            $(obj).next('.vom-block').html(data.html);
                            $(obj).children('.plus').addClass('hidden');
                        }
                    } else {
                        console.log('Unknown response returned');
                    }
                },
                error: function () {
                    console.log('Failed to process requested action');
                }
            });
        };

        this.cancelVom = function(obj) {
            var vomBlock = $(obj).parents('.vom-block').first();
            vomBlock.prev().children('.plus').removeClass('hidden');
            vomBlock.empty();
        };

        this.getVomAreaCode = function(obj) {
            var self = this;
            var postcode = $.trim($(obj).val());
            if (postcode == '') {
                return false;
            }
            var parentBlock = $(obj).parents('.fixed-porting');
            parentBlock.find('.available-numbers').addClass('hidden').find('.results').empty();

            $.ajax({
                async: true,
                type: 'POST',
                data: {'postcode': postcode},
                url: inlifeController + 'getAreaCode',
                success: function (data) {
                    if (data.error) {
                        showModalError(data.message);
                    } else if (!data.error) {
                        if (data.html) {
                            var areaInput = parentBlock.find('.area-code');
                            areaInput.val(data.html);
                            // call get available fixed numbers
                            self.getVomFixedNumbers(areaInput);
                        }
                    } else {
                        console.log('Unknown response returned');
                    }
                },
                error: function () {
                    console.log('Failed to process requested action');
                }
            });

            return false;
        };

        this.getVomFixedNumbers = function(obj) {
            var areacode = $.trim($(obj).val());
            var more = false;
            var parentBlock = $(obj).parents('.fixed-porting').first();
            var resultsBlock = parentBlock.find('.available-numbers .results');

            var index = resultsBlock.data('index');
            if ($(obj).data('more') != undefined) {
                areacode = $('[id="fixed[{0}][area_code]"]'.format(index)).val();
                more = true;
            }
            if (!more) {
                resultsBlock.empty().parents('.available-numbers').addClass('hidden');
            }
            if (areacode == '') {
                return false;
            }

            $.ajax({
                async: false,
                type: 'POST',
                data: {'areacode': areacode},
                url: inlifeController + 'getFixedNumbers',
                success: function (data) {
                    if (data.error) {
                        var areaInput = parentBlock.find('.area-code');
                        areaInput.addClass('no-fixed-numbers');
                        Validation.test('validate-no-fixed-numbers', areaInput[0]);
                        showModalError(data.html, 'getAvailableFixedNumbers service call response');
                    } else if (!data.error) {
                        var areaInput = parentBlock.find('.area-code');
                        areaInput.removeClass('no-fixed-numbers');
                        Validation.test('validate-no-fixed-numbers', areaInput[0]);

                        if (data.html) {
                            parentBlock.find('.available-numbers').removeClass('hidden');

                            var inpCount = 0;
                            var inpChecked = $('[name="fixed[{0}][new_number]"]:checked'.format(index)).length;

                            $.each(data.html, function(i, item) {
                                // number already present in the list, skip it
                                if (resultsBlock.find('#no-{0}'.format(item)).length) return;

                                var div = $('<div/>', {
                                    class: 'radio',
                                    id: 'no-' + item
                                });
                                resultsBlock.append(div);

                                var label = $('<label/>', {text: item});
                                resultsBlock.find('div#no-' + item).append(label);

                                var input = $('<input/>', {
                                    type: 'radio',
                                    class: 'validate-one-required-by-name',
                                    name: 'fixed[{0}][new_number]'.format(index),
                                    value: item,
                                    checked: (inpCount == 0 && inpChecked == 0) ? true : false
                                });
                                inpCount++;
                                resultsBlock.find('div#no-{0} label'.format(item)).prepend(input);
                            });
                        }
                    } else {
                        console.log('Unknown response returned');
                    }
                },
                error: function () {
                    console.log('Failed to process requested action');
                }
            });

            return false;
        };

    }
})(jQuery);
