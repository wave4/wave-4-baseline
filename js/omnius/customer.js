/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function($) {
    window.Customer = function() { this.initialize.apply(this, arguments) };
    window.Customer.prototype = $.extend(VEngine.prototype, {
        _ajaxQueue: $({}),
        init: function() {
            this.initCtnSearch();
            this.setErrorCodes();
            if (window['store']) {
                this.cache = window['store'];
            }
            this.customerPin = {};
            this.customerId = null;
        },

        setErrorCodes: function(codes) {
            this.customerNeedsPin = false;
            this.errorCodes = codes || {
                'VOD-12-005-000': "The password specified  doesn't match"
            };
        },

        initCtnSearch: function(name) {
            var fieldName = name ? name : 'customer_ctn_search';
            var self = this;
            $('input[name="'+fieldName+'"]').keypress(function(event) {
                if (event.keyCode == 13) {
                    self.searchCustomerCtn();
                }
            });
        },

        filterCtn: function(ctns) {
            $('.ctn_record').each(function() {
                var el = $(this);
                var present = false;
                $.each(ctns, function(i, ctn) {
                    if (el.data('id') == ctn['entity_id']) {
                        present = true;
                    }
                });
                present ? el.show() : el.hide();
            });
        },

        searchCustomer: function (data, mode) {
            if (checkoutDeliverPageDialog == true) {
                // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
                $('#checkout_deliver_confirmation').data('function', 'customer.searchCustomer').data('param1', data).appendTo('body').modal();
                return false;
            }

            var el = $(data).parents('form');
            var isAdvanced = false;
            // Restrict search for house number if no postcode is entered
            var hn = false;
            var pc = false;
            if (this.helper.isObject(el)) {
                data = $.map($(el).serializeArray(el), function(obj) {
                    if (obj['name'] == 'customer_type') {
                        isAdvanced = true;
                    }
                    obj['value'] = $.trim(obj['value']);

                    if (obj.name == 'billing_postcode' && obj.value != '') {
                        pc = true;
                    }
                    if (obj.name == 'billing_street' && obj.value != '') {
                        hn = true;
                    }
                    return obj;
                });
            }

            if (hn && !pc) {
                showModalError(Translator.translate('House number search must always contain postcode'));
                return false;
            }
            switch (mode) {
                case 'cart' :
                    return this.searchCustomerOnCart(data, el, isAdvanced);
                    break;
                case "indirect" :
                    return this.searchCustomerIndirect(data, el);
                    break;
            }

            var self = this;
            var callback = function(data, status) {
                var customerList = el.find('#customer_list');
                var customerSearchResults = el.find('#customer_search_results');

                customerList.html('');
                customerSearchResults.removeClass('empty');

                //check if returned customer has retention packages (data.additionalInfo)
                // prompt user of loss, continue only if yes
                if (undefined != data.additionalInfo) {
                    //has retention OR promo
                    //TODO
                }

                if (data.message) {
                    if (isAdvanced) {
                        customerList.html('');
                        customerSearchResults.addClass('empty');
                        var body = '';
                        var title = '';
                        if(data.hasOwnProperty('error_fields') && data.error_fields.length > 0 ){
                            var errors = "";
                            if (data.error_fields) {
                                $.each(data.error_fields, function(index, error) {
                                    errors += "<span class='error'>" + error + "</span><br>";
                                });
                            }
                            body = errors;
                        } else {
                            body = data.message;
                        }

                        showModalError(body);
                    } else {
                        customerSearchResults.addClass('empty');

                        var errors = "";
                        if (data.error_fields) {
                            $.each(data.error_fields, function(index, error) {
                                errors += "<span class='error'>" + error + "</span><br>";
                            });
                        }
                        errors = errors ? errors : data.message + errors;
                        customerList.html(errors);
                    }
                } else {
                    if (isAdvanced) {
                        window.location.reload();
                        return;
                    }

                    $.each(data, function(index, item) {
                        if (item.hasOwnProperty('limit_reached')) {
                            customerList.prepend($('<div></div>').addClass('search-result-warning').append('<b>' + item.limit_reached + '</b>'));
                            return true;
                        }

                        var name = item.prefix + ' ' + item.firstname + ' ' + (item.middlename && item.middlename.length ? item.middlename : '' ) + ' ' + item.lastname;// (item.middlename ? (' ' + item.middlename + ' ') : '')

                        if (item.is_business == 1) {
                            name = item.company_name;
                        }

                        var div = $('<div></div>').addClass('result').attr('id', item.entity_id);

                        if (item.is_prospect == 0) {
                            div.data('prospect', 'false');
                            div.append('<b>' + name + ' (BAN: ' + item.ban + ')' + '</b>');

                            if (item.customer_label != undefined) {
                                div.append('<span class="campaign badge badge-da">' + item.customer_label + '</span>');
                            } else {
                                div.append('<span class="campaign badge badge-da"></span>');
                            }

                            div.append('<p>' + item.billing_street + ', '+ item.billing_postcode + ', ' + item.billing_city + '</p>');
                        } else {
                            div.data('prospect', 'true');
                            div.append('<b>' + name + ' (EMAIL: ' + item.is_prospect + ')' + '</b>');
                        }

                        if ($('#customer_ctn').val()) {
                            div.append('<p>CTN:' + item.customer_ctn + '</p>');
                        }
                        if ($('#postcode_zip').val() || $('#postcode_letters').val()) {
                            div.append('<p>PC:' + item.billing_postcode + '</p>');
                        }
                        div.click(function() {
                            if (el.prop('id') == 'top_menu_search') {
                                var customerId = $(this).prop('id');
                                var section = el.parents('.cb_slide_panel').data('section');
                                topMenuSearch(customerId, section);
                            } else {
                                $(".col-left .enlarge.right-direction").css('display', 'block');
                                var afterCallBack = function () {
                                    self.post('customer.sticker', {}, function (data) {
                                        if (data.sticker) {
                                            $('.col-left').find('#ctn-tabs').html(jQuery(data.sticker).find('#ctn-tabs').html());
                                            bindSlidepanelLeft();
                                        }
                                    });
                                };
                                self.loadCustomerData($(this).prop('id'), $(this).data('prospect'), false, afterCallBack, false, true);
                            }

                            jQuery('.el-tab').removeClass('active');
                            jQuery('a[href=#oo-customer-search]').addClass('active');
                        });
                        customerList.append(div);
                    });
                }
                customerSearchResults.first().show();
                triggerConfiguratorEvent('configurator.customersfound', '');
            };

            this.post('customer.search', data, callback, function(err, status, xhr) {
                /**@var {XMLHttpRequest} xhr */
                if (xhr && xhr.status == 414) {
                    err = 'Request too long.';
                }
                if(xhr && xhr.status != 0) {
                    showModalError(err);
                }
            });
        },

        getLoggedInCustomer: function() {
            return this.customerId;
        },

        setLoggedInCustomer: function (customerId) {
            this.customerId = customerId;
            return this;
        },

        searchCustomerIndirect: function(data, el) {

            var callback = function(data, status) {
                var url = window.location.href;
                if (data.error == true) {
                    showModalError(data.message);
                } else {
                    if(el.prop('id') == 'top_menu_search') {
                        var customerId = data.customerData.entity_id;
                        var section = el.parents('.cb_slide_panel').data('section');
                        topMenuSearch(customerId,section);
                    } else {
                        setPageLoadingState(true);
                        window.location.href = url;
                    }
                }
            };

            this.post('customer.search.indirect', data, callback, function(err, status, xhr) {if(xhr && xhr.status != 0) showModalError(err);});
        },

        loadCustomerData: function(customerId, prospect, useCache, afterLoadCallback, unloadQuote, doAjaxUpdate) {
            var self = this;
            var cacheKey = 'customer/{0}'.format(customerId);
            var rightSidebarContainer = $('.col-right.sidebar');

            var callback = function(data, status) {
                // Always clear the product collection when customer is loaded
                window['prod_mobile'] = {};

                if (data.error) {
                    showModalError(data.message && data.message.length ? data.message : 'Could not load customer data. Please try again.');
                    return;
                }

                if (data.sticker) {
                    $('.col-left').find('.sticker').html(data.sticker);
                    bindSlidepanelLeft();
                }

                if (data.saveCartModal) {
                    $('#save-cart-modal').replaceWith(data.saveCartModal);
                }

                if (data.rightSidebar) {
                    $('.col-right .sticker').replaceWith(data.rightSidebar);
                }

                if(rightSidebarContainer.children('.expanded').length > 0){
                    $('.col-right .sticker').hide();
                    expandTheRightSidebar();
                }

                window['customerLabel'] = data.customerData.customer_label;
                window['isGemini'] = data.customerData.customer_label ? (data.customerData.customer_label.toLowerCase() == 'gemini') : false;

                self.cache.set(cacheKey, data);
                var filtersWrapper;
                if (typeof configurator != 'undefined' && data.customerData.is_business == "0" && $('#subscription-block').length > 0) {
                    var consumerFilter = $('#subscription-block div[data-filter-for="identifier_cons_bus"] a[data-selection="{0}"]'.format(window.consumerIdentifier));
                    if (consumerFilter) {
                        if (consumerFilter.hasClass('active')) {
                            consumerFilter.trigger('click');
                        }
                        configurator.filter(consumerFilter, true);
                        $('#subscription-block div[data-filter-for="identifier_cons_bus"]').addClass('hidden');
                    }
                    filtersWrapper = $('#config-wrapper #subscription-block div[data-filter-for="identifier_subscr_type"] ul');
                    if (filtersWrapper) {
                        filtersWrapper.find('li').remove();
                        $.each(window.filter_consumer_mobile.subscription.identifier_subscr_type.options, function (index, elem) {
                            filtersWrapper.append(
                                '<li><a href="#" data-filter-value="{0}" data-selection="{1}" onclick="configurator.filter(this)">{0}</a></li>'.format(elem, index)
                            )
                        });
                    }
                }

                if (window['configurator']) { //has active configurator
                    var packageId = window['configurator']['packageId'];
                    if (packageId) {
                        $('.side-cart[data-package-id="' + packageId + '"]').addClass('active');
                    } else {
                        $('.side-cart[data-package-id="1"]').addClass('active');
                    }
                }

                if (data.doAjaxUpdate) {
                    self.poolRefreshData();
                }

                if (afterLoadCallback) {
                    afterLoadCallback();
                }
                
                // Set the logged in customer as current
                self.setLoggedInCustomer(customerId);
            };

            if (useCache && this.cache.get(cacheKey)) {
                callback(this.cache.get(cacheKey));
            } else {
                var options;

                options = {
                    id: customerId
                };

                options.unload_quote = unloadQuote;
                options.doAjaxUpdate = doAjaxUpdate;

                this.post('customer.load', options, callback, function (err, status, xhr) {
                    if (xhr && xhr.status != 0) showModalError(err);
                });
            }
            triggerConfiguratorEvent('configurator.customerloaded', '');
        },

        poolRefreshData: function(callback) {
            var thisSelf = this;
            window.checkoutEnabled = false;
            // Flag to show the warning if the agent tries to reload page during customer lookup
            window.showCustomerLoadWarning = true;
            // disable the continue button
            $('#cart_totals input').attr('disabled', 'disabled');
            if (arguments.length > 0) {
                self.toCall = true;
                self.call = arguments[0];
            }

            var showRetainableCall = this.customerNeedsPin;
            $.ajax({
                async: true,
                type: 'POST',
                data: $.extend({}, thisSelf.customerPin),
                url: '/customer/details/poolCustomerData',
                success: function(data) {
                    // enable the continue button
                    if(window.hasCompletePackage == true || data.complete) {
                        $('#cart_totals input').attr('disabled', false);
                    }
                    window.checkoutEnabled = true;
                    if($('#customer-info-section').length && $('#customer-info-section').find('.badge-da').length && data.hasOwnProperty('data') && data.data.hasOwnProperty('customer_label')){
                        $('#customer-info-section').find('.badge-da').first().html(data.data.customer_label);
                    }

                    if (data.hasOwnProperty('serviceError')) {
                        if (data.hasOwnProperty('code')) {
                            if (thisSelf.errorCodes.hasOwnProperty(data.code)) {
                                thisSelf.customerNeedsPin = true;
                                var pinModal = $('#customer_pin_modal');
                                pinModal.find('input[name="cart"]').val(false);
                                pinModal.modal();
                                pinModal.on('hidden.bs.modal', function () {
                                    thisSelf.logoutCustomer();
                                });
                                return;
                            }
                        }

                        showModalError(data.message, data.serviceError);
                        if(data.code && (data.code == 'VOD-12-009-000' || data.code == 'VOD-12-003-000')){
                            thisSelf.customerNeedsPin = false;
                            $.ajax({async: true, type: 'POST', data: '', url: '/customer/details/setCustomerSync'});
                        }
                        return;
                    } else if (data.error) {
                        showModalError(data.message);
                        return;
                    }

                    if(showRetainableCall){
                        thisSelf.customerNeedsPin = false;
                        thisSelf.showOnlyRetainable($('.refresh.show_retainable'));
                    }

                    $.ajax({async: true, type: 'POST', data: '', url: '/customer/details/setCustomerSync'});
                    thisSelf.customerNeedsPin = false;
                    if (data.data) {
                        var customer = data.data;
                        var addressContainer = $('.customer-address');
                        if (addressContainer.length) {
                            addressContainer.find('.address-customer-gender').text("{0}".format(
                                (customer.is_business == 1) ?(
                                    ((customer.hasOwnProperty('contractant_prefix') && customer.contractant_prefix && customer.contractant_prefix.length) ? ' ' + Translator.translate(customer.contractant_prefix) : '' ) + ' '
                                    + customer.contractant_firstname + ' '
                                    + (customer.contractant_middlename && customer.contractant_middlename.length ? ' '
                                    + customer.contractant_middlename: '' ) +  ' '
                                    + customer.contractant_lastname
                                    )
                                : (
                                    ((customer.hasOwnProperty('prefix') && customer.prefix && customer.prefix.length) ? ' ' + Translator.translate(customer.prefix) : '' ) + ' '
                                    + customer.firstname + ' '
                                    + (customer.middlename && customer.middlename.length ? ' '
                                    + customer.middlename: '' ) +  ' '
                                    + customer.lastname
                                    )));

                            addressContainer.find('.address-customer-value').text(customer.customer_value);
                            addressContainer.find('.address-customer-address').text("{0}, {1}, {2}".format(customer.shipping.street, customer.shipping.postcode, customer.shipping.city));
                            addressContainer.find('.address-customer-campaign').text(customer.campaign);
                            if(customer.pin != undefined && customer.pin.length > 0) {
                                addressContainer.find('.address-customer-pin').data('state-0', customer.pin);
                            }
                            if(customer.dob != false) {
                                var dob = (customer.is_business == 1) ? new Date(customer.contractant_dob) : new Date(customer.dob);
                                var pad = function pad(width, string, padding) {
                                    padding = padding || ' ';
                                    return (width <= string.length) ? string : pad(width, padding + string, padding)
                                };
                                addressContainer.find('.address-customer-dob').text("{0}: {1}".format(customer.dob_label, ('0' + dob.getDate()).slice(-2) + '-' + ('0' + (dob.getMonth()+1)).slice(-2) + '-' + dob.getFullYear() ));
                            }

                            $('.ctnamount.badge').text(customer.ctn.length);

                            window.a = customer;
                        }

                        if (data.hasOwnProperty('inlife_orders')) {
                            if ($('#orders-content #inlife-orders').length) {
                                $('#orders-content #inlife-orders').html(data.inlife_orders);
                            }
                        }
                        // If customer label is in-migration refresh the page in order to disable the buttons
                        if (customer.customer_label == 'In-Migration') {
                            // Also hide the configurator in case it is shown
                            $('#config-wrapper').hide();
                            $('#homepage-wrapper').show();
                            alert(Translator.translate('Customer is currently In-Migration, no sales activities are possible'));
                            if(window['customerLabel'] != customer.customer_label) {
                                window.location.href = window.location.href;
                            }
                        }

                        if(customer.ctn_overlay != undefined && $('#products-content').length > 0){
                            $('#customer_details_panel .wrapper').html(customer.ctn_overlay);
                            $('#product_info_left').trigger('click').addClass('active');
                        }

                    } else {
                        console.info('No data retrieved. Trying again...');
                    }

                    localStorage.setItem('doAjaxUpdate', 0);
                },
                error: function() {
                    // enable the continue button
                    if(window.hasCompletePackage == true) {
                        $('#cart_totals input').attr('disabled', false);
                    }
                    window.checkoutEnabled = true;
                    console.log('Failed to update customer'); //TODO how should this be handled?
                },
                complete: function () {
                    if (typeof callback !== 'undefined') {
                        callback();
                    }
                }
            });
        },

        searchCustomerByPin: function(obj) {
            var formData = $(obj.form).serializeObject();
            if (obj.validator && obj.validator.validate()) {
                this.customerPin = { pin: formData.pin };
                var pinModal = $('#customer_pin_modal');
                pinModal.unbind('hidden.bs.modal');
                pinModal.modal('hide');

                if (formData.cart == 'true') {
                    this.poolRefreshDataOnCart();
                } else {
                    this.poolRefreshData();
                }
            }

            return false;
        },

        searchCustomerOnCart: function(data, el, isAdvanced) {

            var self = this;
            var callback = function(data, status) {
                var customerList = el.find('#customer_list');
                var customerSearchResults = el.find('#customer_search_results');

                customerList.html('');
                customerSearchResults.removeClass('empty');
                if(data.message) {
                    if (isAdvanced) {
                        customerList.html('');
                        customerSearchResults.addClass('empty');
                        var body = '';
                        var title = '';
                        if(data.hasOwnProperty('error_fields') && data.error_fields.length > 0 ){
                            var errors = "";
                            if (data.error_fields) {
                                $.each(data.error_fields, function(index, error) {
                                    errors += "<span class='error'>" + error + "</span><br>";
                                });
                            }
                            body = errors;
                        } else {
                            body = data.message;
                        }

                        showModalError(body);
                    } else {
                        customerSearchResults.addClass('empty');

                        var errors = "<br>";
                        if (data.error_fields) {
                            $.each(data.error_fields, function(index, error) {
                                errors += "<span class='error'>" + error + "</span><br>";
                            });
                        }

                        customerList.html(data.message+errors);
                    }
                } else {
                    if (isAdvanced) {
                        $('html body').html('<p style="padding-top: 25%">'+$('#loading_customer_data_label').val()+'</p>');
                        window.location.reload();
                        return;
                    }

                    $.each(data, function(index, item) {
                        if (item.hasOwnProperty('limit_reached')) {
                            customerList.prepend($('<div></div>').addClass('search-result-warning').append('<b>' + item.limit_reached + '</b>'));
                            return true;
                        }
                        if(item.is_prospect != 0) {
                            return;
                        }
                        var name = item.prefix + ' ' + item.firstname + ' ' + (item.middlename && item.middlename.length ? (' ' + item.middlename + ' ') : '') + item.lastname;
                        if(item.is_business == 1) {
                            name = item.company_name;
                        }
                        var div = $('<div></div>').addClass('result').attr('id', item.entity_id);
                        div.append('<b>' + name + ' (BAN: ' + item.ban + ')' + '</b>');
                        div.append('<p>' + item.billing_street + ', '+ item.billing_postcode + ', ' + item.billing_city + '</p>');
                        if(el.find('#customer_ctn').val()) {
                            div.append('<p>CTN:' + item.customer_ctn+'</p>');
                        }
                        if(el.find('#postcode_zip').val() || el.find('#postcode_letters').val()) {
                            div.append('<p>PC:' + item.billing_postcode+'</p>');
                        }
                        div.click(function() {
                            $('.col-left.sidebar').css('width', 'auto');
                            if(!$("#saveCustomer #customer_info").hasClass('hidden')){
                                customer.loadCustomerDataOnCart($(this).prop('id'));
                            }else{
                                $('#load_customer_confirmation').data('id',($(this).prop('id'))).modal();
                            }
                        });
                        customerList.append(div);
                    });
                }
                customerSearchResults.first().show();
            };
            data.push({name: 'cart', value: 'true'});

            this.post('customer.search', data, callback, function(err, status, xhr) {if(xhr && xhr.status != 0) showModalError(err);});
        },

        loadCustomerDataOnCart: function(customerId, useCache) {
            var self = this;
            var cacheKey = 'customer/{0}'.format(customerId);
            $('#load_customer_confirmation').modal('hide');
            $('#search-client').modal('hide');

            var callback = function(data, status) {

                setPageLoadingState(true);
                if(data.error) {
                    setPageLoadingState(false);
                    showModalError(data.message);
                    return false;
                }

                self.cache.set(cacheKey, data);

                self.poolRefreshDataOnCart();
            };
            if (useCache && this.cache.get(cacheKey)) {
                callback(this.cache.get(cacheKey));
            } else {
                this.post('customer.load', {id: customerId, cart: true}, callback, function(err){showModalError(err);});
            }
        },

        poolRefreshDataOnCart: function() {
            var self = this;
            $.ajax({
                async: true,
                type: 'POST',
                data: $.extend({cart: true}, self.customerPin),
                url: '/customer/details/poolCustomerData',
                success: function(data) {
                    setPageLoadingState(false);
                    //TODO if error occurs, the page remains with 'Laden klantgegevens ...'. How should this be handled?
                    if (data.hasOwnProperty('serviceError')) {
                        if (data.hasOwnProperty('code')) {
                            if (self.errorCodes.hasOwnProperty(data.code)) {
                                var pinModal = $('#customer_pin_modal');
                                pinModal.find('input[name="cart"]').val(true);
                                pinModal.modal();
                                return;
                            }
                        }
                        showModalError(data.message, data.serviceError);
                        return;
                    } else if (data.error) {
                        showModalError(data.message);
                        return;
                    }
                    $('html body').html('<p style="padding-top: 25%">'+$('#loading_customer_data_label').val()+'</p>');
                    window.location.href = window.location.href; //must refresh to reload prices
                },
                error: function() {
                    setPageLoadingState(false);
                    console.log('Failed to update customer'); //TODO how should this be handled?
                }
            });
        },

        logoutCustomer: function() {

            // If agent tries to change the customer while on Checkout delivery show confirmation modal
            if (checkoutDeliverPageDialog === true) {
                jQuery('#checkout_deliver_confirmation').data('function','customer.logoutCustomer').appendTo('body').modal();
                return false;
            }

            var callback = function(data, status) {
                if (data.sticker) {
                    $('.col-left').find('.sticker').html(data.sticker);
                }
                if (data.saveCartModal) {
                    $('#save-cart-modal').replaceWith(data.saveCartModal);
                }
                $('html body').html('<p style="padding-top: 25%">'+$('#unloading_customer_data_label').val()+'</p>');
                window.location.reload(true);
            };
            this.post('customer.logout', {}, callback, function(err, status, xhr) {if(xhr && xhr.status != 0) showModalError(err);});
        },
        enableRetentionOverride: function() {
            // Show all numbers
            $('div.ctn_box.retainablectn').removeClass('retainable').addClass('retainablectn-override');
            // Remove the numbers already present in the cart
            $('.mark_ctn[data-rawctn!=""]').each(function (id, el) {
                $('.ctn_record[data-rawctn="' + $(el).data('rawctn') + '"] div.ctn_box.retainablectn').removeClass('retainablectn-override').addClass('retainable');
            })
        },

        showOverrideCtnPopup: function () {
            $('#ctn_override_modal').appendTo('body').modal();
        },

        showOnlyRetainable: function (showRetainableBtn) {
            //$('.ctn_checkbox').hide();
            //var values = $("input[type='checkbox'][class='ctn_checkbox']").map(function() {return this.checked ? $(this).val() : '';}).get();
            $(showRetainableBtn).attr('disabled', true);
            var self = this;

            // Wait for poolCustomerData to finish
            if (!window.checkoutEnabled) {
                setPageLoadingState(true);
                if (!window.showOnlyRetainableInterval) {
                    window.showOnlyRetainableInterval = setInterval(function () {
                        self.showOnlyRetainable(showRetainableBtn)
                    }, 1000);
                } else if (self.customerNeedsPin === true) {
                    setPageLoadingState(false);
                    clearInterval(window.showOnlyRetainableInterval);
                    var pinModal = $('#customer_pin_modal');
                    pinModal.find('input[name="cart"]').val(false);
                    pinModal.modal();
                    $(showRetainableBtn).attr('disabled', false);
                    return;
                }
                return;
            } else {
                if (window.showOnlyRetainableInterval) {
                    clearInterval(window.showOnlyRetainableInterval);
                }
            }

            if (self.customerNeedsPin === true) {
                var pinModal = $('#customer_pin_modal');
                pinModal.find('input[name="cart"]').val(false);
                pinModal.modal();
                $(showRetainableBtn).attr('disabled', false);
                return;
            }

            var retainableSelect = $('#select_only_retainable');
            $('#no_retainable_msg').empty().hide();
            var callback = function (data, status) {
                var creditProfileCheckResult = data.creditProfileCheckResult;
                setPageLoadingState(false);
                $(showRetainableBtn).attr('disabled', false);
                if (creditProfileCheckResult.hasOwnProperty('serviceError')) {
                    showModalError(data.message, data.serviceError);
                    return;
                } else if (creditProfileCheckResult.error) {
                    if (creditProfileCheckResult.manualOverride) {
                        self.showOverrideCtnPopup();
                    } else {
                        $('#no_retainable_msg').text(data.message && data.message.length ? data.message : 'Failed to process your request. Please try again.').show();
                    }
                    return;
                } else {
                    $('.ctn_box.retainable').hide();
                    $('.retainable').hide();
                    $('retainable-ctn').removeClass('retainable-ctn');
                    $('.ctn_checkbox').prop('checked', false).prop('disabled', true);
                    if (data.hasOwnProperty('ctns')) {
                        $.each(data['ctns'], function (i, value) {
                            var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                            el.addClass('retainable-ctn')
                                .find('.retainable').show().end()
                                .find('.ctn_checkbox').prop('disabled', false).end();
                            if (value.status != 'ACTIVE') {
                                el.find('.retainable-number').html(value.ctn + ' (' + value.status + ')');
                            }
                            // Show retainable status
                            var retainableRoot = el.find('[data-type="retainablestatus"]');
                            retainableRoot.find('p:eq(0)').show().find('span').text(Translator.translate(value.retainableStatus));
                            retainableRoot.find('p:eq(1)').show().find('span').text(value.reasoncode).show();
                            if (value.reasoncode === null) {
                                retainableRoot.find('p:eq(1)').hide();
                            }
                        });
                    }

                    $('#one-off-deal').prop('disabled', !data.oneOffDeal);
                    if (data.creditProfileCheckResult.ctns.length > 0) {
                        $.each(data.creditProfileCheckResult.ctns, function (index, value) {
                            if (value['retainable'] == false) {
                                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                                el.find('.retainablectn').removeClass('hide').parent().addClass('hide');
                                el.find('.notretainablectn').parent().removeClass('hide');
                                el.find('.notretainablectn')
                                    .attr('data-original-title', value.reasoncode)
                                    .tooltip('fixTitle');
                                //el.find('.notretainablectn').parent().removeClass('hide');
                            } else {
                                var el = $('.ctn_record[data-rawctn="' + value.ctn + '"]');
                                el.find('.retainablectn').removeClass('hide').parent().removeClass('hide');
                                el.find('.notretainablectn').parent().addClass('hide');
                            }
                        });
                    } else {
                        $('#profile_credit_status').addClass('hidden');
                    }

                    if (retainableSelect.length) {
                        retainableSelect.find('#only_retainable_check').removeAttr('disabled');
                    }
                }
                window.checkCanAddToCart && checkCanAddToCart();
            };
            this.post('customer.showRetainable', {}, callback, function (err) {
                showModalError('Er is een probleem opgetreden bij de communicatie met de Dealer Adapter. De volgende fout is opgetreden: ' + err)
            });
        },

        selectOnlyRetainable: function(checked) {
            var ctnRecord = $('.ctn_record');
            var ctnChecbox = $('.ctn_checkbox');
            if(checked) {
                ctnRecord.each(function(index, item) {
                    if($(item).find('.retainable').filter(':visible').length > 0) {
                        $(item).find('.ctn_checkbox').prop('checked', true);
                        $(item).addClass('selected');
                    }
                    else{
                        $(item).find('.ctn_checkbox').prop('checked', false);
                        $(item).find('.ctn_checkbox').prop('disabled', true);
                    }
                });
            } else {
                ctnRecord.removeClass('selected');
                ctnChecbox.prop('checked', false);
                //To avoid enabling the inlife CTN's.
                //ctnChecbox.prop('disabled', false);
            }
            window.checkCanAddToCart && checkCanAddToCart();
        },

        afterExpandLeft: function (element) {
            var elId = $(element).attr('id');
            if (!elId) {
                return;
            }

            // TODO, FIXME, XXX; quick and dirty fix
            // temp dirty fix for removing active state in menu
            $('#close_customer_details').click(function () {
                $('#ctn-tabs li').removeClass('active');
            });

            if (elId === 'product_info_left') {
                //set max-height for the ctn_overview
                setMaxHeightCTNOverview();
            }
        },

        switchCustomerTab: function(tab, tabNr) {
            var topMenuItem = $('#ctn-tabs');
            $('#customer_details_panel .tab-content .tab-pane:visible').first().removeClass('active');
            $('#'+tab).addClass('active');

            topMenuItem.find('li').removeClass('active');
            topMenuItem.find('li').eq(tabNr).addClass('active');
        },

        searchCustomerCtn: function() {
            var ctn = $.trim($('input[name="customer_ctn_search"]').val());
            if(ctn == '') {
                $('#list_ctns').find('.ctn_record').show();
            } else{
                $.each($('#list_ctns').find('.ctn_record'), function(index, item) {
                    if($.trim($(item).find('label').first().html()) == ctn) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        },

        showMemo: function(memo) {
            $('.memo_detail_content').removeClass('active');
            $(memo).addClass('active');
            $('#memo_data').html($(memo).find('.memo_content_data').first().html());
        },

        switchMemos: function(element, type) {
            $('#memo-tabs').find('li').removeClass('active');
            if(type == 'memo') {
                $('#memo_content_general').show();
                $('#memo_content_case').hide();
            }
            else {
                $('#memo_content_general').hide();
                $('#memo_content_case').show()
            }
            $(element).parent().addClass('active');
        },

        formatPhoneNumberInt: function (phoneNumber) {
            var tempPhoneNumber = phoneNumber.trim();
            if (tempPhoneNumber.substring(0, 3) === '316' && tempPhoneNumber.length === 11) {
                var firstPart = tempPhoneNumber.substring(3, 6);
                var secondPart = tempPhoneNumber.substring(6, 8);
                var thirdPart = tempPhoneNumber.substring(8, 11);
                return '316 ' + firstPart + ' ' + secondPart + ' ' + thirdPart;
            } else {
                var firstPart = tempPhoneNumber.substring(2, 5);
                var secondPart = tempPhoneNumber.substring(5, 7);
                var thirdPart = tempPhoneNumber.substring(7, 10);
                return '316 ' + firstPart + ' ' + secondPart + ' ' + thirdPart;
            }
        },

        formatPhoneNumber: function (phoneNumber) {
            var tempPhoneNumber = phoneNumber.trim();
            if (tempPhoneNumber.substring(0, 3) === '316' && tempPhoneNumber.length === 11) {
                var firstPart = tempPhoneNumber.substring(3, 6);
                var secondPart = tempPhoneNumber.substring(6, 8);
                var thirdPart = tempPhoneNumber.substring(8, 11);
                return '06 ' + firstPart + ' ' + secondPart + ' ' + thirdPart;
            } else {
                return phoneNumber;
            }
        },

        expandRightSidebar: function() {
            var direction = '';
            var enlargeButton = $('.enlarge');
            if($(enlargeButton).hasClass('left-direction')) {
                direction = 'left';
                var leftSide = $('.col-left').outerWidth();
                $(enlargeButton).removeClass('left-direction');
                $(enlargeButton).addClass('right-direction');
            }
            else {
                direction = 'right';
                var rightSide = $('.col-left').outerWidth();
                $(enlargeButton).removeClass('right-direction');
                $(enlargeButton).addClass('left-direction');
            }
            var rightDirection = $(enlargeButton).hasClass('right-direction');
            var panelDefaultWidth = 300;
            var distance = $('.main').outerWidth() - ($(enlargeButton).outerWidth() * 2);

            if($(enlargeButton).hasClass('expanded')) {
                var self = $(enlargeButton);
                $(enlargeButton).parent().find('.panel-content').fadeOut({
                    queue: false,
                    duration: 500,
                    complete: function() {
                        self
                            .siblings('.sticker')
                            .fadeIn(500);
                        window.configurator && window.configurator.assureElementsHeight();
                        if(direction == 'left') {
                            $('.col-right').find('#right-panel-cart').remove();
                        }
                    }
                });
                $(enlargeButton).parent().animate({
                    width: panelDefaultWidth
                }, 500);
                $('.col-main').fadeIn({queue: true, duration: 500});

                $(enlargeButton).removeClass('expanded').addClass('contracted');

            } else {

                if(direction == 'left') {
                    expandTheRightSidebar();
                }
                //if the other panel is already expanded, contract it back
                if($('button.expanded').length>0) {
                    $('button.expanded').first().trigger('click');
                }

                var difference = $(enlargeButton).parent().hasClass('col-left') ? rightSide : leftSide;
                animateWidth($(enlargeButton).parent(), distance - difference);

                $('.col-main').fadeOut({queue: true, duration: 500});

                $(enlargeButton).parent().find('.sticker').fadeOut({
                    queue: false,
                    duration: 500,
                    complete: function() {
                        $(this)
                            .siblings('.panel-content')
                            .fadeIn(500);

                    }
                });

                $(enlargeButton).removeClass('contracted').addClass('expanded');
            }
        },

        modifyInlifeCtn: function (ctn) {
            if (ctn) {
                $(location).attr('href', '/configurator/inlife/?ctn=' + ctn);
                return true;
            }
            return false;
        },

        loadInlifeOrder: function (orderId) {
            $.ajax({
                async: true,
                type: 'POST',
                url: '/configurator/inlife/getInlifeOrderDetails',
                data: {'orderId': orderId},
                success: function(data) {
                    if (data.hasOwnProperty('serviceError')) {
                        showModalError(data.message, data.serviceError);
                        return;
                    } else if (data.error) {
                        showModalError(data.message);
                        return;
                    }
                    $('#orders-content #open-order').html(data.html).show();
                    $('#orders-content #orders-overview').hide();
                    $('#orders-content .tab-title').hide();
                },
                error: function() {
                    console.log('Failed to load In Life order');
                }
            });
        },

        ajaxQueue: function(type, url, data, async) {
            var jqXHR;
            var dfd = $.Deferred();
            var promise = dfd.promise();
            var self = this;

            function doRequest(next) {
                jqXHR = $.ajax({
                    url: url,
                    cache: true,
                    async: async || false,
                    data: data,
                    type: type
                });
                jqXHR.done(dfd.resolve)
                    .fail(dfd.reject)
                    .then(next, next);
            }

            self._ajaxQueue.queue(doRequest);
            promise.abort = function (statusText) {
                if (jqXHR) {
                    return jqXHR.abort(statusText);
                }

                var queue = self._ajaxQueue.queue();
                var index = $.inArray(doRequest, queue);

                if (index > -1) {
                    queue.splice(index, 1);
                }

                dfd.rejectWith({
                    type: type,
                    url: url,
                    data: data,
                    async: async
                }, [ promise, statusText, "" ]);

                return promise;
            };

            return promise;
        },

        /**
         *
         * @param section
         */
        updateSectionResults: function (section) {
            var self = this;
            var types = {
                'np': [
                    'rejected', 'ongoing', 'completed'
                ],
                'cc': [
                    'rejected', 'ongoing', 'additional', 'completed'
                ],
                'vc': [
                    'rejected', 'failed'
                ]
            };
            var currentSection = jQuery('#{0}'.format(section));
            currentSection.find('.results').html('');
            currentSection.parents('.menu-middle:first').find('.ajax-details').addClass('hidden');
            types[section.substr(0, 2)].each(function (el, index) {
                self.ajaxQueue(
                    "POST",
                    MAIN_URL + 'dataimport/index/updateSectionResults',
                    {section: section, type: el, page: 1},
                    true
                )
                    .done(function (response) {
                        if ((index + 1) != types[section.substr(0, 2)].length) {
                            setPageOverrideLoading(true);
                        } else {
                            setPageOverrideLoading(false);
                        }
                        if (response.error) {
                            showModalError(response.message);
                        } else {
                            currentSection.find('.results').append(response.html);
                        }
                    });
            });
        }
    });
})(jQuery);

function showOtherAddress(self) {
    var container = jQuery(self).parents('.search-address');
    container.find('.delivery_address_details').addClass('hidden');
    jQuery(self).addClass('hidden').next().removeClass('hidden');

    jQuery.placeholder.shim();
}

function saveOtherAddress(self) {
    var container = jQuery(self).parents('.search-address');

    //if any address field is still not valid, don't perform anything
    if(container.find('.validation-failed').length) return;
    var done = setOtherAddressText(self);

    if(done) {
        jQuery(self).parent().addClass('hidden');
        jQuery(self).parent().prev().removeClass('hidden');
    }
}

function setOtherAddressText(self) {
    var container = jQuery(self).parents('.search-address');
    var streetField = container.find('.otherAddress input[id*="street"]');
    var housenoField = container.find('.otherAddress input[id*="houseno"]');
    var additionField = container.find('.otherAddress input[id*="addition"]');
    var postcodeField = container.find('.otherAddress input[id*="postcode"]');
    var cityField = container.find('.otherAddress input[id*="city"]');
    if(container.data('address-flag') == 'customer[company_address]') {
        housenoField = container.find('.otherAddress input[id*="company_house_nr"]');
        additionField = container.find('.otherAddress input[id*="company_house_nr_addition"]');
    }
    var street = streetField.val();
    var houseno = housenoField.val();
    var addition = additionField.val();
    var postcode = postcodeField.val();
    var city = cityField.val();
    var emptyField = false;

    //Update search fields
    container.find('.search-address-form input[id*="postcode"]').val(postcodeField.val());
    container.find('.search-address-form input[id*="houseno"]').val(housenoField.val());
    container.find('.search-address-form input[id*="addition"]').val(additionField.val());

    streetField.next('.ajax-validation').remove();
    housenoField.next('.ajax-validation').remove();
    postcodeField.next('.ajax-validation').remove();
    cityField.next('.ajax-validation').remove();

    Validation.reset(streetField.attr('name'));
    Validation.reset(housenoField.attr('name'));
    Validation.reset(postcodeField.attr('name'));
    Validation.reset(cityField.attr('name'));

    if(jQuery.trim(street) == '') {
        Validation.test('required-entry',$(streetField.attr('name')),false);
        emptyField = true;
    }

    if(jQuery.trim(houseno) != '') {
        housenoField.addClass('validate-number validate-length maximum-length-5');
        if(Validation.test('validate-number',$(housenoField.attr('name')),false)) {
            Validation.reset(housenoField.attr('name'));
        } else {
            emptyField = true;
        }
        if (! emptyField && Validation.test('validate-length',$(housenoField.attr('name')),false)) {
            Validation.reset(housenoField.attr('name'));
        } else {
            emptyField = true;
        }
    } else {
        Validation.test('required-entry',$(housenoField.attr('name')),false);
        emptyField = true;
    }

    if(jQuery.trim(postcode) != '') {
        postcodeField.addClass('nl-postcode');
        if(Validation.test('nl-postcode',$(postcodeField.attr('name')),false)) {
            Validation.reset(postcodeField.attr('name'));
        } else {
            emptyField = true;
        }
    } else {
        Validation.test('required-entry',$(postcodeField.attr('name')),false);
        emptyField = true;
    }

    if(jQuery.trim(city) == '') {
        Validation.test('required-entry',$(cityField.attr('name')),false);
        emptyField = true;
    }

    if(!emptyField) {
        var html = street + ' ' + houseno + ' ' + addition +  '<br />' + postcode + ', ' + city;
        container.find('.delivery_address_details').html(html).removeClass('hidden');
        if(container.data('address-flag') == 'address[address]') {
            prefillBillingAddress({'street': street, 'houseno': houseno, 'addition': addition, 'postcode': postcode, 'city': city});
        }
        return true;
    } else {
        return false;
    }
}

function editOtherAddress(self) {
    if(typeof checkout != 'undefined' && checkout.orderEdit && jQuery('.order-lock button').data('state') == 'locked') {
        checkout.orderLockModal(true);
        return false;
    }
    var container = jQuery(self).parents('.search-address');
    showOtherAddress(self);

    var streetField = container.find('.otherAddress input[id*="street"]');
    var housenoField = container.find('.otherAddress input[id*="houseno"]');
    var postcodeField = container.find('.otherAddress input[id*="postcode"]');
    var cityField = container.find('.otherAddress input[id*="city"]');

    streetField.parent().find('.ajax-validation').remove();

    housenoField.addClass('required-entry');
    streetField.addClass('required-entry');
    postcodeField.addClass('required-entry nl-postcode');
    cityField.addClass('required-entry');
}

function searchOtherAddress(self) {
    var container = jQuery(self).parents('.search-address');
    container.find('.otherAddressEmpty').remove();
    container.find('.otherAddressFound').addClass('hidden');

    var postcode = container.find('.search-address-form input[id*="postcode"]').val().replace(/ /g,"");
    var houseno = container.find('.search-address-form input[id*="houseno"]').val().replace(/ /g,"");
    var addition = container.find('.search-address-form input[id*="addition"]').val().replace(/ /g,"");

    if(jQuery.trim(postcode) != '' && jQuery.trim(houseno) != '') {
        container.find('.otherAddressLoading').removeClass('hidden');
        jQuery.post(
            '/checkout/index/searchAddress',
        {postcode: jQuery.trim(postcode), houseno:jQuery.trim(houseno), addition:jQuery.trim(addition)},
            function(result) {
                if (result.error) {
                    container.find('.otherAddressLoading').addClass('hidden').end()
                        .find('.search-address-form').after('<p class="otherAddressEmpty validation-advice">{0}</p>'.format(Translator.translate(result.message)));
                    container.find('.search-address-form')
                        .find('input[id*="postcode"], input[id*="houseno"], input[id*="addition"]').addClass('validation-failed');
                } else {
                    var adr = '<li><a class="search-result" onclick="setOtherAddressHtml(this)" data-street="{0}" data-houseno="{1}" data-addition="{2}" data-postcode="{3}" data-city="{4}">{0} {1} {2}, {3} {4}</a></li>'
                        .format(result.fields.street, result.fields.houseno, addition, result.fields.postcode, result.fields.city);

                    container.find('.search-address-form')
                        .find('input[id*="postcode"], input[id*="houseno"], input[id*="addition"]').removeClass('validation-failed');

                    container.find('.otherAddressLoading').addClass('hidden');
                    container.find('.otherAddressFound').empty().append(adr).removeClass('hidden');
                    container.find('.otherAddressEmpty').remove();

                    var firstFoundAddress = container.find('.otherAddressFound .search-result').eq(0).get(0);
                    setOtherAddressHtml(firstFoundAddress);
                    triggerCheckoutEvent('checkout.postcodeready', '{0} {1} {2}, {3} {4}'.format(result.fields.street, result.fields.houseno, addition, result.fields.postcode, result.fields.city));
                }
            }
    );
    } else {
        container.find('.otherAddressLoading').addClass('hidden');
    }
}

function setOtherAddressHtml(self) {
    self = jQuery(self);

    var container = self.parents('.search-address');
    var packageId = container.data('package-id');
    var street = self.data('street');
    var houseno = self.data('houseno');
    var postcode = self.data('postcode');
    var city = self.data('city');
    var addition = self.data('addition');
    var html = street + ' ' + houseno + ' ' + addition + '<br />' + postcode + ', ' + city;
    // Only if searching from customer billing address prefill the delivery addresses billing address
    if(container.data('address-flag') == 'address[address]') {
        prefillBillingAddress({'street': street, 'houseno': houseno, 'addition': addition, 'postcode': postcode, 'city': city});
    }
    container.find('.search-address-form input[id*="postcode"]').val(postcode).trigger('blur');
    container.find('.search-address-form input[id*="houseno"]').val(houseno).trigger('blur');

    if(container.data('address-flag') == 'customer[company_address]') {
        container.find('.otherAddress input[id*="house_nr"]').val(houseno).trigger('blur');
    } else {
        container.find('.otherAddress input[id*="houseno"]').val(houseno).trigger('blur');
    }

    container.find('.otherAddress input[id*="street"]').val(street).trigger('blur');
    container.find('.otherAddress input[id*="addition"]').val(addition).trigger('blur');
    container.find('.otherAddress input[id*="postcode"]').val(postcode).trigger('blur');
    container.find('.otherAddress input[id*="city"]').val(city).trigger('blur');
    container.find('.delivery_address_details').html(html).parent().removeClass('hidden');
    container.find('.otherAddressFound').addClass('hidden');
}

function convertToUpperCase(element) {
    element = jQuery(element);
    var val = element.val();
    jQuery.ajax({
        type: "POST",
        url: "/checkout/index/convertFirstname",
        data: {name : val},
        async: false
    })
        .done(function (response) {
            if(response.error != 'true') {
                element.val(response.name);
            } else {
                showModalError(response.message);
            }
        });
}

jQuery(document).ready(function($){
    $('body').on('change', '.search-address-form input', function() {
        searchOtherAddress(this);
    });
});
