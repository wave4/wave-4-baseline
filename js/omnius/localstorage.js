/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

var LZString={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",_f:String.fromCharCode,compressToBase64:function(e){if(e==null)return"";var t="";var n,r,i,s,o,u,a;var f=0;e=LZString.compress(e);while(f<e.length*2){if(f%2==0){n=e.charCodeAt(f/2)>>8;r=e.charCodeAt(f/2)&255;if(f/2+1<e.length)i=e.charCodeAt(f/2+1)>>8;else i=NaN}else{n=e.charCodeAt((f-1)/2)&255;if((f+1)/2<e.length){r=e.charCodeAt((f+1)/2)>>8;i=e.charCodeAt((f+1)/2)&255}else r=i=NaN}f+=3;s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+LZString._keyStr.charAt(s)+LZString._keyStr.charAt(o)+LZString._keyStr.charAt(u)+LZString._keyStr.charAt(a)}return t},decompressFromBase64:function(e){if(e==null)return"";var t="",n=0,r,i,s,o,u,a,f,l,c=0,h=LZString._f;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(c<e.length){u=LZString._keyStr.indexOf(e.charAt(c++));a=LZString._keyStr.indexOf(e.charAt(c++));f=LZString._keyStr.indexOf(e.charAt(c++));l=LZString._keyStr.indexOf(e.charAt(c++));i=u<<2|a>>4;s=(a&15)<<4|f>>2;o=(f&3)<<6|l;if(n%2==0){r=i<<8;if(f!=64){t+=h(r|s)}if(l!=64){r=o<<8}}else{t=t+h(r|i);if(f!=64){r=s<<8}if(l!=64){t+=h(r|o)}}n+=3}return LZString.decompress(t)},compressToUTF16:function(e){if(e==null)return"";var t="",n,r,i,s=0,o=LZString._f;e=LZString.compress(e);for(n=0;n<e.length;n++){r=e.charCodeAt(n);switch(s++){case 0:t+=o((r>>1)+32);i=(r&1)<<14;break;case 1:t+=o(i+(r>>2)+32);i=(r&3)<<13;break;case 2:t+=o(i+(r>>3)+32);i=(r&7)<<12;break;case 3:t+=o(i+(r>>4)+32);i=(r&15)<<11;break;case 4:t+=o(i+(r>>5)+32);i=(r&31)<<10;break;case 5:t+=o(i+(r>>6)+32);i=(r&63)<<9;break;case 6:t+=o(i+(r>>7)+32);i=(r&127)<<8;break;case 7:t+=o(i+(r>>8)+32);i=(r&255)<<7;break;case 8:t+=o(i+(r>>9)+32);i=(r&511)<<6;break;case 9:t+=o(i+(r>>10)+32);i=(r&1023)<<5;break;case 10:t+=o(i+(r>>11)+32);i=(r&2047)<<4;break;case 11:t+=o(i+(r>>12)+32);i=(r&4095)<<3;break;case 12:t+=o(i+(r>>13)+32);i=(r&8191)<<2;break;case 13:t+=o(i+(r>>14)+32);i=(r&16383)<<1;break;case 14:t+=o(i+(r>>15)+32,(r&32767)+32);s=0;break}}return t+o(i+32)},decompressFromUTF16:function(e){if(e==null)return"";var t="",n,r,i=0,s=0,o=LZString._f;while(s<e.length){r=e.charCodeAt(s)-32;switch(i++){case 0:n=r<<1;break;case 1:t+=o(n|r>>14);n=(r&16383)<<2;break;case 2:t+=o(n|r>>13);n=(r&8191)<<3;break;case 3:t+=o(n|r>>12);n=(r&4095)<<4;break;case 4:t+=o(n|r>>11);n=(r&2047)<<5;break;case 5:t+=o(n|r>>10);n=(r&1023)<<6;break;case 6:t+=o(n|r>>9);n=(r&511)<<7;break;case 7:t+=o(n|r>>8);n=(r&255)<<8;break;case 8:t+=o(n|r>>7);n=(r&127)<<9;break;case 9:t+=o(n|r>>6);n=(r&63)<<10;break;case 10:t+=o(n|r>>5);n=(r&31)<<11;break;case 11:t+=o(n|r>>4);n=(r&15)<<12;break;case 12:t+=o(n|r>>3);n=(r&7)<<13;break;case 13:t+=o(n|r>>2);n=(r&3)<<14;break;case 14:t+=o(n|r>>1);n=(r&1)<<15;break;case 15:t+=o(n|r);i=0;break}s++}return LZString.decompress(t)},compress:function(e){if(e==null)return"";var t,n,r={},i={},s="",o="",u="",a=2,f=3,l=2,c="",h=0,p=0,d,v=LZString._f;for(d=0;d<e.length;d+=1){s=e.charAt(d);if(!Object.prototype.hasOwnProperty.call(r,s)){r[s]=f++;i[s]=true}o=u+s;if(Object.prototype.hasOwnProperty.call(r,o)){u=o}else{if(Object.prototype.hasOwnProperty.call(i,u)){if(u.charCodeAt(0)<256){for(t=0;t<l;t++){h=h<<1;if(p==15){p=0;c+=v(h);h=0}else{p++}}n=u.charCodeAt(0);for(t=0;t<8;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}else{n=1;for(t=0;t<l;t++){h=h<<1|n;if(p==15){p=0;c+=v(h);h=0}else{p++}n=0}n=u.charCodeAt(0);for(t=0;t<16;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}a--;if(a==0){a=Math.pow(2,l);l++}delete i[u]}else{n=r[u];for(t=0;t<l;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}a--;if(a==0){a=Math.pow(2,l);l++}r[o]=f++;u=String(s)}}if(u!==""){if(Object.prototype.hasOwnProperty.call(i,u)){if(u.charCodeAt(0)<256){for(t=0;t<l;t++){h=h<<1;if(p==15){p=0;c+=v(h);h=0}else{p++}}n=u.charCodeAt(0);for(t=0;t<8;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}else{n=1;for(t=0;t<l;t++){h=h<<1|n;if(p==15){p=0;c+=v(h);h=0}else{p++}n=0}n=u.charCodeAt(0);for(t=0;t<16;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}a--;if(a==0){a=Math.pow(2,l);l++}delete i[u]}else{n=r[u];for(t=0;t<l;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}}a--;if(a==0){a=Math.pow(2,l);l++}}n=2;for(t=0;t<l;t++){h=h<<1|n&1;if(p==15){p=0;c+=v(h);h=0}else{p++}n=n>>1}while(true){h=h<<1;if(p==15){c+=v(h);break}else p++}return c},decompress:function(e){if(e==null)return"";if(e=="")return null;var t=[],n,r=4,i=4,s=3,o="",u="",a,f,l,c,h,p,d,v=LZString._f,m={string:e,val:e.charCodeAt(0),position:32768,index:1};for(a=0;a<3;a+=1){t[a]=a}l=0;h=Math.pow(2,2);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}switch(n=l){case 0:l=0;h=Math.pow(2,8);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}d=v(l);break;case 1:l=0;h=Math.pow(2,16);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}d=v(l);break;case 2:return""}t[3]=d;f=u=d;while(true){if(m.index>m.string.length){return""}l=0;h=Math.pow(2,s);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}switch(d=l){case 0:l=0;h=Math.pow(2,8);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}t[i++]=v(l);d=i-1;r--;break;case 1:l=0;h=Math.pow(2,16);p=1;while(p!=h){c=m.val&m.position;m.position>>=1;if(m.position==0){m.position=32768;m.val=m.string.charCodeAt(m.index++)}l|=(c>0?1:0)*p;p<<=1}t[i++]=v(l);d=i-1;r--;break;case 2:return u}if(r==0){r=Math.pow(2,s);s++}if(t[d]){o=t[d]}else{if(d===i){o=f+f.charAt(0)}else{return null}}u+=o;t[i++]=f+o.charAt(0);r--;f=o;if(r==0){r=Math.pow(2,s);s++}}}};if(typeof module!=="undefined"&&module!=null){module.exports=LZString}
function _detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    if (trident > 0) {
        // IE 11 (or newer) => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;
}
/* Copyright (c) 2010-2013 Marcus Westin */
;(function(win){
    var store = {},
        doc = win.document,
        localStorageName = 'localStorage',
        scriptTag = 'script',
        version = function() {
            return win['storage_key_prefix'] ? win['storage_key_prefix'] : '1.1';
        },
        cleanLimit = 4000,//when size reaches limit, oldest records are deleted untill current size is equal or smaller than this limit
        cleanAmount = 1000,//size that will be cleaned (removed)
        storage;

    store.version = version;
    store.disabled = false;
    store.set = function(key, value) {};
    store.get = function(key) {};
    store.remove = function(key) {};
    store.clear = function() {};
    store.transact = function(key, defaultVal, transactionFn) {
        var val = store.get(key);
        if (transactionFn == null) {
            transactionFn = defaultVal;
            defaultVal = null;
        }
        if (typeof val == 'undefined') { val = defaultVal || {} }
        transactionFn(val);
        store.set(key, val);
    };
    store.getAll = function() {};
    store.forEach = function() {};
    store.cleanup = function() {};
    store.stats = function() {};

    store.serialize = function(value) {
        return LZString.compressToUTF16(JSON.stringify(value));
    };
    store.deserialize = function(value) {
        if (typeof value != 'string') { return undefined }
        try { 
            return JSON.parse(LZString.decompressFromUTF16(value));
        }
        catch(e) { return value || undefined }
    };

    // Functions to encapsulate questionable FireFox 3.6.13 behavior
    // when about.config::dom.storage.enabled === false
    // See https://github.com/marcuswestin/store.js/issues#issue/13
    function isLocalStorageNameSupported() {
        try { return (localStorageName in win && win[localStorageName]) }
        catch(err) { return false }
    }

    function prepareKey(key) {
        return  '___' + version() + '___' + key;
    }

    function originalKey(key) {
        return key.replace('___' + version() + '___', '');
    }

    if (isLocalStorageNameSupported()) {
        storage = win[localStorageName];
        store.set = function(key, val, exp) {
            if (val === undefined) { return store.remove(prepareKey(key)) }
            exp = exp || 604800; //a week
            exp = (exp * 1000); //convert to microseconds
            if (store.stats(true) > cleanLimit) {
                store.cleanup();
            }
            try {
                storage.setItem(prepareKey(key), store.serialize({ val:val, exp:exp, time:new Date().getTime() }));
            } catch (e) {
                store.cleanup();
                storage.setItem(prepareKey(key), store.serialize({ val:val, exp:exp, time:new Date().getTime() }));
            }
            return val;
        };
        store.get = function(key) {
            var info = store.deserialize(storage.getItem(prepareKey(key)));
            if (!info) { return null }
            if (new Date().getTime() - info.time > info.exp) {
                return store.remove(key);
            }
            return info.val
        };
        store.remove = function(key) { storage.removeItem(prepareKey(key)) };
        store.clear = function() { storage.clear() };
        store.getAll = function() {
            var ret = {};
            store.forEach(function(key, val) {
                ret[originalKey(key)] = val
            });
            return ret
        };
        store.forEach = function(callback) {
            for (var i=0; i<storage.length; i++) {
                var key = originalKey(storage.key(i));
                callback(key, store.get(key))
            }
        };
        store.cleanup = function() {
            var startTime = new Date().getTime();
            var r = new RegExp('^___' + version() + '___');
            var data = [], item;
            var currentSize = 0;
            var deleted = 0;
            for (var i=0; i<storage.length; i++) {
                var key = storage.key(i);
                item = store.deserialize(storage.getItem(key));
                if (r.test(key) && item !== null && typeof item === 'object') {
                    if (new Date().getTime() - item.time > item.exp) { //remove if expired
                        storage.removeItem(key);
                    } else {
                        item.key = key;
                        data.push(item);
                    }
                } else {
                    storage.removeItem(key);
                }
            }
            data = data.sort(function(a, b) {
                return (a['time'] - b['time']);
            });
            currentSize = store.stats(true);
            cleanTo = currentSize - cleanAmount;
            if (cleanLimit < (currentSize - deleted)) {
                for (var i=0; i<data.length; i++) {
                    deleted += 3 + ((storage[data[i].key].length * 16) / (8 * 1024));
                    storage.removeItem(data[i].key);
                    if (cleanTo >= (currentSize - deleted)) {
                        break;
                    }
                }
            }
        };
        store.stats = function(returnSize) {
            var size = 0,
                itemSize;
            for (var i=0; i<storage.length; i++) {
                var key = storage.key(i);
                itemSize = 3 + ((storage[key].length * 16) / (8 * 1024));
                returnSize ? false : console.log( key + " = " + itemSize.toFixed(2) + " kB");
                size += itemSize;
            }
            return returnSize ? size : console.log('Total size: ' + size.toFixed(2) + " kB");
        };
    } else if (doc.documentElement.addBehavior) {
        var storageOwner,
            storageContainer;
        // Since #userData storage applies only to specific paths, we need to
        // somehow link our data to a specific path.  We choose /favicon.ico
        // as a pretty safe option, since all browsers already make a request to
        // this URL anyway and being a 404 will not hurt us here.  We wrap an
        // iframe pointing to the favicon in an ActiveXObject(htmlfile) object
        // (see: http://msdn.microsoft.com/en-us/library/aa752574(v=VS.85).aspx)
        // since the iframe access rules appear to allow direct access and
        // manipulation of the document element, even for a 404 page.  This
        // document can be used instead of the current document (which would
        // have been limited to the current path) to perform #userData storage.
        try {
            storageContainer = new ActiveXObject('htmlfile');
            storageContainer.open();
            storageContainer.write('<'+scriptTag+'>document.w=window</'+scriptTag+'><iframe src="/favicon.ico"></iframe>');
            storageContainer.close();
            storageOwner = storageContainer.w.frames[0].document;
            storage = storageOwner.createElement('div')
        } catch(e) {
            // somehow ActiveXObject instantiation failed (perhaps some special
            // security settings or otherwse), fall back to per-path storage
            storage = doc.createElement('div');
            storageOwner = doc.body
        }
        function withIEStorage(storeFunction) {
            return function() {
                var args = Array.prototype.slice.call(arguments, 0);
                args.unshift(storage);
                // See http://msdn.microsoft.com/en-us/library/ms531081(v=VS.85).aspx
                // and http://msdn.microsoft.com/en-us/library/ms531424(v=VS.85).aspx
                storageOwner.appendChild(storage);
                storage.addBehavior('#default#userData');
                storage.load(localStorageName);
                var result = storeFunction.apply(store, args);
                storageOwner.removeChild(storage);
                return result;
            }
        }

        // In IE7, keys cannot start with a digit or contain certain chars.
        // See https://github.com/marcuswestin/store.js/issues/40
        // See https://github.com/marcuswestin/store.js/issues/83
        var forbiddenCharsRegex = new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]", "g");
        function ieKeyFix(key) {
            return key.replace(/^d/, '___$&').replace(forbiddenCharsRegex, '___');
        }
        store.set = withIEStorage(function(storage, key, val) {
            key = ieKeyFix(key);
            if (val === undefined) { return store.remove(prepareKey(key)) }
            storage.setAttribute(prepareKey(key), store.serialize(val));
            storage.save(localStorageName);
            return val;
        });
        store.get = withIEStorage(function(storage, key) {
            key = ieKeyFix(key);
            return store.deserialize(storage.getAttribute(prepareKey(key)));
        });
        store.remove = withIEStorage(function(storage, key) {
            key = ieKeyFix(key);
            storage.removeAttribute(prepareKey(key));
            storage.save(localStorageName);
        });
        store.clear = withIEStorage(function(storage) {
            var attributes = storage.XMLDocument.documentElement.attributes;
            storage.load(localStorageName);
            for (var i=0, attr; attr=attributes[i]; i++) {
                storage.removeAttribute(attr.name)
            }
            storage.save(localStorageName)
        });
        store.getAll = function(storage) {
            var ret = {};
            store.forEach(function(key, val) {
                ret[originalKey(key)] = val
            });
            return ret
        };
        store.forEach = withIEStorage(function(storage, callback) {
            var attributes = storage.XMLDocument.documentElement.attributes;
            for (var i=0, attr; attr=attributes[i]; ++i) {
                callback(originalKey(attr.name), store.deserialize(storage.getAttribute(originalKey(attr.name))));
            }
        })
    }

    try {
        var testKey = '__storejs__';
        store.set(testKey, testKey);
        if (store.get(testKey) != testKey) { store.disabled = true }
        store.remove(testKey);
    } catch(e) {
        store.disabled = true;
    }
    store.enabled = !store.disabled;

    if (typeof module != 'undefined' && module.exports && this.module !== module) { module.exports = store }
    else if (typeof define === 'function' && define.amd) { define(store) }
    else { win.store = store }

})(Function('return this')());
/*
var dbName = "ssfe";

// This is the WebSQL implementation, it is somewhat slower and obsolete
var _db = openDatabase(dbName, '1.0', dbName, 50 * 1024 * 1024);
_db.transaction(function (tx) {  
   tx.executeSql('CREATE TABLE IF NOT EXISTS cache (key unique, value text)');
});
function _createCache(db, key, value) {
    db.transaction(function (tx) {  
       tx.executeSql('INSERT INTO cache (key, value) VALUES (?, ?)', [key, value]);
    });
}
function _updateCache(db, key, value) {
    db.transaction(function (tx) {  
       tx.executeSql('UPDATE cache SET  value = ? WHERE key = ?', [value, key]);
    });
}
function _getCache(db, key) {
    db.transaction(function (tx) {
        tx.executeSql('SELECT value FROM cache WHERE key = ?', [key], function (tx, results) {
            var len = results.rows.length, i;
            for (i = 0; i < len; i++){
                var res = results.rows.item(i).value; // Return first if any
                return res;
            }
        }, null);
    });
}
function _setCache(db, key, value) {
    var oldValue = _getCache(db, key);
    if (typeof oldValue === "undefined")
        _createCache(db, key, value);
    else
        _updateCache(db, key, value);
}
*/
/*
// This is an IndexedDB implementation, but we need to adopt the configurator to make use of its asynchronous nature

var localStorageDb = {};
localStorageDb.indexedDB = {};
localStorageDb.indexedDB.db = null;

localStorageDb.indexedDB.open = function() {
    var version = 1;
    var request = indexedDB.open(dbName, version);

    request.onsuccess = function(e) {
        localStorageDb.indexedDB.db = e.target.result;
    };
    request.onupgradeneeded = function(e) {
        var db = e.target.result;
        e.target.transaction.onerror = localStorageDb.indexedDB.onerror;
        if(db.objectStoreNames.contains(dbName)) {
          db.deleteObjectStore(dbName);
        }
        var store = db.createObjectStore(dbName,
          {keyPath: "key"});
    };
    request.onerror = localStorageDb.indexedDB.onerror;
};
localStorageDb.indexedDB.setValue = function(key, value) {
    var db = localStorageDb.indexedDB.db;
    if (db == null)
        return;
    var trans = db.transaction([dbName], "readwrite");
    var store = trans.objectStore(dbName);
    var request = store.put({
        "value": JSON.stringify(value),
        "key" : key
    });

    request.onerror = function(e) {
        console.log(e.value);
    };
};
localStorageDb.indexedDB.getValue = function(key) {
    var db = localStorageDb.indexedDB.db;
    if (db == null)
        return;
    var trans = db.transaction([dbName], "readwrite");
    var store = trans.objectStore(dbName);
    var request = store.get(key);
    request.onsuccess = function(event) {
        if (request.result)
            return JSON.parse(request.result.value);
        else
            return null;
    };
};
*/
/*
function store () {
    this.disabled = false;
    this.enabled = true;
    this._initDone = false;

    //this._init = function() {
    //    this._initDone = true;
        //localStorageDb.indexedDB.open();
    //}
    
    this.get = function(key) {
        //if (!this._initDone) {
        //    this._init();
        //}
        //return localStorageDb.indexedDB.getValue(key);
        return _getCache(_db, key);
    };
    this.set = function(key, value) {
        //if (!this._initDone) {
        //    this._init();
        //}
        //localStorageDb.indexedDB.setValue(key, value);
        _setCache(_db, key, value[2].responseText);
    };
    
    // These are currently not used
    this.remove = function(key) {};
    this.clear = function() {};
    this.transact = function() {};
    this.getAll = function() {};
    this.forEach = function() {};
    this.serialize = function(value) {};
    this.deserialize = function(value) {};
}
var store = new store();*/