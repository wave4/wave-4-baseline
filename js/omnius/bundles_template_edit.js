/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function ($) {
    /** Serialize form object */
    $.fn.serializeObject = function () {
        var e = this, t = {}, n = {}, r = {validate: /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/, key: /[a-zA-Z0-9_]+|(?=\[\])/g, push: /^$/, fixed: /^\d+$/, named: /^[a-zA-Z0-9_]+$/};
        this.build = function (e, t, n) {
            e[t] = n;
            return e
        };
        this.push_counter = function (e) {
            if (n[e] === undefined) {
                n[e] = 0
            }
            return n[e]++
        };
        $.each($(this).serializeArray(), function () {
            if (!r.validate.test(this.name)) {
                return
            }
            var n, i = this.name.match(r.key), s = this.value, o = this.name;
            while ((n = i.pop()) !== undefined) {
                o = o.replace(new RegExp("\\[" + n + "\\]$"), "");
                if (n.match(r.push)) {
                    s = e.build([], e.push_counter(o), s)
                } else if (n.match(r.fixed)) {
                    s = e.build([], n, s)
                } else if (n.match(r.named)) {
                    s = e.build({}, n, s)
                }
            }
            t = $.extend(true, t, s)
        });
        return t
    }
})(jQuery);

function deleteProduct(el) {
    jQuery(el).parents('.inner-fieldset').first().prev().remove();
    jQuery(el).parents('.inner-fieldset').first().remove();
}

function addProduct() {
    ++window.itemsCount;
    var template = jQuery('#product_template');
    var a = template.prev().clone();
    var b = template.clone();
    b.removeClass('hidden').attr('disabled', false);
    // change the id from template to the next available one
    b.html(b.html().replace(/_template/g, '_' + window.itemsCount));
    b.html(b.html().replace(/disabled=\"disabled\"/g, ''));
    b.attr('id', b.attr('id').replace(/_template/, '_' + window.itemsCount));
    // display the new item
    template.parent().append(a).append(b);
    a.removeClass('hidden');
    addAutoComplete(jQuery('#product_sku_' + window.itemsCount));
}

function addPackage() {
    ++window.packagesCount;
    var template = jQuery('#package_template');
    var a = template.prev().clone();
    var b = template.clone();
    b.removeClass('hidden').attr('disabled', false);
    // change the id from template to the next available one
    b.html(b.html().replace(/_template/g, '_' + window.packagesCount));
    b.html(b.html().replace(/disabled=\"disabled\"/g, ''));
    b.attr('id', b.attr('id').replace(/_template/, '_' + window.packagesCount));
    // display the new item
    template.parent().append(a).append(b);
    a.removeClass('hidden');
}

function deletePackage(el) {
    jQuery(el).parents('.inner-fieldset').first().prev().remove();
    jQuery(el).parents('.inner-fieldset').first().remove();
}

function setSku(field, val) {
    if (skusDropdown.hasOwnProperty(val)) {
        val = skusDropdown[val]['value'];
    }
    jQuery(field).val(val);
};

function addAutoComplete(element) {
    element = jQuery(element);
    element.autocomplete({source: skusAutocomplete});
    var elementSelect = element.parents('.fieldset').first().find('select').first();
    element.on("autocompleteselect", function (event, ui) {
        elementSelect.find('option[value="' + ui.item.value + '"]').prop('selected', true);
    });
    element.on("change", function () {
        var el = elementSelect.find('option[value="' + jQuery(this).val() + '"]');
        if (el.length) {
            el.prop('selected', true);
        } else {
            elementSelect.find('option[value="-1"]').prop('selected', true);
            jQuery(this).val('');
        }
    });
    elementSelect.on("change", function () {
        setSku(element, jQuery(this).val());
    });
}

function addErrorMessage(message) {
    var err = jQuery('#messages');
    var el = jQuery('<ul>', {'class': 'messages'});
    var li = jQuery('<li>', {'class': 'error-msg'});
    var span = jQuery('<span>', {text: message});
    li.append(span);
    el.append(li);
    err.append(el);
}
jQuery(document).ready(function ($) {
    // For all window.itemsCount add the autocomplete and change events
    for (var i = 1; i <= window.itemsCount; i++) {
        addAutoComplete($('#product_sku_' + i));
    }
});
