/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

jQuery(document).ready(function () {
    setInterval(function () {
        jQuery.ajax({
                type: "POST",
                url: checkIndexerUrl,
                data: {form_key: form_key_checkIndexer}
            })
            .done(function (data) {
                var grid = jQuery(data).find('.grid').find('#indexer_processes_grid_table').find('tbody').html();
                jQuery('[id="page:main-container"]').find('#indexer_processes_grid').find('.grid').find('#indexer_processes_grid_table').find('tbody').html(grid);
                jQuery('form#indexer_processes_grid_massaction-form').prepend('<input type="hidden" name="form_key" value="' + FORM_KEY + '"/>');
                indexer_processes_gridJsObject.initGridAjax();
            });
    }, 5000);
});
