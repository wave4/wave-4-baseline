/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * This work is licensed under the Creative Commons Attribution-Share Alike 3.0
 * United States License. To view a copy of this license,
 * visit http://creativecommons.org/licenses/by-sa/3.0/us/ or send a letter
 * to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA
 *
 * Configurable idle (no activity) timer and logout redirect for jQuery.
 * Works cross-browser with multiple windows and tabs within the same domain.
 *
 * Dependencies: JQuery v1.7+, JQuery UI, store.js from https://github.com/marcuswestin/store.js - v1.3.4+
 * v1.0.3
 */

(function($) {

    $.fn.idleTimeout = function(options) {
        //##############################
        //## Private Variables
        //##############################
        var opts = options;
        var idleTimer, dialogTimer, idleTimerLastActivity;
        var checkHeartbeat = 1000; // frequency to check for timeouts - 2000 = 2 seconds.

        //##############################
        //## Private Functions
        //##############################

        var openWarningDialog = function() {
            var dialogContent = "<div id='idletimer_warning_dialog'><p>" + opts.dialogText + "</p></div>";

            var warningDialog = $(dialogContent).dialog({
                buttons: {
                    "ok": function() {
                        destroyWarningDialog();
                        stopDialogTimer();
                        startIdleTimer();
                    },
                    "Log Out": function() {
                        logoutUser();
                    }
                },
                closeOnEscape: false,
                modal: true,
                title: opts.dialogTitle
            });

            // hide the dialog's upper right corner "x" close button
            $('.ui-dialog-titlebar-close').css('display', 'none');
        };

        var isDialogOpen = function() {
            var dialogOpen = $('#idletimer_warning_dialog').dialog('isOpen');

            if (dialogOpen === true) {
                return true;
            } else {
                return false;
            }
        };

        var destroyWarningDialog = function() {
            $(".ui-dialog-content").dialog('destroy').remove();
        };

        var checkIdleTimeout = function() {
            var timeNow = $.now();
            var timeIdleTimeout = (store.get('idleTimerLastActivity') + opts.idleTimeLimit);

            if (timeNow > timeIdleTimeout) {
                if (isDialogOpen() !== true) {
                    openWarningDialog();
                    startDialogTimer();
                }
            } else if (store.get('idleTimerLoggedOut') === true) { //a 'manual' user logout?
                logoutUser();
            } else {
                if (isDialogOpen() === true) {
                    destroyWarningDialog();
                    stopDialogTimer();
                }
            }
        };

        var startIdleTimer = function() {
            stopIdleTimer();
            idleTimerLastActivity = $.now();
            store.set('idleTimerLastActivity', idleTimerLastActivity);
            idleTimer = setInterval(checkIdleTimeout, checkHeartbeat);
        };

        var stopIdleTimer = function() {
            clearInterval(idleTimer);
        };

        var checkDialogTimeout = function() {
            var timeNow = $.now();
            var timeDialogTimeout = (store.get('idleTimerLastActivity') + opts.idleTimeLimit + opts.dialogDisplayLimit);

            if ((timeNow > timeDialogTimeout) || (store.get('idleTimerLoggedOut') === true)) {
                logoutUser();
            }
        };

        var startDialogTimer = function() {
            dialogTimer = setInterval(checkDialogTimeout, checkHeartbeat);
        };

        var stopDialogTimer = function() {
            clearInterval(dialogTimer);
        };

        var logoutUser = function() {
            store.set('idleTimerLoggedOut', true);

//            destroyWarningDialog();
            stopDialogTimer();
            stopIdleTimer();

            if (opts.customCallback) {
                opts.customCallback();
            }

            if (opts.redirectUrl) {
                window.location.href = opts.redirectUrl;
            }
        };

        var activityDetector = function() {

            $('body').on(opts.activityEvents, function() {

                if (isDialogOpen() !== true) {
                    startIdleTimer();
                }
            });
        };

        //###############################
        // Build & Return the instance of the item as a plugin
        // This is your construct.
        //###############################
        return this.each(function() {

            if (store.enabled) {
                idleTimerLastActivity = $.now();
                store.set('idleTimerLastActivity', idleTimerLastActivity);
                store.set('idleTimerLoggedOut', false);
            } else {
                alert('Dependent file missing. Please see: https://github.com/marcuswestin/store.js');
            }

            activityDetector();

            startIdleTimer();
        });
    }
})(jQuery);
