/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function ($) {
    window.Polling = function () {
        this.initialize.apply(this, arguments)
    };
    window.Polling.modalId = '#polling-modal'; // Default modal id for all polling actions

    window.Polling.start = function (section, pollingId, pollingCount) {
        if ($.isArray(pollingId) === true) {
            pollingId = pollingId.pop();
        }

        var self = new Polling(section, pollingCount);
        if (!$(self.modalId).hasClass('in')) {
            window.canShowSpinner = false;
            $(self.modalId).appendTo('body').modal();
        }

        self.updatePolling(pollingId);
    };

    window.Polling.retry = function (obj, pollingCount) {
        var section = $(obj).parents('.modal:first').data('section');
        var self = new Polling(section, pollingCount);
        checkout.showVodError(false);
        self.retryPolling();
    };

    window.Polling.show = function () {
        window.canShowSpinner = false;
        var modal = $(this.modalId);
        modal.find('#progress-bar').removeClass('hidden');
        modal.find('#retry-button').addClass('hidden');
        modal.find('#progress-bar .modal-message').html(Translator.translate('Sending message...'));
        modal.find('#progress-bar .progress-bar').width('20%');

        return modal.appendTo('body').modal();
    };

    window.Polling.hide = function () {
        $(this.modalId).removeData('section').modal('hide');
    };

    window.Polling.prototype = {

        initialize: function (section, pollingCount) {
            if (pollingCount === undefined) {
                pollingCount = 200;
            }

            this.pollingInProgress = false;
            this.modalId = Polling.modalId;
            this.section = section;
            this.performPollingCount = pollingCount;
            this.intervalId = null;
            this.modalContainer = $(this.modalId).data('section', section);
        },

        doPolling: function (id) {
            var self = this;

            if (self.jobProcessed !== true) {
                self.modalContainer.find('#progress-bar .modal-message').html(Translator.translate('Waiting for reply...'));
                self.modalContainer.find('#progress-bar .progress-bar').width('40%');
            }
            --self.performPollingCount;
            if (self.intervalID) {
                window.canShowSpinner = false;

                var agent_id = $('#agent_id');
                var dealer_id = $('#dealer_id');
                var is_order_edit_mode = $('#is_order_edit_mode');
                var current_website_code = $('#current_website_code');
                var axi_store_id = $('#axi_store_id');
                var dealer_group_ids = $('#dealer_code');

                if (self.jobProcessed !== true) {
                    $.post(MAIN_URL + 'job.php', {
                        'id': undefined === id ? "undefined" : id,
                        'is_order_edit_mode': is_order_edit_mode.length ? is_order_edit_mode.val() : null,
                        'agent_id': agent_id.length ? agent_id.val() : null,
                        'dealer_id': dealer_id.length ? dealer_id.val() : null,
                        'website_code': current_website_code.length ? current_website_code.val() : null,
                        'axi_store_id': axi_store_id.length ? axi_store_id.val() : null,
                        'dealer_group_ids': dealer_group_ids.length ? dealer_group_ids.val() : null
                    }, function (response) {
                        if (response.error == true) {
                            self.pollingFailed(response.message);
                        } else if (response.error == false && (undefined !== response.state) && (response.state != -1) && (response.state != -100)) {
                            self.jobProcessed = true;
                            self.modalContainer.find('#progress-bar .modal-message').html(Translator.translate('Processing answer...'));
                            self.modalContainer.find('#progress-bar .progress-bar').width('60%');
                        } else {
                            console.log('error occurred / no response received');
                        }
                    });
                } else {
                    if(!self.pollingInProgress){
                    self.startJobAfterPolling(id, self.jobResponse);
                    }
                }

                window.canShowSpinner = true;
            }

            if (self.performPollingCount == 0) {
                self.pollingFailed(Translator.translate('Retry limit exceeded'));
            }
        },

        startJobAfterPolling: function (id) {
            var self = this;
            self.pollingInProgress = true;
            $.post(MAIN_URL + 'checkout/index/' + self.section + 'Polling', {'id': id}, function (response) {
                self.pollingInProgress = false;
                if(response.hasOwnProperty('serviceError')){
                    self.pollingFailed(response.message);
                } else if (response.error == true) {
                    self.modalContainer.find('#retry-button').removeClass('hidden');
                    self.modalContainer.find('#progress-bar').addClass('hidden');
                    self.modalContainer.find('#error-message').html(response.message);
                } else if (response.error == false) {
                    if (response.hasOwnProperty('performed') && response.performed == true) {
                        self[self.section + 'PollingSuccess'](response);
                        if (self.section == 'saveSuperOrder') {
                            $('#contract-continue-button').parent().removeClass('hidden');
                            $('#saveContract .checkbox-inline').removeClass('hidden');
                        }
                    }
                } else {
                    console.log('error occurred / no response received');
                }
            });
        },

        saveContractPollingSuccess: function (response) {
            var self = this;

            var nextStep = checkout.stepsOrder[checkout.stepsOrder.indexOf(self.section) + 1];

            processPreviousSteps(nextStep);

            $.each(checkout.stepsOrder, function (id, el) {
                if (el != nextStep) {
                    $('#cart-left').find("[name=" + el + "]").parent().addClass('disabled');
                    $('.bs-docs-section').find("[action=" + el + "]").addClass('hidden');
                    if (el == self.section) {
                        $('#cart-left').find("[name=" + el + "]").parent().addClass('hidden');
                    }
                } else {
                    return false;
                }
            });
            if (undefined != nextStep) {
                $('#credit-check-menu').parent().find('[name=' + nextStep + ']').parent().addClass('active').removeClass('hidden disabled');
                $('#'.nextStep).removeClass('hidden');
            }

            self.successCallback();

            self.enablePollingButton();
            scrollToSection($('#' + self.section));
        },

        saveOverviewPollingSuccess: function (response) {
            var self = this;

            $.post(MAIN_URL + 'checkout/index/saveOverviewAfter', {'polling_id': self.modalContainer.find('#polling-id').val()}, function (response) {
                if (response.error == true) {
                    self.pollingFailed(response.message);
                } else {
                    if (undefined != window.creditCheckData) {
                        doCreditCheck(window.creditCheckData.addresses, window.creditCheckData.order_no, window.creditCheckData.creditStub);
                    } else {
                        // Deactivate previous steps and go to next available step
                        $('.bs-docs-section').addClass('hidden');
                        $('#cart-left li').addClass('disabled');
                        $('#cart-left li a.pre-order').attr('href', '');

                        scrollToSection($('#' + self.section));
                    }

                    self.enablePollingButton();
                    disableAll();
                }
            });

            self.successCallback();
        },

        redoCreditCheckPollingSuccess: function (response) {
            var self = this;

            $('#redo-credit-check').addClass('hidden');
            $("#order-edit").addClass('hidden');

            var superorderNo = self.modalContainer.find('#polling-superorder-no').val();

            restartCreditCheck(superorderNo);

            self.successCallback();
            self.enablePollingButton();
        },

        saveSuperOrderPollingSuccess: function (response) {
            var self = this;

            var contractContent = response.contract_page;
            $('.bs-docs-section').addClass('hidden');
            $('#cart-left li').addClass('disabled');
            $('#cart-left li a.pre-order').attr('href', '');

            // disable order lock button
            checkout.toggleLockState($('.order-lock button'), 'unlocked', true);

            var nextStep = checkout.stepsOrder[checkout.stepsOrder.indexOf('saveSuperOrder') + 1];

            processPreviousSteps(nextStep);

            var contract_content = $('#contract_page').find('#contract-content');
            if ($('#contract_page') && contract_content && contractContent) {
                contract_content.replaceWith($(contractContent).find('#contract-content'));
            }

            self.successCallback();
            self.enablePollingButton();

            disableLeavePageDialog();
            scrollToSection($('#saveSuperOrder'));
        },

        registerReturnPollingSuccess: function (response) {
            var self = this;

            $.post(MAIN_URL + 'checkout/index/saveOverviewAfter', {'polling_id': self.modalContainer.find('#polling-id').val(), 'refund': true}, function (response) {
                if (response.error == true) {
                    self.pollingFailed(response.message);
                    return;
                }
            });

            self.saveSuperOrderPollingSuccess(response);
        },

        enablePollingButton: function () {
            var self = this;

            self.modalContainer.modal('hide');
        },

        cancelOrderPollingSuccess: function () {
            var self = this;

            self.successCallback();

            $(location).attr('href', MAIN_URL);
        },

        successCallback: function () {
            var self = this;

            self.modalContainer.find('#progress-bar .modal-message').html('');
            self.modalContainer.find('#progress-bar .progress-bar').width('100%');

            clearInterval(self.intervalID);
            self.intervalID = false;
        },

        pollingFailed: function (message) {
            var self = this;
            if (self.section != 'saveOverview') {
                checkout.showVodError(true, message, 1, 1);
            }
            clearInterval(self.intervalID);
            self.intervalID = false;
        },

        retryPolling: function () {
            var self = this;

            window.canShowSpinner = false;
            $.post(MAIN_URL + 'checkout/index/retryPolling', {'polling_id': self.modalContainer.find('#polling-id').val()}, function (response) {
                window.canShowSpinner = true;
                if (response.error == true) {
                    self.modalContainer.find('#retry-button').removeClass('hidden');
                    self.modalContainer.find('#progress-bar').addClass('hidden');
                    self.modalContainer.find('#error-message').html(response.message);
                } else if (response.error == false) {
                    self.updatePolling(response.pollingId);
                } else {
                    console.log('error occurred / no response received');
                }
            });
        },

        updatePolling: function (pollingId) {
            var self = this;

            self.updateModal(pollingId);

            self.doPolling(pollingId);
            self.intervalID = setInterval(function () {
                self.doPolling(pollingId);
            }, 5000);
        },

        updateModal: function (pollingId) {
            var self = this;

            if (pollingId !== undefined) {
                self.modalContainer.find('#polling-id').val(pollingId);
            }
            self.modalContainer.find('#progress-bar').removeClass('hidden');
            self.modalContainer.find('#retry-button').addClass('hidden');
            self.modalContainer.find('#progress-bar .modal-message').html(Translator.translate('Sending message...'));
            self.modalContainer.find('#progress-bar .progress-bar').width('20%');
        }
    };
})(jQuery);
