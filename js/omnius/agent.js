/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function($){
    window.Agent = function() { this.initialize.apply(this, arguments) };
    window.Agent.prototype = $.extend(VEngine.prototype, {

        /**
         * Login as another agent
         * @param username
         */
        loginOtherAgent: function(username) {
            // If agent tries to login another agent while on Checkout delivery show confirmation modal
            if (checkoutDeliverPageDialog === true) {
                jQuery('#checkout_deliver_confirmation').data('function','agent.loginOtherAgent').data('param1', username).appendTo('body').modal();
                return false;
            }
            var self = this;
            var callback = function (data, status) {
                if(data.error) {
                    $('#agent-login-error').html(data.message);
                } else {
                    setPageLoadingState(true);
                    //$('.login-user-agent').hide();
                    //$('#agent-username').html(data.username);
                    //$('#agent-dealer').html(data.dealer);
                    //$('.logout-user').show().removeClass('hide');
                    //$('#agent-login-error').html('');
                    //$('#agent-load-tab').hide();
                    //$('#agent-load-tab-2').hide();
                    //$('#impersonated_agent_details_top').html(data.username + ' - ' + data.dealer);
                    //$('#agent_details_top').addClass('grey');
                    //$('button.change-password').addClass('hidden');
                    window.location.reload(true);
                }
            };
            this.post('agent.loginOther', {username: username}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
        },

        /**
         * Logout from other agent
         */
        logoutOtherAgent: function() {
            // If agent tries to logout while on Checkout delivery show confirmation modal
            if (checkoutDeliverPageDialog === true) {
                jQuery('#checkout_deliver_confirmation').data('function','agent.logoutOtherAgent').appendTo('body').modal();
                return false;
            }

            var self = this;
            var callback = function(data, status) {
                if(data.error) {
                    $('#agent-logout-error').html(data.message);
                } else {
                    setPageLoadingState(true);
                    //$('.login-user-agent').show().removeClass('hide');
                    //$(".logout-user").hide().addClass('hide');
                    //$('#agent-logout-error').html('');
                    //$('#agent-load-tab').show();
                    //$('#agent-load-tab-2').show();
                    //$('#impersonated_agent_details_top').html('');
                    //$('#agent_details_top').removeClass('grey');
                    //$('button.change-password').removeClass('hidden');
                    window.location.reload(true);
                }
            };
            this.post('agent.logoutOther', {}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
        },

        /**
         * Logout
         */
        logout: function() {
            // If agent tries to logout while on Checkout delivery show confirmation modal
            if (checkoutDeliverPageDialog === true) {
                jQuery('#checkout_deliver_confirmation').data('function','agent.logout').appendTo('body').modal();
                return false;
            }

            var callback = function(data, status) {
                window.location.reload(true);
            };
            this.post('agent.logout', {}, callback, function(){alert('Failed to logout current agent');});
        },

        /**
         * Change password
         * @param currPass
         * @param newPass
         * @param newPassAgain
         */
        changePassword: function(currPass, newPass, newPassAgain) {
            var data = {"curr-password" : currPass, "password" : newPass, "password-again" : newPassAgain };
            var self = this;
            var callback = function(data, status) {
                if(data.error) {
                    $('#agent-change-password-error').html(Translator.translate(data.message));
                } else {
                    $('#agent-change-password-error').html(Translator.translate(data.message));
                }
            };
            this.post('agent.changePass', data, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
        },

        /**
         * Retrieve agent list
         * Pass useCache as true to cache the response
         * as this is a heavy request
         * @param useCache
         */
        getAgents: function(useCache) {
            var self = this;
            var input = $('#agent-username-input');
            var callback = function(data, status) {
                if(data.error) {
                    $('#agent-login-error').html(data.message);
                } else {
                    input.autocomplete({
                        lookup: data.agents
                    }).val('');
                    input.data('agent-list', data);
                }
            };

            if (useCache && input.data('agent-list')) {
                callback(input.data('agent-list'));
            } else {
                this.post('agent.list', {}, callback, function(){$('#agent-login-error').html(self.settings.errorMessage);});
            }
        }

    });

})(jQuery);
