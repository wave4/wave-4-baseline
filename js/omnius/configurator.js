/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

(function($) {
    var root = window;
    root.Configurator = {};

    Base = Configurator.Base = function(options) {
        this.options = $.extend(this.options, options || {});
        this.initialize.apply(this, this.options);
    };

    var automatically_refresh_selection = false;
    $.extend(Base.prototype, {
        initialized: false,
        ltIE9: false,
        options: {
            endpoints: {
                'templates': 'configurator/template/all',
                'products': 'configurator/mobile/all'
            }
            , packageId: null
            , container: '#config-wrapper'
            , sections: []
            , type: 'mobile'
            , spinner: 'skin/frontend/omnius/default/images/spinner.gif'
            , spinnerId: '#spinner'
            , cache: false
            , customerPanel: '#customer_details_panel'
            , enlargeLeft: '.enlarge.right-direction'
            , elementsToShowOnClose: 20
            , include_btw: true
            , hardwareOnlySwap : false
            , hiddenSections : ['subscription', 'addon']
            , isHollandsNieuwe : false
        },

        allowedSaleType: 'retentie',
        allowedDefaultSim: 'acquisitie',
        fromSimFooter: false,
        cart: {},
        mandatory: [],
        waiting: false,
        lastSection: null,
        filterStack: {},
        filterValues: {},
        permanentFilterStack: {},
        permanentFilterValues: {},
        packageTotals: {'maf':0, 'price':0},
        ajaxQueueSize: 0,
        shelve : {},
        lastPrices : null,
        lastUpdatedPrices : null,

        $$: function(selector, forceRefresh) {
            forceRefresh = forceRefresh || false;
            
            if (!forceRefresh && this.shelve.hasOwnProperty(selector)) {
                return this.shelve[selector];
            }    
            var el = $(selector);
            this.shelve[selector] = el;
            return el;
        },

        log: function(method) {
            // return;
            console.log('configurator.{0}'.format(method));
        },
        
        debug: function() {
            console.info(Date.now());
            console.debug(arguments); 
        },
        
        _collectTemplates: function(globalTemplates) {
            var self = this;
            
            /** Gather the section templates */
            var templates = {};
            $.each(self.options.sections, function(key, val) {
                templates[val] = globalTemplates[self.options.type][val] || '';
            });
            this.setTemplates(templates);

            /** Gather the partials */
            $.each(self.options.sections, function(key, val) {
                var partialsTemp = globalTemplates['partials'][self.options.type][val];
                $.each(partialsTemp, function(partialId, html) {
                    Handlebars.registerPartial(val + '_' + partialId, Handlebars.compile(html, {noEscape: true}));
                });
            });

            $.each(globalTemplates['partials'][self.options.type]['general'], function(partialId, html) {
                Handlebars.registerPartial('general_' + partialId, Handlebars.compile(html, {noEscape: true}));
            });
        },
        
        initialize: function(options) {
            this.ltIE9 = this.$$('#isLTIE9').val() == '1';
            this.websiteId = this.$$('#current_website_id').val();
            this.$$('#config-wrapper').show(); /* hide the wrapper; */
            var self = this;
            this.cart = {};
            this.options = $.extend(this.options, options);
            this.packageId = this.options.packageId;
            this.saleType = this.options.sale_type;
            this.cache = this.options.cache || false;
            this.http.cache = new Cache(this, this.options.type);
            this.http.endpoints = $.extend(this.http.endpoints, this.options.endpoints);
            this.http.spinnerId = this.options.spinnerId;
            this.isHollandsNieuwe = this.options.isHollandsNieuwe;
            this.permanentFilterStack = root.permanentFilters;
            //assure that sections are array (indirect bug)
            var getType = {};
            if (self.options.sections && getType.toString.call(self.options.sections) === '[object Object]') {
                var sections = [];
                for (var i in self.options.sections) {
                    if (self.options.sections.hasOwnProperty(i)) {
                        sections.push(self.options.sections[i]);
                    }
                }
                self.options.sections = sections;
            }

            /** Reset filters */
            this.filterStack = {};
            this.filterValues = {};

            /* Close left side bar if expanded */
            if($(this.options.enlargeLeft).hasClass('expanded')) {
                $(this.options.enlargeLeft).trigger('click');
            }

            /*  Close customer panel if it is opened */
            if($(this.options.customerPanel).is(':visible')) {
                $(this.options.customerPanel + ' .close:first').trigger('click');
            }

            /** Temporary prevent all clicks in document */
            $(document).click(function(e) {
                e.stopPropagation();
            });
            
            window.addResizeEvent(function() {
                self.assureElementsHeight();
            });

            this.initSpinner();

            this._collectTemplates(window.templates);

            this.setFilters(root.filters, function() {
                self.setProducts(root.products, root.families, function() {
                    self.startConfigurator(function() {
                        self.prepareCart(root.initCart, function() {
                            $(root)
                                .unbind('resize orientationchange')
                                .bind('resize orientationchange', function() {
                                    var el = $('{0}'.format(self.options.container));
                                    var header = self.$$('#header');
                                    if (el.length) {
                                        var height = $(window).outerHeight() - 2;
                                        if (header.length) {
                                            height -= header.outerHeight();
                                        }
                                        el.css({
                                            /*'overflow-y': 'auto'*/
                                            /*'height': height*/
                                        });
                                    }
                                })
                                .trigger('resize');
                            var packageBlock = $('[data-package-id="{0}"]'.format(self.packageId));
                            if (packageBlock.length) {
                                packageBlock.addClass('active');

                                var scrollTo = packageBlock.offset().top - packageBlock.parent().offset().top + packageBlock.parent().scrollTop();
                                if (scrollTo != 0) {
                                    packageBlock.parent().animate({
                                        scrollTop: scrollTo
                                    }, 1000);
                                }
                            }
                            if (self.options.hardwareOnlySwap) {
                                $.each(self.options.hiddenSections, function(index, elem) {
                                    $('.conf-block[data-type="{0}"]'.format(elem)).hide();
                                });
                            }

                            /**
                             * Open first selection when configurator starts
                             */
                            var firstSelectionBlock = $(self.options.container).find('.conf-block').first();
                            if (firstSelectionBlock.length) {
                                self.toggleSection(jQuery(self.options.container).find('.conf-block:visible').first().data('type'));
                            }

                            self.initialized = true;
                        });
                    });
                });
            });

            //update prices will be executed by the toggleSection function
            //self.updatePrices(root.prices);

            /** Release click lock on document */
            $(document).unbind('click');
            if (typeof self.simType == 'undefined' && (self.saleType == self.allowedSaleType)) {
                $('.radio.sim-old input[type="radio"]').prop('checked', true);
            }
            if (typeof self.simType != 'undefined') {
                var simCardName = '.sim-new ul li a[data-type="'+ self.simType + '"]';
                $(simCardName).addClass('selected-sim');
            }

            self.reapplyRules();
        },

        getPrices: function () {
            var self = this;
            var cartProducts = self.getPackageProductIds();
            var theType = self.saleType == 'retentie' ? 1 : 0;

            var data = {
                'website_id': self.websiteId,
                'type': theType
            };
            if (cartProducts.length > 0) {
                data.products = cartProducts.join(',');
            }

            this.http.ajax('GET', 'cart.getPrices', data, true).done(function (data) {
                var prices = data['prices'];
                root['prices'] = prices;

                if (prices) {
                    var milliseconds = new Date().getTime();
                    self.lastPrices = milliseconds;
                    self.updatePrices(prices);
                }
            });
        },

        reapplyRules: function () {
            var self = this;
            // get ordered products ids
            var cartProducts = self.getPackageProductIds();
            var rulesCallback = function (data) {
                self.allowedProducts = self.getPackageProductIds().length ? data['rules'] : root.allowedProducts;
                if ($('#is_order_edit_mode').val() == 1) {
                    self.mandatory = [];
                } else {
                    self.mandatory = self.getPackageProductIds().length ? data['mandatory'] : [];
                }

                self.applyMandatory();
                $.each(self.products, function (section, products) {
                    self.filterProducts(products, section);
                    $('.conf-block[data-type="{0}"]'.format(section)).find('.search-on-type').trigger('keyup');
                });
            };
            var postData = {
                'websiteId': self.websiteId,
                'type': self.options.type
            };
            if (cartProducts.length > 0) {
                postData.products = cartProducts.join(',');
            }
            this.http.get('cart.getRules', postData, rulesCallback);
        },

        applyMandatory : function() {
            var self = this;
            $('.conf-block .family .family-status .optional').removeClass('hidden');
            $('.conf-block .family .family-status .mandatory').addClass('hidden');
            if (self.mandatory != undefined) {
                $.each(self.mandatory, function (index, value) {
                    var status = $('.conf-block#addon-block .family[data-family-id="' + value + '"] .family-status');
                    status.find('.optional').addClass('hidden');
                    status.find('.mandatory').removeClass('hidden');
                });
            }
        },

        destroy: function() {
            $(this.options.container).find('.content')[0].innerHTML = '';
            $('#homepage-wrapper').show(); /* show favorites and deals; */
            $('#config-wrapper').hide(); /* hide the wrapper;*/
        },

        initSpinner: function() {
            if ( ! $(this.options.spinnerId).length) {
                this.$$('body').before('<div id="' + this.options.spinnerId.replace(/\.|#/g, '') + 
                    '" class="spinner"><p><img src="#"/></p></div>'.replace(/#/g, this.options.spinner));
            }
        },

        prepareCart: function(data, callback) {
            var self = this;
            $.each(data, function(type, ids) {
                self.cart[type] = ids.unique();
            });

            if (root.sim) {
                this.sim = root.sim;
            }

            if(self.cart['simcard']) {
                self.selectedSim = true;
                self.cart['sim'] = self.cart['simcard'];
            }

            if (self.cart['sim']) {
                if ((typeof self.sim[self.cart['sim'][0]]) != 'undefined'){
                    var sim = self.sim[self.cart['sim'][0]];
                    var simType = sim['name'];
                    if(simType) {
                        this.simType = simType;
                        self.addSimOption(simType);
                    }
                }
            }

            $.each(self.cart, function(section, products) {
                var block = $('.conf-block[data-type="{0}"]'.format(section));
                if (block.length) {
                    $.each(products, function(key, id) {
                        var row = block.find('[data-id="{0}"]'.format(id));
                        if (row.length) {
                            self.selectProduct(row, false);
                        }
                    });
                }
                self.addSelections(section, false);
            });

            $.each(self.products, function(section, products) {
                self.filterProducts(products, section);
            });

            /* apply updated prices, if we have any */
            if (root['prices']) {
                self.updatePrices(root['prices']);
            }

            /* update all section */
            $.each(this.options.sections, function(i, section) {
                self.toggleSection(section, true);
            });

            if (root.getType(callback) == 'function') {
                callback();
            }
        },

        updateDevicePrice: function (item, id, price, section) {
            var self = this;

            var map = function (items, id) {
                var length = items.length;
                for (var j = 0; j < length; j++) {
                    if (("" + items[j]['entity_id']) == ("" + id)) {
                        return items[j];
                    }
                }
                return {};
            };

            if ((!price['price'] || !price['price_with_tax'])
                && price['price'] != 0
                && price['price_with_tax'] != 0
                && section != 'subscription'
                && section != 'addon'
            ) {
                var product = map(self.products[section], id);
                price = {
                    "price": product['price'],
                    "price_with_tax": product['price_with_tax']
                };
            }

            var $_priceForDevice = item.find('.price-for-device:first');
            if (price && (undefined != price['price'] || undefined != price['price_with_tax'])) {
                // Make sure the price is not empty when showing
                if (
                    $_priceForDevice.length
                    && price['price'] != null && price['price'] >= 0
                ) {
                    $_priceForDevice.find('.normal-price:first').text(Handlebars.helpers.money(price['price'], {hash: {}}));
                    $_priceForDevice.find('.tax-price:first').text(Handlebars.helpers.money(price['price_with_tax'], {hash: {}}));
                    $_priceForDevice[0].style.display = 'block';
                } else if (section != 'addon') {
                    item.find('.normal-price:first').text(Handlebars.helpers.money(price['price'], {hash: {}}));
                    item.find('.tax-price:first').text(Handlebars.helpers.money(price['price_with_tax'], {hash: {}}));
                } else {
                    $_priceForDevice[0].style.display = 'none';
                }
            } else if ($_priceForDevice && $_priceForDevice[0]) {
                // If price is empty remove the label
                $_priceForDevice[0].style.display = 'none';
            }

            return price;
        },

        updatePrices: function(updatedPrices) {
            if (!this.initialized) return;
            var self = this;

            if (self.lastPrices && self.lastUpdatedPrices == self.lastPrices) {
                return;
            }

            self.lastUpdatedPrices = self.lastPrices;
            $('#config-wrapper .price-for-device').hide();
            var keys = Object.keys(updatedPrices);

            var itemSections = $('#config-wrapper .conf-block');

            itemSections.each(function() {
                var section = $(this).data('type');
                $(this).find('.item-row').each(function(key, item) {
                    item = $(item);
                    var dataId;
                    if (dataId = item[0].attributes['data-id'] ) { //&& item[0].style.display != 'none'
                        var id = "" + dataId.value;

                        var topIncTax = item.find('.incl-vat-tax.price .top:first');
                        var topExlTax = item.find('.excl-vat-tax.price .top:first');

                        if (-1 !== keys.indexOf(id)) {

                            var price = self.updateDevicePrice(item, id, updatedPrices[id], section);
                            if (price) {
                                /**
                                 * Hide and show "starting from" price if it is bigger or equal to the current price
                                 * @type {Number}
                                 */
                                var startingFromIncTax = parseInt(topIncTax.text().replace(/[^0123456789]/g, ''));
                                var startingFromExcTax = parseInt(topExlTax.text().replace(/[^0123456789]/g, ''));
                                var priceIncTax = parseInt(parseFloat(price['price_with_tax']).toFixed(2).replace(/[^0123456789]/g, ''));
                                var priceExcTax = parseInt(parseFloat(price['price']).toFixed(2).replace(/[^0123456789]/g, ''));

                                if (topIncTax.length) {
                                    if (priceIncTax < startingFromIncTax) {
                                        topIncTax[0].style.visibility = 'visible';
                                    } else {
                                        topIncTax[0].style.visibility = 'hidden';
                                    }
                                }

                                if (topExlTax.length) {
                                    if (priceExcTax < startingFromExcTax) {
                                        topExlTax[0].style.visibility = 'visible';
                                    } else {
                                        topExlTax[0].style.visibility = 'hidden';
                                    }
                                }
                            }
                        } else if (section == 'accessoire') {
                            if (topIncTax.length) {
                                topIncTax[0].style.visibility = 'hidden';
                            }
                            if (topExlTax.length) {
                                topExlTax[0].style.visibility = 'hidden';
                            }
                        }
                    }
                });
            });
        },

        updateBtwState : function(state) {
            for (var category in this.products) {
                if (this.products.hasOwnProperty(category)) {
                    var len = this.products[category].length;
                    for (var i = 0; i < len; ++i) {
                        this.products[category][i]['include_btw'] = state;
                    }
                }
            }
        },
        
        setPackageId: function(packageId) {
            this.packageId = packageId;
        },

        getPackageProductIds: function () {
            var cartProducts = [];
            var self = this;
            // get all products from cart on the current package
            for (var confSection in self.cart) {
                if (self.cart.hasOwnProperty(confSection)) {
                    var len = self.cart[confSection].length;
                    for (var i = 0; i < len; ++i) {
                        cartProducts.push(self.cart[confSection][i]);
                    }
                }
            }

            cartProducts.sort();
            return cartProducts;
        },

        increaseQueueSize: function() {
            ++this.ajaxQueueSize;
            $('#cart-spinner').removeClass('hide');
            $('.col-right.sidebar').find('> button.enlarge').hide();
            $('.cart_packages')
                .css('overflow', 'hidden')
                .scrollTop(0);
        },

        decreaseQueueSize: function() {
            --this.ajaxQueueSize;
            if (this.ajaxQueueSize <= 0) {
                this.ajaxQueueSize = 0;
                $('#cart-spinner').addClass('hide');
                $('.col-right.sidebar > button.enlarge').show();
                var cartPackages = $('.cart_packages');

                var activePackageBlock = cartPackages.find('[data-package-id={0}]'.format(this.packageId));
                var scrollTo = activePackageBlock.offset().top - cartPackages.offset().top + cartPackages.scrollTop();
                cartPackages.scrollTop(scrollTo);
                cartPackages.css('overflow', 'auto');
            }
        },

        addMultiToCart: function(products, section, callback, async) {
            startPiwikCall(MAIN_URL + "configurator/cart/addMulti");
            var self = this;

            // show spinner in cart separate
            window.setPageLoadingState && window.setPageLoadingState(true);
            window.manualLoader = true;
            // increase size of Queue on click
            self.increaseQueueSize(); 
           
            // disable proceed button during spinner
            $('#cart_totals input[type="button"]').attr('disabled', true);

            // get ordered products ids
            var cartProducts = self.getPackageProductIds();
            var theType = self.saleType == 'retentie' ? 1 : 0;

            var dfdData2 = {
                'website_id': self.websiteId,
                'type'      : theType
            };
            if (cartProducts.length > 0) {
                dfdData2.products = cartProducts.join(',');
            }

            dfd2 = this.http.ajax('GET', 'cart.getPrices', dfdData2, true);

            // get rules and prices
            var request;
            if (cartProducts.length) {

                var dfdData1 = {
                    'websiteId': self.websiteId,
                    'type': self.options.type,
                    'products': cartProducts.join(',')
                };

                dfd1 = this.http.ajax('GET', 'cart.getRules', dfdData1, true);

                request = $.when(dfd1,dfd2);
            } else {
                // if no products the initial show combinations
                var dfd = $.Deferred();
                request = $.when(dfd,dfd2);
                dfd.resolve([{ rules: self.getAllProductsIds() }]);
            }
            request.done(function(data1, data2) {
                if (data1[0]) {
                    data1 = data1[0];
                }
                if (data2[0]) {
                    data2 = data2[0];
                }

                self.allowedProducts = data1['rules'];
                if ($('#is_order_edit_mode').val() == 1) {
                    self.mandatory = [];
                } else {
                    self.mandatory = data1['mandatory'];
                }

                self.applyMandatory();
                var prices = data2['prices'];
                root['prices'] = prices;

                /* show only available */
                $.each(self.products, function(section, products) {
                    self.filterProducts(products, section);
                    $('.conf-block[data-type="{0}"]'.format(section)).find('.search-on-type').trigger('keyup');
                });

                if (prices) {
                    self.updatePrices(prices);
                }

                // hide page loader
                window.setPageLoadingState && window.setPageLoadingState(false);
                window.manualLoader = false;
                // call UI automation trigger
                triggerConfiguratorEvent('configurator.addmulti', cartProducts.join(','));
            }).then(function () {
                // addMulti call
                self.http.ajaxQueue('POST', 'cart.addMulti', {
                    'products': products,
                    'type': self.options.type,
                    'section': section,
                    'simType': self.simType,
                    'oldSim': self.options.old_sim,
                    'packageId': self.packageId
                }, true)
                    .done(function (data) {
                        callback(data);

                        if (typeof self.simType == 'undefined' && (self.saleType == self.allowedSaleType)) {
                            self.options.old_sim = true;
                            $('.sim-select .sim-old input[type="radio"]').prop('checked', true);
                        }

                        // Check if there are any mandatory families that only have one item, and click it
                        var theFamilies = $('#addon-block').find('.selection-block .product-table .family');
                        $.each(theFamilies, function (i, fam) {
                            if ($(fam).find('.family-header .family-status .hidden').hasClass('optional')) {
                                var familyItems = $(fam).find('.family-content .item-row');
                                var itemToTrigger = [];
                                $.each(familyItems, function (index, item) {
                                    if ($(item).css('display') == 'block') {
                                        itemToTrigger.push($(item));
                                    }
                                });

                                if (itemToTrigger.length == 1) {
                                    var triggerItem = itemToTrigger.pop();
                                    if (!triggerItem.hasClass('selected')) {
                                        automatically_refresh_selection = 'addon';
                                        triggerItem.trigger('click');
                                    }
                                }
                            }
                        });

                        // Update left side sections if they are complete or not
                        var sections;
                        if (data.hasOwnProperty('incomplete_sections')) {
                            sections = data['incomplete_sections'];
                        } else {
                            sections = [];
                        }
                        self.showWarnings(sections);
                    }).then(function () {
                        self.reapplyRules();
                        self.getPrices();
                        self.decreaseQueueSize();
                        endPiwikCall(MAIN_URL + "configurator/cart/addMulti");
                    });

                $.each($('.conf-block'), function (index, value) {
                    var block = $(value);
                    if (block.data('expanded') == false) {
                        self._closeBox(block);
                    }
                });
            });
        },

        showWarnings: function (sections) {
            // Remove highlight from all sections
            var oldHighlighted = $('#config-wrapper div.incomplete');
            oldHighlighted.removeClass('incomplete');
            // Clear all the warnings for the sections
            $('#config-wrapper span.incomplete').addClass('hidden');

            $.each(sections, function (section) {
                $('#' + section + '-block span.incomplete').removeClass('hidden');
                // If the old sections are still incomplete, highlight again
                oldHighlighted.each(function (id, el) {
                    el = $(el);
                    if (el.attr('id') == (section + '-block')) {
                        el.addClass('incomplete');
                    }
                })
            });
        },

        removeFromCart: function(id, callback, async) {
            this.http.post('cart.delete', {'product': id, 'type': this.options.type, 'packageId': this.packageId}, callback, null, async);
        },

        getProductType: function(productId) {
            var item = $('.selection-block [data-id="{0}"]'.format(productId));
            if ( ! item.length) {
                return false;
            }
            return item.parents('.conf-block').data('type');
        },

        setTemplates: function(templates) {
            this.templates = templates;
        },
        
        getProductPopupTemplate: function() {
            return window.templates['partials'][this.options.type]['general']['product_popup'];    
        },

        setProducts: function(products, families, callback) {
            this.allowedProducts = root.allowedProducts || [];
            var self = this;
            this.products = products;
            this.families = families;

            if (root.getType(callback) == 'function') {
                callback();
            }
        },
        
        getAllProductsIds: function() {
            var ids = [];
            for (var category in this.products) {
                if (this.products.hasOwnProperty(category)) {
                    var len = this.products[category].length;
                    for (var i = 0; i < len; ++i) {
                        ids.push(this.products[category][i]['entity_id']);
                    }
                }
            }
            return ids;
        },

        setFilters: function(filters, callback) {
            this.filters = filters;
            if (root.getType(callback) == 'function') {
                callback();
            }
        },

        startConfigurator: function(callback) {
            var container = $(this.options.container).find('.content');
            var self = this;
            var el, child;
            var fragment = '';
            
            // update btw state
            self.updateBtwState(self.options.include_btw);

            if (Handlebars.partials['general_header']) {
                fragment += Handlebars.partials['general_header']({})
            }

            for (var i = 0; i < self.options.sections.length; i++) {
                if (self.templates[self.options.sections[i]]) {
                    var productsTemplate = root.getType(Handlebars.partials['{0}_selected_item'.format(self.options.sections[i])]) === 'function' ? '{0}_selected_item'.format(self.options.sections[i]) : 'general_selected_item';
                    var sectionProducts = self.products[self.options.sections[i]] ? self.products[self.options.sections[i]] : {};
                    if (root.getType(Handlebars.partials['{0}_family'.format(self.options.sections[i])]) === 'function') {
                        var familyTemplate = '{0}_family'.format(self.options.sections[i]);
                        if (self.families.length > 0 && sectionProducts.length > 0) {
                            var k = sectionProducts.length;
                            while (k--) {
                                for (var j = 0; j < self.families.length; j++) {
                                    if ($.inArray(self.families[j]['entity_id'], sectionProducts[k]['families']) != -1) {
                                        if (typeof self.families[j]['products'] == "undefined") {
                                            self.families[j]['products'] = [];
                                        }
                                        self.families[j].products.push(sectionProducts[k]);
                                        sectionProducts.splice(k, 1);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    // Revert the products so the order is not lost
                    var len;
                    for (j = 0, len = self.families.length; j < len; j++) {
                        if (typeof self.families[j]['products'] != "undefined") {
                            self.families[j]['products'].reverse();
                        }
                    }
                    var data = {
                        index: i,
                        productType: this.options.sections[i],
                        familyTemplate: familyTemplate ? familyTemplate : {},
                        families: self.families.length > 0 ? self.families : {},
                        productsTemplate: productsTemplate,
                        products: sectionProducts,
                        filters: self.filters[this.options.sections[i]] ? self.filters[self.options.sections[i]]: {},
                        closed: i ? true : false
                    };
                    fragment += Handlebars.compile(self.templates[self.options.sections[i]], {noEscape: true})(data);
                }
            }

            if (window.hasOwnProperty('families')) {
                $.each(window.families, function (index, valueFam) {
                    if (valueFam.hasOwnProperty('products')) {
                        $.each(valueFam.products, function (index, value) {
                            products.addon.push(value);
                        });
                    }
                });
            }

            if (Handlebars.partials['general_footer']) {
                fragment += Handlebars.partials['general_footer']({});
            }
            
            el = $('<div>' + fragment + '</div>');
            fragment = document.createDocumentFragment();
            while (child = el[0].firstChild) {
                fragment.appendChild(child);
            }

            var configuratorHtml = $(fragment.children || fragment.childNodes);
            var searchFields = configuratorHtml.find('[data-search-on-type]');
            var searchFieldsCount = searchFields.length;
            for (var j = 0; j < searchFieldsCount; j++) {
                var section = searchFields[j].attributes['name'].value.split('-')[1];
                if (section && self.products[section]) {
                    searchFields[j].attributes['data-documents'].value = JSON.stringify(self.products[section]);
                }
            }

            var wrapper = $('#homepage-wrapper'); 
            if (wrapper.is(':visible')) {
                wrapper.hide(); /* hide favorites and deals; */
            }

            //append content
            container.html(configuratorHtml);
            container.find('.conf-block').on('box.opened', function() {
                var el = $(this);
                setTimeout(function() {el.find('.item-row').slice(self.options.elementsToShowOnClose).removeClass('hide'); }, 400);
            }).on('box.closed', function() {
                $(this).find('.item-row').slice(self.options.elementsToShowOnClose).addClass('hide');
            });

            if (root.getType(callback) == 'function') {
                callback();
            }
        },

        assureElementsHeight: function() {
            var self = this;

            var checkExist = setInterval(function() {
                if ($('.calculateHeightLeftOver').length) {
                    /* get open section elements */
                    var openedSection = $('.selection-block.box-opened .product-table').first();
                    var openSectionHeight = openedSection.parent().height();

                    /* get element at the bottom of the list */
                    var leftOverHeightContainer = $('#config-wrapper').find('.calculateHeightLeftOver');

                    /* get offset and height */
                    var calculateHeightLeftOverOffset = leftOverHeightContainer.first().offset().top;
                    var calculateHeightLeftOverHeight = leftOverHeightContainer.first().height();

                    /* get window height */
                    var windowHeight = $(window).height();

                    /* calculate parent section height */
                    var sectionHeight = windowHeight - calculateHeightLeftOverHeight - calculateHeightLeftOverOffset;

                    /* calculate inner table height -- should be the rest of the parent available height */
                    var offsetInParent = 0;
                    openedSection.prevAll().each(function(i,_el) {
                        var elem = $(_el);
                        offsetInParent += elem.is(':visible') ? elem.height() : 0;
                    });

                    /* set the height for the parent and the table */
                    openedSection.height(sectionHeight + openSectionHeight - offsetInParent);

                    if (self.ltIE9) {
                        /* <= IE9 doesn't support transition so we use the slower js animate function   */
                        openedSection.parent().animate({
                            height: sectionHeight + openSectionHeight - 5
                        }, 200);
                    } else {
                        /* leave the transition to css */
                        openedSection.parent().height(sectionHeight + openSectionHeight - 5 /* padding */);
                    }

                    /* stop the repetition */
                    clearInterval(checkExist);
                    openedSection.parent().trigger('box.opened');
               }
            }, 100); /* check every 100ms */
        },

        filter: function(element, permanent) {
            var self = this;
            var el = $(element);
            var value = el.data('selection');
            var property = el.parents('.filter').data('filter-for');
            var type = el.parents('.conf-block').data('type');
            var searchOnType = el.parents('.conf-block').find('.search-on-type');

            if (undefined === this.filterStack[type]) {
                this.filterStack[type] = {};
            }

            if (undefined === this.filterValues[type]) {
                this.filterValues[type] = [];
            }

            if (el.hasClass('active')) {
                delete this.filterStack[type][property];
                el.removeClass('active');
                delete self.filterValues[type][$.inArray(el.text(), self.filterValues[type])];
            } else {
                this.filterStack[type][property] = value;
                el.addClass('active');
                el.parent().siblings().find('a').each(function () {
                    $(this).removeClass('active');
                    delete self.filterValues[type][$.inArray($(this).text(), self.filterValues[type])];
                });
                self.filterValues[type].push(el.text());
                if (permanent) {
                    if (!self.permanentFilterStack.hasOwnProperty(type)) {
                        self.permanentFilterStack[type] = {};
                    }
                    self.permanentFilterStack[type][property] = value;

                    if (!self.permanentFilterValues.hasOwnProperty(type)) {
                        self.permanentFilterValues[type] = [];
                    }
                    self.permanentFilterValues[type].push(el.text());
                }
            }

            // Check if the Clear Filters button should be shown
            this.showClearFilters(searchOnType, type);

            self.filterValues[type] = $.map(self.filterValues[type].unique(), function (n, i) {
                return n && n.length ? n : null;
            });
            if (permanent) {
                self.permanentFilterValues[type] = $.map(self.permanentFilterValues[type].unique(), function (n, i) {
                    return n && n.length ? n : null;
                });
            }
            this.filterProducts(self.products[type], type);

            searchOnType.trigger('keyup');
        },

        updateSimsList: function (possibleSims) {
            $('.radio.sim-new a').parent().addClass('hidden');
            if (possibleSims) {
                $.each(possibleSims, function (id, el) {
                    $('.radio.sim-new a[data-type="' + el + '"]').parent().removeClass('hidden');
                });
            }
        },

        activateDisabledItems: function (section, showPrices) {
            showPrices = showPrices != undefined ? showPrices : false;
            // show spinner in cart separate
            window.setPageLoadingState && window.setPageLoadingState(true);
            window.manualLoader = true;

            var items = [];
            var self = this;
            self.temporaryAllowedProducts = [];
            // Display all the disabled items from the section
            section = $(section).parents('.conf-block');
            rulesCallback = function (result) {
                if (result.error) {
                    showModalError(result.message);
                    items = [-1];
                } else {
                    items = JSON.parse(result.ids);
                }

                section.find('.item-row').each(function (id, el) {
                    el = $(el);
                    if (items.length == 0 || jQuery.inArray("" + el.data('id'), items) != -1) {
                        if (el.css("display") == "none") {
                            el.css("display", 'block');
                            el.addClass('incompatible-row');
                            self.temporaryAllowedProducts.push("" + el.data('id'));
                            if (!showPrices) {
                                el.find('.item-pricing').addClass('hidden');
                            }
                        }
                    }
                });
                section.find('input.search-on-type').trigger('change');
            };
            var selectedItems = [];
            // Send all the selected items for cache purposes
            $('#config-wrapper .item-row.selected').each(function () {
                selectedItems.push($(this).data('id'));
            });
            selectedItems.sort(function (l, r) {
                return r < l;
            });
            self.increaseQueueSize();
            this.http.ajaxQueue('GET', 'cart.get' + section.data('type') + 'Rules', {"ids": selectedItems, "website_id": self.websiteId}, true)
                .done(function (data) {
                    rulesCallback(data);
                    self.decreaseQueueSize();
                    // hide page loader
                    window.setPageLoadingState && window.setPageLoadingState(false);
                    window.manualLoader = false;
                });
            section.find('.hide-incompatible').removeClass('hidden');
            section.find('.show-incompatible').addClass('hidden');
        },

        deactivateDisabledItems: function (section) {
            var self = this;
            self.temporaryAllowedProducts = [];
            // Display all the disabled items from the section
            section = $(section).parents('.conf-block');
            section.find('.item-row.incompatible-row').each(function (id, el) {
                el = $(el);
                if (el.css("display") == "block") {
                    el.css("display", 'none');
                    el.removeClass('incompatible-row');
                }
            });
            section.find('.hide-incompatible').addClass('hidden');
            section.find('.show-incompatible').removeClass('hidden');
            section.find('input.search-on-type').trigger('change');
        },

        clearFilters: function(element) {
            var el = $(element);
            var self = this;
            var type = el.parents('.conf-block').data('type');
            this.filterStack[type] = {};
            self.filterValues[type] = [];
            this.filterProducts(self.products[type], type);
            var searchOnType = el.parents('.conf-block').find('.search-on-type');
            searchOnType.val('');
            el.parents('.conf-block').find('.section-filters li a').removeClass('active');
            searchOnType.trigger('keyup');
            el.addClass('hidden');
        },

        filterProducts: function(productsInitial, type) {
            if (productsInitial  === undefined) {
                return null;
            }
            var searchOnType = root.getType(type) === 'object' ? $(type) : false;
            type = root.getType(type) === 'object' ? $(type).parents('.conf-block').data('type') : type;
            var products = this.filterCollection(productsInitial, type);
            var block = $('.conf-block[data-type="{0}"]'.format(type));
            var self = this;
            var families = $('#addon-block .family');
            var familiesToRemove = [];
            var displayOnlyMandatory = false;
            if (self.filterStack[type] != undefined && self.filterStack[type]['mandatory'] != undefined) {
                switch (self.filterStack[type]['mandatory']) {
                    case 0:
                        if (families.length > 0) {
                            $.each(families, function (index, value) {
                                var hidden = $(value).find('.family-header .family-status .hidden');
                                if (hidden.hasClass('mandatory')) {
                                    self.displayFamily(value);
                                } else {
                                    $(value).css('display', 'none');
                                    familiesToRemove.push("" + $(value).data('family-id'));
                                }
                            });
                        }
                        break;
                    case 3:
                        if (families.length > 0) {
                            $.each(families, function (index, valueTmp) {
                                var hidden = $(valueTmp).find('.family-header .family-status .hidden');
                                if (hidden.hasClass('mandatory')) {
                                    $(valueTmp).css('display', 'none');
                                    familiesToRemove.push("" + $(valueTmp).data('family-id'));
                                } else {
                                    self.displayFamily(valueTmp);
                                }
                            });
                        }
                        displayOnlyMandatory = true;
                        break;
                }
            } 
            var productsNew = [];

            for (var j = 0; j < products.length; j++) {
                if (familiesToRemove.length == 0 || !products[j].hasOwnProperty('families') || products[j]['families'].filter(function (n) {
                        return familiesToRemove.indexOf(n) != -1;
                    }).length == 0) {
                    if (displayOnlyMandatory && (!products[j].hasOwnProperty('families') || products[j]['families'].length == 0)) {
                        continue;
                    }
                    productsNew.push(products[j]);
                }
            }

            this.updateFilterStatus(productsNew, type);
            var productsIds = $.map(productsNew, function (n, i) {
                return n ? n['entity_id'] : null
            });

            $('.conf-block[data-type="' + type + '"] .item-row').each(function() {
                if ( ! $(this).data('disabled')) {
                    if (-1 === productsIds.indexOf($.trim($(this).data('id')))) {
                        $(this)[0].style.display = 'none';
                    } else {
                        $(this)[0].style.display = 'block';

                        if (self.temporaryAllowedProducts && self.temporaryAllowedProducts.length && (-1 !== self.temporaryAllowedProducts.indexOf($.trim("" + $(this).data('id'))))
                            && ((self.allowedProducts && self.allowedProducts.length && (-1 === self.allowedProducts.indexOf($.trim("" + $(this).data('id'))))) || (!self.allowedProducts || !self.allowedProducts.length))
                        ) {
                            $(this).addClass('incompatible-row');
                        } else {
                            $(this).removeClass('incompatible-row');
                        }
                    }
                }
            });

            // Check if the Clear Filters button should be shown
            if (searchOnType) {
                this.showClearFilters(searchOnType, type);
            }
            if (families.length > 0) {
                $.each(families, function (index, value) {
                    self.displayFamily(value);
                });
            }
            this.assureElementsHeight();
        },

        showClearFilters: function(searchOnType, type) {
            if (undefined === this.filterStack[type]) {
                this.filterStack[type] = {};
            }

            if (Object.keys(this.filterStack[type]).length == 0 && searchOnType.val().length == 0) {
                // Hide the clear filters button for this section
                searchOnType.parents('.selection-block').find('.clear-config-filters').addClass('hidden');
            } else {
                // Show the clear filters button for this section
                searchOnType.parents('.selection-block').find('.clear-config-filters').removeClass('hidden');
            }
        },

        showToonAlles: function (section) {
            var self = this;
            var sectionName = section.data('type');
            var element = section.find('.show-incompatible');
            var doShow = false;
            // Check if the section has the toon alles button
            if (element.length) {
                // Get all the hidden elements
                var hiddenElements = section.find('.product-table .item-row[style$="display: none;"]');
                // Filter hidden elements to make sure whey were not hidden by the filters
                hiddenElements = $.grep(hiddenElements, function (el) {
                    return self.allowedProducts.indexOf("" + $(el).data('id')) === -1;
                });

            }
        },

        filterCollection: function(products, type) {
            var self = this;
            var filtered = [];
            var hasFilters = false;
            var productsLength = products.length;
            for (var i = 0; i < productsLength; i++) {
                var valid = true;
                if (self.permanentFilterStack.hasOwnProperty(type)) {
                    $.extend(self.filterStack[type], self.permanentFilterStack[type]);
                }
                if (self.filterStack[type]) {
                    for (var property in self.filterStack[type]) {
                        if (property == 'mandatory') {
                            continue;
                        }
                        if (self.filterStack[type].hasOwnProperty(property)) {
                            hasFilters = true;
                            var getType = {};
                            if (products[i][property] && getType.toString.call(products[i][property]) === '[object Array]') {
                                valid = (products[i][property].indexOf("" + self.filterStack[type][property]) >= 0);
                            } else if (products[i][property] != self.filterStack[type][property]) {
                                valid = false;
                            }
                            if (!valid) {
                                break;
                            }
                        }
                    }
                }
                valid ? filtered.push(products[i]) : valid;
            }
            if ((self.allowedProducts && self.allowedProducts.length) || (self.temporaryAllowedProducts && self.temporaryAllowedProducts.length)) {
                var allowed = self.allowedProducts && self.allowedProducts.length;
                var temporary = self.temporaryAllowedProducts && self.temporaryAllowedProducts.length;
                var filteredProducts = hasFilters ? filtered : products;
                var filteredLength = filteredProducts.length;
                filtered = [];
                hasFilters = true;
                var j, skip;
                for (j = 0; j < filteredLength; j++) {
                    skip = false;
                    if (allowed) {
                        if (-1 !== self.allowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
                            filtered.push(filteredProducts[j]);
                            skip = true;
                        }
                    }
                    if (temporary && !skip) {
                        if (-1 !== self.temporaryAllowedProducts.indexOf("" + filteredProducts[j]['entity_id'])) {
                            filtered.push(filteredProducts[j]);
                        }
                    }
                }
            } else {
                // no rules available for current selection => return only the products currently in cart
                var current_prods = self.getCartItems();
                products = products.filter(function (prod) {
                    return $.inArray(
                            parseInt(prod['entity_id']),
                            current_prods
                        ) != -1
                });
            }

            return hasFilters ? filtered : products;
        },

        displayFamily: function (valueTmp) {
            var oneVisible = false;
            $.each($(valueTmp).find('.family-content .item-row'), function (index, item) {
                if (item.style.display == 'block') {
                    oneVisible = true;
                }
            });
            if (oneVisible) {
                $(valueTmp).css('display', 'block');
            } else {
                $(valueTmp).css('display', 'none');
            }
        },

        hasSelections: function() {
            var count = 0;
            $.each(this.cart, function(section, items) {
                if (items && items.length) {
                    count += items.length;
                }
            });
            return count;
        },

        getCartItems: function() {
            var products = [];
            $('.conf-block').find('.item-row.selected').each(function() {
                products.push(parseInt($.trim($(this).data('id'))));
            });
            return products.unique();
        },

        updateFilterStatus: function(products, type) {
            var block = $('.conf-block[data-type="{0}"]'.format(type));
            var statusText = block.find('.filter-status span');
            var element = block.find('.filter-input input');
            var searchString = $.trim(element.val());
            var searchFilters = this.filterValues[type] ? this.filterValues[type] : [];
            searchFilters = $.map(searchFilters.unique(), function(n, i){return n && n.length ? n : null;});
            searchFilters = $.trim(searchFilters.join(', '));
            if (searchString.length) {
                searchFilters.length ? searchFilters += ', ' + searchString : searchFilters = searchString;
            }
            if (statusText.length) {
                if (searchFilters.length) {
                    statusText.html(statusText.data('message').format(products.length, searchFilters)).show();
                } else {
                    statusText.html(statusText.data('default-message').format(products.length)).show();
                }
            }
        },

        addPackage: function(package_id, callback) {
            this.http.post('cart.addPackage', {'product': package_id}, callback, false, true);
        },

        addSimOption: function(simType, makeCall) {
            simType = $.trim(simType);
            var self = this;
            var simSelect = $(this.options.container).find('.sim-select');
            var oldSim = simSelect.find('.sim-old');
            var newSim = simSelect.find('.sim-new');
            oldSim.find('input').prop('checked', false);
            newSim.find('input').prop('checked', true);
            
            triggerConfiguratorEvent('configurator.changesim', '');
            
            var validSimOptions = [];
            jQuery.each(jQuery('.radio.sim-new li:visible a'), function (id, el) {
                validSimOptions.push($(el).data('type'));
            });

            if(!!(simType && (typeof self.simType != 'undefined') && (self.simType == simType) && makeCall)) {
                return false;
            } else if(!!(simType && ($.inArray(simType, validSimOptions) != -1))) {
                self.simType = simType;
            } else if(simType == '') {
                self.simType = null;
            }

            newSim
                .find('.list-inline a')
                .removeClass('selected-sim')
                .show();

            $('a[data-type="{0}"]'.format(simType)).addClass('selected-sim');

            if (makeCall) {
                self.selectedSim = true;
                self.fromSimFooter = true;
                self.options.old_sim = false;
                self.addSelections(simSelect.parents('.conf-block').data('type'), true);
                self.fromSimFooter = false;
            }
        },

        clearSimOption: function(makeCall) {
            var self = this;
            var simSelect = $(this.options.container).find('.sim-select');
            var newSim = simSelect.find('.sim-new');

            newSim.find('input').prop('checked', false);
            newSim
                .find('.list-inline a')
                .removeClass('selected-sim')
                .hide();

            $(self.options.container)
                .find('.sim-select')
                .find('.sim-old')
                .find('input')
                .prop('checked', true);
            self.simType = null;
            if ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe) {
                self.options.old_sim = true;
            }

            if (makeCall) {
                self.clearSim = true;
                window.simCardChecker.setUseMySim(self.packageId);
                self.addSelections(simSelect.parents('.conf-block').data('type'), true);
            }
        },

        getDefaultSimOption: function(typeId, type) {
            var result = $.map(this.products[type], function(n,i) {return n && n['entity_id'] == typeId ? n : null})[0];
            return result && result['sim_type'] ? result['sim_type'] : null;
        },

        toggleSection: function(section, onlyUpdate) {
            var self = this;
            var box = $('.conf-block[data-type="{0}"]'.format(section));
            if(!box.is(':visible')) {
                return;
            }
            // Always reset this when a section is set to active
            self.temporaryAllowedProducts = [];
            // Hide all the disabled items from the section when the section is set to active
            box.find('.item-row.incompatible-row').each(function (id, el) {
                if ($(el).css("display") == "block") {
                    $(el).css("display", 'none');
                    $(el).removeClass('incompatible-row');
                    $(el).find('.item-pricing').removeClass('hidden');
                }
            });
            box.find('.search-on-type').trigger('keyup');
            var selectedItem = box.find('.selected-item');
            var elements = $('.item-row.selected');
            var expandHandle = box.find('.expand-handle');
            var ids = [];
            elements.each(function() {
                ids.push($(this).data('id'));
            });

            var callback = self._createSectionCallback(ids);
            setTimeout(function () {
                self.showToonAlles(box);
            }, 200);
            /* update selected-items section */
            if (onlyUpdate) {
                callback();
                if (root['prices']) {
                    self.updatePrices(root['prices']);
                }
                return;
            }

            /* do nothing if section already expanded */
            if (box.data('expanded')) {
                /* callback(); */
                return;
            }

            // other box clicked
            box.trigger('box.closed');

            /* close other boxes when opening the current one */
            $('.conf-block[data-type!="{0}"]'.format(section)).each(function() {
                var tmpBox = $(this);
                if (tmpBox.data('expanded')) {
                    self._closeBox(tmpBox);
                    tmpBox.find('.selected-item').slideDown('fast', function() { callback(); });
                    tmpBox.find('.expand-handle').text(tmpBox.find('.expand-handle').data('open'));
                    tmpBox.data('expanded', false);
                } else {
                    callback();
                }
                tmpBox.find('.block-header').show();
                /* tmpBox.find('.block-footer').hide(); */
            });

            if (box.data('expanded')) {
                self._openBox(box);
                selectedItem.slideDown('fast', function() { callback(); });
                expandHandle.text(expandHandle.data('open'));
                box.data('expanded', false);
                box.find('.block-header').show();
                box.find('.block-footer').hide();
            } else {
                selectedItem.slideUp('fast', function() {
                    self._openBox(box);
                    callback();
                    self.assureElementsHeight();
                });

                expandHandle.text(expandHandle.data('close'));
                box.data('expanded', true);
                box.find('.block-header').hide();
                box.find('.block-footer').show();
            }

            /* apply updated prices, if we have any */
            if (root['prices']) {
                selectedItem.promise().done(function() {
                    self.updatePrices(root['prices']);
                });
            }
        },

        _createSectionCallback: function(ids) {
            var self = this;
            
            return function () {
                var method, confBox;

                $.each(self.options.sections, function (index, configuratorSection) {
                    confBox = $('.conf-block[data-type="{0}"]'.format(configuratorSection));

                    if (root.getType(Handlebars.partials['{0}_selected_item'.format(configuratorSection)]) === 'function') {
                        method = Handlebars.partials['{0}_selected_item'.format(configuratorSection)];
                    } else {
                        method = Handlebars.partials['general_selected_item'];
                    }

                    var products = $.map(self.products[configuratorSection], function (n, i) {
                        if (-1 !== $.inArray(parseInt(n['entity_id']), ids)) {
                            return n
                        }
                    });

                    var content = '';
                    for (var i = 0; i < products.length; i++) {
                        var item = $(method(products[i]));

                        if (dataId = item[0].attributes['data-id'] ) {
                            var id = "" + dataId.value;
                            if (root['prices'][id]) {
                                self.updateDevicePrice(item, id, root['prices'][id], configuratorSection);
                            }
                        }

                        content += item[0].outerHTML;
                    }

                    if ($.trim(content).length) {
                        confBox.find('.selected-item .block-header .left p span').first().show();
                    } else {
                        confBox.find('.selected-item .block-header .left p span').first().hide();
                    }

                    content = $.trim(content).length ?
                        $.trim(content) :
                        '<div class="no-item-message"><span>{0}</span></div>'.format(
                            confBox.find('.selected-item').data('no-item'));

                    confBox.find('.selected-item')
                        .find('.content')[0].innerHTML = content;
                });
            };
        },

        _closeBox: function ($box) {
            $box
                .find('.selection-block').removeClass('box-opened').addClass('box-closed')
                    .find('.product-table').removeAttr('style').end()
                .removeAttr('style');
            /* also reset style */
            // Hide the "hide incompatible" button each time a section is closed
            $box.find('button.hide-incompatible').addClass('hidden');
        },

        _openBox: function ($box) {
            $box.find('.selection-block').removeClass('box-closed').addClass('box-opened');
        },

        addSelections: function(section, makeCall) {
            var self = this;
            makeCall = undefined === makeCall ? true : makeCall;
            var box = $('.conf-block[data-type="{0}"]'.format(section));
            var selectedItem = box.find('.selected-item');
            var elements = box.find('.item-row.selected');
            var ids = [];
            elements.each(function() {
                ids.push($(this).data('id'));
            });

            /* Hide promo rules block */
            $('#promo-rules').removeClass('open').find('.promo-rules-container').hide();

            var selectSections = function() {
                if ( ! self.cart[section]) {
                    self.cart[section] = [];
                }
                self.cart[section] = ids.unique();
                if ( ! box.find('.selected').length) {
                    self.cart[section] = [];
                }
            };

            var failed = false;

            var callback = function(data) {
                self.assureElementsHeight();

                if (data.hasOwnProperty('error') && data.error) {
                    if (data.hasOwnProperty('reload') && data.reload) {
                        window.location.reload();
                    } else {
                        if (data.hasOwnProperty('message')) {
                            showModalError(data.message);
                        }

                        self.lastAddedItem.removeClass('selected');
                        self.cart[section].pop(self.lastAddedItem.data('id'));

                        if (self.cart[section].length == 0) {
                            var content = '<div class="no-item-message"><span>{0}</span></div>'.format(selectedItem.data('no-item'));

                            selectedItem.find('.content')[0].innerHTML = content;
                        }

                        return;
                    }
                    failed = true;
                    return;
                }
                
                if (data.hasOwnProperty('simError') && data.simError) {
                    showModalError(data.simError);
                    // remove selected sim
                    $('.conf-block[data-type="subscription"]')
                        .find('.sim-select .list-inline a.selected-sim').removeClass('selected-sim');
                } else if (data.hasOwnProperty('sim') && data.sim) {
                    self.addSimOption(data.sim);
                }

                self.lastSection = section;  /* save a reference to the section containing the last added/removed product */

                selectSections();

                self.setPackageId(data['activePackageId']);
                self.applyRules(data);

                if (configurator.ajaxQueueSize > 1 || window.checkoutEnabled === false) {
                    $('#cart_totals input').attr('disabled', 'disabled');
                }

                /* keep amount of promorules synchronized when callback is done */
                if (data.promoRules) {
                    $('.promo-rules-duplicate').parent().removeClass('hidden');
                    $('.promo-rules-duplicate').html(data.promoRules);
                }

                if (data.hasOwnProperty('isHollandsNieuwe') && data.isHollandsNieuwe) {
                    self.isHollandsNieuwe = true;
                    $('.conf-block[data-type="subscription"]').parent().find('.sim-select').find('.sim-old').show();
                } else if(self.saleType != self.allowedSaleType) {
                    $('.conf-block[data-type="subscription"]').parent().find('.sim-select').find('.sim-old').hide();
                }

                // Remove the defaulted products from the configurator visually
                if (data.hasOwnProperty('removedDefaultedItems')) {
                    $.each(data.removedDefaultedItems, function (id, el) {
                        self.cart[el.type].pop(el.product_id);
                        var item = $('#' + el.type + '-block').find(".product-table [data-id=" + el.product_id + "]");
                        item.removeClass('selected');
                        self.toggleSection(el.type, true);
                        var familyId = item.parents('.family').data('family-id');
                        var family = false;
                        window.families.each(function (el) {
                            if (el.entity_id == familyId) {
                                family = el;
                                return false;
                            }
                        });

                        if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                            if (item.parents('.family').length > 0) {
                                $.each(item.siblings(), function (index, value) {
                                    $(value).removeClass('exclusive-row');
                                });
                            }
                        }
                    });
                }

                // Add the defaulted products to the configurator visually
                if (data.hasOwnProperty('addedDefaultedItems')) {
                    $.each(data.addedDefaultedItems, function (id, el) {
                        if (!self.cart.hasOwnProperty(el.type)) {
                            self.cart[el.type] = [];
                        }
                        self.cart[el.type].push(el.product_id);
                        var item = $('#addon-block').find(".product-table [data-id=" + el.product_id + "]");
                        item.addClass('selected');
                        self.toggleSection(el.type, true);
                        var familyId = item.parents('.family').data('family-id');
                        var family = false;
                        window.families.each(function (el) {
                            if (el.entity_id == familyId) {
                                family = el;
                                return false;
                            }
                        });

                        if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                            if (item.parents('.family').length > 0) {
                                $.each(item.siblings(), function (index, value) {
                                    $(value).addClass('exclusive-row');
                                });
                            }
                        }
                    });
                }
                    
                //if automatically selected an addon, refresh the addon block
                if (automatically_refresh_selection) {
                    configurator.toggleSection(automatically_refresh_selection, true);
                    window.setPageLoadingState && window.setPageLoadingState(false);
                    automatically_refresh_selection = false;
                }
            };

            var simSelect = $('.conf-block[data-type="subscription"]').parent().find('.sim-select');
            var oldSimSelect = simSelect.find('.sim-old');
            var newSimSelect = simSelect.find('.sim-new');
            if (self.cart['subscription'] && self.cart['subscription'].length) {

                var checkIfIsSimOnly = $.map(self.products.subscription, function(i, y) {
                    if (i.entity_id == self.cart.subscription[0]) {
                        return i;
                    }
                });
            }

            if (! this.clearSim) {
                if ((self.cart['subscription'] && self.cart['subscription'].length)
                    && (
                        (checkIfIsSimOnly.length > 0 && checkIfIsSimOnly[0]['sim_only'] == true)
                        || (self.cart['device'] && self.cart['device'].length)
                    )
                ) {
                    if (simSelect.length && ((this.saleType == this.allowedSaleType)
                        || this.isHollandsNieuwe)
                    ) {
                        oldSimSelect.show();
                        if (self.options.old_sim) {
                            self.clearSimOption();
                        } else if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && ((this.saleType == this.allowedSaleType) || this.isHollandsNieuwe ) && !self.simType) {
                            // Use old sim as default for retention packages
                            self.options.old_sim = true;
                        }
                    } else {
                        newSimSelect.find('input').prop('checked', true);
                        newSimSelect
                            .find('.list-inline a')
                            .removeClass('selected-sim')
                            .show();
                        oldSimSelect.hide();
                    }

                    if ((self.cart['device']  && self.cart['device'].length) && !self.fromSimFooter){
                        var checkIfHasSim = $.map(self.products.device, function(i, y){
                            if (i.entity_id == self.cart.device[0] ){
                                return i;
                            }
                        });
                        if (checkIfHasSim.length > 0 && checkIfHasSim[0].sim_type == '' && (typeof self.selectedSim == 'undefined' || self.selectedSim == false)) {
                            this.simType = null;
                        }
                    }
                    if ((typeof self.selectedSim == 'undefined' || self.selectedSim == false) && (this.saleType == this.allowedDefaultSim) && !this.isHollandsNieuwe) {
                        if (checkIfIsSimOnly[0]['sim_only']) {
//                            var typeId = self.cart['subscription'][0];
//                            var type = 'subscription';
                        } else if (section == 'device') {
                            var typeId = self.cart['device'][0];
                            var type = 'device';
                            this.addSimOption(this.getDefaultSimOption(typeId, type));
                        } else {
                            this.addSimOption(self.simType);
                        }
                    }
                    simSelect.show();
                    simSelect.parents('.conf-block').find('.block-footer').removeClass('hidden');
                } else {
                    simSelect.hide();
                    simSelect.parents('.conf-block').find('.block-footer').addClass('hidden');
                    self.clearSimOption();
                }
            }

            if (makeCall) {
                this.addMultiToCart(ids, box.data('type'), callback, true);
            } else {
                selectSections();
            }

            if (failed) {
                self.lastSection = false;
                return;
            }
        },

        chooseNewSim: function(item) {
            this.simType = null;
            this.options.old_sim = false;
            $(item).parents('.sim-new').find('a').css('font-weight', 'normal').show();
        },

        selectProduct: function(item, makeCall) {
            makeCall = undefined !== makeCall ? makeCall : true;
            item = $(item);
            // Hide all the disabled items from the section once a product has been clicked
            item.parents('.content').find('.item-row.incompatible-row').each(function (id, el) {
                if ($(el).css("display") == "block") {
                    $(el).css("display", 'none');
                    $(el).removeClass('incompatible-row');
                    $(el).find('.item-pricing').removeClass('hidden');
                }
            });

            // If the item has class 'exclusive-row' it means we need to deselect the other product prior to selecting it, as it is part of a mutually-excluded family
            if (item.hasClass('exclusive-row')) {
                item.parent().find('.item-row.selected').removeClass('selected');
                item.removeClass('exclusive-row');
            }

            var productId = item.data('id');
            var section = this.getProductType(productId);
            var handleMethod = 'select{0}'.format(section.charAt(0).toUpperCase() + section.slice(1));

            this.lastAddedItem = item;
            // handle subscription sim only
            if (section == 'subscription' && !item.hasClass('selected')) {
                var product = this.products[section].filter(function(prod) { return prod['entity_id'] == productId }).first();
                this.updateSimsList(product.identifier_simcard_allowed);
                simCardChecker.checkPackageSubscription(product, this.selectedSim || this.options.old_sim, this.packageId);
            }
            
            if (root.getType(this[handleMethod]) === 'function') {
                this[handleMethod](item);
            } else {
                if ((makeCall && (item.hasClass('disabled') || item.hasClass('disabled-row'))) || item.parents('.selected-item').length) {
                    return;
                }
                var box = item.parents('.conf-block');

                if (item.hasClass('selected')) { /*already selected so we need to deselect it */
                    var greyOut = false;
                    if (item.parents('.family').find('.family-header .family-status .mandatory:visible').length > 0) {
                        if (item.siblings('.item-row:visible').length == 0) {
                            return;
                        }
                    }

                    item.removeClass('selected');

                    if ( ! this.cart[section]) {
                        this.cart[section] = [];
                    } else {
                        delete this.cart[section][$.inArray(productId, this.cart[section])];
                        this.cart[section] = $.map(this.cart[section].unique(), function(n, i){return n ? n : null;});
                    }
                } else { /* we need to select the item */
                    item.addClass('selected');

                    var greyOut = true;

                    if ( ! this.cart[section]) {
                        this.cart[section] = [];
                    }
                    if ( ! box.data('multi')) { /* block is of type multi */
                        item.siblings().removeClass('selected');
                        this.cart[section] = [productId];
                    } else {
                        this.cart[section].push(productId);
                        this.cart[section] = $.map(this.cart[section].unique(), function(n, i){return n ? n : null;});
                    }
                }
            }

            if (makeCall) {
                this.selectedSim = false;
                this.addSelections(section, makeCall);
                if (section == 'subscription') {
                    this.toggleSection('device', false);
                }
            }

            if (greyOut != undefined && item.parents('.family').length > 0) {
                var familyId = item.parents('.family').data('family-id');
                var family = false;
                window.families.each(function (el) {
                    if (el.entity_id == familyId) {
                        family = el;
                        return false;
                    }
                });
                if (family && (family.hasOwnProperty('allow_mutual_products') && family.allow_mutual_products == 1)) {
                    if (greyOut == true) {
                        $.each(item.siblings(), function (index, value) {
                            $(value).addClass('exclusive-row');
                        });
                    } else {
                        $.each(item.siblings(), function (index, value) {
                            $(value).removeClass('exclusive-row');
                        });
                    }
                }
            }
        },

        applyRules: function(data) {
            var self = this;
            $('.item-row').each(function() {
                $(this).removeClass('disabled');
            });
            if(data.hasOwnProperty('totalMaf')) self.packageTotals.maf = data.totalMaf;
            if(data.hasOwnProperty('totalPrice')) self.packageTotals.price = data.totalPrice;

            /* update side cart */
            var rightBlock = $('div[data-package-id].active:visible').first();
            var cartSidebar = rightBlock.find('.content').first();
            var cartTotal = rightBlock.find('.coupon-section').first();
            var totals = $('#cart_totals');
            var products = [];
            $.each(self.products, function(section, items) {
                if (self.cart[section]) {
                    $.each(items, function(key, item) {
                        for(var i=0;i<self.cart[section].length;i++) {
                            if (item['entity_id'] == self.cart[section][i]) {
                                products.push(item);
                            }
                        }
                    })
                }
            });

            /* make sure duplicate products are not added in the array */
            products = $.unique(products);
            rightBlock.replaceWith(data.rightBlock);
            if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
            if(data.hasOwnProperty('complete')) {
                /* disable button "Ga Verder" */
                window.hasCompletePackage = data.complete;
                totals.find('#configurator_checkout_btn').prop('disabled', !data.complete);
            }

            self.packagePromoRules(data, data.activePackageId);
        },

        packagePromoRules: function(data, packageId) {
            /* check for promo rules after selection */
            if(data.hasOwnProperty('promoRules') && data.promoRules != 0) {
                $('div[data-package-id="{0}"]'.format(packageId)).find('.promo-rules-trigger span').text(data.promoRules).parent().removeClass('hidden');
            } else {
                $('div[data-package-id]').find('.promo-rules-trigger').addClass('hidden');
            }
        },

        changeSelection: function(section) {
            var block = $('#{0}-block'.format(section));
            var selectedItemBlock = block.find('.selected-item');
            selectedItemBlock.slideUp('fast', function() {
                block
                    .find('.selection-block')
                    .slideDown('fast', function() {
                        selectedItemBlock.find('.content')[0].innerHTML = '';
                    });
                block.find('.item-row.selected').show();
            });
        },

        deletePackages: function() {
            this.http.post('cart.empty', {}, function() {
                window.location.reload();
            }, null, true);
        },

        http: {

            endpoints: {},

            _ajaxQueue: $({}),

            spinnerId: '#spinner',

            addEndpoint: function(key, url) {
                this.endpoints[key] = url;
            },

            removeEndpoint: function(key) {
                if (key in this.endpoints) {
                    delete this.endpoints[key];
                }
            },

            hasEndpoint: function(key) {
                return key in this.endpoints;
            },

            getEndpoint: function(key) {
                if ( ! this.hasEndpoint(key)) {
                    throw new Error('Endpoint not present');
                }
                return this.endpoints[key];
            },

            setLoading: function(state) {
                /**
                 * If window has spinner and its value is false, do nothing
                 */
                if (false === window.canShowSpinner) {
                    return;
                }
                
                var loader = $(this.spinnerId);
                loader.find('img').css({
                    marginTop: ($(window).height() / 2) - 20
                });
                $(this.spinnerId).toggle(state);
                this.waiting = state ? true : false;
            },

            get: function(url, data, callback, errback, cache, async) {
                callback && getType(callback) === 'function' || (callback = function(data) {
                    return data;
                });
                errback && getType(errback) === 'function' || (errback = function(xhr, errMes) {
                    showModalError(errMes);
                });
                var response, status;

                this
                    .ajax('GET', url, data, async)
                    .done(function(res, sts) {
                        response = res;
                        status = sts;
                    })
                    .then(function() {
                        response = callback(response, status)
                    })
                    .fail(errback);

                return response;
            },

            post: function(url, data, callback, errback, async) {
                callback && getType(callback) === 'function' || (callback = function(data) {
                    return data;
                });
                errback && getType(errback) === 'function' || (errback = function(xhr, errMes) {
                    showModalError(errMes);
                });
                var response, status;
                this
                    .ajax('POST', url, data, async)
                    .done(function(res, sts) {
                        response = res;
                        status = sts;
                    })
                    .then(function() {
                        response = callback(response, status)
                    })
                    .fail(errback);

                return response;
            },

            ajax: function (type, url, data, async) {
                var self = this;
                var endpoint = this.hasEndpoint(url) ? this.getEndpoint(url) : url;
                var response = $.Deferred();

                return $.ajax({
                    url: endpoint,
                    cache: true,
                    async: async || false,
                    data: data,
                    type: type
                }).done(function (res) {
                    response.resolve(res);
                });

                return response.promise();
            },
            
            ajaxQueue: function(type, url, data, async) {
                var jqXHR;
                var dfd = $.Deferred();
                var promise = dfd.promise();
                var self = this;

                function doRequest(next) {
                    jqXHR = self.ajax(type, url, data, async);
                    jqXHR.done(dfd.resolve)
                        .fail(dfd.reject)
                        .then(next, next);
                }

                self._ajaxQueue.queue(doRequest);

                promise.abort = function (statusText) {
                    if (jqXHR) {
                        return jqXHR.abort(statusText);
                    }

                    var queue = self._ajaxQueue.queue();
                    var index = $.inArray(doRequest, queue);

                    if (index > -1) {
                        queue.splice(index, 1);
                    }

                    dfd.rejectWith({
                        type: type,
                        url: url,
                        data: data,
                        async: async
                    }, [ promise, statusText, "" ]);

                    return promise;
                };

                return promise;
            }
        }
    });

    Cache = Configurator.Cache = function(prefix) {
        this.initialize.apply(this, prefix);
    };

    $.extend(Cache.prototype, {

        initialize: function(prefix) {
            this.prefix = prefix || 'ConfiguratorBase_';
        },

        prefix : 'ConfiguratorBase_',

        /**
         * @param key
         * @param value
         * @returns {*}
         */
            set : function(key, value) {
            if (root.getType(value) === 'object' || root.getType(value) === 'array') {
                value = JSON.stringify(value);
            }
            store.set(this.prefix + key, value);
            return value;
        },

        /**
         * @param key
         * @returns {*}
         */
        get : function(key) {
            var result = store.get(this.prefix + key);
            if (result !== null) {
                try {
                    result = JSON.parse(result);
                } catch(e) {
                    /* no-op */
                }
                return result;
            }
            return undefined;
        },

        /**
         * @param key
         * @returns {*}
         */
        pluck : function(key) {
            var result = localStorage.getItem(this.prefix + key);
            if(result !== null) {
                localStorage.removeItem(this.prefix + key);
                try {
                    result = JSON.parse(result);
                } catch(e) {
                    /* no-op */
                }
                return result;
            }
            return undefined;
        },

        /**
         * @returns {boolean}
         */
        flush : function() {
            var reg = new RegExp('^' + this.prefix);
            Object.keys(localStorage)
                .forEach(function(key){
                    if (reg.test(key)) {
                        localStorage.removeItem(key);
                    }
                });
            return true;
        }
    });

}(jQuery));

if (!String.prototype.format) {
    String.prototype.format = function() {

        var args = arguments;
        var sprintfRegex = /\{(\d+)\}/g;

        var sprintf = function (match, number) {
            return number in args ? args[number] : match;
        };

        return this.replace(sprintfRegex, sprintf);
    };
}

if (!window.getType) {
    window.getType = function(item) {
        var getType = {};
        var type = getType.toString.call(item).match(/\s(.*)\]/)[1].toLowerCase();
        if (type === 'string') {
            if (/^[\],:{}\s]*$/
                .test(item.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                    .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                    .replace(/(?:^|:|,)(?:\s*\[)+/g, '')) && item.length > 0
                ) {
                return 'json';
            } else {
                return 'string';
            }
        }
        return type;
    }
}

if (!Array.prototype.unique) {
    Array.prototype.unique= function() {
        var unique= [];
        for (var i = 0; i < this.length; i += 1) {
            if (unique.indexOf(this[i]) == -1) {
                unique.push(this[i])
            }
        }
        return unique;
    };
}

if (!Array.prototype.filter)
{
    Array.prototype.filter = function(fun /*, thisArg */)
    {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun != "function")
            throw new TypeError();

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++)
        {
            if (i in t)
            {
                var val = t[i];

                if (fun.call(thisArg, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

if (typeof window.performance === 'undefined') {
    window.performance = {};
}

if (!window.performance.now) {

    var nowOffset = Date.now();

    if (performance.timing && performance.timing.navigationStart) {
        nowOffset = performance.timing.navigationStart
    }

    window.performance.now = function now() {
        return Date.now() - nowOffset;
    }
}
jQuery(document).ready(function () {
    var catalogVersion = jQuery('#current_build').html();
    var localVersion = store.get('catalog_version');
    if (localVersion != null && jQuery('#current_build').length > 0 && localVersion != catalogVersion)
        store.clear(); // Clear cache if not latest version
    store.set('catalog_version', catalogVersion);
});
