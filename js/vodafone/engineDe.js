'use strict';

function proceedToExtendedCart() {
    jQuery('.enlarge.left-direction').trigger('click');
}

/**
 *
 */
function expandTheRightSidebar() {
    var $ = jQuery;
    var btn = $('.col-right.sidebar .enlarge');

    $.post(MAIN_URL + 'configurator/init/expandCart', {'checkout': window['onCheckout']}, function(html) {
        $('#right-panel-cart').remove();
        $('.col-right').append(html).find('#right-panel-cart').show();
        // $('.expanded-package-details').scrollspy();
        if(btn.data('activate-id')){
            $('[data-summary-id="'+btn.data('activate-id')+'"]').trigger('click');
        }
        if (!supports_calc()) {
            CSSCalc();
        }
    });
}