'use strict';

(function ($) {
    window.CustomerDe = function () {
        this.initialize.apply(this, arguments);
        this.customerData = null; // the loaded customer details data
        this.customerSearchResultsJson = null; // the entire list found on a search of a customer
        this.contractNoCheckedForRetention = {}; // in the customer details, products tab -> the accounts checked for retention in the form {contract_no -> kias}
        this.templateSourceCustomersList = $("#Handlebars-Template-Search-Customer").html();
        this.templateSourceCustomersPotentialLink = $("#Handlebars-Template-Customer-Potential-Link").html();
        this.customerInfoContainer = $("#customer-search-results");
        this.leftCustomerInfoContainer = $('.col-left').find('.sticker');
        this.customerShoppingCartData = {};
        this.customerProducts = {};
        this.templateShoppingCartViewModal = $("#Handlebars-Template-Shopping-Cart-View-Modal").html();
        this.customersToLink = [];
        this.typePrivate = null;
        this.typeSoho = null;
    };
    window.CustomerDe.prototype = $.extend(VEngine.prototype, {
        isBusiness: false,
        inCampaignMode: false,
        leaveCampaignModal: '#leaveCampaignConfirmation',
        setInCampaignMode: function (state) {
            this.inCampaignMode = state;
        },
        // Showing confirm leaving campaign modal
        showConfirmCampaignLeaveModal: function (productId) {
            var jElement = jQuery(this.leaveCampaignModal);
            // Change data-product-id attribute on confirmation button
            jElement.find('.confirm-ok').attr('data-product-id', productId);
            // Show it
            jElement.modal('show');
        },
        // Closing confirm leaving campaign modal
        closeCampaignModal: function () {
            jQuery(this.leaveCampaignModal).modal('hide');
        },
        // Used only from leave campaign confirmation modal
        selectProduct: function (element) {
            // Leave campaign mode to pass through configurator.selectProduct
            this.inCampaignMode = false;

            this.closeCampaignModal();

            // Remove campaigns spans from configurator
            jQuery("#config-wrapper").find(".item-row").find('.campaign-identifier').find('span').addClass('hidden').html("");
            var productId = jQuery(element).attr('data-product-id');
            jQuery("#config-wrapper").find(".item-row[data-id='" + productId + "']").trigger('click');
        },
        setBusinessState: function () {
            //  If customer loaded, customer type cannot be changed
            if (this.customerData) {
                return;
            }
            var toggleClass = '.soho-toggle';
            var hasProductsInCartAttr = $('#configurator_checkout_btn').attr('data-hasProductsInCart');
            if (hasProductsInCartAttr == 'no') {
                ToggleBullet.onSwitchChange(toggleClass, 'soho');
                /** customer switch has been updated */
                if (this.isBusiness = ToggleBullet.state(toggleClass)) {
                    /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
                    $('[data-id="customer-details-dob"]').hide();
                } else {
                    $('[data-id="customer-details-dob"]').show();
                }
            }
            else {
                ToggleBullet.switchPending = true;
                showCancelCartModal();
            }
        },
        searchCustomer: function (data) {
            var self = this,
                el = $(data).parents('form');
            var $_loadingGif = $(data).children('.loading-search'),
                $_searchFilters = $('.search-fields');

            $_loadingGif.removeClass('invisible');
            $_searchFilters.addClass('disabled-section');

            if (this.helper.isObject(el)) {
                data = $.map($(el).serializeArray(el), function (obj) {
                    return obj;
                });
            }

            var advancedSearchRadio = $('#advanced-search');
            if (advancedSearchRadio.val() == 0) {
                for (var i = 0; i  < data.length; i++) {
                    if (data[i].name == "email" || data[i].name == "device_id") {
                        data.splice(i,1);
                        i--;
                    }
                }
            }

            var callback = function (data, status) {
                self.customerSearchResultsJson = data;
                if (data.error) {
                    showModalError(data.message);
                }
                else if (!data.hasOwnProperty('one_result')) {
                    self.showCustomerList();
                } else {
                    if (self.customerSearchResultsJson.data.hasOwnProperty('potential_links')) {
                        self.showLinkPage(data);
                        $('#customer_link_details').find('.back-button').hide();
                    } else {
                        var customerId = self.customerSearchResultsJson.data.customer.Role.Person.ID;
                        self.loadCustomerData(customerId);
                    }
                }
                $_loadingGif.addClass('invisible');
                $_searchFilters.removeClass('disabled-section');
            };
            var customersEndpoint = 'customer.search';
            if(el.find('.legacy-search-switch .toggle-button').hasClass('on-state')) {
                customersEndpoint = 'customer.searchLegacy';
            }
            this.post(customersEndpoint, {data: data, loader: false}, callback, function (err, status, xhr) {
                /**@var {XMLHttpRequest} xhr */
                if (xhr && xhr.status == 414) {
                    err = 'Request too long.';
                }
                if (xhr && xhr.status != 0) {
                    showModalError(err);
                }
                $_loadingGif.addClass('invisible');
                $_searchFilters.removeClass('disabled-section');
            });
        },
        loadCustomerData: function (customerId) {
            var self = this;

            var callback = function (data) {
                // Preserve customer data
                self.customerData = data.customerData;
                if (data.sticker) {
                    var leftDetailsSticker = $(data.sticker),
                        leftDetailsStickerTemplae = leftDetailsSticker.html();
                    // Parse the json with customer results in the handlebars template
                    var template = Handlebars.compile(leftDetailsStickerTemplae),
                        customersHTML = template(data.customerData);
                    self.leftCustomerInfoContainer.html(customersHTML);

                    // Check customer type, set switch accordingly and disable switch
                    self.handleCustomerState();
                    
                    // Check if element is expanded and hide it for customer campaigns content to be visible
                    if ($('.enlargeSearch').hasClass('expanded')) {
                        LeftSidebar.toggleLeftSideBar(false);
                    }

                    // Load customer serviceability by service address id
                    if (data.customerData.service_address_id !== undefined && data.customerData.service_address_id) {
                        $('.address-check span.loading-search').removeClass('invisible');

                        // Build new complete customer address element used by checkService function
                        var elem = jQuery('<div/>', {
                            "data-id": data.customerData.service_address_id,
                            "data-street": data.customerData.street,
                            "data-building-nr": data.customerData.no,
                            "data-postalcode": data.customerData.postal_code,
                            "data-city-name": data.customerData.city
                        });

                        // Perform serviceAvailability call
                        address.checkService(elem);
                        // Filter available campaigns
                        phone.filterCustomerCampaigns(data.customerData);
                    } else {
                        showModalError("No response received for service address id. Serviceability not checked.");
                    }
                }
                else {
                    var errorMessage = '';
                    for (var p in data.message) {
                        errorMessage += data.message[p] + ' ';
                    }
                    showModalError(errorMessage);
                }
            };

            var options;
            options = {
                customerId: customerId
            };

            this.post('customer.load', options, callback, function (err, status, xhr) {
                if (xhr && xhr.status != 0) {
                    showModalError(err);
                }
            });
        },

        handleCustomerState: function() {
            var toggleClass = '.soho-toggle';
            
            if (this.customerData['isSoho'] || this.customerData['PartyType'] == this.typeSoho) {
                ToggleBullet.switchOn(toggleClass);
            } else {
                ToggleBullet.switchOff(toggleClass);
            }
            ToggleBullet.disable(toggleClass);
        },

        toggleCollapseLinkedCustomer: function (button, event, classPrefix) {
            event.stopPropagation();
            var target = $(button).attr('data-target');
            var targetSelected = $('.' + target);
            console.log(targetSelected);
            if (targetSelected.hasClass("out")) {
                targetSelected.removeClass("out");
                $(button).removeClass('collapse-button-' + classPrefix);
                $(button).addClass('contract-button-' + classPrefix);
            } else {
                targetSelected.addClass("out");
                $(button).addClass('collapse-button-' + classPrefix);
                $(button).removeClass('contract-button-' + classPrefix);
            }
        },
        getAuthDetails: function (customerId, event, button) {
            event.stopPropagation();
            var $_modalTarget = $j('#authModal');
            $_modalTarget.find('.modal-body > p').addClass('hidden');

            var offest = $(button).offset();
            var height = $(button).height();

            var modalData = {
            };

            // Get service call data
            $.ajax({
                url: '/customerde/index/getCustomerAuth/',
                type: "post",
                data: ({customer: customerId}),
                dataType: "json",
                success: function (results) {
                    // parse the json with customer results in the handlebars template
                    $j.extend(modalData, results.data);

                    $j.each(modalData, function (index, value) {
                        var $_elem = $_modalTarget.find('.' + index);
                        $_elem.text(value);
                        if (value) {
                            $_elem.parent().removeClass('hidden');
                        }
                    });

                    $_modalTarget.appendTo('body').modal();

                    $('.modal.auth-modal').css("top", offset.top +'px');
                }
            });
        },
        showLinkPageDetails: function (customerId) {
            var self = this;
            // Get service call data
            $.ajax({
                url: '/customerde/index/getCustomerPotentialLinks/',
                type: "post",
                data: ({customerId: customerId}),
                dataType: "json",
                success: function (response) {
                    // if there is a problem with the customer id do not proceed
                    if (response.hasOwnProperty('validationError') && response.validationError == true) {
                        var errorMessage = '';
                        for (var p in response.message) {
                            errorMessage += response.message[p] + ' ';
                        }
                        showModalError(errorMessage);
                    }
                    else {
                        self.showLinkPage(response);
                    }
                }
            });
        },
        showCustomerList: function () {
            var self = this;
            // parse the json with customer results in the handlebars template
            var template = Handlebars.compile(self.templateSourceCustomersList),
                customersHTML = template(self.customerSearchResultsJson);
            self.customerInfoContainer.html(customersHTML);
            // after the search expand the left side column
            LeftSidebar.toggleLeftSideBar(true);
            this.customerInfoContainer.show();
        },
        markLinkCustomersEvent: function (customerId, element) {
            var self = this;
            self.markLinkCustomers(customerId, element);
        },
        markLinkCustomers: function (customerId, element) {
            var self = this;
            var elemIsLinkedSelector = '.icon-already-linked.linked-id-'+customerId;
            var elemIsNotLinkedSelector = '.icon-not-linked.linked-id-'+customerId;
            if($(elemIsLinkedSelector).hasClass('hidden')) {
                $(elemIsLinkedSelector).removeClass('hidden');
                $(elemIsNotLinkedSelector).addClass('hidden');
                self.customersToLink.push(customerId);
            }
            else {
                $(elemIsNotLinkedSelector).removeClass('hidden');
                $(elemIsLinkedSelector).addClass('hidden');
                self.customersToLink.pop(customerId);
            }

            // depending if a potential linked customer is checked, show or hide the buttons to "proceed and link"/"proceed"
            if (self.customersToLink.length) {
                $('.link-proceed-button').show();
                $('.proceed-button').hide();
            } else {
                $('.link-proceed-button').hide();
                $('.proceed-button').show();
            }
        },
        linkCustomers: function (customerId) {
            var self = this;

            // if there are potential links marked => create the links
            if(self.customersToLink.length) {
                $.ajax({
                    url: '/customerde/index/createPotentialLink/',
                    type: "post",
                    data: ({customer: customerId, 'potentialCustomersLinks': self.customersToLink}),
                    dataType: "json",
                    success: function (response) {
                        if (response.error) {
                            showModalError(response.message);
                        } else {
                            self.loadCustomerData(customerId);
                        }
                    }
                });
            }
            // else load the customer
            else {
                self.loadCustomerData(customerId);
            }

        },
        showLinkPage: function (response) {
            var self = this;
            $('#customer_link_details').find('.back-button').show();
            var templateData = (response.error) ? response : {
                'error': response.error,
                'potential_links': response.data.potential_links,
                'customer_data': response.data.customer
            };
            // parse the json with customer potential links in the handlebars template
            var template = Handlebars.compile(self.templateSourceCustomersPotentialLink),
                customersHTML = template(templateData);
            this.customerInfoContainer.html(customersHTML);

            if (response.hasOwnProperty('data') && response.data.hasOwnProperty('customer') && response.data.customer.hasOwnProperty('linked')) {
                $('input[type=checkbox]').each(function () {
                    this.checked = true;
                    self.markLinkCustomers(this.value, this);
                });
            }
            // after the search expand the left side column
            LeftSidebar.toggleLeftSideBar(true);
        },
        loadCustomerInfo: function (element) {
            var self = this;

            element = $(element);
            if (!element.parent().hasClass('active')) {
                var targetId = element.attr('data-toggle');

                $.post(MAIN_URL + 'customerde/details/showCustomerPanel', {'section': targetId}, function (response) {
                    if (response.error == true) {
                        showModalError(response.message);
                    } else {

                        // set the shopping cart data on a customer variable
                        switch (targetId) {
                            case 'carts-content':
                                self.customerShoppingCartData = response;
                                break;
                            case 'products-content':
                                self.customerProducts = response;
                                break;
                        }

                        /** templates ids need to be defined as left-sidebar- + section **/
                        var template = Handlebars.compile($("#left-sidebar-" + targetId).html()),
                            customersHTML = template(response);
                        self.customerInfoContainer.html(customersHTML);

                        /** Clear previously active element **/
                        element.parent().siblings().removeClass('active');

                        /** Mark selected element as active **/
                        element.parent().addClass('active');

                        /** Apply bootstrap dropDown (if needed) for newly parsed content **/
                        $('.dropdown-toggle').dropdown();

                        LeftSidebar.toggleLeftSideBar(true);
                    }
                });

                // hide the slide button form the left container
                $('.col-left .reduce').hide();
                // hide the shadow
                $('.col-left').css('box-shadow', "none");
            } else {
                // remove active class of siblings
                element.parent().siblings().each(function () {
                    $(this).removeClass('active');
                });
            }
        },
        unloadCustomer: function () {
            var self = this;

            // Unload customer
            $.ajax({
                url: '/customerde/index/unloadCustomer/',
                type: "post",
                cache: false,
                data: ({customer: this.customerData.id}),
                dataType: "json",
                success: function (response) {
                    setPageLoadingState(true);
                    self.customerData = null;
                    window.location.reload();
                }
            });
        },
        /**
         *
         * Sort a html table content by column
         *
         * @param element
         * @param container string | the container that holds the values
         * @param rowsContainer string | the type of element that holds row entries (tr|ul)
         * @param cellContainer string | the type of element that holds the value (td|li)
         */
        sortRows: function (element, container, rowsContainer, cellContainer) {
            element = $(element);
            /** If custom compare needed, use orderBy **/
            var orderBy = element.attr("order-by");
            var columnIndex = element.index();
            /** Remove all sorting references for element siblings */
            if (element.hasClass('sort-desc')) {
                element.siblings().removeClass("sort-asc").removeClass("sort-desc");
                element.removeClass('sort-desc');
                element.addClass('sort-asc');
            } else {
                element.siblings().removeClass("sort-asc").removeClass("sort-desc");
                element.removeClass('sort-asc');
                element.addClass('sort-desc');
            }

            var elems = jQuery.makeArray($(container).find(rowsContainer));
            elems.sort(function (a, b) {
                var valueA = $(a).find(cellContainer).eq(columnIndex).html().trim();
                var valueB = $(b).find(cellContainer).eq(columnIndex).html().trim();
                var result;

                /** Numeric comparison for default sorting ascending **/
                if (((valueA - 0) == valueA && ('' + valueA).trim().length > 0) && ((valueB - 0) == valueB && ('' + valueB).trim().length > 0)) {
                    var nrValueA = parseInt(valueA);
                    var nrValueB = parseInt(valueB);

                    result = nrValueA > nrValueB ? 1 : -1;
                } else {
                    /** strange behavior for Date.parse direct comparison so values will be converted to timestamps and compared as string **/
                    if (orderBy == "date") {
                        var splitDateA = valueA.split('-');
                        var splitDateB = valueB.split('-');

                        valueA = splitDateA[2] + splitDateA[1] + splitDateA[0];
                        valueB = splitDateB[2] + splitDateB[1] + splitDateB[0];
                    }

                    result = valueA.localeCompare(valueB);
                }

                if (element.hasClass('sort-asc')) {
                    return result;
                } else {
                    return result >= 0 ? -1 : 1;
                }
            });

            $(container).html(elems);

            /** Reindex drop down for bootstrap **/
            $('.dropdown-toggle').dropdown();
        },
        getBillingInformation: function (ban) {
            event.stopPropagation();
            var $_modalTarget = $j('#billingModal');
            $_modalTarget.find('.modal-body > p').addClass('hidden');

            var modalData = {
                'ban': ban
            };

            // Get service call data
            $.ajax({
                url: '/customerde/details/getBillingInformation/',
                type: "post",
                data: ({ban: ban}),
                dataType: "json",
                success: function (results) {
                    if ((results.hasOwnProperty('validationError') && results.validationError == true) ||
                        (results.hasOwnProperty('error') && results.error == true)) {
                        showModalError(results.message);
                    }
                    else {
                        $j.extend(modalData, results);

                        $j.each(modalData, function (index, value) {
                            var $_elem = $_modalTarget.find('.' + index);
                            $_elem.text(value);
                            if (value) {
                                $_elem.parent().removeClass('hidden');
                            }
                        });
                        $_modalTarget.appendTo('body').modal();
                    }
                }
            });
        },
        showShoppingCart: function (quoteId) {
            var self = this,
                shoppingCartDetails = {};
            event.stopPropagation();

            $.each(self.customerShoppingCartData.shoppingCarts, function (key, quoteObj) {
                //to do show a specific shopping cart details when the quoteId will be send as a valid one
                // if(key == quoteId) {
                    shoppingCartDetails = quoteObj;
                // }
            });

            var template = Handlebars.compile(self.templateShoppingCartViewModal),
                cartsHTML = template(shoppingCartDetails);
            $("#shoppingCartModal").html(cartsHTML);
            $("#shoppingCartModal").appendTo('body').modal();
        },
        deleteShoppingCart: function (quoteId) {
            var self = this;
            event.stopPropagation();
            $("#shoppingCartModalDelete").appendTo('body').modal();
        },
        prolongationCheck: function (callBack) {
            var self = this;
            // Get service call data
            $.ajax({
                url: '/customerde/details/prolongationCheck/',
                type: "post",
                data: {
                    customerId: self.customerData.ban,
                    linkId: self.customerData.link_id,
                    'accounts': self.contractNoCheckedForRetention,
                    dealerId: SOASTA.dealer_code
                },
                dataType: "json",
                success: function (response) {
                    if (response.validationError) {
                        showModalError(response.message);
                    }

                    if (typeof callBack === "function") {
                        //Render response on callback function
                        callBack(response);
                    }
                }
            });
        },
        /**
         * This function is called from products-content.phtml template to update prolongation check list
         * @param contractNumber
         * @param kiasNo
         */
        addToProlongationList: function (contractNumber, kiasNo, ctn) {
            this.contractNoCheckedForRetention[contractNumber] = {
                kiasNo: kiasNo,
                ctn: ctn
            };
        },
        clearProlongationList: function () {
            //Clear prolongation list
            this.contractNoCheckedForRetention = {};
        },
        expandRightSidebar: function() {
            var direction = '';
            var enlargeButton = $('.enlarge');
            if($(enlargeButton).hasClass('left-direction')) {
                direction = 'left';
                var leftSide = $('.col-left').outerWidth();
                $(enlargeButton).removeClass('left-direction');
                $(enlargeButton).addClass('right-direction');
            }
            else {
                direction = 'right';
                var rightSide = $('.col-left').outerWidth();
                $(enlargeButton).removeClass('right-direction');
                $(enlargeButton).addClass('left-direction');
            }
            var rightDirection = $(enlargeButton).hasClass('right-direction');
            var panelDefaultWidth = 315;

            if($(enlargeButton).hasClass('expanded')) {
                var self = $(enlargeButton);
                $(enlargeButton).parent().find('.panel-content').fadeOut({
                    queue: false,
                    duration: 500,
                    complete: function() {
                        self
                            .siblings('.sticker')
                            .fadeIn(500);
                        window.configurator && window.configurator.assureElementsHeight();
                        if(direction == 'left') {
                            $('.col-right').find('#right-panel-cart').remove();
                        }
                    }
                });
                $(enlargeButton).parent().animate({
                    width: panelDefaultWidth
                }, 500);
                $('.col-main').fadeIn({queue: true, duration: 500});

                $(enlargeButton).removeClass('expanded').addClass('contracted');

            } else {

                if(direction == 'left') {
                    expandTheRightSidebar();
                }
                //if the other panel is already expanded, contract it back
                if($('button.expanded').length>0) {
                    $('button.expanded').first().trigger('click');
                }

                /** Triggering resize for cart content to expand **/
                $(window).trigger('resize');

                $('.col-main').fadeOut({queue: true, duration: 500});

                $(enlargeButton).parent().find('.sticker').fadeOut({
                    queue: false,
                    duration: 500,
                    complete: function() {
                        $(this)
                            .siblings('.panel-content')
                            .fadeIn(500);

                    }
                });

                $(enlargeButton).removeClass('contracted').addClass('expanded');
            }
        },
        switchSavedCartMenu: function(event, self) {
            event.stopPropagation();
            self = jQuery(self);

            if(!self.hasClass('open')) {
                jQuery('.col-left .block-container').find('div').removeClass('open');
                jQuery('.checkout-package-details .saved-carts-menu').find('div').removeClass('open');
            }
            self.toggleClass('open');

            self.siblings('.menu-expand').first().toggleClass('open');

            if(!self.siblings('.menu-expand').first().hasClass('top-menu')){
                jQuery('.menu-expand.top-menu').removeClass('open');
                jQuery('.drop-menu.top-menu').removeClass('open');
            }
        }
    });
})(jQuery);
