/** Disable prototype plugins that affect jQuery / bootstrap / etc functionality **/
if (Prototype.BrowserFeatures.ElementExtensions) {
    var disablePrototypeJS = function (method, pluginsToDisable) {
            var handler = function (event) {
                event.target[method] = undefined;
                setTimeout(function () {
                    delete event.target[method];
                }, 0);
            };
            pluginsToDisable.each(function (plugin) {
                jQuery(window).on(method + '.bs.' + plugin, handler);
            });
        },
        pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
    disablePrototypeJS('show', pluginsToDisable);
    disablePrototypeJS('hide', pluginsToDisable);
}

jQuery(document).ready(function ($) {
    var theBody = $('body');

    window.customerDe = new CustomerDe({
        baseUrl: MAIN_URL
        , spinner: MAIN_URL + 'skin/frontend/vodafone/default/images/spinner.gif'
    });
    $.each({
        'customer.search': 'customerde/search/getCustomerResults',
        'customer.searchLegacy': 'customerde/search/getCustomerResultsLegacy',
        'customer.load': 'customerde/details/loadCustomer',
        'customer.showRetainable': 'customerde/details/showRetainable'
    }, function (key, url) {
        window.customerDe.addEndpoint(key, url);
    });
    window.customerDe.init();
    theBody.on('click', '.enlarge', function (e) {
        customerDe.expandRightSidebar();
    });

    // When polling screen is shown, fill bar and redirect to complete (stub mode)
    $("#polling-modal").on('shown.bs.modal', function () {
        var self = $(this);
        for (var i = 1; i <= 8; i++) {
            setTimeout(function (j) {
                self.find('.progress-bar').width((j * 10 + 20) + "%");
            }, 300 * i, i);
        }

        setTimeout(function () {
            window.location = '/checkout/cart/success';
        }, 300 * i+1);
    });
});
