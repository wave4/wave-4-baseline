var phone = {
    poolingId: null,
    poolingImageId: null,
    inPooling: true,
    phoneCallSelector: '#customerPoolingIcon',
    phoneHangUpSelector: '.caller-icon-hang-up',
    phoneInactiveSelector: '.caller-icon-inactive',
    // Set current object ready for accepting incoming customers
    startPooling: function() {
        this.inPooling = true;
        // Set interval for ajax call every one second
        this.poolingId = setInterval(this.askForCustomer, 1000);
        // Set interval for icon switch every 10th of a second
        this.poolingImageId = setInterval(this.animatePhoneIcons, 400);
    },
    askForCustomer: function () {
        // Disable loading modal for this ajax request
        window.manualLoader = true;
        jQuery.post('/customerde/call/incomingCall/', {})
            .done(function(data) {
                // Got a customer id, let's set it on phone icon
                jQuery("#customerPoolingIcon").attr('data-customer-id', data.customerId);
                // Enable loading modal for further ajax request
                window.manualLoader = false;
            })
        ;
    },
    animatePhoneIcons: function () {
        jImage = jQuery("#customerPoolingIcon").children('img');
        if (jImage.attr('data-icon') === 'data-icon1') {
            jImage.attr('src', jImage.attr('data-icon2'));
            jImage.attr('data-icon', 'data-icon2');
        } else {
            jImage.attr('src', jImage.attr('data-icon1'));
            jImage.attr('data-icon', 'data-icon1');
        }
    },
    loadCustomer: function(element) {
        var customerId = jQuery(element).attr('data-customer-id');
        // There is a valid customer ready for loading
        if (customerId) {

            //Stop phone pooling and switch icons from answr to hang-up
            this.stopPooling()
                .showHangUpIcon();

            //Load customer data and parse show allowed campaigns for this customer
            customerDe.loadCustomerData(customerId);

            //@todo notify services that this CTN is requested by an agent in order to remove it from pooling list
        }
    },
    showHangUpIcon: function () {
        jQuery(this.phoneInactiveSelector).removeClass('hide').addClass('hide');
        jQuery(this.phoneCallSelector).removeClass('hide').addClass('hide');
        jQuery(this.phoneHangUpSelector).removeClass('hide');

        return this;
    },
    //This will temporarily stop pooling for customer ids
    stopPooling: function () {
        this.inPooling = false;
        clearInterval(this.poolingId);
        clearInterval(this.poolingImageId);

        return this;
    },
    hangUp: function () {
        customerDe.unloadCustomer();
    },
    filterCustomerCampaigns: function (customerData) {
        if (customerData.hasOwnProperty('customerCampaigns')) {
            customerData.customerCampaigns.forEach(function (campaignId) {
                //Enable all campaigns allowed for current customer
                var campaignElement = jQuery("#homepage-wrapper #campaigns").find("[data-campaign='" + campaignId + "']");
                campaignElement.find('.content-block').removeClass('disabled');
                campaignElement.find('.buy-campaign-button').removeClass('disabled');
                campaignElement.find('.buy-campaign-button').find("input[type='button']").removeAttr('disabled');
            });
        } else {
            console.log("Customer has no allowed campaigns associated");
        }

        return this;
    },
    // Init configurator data is built in Dyna_Bundles_Helper_Data::getCampaignsData()
    loadCampaign: function (packageType, packageId, saleType, ctn, currentProducts) {
        packageType = packageType ? packageType : null;
        packageId = packageId ? packageId : null;
        saleType = saleType ? saleType : null;
        ctn = ctn ? ctn : null;
        currentProducts = currentProducts ? currentProducts : null;

        // Hide right sidebar packages list
        jQuery('#package-types').addClass('hide');
        // Show the add package button
        jQuery('#show-packages').removeClass('hide');

        // Call init configurator with campaign arguments
        initConfigurator(packageType, packageId, saleType, ctn, null, true, null, null, function () {
            var products = currentProducts.split(",");

            jQuery.each(products, function (index, product_id) {
                var item = jQuery("#config-wrapper").find(".item-row[data-id='" + product_id + "']");

                item.find('.campaign-identifier')
                    .find('span')
                    .removeClass('hidden')
                    .html(Translator.translate("Campaign"));

                var prod_type = item.parents('.conf-block:first').data('type');

                 if (!configurator.cart[prod_type]) {
                     configurator.cart[prod_type] = [];
                 }
                configurator.cart[prod_type].push(product_id);

                jQuery("#config-wrapper").find(".item-row[data-id='" + product_id + "']").trigger("click");
            });
        });
    }
};
