/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

jQuery(document).ready(function($) {
    if (window['store'] && window['store']['cleanup']) {
        window['store']['cleanup']();
    }

    window.leavePageDialogSwitcher = false;
    window.checkoutDeliverPageDialog = false;

    var theBody         = $('body');
    var footerModals    = $('#footer_modals');
    var htmlDOM         = $('html');
    var popoverClass    = $('.popover');

    $('.customer_search_popover').popover({
        placement: 'right',
        html: true,
        container: 'body',
        trigger: 'click',
        content: function () {
            return $('#search_info_text_popover_content').html();
        }
    }).on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).show();
        if (!$('.popover').hasClass('parsed')) {
            $('.popover').css('top', '10px').css('z-index', 20001).css('width', 300 + 'px').addClass('parsed');
        } else {
            $('.popover').removeClass('parsed');
        }
    });

    if($('.side-cart.block .block-container .content.one-off-deal-package').length > 0){
        jQuery('.buttons-container #package-types').addClass('hide');
        $('#show-packages').addClass('hide');
    }

    htmlDOM.on('click', function () {
        if ($('.popover').length) {
            $('.customer_search_popover').popover('hide').show();
            $('.popover').removeClass('parsed');
        }
    });
    var topTabs = $('#top-tabs a');
    if (topTabs.length) {
        $('#top-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#top-tabs a:first').tab('show');
    }

    window.agent = new Agent({
        baseUrl: MAIN_URL
        ,spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });
    $.each({
        'agent.loginOther':'agent/account/loginAsAgent',
        'agent.logoutOther':'agent/account/logoutAsAgent',
        'agent.logout':'agent/account/logout',
        'agent.changePass':'agent/account/changeAgentPassword',
        'agent.list':'agent/account/getAgentsList'
    }, function(key, url) {
        window.agent.addEndpoint(key, url);
    });

    window.customer = new Customer({
        baseUrl: MAIN_URL
        ,spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });
    $.each({
        'customer.search':'customer/search/index',
        'customer.search.indirect':'customer/search/indirect',
        'customer.load':'customer/details/loadCustomer',
        'customer.sticker':'customer/details/getSticker',
        'customer.logout':'customer/details/unloadCustomer',
        'customer.showRetainable':'customer/details/showRetainable'
    }, function(key, url) {
        window.customer.addEndpoint(key, url);
    });
    window.customer.init();

    window.search = new Search({events: 'change keyup'});

    theBody.on('click', '.enlargeSearch', function(event) {
        LeftSidebar.toggleLeftSideBar();
    });

    function hideErrorMessages(elem) {
        // fix for identity number error message not hiding when valid input on change identity type
        if ( $(elem).val() != ''
            && Validation.validate(document.getElementById(elem.attr('id'))) === true
        ) {
            $(elem).parent().find('.validation-advice').hide('slow');
        } else if ($(elem).val() == '') {
            $(elem).parent().find('.validation-advice').hide('slow');
        }
    }

    //force correct date format for all fields with the class "date-format"
    theBody.on( "input propertychange", ".date-format", function() {
        var el = $(this);
        el.prop('maxlength', 10);

        var val = $.trim(el.val()).replace(/[^\d\-]/g, '');
        if (val !== $.trim(el.val())) {
            el.val(val);
        }
        var digits = val.replace(/[^\d]/g, '');
        if (digits.length === 8) {
            var formattedDate = moment(digits, 'DDMMYYYY').format('DD-MM-YYYY');
            if (formattedDate !== 'Invalid date') {
                el.val(formattedDate);
                hideErrorMessages(el);
            }
        }
    });

    theBody.on("click", '.top-menu-buttons .btn',function (e) {
        $('.top-menu-buttons .btn.active').removeClass('active');
        $(this).addClass('active');
    });

    theBody.on('submit', '.loginOtherUserForm',function(e){
        e.preventDefault();
        loginOtherUser();
    });

    theBody.on('click','#home-tabs a',function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('[data-slidebutton-top]').slidepanel({
        orientation: 'top',
        mode: 'overlay',
        static: true,
        container: '#top-panels',
        before_expand: function(elem) {
            $('#top-panels').show();
            $('body').addClass('toPrint');
        },
        after_collapse_callback: function() {
            $('#top-panels').hide();
            $('body').removeClass('toPrint');
        }
    });

    theBody.on('click', '#one-off-deal', function(e) {
        e.preventDefault();
        if ($('.col-right.sidebar .sticker .cart_packages .side-cart:not([data-package-type="template"])').length > 0) {
            $('#create-one-of-deal-modal').modal();
        } else {
            createOneOffDeal();
        }
    });

    theBody.on("click", '.user-menu-buttons .btn',function (e) {
        $('.user-menu-buttons .btn.active').removeClass('active');
        $(this).addClass('active');
    });


    var setContainerHeight = function() {
        var windowHeight = $(window).height();
        var containerOffsetResults = 200;
        var containerOffsetCustomers = 455;
        var containerOffsetCustomerResults = 135;
        var calculateContainerHeightResults = windowHeight - containerOffsetResults;
        var calculateContainerHeightCustomers = windowHeight - containerOffsetCustomers;
        var calculateContainerHeightCustomerResults = windowHeight - containerOffsetCustomerResults;
        $('#top-panels .tab-pane .results').height(calculateContainerHeightResults);
        $('#creditcheck-search-results').height(calculateContainerHeightCustomerResults);
        $('#validation-search-results').height(calculateContainerHeightCustomerResults);
        $('#porting-search-results').height(calculateContainerHeightCustomerResults);
        $('#openorders-search-results').height(calculateContainerHeightCustomerResults);
        $('#customer-screen-orders').height(calculateContainerHeightCustomerResults);
        $('#top-panels #customer_list').height(calculateContainerHeightCustomers);
    };
    var checkContainerExist = setInterval(function () {
        if ($('#top-panels .menu-middle').length) {
            setContainerHeight();
            window.addResizeEvent(function () {
                setContainerHeight();
            });
            clearInterval(checkContainerExist);
        }
    }, 100); // check every 100ms

    theBody.on('click','#cc-tabs a, #np-tabs a, #vc-tabs a',function (e) {
        e.preventDefault();
        $(this).tab('show');
        $('.ajax-details').addClass('hidden');
    });

    theBody.on('click', '.address-customer-pin',function(){
        var state = $(this).data('state');
        var active = Math.abs(parseInt(state)-1);
        var text = $(this).data('state-'+active);
        $(this).text(text).data('state', active);
    });

    theBody.on('click','.ctn-hide',function(){
        $('#ctn-tabs').toggle();
        if ($('#ctn-tabs').is(':visible')){
            $(this).parent().children('#arrow').removeClass('arrow-right');
            $(this).parent().children('#arrow').addClass('arrow-down');
        }else {
            $(this).parent().children('#arrow').removeClass('arrow-down');
            $(this).parent().children('#arrow').addClass('arrow-right');
        }
    });

    theBody.on('click','.customer-info-hide',function(){
        $('#customer-info-section').toggle();
        if ($('#customer-info-section').is(':visible')){
            $(this).parent().children('.customer-info-hide:first-child').removeClass('arrow-right');
            $(this).parent().children('.customer-info-hide:first-child').addClass('arrow-down');
        }else {
            $(this).parent().children('.customer-info-hide:first-child').removeClass('arrow-down');
            $(this).parent().children('.customer-info-hide:first-child').addClass('arrow-right');
        }
    });

    $('.sticker').on('click', '.customer_ctns ul li', function(){
        if($(this).hasClass('active')){
            $('.col-left .reduce').hide();
            // hide the shadow
            $('.col-left').css('box-shadow',"none");
            $('#left-panels').show();
            if (window.configurator) { $('#config-wrapper').hide(); }
        } else {
            if (window.configurator) { $('#config-wrapper').show(); }
            $('.col-left .reduce').show();
            // show the shadow
            $('.col-left').css('box-shadow',"2px 2px 5px 2px #dadada");
            $('#left-panels').hide('slow');
        }
        //remove/add full width when showing/hiding userinfo
        if($(this).hasClass('active') && $(this).find('#customer_info_left').length){
            $('.main .col-main').addClass("main-col-fullwidth");
        }else{
            $('.main .col-main.main-col-fullwidth').removeClass("main-col-fullwidth");
        }
    });

    theBody.on( 'click', '#close_customer_details',function(){
        $('#left-panels').hide('slow');
        // show config wrapper
        if (window.configurator) { $('#config-wrapper').show(); }
        // show the slide button on the left container
        $('.col-left .reduce').show();
        // hide the box-shadow
        $('.col-left').css('box-shadow',"2px 2px 5px 2px #dadada");
        //remove full width when not showing userinfo
        $('.main .col-main.main-col-fullwidth').removeClass("main-col-fullwidth");
    });


    bindSlidepanelLeft();

    // Open a specific customer tab
    var openTab = Mage.Cookies.get('openTab');
    if (openTab != undefined && openTab != '') {
        $(openTab).trigger('click');
        Mage.Cookies.set('openTab', '');
    }

    footerModals.on('click', '#save-cart-modal #change_address', function(){
        $('.email_send_wrapper').toggleClass('hidden');
        var originalEmail = $('#original_email');
        originalEmail.toggleClass('hidden');
        if(originalEmail.hasClass('hidden')){
            $('#change_address_label').html(Translator.translate('Customer email address'));
            $('#email_send').addClass('required-entry validate-email');
        }else{
            $('#change_address_label').html(Translator.translate('Other/customer address'));
            $('#email_send').removeClass('required-entry validate-email');
        }
    });

    footerModals.on('click', '#save-cart-modal .save-cart-button', function() {
        var modal = $('#save-cart-modal');
        var email_send = null;
        var to_send_email = true;
        if(!$('.email_send_wrapper').hasClass('hidden')){
            email_send = $('#email_send').val();
        }
        if ($('#send_the_cart').length > 0){
            if ($('#send_the_cart:checked').length == 0){
                to_send_email = false;
            }
        }

        var save_the_cart = true;
        if ($('#save_the_cart').length > 0){
            if ($('#save_the_cart:checked').length == 0){
                save_the_cart = false;
            }
        }

        var new_cart = true;
        if ($('#new_cart').length > 0){
            if ($('#new_cart:checked').length == 0){
                new_cart = false;
            }
        }

        var guestForm = $('#guest_form');
        if (guestForm.length > 0){
            var title = guestForm.find('input[name="titles"]:checked').data('titles');
            var initial = guestForm.find('#initial').val();
            var firstname = guestForm.find('#firstname').val();
            var lastname = guestForm.find('#lastname').val();
            var dob = guestForm.find('#dob').val();
            var guest = 1;
        }

        if (!saveCartModalForm.validator.validate()){
            return false;
        }

        $.post(MAIN_URL + 'checkout/cart/persistCart',{
            guest: guest,
            title: title,
            initial: initial,
            firstname: firstname,
            lastname: lastname,
            dob: dob,
            email_send: email_send,
            to_send: to_send_email,
            to_save: save_the_cart,
            new_cart: new_cart
        }, function(data) {
            if(data.error == true){
                $('#save-cart-modal div.alert').addClass('alert-danger').removeClass('hidden').html(Translator.translate(data.message));
            }else {
                modal.find('.after-click-block .black-check-label').text(data.message);
                modal
                    .find('.modal-body')
                    .html(modal.find('.after-click-block').html());
                modal.find('.modal-footer button').hide();
            }

            setTimeout(function(){
                $('#save-cart-modal div.alert').addClass('hidden').removeClass('alert-success alert-danger');
                if(data.error == false){
                    modal.modal('hide');
                    window.location.reload();
                }
            }, 4000);
        });
    });

    footerModals.on('click', '.cancel-cart-button', function () {
        // var toggleClass = '.soho-toggle';
        // ToggleBullet.onSwitchChange(toggleClass, 'soho');

        var modal = $('#cancel-cart-modal');
        modal
            .find('.modal-body')
            .html(modal.find('.after-click-block').html());
        modal.find('.modal-footer button').hide();
        window.canShowSpinner = false;
        if (window.ToggleBullet.switchPending == true) {
            window.ToggleBullet.onSwitchChange('.soho-toggle', 'soho');
            window.ToggleBullet.switchPending = false;
        }

        $.post(MAIN_URL + 'configurator/cart/empty', function () {
            setTimeout(function () {
                modal.modal('hide');
                window.location.reload();
            }, 2000);
        });
        window.canShowSpinner = true;
    });

    footerModals.on('click', '.logout-customer-button', function () {
        var modal = $('#logout-customer-modal');
        modal.find('.modal-footer button').hide();
        window.canShowSpinner = false;
        customer.logoutCustomer();
        window.canShowSpinner = true;
    });

    //TODO: check if the package is retention by data-sale-type attribute (add class to retention packages)
    $('.block-container .title .mark').each(function() {
        if ($(this).html() === "retentie") {
            $(this).parent().parent().addClass('retentie-package');
        }
    });

    $('.package-with-ctn').parent().find('.mark_ctn').each(function (i, _el) {
        var el = $(_el);
        el.html('&nbsp;' + (window.customer ? window.customer.formatPhoneNumberInt(el.text()) : el.text()));
    });

    // Call a specific function with the provided arguments
    var callFunc = Mage.Cookies.get('callFunc');

    if (callFunc != undefined && callFunc != '') {
        callFunc = JSON.parse(callFunc);
        var func = callFunc["fn"];
        window[func].apply({},callFunc["args"].toArray());
        Mage.Cookies.set('callFunc', '');
    }

    $.fn.removeClassPrefix=function(e){this.each(function(t,n){var r=n.className.split(" ").filter(function(t){return t.lastIndexOf(e,0)!==0});n.className=r.join(" ")});return this};

    window.duplicatePackage = function(event, element) {
        event.stopPropagation();
        event.preventDefault();
        var packageContainer = $('.cart_packages');
        var sideBlock = $(element).parents('.side-cart');
        var packageId = sideBlock.data('package-id');
        var type = $(element).parents('.side-cart').data('package-type');
        $.post(MAIN_URL + 'configurator/cart/duplicatePackage', {'packageId': packageId, 'type': type}, function(data) {
            if (data.error) {
                showModalError(data.message);
            } else {
                packageContainer.find('.side-cart').removeClass('active');
                var clone = $(data.rightBlock);
                togglePromoRules('close');
                clone
                    .data('package-type', data.type)
                    .data('package-id', data.packageId)
                    .attr('data-package-id', data.packageId)
                    .removeClass('active')
                    .find('.menu-expand')
                    .removeClass('open');
                $('.promo-rules-trigger').addClass('hidden');
                clone.insertAfter(packageContainer.find('.side-cart').last());

                sideBlock.find('.menu-expand').removeClass('open');

                $('.col-right .sticker #cart_totals').replaceWith(data.totals);
                clone.find('span.package-type').click();

                if(typeof configurator != 'undefined') {
                    configurator.packageId = data.packageId;
                }
            }
        });
    };

    window.removePackage = function(event, element) {
        event.stopPropagation();
        event.preventDefault();
        var sideBlock = $(element).parents('.side-cart');
        var blockPackageId = sideBlock.data('package-id');
        $(element).removeAttr('onclick');
        // Reset the state of the tax toggle
        store.remove('btwState');
        var callback = function(data) {
            if (data.hasOwnProperty('showOneOfDealPopup') && data.showOneOfDealPopup) {
                $('#one-of-deal-modal').data('packageId', blockPackageId).modal();
                return;
            }
            if (sideBlock.hasClass('active') && window['configurator']) {
                window['configurator'].destroy();
            }
            sideBlock.remove();
            if (data) {
                var totals = $('.sidebar #cart_totals');
                if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
            }
            // If this is within the checkout, we need to refresh the page
            if (controller && controller == 'cart') {
                $(location).attr('href', MAIN_URL + 'checkout/cart');
            } else {
                if (data.error) {
                    showModalError(data.message);
                } else {
                    // Reset packages indexs in right sidebar
                    if(data.hasOwnProperty('packagesIndex') && data['packagesIndex'] != null) {
                        var packages = $('.col-right .cart_packages .side-cart.block');
                        $.each(packages , function(index, value){
                            var initialPackageId = $(value).data('package-id');
                            var newPackageId = data['packagesIndex'][initialPackageId];
                            $(value).attr('data-package-id', newPackageId);
                            $(value).data('package-id', newPackageId);
                        });

                        if(typeof window.configurator != 'undefined'){
                            configurator['packageId'] = data['packagesIndex'][configurator['packageId']] ;
                        }
                    } else {
                        $('#package-types').removeClass('hide');
                        $('#show-packages').addClass('hide');
                    }
                    $(element).attr('onclick', 'removePackage(this, event)');
                }
            }
        };

        if (!blockPackageId) {
            callback(data);
        } else {
            // @todo on removal of bundle package remove entire bundle
            $.post(MAIN_URL + 'configurator/cart/removePackage', {'packageId': blockPackageId}, function(data) {
                if(data.complete) {
                    $('#configurator_checkout_btn').show();
                } else {
                    if ($(element).parents('.side-cart').length == 0) {
                        $('#configurator_checkout_btn').hide();
                    }
                }
                callback(data);
            });
        }
    };

    function addButtonClass($packageList) {
        var buttonsCount = $packageList.find('> div').length;
        $('#cart-spinner-wrapper').find('.cart_packages').addClass('control-buttons-' + buttonsCount);
    }

    function showAddPackageButtons() {
        var packageList = $('#package-types');
        addButtonClass(packageList);

        packageList.removeClass('hide');
    }

    var openPackage = null;
    if(controller == 'cart') { showAddPackageButtons(); }

    $('#buttons-trigger').on('click', function() {
        var target = $(this).attr('data-target');
        $('#' + target).toggle();
    });

    htmlDOM.on('click', function() {
        $('.menu-expand').removeClass('open');
        $('.drop-menu').removeClass('open');
        $('.btn-group').removeClass('open');
    });

    $(".menu-expand, .drop-menu").on('click',function(e){
        e.stopPropagation();
    });

    $('.sidebar').on('click', '#show-packages', function() {
        showAddPackageButtons();
    });

    if (getUrlParam('packageId')) {
        var sideBlock = $('.cart_packages [data-package-id="'+getUrlParam('packageId')+'"]');
        if (sideBlock.length) {
            activateBlock(sideBlock.find('a.side-block-activate'));
        }
    }
    //If we receive the parameter on the URL, init the configurator
    if (getUrlParam('type')) {
        initConfigurator(getUrlParam('type'), null, null, null);
    }

    var leftSide = $('.col-left').outerWidth();
    var rightSide = $('.col-right').outerWidth();

    function highlightErrors(content) {
        content = JSON.parse(content);
        if (!content) {
            return false;
        }
        var active = false;
        var activePackage = $('#cart-spinner-wrapper .cart_packages .active');
        if (activePackage.length) {
            active = activePackage.data('package-id');
        }

        $.each(content, function (id, el) {
            if (el.hasOwnProperty('soc-error')) {
              showModalError(el['soc-error']);
            } else if (active && id == active) {
                // Remove highlight from all sections
                $('#config-wrapper div.incomplete').removeClass('incomplete');
                // Highlight the missing sections
                $.each(el, function (section, message) {
                    $('#' + section + '-block').addClass('incomplete');
                });
            } else {
                // When package is not active highlight the entire package
                $('#cart-spinner-wrapper .cart_packages [data-package-id="' + id + '"]').addClass('incomplete');
            }
        });
    }

    // theBody.on('click', '.enlarge', function(e) {
    //     if ($(this).hasClass('left-direction')) {
    //         customer.expandRightSidebar();
    //     }
    // });

    theBody.on('click', '#top-panels .additional .top-menu-element.available, #top-panels .ongoing .top-menu-element.available', function(e) {
        $(this).toggleClass('selected');
    });

    theBody.on('click', '#top-panels .top-menu-stop-propagation', function(e) {
        e.stopPropagation();
    });

    theBody.on('click', '.reduce', function () {
        if ($(this).parent().hasClass('smallView')) {
            $(this).parent().removeClass('smallView');
            $(this).parent().addClass('bigView');
            alignRightColumn();
            if ($('.checkout.bigView').length) {
            } else if ($('.col-main.bigView').length) {
                $('.col-main.bigView').removeClass('bigView');
            }
        } else if ($(this).parent().hasClass('bigView')) {
            $(this).parent().removeClass('bigView');
            $(this).parent().addClass('smallView');
            alignRightColumn();
            if ($('.checkout').length) {
            } else if ($('.col-main').length) {
                $('.col-main').addClass('bigView');
            }
        }
    });

    if ($('.checkout').length && $('.col-left.guest').length) {
        $('.col-left .reduce').hide();
    }

    //catch enter pressed on coupon expanded shopping cart
    theBody.on('keypress', '.package-item-description-coupon .cart-coupon-input', function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).blur();
            $('body').focus();
            applyCouponCode($(this).parent(), '.package-expand', window['onCheckout']);
        }
    });

    theBody.on('click', '#right_expanded_packets li a', function(e){
        e.preventDefault();
        var elem = $(this).attr('href'),
            container = $('.expanded-package-details'),
            scrollTo = $(elem);
        $('#right_expanded_packets li').removeClass();
        $(this).parent().addClass('active');
        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() -10
        });
    });

    htmlDOM.on('click', function(){
        $('.menu-expand').removeClass('open');
        $('.drop-menu').removeClass('open');
        $('.btn-group').removeClass('open');
    });

    theBody.on('click', '.package-expand .nav-tabs a', function(e){
        e.preventDefault();
        $(this).tab('show');
    });

    // @FIXME
    setTimeout(function() {
        addPaddingToPackageDetailsContainer();
    }, 500);

    window['refreshed'] = false;
    $(window).bind('beforeunload',function(){
        window['refreshed'] = true;
    });

    window.addResizeEvent(function() {
        rtime = new Date();
        if (window.timeout === false) {
            window.timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    if (!supports_calc()) {
        window.onresize();
    }

    $(window).on('unload', function (ev) {
        if (window.leavePageDialogSwitcher === true) {
            disableLeavePageDialog();
        }
    });

    //Moved in agent exists condition - js-init.phtml

});
