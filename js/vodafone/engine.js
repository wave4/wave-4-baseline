/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

'use strict';

(function($){
    /** Serialize form object */
    $.fn.serializeObject=function(){var e=this,t={},n={},r={validate:/^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,key:/[a-zA-Z0-9_]+|(?=\[\])/g,push:/^$/,fixed:/^\d+$/,named:/^[a-zA-Z0-9_]+$/};this.build=function(e,t,n){e[t]=n;return e};this.push_counter=function(e){if(n[e]===undefined){n[e]=0}return n[e]++};$.each($(this).serializeArray(),function(){if(!r.validate.test(this.name)){return}var n,i=this.name.match(r.key),s=this.value,o=this.name;while((n=i.pop())!==undefined){o=o.replace(new RegExp("\\["+n+"\\]$"),"");if(n.match(r.push)){s=e.build([],e.push_counter(o),s)}else if(n.match(r.fixed)){s=e.build([],n,s)}else if(n.match(r.named)){s=e.build({},n,s)}}t=$.extend(true,t,s)});return t}
    var userLoaded = true;
    var timers = {};
    var root = window;
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            var d = new Date();
            timers[settings.url] = d.getTime();
            // Do not display loading for /configurator/init/getGeneralModals call
            if (!isValidURLForSpinner(settings.url)) {
                return;
            }

            var manualLoading = window.manualLoader || !settings.loader;
            if (!manualLoading) {
                if (VEngine.prototype) {
                    VEngine.prototype.setLoading(true);
                }
            }
        },
        complete: function (event, XMLHttpRequest) {
            if(timers[this.url] !== undefined) {
                var d = new Date();
                triggerAjaxEvent(this.url, d.getTime() - timers[this.url]);
                delete timers[this.url];
            }

            if (event.hasOwnProperty('responseJSON') && event.responseJSON.hasOwnProperty('error')) {
                if (event.responseJSON.error == true && event.responseJSON.session == 'expired') {
                    userLoaded = false;
                    $('html body').html('<p class="text-center" style="padding-top: 25%">You are being redirected to login page...</p>');
                    window.location.reload(true);
                    return;
                }
            }

            //Register global error handler for failed ajax requests
            if (userLoaded && event.status < 200 || event.status >= 400) {
                if (customer) {
                    customer.setLoading(false);
                }
                var message = event.statusText;
                if (event.status == 0) {
                    message = Translator.translate('URL not reachable/Network error');
                }

                if(!window['refreshed']) {
                    if (window['showModalError']) {
                        showModalError("Request failed with status code: " + event.status + ' ' + message);
                    } else {
                        alert("Request failed with status code: " + event.status + ' ' + message);
                    }
                }
            }
        },
        success: function (data, textStatus, jqXHR) {
            if(timers[this.url] !== undefined) {
                var d = new Date();
                triggerAjaxEvent(this.url, d.getTime() - timers[this.url]);
                delete timers[this.url];
            }
        }
    });

    /**
     * Generate the cache key under which to store the local data - either the cache key supplied,
     * or one generated from the url, the type and, if present, the data.
     */
    var genCacheKey = function (options) {
        var url = options.url.replace(/jQuery.*/, '');
        // Strip _={timestamp}, if cache is set to false
        if (options.cache === false) {
            url = url.replace(/([?&])_=[^&]*/, '');
        }

        return options.cacheKey || options.type + url + (options.data ? ('?' + options.data) : '');
    };
    /**
     * Determine whether we're using localStorage or, if the user has specified something other than a boolean
     * value for options.localCache, whether the value appears to satisfy the plugin's requirements.
     * Otherwise, throw a new TypeError indicating what type of value we expect.
     * @param {boolean|object} storage
     * @returns {boolean|object}
     */
    var getStorage = function(options){

        // Cancel localcache if ==========  {localCache : false}
        if (options.localCache === false) return false;

        // Enable cache only for GET calls
        var type = options.type;
        if (type.toLowerCase() != 'get') return false;

        // By default, local cache is enabled

        var storage = store; // Use `store` object from localstorage.js

        if (typeof storage === "object" && 'get' in storage &&
            'remove' in storage && 'set' in storage)
        {
            return storage;
        }
        throw new TypeError("localCache must either be a boolean value, " +
            "or an object which implements the Storage interface.");
    };
    /**
     * Prefilter for caching ajax calls.
     * See also $.ajaxTransport for the elements that make this compatible with jQuery Deferred.
     * New parameters available on the ajax call:
     * localCache   : true // required - either a boolean (in which case localStorage is used), or an object
     * implementing the Storage interface, in which case that object is used instead.
     * cacheTTL     : 5,           // optional - cache time in hours, default is 5.
     * cacheKey     : 'post',      // optional - key under which cached string will be stored
     * isCacheValid : function  // optional - return true for valid, false for invalid
     * @method $.ajaxPrefilter
     * @param options {Object} Options for the ajax call, modified with ajax standard settings
     */
    $.ajaxPrefilter(function(options, originalOptions){
        var storage = getStorage(options),
            cacheKey = genCacheKey(options),
            cacheValid = options.isCacheValid,
            value;

        if (!storage) return;

        if (cacheValid && typeof cacheValid === 'function' && !cacheValid()){
            storage.remove(cacheKey);
        }

        value = storage.get(cacheKey);
        if (!value){
            // If it not in the cache, we store the data, add success callback - normal callback will proceed
            if (options.success) {
                options.realsuccess = options.success;
            }
            options.success = function(data) {
                var strdata = data;
                //if (this.dataType.toLowerCase().indexOf('json') === 0) strdata = JSON.stringify(data);
                if (root.getType(strdata) === 'object' || root.getType(strdata) === 'array') {
                    strdata = JSON.stringify(strdata);
                }

                // Save the data to storage catching exceptions (possibly QUOTA_EXCEEDED_ERR)
                try {
                    storage.set(cacheKey, strdata);
                } catch (e) {
                    // Remove any incomplete data that may have been saved before the exception was caught
                    storage.remove(cacheKey);
                    console.log('Cache Error:'+e, cacheKey, strdata );
                }

                if (options.realsuccess) options.realsuccess(data);
            };
        }
    });

    /**
     * This function performs the fetch from cache portion of the functionality needed to cache ajax
     * calls and still fulfill the jqXHR Deferred Promise interface.
     * See also $.ajaxPrefilter
     * @method $.ajaxTransport
     * @params options {Object} Options for the ajax call, modified with ajax standard settings
     */
    $.ajaxTransport("+*", function(options, originalOptions, jqXHR){

        if (options.localCache !== false)
        {
            var cacheKey = genCacheKey(options),
                storage = getStorage(options),
                value = (storage) ? storage.get(cacheKey) : false;

            if (value){

                // In the cache? Get it, parse it to json if the dataType is JSON,
                // and call the completeCallback with the fetched value.
                //if (options.dataType.toLowerCase().indexOf('json') === 0) value = JSON.parse(value);
                try {
                    value = JSON.parse(value);
                } catch(e) {
                    /* no-op */
                }
                return {
                    send: function(headers, completeCallback) {
                        completeCallback(200, 'success', {0: value}, '');
                    },
                    abort: function() {
                        console.log("Aborted ajax transport for json cache.");
                    }
                };
            }
        }
    });

    window.VEngine = function() { this.initialize.apply(this, arguments) };
    window.VEngine.prototype = (function() {
        var engine = {};

        engine.settings = {
            baseUrl: location.protocol + '//' + location.host
            ,endpoints: {}
            ,errorMessage : 'An issue has been encountered. Please try again.'
            ,spinner: 'skin/frontend/omnius/default/images/spinner.gif'
            ,spinnerId: '#spinner'
            ,cacheKey: 'engine'
            ,debug: true
        };

        engine.waiting = false;

        /**
         * Bootstrapping the app
         */
        engine.initialize = function(opts)
        {
            /** Merge options with default ones */
            $.extend(this.settings, opts);

            /**
             * If store.js is available, use it instead
             */
            if (window['store']) {
                engine.cache = window['store'];
            }

            /** Set cache prefix */
            this.cache.prefix = 'vEngineCache_' + this.settings.cacheKey;

            /** Temporary prevent all clicks in document */
            $(document).click(function(e) {
                e.stopPropagation();
            });

            /** Init spinner */
            this.initSpinner();

            /**
             * Register ajax start/end events
             */
            var eng = this;
            $(document).ajaxComplete(function(event, xhr, settings) {
                if (!isValidURLForSpinner(settings.url)) {
                    return;
                }
                var manualLoading = window.manualLoader || false;
                if (!manualLoading) {
                    eng.setLoading(false);
                }
            });

            //load data, etc

            /** Release click lock on document */
            $(document).unbind('click');

            this.debug('Engine started');
        };

        /**
         * Initiates the loading spinner image
         * Creates an element and appends it to
         * the document body after applying styles
         */
        engine.initSpinner = function()
        {
            if ( ! $(this.settings.spinnerId).length) {
                $('body').before('<div id="' + this.settings.spinnerId.replace(/\.|#/g, '') + '" class="spinner"><p><img src="#"/></p></div>'.replace(/#/g, this.settings.spinner));
                this.debug('Spinner initiated');
            } else {
                this.debug('Spinner already present');
            }
            this.setLoading(false);
        };

        /**
         * Flag method that puts the app
         * in loading mode or removes it,
         * depending on the truth value
         * of the "state" parameter
         *
         * @param state
         */
        engine.setLoading = function (state) {
            /**
             * If window has spinner and its value is false, do nothing
             */
            if (false === window.canShowSpinner) {
                return;
            }
            /** Update loader style properties before showing it */
            var loader = $(this.settings.spinnerId);
            loader.find('img').css({
                marginTop: ($(window).height() / 2) - 20
            });

            /** Hide or show depending on "state" truth value */
            $(this.settings.spinnerId).toggle(state);
            this.waiting = state ? true : false;
        };

        /**
         *
         * @param key
         * @param url
         */
        engine.addEndpoint = function(key, url)
        {
            this.settings.endpoints[key] = url;
        };

        /**
         *
         * @param key
         */
        engine.removeEndpoint = function(key)
        {
            if (key in this.settings.endpoints) {
                delete this.settings.endpoints[key];
            }
        };

        /**
         *
         * @param key
         * @returns {boolean}
         */
        engine.hasEndpoint = function(key)
        {
            return key in this.settings.endpoints;
        };

        /**
         * Make a GET call
         *
         * @param url
         * @param data
         * @param callback
         * @param errback
         */
        engine.get = function(url, data, callback, errback)
        {
            this.ajax('GET', url, data, callback, errback);
        };

        /**
         * Make a POST call
         *
         * @param url
         * @param data
         * @param callback
         * @param errback
         */
        engine.post = function(url, data, callback, errback)
        {
            this.ajax('POST', url, data, callback, errback);
        };

        /**
         *
         * @param type
         * @param url
         * @param data
         * @param callback
         * @param errback
         */
        engine.ajax = function(type, url, data, callback, errback)
        {
            var endpoint = this.hasEndpoint(url) ? this.settings.endpoints[url] : url;
            this.debug('START "{0}" call to {1}'.format(type, url));
            var eng = this;
            $.ajax({
                url: engine.settings.baseUrl + endpoint,
                cache: true,
                data: data,
                type: type,
                loader: data.loader
            })
            .success(function(data, status) {
                callback(data, status);
                eng.debug('END "{0}" call to {1}'.format(type, url));
            })
            .error(function(xhr, textStatus, errorMessage) {
                undefined !== errback ? errback(errorMessage, textStatus, xhr) : function() {};
                eng.error('FAILED "{0}" call to {1} failed with the message "{2}"'.format(type, url, errorMessage));
            });
        };

        /**
         * Logs a debug message in the console
         * The logging is made only if window.console
         * is available and if settings.debug is true
         *
         * @param message
         */
        engine.debug = function(message) {
            if (this.settings.debug && window.console) {
                if ( ! window.console.debug) {
                    window.console.debug = window.console.log;
                }
                console.debug(this.time() + '  ' + message);
            }
        };

        /**
         * Logs an error message in the console
         * and triggers an alert with the message
         * The logging is made only if window.console
         * is available and if settings.debug is true
         *
         * @param message
         */
        engine.error = function(message) {
            if (this.settings.debug && window.console) {
                if ( ! window.console.error) {
                    window.console.error = window.console.log;
                }
                console.error(new Date() + ': ' + message);
                //alert(this.time() + '  ' + message);
            }
        };

        /**
         * Logs a warn message in the console
         * The logging is made only if window.console
         * is available and if settings.debug is true
         *
         * @param message
         */
        engine.warn = function(message) {
            if (this.settings.debug && window.console) {
                if ( ! window.console.warn) {
                    window.console.warn = window.console.log;
                }
                console.warn(this.time() + '  ' + message);
            }
        };

        /**
         * Internal method that returns the current
         * date and time well formatted for use
         * when logging message in the console
         *
         * @returns {string}
         */
        engine.time = function() {
            var d = new Date();
            var hour = (d.getHours() % 10 == d.getHours()) ? '0' + d.getHours() : d.getHours();
            var min = (d.getMinutes() % 10 == d.getMinutes()) ? '0' + d.getMinutes() : d.getMinutes();
            var sec = (d.getSeconds() % 10 == d.getSeconds()) ? '0' + d.getSeconds() : d.getSeconds();
            var day = (d.getDate() % 10 == d.getDate()) ? '0' + d.getDate() : d.getDate();
            var month = d.getMonth();
            month++;
            month = (month % 10 == month) ? '0' + month : month;
            var year = d.getFullYear();
            return '['+year+'-'+month+'-'+day+' '+hour+':'+min+':'+sec+']';
        };

        /**
         * Internal helper
         *
         * @type {{isFunction: Function}}
         */
        engine.helper = {

            /**
             * Checks if the given object
             * is a function
             *
             * @param item
             * @returns {*|boolean}
             */
            isFunction: function(item) {
                var getType = {};
                return item && getType.toString.call(item) === '[object Function]';
            },
            isObject: function(item) {
                var getType = {};
                return item && getType.toString.call(item) === '[object Object]';
            },
            isArray: function(item) {
                var getType = {};
                return item && getType.toString.call(item) === '[object Array]';
            },
            isJson: function(item) {
                return $.trim(item).length && /^[\],:{}\s]*$/
                    .test(item.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
            }
        };

        /**
         * Simple cache store using localStorage
         *
         * @type {{prefix: string, set: Function, get: Function, pluck: Function, flush: Function}}
         */
        engine.cache = {
            /**
             * Internal key prefix
             * This prefix will be prepended
             * to all stored keys
             */
            prefix : 'vEngineCache_',

            /**
             * @param key
             * @param value
             * @returns {*}
             */
            set : function(key, value) {
                if (engine.helper.isObject(value) || engine.helper.isArray(value)) {
                    value = JSON.stringify(value);
                }
                localStorage.setItem(this.prefix + key, value);
                return value;
            },

            /**
             * @param key
             * @returns {*}
             */
            get : function(key) {
                var result = localStorage.getItem(this.prefix + key);
                if (result !== null) {
                    try {
                        result = JSON.parse(result);
                    } catch(e) {
                        //no-op
                    }
                    return result;
                }
                return undefined;
            },

            /**
             * @param key
             * @returns {*}
             */
            pluck : function(key) {
                var result = localStorage.getItem(this.prefix + key);
                if(result !== null) {
                    localStorage.removeItem(this.prefix + key);
                    try {
                        result = JSON.parse(result);
                    } catch(e) {
                        //no-op
                    }
                    return result;
                }
                return undefined;
            },

            /**
             * @returns {boolean}
             */
            flush : function() {
                var reg = new RegExp('^' + this.prefix);
                Object.keys(localStorage)
                    .forEach(function(key){
                        if (reg.test(key)) {
                            localStorage.removeItem(key);
                        }
                    });
                return true;
            }
        };

        return engine
    })();
})(jQuery);

var isValidURLForSpinner = function(url) {
    var valid = true;
    var dontSpin = ['checkout/index/validateDate', 'configurator/init/getGeneralModals'];
    jQuery.each(dontSpin, function(index, value) {
        if(url.search(value) != -1) {
            valid = false;
            return;
        }
    });

    return valid;
};

if (!String.prototype.format) {
    String.prototype.format = function() {

        var args = arguments;
        var sprintfRegex = /\{(\d+)\}/g;

        var sprintf = function (match, number) {
            return number in args ? args[number] : match;
        };

        return this.replace(sprintfRegex, sprintf);
    };
}
/**
 * Manually set Loader state. 
 * Calling this function will set the page in loading state until canceled manually. 
 * Note: This overrides the default behavior of the ajax loading until canceled.
 * 
 * @param {boolean} state
 */
function setPageLoadingState(state) {
    window.manualLoader = state || false;
    window.VEngine.prototype.setLoading(state);
}

/**
 * Manually set Loader state.
 * Calling this function will prevent the loader from showing until canceled manually.
 *  Note: This overrides the default behavior of the ajax loading until canceled.
 * @param {boolean} state
 */
function setPageOverrideLoading(state) {
    window.manualLoader = state || false;
}

/**
 * Format date automatically to dd-mm-yyyy
 * @param elem
 */
function onDateKeyUp(elem) {
    var value = elem.value;
    if (value && !value.match(/\d{2}-\d{2}-\d{4}/) && value.replace(/-/g, '').length == 8 ) {
        value = value.replace(/[^0-9]/g, '');
        elem.value = [value.slice(0, 2), value.slice(2, 4), value.slice(4, 8)].join('-');
    }
}

/**
 *
 */
function enableLeavePageDialog() {
    window.leavePageDialogSwitcher = true;
    updatePageDialogSwitcher(true);
}

/**
 *
 */
function disableLeavePageDialog() {
    window.leavePageDialogSwitcher = false;
    updatePageDialogSwitcher(false);
}

/**
 *
 * @param value
 */
function updatePageDialogSwitcher(value) {
    jQuery.ajax({
        async: true,
        type: 'POST',
        url: MAIN_URL + 'checkout/cart/leavePageDialogSwitcher',
        data: {locked: value},
        success: function () {
        },
        error: function () {
            console.log('Failed to retrieve the data');
        }
    });
}

/**
 * Super agents login as other agent
 */
function loginOtherUser(){
    jQuery('#agent-username-input').val() ? (jQuery('#agent-login-error').html('') && agent.loginOtherAgent(jQuery('#agent-username-input').val())) : jQuery('#agent-login-error').html(Translator.translate('Please select a value'));
}

/**
 *
 * @param el
 */
function showOptions(el) {
    el = jQuery(el);
    var block = el.parents('.search-block');
    var section = block.find('#search-customer-type');
    var on = el.data('on');
    var visible = el.data('visible');
    var hidden = el.data('hidden');
    if (on == 0) {
        section.show();
        el.text(visible).data('on', '1');
        section.find(':radio').prop('disabled', false);
        checkCustomerTypeOption(section.find(':radio').first().prop('checked', true));
        block.find('#pin').prop('disabled', false);
    } else {
        section.hide();
        el.text(hidden).data('on', '0');
        section.find(':radio').prop('disabled', true);
        block.find(':text').prop('disabled', false);
        block.find('#pin').prop('disabled', true);
    }
}

/**
 *
 * @param el
 */
function checkCustomerTypeOption(el) {
    el = jQuery(el);
    var section = el.parents('.search-block');
    var siblings = el.parents('#search-customer-type').find(':radio').not(el);
    var allowedFields = [];
    switch (el.val()) {
        case '1':
            allowedFields = ['customer_ctn', 'dob', 'pin'];
            break;
        case '2':
            allowedFields = ['ban', 'pin', 'company_coc'];
            break;
        case '3':
            allowedFields = ['customer_ctn', 'pin', 'company_coc'];
            break;
        default:
            alert(Translator.translate('Invalid option'));
            return;
    }
    section.find(':text').each(function() {
        var input = jQuery(this);
        if (-1 === jQuery.inArray(input.prop('name'), allowedFields)) {
            input.prop('disabled', true);
        } else {
            input.prop('disabled', false);
        }
    });
}

/**
 *
 * @param order_id
 * @returns {boolean}
 */
function loadSuperOrder(order_id) {
    if (checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        jQuery('#checkout_deliver_confirmation').data('function', 'loadSuperOrder').data('param1', order_id).appendTo('body').modal();
        return false;
    }

    if (customer) {
        customer.setLoading();
    }
    window.location.href = MAIN_URL + 'checkout/cart/editSuperorder?superorderId=' + order_id;
}

/**
 *
 * @param item
 */
function viewItemDetails(item) {
    var item = jQuery(item);
    var orderRef = item.data('order-reference');
    var orderId = item.data('order-id');
    var packageId = item.data('package-id');
    var tab = item.parents('.tab-pane');
    var section = item.parents('.cb_slide_panel').data('section');
    var tabId = tab.attr('id');

    jQuery.post(MAIN_URL + 'dataimport/index/viewOrderDetails', {orderId: orderId, section: section, orderRef: orderRef, packageId: packageId}, function(response) {
        if(response.error) {
            showModalError(response.message);
        } else {
            tab.removeClass('active');
            var html = setHtmlTemplateData(section, response.data);
            var ajaxItem = jQuery('#ajax-item-details-{0}'.format(section));
            ajaxItem.html(html).removeClass('hidden');
            ajaxItem.find('.back').data('prev-tab', tabId).removeClass('hidden');
        }
    });

}

function updateSelectedSectionResults(section) {
    var ids = [];
    jQuery('#{0} .top-menu-element.selected'.format(section)).each(function (id, el) {
        ids.push(jQuery(el).data('order-number'));
    });
    if (ids.length == 0) {
        alert(Translator.translate('You must select at least an order that should be updated'));
        return false;
    }

    jQuery.post(MAIN_URL + 'dataimport/index/updateSectionData', {section: section, ids: ids}, function (response) {
        if (response.error) {
            showModalError(response.message);
        } else {
            window.manualLoader = true;
            customer.updateSectionResults(section);
        }
    });
}

function showInitOrderModal(dom){
    var item = jQuery(dom);
    var modalDom = jQuery('#customer_init_order');
    var submitBtn = modalDom.find('.modal-dialog .modal-content .modal-footer .btn-info');
    submitBtn.data('customer', item.data('customer'));
    submitBtn.data('order', item.data('order'));
    modalDom.modal();
    modalDom.css('z-index', '9999');
}

/**
 *
 * @param dom
 */
function viewOrder(dom) {
    var valueSource = jQuery(dom);
    var order_id = valueSource.data('order');
    var customerId = valueSource.data('customer');
    valueSource.data('order', '');
    valueSource.data('customer', '');
    self.order_id = order_id;
    var callback = function () {
        window.manualLoader = true;
        var refreshCallback = function () {
            loadSuperOrder(self.order_id);
        };
        return customer.poolRefreshData(refreshCallback);
    };

    jQuery('#customer_init_order').modal('hide').data('modal', null); // destroy modal

    // Only reload the customer if the order belongs to another customer
    if (customerId != customer.getLoggedInCustomer()) {
        customer.loadCustomerData(customerId, false, false, callback, true);
    } else {
        loadSuperOrder(self.order_id);
    }
}

/**
 *
 * @param customerId
 * @param section
 */
function topMenuSearch(customerId, section) {
    jQuery.post(MAIN_URL + 'dataimport/index/searchByCustomer', {customerId: customerId, section: section, customerScreen: false}, function (response) {
        if (response.error) {
            jQuery('#{0}-no-search-results'.format(section)).removeClass('hidden');
            jQuery('#{0}-search-results'.format(section)).empty().addClass('hidden');
        } else {
            var html = '';
            if (section != 'openorders') {
                jQuery.each(response.orders, function (id, order) {
                    jQuery.each(order, function (package_id, package_obj) {
                        html += setHtmlTemplateData(section, package_obj);
                    });
                });

                jQuery('#{0}-no-search-results'.format(section)).addClass('hidden');
                jQuery('#{0}-search-results'.format(section)).html(html).removeClass('hidden');
            } else {
                html = setHtmlTemplateData(section, response);

                jQuery('#{0}-no-search-results'.format(section)).addClass('hidden');
                jQuery('#{0}-search-results'.format(section)).html(html).removeClass('hidden');

                var container = jQuery('#openorders-search-results');
                updateSectionOpenOrdersInxedes(container, response, false);
            }
        }
    });
}

/**
 *
 * @param section
 * @param data
 * @returns {*}
 */
function setHtmlTemplateData(section, data) {
    var sectionTemplate = '{0}_template'.format(section);
    var sectionTemplateCompiled = '{0}_template_compiled'.format(section);

    var source   = window[sectionTemplate] || jQuery('#{0}-details-template'.format(section)).html();
    window[sectionTemplate] = source;

    var template = window[sectionTemplateCompiled] ||  Handlebars.compile(source);
    window[sectionTemplateCompiled] = template;

    return template(data);
}

/**
 *
 * @param item
 */
function backToList(item) {
    item = jQuery(item);
    var prevTab = item.data('prev-tab');
    jQuery('#{0}'.format(prevTab)).addClass('active');
    item.parents('.ajax-details').addClass('hidden');
}

function updateSectionResultsByType(section, type, page, container) {
    jQuery.post(MAIN_URL + 'dataimport/index/updateSectionResults', {section: section, type: type, page: page}, function (response) {
        if (response.error) {
            showModalError(response.message);
        } else {
            container.replaceWith(response.html);
        }
    });
}

function updateSectionOpenOrdersSection(cluster, customer, page, orderByField, orderType, resultsOnPage, customerScreen, async) {
    cluster = typeof cluster !== 'undefined' ? cluster : 'all';
    customer = typeof customer !== 'undefined' ? customer : 0;
    page = typeof page !== 'undefined' ? page : 1;
    orderByField = typeof orderByField !== 'undefined' ? orderByField : 'id';
    orderType = typeof orderType !== 'undefined' ? orderType : 'desc';
    resultsOnPage = typeof resultsOnPage !== 'undefined' ? resultsOnPage : '';
    customerScreen = typeof customerScreen !== 'undefined' ? customerScreen : false;
    async = typeof async !== 'undefined' ? async : true;
    var section = customerScreen ? 'customer-screen-orders' : 'openorders';

    if (customer != 0) {
        jQuery('.el-tab').removeClass('active');
        jQuery('a[href=#oo-customer-search]').addClass('active');
    }

    jQuery.ajax({
        type: "POST",
        url: MAIN_URL + 'dataimport/index/searchByCustomer',
        data: {
            'section': section,
            'customerId': customer,
            'customerScreen': customerScreen,
            'type': '',
            'page': page,
            'cluster': cluster,
            'orderbyfield': orderByField,
            'ordertype': orderType,
            'resultsOnPage': resultsOnPage
        },
        async: async
    }).done(function (response) {
        {
            var htmlResponse = setHtmlTemplateData('openorders', response);
            var container;
            if (customerScreen) {
                jQuery('#customer-screen-orders').html(htmlResponse);
                jQuery(window).trigger('resize');
                container = jQuery('#customer-screen-orders');
            } else {
                jQuery('#openorders-no-search-results'.format('openorders')).addClass('hidden');
                jQuery('#openorders-search-results'.format('openorders')).html(htmlResponse).removeClass('hidden');
                container = jQuery('#openorders-search-results');
            }

            updateSectionOpenOrdersInxedes(container, response, customerScreen);
        }
    });
}

function updateSectionOpenOrdersInxedes(container, response, customerScreen) {
    var $ = jQuery;
    if (response.active) {
        if (customerScreen) {
            var ordersPanel = '#customer-screen-orders'
        } else {
            var ordersPanel = '#openorders-search-results';
        }
        $(ordersPanel + " " + ".order_table_details[target='" + response.active + "_list']").find('span').html('k');
        $(ordersPanel + " " + "." + response.active + "_list").show();
    }

    container.find('.openorderby').off('click').click(function () {
        var cluster = $(this).attr('cluster');
        var page = 1;
        var orderByField = $(this).attr('order-by');
        var orderType = $(this).attr('oder-type');
        var customer = $(this).parents('table.orders_table').attr('orders_customer');
        var results = $(this).attr('orders-results');
        updateSectionOpenOrdersSection(cluster, customer, page, orderByField, orderType, results, customerScreen);
    });

    container.find('.open_order_error_info').hover(
        function () {
            $(this).find('.open_order_error').show();
        },
        function () {
            $(this).find('.open_order_error').hide();
        }
    );

    container.find(".order_details").off('click').click(function () {
        var container = $(this);
        if (container.attr('cluster') == 'inlife') {
            customer.loadInlifeOrder(container.attr('order_number'));
        } else {
            var target = container.next();
            toggleOrderDetails(container, !target.is(':visible'));
        }
    });
    
    container.find('.order_table_details').off('click').click(function () {
        var target = $(this).next('.' + $(this).attr('target'));
        if (target.is(':visible')) {
            $(this).find('span').html('j');
            target.hide();
        } else {
            $(this).parent().find('.orders_table').hide();
            $(this).parent().find(".order_table_details").find('span').html('j');
            var cluster = $(this).attr('orders_type');
            var customer = $(this).attr('orders_customer');
            updateSectionOpenOrdersSection(cluster, customer, 1, 'id', 'desc', '', customerScreen);
            $(this).find('span').html('k');
            target.show();
        }
    });

    container.find(".open_order_results_change").off('click').click(function () {
        var cluster = $(this).parent().find('span input').attr('orders_section');
        var orderByField = $(this).parent().find('span input').attr('order-by');
        var orderType = $(this).parent().find('span input').attr('orders_type');
        var customer = $(this).parent().find('span input').attr('orders_customer');
        var page = 1;
        var results = $(this).parent().find('span input').val();
        var maximum_results = $('#maximum_oo_results').val();
        if (parseInt(results) > parseInt(maximum_results)) {
            showModalError(Translator.translate("Sorry, but the maximum number of results per page is ") + " " + maximum_results);
        } else {
            updateSectionOpenOrdersSection(cluster, customer, page, orderByField, orderType, results, customerScreen);
        }
    });

    container.find('.open_orders_results input').keyup(function (e) {
        if (e.keyCode == 13) {
            var cluster = $(this).attr('orders_section');
            var orderByField = $(this).attr('order-by');
            var orderType = $(this).attr('orders_type');
            var customer = $(this).attr('orders_customer');
            var page = 1;
            var results = $(this).val();
            var maximum_results = $('#maximum_oo_results').val();
            if (parseInt(results) > parseInt(maximum_results)) {
                showModalError(Translator.translate("Sorry, but the maximum number of results per page is ") + " " + maximum_results);
            } else {
                updateSectionOpenOrdersSection(cluster, customer, page, orderByField, orderType, results, customerScreen);
            }
        }
    });

    container.find('.pagination-open-orders li').click(function () {
        if (!$(this).hasClass('disabled')) {
            var cluster = $(this).attr('orders_section');
            var orderByField = $(this).attr('order-by');
            var orderType = $(this).attr('orders_type');
            var customer = $(this).attr('orders_customer');
            var page = $(this).attr('orders-page');
            var results = $(this).attr('orders-results');
            updateSectionOpenOrdersSection(cluster, customer, page, orderByField, orderType, results, customerScreen);
        }
    });
}

function toggleOrderDetails(container, state) {
    var target = jQuery(container).next();
    if (!state) {
        container.find('span.menu-arrow-down').html('j');
        target.hide();
    } else {
        container.parent().find('.package_target').hide();
        container.parent(".order_details span.menu-arrow-down").html('j');
        container.find('span.menu-arrow-down').html('k');
        target.show();
    }
}

function createOneOffDeal(nonRetainableArgs) {
    if (jQuery('#is_order_edit_mode').val() == 1) {
        showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
        return;
    }

    var msg = jQuery('#no_retainable_msg');
    msg.hide();
    if (typeof nonRetainableArgs == "undefined") {
        var nonRetainable = {};
        jQuery('.ctn_record').not('.retainable-ctn').each(function(i,_el) {
            jQuery.extend(nonRetainable, {0: jQuery(_el).data('rawctn')});
        });
    } else {
        var nonRetainable = nonRetainableArgs;
    }

    // Handle situation when it is called from order edit page
    if (typeof initConfigurator == 'undefined' || typeof createCartPackageLine == 'undefined') {
        jQuery('#spinner').toggle('show', function() {
            Mage.Cookies.set('callFunc', JSON.stringify({"fn": "createOneOffDeal", "args": [nonRetainable]}));
            window.location.replace('/');
        });
        return;
    }

    jQuery.post(MAIN_URL + 'customer/details/oneOffDeal', { nonRetainable: jQuery.makeArray(nonRetainable) }, function(data) {
        if (data.hasOwnProperty('error')) {
            msg.html(data.message).show();
            return;
        }
        jQuery('.col-right .sticker').replaceWith(data.rightSidebar);
        jQuery('#close_customer_details').trigger('click');
        jQuery('.buttons-container #package-types').addClass('hide');
        jQuery('#show-packages').addClass('hide');

        var _ctn;
        var _packageId;
        jQuery.each(data.packageIds, function(ctn, packageData) {
            _ctn = ctn;
            _packageId = packageData.id;
        });

        initConfigurator(data.type, _packageId,  data.saleType, _ctn);
    });

    jQuery('#create-one-of-deal-modal').modal('hide');
}

/**
 *
 * @param ctn
 * @param sale_type
 * @param current_products
 * @returns {boolean}
 */
function createPackage(ctn, sale_type, current_products) {
    if(checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        jQuery('#checkout_deliver_confirmation').data('function','createPackage').data('param1', ctn).data('param2', sale_type).data('param3', current_products).appendTo('body').modal();
        return false;
    }

    ctn = jQuery.trim(ctn);
    current_products = typeof current_products !== 'undefined' ? current_products : null;

    // Handle situation when it is called from order edit page
    if (typeof initConfigurator == 'undefined' || typeof activateBlock == 'undefined') {
        jQuery('#spinner').toggle('show', function() {
            Mage.Cookies.set('callFunc', JSON.stringify({"fn": "initConfigurator", "args": ["mobile", null, sale_type, ctn, current_products]}));
            window.location.replace('/');
        });
        return;
    }

    jQuery('#close_customer_details').trigger('click');
    if (jQuery('.mark_ctn[data-rawctn="' + ctn + '"]').length) {
        activateBlock(jQuery('.mark_ctn[data-rawctn="' + ctn + '"]').first().siblings('.package-type').first());
    } else {
        jQuery('#package-types').addClass('hide');

        if (!showOnlyOnePackage) {
            jQuery('#show-packages').removeClass('hide');
            jQuery('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
        }

        initConfigurator('mobile', null, sale_type, ctn, current_products);
    }
}

/**
 *
 */
function proceedToExtendedCart() {
    jQuery('.enlarge.left-direction').trigger('click');
}

/**
 *
 * @param elem
 */
function acceptRemoveOneOfDeal(elem, controller) {
    var $ = jQuery;
    var el = $(elem);
    var package_id = el.parents('#one-of-deal-modal').data('packageId');
    var sideBlock = el.parents('.side-cart');

    $.post(MAIN_URL + 'configurator/cart/removePackage', {'packageId': package_id, accepted: 1}, function (data) {
        if (data.complete) {
            $('#configurator_checkout_btn').show();
        } else {
            $('#configurator_checkout_btn').hide();
        }

        if (sideBlock.hasClass('active') && window['configurator']) {
            window['configurator'].destroy();
        }
        sideBlock.remove();
        if (data) {
            var totals = $('.sidebar #cart_totals');
            if (data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
        }

        // remove one of deal packages
        if (data.hasOwnProperty('removeRetainables') && data.removeRetainables) {
            $.each(data.removeRetainables, function (_, i) {
                $('[data-package-id="' + i + '"]').remove();
            });
        }

        // If this is within the checkout, we need to refresh the page
        if (controller && controller == 'cart') {
            $(location).attr('href', MAIN_URL + 'checkout/cart');
        } else {
            // Reset packages indexs in right sidebar
            if (data.hasOwnProperty('packagesIndex') && data['packagesIndex'] != null) {
                $('.col-right .cart_packages .side-cart.block').each(function () {
                    var initialPackageId = $(this).data('package-id');
                    var newPackageId = data['packagesIndex'][initialPackageId];
                    $(this).data('package-id', newPackageId);
                    $(this).attr('data-package-id', newPackageId);
                });
            }
        }

        if (window.configurator != undefined) {
            window.configurator.destroy();
        }

        $('.buttons-container #package-types').removeClass('hide');

        $('#one-of-deal-modal').modal('hide').data('modal', null); // destroy modal
        $('.expanded.right-direction.enlarge').click();
    });
}

/**
 * Show a modal with specific content
 * @param {string|html} body
 * @param {string|html} title // optional
 * @param {callback} callbackOnClose // optional
 */
function showModalError(body, title, callbackOnClose) {
    var $  = jQuery;

    if($.trim(body) == '') {
        body = Translator.translate('Unknown error or no response received');
    }
    if($('#error-modal :visible').length > 0) {
        if(errorQueue.length) {
            var element = errorQueue.pop();
            errorQueue.push(element);
            if(element.body != body || element.title != title || element.callback != callbackOnClose) {
                errorQueue.push({'body':body, 'title':title, 'callback': callbackOnClose});
            }
        } else {
            errorQueue.push({'body':body, 'title':title, 'callback': callbackOnClose});
        }
        return;
    }

    callbackOnClose = callbackOnClose || function() {};
    title = title || Translator.translate('Error');
    var buttonText = Translator.translate('Ok');

    var modal = $('<div class="modal" id="error-modal" tabindex="-1" role="dialog" aria-hidden="true" style="top:10%; text-align: left; z-index:9999; overflow-y: auto;">' +
    '<div class="modal-dialog">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
    '<h4 class="modal-title" id="error-modal-title"></h4>' +
    '</div>' +
    '<div class="modal-body">' +
    '<div class="modal-block">' +
    '<div class="message" id="error-modal-body"></div>' +
    '<div class="clearfix clearer clear"></div>' +
    '</div>' +
    '</div>' +
    '<div class="modal-footer">' +
    '<button type="button" class="btn pull-right" data-dismiss="modal">' + buttonText + '</button>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>');

    modal
        .find('#error-modal-title').html(title).end()
        .find('#error-modal-body').html(body).end()
        .modal()
        .on('hide.bs.modal', function() { callbackOnClose(); })
        .on('hidden.bs.modal', function() {
            modal.remove();
            if(typeof errorQueue !== 'undefined') {
                if (errorQueue.length) {
                    var element = errorQueue.pop();
                    showModalError(element.body, element.title, element.callback);
                }
            }
        })
    ;
}

/**
 *
 */
function businessOnlyModal() {
    jQuery('#business_only_modal').modal();
}

/**
 *
 * @param self
 * @param coupon
 * @param parent_class
 * @returns {boolean}
 */
function removeCouponCode(self, coupon, parent_class) {
    if(jQuery.trim(coupon) != '') {
        var package_id = jQuery(self).parents(parent_class).data('package-id');
        var isCheckout = arguments.length == 4;
        jQuery.post(MAIN_URL + 'pricerules/promo/coupon', {coupon:jQuery.trim(coupon), packageId:package_id, remove: true, parentClass: parent_class , isCheckout: isCheckout}, function(response) {
            if(response.error == true) {
                jQuery('#coupon-check-invalid-coupon .message').html(Translator.translate(response.message));
                jQuery('#coupon-check-invalid-coupon').modal();
            } else {
                jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.cart-coupon-input').attr('disabled', false).val('').show();
                jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.submit-coupon-button').show();
                jQuery('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.remove-coupon').remove();
                jQuery('.sticker #cart_totals').replaceWith(response.totals);
                jQuery('.expanded-shoppingcart-totals #cart_totals').replaceWith(response.totals);
                if (isCheckout){
                    jQuery('.checkout-totals #cart_totals').replaceWith(response.totals);
                    if (typeof response.quoteTotals != 'undefined' && response.quoteTotals) {
                        window['quoteGrandTotal'] = response.quoteTotals.quoteGrandTotal.rawValue;
                        jQuery('.quoteGrandTotal').text(response.quoteTotals.quoteGrandTotal.formatted);
                        jQuery('.split-payment-method[data-package-id="{0}"]'.format(package_id)).find('.amount').text(response.quoteTotals.packageTotal);
                        window['checkout'].ignoreOffer = true;
                        jQuery('[name="customer[type]"]:checked').trigger('click');
                        window['checkout'].ignoreOffer = false;
                    }
                }

                if (parent_class == '.side-cart') {
                    jQuery('.side-cart.active').replaceWith(response.rightBlock);
                }
                if (parent_class == '.package-expand') {
                    jQuery('#right-panel-cart').replaceWith(response.rightBlock);
                    //jQuery('#right-panel-cart').replaceWith(response.rightBlockCollapsed);
                    //jQuery('div[data-package-id]').first().replaceWith(response.rightBlockCollapsed);
                    //jQuery('.cart_right_expanded_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlock);
                    jQuery('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
                    jQuery('#right-panel-cart').show();
                }
            }
        });
    }
    return false;
}

/**
 *
 * @param form
 * @param parent_class
 * @returns {boolean}
 */
function applyCouponCode(form, parent_class) {
    var $ = jQuery;
    var isCheckout = (arguments.length == 3 && arguments[2] != 0);
    var additional_param = '';
    var field = $(form).find('[name=coupon]');
    var coupon = field.val();
    var package_id = $(form).parents(parent_class).data('package-id');
    if($.trim(coupon) != '') {
        $.post(MAIN_URL + 'pricerules/promo/coupon', {coupon:coupon, packageId:package_id, parentClass: parent_class, isCheckout: isCheckout }, function(response) {
            if(response.error == true) {
                $('#coupon-check-invalid-coupon .message').html(Translator.translate(response.message));
                $('#coupon-check-invalid-coupon').modal();
                $(form).find('.cart-coupon-input').val('')
            } else {
                if (isCheckout){
                    additional_param = ', true';
                    $('.checkout-totals #cart_totals').replaceWith(response.totals);
                    if (typeof response.quoteTotals != 'undefined' && response.quoteTotals) {
                        window['quoteGrandTotal'] = response.quoteTotals.quoteGrandTotal.rawValue;
                        $('.quoteGrandTotal').text(response.quoteTotals.quoteGrandTotal.formatted);
                        $('.split-payment-method[data-package-id="{0}"]'.format(package_id)).find('.amount').text(response.quoteTotals.packageTotal);
                        window['checkout'].ignoreOffer = true;
                        $('[name="customer[type]"]:checked').trigger('click');
                        window['checkout'].ignoreOffer = false;
                    }
                }
                $('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('.submit-coupon-button').hide();
                $('{0}[data-package-id="{1}"]'.format(parent_class,package_id)).find('[name=coupon]').attr('disabled', 'disabled').hide().parent().append($('<span />', { class: 'remove-coupon', html: 'Coupon: '+coupon, onclick: 'removeCouponCode(this,\''+coupon+'\',\''+parent_class+'\''+ additional_param +')' }));
                $('.sticker #cart_totals').replaceWith(response.totals);
                $('.expanded-shoppingcart-totals #cart_totals').replaceWith(response.totals);

                if (parent_class == '.side-cart' && $('.side-cart').length > 0) {
                    $('.side-cart.active').replaceWith(response.rightBlock);
                }
                if (parent_class == '.package-expand' && $('#right-panel-cart').length > 0) {
                    $('#right-panel-cart').replaceWith(response.rightBlock);
                    $('.cart_packages div[data-package-id="'+ package_id +'"]').replaceWith(response.rightBlockCollapsed);
                    //$('div[data-package-id]').first().replaceWith(response.rightBlockCollapsed);
                    $('#right-panel-cart').show();
                }
            }
        });
    }
    return false;
}

function customerEmailChanged(input) {
    var $ = jQuery;
    var email = $(input).val();

    if (!checkout || checkout.currentCheckoutStep !== 'saveOverview') {
        return;
    }

    var $saveOverviewContainer = $('.save-overview-container');
    var isHidden = $saveOverviewContainer.hasClass('hidden');
    $.post(MAIN_URL + 'checkout/cart/changeCustomerEmail', {email: email}, function(response) {
        $saveOverviewContainer.replaceWith(response);

        if (!isHidden) {
            $('.save-overview-container').removeClass('hidden');
        }
    });
}

/**
 *
 * @param self
 * @param target
 */
function togglePassword(self, target) {
    if (jQuery(self).is(":checked")) {
        jQuery(target).attr('type', 'text');
    } else {
        jQuery(target).attr('type', 'password');
    }
}

/**
 *
 */
function showSaveCartModal() {
    jQuery('#save-cart-modal').modal();
}

/**
 *
 */
function showCancelCartModal() {
    jQuery('#cancel-cart-modal').modal();
}

function showLogoutCustomerModal()
{
    jQuery('#logout-customer-modal').modal();
}

/**
 *
 * @param el
 * @param e
 * @param showOtherStores
 */
function showStockStatus(el, e, showOtherStores) {
    e.stopPropagation();
    if(jQuery('#stock-modal').hasClass('in')){ // Check if modal is already active
        jQuery('#stock-modal').modal('hide');
    }
    var url = MAIN_URL + 'configurator/init/getStock';
    var id = jQuery(el).data('id');
    jQuery.ajax(url, {
            type: 'POST',
            data: {axi_store_id: id}
        }
    ).done(function (data) {
            if (data.hasOwnProperty('error') && data.error) {
                showModalError(Translator.translate(data.message));
            } else {
                jQuery('#stock-modal').modal();
                jQuery('#stock-modal .modal-dialog .modal-body .modal-block').html(data);
                if (showOtherStores) {
                    jQuery('#stock-modal .modal-dialog .modal-body .modal-block #display-stores').trigger('click');
                }
            }
        }
    );
}

/**
 *
 * @param cartId
 */
function showDetailsCartModal(cartId) {
    jQuery.ajax(MAIN_URL + 'configurator/init/showDetailsCart', {
            type: 'POST',
            data: {cart_id : cartId}
        }
    ).done(function(data) {
        if (data.error) {
            showModalError(Translator.translate('Failed to initiate cart details'));
        } else {
            jQuery('#cart-show-details').modal();
            jQuery('#cart-show-details .message').html();
            jQuery('#cart-show-details .modal-header').html(data.header + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>');
            jQuery('#cart-show-details .modal-body').html(data.html);

        }
    });
}

/**
 *
 * @param self
 */
function showDetailedPackage(self) {
    self = jQuery(self);
    var id = self.parent().data('package-id');
    var element = jQuery('.enlarge.left-direction');
    if (element.length) {
        element.data('activate-id', id);
        element.trigger('click');
    }
}

/**
 *
 * @param action
 */
function togglePromoRules(action) {
    var container = '#promo-rules';
    if(action == 'open') {
        jQuery(container).find('.promo-rules-container').slideDown(function(){
            configurator.assureElementsHeight();
        });
        jQuery(container).removeClass('hidden').addClass('open');
    } else {
        jQuery(container).find('.promo-rules-container').slideUp(function(){
            configurator.assureElementsHeight();
        });
        jQuery(container).addClass('hidden').removeClass('open');
    }
}

/**
 *
 * @param event
 * @param self
 */
function getApplicablePromoRules(event, self) {
    event.stopPropagation();
    var packageId = jQuery(self).parents('.side-cart.block').data('package-id');
    jQuery.post(MAIN_URL + 'pricerules/promo/search', {condition: packageId}, function(response) {
        if(response.error == false) {
            jQuery('#promo-rules-ajax').html(response.rules);
        } else {
            jQuery('#promo-rules-ajax').html(response.message);
        }
        togglePromoRules('open');

    });
}

/**
 *
 * @param element
 */
// @todo refactor these 2 methods: activate and togglePackageConfigurator
function activateBlock(element) {
    var currentPackageId =  jQuery(element).parent().attr('data-package-id');
    var notActive = !jQuery(element).parents('.side-cart.block').hasClass('active');
    if(notActive) {
        if(controller != 'cart') {
            jQuery('div[data-package-type]:visible:not([data-package-id])').remove();
            jQuery('div[data-package-type]:visible').removeClass('active');
            jQuery('.promo-rules-trigger').addClass('hidden');
            jQuery(element).parents('.side-cart').addClass('active');
            initConfigurator(jQuery(element).parents('.side-cart').attr('data-package-type'), jQuery(element).parents('.side-cart').attr('data-package-id'), jQuery(element).parents('.side-cart').attr('data-sale-type'), null);
        } else {
            //var type = jQuery(element).parents('.side-cart').data('package-type');
            var packageId = jQuery(element).parents('.side-cart').data('package-id');
            window.location.href = MAIN_URL + '?packageId=' + packageId;
        }
    }
}

function togglePackageConfigurator(element, activate)
{
    if(activate){
        jQuery('div[data-package-type]:visible:not([data-package-id])').remove();
        jQuery('div[data-package-type]:visible').removeClass('active');
        jQuery('.promo-rules-trigger').addClass('hidden');
        jQuery(element).parents('.side-cart').addClass('active');
    }
    else{
        jQuery(element).parents('.side-cart').removeClass('active');
    }
}

/**
 *
 * @param type
 * @param saleType
 * @param ctn
 * @param isCtnDummyPackage
 * @param current_products
 * @param packageId
 * @returns {*}
 */
function createCartPackageLine(type, saleType, ctn, isCtnDummyPackage, current_products, packageId) {
    isCtnDummyPackage = isCtnDummyPackage || false;
    var sideBlock = jQuery('div[data-package-type="template-' + type + '"]:hidden').first();
    var clone = null;
    if(sideBlock) {
        //make sure no other box is active
        jQuery('div[data-package-type]:visible').removeClass('active').find('.promo-rules-trigger').addClass('hidden');
        clone = sideBlock.clone();

        //remove options that are not allowed
        // if(window[type+'Options']){
        //     clone.find('.row.group-header').each(function(index, element){
        //         if(jQuery.inArray(jQuery.trim(jQuery(element).data('group-type')), jQuery.makeArray(window[type+'Options'])) == -1){
        //             jQuery(element).remove();
        //         }
        //     });
        // }

        if (packageId) {
            clone.attr('data-package-id', packageId)
        }

        if (saleType) {
            // set the sale type to the configurator package
            clone.attr('data-sale-type', saleType)
        }

        if (ctn) {
            clone
                .find('.mark_ctn').html('&nbsp;' + (window.customer ? window.customer.formatPhoneNumberInt(ctn) : ctn))
                .attr('data-rawctn', ctn)
                .end()
                .find('.content').addClass('package-with-ctn')
                .end()
                .find('.box-description').html(createDescription(current_products || ''));

            if (isCtnDummyPackage) {
                clone.addClass('dummy-ctn')
                    .attr('data-package-id', ctn)
                    .attr('id', 'ctn-' + ctn);
            }
        }
        if (saleType) {
            clone.find('.mark').html(Translator.translate(saleType));
        }

        if (type == 'mobile') {
            clone.find('.mark').removeClass('hidden');
        }

        clone.attr('data-package-type', type).show();
        var translation = Translator.translate(type);

        clone.find('.package-type').first().html(translation);
        clone.insertAfter(jQuery('div[data-package-type]').last());
        if (clone.offset()) {
            jQuery('.cart_packages').animate({
                scrollTop: clone.offset().top
            }, 1000);
        }
    }
    return clone;
}

/**
 *
 * @param packageDescription
 * @returns {*}
 */
function createDescription(packageDescription) {
    return jQuery('<span class="package-current-label">{0}: <span class="box-description-ctn">{1}</span></span><br/><span class="package-new-label">{2}</span>'.format(
        Translator.translate('Current'),
        packageDescription,
        Translator.translate('New')
    ));
}

/**
 *
 * @param ctnPackages
 * @param clone
 */
function handleCTNPackage(ctnPackages, clone){
    for (var ctn in ctnPackages) {
        if (ctnPackages.hasOwnProperty(ctn)) {
            if (ctnPackages[ctn]['id']) {
                // dummy ctn
                jQuery('#ctn-'+ctn)
                    .removeClass('dummy-ctn')
                    .removeAttr('id')
                    .attr('data-package-id', ctnPackages[ctn]['id'])
                    .find('.content').addClass('package-with-ctn').find('.box-description')
                    .append(createDescription(ctnPackages[ctn]['description']))
                ;
            } else {
                // current package
                if (clone) {
                    clone.find('.content').addClass('package-with-ctn').find('.box-description')
                        .html(createDescription(ctnPackages[ctn]['description']))
                    ;
                }
            }
        }
    }
}

/**
 *
 * @param param
 * @returns {*}
 */
function getUrlParam(param) {
    var url = window.location.search.substring(1);
    var urlParams = url.split('&');
    for (var i = 0; i < urlParams.length; i++)
    {
        var parameterName = urlParams[i].split('=');
        if (parameterName[0] == param) {
            return parameterName[1];
        }
    }
    return false;
}

/**
 *
 * @param $content
 * @param include_btw
 */
function setUpUI($content, include_btw) {
    window.search = new Search({events: 'change keyup'});
    $content
        // init vat button
        .find('[name="exclInclTaxSwitch"]').each(function (i, _el) {
            var el = jQuery(_el);
            if (!include_btw) {
                el.bootstrapSwitch('state', false, true);
            } else {
                el.bootstrapSwitch();
            }
        })
        .end()
    ;
}

/**
 *
 * @param data
 * @param clone
 * @param type
 */
function callbackInit(data, clone, type){
    if (data.error) {
        showModalError(Translator.translate('Failed to initiate configurator'));
    } else {
        data.products = window.products;
        data.filters = window.filters;

        var content = jQuery('#config-wrapper').find('.content');
        content.get(0).innerHTML = '';

        window.initCart = data.initCart;
        window.sim = data.sim;
        window.prices = data.prices;
        window.allowedProducts = data.allowedProducts;

        if (data.ctnPackages) {
            // add package id to dummy ctn
            handleCTNPackage(data.ctnPackages, clone);
        }

        if (clone) {
            clone.attr('data-package-id', data.packageId);
        }
        window.configurator = new Configurator.Base(data.config);

        setUpUI(content, data.config.include_btw);

        configurator.packagePromoRules(data, data.packageId);

        var clear_filters = jQuery('#config-wrapper #subscription-block .clear-config-filters');
        if (clear_filters.length) {
            clear_filters.trigger('click');
        }
    }
}

/**
 *
 * @returns {boolean}
 */
function supports_calc() {
    if (window.supportsCalc == undefined) {
        var prop = 'width:';
        var value = 'calc(10px);';
        var el = document.createElement('div');
        var prefixes = ['-webkit-', '-moz-', '-o-', '-ms-', '', ''];

        el.style.cssText = prop + prefixes.join(value + prop) + value;
        window.supportsCalc = !!el.style.length;


    }
    return window.supportsCalc;
}

/**
 *
 * @constructor
 */
function CSSCalc() {
    jQuery('div .col3-layout .main .col-main').css('width', '100%').css('width', '-=585px');
    jQuery('div .col3-layout.edit-mode .main .col-main').css('width', '100%').css('width', '-=250px').css('height', '100%').css('height', '-=35px');
    jQuery('div .col3-layout.edit-mode .main .col-main.checkout.bigView').css('width', '100%').css('width', '-=60px');
    jQuery('.edit-mode #cart #cart-content').css('height', '100%').css('height', '-=125px');
    jQuery('#right-panel-cart').css('height', '100%').css('height', '-=30px');
    jQuery('.cart_right_expanded_packages').css('height', '100%').css('height', '-=70px');
    jQuery('.expanded-package-details').css('height', '100%').css('height', '-=230px');
    jQuery('.package-item .description').css('width', '100%').css('width', '-=240px');
}

/**
 * 
 * @param $elem
 * @param width
 */
function animateWidth($elem, width) {
    $elem.animate({
        width: width
    }, 500);
}

/**
 *
 */
function alignRightColumn() {
    var rightColumnButton = jQuery('.enlarge');
    if (rightColumnButton.hasClass('expanded')) {
        var width = jQuery('.main').outerWidth() - (rightColumnButton.outerWidth() * 2) -
            jQuery('.sticker').parent().outerWidth();
        animateWidth(rightColumnButton.parent(), width);
    }
}

/**
 *
 */
function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        window.timeout = false;
        if (!supports_calc()) {
            CSSCalc();
        }
        alignRightColumn();
    }
}

/**
 *
 */
function bindSlidepanelLeft() {
    var $ = jQuery;
    var panel = $('[data-slidebutton-left]');
    if (panel.data('plugin_slidepanel') == null || panel.data('plugin_slidepanel') == undefined) {
        // first bind the AJAX call click
        $('[data-slidebutton-left]').each(function(){
            $(this).on('click', function(e) {
                e.preventDefault();
                if(!$(this).parent().hasClass('active')) {
                    e.stopImmediatePropagation();

                    var trigger = $(this);
                    var targetId = trigger.attr('data-toggle');

                    $.post(MAIN_URL + 'customer/details/showCustomerPanel', {'section': targetId}, function(response) {
                        if(response.error == true) {
                            showModalError(response.message);
                            console.log(response);
                        } else {
                            $('#customer_details_panel .wrapper').html(response.customerPanel);
                            trigger.parent().addClass('active');

                            // proceed with the click
                            trigger.click();
                        }
                    });

                    // hide the slide button form the left container
                    $('.col-left .reduce').hide();
                    // hide the shadow
                    $('.col-left').css('box-shadow',"none");
                } else {
                    // remove active class of siblings
                    $(this).parent().siblings().each(function(){
                        $(this).removeClass('active');
                    });
                }
            });
        });

        $('[data-slidebutton-left]').slidepanel({
            orientation: 'left',
            mode: 'overlay',
            static: true,
            container: '#left-panels',
            before_expand: function(elem) {
                var targetId = $(elem).attr('data-toggle');
                $('#'+ targetId).addClass('active');
                $('.expanded').click();
            },
            after_expand: function(trigger) {
                customer.afterExpandLeft(trigger);
            },
            after_collapse: function(trigger) {
                trigger.parent().removeClass('active');
            }
        });
    }
}

function manualPickup(packageId) {
    var $ = jQuery;
    if ($('.manual_pickup_remarks_div_' + packageId).is(":visible")) {
        $('#pack-' + packageId).find('.manual_pickup_check').removeAttr('checked');
        $('#pack-' + packageId).find('.manual_pickup_check').attr('disabled');
        $('.manual_pickup_remarks_' + packageId).val('');
        $('.manual_pickup_remarks_div_' + packageId).toggleClass('hidden');
    } else {
        $('#pack-' + packageId).find('.manual_pickup_check').attr('checked', 'checked');
        $('#pack-' + packageId).find('.manual_pickup_check').removeAttr('disabled');
        $('.manual_pickup_remarks_div_' + packageId).toggleClass('hidden');
    }
}

function manualActivate(packageId, isRetail) {
    var $ = jQuery;
    var block = $('.manual_activation_remarks_div_' + packageId);
    var simcardInput = block.find('[id*="manual_activation_simcard"]').first();
    if (block.is(":visible")) {
        $('[id="manual_activation_reason[' + packageId + ']"]').val('');
        block.toggleClass('hidden');
        // Disable the validations when the textbox is hidden
        block.find('select').first().removeClass('validate-select');
        simcardInput.removeClass('required-entry validate-sim-cart');
        block.find('.validation-advice').remove();
        block.find('.validation-failed').removeClass('validation-failed');
        block.find('textarea').first().removeClass('required-entry');
    } else {
        block.toggleClass('hidden');
        block.find('select').first().addClass('validate-select');
        simcardInput.addClass('validate-sim-cart');
        if (isRetail) {
            simcardInput.addClass('required-entry');
        }
    }
}

function toggleNotes(packageId) {
    var $ = jQuery;
    if ($('#credit-note-' + packageId).is(":visible")) {
        $('#credit-note-data-' + packageId).val(' ');
    }
    $('#credit-note-' + packageId).toggle();
}

function removeExpandedPackage(id) {
    var $ = jQuery;
    $.post(MAIN_URL + 'configurator/cart/removePackage', {'packageId': id, 'expanded': true}, function (data) {
        if (data.hasOwnProperty('showOneOfDealPopup') && data.showOneOfDealPopup) {
            $('#one-of-deal-modal').data('packageId', id).modal();
            return;
        }
        $('[data-summary-id="' + id + '"]').parent().parent().remove();
        $('#pack-' + id).remove();
        window.hasCompletePackage = data.complete;
        if (data.complete) {
            $('#expanded_checkout_btn').show();
            $('#configurator_checkout_btn').show();
        } else {
            $('#expanded_checkout_btn').hide();
            $('#configurator_checkout_btn').hide();
            // If no packages are complete collapse the right sidebar
            $('.sidebar .enlarge').trigger('click');
        }
        var totals = $('.sidebar #cart_totals');
        if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);

        var identif = '.side-cart.block[data-package-id="' + id + '"]';
        if ($(identif).length > 0 && $(identif).find('.menu-expand').find('li').first().length > 0) {
            $(identif).remove();
        }
        if ($('#incomplete-packages').nextAll().length == 0) {
            $('#incomplete-packages-devider').hide();
            $('#incomplete-packages').hide();
        }

        // Reset packages indexes in right sidebar
        $('.col-right .cart_packages .side-cart.block').each(function(){
            if(data.hasOwnProperty('packagesIndex') && data['packagesIndex'] != null) {
                var initialPackageId = $(this).data('package-id');
                var newPackageId = data['packagesIndex'][initialPackageId];
                $(this).data('package-id', newPackageId);
                $(this).attr('data-package-id',newPackageId);
            }
            $(this).removeClass('active');
        });

        if(undefined !== window['configurator']) {
            // destroy configurator
            configurator.destroy();
        }

        // Refresh the expanded cart content
        $('.col-right #right-panel-cart').remove();
        $('.col-right').append(data.expanded).find('#right-panel-cart').show();
    });
}

function removeOverviewPackage(blockPackageId) {
    var callback = function(data) {
        if (data) {
            jQuery('.cart_packages div[data-package-id=' + blockPackageId + ']').remove();
            jQuery('#pack-' + blockPackageId).remove();
            var totals = jQuery('.sidebar #cart_totals');
            if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
            totals = jQuery('.checkout-totals #cart_totals');
            if(data.hasOwnProperty('totals')) totals.replaceWith(data.totals);
            jQuery('.sidebar #configurator_checkout_btn').remove();
            jQuery('.checkout-totals #configurator_checkout_btn').remove();
        }
    };
    if (blockPackageId) {
        jQuery.post(MAIN_URL + 'configurator/cart/removePackage', {'packageId': blockPackageId}, function (data) {
            if (!data.complete) {
                jQuery(location).attr('href', MAIN_URL);
            } else {
                jQuery(location).attr('href', MAIN_URL + 'checkout/cart');
            }
            // Temporary removed, just redirect to the checkout
            // callback(data);
        }).fail(function() {
            showModalError(Translator.translate('Failed to remove package'));
        });
    }
}

function activateOverviewBlock(blockPackageId) {
    if(blockPackageId) {
        jQuery(location).attr('href', MAIN_URL + '?packageId=' + blockPackageId);
    }
}

function switchPackageMenu(event, self) {
    event.stopPropagation();
    self = jQuery(self);
    if(!self.hasClass('open')) {
        jQuery('.col-right .block-container').find('div').removeClass('open');
        jQuery('.checkout-package-details .package-expand').find('div').removeClass('open');
    }
    self.toggleClass('open');
    self.siblings('.menu-expand').first().toggleClass('open');
    if(!self.siblings('.menu-expand').first().hasClass('top-menu')){
        jQuery('.menu-expand.top-menu').removeClass('open');
        jQuery('.drop-menu.top-menu').removeClass('open');
    }
}

// add padding to the package details container to make scrolling work better
function addPaddingToPackageDetailsContainer(){
    var $ = jQuery;
    if ($('.expanded-package-details .package-expand').length > 1) {
        var containerHeight = $('.expanded-package-details').height();
        var lastPackageHeight = $('.expanded-package-details .package-expand').last().height();
        var paddingToBeAdded = containerHeight - lastPackageHeight - 10; //get some padding off
        $('.expanded-package-details').css('padding-bottom',paddingToBeAdded);
    }
    return;
}

function appendSplit(item){
    jQuery(item).append('<input type="hidden" name="split" value="true" />');
}

function goToCheckout() {
    if (!window.cartForm.validator.validate()) {
        return false;
    }
    if (window.checkoutEnabled === false) {
        showModalError(Translator.translate('Customer data is still being retrieved'));
        return false;
    }
    jQuery.post(MAIN_URL + 'checkout/cart/cartPackages', function(data) {
        if (data.error) {
            showModalError(data.message);
        } else {
            if(data.show) {
                var id = '#cart-packages-modal';
                jQuery(id).remove();
                jQuery('body').append(data.modal);
                jQuery(id).modal();
            } else {
                jQuery('#expanded_form').submit();
                jQuery('#spinner').addClass('importantDisplayBlock');
            }
        }
    });
}

function removeCtn(element) {
    var el = jQuery(element).parents('.ctn');
    var ctn = jQuery.trim(el.find('span').text());
    var packageId = el.data('package-id');
    var activeBox = jQuery('.package-summary.active').data('summary-id');
    var endpoint = MAIN_URL + 'checkout/cart/removeCtn';

    jQuery.post(
        endpoint
        , { 'ctn' : ctn, 'packageId' : packageId }
    )
    .then(function(data, status) {
        jQuery('#right-panel-cart').replaceWith(data['right_sidebar']);
        jQuery('#right-panel-cart').show();
        return data;
    })
    .then(function(data) {
        var packageSummary = jQuery('.package-summary[data-summary-id="'+activeBox+'"]');
        var pack = jQuery('#pack-'+packageId);
        packageSummary.trigger('click');
        pack.find('a[href="#ctns-'+packageId+'"]').trigger('click');
        jQuery('#cart_totals').replaceWith(data['totals']);
        jQuery('div.side-cart[data-package-id="' + packageId + '"]').first().replaceWith(data['rightBlock']);
    })
    .fail(function(xhr, message) {
        showModalError(Translator.translate('Failed to remove CTN'));
    });
}

function searchStores() {
    var keyword = jQuery.trim(jQuery('#srch-term').val());
    var reg = new RegExp(window.search.escapeValue(keyword), 'ig');
    var stores = jQuery('#storesFound').find('tr');
    if (keyword.length) {
        stores.each(function() {
            if (jQuery(this).find('td').text().match(reg)) {
                jQuery(this).show();
            } else {
                jQuery(this).hide();
            }
        });
    } else {
        stores.show();
    }
}

function handleEnterKey(event) {
    if(event.keyCode === 13) {
        searchStores();
    }

    return false;
}

function toggleDropdownButton() {
    jQuery('#cart-overview [data-toggle=dropdown]').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (jQuery(this).parent().hasClass('open')){
            jQuery(this).parent().removeClass('open');
        }else{
            jQuery('.cart_overview').find('.btn-group.open').removeClass('open');
            jQuery(this).parent().addClass('open');
        }
    });
}

function setMaxHeightCTNOverview(){
    var heightDetailsButtons = jQuery('.ctn-detail-buttons').height();
    var offsetTopOverview = jQuery('.ctn_overview').offset().top;
    var windowHeight = jQuery(window).height();
    var maxHeight = windowHeight - heightDetailsButtons - offsetTopOverview - 56; //minus padding
    jQuery('.ctn_overview').css('max-height',maxHeight);
}

function expandCtnDetails(element, className) {
    var el = jQuery(element);
    var detailsEl = el.parent().parent().find(className);
    if (detailsEl.length) {
        detailsEl.toggle();
        var state = 'opened';
        if ( ! detailsEl.is(':visible')) {
            state = 'closed';
        }
        el.text(el.data(state));
    }
}

function closeOrder() {
    var $ = jQuery;
    $('#orders-content #open-order').hide();
    $('#orders-content #orders-overview').show();
    $('#orders-content .tab-title').show();
    return false;
}

function activateCart(quote_id) {
    jQuery.post(MAIN_URL + 'checkout/cart/activateCart', {quote_id: quote_id}, function(response) {
        if(response.error == false) {
            console.log(response);
            window.location.reload();
        }
    });
}

function parseDate(input) {
    var parts = input.match(/(\d+)/g);
    return new Date(parts[2], parts[1]-1, parts[0], parts[3], parts[4], parts[5]); //     months are 0-based
}

function sortCtnList(orderBy, element) {
    if(jQuery(element).hasClass('arrow_asc')) {
        jQuery(element).removeClass('arrow_asc');
        jQuery(element).addClass('arrow_desc');
    } else{
        jQuery(element).removeClass('arrow_desc');
        jQuery(element).addClass('arrow_asc');
    }
    var elems = jQuery.makeArray(jQuery(".ctn_overview").find('ul'));
    elems.sort(function(a, b) {
        if (orderBy == 'date') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (parseDate(jQuery(a).find('li').eq(2).text() + ' 00:00:00') > parseDate(jQuery(b).find('li').eq(2).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (parseDate(jQuery(b).find('li').eq(2).text() + ' 00:00:00') > parseDate(jQuery(a).find('li').eq(2).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }

        if (orderBy == 'nr') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (jQuery(a).find('li').eq(1).text() > jQuery(b).find('li').eq(1).text()) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (jQuery(b).find('li').eq(1).text() > jQuery(a).find('li').eq(1).text()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    });
    jQuery(".ctn_overview").html(elems);
}

function sortOrderList(orderBy, element) {
    if(jQuery(element).hasClass('arrow_asc')) {
        jQuery(element).removeClass('arrow_asc');
        jQuery(element).addClass('arrow_desc');
    } else {
        jQuery(element).removeClass('arrow_desc');
        jQuery(element).addClass('arrow_asc');
    }

    var elems = jQuery.makeArray(jQuery("#order_overview_list").find('ul'));
    elems.sort(function (a, b) {
        if (orderBy == 'date') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (parseDate(jQuery(a).find('li').eq(0).text() + ' 00:00:00') > parseDate(jQuery(b).find('li').eq(0).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (parseDate(jQuery(b).find('li').eq(0).text() + ' 00:00:00') > parseDate(jQuery(a).find('li').eq(0).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }

        if (orderBy == 'nr') {
            return jQuery(element).hasClass('arrow_asc')
                ? jQuery(a).find('li').eq(1).text() - jQuery(b).find('li').eq(1).text()
                : jQuery(b).find('li').eq(1).text() - jQuery(a).find('li').eq(1).text();
        }

        if (orderBy == 'status') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (jQuery(a).find('li').eq(2).text() > jQuery(b).find('li').eq(2).text()) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (jQuery(b).find('li').eq(2).text() > jQuery(a).find('li').eq(2).text()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    });
    jQuery("#order_overview_list").html(elems);
}

function sortCartList(orderBy, element) {
    if (jQuery(element).hasClass('arrow_asc')) {
        jQuery(element).removeClass('arrow_asc');
        jQuery(element).addClass('arrow_desc');
    } else {
        jQuery(element).removeClass('arrow_desc');
        jQuery(element).addClass('arrow_asc');
    }

    var elems = jQuery.makeArray(jQuery("#carts-content .cart_overview").find('> ul'));
    elems.sort(function (a, b) {
        if (orderBy == 'date') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (parseDate(jQuery(a).find('li').eq(1).text() + ' 00:00:00') > parseDate(jQuery(b).find('li').eq(1).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (parseDate(jQuery(b).find('li').eq(1).text() + ' 00:00:00') > parseDate(jQuery(a).find('li').eq(1).text() + ' 00:00:00')) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }

        if (orderBy == 'maker') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (jQuery(a).find('li').eq(2).text() > jQuery(b).find('li').eq(2).text()) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (jQuery(b).find('li').eq(2).text() > jQuery(a).find('li').eq(2).text()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }

        if (orderBy == 'status') {
            if (jQuery(element).hasClass('arrow_asc')) {
                if (jQuery(a).find('li').eq(0).text() > jQuery(b).find('li').eq(0).text()) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if (jQuery(b).find('li').eq(0).text() > jQuery(a).find('li').eq(0).text()) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    });
    jQuery("#carts-content .cart_overview").html(elems);
    toggleDropdownButton();
}

function deleteQuote(quoteId, type) {
    var modal = jQuery('#remove-saved-quote-modal');
    modal.data('quote-id', quoteId).appendTo('body');
    var body = '.modal-body p.' + type;
    modal.find('.modal-body p').hide();
    modal.find(body).show();
    modal.modal();
}

function deleteSavedQuote(quoteId) {
    if (checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        jQuery('#checkout_deliver_confirmation').data('function', 'deleteSavedQuote').data('param1', quoteId).appendTo('body').modal();
        return false;
    }

    var url = MAIN_URL + 'configurator/cart/deleteSavedQuote';
    jQuery.post(url, {quote_id: quoteId}, function (data) {
        if (data.error) {
            showModalError(data.message);
        } else {
            window.location.reload();
        }
    });
}

function checkCanAddToCart(elem) {
    var el = jQuery(elem);
    var isChecked = el.length && el.prop('checked');
    el.length && el.parent().removeClass('selected'); // only if elem exists

    if (isChecked) {
        jQuery('.ctn_actions').find('input[type="button"]').prop('disabled', false);
        el.length && el.parent().addClass('selected');
    } else {
        // check if any of the checkboxes is checked
        var checkboxesList = jQuery('.ctn_record .ctn_checkbox');
        var hasAnyRetainable = false;
        var listLength = checkboxesList.length;

        for (var i = 0; i < listLength; ++i) {
            if (checkboxesList.eq(i).prop('checked')) {
                hasAnyRetainable = true;
                break; // one checked .. break
            }
        }

        jQuery('.ctn_actions').find('input[type="button"]').prop('disabled', !hasAnyRetainable);
    }
}

function confirmOffer(quoteId) {
    jQuery('#confirm-offer').data('quoteId', quoteId).modal();
}

function confirmRisk(quoteId) {
    jQuery('#confirm-riskorder').data('quoteId', quoteId).modal();
}

function reloadQuote(quoteId) {
    if(checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        jQuery('#checkout_deliver_confirmation').data('function','reloadQuote').data('param1', quoteId).appendTo('body').modal();
        return false;
    }

    //if retrieveCustomerInfo is not yet finalized, don't allow agent to continue
    if (!window.checkoutEnabled) {
        showModalError(Translator.translate('Customer data are not yet loaded from the Dealer Adapter services'));
        return;
    }

    if (jQuery('#is_order_edit_mode').val() == 1) {
        showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
        return;
    } else {
        jQuery.post(MAIN_URL + 'checkout/cart/activateCart', {quote_id: quoteId}, function (data) {
            if (data.error) {
                showModalError(data.message);
            } else {
                window.location.href = MAIN_URL + '?packageId=' + data.packageId;
            }
        });
    }
}

function reloadOffer(element) {
    if (checkoutDeliverPageDialog == true) {
        // If agent tries to load a customer via advanced search while on Checkout delivery show confirmation modal
        jQuery('#checkout_deliver_confirmation').data('function', 'reloadOffer').appendTo('body').modal();
        return false;
    }
    var quoteId = jQuery(element).data('quoteId');
    if (jQuery('#is_order_edit_mode').val() == 1) {
        showModalError(Translator.translate('You can not renew an order because you are changing and order. You have to cancel your changes first and go to the configurator.'));
        return;
    } else {
        jQuery.post(MAIN_URL + 'checkout/cart/activateCart', {quote_id: quoteId, is_offer: 1}, function (data) {
            jQuery('#confirm-offer').modal('hide');
            if (data.error) {
                showModalError(data.message);
            } else {
                window.location.href = MAIN_URL + 'checkout/cart';
            }
        });
    }
}

function addTabFunctionUserInfo(section){
    var iframeBaseUrl = window[section + 'IframeUrl'];
    jQuery('.user-info-nav-buttons .btn').on('click', function(){
        if (jQuery(this).hasClass('active')) {
            return false;
        }
        jQuery(this).parent().find('.active').removeClass('active');
        jQuery(this).addClass('active');
        var CTNNumber = jQuery(this).data('phone');

        if (section == 'nba') {
            var iframeUrl = iframeBaseUrl.replace('{ctn}', CTNNumber);
            window.open(iframeUrl);
        } else {
            var iframeUrl = iframeBaseUrl + CTNNumber;
            jQuery('.user-info-iframe').attr('src',iframeUrl).removeClass('hidden');
        }
    });
    if (section != 'nba') {
        jQuery('.user-info-nav-buttons .btn').first().click();
    }
}

function createPackages() {
    var ctnCheckboxes = jQuery('.ctn_checkbox:checked');
    if (ctnCheckboxes.length > 0) {
        var ctns = '';
        var sale_type = 'inlife';
        ctnCheckboxes.each(function (index, element) {
            var el = jQuery(element);
            if (!el.is(':disabled')) {
                if (ctns != '') ctns += ',';
                // ctns += jQuery('p[for="' + jQuery(element).prop('id') + '"]').first().html();
                ctns += el.data('ctn');
                if (el.parent('.ctn_record').find('.retainable:visible').length > 0) {
                    sale_type = 'retentie';
                }
            }
        });
        createPackage(ctns, sale_type);
    } else {
        showModalError(Translator.translate('Please select at least one CTN before continuing'));
    }
}

function proceedToCheckout() {
    var simCardOnly = simCardChecker.isSimOnlyWithoutSim();
    if (!simCardOnly) { return false; }

    setPageLoadingState(true);
    window.location.href = MAIN_URL + 'checkout/cart/saveEditedQuote';

    return false;
}

function checkPackage(button, orderNumber) {
    var $ = jQuery;
    if (!orderNumber) {
        return;
    }
    button = $(button);
    button.addClass('hide');
    button.parent().find('.open-orders-spinner').removeClass('hide');
    toggleOrderDetails(button.parents('tr'), true);
    var oldLoader = window.manualLoader;
    window.manualLoader = true;
    $.ajax({
        async: true,
        type: 'POST',
        url: MAIN_URL + 'superorder/index/checkPackage',
        data: {orderNumber: orderNumber},
        success: function (response) {
            button.parent().find('.open-orders-spinner').addClass('hide');
            button.removeClass('hide');
            if (!response.error) {
                $.each(response.html, function (id, el) {
                    $('tr[data-id="' + orderNumber + '.' + id + '"] .order-edit-buttons').html(el);
                });
            } else {
                showModalError(response.message);
            }
        },
        error: function () {
            button.removeClass('hide');
            button.parent().find('.open-orders-spinner').addClass('hide');
        }
    });
    window.manualLoader = oldLoader;
}

function editPackage(packageId, isDelivered) {
    var id = isDelivered ? 'edit_delivered_package' : 'edit_not_delivered_package';
    var $modalDiv = jQuery('#' + id);
    $modalDiv.data('packageId', packageId);

    if (isDelivered) {
        jQuery('#edit_delivered_package .modal-body #package_hardware_items').addClass('hidden').empty();
        $modalDiv.find('#refund_reason_change').attr('name', 'refund_reason').removeClass('hidden');
        $modalDiv.find('#refund_reason_cancel').attr('name', '').addClass('hidden');
        // Reset form values for delivered packages
        if ($modalDiv.children('form').length) {
            $modalDiv.children('form')[0].reset();
        }
        $modalDiv.data('isCancel', 0).find('#package_hardware_items').addClass('hidden').empty();
        $modalDiv.find('.modal-body .change-text').show();
        $modalDiv.find('#refund_reason_change').trigger('change');
    }

    $modalDiv.appendTo('body').modal();
}

// Append the right method to the modal and execute the command
function appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId) {
    var modal = jQuery('#top_menu_lock_modal').first();
    modal.data('function', myFunc);
    modal.data('params', myParams);
    modal.data('orderId', orderId);
    modal.data('orderNumber', orderNumber);
    modal.data('customerId', customerId);
    modal.appendTo('body').modal();
}

function cancelOneOffPackages() {
    jQuery('#remove-one-of-deal-order-modal').modal();
}


function cancelDeliveredPackage(packageId) {
    // Because we share the same modal with Edit action, hide the edit text
    jQuery('#edit_delivered_package .modal-body #package_hardware_items').addClass('hidden').empty();
    jQuery('#edit_delivered_package .modal-body .change-text').hide();
    jQuery('#edit_delivered_package #refund_reason_cancel').attr('name', 'refund_reason').removeClass('hidden');
    jQuery('#edit_delivered_package #refund_reason_change').attr('name', '').addClass('hidden');
    jQuery('#edit_delivered_package').data('isCancel', 1).data('packageId', packageId).appendTo('body').modal();
    jQuery('#edit_delivered_package #refund_reason_cancel').trigger('change');
}

function cancelPackage(packageId, oneOff) {
    if (customer) {
        customer.setLoading();
    }

    if (oneOff) {
        packageId = jQuery('#all-package-ids').val();
    }

    window.location.href = MAIN_URL + 'checkout/cart/removePackage?packageId=' + packageId;

    if (oneOff) {
        jQuery('#remove-one-of-deal-order-modal').modal('hide');
    }
}

function editTopMenuPackage(orderNumber, orderId, packageId, isDelivered, customerId) {
    var myFunc = 'editPackage';
    var myParams = [packageId, isDelivered];
    appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId);
}

function cancelTopMenuPackage(orderNumber, orderId, packageId, type, customerId) {
    var myFunc;
    var myParams;
    switch (type) {
        case '1':
            myFunc = 'cancelOneOffPackages';
            myParams = [];
            break;
        case '2':
            myFunc = 'cancelDeliveredPackage';
            myParams = [packageId];
            break;
        case '3':
            myFunc = 'cancelPackage';
            myParams = [packageId];
            break;
        default:
            myFunc = '';
            myParams = [];
    }
    appendDataToModal(myFunc, myParams, orderNumber, orderId, customerId);
}

function loadTopMenuOrder($, orderNumber, orderId, myFunc, myParams) {
    // enter order edit mode:
    $.ajax({
        async: true,
        type: 'get',
        url: MAIN_URL + 'checkout/cart/editSuperorder',
        data: {superorderId: orderNumber, redirect: 0},
        success: function (response) {
            // Once confirmed, lock the order: 
            $.ajax({
                async: true,
                type: 'post',
                url: MAIN_URL + 'checkout/cart/lockSuperorder',
                data: {state: "unlocked", orderId: orderId},
                success: function (response) {
                    if (response.error) {
                        $.ajax({
                            async: true,
                            type: 'get',
                            url: MAIN_URL + 'checkout/cart/cancelEditOrder',
                            data: {redirect: 0}
                        });
                        showModalError(response.message);
                    } else {
                        // redirect to the correct url:
                        window[myFunc].apply(this, myParams);
                    }
                },
                error: function () {
                }
            });
        },
        error: function () {
        }
    });
}

function confirmTopMenu(modal) {
    var $ = jQuery;
    modal = $(modal).parents('.modal').first();
    var myFunc = modal.data('function');
    var myParams = modal.data('params');
    var orderId = modal.data('orderId');
    var orderNumber = modal.data('orderNumber');
    var customerId = modal.data('customerId');
    var oldLoader = window.manualLoader;
    // Force exit order edit mode
    $.ajax({
        async: true,
        type: 'get',
        url: MAIN_URL + 'checkout/cart/cancelEditOrder',
        data: {redirect: 0}
    }).done(function() {
        var callback = function () {
            var refreshCallback = function () {
                loadTopMenuOrder.call(this, $, orderNumber, orderId, myFunc, myParams);
            };
            return customer.poolRefreshData(refreshCallback);
        };

        // If the logged in customer is not the same as the customer to which the order belongs, we need to log him 
        if (customerId != customer.getLoggedInCustomer()) {
            customer.loadCustomerData(customerId, false, false, callback, true);
        } else {
            loadTopMenuOrder.call(this, $, orderNumber, orderId, myFunc, myParams);
        }
        window.manualLoader = oldLoader;
    });
}

function getPackageHardware(obj) {
    var $ = jQuery;
    // TODO: cache mechanism?!
    var $container = $(obj).parents('#edit_delivered_package');
    var selectedValue = $(obj).find(':selected').val();
    if (selectedValue == 'ONTEVREDEN') {
        $container.find('#package_hardware_items').addClass('hidden').empty();
        return;
    }
    setPageOverrideLoading(true);
    $container.find('.loader').toggleClass('hide');

    var packageId = $container.data('packageId');

    $.post(MAIN_URL + 'checkout/cart/getPackageHardware', {'package_id': packageId}, function (results) {
        if (!results.error) {
            var html = '';
            if (results.data && Object.keys(results.data).length > 0) {
                html += '<h4>' + Translator.translate('Please select the damaged products') + ':</h4><hr />';
                if (results.notice.length > 0) {
                    $container.find('.notice-hardware').removeClass('hidden');
                }
                $.each(results.data, function (sku, name) {
                    var classGrey = '';
                    var deleted = '';

                    if ($.inArray(sku, results.notice) > -1) {
                        classGrey = 'grey';
                        deleted = 'deleted';
                    }

                    html += ('<p class="{0}"><label class="checkbox-inline"><input type="checkbox" class="{2}" name="item_doa[]" value="{3}" /><span class="doa">' + Translator.translate('DOA') + ':</span> {1}</label></p>').format(classGrey, name, deleted, sku);
                });
            } else {
                html = Translator.translate('No hardware items found in the package');
            }

            $container.find('#package_hardware_items').removeClass('hidden').html(html);
        } else {
            $container.find('#package_hardware_items').removeClass('hidden').html(message);
        }
        $container.find('.loader').toggleClass('hide');
    });
    setPageOverrideLoading(false);
}

function beforeEditSuperorderPackage(obj) {
    var $ = jQuery;
    setPageLoadingState(true);
    var $container = $(obj).parents('#edit_delivered_package');
    var packageId = $container.data('packageId');
    var isCancel = $container.data('isCancel');

    var checkedItems = $('[name="item_doa[]"].deleted:checked').length > 0;
    if (packageId) {
        var formData = $.extend($container.children('form').serializeObject(), {'package_id': packageId});
        $.post(MAIN_URL + 'checkout/cart/beforeEditSuperorderPackage', formData, function (data) {
            var controllerAction = isCancel ? 'removePackage?' : 'editSuperorderPackage?';
            controllerAction = (checkedItems && !isCancel) ? 'editSuperorderPackage?force=1&' : controllerAction;
            window.location.href = MAIN_URL + 'checkout/cart/' + controllerAction + 'packageId=' + packageId;
        });
    }
}

function startPiwikCall(url) {
    if (window._paq !== undefined) {
        var d = new Date();
        if (window.timers === undefined) {
            window.timers = [];
        }
        window.timers[url] = d.getTime();
    }
}

function endPiwikCall(url) {
    if (window._paq !== undefined && window.timers[url] !== undefined) {
        var d = new Date();

        var match = url.replace(MAIN_URL, '');

        var params = match.split('?');
        var path = params[0].split('/');

        var eventCategory = path[0];
        var eventAction = path[1];
        var eventName = path[2];
        var eventValue = d.getTime() - window.timers[url];
        var secondaryDimension = {time: d.getTime(), params: params[1]};

        window._paq.push(['trackEvent', eventCategory, eventAction, eventName, eventValue, secondaryDimension]);
        delete window.timers[url];
    }
}
