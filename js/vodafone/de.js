(function ($) {
    window.LeftSidebar = function () {
        var alignLeftColumn = function () {
                var leftColumnButton = $('.enlargeSearch');
                var difference = $('.col-right').outerWidth();
                var distance = $('.main').outerWidth() - (leftColumnButton.outerWidth() * 2);
                var panelDefaultWidth = 221;

                var animateWidthLength = distance - difference;
                if (distance - difference < panelDefaultWidth) {
                    animateWidthLength = panelDefaultWidth
                }

                var leftSide = leftColumnButton.parent();
                leftSide.animate({width: animateWidthLength}, 500, function () {
                    var px = getAlignLinkedBorderImageFromProductDetails();
                    $(".account-action-linked").css("cssText", "left: " + px + "px !important;");
                });

                // the height of the table after window resize: the window height - the header height - the top padding
                var colLeft = $('.col-left');
                var paddingTop = (colLeft.css('padding-top')) ? (colLeft.css('padding-top')).replace("px", "") : 0;
                var tableHeight = $(window).height() - $('#header').height() - paddingTop - 30;
                var scrollableTable = $('.scrollable-y-table');
                // add the vertical scroll if the window size is smaller than the max-width of the table
                var maxTableHeight = (scrollableTable.css('max-height')) ? scrollableTable.css('max-height').replace("px", "") : 0;
                (tableHeight < maxTableHeight) ? scrollableTable.addClass('scroll-y') : scrollableTable.removeClass('scroll-y');
                scrollableTable.animate({
                    height: tableHeight
                }, 500);

            },
            toggleLeftSideBar = function (stayOpen) {
                var direction = '';
                var enlargeButton = $('.enlargeSearch');
                // if a search is done and the left side bar is already expanded: do nothing
                if (stayOpen && $(enlargeButton).hasClass('expanded')) {
                    alignLeftColumn();
                    return;
                }
                if ($(enlargeButton).hasClass('left-direction')) {
                    direction = 'left';
                    var leftSide = $('.col-right').outerWidth();
                    $(enlargeButton).removeClass('left-direction');
                    $(enlargeButton).addClass('right-direction');
                }
                else {
                    direction = 'right';
                    var rightSide = $('.col-right').outerWidth();
                    $(enlargeButton).removeClass('right-direction');
                    $(enlargeButton).addClass('left-direction');
                }

                var panelDefaultWidth = 221;
                var customerDetailsPage = $('.customer_ctns');
                // contract - show only the sticker
                if ($(enlargeButton).hasClass('expanded')) {
                    var self = $(enlargeButton);
                    $(enlargeButton).parent().find('.panel-content').fadeOut({
                        queue: false,
                        duration: 500,
                        complete: function () {
                            self
                                .siblings('.sticker')
                                .fadeIn(500);
                        }
                    });
                    $(enlargeButton).parent().animate({
                        width: panelDefaultWidth
                    }, 500);
                    $('.col-main').fadeIn({queue: true, duration: 500});

                    $(enlargeButton).removeClass('expanded').addClass('contracted');
                    // if is on the customer details page -> add border to the ctn blocks
                    if (customerDetailsPage) {
                        customerDetailsPage.find('li:first').removeClass('customer_cnts_li_first_child_expanded');
                        customerDetailsPage.find('li:last').removeClass('customer_cnts_li_last_child_expanded');
                        customerDetailsPage.find('li').removeClass('customer_cnts_li_expanded');
                        customerDetailsPage.find('li').removeClass('active');
                    }

                } else {
                    var buttonExpanded = $('button.expanded');
                    //if the other panel is already expanded, contract it back
                    if (buttonExpanded.length > 0) {
                        buttonExpanded.first().trigger('click');
                    }

                    alignLeftColumn();
                    $('.col-main').fadeOut({queue: true, duration: 500});
                    $(enlargeButton).parent().find('.panel-content').fadeIn({
                        queue: false,
                        duration: 500
                    });

                    $(enlargeButton).removeClass('contracted').addClass('expanded');
                    // if is on the customer details page -> add border to the ctn blocks
                    if (customerDetailsPage) {
                        customerDetailsPage.find('li').addClass('customer_cnts_li_expanded');
                        customerDetailsPage.find('li').first().addClass('customer_cnts_li_first_child_expanded');
                        customerDetailsPage.find('li').last().addClass('customer_cnts_li_last_child_expanded');
                    }
                }
            },
            resizeLeftPanel = function () {
                if (new Date() - rtime < delta) {
                    setTimeout(resizeLeftPanel, delta);
                } else {
                    window.timeout = false;
                    if (!supports_calc()) {
                        CSSCalc();
                    }
                    if ($('.enlargeSearch').hasClass('expanded')) {
                        alignLeftColumn();
                    }
                }
            },
            getAlignLinkedBorderImageFromProductDetails = function () {
                var linkedImageTd = $('.account-action-linked-cell');
                if (linkedImageTd) {
                    return linkedImageTd.width();
                }
                return 0;
            };
        return {
            resizeLeftPanel: resizeLeftPanel,
            toggleLeftSideBar: toggleLeftSideBar
        }
    }();
    window.CustomerFiltersSearch = function () {
        /**
         * when the advanced search is off:
         * - the advanced search input fields must be hidden;
         * - the bullet must in "off-state" (meaning it is now grey)
         * - the value of the radio is "0"
         */
        var offStateHandleAdvancedSearch = function () {
                var customerSearchForm = $('#customer_search_form');
                var onAdvancedClass = ".on-advanced";
                // hide the fields that are available only when advanced search is enabled
                customerSearchForm.find(onAdvancedClass).addClass('hide');
                customerSearchForm.find(onAdvancedClass).removeClass('show');
                $('.advanced-search-label').addClass('off-state');
                $('#customer_search').find('.advanced-search-switch').find('.toggle-button').data('state', true).addClass('off-state').removeClass('on-state');
                $('#advanced-search').val(0);
            },
            /**
             * when the legacy search is off:
             * - all the initial input set must be enabled
             * - the legacy radio buttons (mobile, dsl, cable) must be hidden
             * - the legacy search input fields must be hidden;
             * - the bullet must in "off-state" (meaning it is now grey)
             * - the value of the radio is "0"
             */
            offStateHandleLegacySearch = function () {
                // set all the input fields to be enabled
                enableAllInputFields();
                // hide the radio buttons for mobile,dsl, cable
                $('#legacy-search-filters').hide();
                // show the label as disabled
                $('.legacy-search-label').addClass('off-state');
                var customerSearchForm = $('#customer_search_form');
                var onLegacyFilters = ".on-legacy-filter";
                // hide all the additional legacy filters
                customerSearchForm.find(onLegacyFilters).addClass('hide');
                customerSearchForm.find(onLegacyFilters).removeClass('show');
                $('#customer_search').find('.legacy-search-switch').find('.toggle-button').data('state', true).addClass('off-state').removeClass('on-state');
                $('#legacy-search').val(0);
            },
            /**
             * when the advanced search is on:
             * - set the legacy to off (the advanced and legacy are mutually exclusive)
             * - show the specific advanced search fields
             * - the bullet must in "on-state" (meaning it is now green)
             * - the value of the radio is "1"
             */
            onStateHandleAdvancedSearch = function () {
                // set the legacy to off
                offStateHandleLegacySearch();
                var customerSearchForm = $('#customer_search_form');
                var onAdvancedValueShowClass = ".on-advanced";
                // show the specific advanced filters inputs
                customerSearchForm.find(onAdvancedValueShowClass).addClass('show');
                customerSearchForm.find(onAdvancedValueShowClass).removeClass('hide');
                $('.advanced-search-label').removeClass('off-state');
                $('#customer_search').find('.advanced-search-switch').find('.toggle-button').data('state', false).addClass('on-state').removeClass('off-state');
                $('#advanced-search').val(1);
            },
            /**
             * when the legacy search is on:
             * - set the advanced to off (the advanced and legacy are mutually exclusive)
             * - show the radio buttons for legacy search: mobile, dsl, cable
             * - the bullet must in "on-state" (meaning it is now green)
             * - the value of the radio is "1"
             * - check if the radio button for legacy is already marked (there is already selected a mobile, dsl or cable) and show/disable specific fields
             */
            onStateHandleLegacySearch = function () {
                offStateHandleAdvancedSearch();
                $('#legacy-search-filters').show();
                $('.legacy-search-label').removeClass('off-state');
                $('#customer_search').find('.legacy-search-switch').find('.toggle-button').data('state', false).addClass('on-state').removeClass('off-state');
                $('#legacy-search').val(1);
                showHideDisableLegacySpecificFilters();
            },
            /**
             * when the input fields have value
             * - the background is greenish
             * - the reset link is available for use
             * else:
             * - the reset link is hidden
             * - no background is present on the input
             */
            removeOrAddFilterInputsStyles = function () {
                var input = $(".customer-search-form input.form-control");
                if (input.map(function (idx, elem) {
                        if ($(elem).val() != "") {
                            $(elem).addClass('input-background');
                            return $(elem);
                        }
                        else {
                            $(elem).removeClass('input-background');
                        }
                    }).size() > 0) {
                    $(".reset-search").show();
                } else {
                    $(".reset-search").hide();
                }
            },
            /**
             * on refresh initialize the advanced search:
             * - must be in off state
             */
            initAdvancedSearch = function () {
                offStateHandleAdvancedSearch();
            },
            /**
             * on refresh initialize the legacy search:
             * - must be in off state
             */
            initLegacySearch = function () {
                offStateHandleLegacySearch();
            },
            /**
             * on change advanced search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            onSwitchAdvancedSearchChange = function (elem) {
                enableAllInputFields();
                ($(elem).data('state') == true) ? onStateHandleAdvancedSearch() : offStateHandleAdvancedSearch();
            },
            /**
             * on change legacy search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            onSwitchLegacySearchChange = function (elem) {
                enableAllInputFields();
                ($(elem).data('state') == true) ? onStateHandleLegacySearch() : offStateHandleLegacySearch();
            },
            /**
             * on change legacy search toggle treat the off/on state
             * - start with a clean set of input fields: set them all on enabled
             */
            toggleResetFiltersEvent = function () {
                //if any input is not blank, show the link
                $(".customer-search-form input.form-control").bind('change keyup', function (el, index) {
                    removeOrAddFilterInputsStyles();
                });
            },
            /**
             * initialize the customer filters
             */
            initCustomerFilters = function () {
                enableAllInputFields();
                initAdvancedSearch();
                initLegacySearch();
                removeOrAddFilterInputsStyles();
                toggleResetFiltersEvent();
            },
            /**
             * reset all input in the filters
             */
            resetFilters = function () {
                // remove all values from filters
                $('.customer-search-form').find('input[type=text], textarea').val('');
                removeOrAddFilterInputsStyles();
            },
            /**
             * when dls, cable or mobile is clicked from the legacy filters we should disable specific fields and show new specific fields
             * - first we enable all the input fields and after we apply the logic for the legacy option selected
             */
            showHideDisableLegacySpecificFilters = function() {
                // the legacy option selected: mobile,dsl or cable
                var legacyValue =  $('input[name=legacy_type]:checked', '#customer_search_form').val();
                // the fields with this class should be disabled for this legacy option (these are fields from the initial search inputs that are not available for this legacy search)
                var onLegacyValueDisableClass = ".on-"+ legacyValue + "-disable";
                // the fields with this class should be shown for this legacy option (these are fields available only for this legacy)
                var onLegacyValueShowClass = ".on-"+ legacyValue + "-show";
                // the fields with this class should be hidden for this legacy option (these are fields that are available for other legacies, not for the one selected now)
                var onLegacyValueHideClass = ".on-"+ legacyValue + "-hide";
                var customerSearchForm = $('#customer_search_form');

                // enable all input fields
                enableAllInputFields();
                // disable the fields from the initial set of inputs that are not available for this legacy
                customerSearchForm.find(onLegacyValueDisableClass).find('input').attr('disabled', true);
                // show the specific legacy fields
                customerSearchForm.find(onLegacyValueShowClass).addClass('show');
                customerSearchForm.find(onLegacyValueShowClass).removeClass('hide');
                // hide the fields that were available for other legacies
                customerSearchForm.find(onLegacyValueHideClass).addClass('hide');
                customerSearchForm.find(onLegacyValueHideClass).removeClass('show');
                // add the validation classes for the legacy filters
                addValidationClassesForLegacyFilters();

                // for cable there are specific other rules that should be verified regarding the fields
                if(legacyValue == 'cable') {
                    disableDeviceAndOrderCableInput();
                    disableAddressCableInput();
                }
            },
            /**
             * enable all input fields
             */
            enableAllInputFields = function() {
                var customerSearchInputTxt = $('#customer_search_form').find('input[type=text]');
                customerSearchInputTxt.removeAttr('disabled');
            },
            /**
             * removes the DSL validation classes and specific html added for validation
             */
            removeDslValidation = function() {
                $('.dsl-required').each(function () {
                    // $(this).find('input').removeClass("required-entry");
                    $(this).find('label span').remove();
                });
            },
            /**
             * add the DSL validation classes and specific html added for validation
             */
            addDslValidation = function() {
                // ass seen in the mockup the Mandatory input fields for DSL are Nachname, Geburtstag  and PLZ; we should add the validation class and the * mark
                $('.dsl-required').each(function () {
                    // $(this).find('input').addClass("required-entry");
                    $(this).find('label').append("<span>*</span>");
                });
            },
            /**
             * add/remove the validation classes specific for legacy system marked
             */
            addValidationClassesForLegacyFilters = function() {
                var legacyValue =  $('input[name=legacy_type]:checked', '#customer_search_form').val();
                if(legacyValue == 'dsl') {
                    addDslValidation();
                }
                // remove the dsl validation of other legacy is selected
                else {
                    removeDslValidation();
                }
            },
            disableDeviceAndOrderInputsOnCableAddressSearch = function() {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-address-disable').find('input').attr('disabled', true);
            },
            enableDeviceAndOrderInputsOnCableAddressSearch = function() {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-address-disable').find('input').removeAttr('disabled');
            },
            disableAddressInputsOnCableDeviceOROrderSearch = function() {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-device-or-order-disable').find('input').attr('disabled', true);
            },
            enableAddressInputsOnCableDeviceOROrderSearch = function() {
                var customerSearchForm = $('#customer_search_form');
                customerSearchForm.find('.for-cable-device-or-order-disable').find('input').removeAttr('disabled');
            },
            /**
             * if the input for device
             */
            disableDeviceAndOrderCableInput = function() {
                var legacyValue =  $('input[name=legacy_type]:checked', '#customer_search_form').val();
                if(legacyValue == 'cable') {
                    if($('#zipcode').val() || $('#city').val()|| $('#street').val()|| $('#no').val()|| $('#addNo').val()) {
                        disableDeviceAndOrderInputsOnCableAddressSearch();
                    }
                    else {
                        enableDeviceAndOrderInputsOnCableAddressSearch();
                    }
                }
            },
            disableAddressCableInput = function() {
                var legacyValue =  $('input[name=legacy_type]:checked', '#customer_search_form').val();
                if(legacyValue == 'cable') {
                    if($('#customer').val() || $('#device_id').val()) {
                        disableAddressInputsOnCableDeviceOROrderSearch();
                    }
                    else {
                        enableAddressInputsOnCableDeviceOROrderSearch();
                    }
                }
            };
        return {
            initCustomerFilters: initCustomerFilters,
            onSwitchLegacySearchChange: onSwitchLegacySearchChange,
            onSwitchAdvancedSearchChange: onSwitchAdvancedSearchChange,
            showHideDisableLegacySpecificFilters: showHideDisableLegacySpecificFilters,
            disableDeviceAndOrderCableInput: disableDeviceAndOrderCableInput,
            disableAddressCableInput: disableAddressCableInput,
            resetFilters: resetFilters
        }
    }();
    window.ToggleBullet = function() {
        var switchPending = false,
            switchOn = function (toggleClass) {
                $(toggleClass).attr('data-state', true);
                $(toggleClass).closest('.toggle-button').addClass('on-state').removeClass('off-state');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('on-state').removeClass('off-state');
                }
            },
            switchOff = function (toggleClass) {
                $(toggleClass).attr('data-state', false);
                $(toggleClass).closest('.toggle-button').addClass('off-state').removeClass('on-state');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('off-state').removeClass('on-state');
                }
            },
            onSwitchChange = function (toggleClass, toggleType) {
                var currentState = state(toggleClass);
                currentState == 1 ? switchOff(toggleClass) : switchOn(toggleClass);
                currentState = state(toggleClass);
                switch(toggleType) {
                    case 'soho':
                        $.post(MAIN_URL + 'customerde/details/setCustomerTypeSoho', {state: currentState});
                        ToggleBulletSoho.handleState(currentState, true);
                        break;
                    default:
                        break;
                }
            },
            state = function (toggleClass) {
                if($(toggleClass).attr('data-state') === "true" || $(toggleClass).attr('data-state') == 1) {
                    return 1;
                }
                else {
                    return 0;
                }
            },
            disable = function (toggleClass) {
                $(toggleClass).closest('.toggle-button').addClass('disabled').attr('onclick','');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').addClass('disabled').attr('onclick','');
                }
            },
            enable = function (toggleClass) {
                $(toggleClass).closest('.toggle-button').removeClass('disabled').attr('onclick','customerDe.setBusinessState()');
                if (!$(toggleClass).closest('.toggle-button').hasClass('switch')) {
                    $(toggleClass).find('.toggle-button').removeClass('disabled').attr('onclick','customerDe.setBusinessState()');
                }
            };
        return {
            switchPending: switchPending,
            onSwitchChange: onSwitchChange,
            state: state,
            switchOn: switchOn,
            switchOff: switchOff,
            disable: disable,
            enable: enable
        }
    }();
    window.ToggleBulletSoho = function() {
        var findStateAndHandleIt = function(toggleClass) {
            var currentState = ($(toggleClass).attr('data-state') === 'true' || $(toggleClass).attr('data-state') == 1) ? 1 : 0;
            handleState(currentState, false);
        },
        handleState = function(currentState, updateOnPrice) {
            if(currentState == 1 || currentState === 'true') {
                sohoStateHandle(updateOnPrice);
            }
            else {
                privateStateHandle(updateOnPrice);
            }
        },
        sohoStateHandle = function(updateOnPrice) {
            $('.mobile-discounts-container').addClass('hide');
            /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
            $('[data-id="customer-details-dob"]').addClass('hide');
            // when the customer is Soho the tax must be set to excluded
            var state = ToggleBullet.state('.tax-toggle');
        },
        privateStateHandle = function(updateOnPrice) {
            $('.mobile-discounts-container').removeClass('hide');
            /** OMNVFDE-301: For SOHO, nationality and DOB not displayed, **/
            $('[data-id="customer-details-dob"]').removeClass('hide');
            var state = ToggleBullet.state('.tax-toggle');
        };
        return {
            handleState: handleState,
            findStateAndHandleIt : findStateAndHandleIt
        }
    }();
})(jQuery);

jQuery(document).ready(function ($) {
    $('[data-toggle="tooltip"]').tooltip();
    CustomerFiltersSearch.initCustomerFilters();
});

// resize timeout
var rtime = new Date();
var delta = 200;
window.timeout = false;

window.__resizeCallbacks = [];
window.getResizeEvents = function () {
    return window.__resizeCallbacks;
};
window.addResizeEvent = function (callback) {
    window.__resizeCallbacks.push(callback);
    window.onresize = function () {
        var len = window.__resizeCallbacks.length;
        for (var i = 0; i < len; ++i) {
            window.__resizeCallbacks[i]();
        }
    }
};
window.addResizeEvent(function () {
    rtime = new Date();
    if (window.timeout === false) {
        window.timeout = true;
        setTimeout(LeftSidebar.resizeLeftPanel, delta);
    }
});
Handlebars.registerHelper('setIndexKey', function(value){
    this.indexKey = value;
});
Handlebars.registerHelper('compare', function (lvalue, rvalue, options) {
    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 3 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
        '==': function (l, r) {
            return l == r;
        },
        '===': function (l, r) {
            return l === r;
        },
        '!=': function (l, r) {
            return l != r;
        },
        '<': function (l, r) {
            return l < r;
        },
        '>': function (l, r) {
            return l > r;
        },
        '<=': function (l, r) {
            return l <= r;
        },
        '>=': function (l, r) {
            return l >= r;
        },
        'typeof': function (l, r) {
            return typeof l == r;
        },
        "lastArrayElement": function (l, r) {
            return l == r + 1;
        }
    };

    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

    var result = operators[operator](lvalue, rvalue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
Handlebars.registerHelper('compareTwoConditions', function (lvalue, rvalue, lSecondValue, rSecondValue, options, logicalOperator) {
    if (arguments.length < 5)
        throw new Error("Handlerbars Helper 'compareTwoConditions' needs 6 parameters");

    var operator = options.hash.operator || "==";
    var logicOperatorString = options.hash.logicalOperator || "||";

    var operators = {
        '==': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l == r || sl == sr);
            }
            if (logicOperatorString == "&&") {
                return (l == r && sl == sr);
            }
        },
        '===': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l === r || sl === sr);
            }
            if (logicOperatorString == "&&") {
                return (l === r && sl === sr);
            }
        },
        '!=': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l != r || sl != sr);
            }
            if (logicOperatorString == "&&") {
                return (l != r && sl != sr);
            }
        },
        '<': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l < r || sl < sr);
            }
            if (logicOperatorString == "&&") {
                return (l < r && sl < sr);
            }
        },
        '>': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l > r || sl > sr);
            }
            if (logicOperatorString == "&&") {
                return (l > r && sl > sr);
            }
        },
        '<=': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l <= r || sl <= sr);
            }
            if (logicOperatorString == "&&") {
                return (l <= r && sl <= sr);
            }
        },
        '>=': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l >= r || sl >= sr);
            }
            if (logicOperatorString == "&&") {
                return (l >= r && sl >= sr);
            }
        },
        'typeof': function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (typeof l == r || typeof sl == sr);
            }
            if (logicOperatorString == "&&") {
                return (typeof l == r && typeof sl == sr);
            }
        },
        "lastArrayElement": function (l, r, sl, sr) {
            if (logicOperatorString == "||") {
                return (l == r + 1 || sl == sr + 1);
            }
            if (logicOperatorString == "&&") {
                return (l == r + 1 && sl == sr + 1);
            }
        }
    };
    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compareTwoConditions' doesn't know the operator " + operator);

    var result = operators[operator](lvalue, rvalue, lSecondValue, rSecondValue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
Handlebars.registerHelper('json', function (context) {
    return JSON.stringify(context);
});
Handlebars.registerHelper('getCheckboxIsChecked', function (valueToCompare) {
    return (valueToCompare) ? 'checked' : 'unchecked';
});
Handlebars.registerHelper('setClassDependingOnValue', function (valueToCompare, classToAdd) {
    return (valueToCompare) ? classToAdd : '';
});
