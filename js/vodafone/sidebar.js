'use strict';

(function ($) {
    window.Sidebar = function () {
        this.initialize.apply(this, arguments)
    };
    window.Sidebar.prototype = $.extend(VEngine.prototype, {
        openPackage: null,
        requiredServiceAbilityFlag: 'requires-serviceability',
        initConfigurator: function (element) {
            /** If no serviceability check has been made, request address check and come back here */
            if ($(element).hasClass(this.requiredServiceAbilityFlag) && !address.services) {
                address.showCheckModal(this.initConfigurator, element, window.sidebar);
                // address.showServiceabilityUnknownModal();
            } else {
                /** Init configurator */
                var target = $(element).attr('data-target');
                var panelId = $('#buttons-trigger').attr('data-target');
                var migration = $(element).attr('data-migration');
                $('#' + panelId).hide();

                if(controller != 'cart') {
                    $('#' + target).show();

                    // minimize all
                    $('#' + target + ' .content').each(function(){ $(element).removeClass('hidden'); });
                    if(this.openPackage) {
                        $('#' + this.openPackage + ' .content').each(function(){ $(element).addClass('hidden'); });
                    }

                    this.openPackage = target;
                    initConfigurator(target, null, null, null, null, true, migration);

                    $('#package-types').addClass('hide');

                    if (!showOnlyOnePackage) {
                        $('#show-packages').removeClass('hide');
                        $('#cart-spinner-wrapper').find('.cart_packages').removeClassPrefix('control-buttons-');
                    }
                } else {
                    e.preventDefault();
                    window.location.href = MAIN_URL + '?type=' + target;
                }

            }
        },
        disableServiceNeededPackages: function () {
            $("#package-types").find('.' + sidebar.requiredServiceAbilityFlag).removeClass("disabled").addClass("disabled");
        },
        updatePrices: function (element) {
            window.setPageOverrideLoading && window.setPageOverrideLoading(true);
            /** We need the new state, which will be updated after the ajax call **/
            var state = !ToggleBullet.state('.tax-toggle');
            store.set('btwState', state);
            ToggleBullet.onSwitchChange('.tax-toggle', 'tax');

            if (state == true) {
                // OMNVFDE-705: When including tax is selected, hide the price details block
                $('#cartTotalsDetails').addClass('hide');
                // Increase cart height
                $("#cart-spinner-wrapper").attr("style", "height: calc(100% - 380px); height: -moz-calc(100% - 380px); height: -webkit-calc(100% - 240px); height: -o-calc(100% - 380px); height: -ms-calc(100% - 380px);");
            } else {
                $('#cartTotalsDetails').removeClass('hide');
                $('.cart-totals-table').removeClass('hide');

                // Decrease cart height
                $("#cart-spinner-wrapper").removeAttr('style');
            }
        }
    });

    // Init sidebar object on document ready
    window.sidebar = new Sidebar({
        baseUrl: MAIN_URL,
        spinner: MAIN_URL + 'skin/frontend/omnius/default/images/spinner.gif'
    });

    /** By default, all packages that require serviceability will be disabled and further enabled by serviceability call */
    $("#package-types").find(".requires-serviceability").removeClass("disabled").addClass("disabled");

    $(document).ready(function(){
        $(document).bind("serviceability-check-completed", function () {
            if (address.services) {
                $("#package-types").find('.' + sidebar.requiredServiceAbilityFlag).removeClass("disabled");
            } else {
                $("#package-types").find('.' + sidebar.requiredServiceAbilityFlag).removeClass("disabled").addClass("disabled");
            }
        });


        ToggleBullet.onSwitchChange('.tax-toggle', 'tax');
        //By default if no state is set, hide the details
        if (store.get('btwState') == null) {
            ToggleBullet.switchOn('.tax-toggle');

            jQuery('#cartTotalsDetails').addClass('hide');
            // Increase cart height
            jQuery("#cart-spinner-wrapper").attr("style", "height: calc(100% - 380px); height: -moz-calc(100% - 380px); height: -webkit-calc(100% - 240px); height: -o-calc(100% - 380px); height: -ms-calc(100% - 380px);");
        } else if (store.get('btwState') == true) {
            ToggleBullet.switchOn('.tax-toggle');

            jQuery('#cartTotalsDetails').addClass('hide');
            // Increase cart height
            jQuery("#cart-spinner-wrapper").attr("style", "height: calc(100% - 380px); height: -moz-calc(100% - 380px); height: -webkit-calc(100% - 240px); height: -o-calc(100% - 380px); height: -ms-calc(100% - 380px);");

        } else {
            ToggleBullet.switchOff('.tax-toggle');

            jQuery('#cartTotalsDetails').removeClass('hide');
            jQuery('.cart-totals-table').removeClass('hide');

            // Decrease cart height
            jQuery("#cart-spinner-wrapper").removeAttr('style');
        }

        /*$(".content").on("click", ".panel-heading", function(){
            var $panel = $(this).closest('.panel');
            var $item = $panel.find(".selected-item");
            var $block = $panel.find(".selection-block");
            //close the other ones

            jQuery(".panel").each(function(index){
                if(!$(this).is($panel)) {
                    var $item = $(this).find(".selected-item");
                    var $block = $(this).find(".selection-block");
                    $item.show();
                    $block.hide();
                }
            })
            $item.toggle();
            $block.toggle();

        });
        $(".content").on("click", "input, select", function(){
            return false;
        });
        $('.content').on('keyup', ".search", function(){
            var $value = $(this).val();
            var $panel = $(this).closest('.panel');
            if($panel.hasClass('panel-searchable')) {
                $panel.find(".item-row").each(function(index) {
                    var $itemText = $(this).find('.item-title').text();
                    if($itemText.indexOf($value) !=0) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            }
        });*/

    })

})(jQuery);
