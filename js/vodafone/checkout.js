/*
 * Copyright (c) 2016. Dynacommerce B.V.
 */

//'use strict';
(function ($) {
    var root = window;

    Checkout = function (store, orderEdit) {

        this.baseURL = '/';
        this.checkoutController = '/checkout/index/';
        this.cartController = '/checkout/cart/';
        this.checkoutAction = null;
        this.validatorMessages = {};
        this.currentCheckoutStep = '';
        this.stepsOrder = [];
        this.form = root.form;
        this.store = store;
        this.orderEdit = orderEdit;
        this.isOffer = 0; // 0 = not offer, 1 = new offer, 2 = loaded offer
        this.allowLockSteal = false;
        this.lockedByMsg = '';
        this.searchButtonId = '';
        this.bankGeneratorLinkIdPrefix = '';
        this.bankGeneratorContainer = '#checkout-bank-generator';
        this.contractSendContainer = '#checkout-offer-send';
        this.bankData = {
            institutionList: [
                "Deutsche Bank",
                "National Bank",
                "Local Bank"
            ]
        };

        this.initCart = function () {
            if (!$.isEmptyObject(this.validatorMessages)) {
                this.addValidatorRules();
            }
        };

        this.initialize = function (options) {
            var self = this;

            this.options = $.extend(this.options, options);

            // Activate checkout steps
            $.each(this.options.activesteps, function (index, step) {
                self.activateStep(step);
                self.lastStep = step;
            });
        };

        this.checkCurrentStep = function (currentCheckoutStep) {

            if (this.orderEdit != 0 && (currentCheckoutStep == 'null')) {
                currentCheckoutStep = 'saveSuperOrder';
            } else if (this.orderEdit == 0 && (currentCheckoutStep == 'null')) {
                currentCheckoutStep = 'saveCustomer';
            }

            if (currentCheckoutStep != 'saveCustomer') {
                $('#saveCustomer #create-offer').addClass('hidden');
            }
            this.currentCheckoutStep = currentCheckoutStep;

            var self = this;
            var active = true;
            var activeSteps = [];
            $('#cart-content').children('.bs-docs-section').each(function (f, elem) {
                if ($(elem).hasClass('skip')) return;
                var cartForm = $(elem).children('.cart');

                if (cartForm.length) {
                    var name = $(elem).children('.cart').attr('action');
                    // Skip the delivery step, because this is done separately
                    if (name != 'saveDeliveryAddress') {
                        self.form[name + 'Data'] = $('#' + name).serialize();
                    }
                    self.stepsOrder.push(name);
                }
            });

            self.stepsOrder.each(function (name) {
                if ((currentCheckoutStep != 'null') && ($.inArray(currentCheckoutStep, self.stepsOrder) != -1) && active) {
                    $('#{0}'.format(name)).parent().removeClass('hidden');

                    $('#cart-left').find('[name="' + name + '"]').parent().removeClass('disabled active').addClass('active');
                    $('#' + name).find('button[type=submit]').addClass('hide');
                    activeSteps.push(name);
                    if (currentCheckoutStep == name) {
                        active = false;
                        $('#' + name).find('button[type=submit]').removeClass('hide');
                    }

                    self.fillFormData(name);
                }
            });

            if ((this.orderEdit == 0) && currentCheckoutStep != 'null' && ($.inArray(currentCheckoutStep, self.stepsOrder) != -1)) {
                $('#cart-left').find('[name="' + currentCheckoutStep + '"]').parent().addClass('active');
                $('#' + currentCheckoutStep).find('button[type=submit]').removeClass('hide');
                var currentStepDiv = $('#{0}'.format(currentCheckoutStep));

                if (currentStepDiv.length) {
                    scrollToExactSection(currentStepDiv);
                }
            }

            this.options.activesteps = activeSteps;

            if (this.orderEdit != 0) {
                this.initialize();
            }
        };

        this.validatePreviousSteps = function () {
            var self = this;
            var passed = true;
            $.each(self.stepsOrder, function (id, step) {
                if (step != self.currentCheckoutStep) {
                    if (self.form[step].validator.validate() != true) {
                        passed = false;
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            });
            return passed;
        };

        this.toggleLockAction = function () {
            if (($('.lock-info-spinner').length == 1)) {
                // If any actions should be performed when the lock info is loading should go here
                return;
            }
            var self = this;
            if (self.allowLockSteal) {
                $('#main_lock_btn').click();
            } else {
                self.orderLockModal(self.lockedByMsg);
            }
        };

        this.restartDelivery = function (button, orderNumber, deliveryId) {
            var self = this;
            if (self.checkoutDisabled()) {
                self.toggleLockAction();
                return false;
            }

            $.post(this.cartController + 'restartHomeDelivery', {'orderNumber': orderNumber, 'deliveryId': deliveryId},
                function (response) {
                    $(button).prop('disabled', true);
                    $('button[data-delivery-id="{0}"]'.format(deliveryId)).prop('disabled', true);

                    if (response.hasOwnProperty('serviceError')) {
                        var msg = Translator.translate('Offering again the packages for home delivery has failed.') + '<br />';
                        msg += Translator.translate('Error message:');
                        msg += ' ' + response.message;
                        showModalError(msg, response.serviceError);
                    } else {
                        showModalError(response.message, Translator.translate('Restart delivery'));
                        // Exit order edit page when modal is closed
                        $('#error-modal').on('hidden.bs.modal', function (e) {
                            setPageLoadingState(true);
                            window.location = self.cartController + 'exitChangeOrder';
                        });
                    }
                }
            );
        };

        this.addValidatorRules = function () {
            Validation.add('nl-postcode', this.validatorMessages['nl-postcode'], function (v) {
                return (v == '' || (v == null) || (v.length == 0) || /^[0-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(v));
            });

            Validation.add('validate-date-future', this.validatorMessages['validate-date-future'], function (v, element) {
                var date = new Date();
                var input = v.split('-');
                var test = new Date(input[1] + '/' + input[0] + '/' + input[2]);

                var dateNow = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                return test.valueOf() >= dateNow.valueOf();
            });

            Validation.add('validate-date-age', this.validatorMessages['validate-date-age'], function (v, element) {
                var reg = /^(0[1-9]|[12][0-9]|3[01])([-])(0[1-9]|1[012])\2(\d{4})$/;
                return reg.test(v);
            });

            Validation.add('validate-date-nl', this.validatorMessages['validate-date-nl'], function (v, element) {
                if ((v != '') && (v != null) && (v.length != 0)) {
                    var reg = /^(0[1-9]|[12][0-9]|3[01])([-])(0[1-9]|1[012])\2(\d{4})$/;
                    return reg.test(v);
                } else {
                    return true;
                }
            });

            Validation.add('validate-legitimation-number-0', this.validatorMessages['validate-legitimation-number-0'], function (v, element) {
                v = v.toUpperCase();
                if (v.substring(0, 3) == 'NLD') {
                    v = v.substring(3);
                }
                return (v.length > 0);
            });

            Validation.add('validate-legitimation-number-1-4', this.validatorMessages['validate-legitimation-number-1-4'], function (v, element) {
                v = v.toUpperCase();
                if (v.substring(0, 3) == 'NLD') {
                    v = v.substring(3);
                }
                //For documents types 0, 1, 2, 3 and 4 = eight numeral digits (no character) and the last five digits are not equal. Do not submit the characters "NLD" in front of the document number.
                return (v.length === 8 && /^[0-9]+$/.test(v) && !/^(\d)\1*$/.test(v.substr(v.length - 5)));
            });

            Validation.add('validate-legitimation-number-p', this.validatorMessages['validate-legitimation-number-p'], function (v, element) {
                //For document type P and Country Code <> NL = no format rules.
                return true;
            });

            Validation.add('validate-legitimation-number-p-nl', this.validatorMessages['validate-legitimation-number-p-nl'], function (v, element) {
                //For document type P and Country Code NL = nine digits, starts with a character ends with a number (in between can be character and numbers). Character (letter) "O" is not valid.
                return (/[A-Z]/.test(v.charAt(0)) && v.length === 9 && /[0-9]/.test(v.charAt(v.length - 1)) && -1 === v.toLowerCase().indexOf('o'));
            });

            Validation.add('validate-legitimation-number-r', this.validatorMessages['validate-legitimation-number-r'], function (v, element) {
                //For document type R = ten numeral digits (no characters).
                return (v.length === 10 && /^[0-9]+$/.test(v));
            });

            Validation.add('validate-legitimation-number-n', this.validatorMessages['validate-legitimation-number-n'], function (v, element) {
                //For document type N = nine digits, starts with a character ends with a number (in between can be character and numbers)
                return (/[A-Z]/.test(v.charAt(0)) && v.length === 9 && /[0-9]/.test(v.charAt(v.length - 1)));
            });

            Validation.add('validate-payment-single', Translator.translate('This is a required field.'), function (v, element) {
                return $(element).parent().find('[name="' + $(element).parent().attr('id') + '"]').val();
            });

            Validation.add('validate-invalid-iban-street', Translator.translate('Invalid IBAN address.'), function(v, el){
                return false;
            });

            Validation.add('validate-business-commercial-register-type-de', Translator.translate('This is a required field.'), function(v, el) {
                return v.length > 0;
            });

        /*  Validation.add('validate-iban-code', this.validatorMessages['validate-iban-code'], function (v, element) {
                console.log("DAAAAAAMNN!!");
                console.log(jQuery(element).attr('id') + ' is visible: ' + jQuery(element).is(':visible'));
                console.log(!jQuery(element).is(':visible') || (v.length === 22 && /[a-zA-Z]/.test(v.charAt(0)) && /[a-zA-Z]/.test(v.charAt(1))));
                return (!jQuery(element).is(':visible') || (v.length === 22 && /[a-zA-Z]/.test(v.charAt(0)) && /[a-zA-Z]/.test(v.charAt(1))));
            }); */
        };

        this.acceptTerms = function (self) {
            if ($(self).prop('checked') == true) {
                $('#cart #submit').removeAttr('disabled');
            } else {
                $('#cart #submit').attr('disabled', 'disabled');
            }
        };

        this.checkoutDisabled = function () {
            return (this.orderEdit && $('.order-lock button').length && $('.order-lock button').data('state') == 'locked' && $('.order-lock button').prop('disabled') == false) || ($('.lock-info-spinner').length == 1);
        };

        this.savePreviousStep = function (noErrors, stepName, checkoutAction, thisForm) {
            var self = this;
            if (!(stepName in self.form)) {
                // Workaround for retention fix
                noErrors = true;
            } else if (self.form[stepName].validator && self.form[stepName].validator.validate() && noErrors) {
                var oldForm = $('#' + stepName);
                oldForm.find('.ajax-validation').fadeOut('slow', function () {
                    $(this).remove();
                });
                oldForm.find('.cart-errors').remove();
                if (self.form[stepName + 'Data'] != $('#' + stepName).serialize()) {
                    $.ajax({
                        type: "POST",
                        url: this.checkoutController + stepName,
                        data: oldForm.serializeObject(),
                        async: false
                    })
                        .done(function (response) {
                            var postCallbackTest = self.postCallback(response, oldForm);
                            if (!postCallbackTest) {
                                noErrors = false;
                            } else {
                                // Save data to be able to compare in case of going back some steps
                                self.form[checkoutAction + 'Data'] = thisForm.serialize();
                            }
                        });
                }
            } else {
                noErrors = false;
                thisForm.find('#submit').button('reset').removeClass('disabled').prop('disabled', false);
            }
            return noErrors;
        };
        this.submitCart = function (obj, async) {
            async = (async == true || async == 'undefined' || async == undefined);
            var self = this;
            var thisForm = obj;
            if (self.checkoutDisabled()) {
                self.toggleLockAction();
                return false;
            }
            thisForm.find('.ajax-validation').fadeOut('slow', function () {
                $(this).remove();
            });
            thisForm.find('.cart-errors').remove();

            var currentForm = thisForm.attr('id');

            if (self.form[currentForm].validator && self.form[currentForm].validator.validate()) {
                var checkoutAction = thisForm.attr('action');
                var formData = thisForm.serializeObject();
                var stepsOrder = self.stepsOrder;
                var currentStep = stepsOrder.indexOf(checkoutAction);
                var noErrors = true;
                for (var i = currentStep - 1; i >= 0; --i) {
                    noErrors = this.savePreviousStep(noErrors, stepsOrder[i], checkoutAction, thisForm);
                }
                if (noErrors) {
                    thisForm.find('button[type=submit]').addClass('hide');
                    var nextSection = thisForm.parents('.bs-docs-section').nextAll('.bs-docs-section').first().find('h3:first').attr('id');
                    var nextStep = thisForm.parents('.bs-docs-section').nextAll('.bs-docs-section').first().find('form').attr('id');

                    formData['current_step'] = nextSection;
                    formData['is_offer'] = self.isOffer;

                    // On order save first show the popup, then make the calls
                    if (checkoutAction == 'saveOverview') {
                        // Hide all active buttons from the overview when save is pressed
                        $('#cart-content .new-customer:visible, #cart-content .submit button:visible, #cart-content .expanded-package-container .drop-menu:visible, #cart-content .package-item-description-coupon:visible').addClass('hidden');
                    }

                    if (checkoutAction == 'saveContract') {
                        Polling.show();
                    }

                    if (checkoutAction == 'saveSuperOrder') {
                        Polling.show();
                        enableLeavePageDialog();
                    }


                    $.ajax({
                            type: "POST",
                            url: this.checkoutController + checkoutAction,
                            data: formData,
                            async: async
                        })
                        .done(function (response) {
                            self.checkoutAction = checkoutAction;
                            self.currentCheckoutStep = stepsOrder[currentStep + 1];
                            // RFC-151034 : Alert agent with message "The order content has just been changed, please re-open the order"
                            if (self.checkoutAction == 'saveContract') {
                                if (response.hasOwnProperty('serviceError')) {
                                    Polling.hide();
                                    showModalError(response.message, response.serviceError);
                                    return false;
                                }
                            }

                            var callResponse = self.postCallback(response, thisForm);
                            if (!callResponse) {
                                if (self.checkoutAction == 'saveContract' && !response.hasOwnProperty('serviceError')) {
                                    var approveModal = $('#approve-package-fail-modal');
                                    approveModal.find('#approve-package-error-msg').html(response.message);
                                    approveModal.modal();
                                    thisForm.find('.cart-errors').html('');
                                }
                                return false;
                            }

                            if (self.isOffer == 1) {
                                if (response.is_offer == true) {
                                    // redirect to home page if the offer has been saved
                                    window.location = '/';
                                }
                            } else {
                                if (self.checkoutAction != 'saveContract' && self.checkoutAction != 'saveSuperOrder') {
                                    processPreviousSteps(nextStep);
                                }
                                triggerCheckoutEvent('checkout.stepdone', checkoutAction);
                                self.fillFormData(nextStep);

                                $('#cart_left_' + formData['current_step']).trigger('click');
                            }
                        });
                    return true;
                } else {
                    if (obj.attr('id') == 'saveSuperOrder') {
                        $('.checkout-totals').find('button.disabled').removeClass('disabled');
                    }
                    return false;
                }
            } else {
                return false;
            }
        };

        this.postCallback = function (response, changedForm) {
            var self = this;
            changedForm.find('.cart-errors').remove();
            if (response.hasOwnProperty('serviceError')) {
                // Close the popups in case of a service error, because it means there is no polling id and no polling can be made
                if ('saveOverview' == this.checkoutAction) {
                    self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 1);
                } else if ('saveContract' == this.checkoutAction) {
                    self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 0);
                } else if ('saveSuperOrder' == this.checkoutAction) {
                    self.showVodError(true, response.serviceError + ' - ' + response.message, 1, 1);
                    disableLeavePageDialog();
                } else {
                    showModalError(response.message, response.serviceError);
                }

                changedForm.find('button[type=submit]').removeClass('hide').button('reset');
            } else if (response.error == true) {
                // Close the popups in case of an error, because it means there is no polling id and no polling can be made
                if ('saveOverview' == this.checkoutAction) {
                    Polling.hide();
                }

                if ('saveContract' == this.checkoutAction) {
                    Polling.hide();
                }

                if ('saveSuperOrder' == this.checkoutAction) {
                    Polling.hide();
                    disableLeavePageDialog();
                }
                if (response.hasOwnProperty('modalError')) {
                    showModalError(Translator.translate(response.modalError, Translator.translate('Error')));
                }

                if (response.unreserved != undefined) { //could not reserve all packages
                    //SHOW MODAL
                    var reservedModal = $('#reserve-fail-modal');
                    reservedModal.find('.failed-reservation').html(response.unreserved);
                    var replacementText = $(".failed-reservation").find("p:contains('Mobile package')").text().replace('Mobile package', Translator.translate('Mobile package'));
                    $(".failed-reservation").find("p:contains('Mobile package')").text(replacementText);
                    reservedModal.modal();
                    return;
                }

                var otherAddressField = {};
                window.data = response;
                var errorText = '';
                var focused = false;
                var showOtherAddressFields = false;
                var showCompanyDetailsFields = false;
                for (var i in response.fields) {
                    if (response.fields.hasOwnProperty(i)) {
                        var label;
                        //var identifier = i.replace(/([ !"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~])/g,'\\$1');

                        var field = $('[name="' + i + '"]');

                        if (!showOtherAddressFields) {
                            if (i.indexOf('otherAddress') != -1) {
                                showOtherAddressFields = true;
                                if (i.indexOf('pakket') != -1) {
                                    var re = /pakket\]\[(\d+)\]\[otherAddress/g;
                                    var packId = i.split(re, 2);
                                    otherAddressField[packId[1]] = field;
                                } else {
                                    otherAddressField[0] = field;
                                }
                            }
                            if (showOtherAddressFields) {
                                for (var j in otherAddressField) {
                                    showOtherAddress(otherAddressField[j].parents('.otherAddress').removeClass('hidden').find('.editOtherAddress'));
                                }
                            }
                        }

                        if (!showCompanyDetailsFields) {
                            if (i.indexOf('company_legal_form') != -1 || i.indexOf('company_vat_id') != -1) {
                                showCompanyDetailsFields = true;
                            }

                            if (showCompanyDetailsFields) {
                                $('#customer-company-search .enter-yourself').trigger('click');
                            }
                        }

                        field.parent().find('.validation-advice').remove();
                        var fieldError = '<div class="validation-advice ajax-validation" style="">' + Translator.translate(response.fields[i]) + '</div>';
                        field.addClass('validation-failed').after(fieldError);
                        if (field.length && $.trim(field.siblings('label').text()).length) {
                            label = $.trim(field.siblings('label').text());
                        } else if (field.length && $.trim(field.parent().siblings('label').text()).length) {
                            label = $.trim(field.parent().siblings('label').text());
                        } else {
                            if (field.attr('placeholder') !== undefined) {
                                label = field.attr('placeholder');
                            } else {
                                var match = i.match(/\[(.*)\]/);
                                if (match) {
                                    label = match[1].split('_').map(function (i) {
                                        return i.charAt(0).toUpperCase() + i.slice(1);
                                    }).join(' ');
                                } else {
                                    label = i.split('_').map(function (i) {
                                        return i.charAt(0).toUpperCase() + i.slice(1);
                                    }).join(' ');
                                }
                            }
                        }

                        if (!focused) {
                            field.parent().get(0).scrollIntoView();
                            focused = true;
                        }

                        errorText += '<i>' + label + '</i>: ' + response.fields[i] + '<br>';
                    }
                }
                if (!response.hasOwnProperty('modalError')) {
                    changedForm.find('button[type=submit]').removeClass('hide').button('reset');
                    changedForm.append('<div class="cart-errors"><strong>' + Translator.translate(response.message) + '</strong>:<br >' + Translator.translate(errorText) + '</div>');
                }
                return false;
            } else if (response.error == false) {

                if (response.orders != undefined) {
                    $('#order-increment-id').text(response.orders);
                }

                if (this.checkoutAction == 'saveOverview' || this.checkoutAction == 'saveSuperOrder') {
                    if (response.contract_page) {
                        if (response.skip_number_selection == undefined) {
                            $('#saveNumberSelection').parent().removeClass('skip');
                            $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().removeClass('hidden');
                        } else {
                            $('#saveNumberSelection').parent().addClass('skip');
                            $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().addClass('hidden');
                        }

                        $('#saveContract').parent().removeClass('skip');
                        $('#cart-left .bs-sidebar .nav li a[href="#contract"]').parent().removeClass('hidden');
                        var contract_content = $('#contract_page').find('#contract-content');
                        if ($('#contract_page') && contract_content) {
                            contract_content.replaceWith($(response.contract_page).find('#contract-content'));
                        }

                        if (this.checkoutAction == 'saveSuperOrder') {
                            $('#contract-continue-button').parent().addClass('hidden');
                            $('#saveContract .checkbox-inline').addClass('hidden');
                        }

                        var hnIndex = self.stepsOrder.indexOf("saveNewNetherlands");
                        if (self.stepsOrder.indexOf("saveNumberSelection") == -1 && $('#saveNumberSelection').length > 0) {
                            if (hnIndex != -1) {
                                self.stepsOrder.splice(hnIndex, 0, 'saveNumberSelection');
                            } else {
                                self.stepsOrder.splice(self.stepsOrder.length - 1, 0, 'saveNumberSelection');
                            }
                        }
                        if (self.stepsOrder.indexOf("saveContract") == -1) {
                            self.stepsOrder.splice(self.stepsOrder.length - 1, 0, 'saveContract');
                        }
                    } else {
                        $('#saveNumberSelection').parent().addClass('skip');
                        $('#saveContract').parent().addClass('skip');
                    }

                    // Added for RFC-150665/RFC-150663
                    if (response.newOrderMessage && response.newOrderMessage.length) {
                        $('#showOrderConfirmation #order-confirmation-new-order-message').html(response.newOrderMessage);
                    }

                    if (response.oldOrderMessage && response.oldOrderMessage.length) {
                        $('#showOrderConfirmation #order-confirmation-old-order-message').html(response.oldOrderMessage);
                    }

                    $('.order-lock').remove();
                }
                if (response.new_order != undefined) {
                    $('#new-order-increment-id').data('id', response.new_order_id);
                    $('#new-order-increment-id').text(response.new_order);
                    $('.new_superorder').removeClass('new_superorder');
                }
                if (this.checkoutAction == changedForm.attr('action') && self.isOffer != 1) {
                    $('#saveCustomer #create-offer').addClass('hidden');

                    if (this.checkoutAction == 'saveOverview') {
                        showProcessOrderModal(response.polling_id);
                        if ($('#saveCreditCheck').length && response.hasOwnProperty('addresses') && response.hasOwnProperty('order_id')) {
                            // If the creditcheck step is present and necessary
                            var creditStub = response.credit_fail == true;
                            window.creditCheckData = {addresses: response.addresses, order_id: response.order_id, creditStub: creditStub, order_no: response.orders};
                        }
                    }

                    if (store == 'retail' || store == 'belcompany' || store == 'indirect') {

                        if (store == 'retail' || store == 'belcompany') {
                            if (this.checkoutAction == 'saveDeliveryAddress') {
                                if (response.hasOwnProperty('delivery_steps') && response.delivery_steps == false) {
                                    // hide delivery steps
                                    $('#saveNumberSelection').parent().addClass('hidden skip');
                                    $('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().addClass('hidden');
                                    $('#saveContract').parent().addClass('hidden skip');
                                    $('#cart-left .bs-sidebar .nav li a[href="#contract"]').parent().addClass('hidden');
                                    self.stepsOrder = $.grep(self.stepsOrder, function (value) {
                                        return value != 'saveContract' && value != 'saveNumberSelection';
                                    });
                                } else {
                                    // show delivery steps
                                    //$('#saveNumberSelection').parent().removeClass('skip');
                                    //$('#cart-left .bs-sidebar .nav li a[href="#number-selection"]').parent().removeClass('hidden');
                                    //$('#saveContract').parent().removeClass('skip');
                                    //$('#cart-left .bs-sidebar .nav li a[href="#contract"]').parent().removeClass('hidden');
                                    //self.stepsOrder.splice( self.stepsOrder.length-1, 0, 'saveNumberSelection');
                                    //self.stepsOrder.splice( self.stepsOrder.length-1, 0, 'saveContract');
                                }
                            }

                            if (this.checkoutAction == 'saveNewNetherlands') {
                                // Update the order confirmation data with the newly inputted order numbers
                                var newNethData = $('#saveNewNetherlands').serializeObject();
                                var implodeData = [];
                                newNethData.new_netherlands.each(function (el) {
                                    implodeData.push(el.order_number);
                                });
                                $('#order-hn-increment-id').html(implodeData.join(', '));
                            }
                        }

                        if (this.checkoutAction == 'saveCreditCheck') {
                            // check if the number selection step is available
                            if ($('#saveNumberSelection').length && response.hasOwnProperty('html')) {
                                updateNumberSelection(response.html);
                            }
                        }

                        if (this.checkoutAction == 'saveNumberSelection' && $('#saveContract').length && response.contract != undefined) {
                            updateContract(response.contract);
                        }
                    }

                    if (this.checkoutAction == 'saveSuperOrder') {
                        if (!response.hasOwnProperty('orders')) {
                            Polling.hide();
                            window.location = this.baseURL + 'checkout/cart/exitChangeOrder';
                        } else {
                            Polling.start('saveSuperOrder', response.pool_id);
                        }
                    }

                    if (this.checkoutAction == 'saveCreditCheck') {
                        $('#creditcheck-waiting').addClass('hidden');
                        $('#credit-check-menu').addClass('disabled').removeClass('active');
                        var nextStep = this.stepsOrder[this.stepsOrder.indexOf('saveCreditCheck') + 1];
                        if (undefined != nextStep) {
                            $('#credit-check-menu').parent().find('[name=' + nextStep + ']').parent().addClass('active');
                        }
                    }

                    if (this.checkoutAction == 'saveContract') {
                        self.showVodError(false);
                        Polling.start('saveContract', response.pollingId);
                    }

                    if (this.checkoutAction != 'saveOverview' && this.checkoutAction != 'saveContract' && this.checkoutAction != 'saveSuperOrder') {
                        scrollToSection(changedForm);
                    }
                }
                self.form[changedForm.attr('action') + 'Data'] = changedForm.serialize();
                return true;
            } else {
                changedForm.append('<div class="cart-errors"><strong>' + Translator.translate('A server error occurred') + '</strong>:<br ></div>');
                return false;
            }
        };

        this.switchOrderLock = function (obj) {
            var state = $(obj).data('state');

            this.switchLock(obj, state);
        };

        this.registerReturn = function (packageId) {
            if (this.checkoutDisabled()) {
                this.toggleLockAction();
                return false;
            } else {
                var query = $('#order_register_return_' + packageId);
                query.appendTo('body').modal();
            }
            return true;
        };

        this.showRegisterReturnModalError = function (packageId, message) {
            var field = $('#order_register_return_error_' + packageId);
            if (message === false) {
                field.parent().addClass('hidden');
            } else {
                field.parent().removeClass('hidden');
                field.html(message);
            }
        };

        this.submitRefund = function (object, packageId, complete) {
            $.ajax({
                async: true,
                type: 'POST',
                data: object.serialize() + "&trigger_complete%5B" + packageId + "%5D=" + complete,
                url: MAIN_URL + 'checkout/index/registerReturn',
                success: function (data) {
                    if (data.error) {
                        checkout.showRegisterReturnModalError(packageId, data.message);
                        checkout.registerReturn(packageId);
                    } else {
                        checkout.showRegisterReturnModalError(packageId, false);
                        $('#return_button_' + packageId).addClass('hidden');
                        if (data.html) {
                            $('#returned-note-' + packageId).html(data.html);
                        }
                        // Change the order confirmation message as stated in RFC-150663
                        if (data.oldOrderMessage && data.oldOrderMessage.length) {
                            $('#showOrderConfirmation #order-confirmation-old-order-message').html(data.oldOrderMessage);
                        }
                        if (data.polling_id) {
                            setPageLoadingState(false);
                            Polling.start('registerReturn', data.polling_id);
                        }
                    }
                },
                error: function () {
                    console.log('Failed to retrieve the data');
                }
            });
        };

        this.cancelPendingOrder = function (orderNumber) {
            if (orderNumber) {
                $.ajax({
                    async: true,
                    type: 'POST',
                    data: {'super_order': orderNumber},
                    url: MAIN_URL + 'checkout/index/cancelPendingOrder',
                    success: function (data) {
                        if (data.error == false) {
                            window.location.href = '/checkout/cart/exitChangeOrder';
                        } else {
                            showModalError(data.message);
                        }
                    },
                    error: function () {
                        console.log('Failed to cancel the order');
                    }
                });
            }
        };

        this.takeOverLock = function (obj) {
            this.switchOrderLock($('.order-lock button'));
        };

        this.orderLockModal = function (message) {
            var self = this;

            if (this.allowUnlock) {
                $('#order_lock_modal p.unlock-order').removeClass('hidden');
                $('#order_lock_modal .modal-body .text').text(message);
                $('#order_lock_modal .unlock-order').removeClass('hidden');
                if (self.allowLockSteal) {
                    $('#order_lock_modal button.take-over-lock').removeClass('hidden');
                    $('#order_lock_modal .modal-body .text-warning').removeClass('hidden');
                    $('#order_lock_modal .unlock-order').addClass('hidden');
                } else {
                    $('#order_lock_modal .modal-body .text-warning').addClass('hidden');
                    $('#order_lock_modal button.take-over-lock').addClass('hidden');
                }
                $('#order_lock_modal .pull-left').html(Translator.translate('Cancel'));
            } else {
                $('#order_lock_modal .unlock-order').addClass('hidden');
                $('#order_lock_modal .modal-body .text').text(message).removeClass('hidden');
            }
            $('#order_lock_modal').appendTo('body').modal();
        };

        this.disableAdyen = function () {
            if ($('#delivery-payment .split-payment-method select:enabled').length) {
                $('#delivery-payment .split-payment-method select:enabled').each(function (id, el) {
                    // Only deselect if adyen was found
                    if ($(el).find('option[value=adyen_hpp]:selected').length) {
                        $(el).find('option').prop('selected', false);
                        $(el).find('option[value!=""]').first().prop('selected', true);
                    }
                });
                $('#delivery-payment select:enabled option[value="adyen_hpp"]').remove();
            }
            if ($('#payment_method_text:enabled').length) {
                $('#delivery-payment a[data-value="adyen_hpp"]').parents('.dropdown-menu').find('a').first().trigger('click');
                $('#delivery-payment a[data-value="adyen_hpp"]').parent().remove();
            }
        };

        this.disableRambours = function (packages) {
            // When more packages check in the split payment blocks if there is a package that needs the payment type removed
            $('#delivery-payment .split-payment-method select').each(function (id, el) {
                el = $(el);
                var packageId = el.parent().data('package-id');
                if (packages.hasOwnProperty(packageId) && packages[packageId]) {
                    // Only deselect if rambours was found
                    if (el.find('option[value=cashondelivery]:selected').length) {
                        el.find('option[value=cashondelivery]').remove();
                        el.find('option').attr('selected', false);
                        el.find('option[value="checkmo"]').attr('selected', 'selected');
                    } else {
                        el.find('option[value=cashondelivery]').remove();
                    }
                }
            });

            // When only one package check in the single payment block if the only package needs the payment removed
            if (packages.hasOwnProperty(1)) {
                // If rambours is selected, select another payment method
                if ($('[name="payment[method]"]').val() == 'cashondelivery') {
                    $('#delivery-payment a[data-value="cashondelivery"]').parents('.dropdown-menu').find('a[data-value="checkmo"]').trigger('click');
                }
                $('#delivery-payment a[data-value="cashondelivery"]').parent().remove();
            }
            window.restrictedRambours = packages;
        };

        this.switchLock = function (obj, state) {
            var self = this;
            var reverse = state == 'locked' ? 'unlocked' : 'locked';
            $(obj).prop('disabled', 'disabled');
            if (state == 'unlocked') {
                if ($(obj).data('edited') == 1) {
                    $('#order_unlock_modal').appendTo('body').modal();
                    return false;
                }
            }

            var callback = function () {
                $.post(self.cartController + 'lockSuperorder', {state: reverse, orderId: $(obj).data('order-id')}, function (response) {
                    if (response.error) {
                        if (response.hasOwnProperty('locked')) {
                            self.orderLockModal(response.message);
                        } else {
                            showModalError(response.message);
                        }
                    } else {
                        if (response.hasOwnProperty('edited') && response.edited == 1) {
                            window.location = self.baseURL;
                        } else {
                            self.toggleLockState(obj, state, false);
                            self.disableAdyen();
                        }
                    }
                });
            };

            if ($(obj).data('force')) {
                $.post(self.cartController + 'lockSuperorder', {state: 'locked', orderId: $(obj).data('order-id')}, function (response) {
                    if (response.error) {
                        if (response.hasOwnProperty('locked')) {
                            self.orderLockModal(response);
                        } else {
                            showModalError(response.message);
                        }
                    } else {
                        callback();
                    }
                    $('#cart-top .order-lock button').data('force', false);
                });
            } else {
                callback();
            }

        };

        this.toggleLockState = function (obj, state, disable) {
            if (!$(obj).length) {
                return;
            }

            var self = this;
            var reverse = state == 'locked' ? 'unlocked' : 'locked';
            $('.order-lock .lock-icon').addClass(reverse).removeClass(state);
            $('#order-{0}-text'.format(state)).addClass('hide');
            $(obj).data('state', reverse).text($(obj).data('{0}-text'.format(reverse)));
            $('#order-{0}-text'.format(reverse)).removeClass('hide');
            if (disable) {
                $(obj).prop('disabled', true);
            } else {
                $(obj).prop('disabled', false);
            }

            //if button onclick displayed the steal modal, make sure we always overwrite it
            if (!$(obj).data('onclick-changed')) {
                $(obj).data('onclick-changed', true);
                $(obj)[0].onclick = null;
                $(obj).click(function () {
                    checkout.switchOrderLock(obj)
                });
                self.allowLockSteal = false;
                self.lockedByMsg = '';
            }
            $(obj).prop('disabled', false);
        };

        this.setRefundMethod = function (obj) {
            var value = $(obj).find('option').filter(':selected').first().val();
            $('form#saveSuperOrder input[name=refund_method]').val(value);
        };

        this.checkStoreStock = function (target) {
            // Search for stores addresses
            if (this.orderEdit && $('.order-lock button').length && $('.order-lock button').data('state') == 'locked') {
                this.toggleLockAction();
            } else {
                var searchStores = $('#search-stores');
                var stores = searchStores.find('#storesFound').find('table tbody tr');
                var targetElem = $('[id="' + target + '"]');

                stores.remove();
                $('#storesFound').addClass('hidden');
                searchStores.data("target-input", target);

                if (targetElem.data('package-id')) {
                    searchStores.data("package-id", targetElem.data('package-id'));
                } else {
                    searchStores.removeData('package-id');
                }
                searchStores.modal();
            }
        };

        this.searchStores = function () {
            var packageId = $('#search-stores').data('package-id');
            $.ajax(
                this.checkoutController + 'stockStores',
                {
                    data: {package_id: packageId},
                    localCache: false
                }).done(function (res) {
                    if (res.hasOwnProperty('serviceError')) {
                        showModalError(res.message, res.serviceError);
                        return;
                    } else if (res.error) {
                        showModalError(res.message);
                    } else {
                        $('#search-stores').find('#storesFound').find('table tbody').html(res.list);

                        $('[name="searchStore[postcode]"]').autocomplete({
                            lookup: _stores
                        });

                        var stores = $('#search-stores').find('#storesFound').find('table tbody tr');
                        var keyword = $.trim($('[id="searchStore[postcode]"]').val());

                        var reg = new RegExp(window.search.escapeValue(keyword), 'ig');

                        if (keyword.length) {
                            stores.each(function () {
                                if ($(this).find('td').text().match(reg)) {
                                    $(this).show();
                                } else {
                                    $(this).hide();
                                }
                            });
                        } else {
                            stores.show();
                        }

                        if ($('#storesFound').is(':hidden')) {
                            $('#storesFound').removeClass('hidden');
                        }
                    }
                }
            );
        };

        this.setStoreAddress = function () {
            var selected = $('#storesFound').find('input[name=store]').filter(':checked');
            if (!selected.length) {
                $('#search-stores').modal('hide');
                $('#storesFound').addClass('hidden');
                return;
            }
            var street = selected.data('street');
            var city = selected.data('city');
            var storeId = selected.data('store-id');
            var address = (street && street.length ? street + ', ' : '') + city;
            var target = $('#search-stores').data('target-input');
            var idTarget = target.replace('store', 'store_id');

            $('[id="' + target + '"]').val(address).prop('data-store-id', storeId);
            $('[id="' + idTarget + '"]').val(storeId);

            $('#search-stores').modal('hide');
            $('#storesFound').addClass('hidden');
        };

        this.setDropdownOption = function (group, self, open) {
            var txt = self.text();
            var value = self.data('value');
            var type = group.data('type');
            var input = group.data('input');
            if (open) group.removeClass('open');
            group.find('button.text').first().text(txt);
            group.find('input[name="' + input + '"]').val(value);
            return type;
        };

        this.removeGeneralValidationRules = function () {
            var self = this;
            $.each(this.options.generalValidationRules, function (index, rules) {
                $.each(rules, function (key, value) {
                    if (key.match(/\[\]$/)) { // unknown length array => remove class from all fields
                        key = key.slice(0, -2); // remove [] at the end
                        $('[name^="' + key + '"]').removeClass(value);
                        return;
                    }
                    value += ' validation-failed validation-passed';

                    $('[id="' + key + '"]').removeClass(value);
                });
            });
        };

        this.setValidationRules = function (type) {
            var self = this;
            $.each(this.options.generalValidationRules, function (index, rules) {
                if (type == index) {
                    $.each(rules, function (key, value) {
                        $('[id="' + key + '"]').addClass(value);
                    });
                } else if (index != '-1') {
                    $.each(rules, function (key, value) {
                        $('[id="' + key + '"]').removeClass(value);
                    });
                }
            });

            if ($('[id="customer[logged]"]').val() == 'false') {
                if (type == 0) {
                    updateDocumentExpiryDate($('[id="customer[id_type]"]'));
                } else if (type == 1) {
                    updateDocumentExpiryDate($('[id="customer[contractant_id_type]"]'));
                }
            }
        };

        this.setCheckoutContainerValidationRules = function (container) {
            var self = this;

            container.find('input').each(function () {
                self.setCheckoutElementValidationRules(jQuery(this));
                jQuery(this).prop('disabled', false);
            });

            container.find('.hidden').each(function() {
                self.removeCheckoutContainerValidationRules(jQuery(this));
                jQuery(this).prop('disabled', true);
            });
        };

        this.removeCheckoutContainerValidationRules = function (container) {
            var self = this;

            container.find('input').each(function () {
                self.removeCheckoutElementValidationRules(jQuery(this));
                jQuery(this).prop('disabled', true);
            });
        };

        this.setCheckoutElementValidationRules = function (element) {
            var elementId = element.attr('id');

            if (elementId && elementId in this.options.checkoutValidationRules.id) {
                element.addClass(this.options.checkoutValidationRules.id[elementId]);
            } else {
                for (var key in this.options.checkoutValidationRules.class) {
                    if (element.hasClass(key)) {
                        element.addClass(this.options.checkoutValidationRules.class[key]);
                        break;
                    }
                }
            }
        };

        this.removeCheckoutElementValidationRules = function (element) {
            var elementId = element.attr('id');

            if (elementId && elementId in this.options.checkoutValidationRules.id) {
                element.removeClass(this.options.checkoutValidationRules.id[elementId]);
            } else {
                for (var key in this.options.checkoutValidationRules.class) {
                    if (element.hasClass(key)) {
                        element.removeClass(this.options.checkoutValidationRules.class[key]);
                        break;
                    }
                }
            }
        };
        
        this.handleCheckoutStepValidationRules = function (step) {
            // Add validation classes for each input
            jQuery('form#' + step + ' input').each(function () {
                checkout.setCheckoutElementValidationRules(jQuery(this));
            });

            // Remove validation classes for hidden elements
            jQuery('form#' + step + ' .hidden').each(function () {
                jQuery(this).find('input').each(function () {
                    checkout.removeCheckoutElementValidationRules(jQuery(this));
                });
            });
        };

        this.switchPortabilityRules = function (packageId, mode) {
            $.each(this.options.portabilityValidationRules, function (key, value) {
                key = key.replace('#', packageId);

                if (mode == 'add') {
                    $('[id="' + key + '"]').addClass(value);
                } else {
                    value += ' validation-failed validation-passed';
                    $('[id="' + key + '"]').removeClass(value);
                }
            });
        };

        this.switchClient = function (type) {

            $('[name="customer[type]"]').prop('checked', false);
            $('#customer-message').addClass('hidden');
            $('#business-message').addClass('hidden');
            $('#hardware-message').addClass('hidden');
            $('#show-rambours-method').addClass('hidden');

            //decide what message(s) to show
            if (!window['showHardwareError']) {
                $('#show-rambours-method').removeClass('hidden');
                $('#hardware-message').removeClass('hidden');
            }

            if (type == 0) {
                $('#saveCustomer #create-offer').addClass('hidden');
                $('[name="customer[type]"]').filter('[value="0"]').prop('checked', true).prop('disabled', false);

                var show = 'personal';
                var hide = 'business';
            } else if (type == 1) {
                if (!this.isOffer && (window['checkout'].ignoreOffer == undefined || window['checkout'].ignoreOffer == false)) {
                    $('#saveCustomer #create-offer').removeClass('hidden');
                }
                $('[name="customer[type]"]').filter('[value="1"]').prop('checked', true).prop('disabled', false);

                var show = 'business';
                var hide = 'personal';
            }

            this.checkQuoteAmount(type);
            this.setValidationRules(type);

            $('#' + show + '-contract').show();
            $('#overview-data-' + show).show();

            $('#' + hide + '-contract').hide();
            $('#overview-data-' + hide).hide();

            // When client type swaps, reset options in delivery
            var defaultDelivery = this.options.defaultdelivery[this.store];

            $('#cart #delivery-type .dropdown-menu li a[data-value="{0}"]'.format(defaultDelivery['delivery'])).trigger('click');
            $('#cart #payment-type .dropdown-menu li a[data-value="{0}"]'.format(defaultDelivery['payment'])).trigger('click');

            if ((type == 0 && window['quoteGrandTotal'] > window['consumerMaxAmount']) || (type == 1 && window['quoteGrandTotal'] > window['businessMaxAmount'])) {
                $('#payment-type #payment_method_text').text(Translator.translate('Choose'));
                $('[name="payment[method]"]').val('');
            }
            // Quote amount too big, disable all
            if (show === 'business' && window['quoteGrandTotal'] >= 10000) {
                $('#grandtotal-exceeded').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        };

        this.checkQuoteAmount = function (clientType) {
            if (clientType == 0) {
                //decide what message(s) to show
                if (window['quoteGrandTotal'] > window['consumerMaxAmount']) {
                    $('#show-rambours-method').removeClass('hidden');
                    $('#customer-message').removeClass('hidden');
                    window['showRamboursPaymentOption'] = 0;
                } else {
                    window['showRamboursPaymentOption'] = 1 && window['showHardwareError'];
                    if (window['showRamboursPaymentOption']) {
                        $('#show-rambours-method').addClass('hidden');
                        $('#customer-message').addClass('hidden');
                    }
                }
            } else if (clientType == 1) {
                //decide what message(s) to show
                if (window['quoteGrandTotal'] > window['businessMaxAmount']) {
                    $('#show-rambours-method').removeClass('hidden');
                    $('#business-message').removeClass('hidden');
                    window['showRamboursPaymentOption'] = 0;
                } else {
                    window['showRamboursPaymentOption'] = 1 && window['showHardwareError'];
                    if (window['showRamboursPaymentOption']) {
                        $('#show-rambours-method').addClass('hidden');
                        $('#business-message').addClass('hidden');
                    }
                }
            }
        };

        this.fillFormData = function (step) {
            var fn = window['fill' + step.charAt(0).toUpperCase() + step.slice(1) + 'Data'];

            if ('function' === typeof fn) {
                fn();
            }
        };

        this.prefillCustomerAddressData = function (data) {
            $('[id="address[otherAddress][city]"]').val(data.city);
            $('[id="address[otherAddress][postcode]"]').val(data.postcode);
            $('[id="address[otherAddress][street]"]').val(data.street_name);
            $('[id="address[otherAddress][houseno]"]').val(data.houseno);
            $('[id="address[otherAddress][addition]"]').val(data.addition);
            $('[id="address[telephone]"]').val(data.telephone);
            $('[id="address[fax]"]').val(data.fax);
        };

        this.prefillCustomerData = function (data) {
            // Business customers
            if (data.is_business == 1) {
                $('[id="customer[company_vat_id]"]').val(data.company_vat_id);
                $('[id="customer[company_coc]"]').val(data.company_coc);
                $('[id="customer[contractant_firstname]"]').val(data.contractant_firstname);
                $('[id="customer[contractant_middlename]]"]').val(data.contractant_middlename);
                $('[id="customer[contractant_lastname]"]').val(data.contractant_lastname);
                $('[id="customer[contractant_dob]"]').val(data.contractant_dob);
                $('[id="customer[contractant_id_number]"]').val(data.contractant_id_number);
                $('[id="customer[contractant_valid_until]"]').val(data.contractant_valid_until);
                $('[id="customer[contractant_issuing_country]"]').val(data.contractant_issuing_country);
                $('[id="customer[contractant_id_type]"]').val(data.contractant_id_type);
                $('[name="customer[contractant_gender]"]').filter('[value="' + data.contractant_gender + '"]').prop('checked', true);
                $('[name="customer[type]"]').filter('[value="' + '1' + '"]').prop('checked', true);
            } else {
                $('[name="customer[gender]"]').filter('[value="' + data.gender + '"]').prop('checked', true);
                $('[id="customer[firstname]"]').val(data.firstname);
                $('[id="customer[middlename]"]').val(data.middlename);
                $('[id="customer[lastname]"]').val(data.lastname);
                $('[id="customer[dob]"]').val(data.dob);
                $('[id="customer[id_type]"]').val(data.id_type);
                $('[id="customer[issuing_country]"]').val(data.issuing_country);
                $('[id="customer[valid_until]"]').val(data.valid_until);
                $('[id="customer[id_number]"]').val(data.id_number);
                $('[name="customer[type]"]').filter('[value="' + '0' + '"]').prop('checked', true);
            }
            $('[id="address[account_no]"]').val(data.account_no);
            $('[id="address[account_holder]"]').val(data.account_holder);

            // Privacy section
            if (data.privacy_sms == 1) $('[id="privacy[sms]"]').prop('checked', true); else $('[id="privacy[sms]"]').prop('checked', false);
            if (data.privacy_email == 1) $('[id="privacy[email]"]').prop('checked', true); else  $('[id="privacy[email]"]').prop('checked', false);
            if (data.privacy_mailing == 1) $('[id="privacy[mailing]"]').prop('checked', true); else $('[id="privacy[mailing]"]').prop('checked', false);
            if (data.privacy_telemarketing == 1) $('[id="privacy[telemarketing]"]').prop('checked', true); else $('[id="privacy[telemarketing]"]').prop('checked', false);
            if (data.privacy_number_information == 1) $('[id="privacy[number_information]"]').prop('checked', true); else $('[id="privacy[number_information]"]').prop('checked', false);
            if (data.privacy_phone_books == 1) $('[id="privacy[phone_books]"]').prop('checked', true); else $('[id="privacy[phone_books]"]').prop('checked', false);
            if (data.privacy_disclosure_commercial == 1) $('[id="privacy[disclosure_commercial]"]').prop('checked', true); else $('[id="privacy[disclosure_commercial]"]').prop('checked', false);
            if (data.privacy_market_research == 1) $('[id="privacy[market_research]"]').prop('checked', true); else $('[id="privacy[market_research]"]').prop('checked', false);
            if (data.privacy_sales_activities == 1) $('[id="privacy[sales_activities]"]').prop('checked', true); else $('[id="privacy[sales_activities]"]').prop('checked', false);

        };

        this.prefillCustomer = function (logged, customerData, addressData) {
            var inputs;
            if (logged == 'true') {
                this.prefillCustomerAddressData(addressData);
                this.prefillCustomerData(customerData);
                $('#client_type').hide();
                this.switchClient(customerData.is_business);

                inputs = $('#saveCustomer').serializeObject();

                //setOverview(inputs);

                this.removeGeneralValidationRules();

                //$('#client_info_prefilled').html($('#overview-data').html()).removeClass('hidden');
                //$('#client_info_input').addClass('hidden');
            } else {
                this.prefillCustomerAddressData(addressData);

                inputs = $('#saveCustomer').serializeObject();
                prefillBillingAddress(inputs.address.otherAddress);
            }
        };

        this.setCreditCheck = function (inputs) {
            var html = $('#overview-data').html();
            var phtml = $(html).not('#customer_privacy_overview').not('.privacy-separator');
            phtml.find('.edit-info').remove();
            $('#creditcheck-data').html(phtml);
        };

        this.hideErrorMessage = function (elem) {
            // fix for identity number error message not hiding when valid input on change identity type
            if ($(elem).val() != ''
                && Validation.validate(document.getElementById(elem.attr('id'))) === true
            ) {
                $(elem).parent().find('.validation-advice').hide('slow');
            } else if ($(elem).val() == '') {
                $(elem).parent().find('.validation-advice').hide('slow');
            }
        };

        this.addValidationClass = function (validationClass, elem) {
            var classes = elem.prop('class').split(' ');
            elem.prop('class', '');
            $.each(classes, function (i, className) {
                if (!/^validat/.test(className)) {
                    elem.addClass(className);
                }
            });
            elem.addClass(validationClass);
            this.hideErrorMessage(elem);
        };

        this.searchCompany = function (obj) {
            var container = $(obj).parents('#customer-company-search');
            var coc_field = container.find('input[id*="company_coc"]');
            var coc = coc_field.val();

            container.find('.searchCompanyEmpty').remove();
            coc_field.removeClass('validation-failed');

            $.post(
                this.checkoutController + 'searchCompany',
                {company_coc: $.trim(coc)},
                function (result) {
                    if (result.hasOwnProperty('serviceError')) {
                        showModalError(result.message, result.serviceError);
                        return;
                    } else if (result.error) {
                        coc_field.addClass('validation-failed');
                        coc_field.after('<p class="searchCompanyEmpty validation-advice" id="advice-customer[company][search][company_coc]">{0}</p>'.format(result.message));
                    } else {
                        coc_field.removeClass('validation-failed');
                        container.find('.searchCompanyEmpty').remove();

                        $('#business-input-address').hide();
                        $('#customer-company-search .enter-yourself').show();

                        /** Fill the text section **/
                        var company_address_content = $('#company-address-content');
                        $.each(result, function (field, value) {
                            var find_field = company_address_content.find('[id*="{0}"]'.format(field));
                            if (find_field.length) {
                                find_field.text(value);
                                find_field.trigger('change');
                            }
                        });
                        $('#business-input-address').show();
                        /** Fill the company inputs **/
                        var company_address_inputs = $('#business-input-address');
                        var company_legal_form = company_address_inputs.find('[id="customer[company_legal_form]"]');
                        //company_address_inputs.find('[id="customer[company_vat_id]"]').val('');
                        company_legal_form.val(company_legal_form.prop('defaultSelected'));
                        company_address_inputs.find('[id="customer[company_address][search][houseno]"]').val(result.houseno != '' ? result.houseno : '').trigger('change');
                        company_address_inputs.find('[id="customer[company_address][search][postcode]"]').val(result.postcode != '' ? result.postcode : '').trigger('change');
                        company_address_inputs.find('[id="customer[company_vat_id]"]').val(result.company_vat_id != '' ? result.company_vat_id : '').trigger('change');
                        company_address_inputs.find('[id="customer[company_name]"]').val(result.company_name != '' ? result.company_name : '').trigger('change');
                        company_address_inputs.find('[id="customer[company_coc]"]').val(result.k_v_k_number != '' ? result.k_v_k_number : '').trigger('change');
                        company_address_inputs.find('[id="customer[company_date]"]').val(result.date != '' ? result.date : '').trigger('change');
                        company_address_inputs.find('[id="customer[otherAddress][company_street]"]').val(result.street != '' ? result.street : '').trigger('change');
                        company_address_inputs.find('[id="customer[otherAddress][company_house_nr]"]').val(result.houseno != '' ? result.houseno : '').trigger('change');
                        company_address_inputs.find('[id="customer[otherAddress][company_postcode]"]').val(result.postcode != '' ? result.postcode : '').trigger('change');
                        company_address_inputs.find('[id="customer[otherAddress][company_city]"]').val(result.city != '' ? result.city : '').trigger('change');
                        $('#business-search-results-input').removeClass('hidden');
                        $('#business-search-results-input-edit').trigger('click');
                        coc_field.val(result.k_v_k_number);
                    }
                }
            );
        };

        this.updateLegitimationValidation = function (id) {
            var identificationType = $(id).parent().parent().parent().find('[name$="id_type]"]');
            var identificationNumber = $(id).parent().parent().parent().find('[name$="id_number]"]');
            var countryType = $(id).parent().parent().parent().find('[name$="issuing_country]"]');
            switch (identificationType.val()) {
                case 'P':
                    //set validation class
                    if ('NL' == countryType.val()) {
                        this.addValidationClass('validate-legitimation-number-p-nl', identificationNumber);
                    } else {
                        this.addValidationClass('validate-legitimation-number-p', identificationNumber);
                    }
                    break;
            }
        };

        this.createOffer = function (obj) {
            if (this.currentCheckoutStep == 'saveCustomer') {
                this.isOffer = 1;
                $('form#saveCustomer').submit();
            }
        };

        this.fillAddressFromSearch = function () {
            var selectedAddress = $('input[name="customer_address_search"]:checked').next();
            var addressData = {
                'postcode': selectedAddress.find('.address-search-postcode').html(),
                'city': selectedAddress.find('.address-search-city').html(),
                'street': selectedAddress.find('.address-search-street').html(),
                'houseno': selectedAddress.find('.address-search-houseno').html(),
                'addition': selectedAddress.find('.address-search-addition').html(),
                'district': selectedAddress.find('.address-search-district').html()
            };

            this.fillCustomerAddressData(addressData);
        };

        this.fillCustomerAddressData = function (data) {
            var idPrefix = this.searchButtonId.replace('[search]', '');

            $('[id="' + idPrefix + '[city]"]').val(data.city);
            $('[id="' + idPrefix + '[postcode]"]').val(data.postcode);
            $('[id="' + idPrefix + '[street]"]').val(data.street);
            $('[id="' + idPrefix + '[houseno]"]').val(data.houseno);
            $('[id="' + idPrefix + '[addition]"]').val(data.addition);
            if ($('[id="' + idPrefix + '[district]"]').length > 0) {
                $('[id="' + idPrefix + '[district]"]').val(data.district);
            }
        };
        
        this.initBankGenerator = function () {
            var template = Handlebars.compile($('#Bank-Data-Generator-Template').html());
            var responseHTML = template(this.bankData);

            $(this.bankGeneratorContainer).find('.modal-body').html(responseHTML);
        };
        
        this.displayBankGenerator = function (element) {
            this.bankGeneratorLinkIdPrefix = element.attr('id').replace('[generate]','');
            $(this.bankGeneratorContainer).modal();
        };

        this.getBankDataRequest = function () {
            var self = this;

            var params = {
                account_number: $('[id="bank_generator[account_number]"]').val(),
                bank_code: $('[id="bank_generator[bank_code]"]').val(),
                credit_provider: $('[id="bank_generator[credit_provider]"]').val(),
                usage: $('[id="bank_generator[usage]"]').val()
            };

            $.ajax({
                url: '/customerde/index/generateBankData/',
                type: "post",
                data: params,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.error == false) {
                        self.fillBankDataFields(response.bankData[0]);
                    } else {
                        showModalError(response.message);
                    }
                },
            });
        }

        this.validateIBANRequest = function() {
            var self = this;
            $ibanField = $('[id="payment[monthly][direct_debit][iban]"]');

            var params = {
                iban: $ibanField.val()
            };

            if (!jQuery('.iban-check-address').hasClass('hidden')) {
                params["addressExtra"] ={}
                jQuery('.iban-check-address').find("input").each(function(index ,el){
                    params["addressExtra"][$(this).attr("name")] = $(this).val();
                });
            }

            $.ajax({
                url: '/customerde/index/validateIBAN/',
                type: "post",
                data: params,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.error == false) {
                        // Invalid IBAN
                        if(response.valid == false) {
                            $ibanField.removeClass("validation-passed");
                            $ibanField.addClass("validation-failed");
                        } else {
                            // Invalid Address
                            if("validAddress" in response && response.validAddress == false) {
                                jQuery('.iban-check-address').find('input').attr('disabled', false);
                                jQuery('.iban-check-address').removeClass('hidden');

                                jQuery('.iban-field').addClass('validate-invalid-iban-street');
                                Validation.validate(jQuery('.iban-field')[0]);
                                jQuery('.iban-field').removeClass('validate-invalid-iban-street')
                            } else {
                                $ibanField.removeClass("validation-failed");
                            }
                        }
                    } else {
                        showModalError(response.message);
                    }
                }
            });
        };

        this.fillBankDataFields = function (data) {
            var idPrefix = this.bankGeneratorLinkIdPrefix;

            $('[id="' + idPrefix + '[iban]"]').val(data.ID);
        };

        this.initContractSend = function () {
            $(this.contractSendContainer).modal();
        };

        this.getOfferData = function () {
            return {
                firstName: $('[id="offer_data[firstname]"]').val(),
                lastName: $('[id="offer_data[lastname]"]').val(),
                postcode: $('[id="offer_data[postcode]"]').val(),
                city: $('[id="offer_data[city]"]').val(),
                street: $('[id="offer_data[street]"]').val(),
                houseno: $('[id="offer_data[houseno]"]').val(),
                addition: $('[id="offer_data[addition]"]').val(),
                telephonePrefix: $('[id="offer_data[telephone_prefix]"]').val(),
                telephoneNumber: $('[id="offer_data[telephone]"]').val(),
                email: $('[id="offer_data[email]"]').val()
            };
        };

        this.sendOfferData = function () {
            var self = this;
            var offerData = self.getOfferData();

            $.ajax({
                url: MAIN_URL + 'checkout/index/sendOffer',
                type: 'post',
                data: offerData,
                dataType: 'json',
                success: function (response) {
                    if (response.error == false) {
                        console.log(response.message);
                    } else {
                        showModalError(response.message);
                    }
                },
            });
        };
    };

    Checkout.prototype = {

        cartContainer: '#cart-content',
        modalId: '#polling-modal',

        modalOptions: {
            '1': 'axi', //AXI
            '0': 'da'  //DealerAdapter
        },

        clientType: 0, // 0 = Personal, 1 = Business

        options: {
            activesteps: {
                0: 'saveCustomer'
            },
            defaultdelivery: {
                'telesales': {
                    'delivery': 'deliver',
                    'payment': 'cashondelivery'
                },
                'vodafone_webshop': {
                    'delivery': 'deliver',
                    'payment': 'cashondelivery'
                },
                'retail': {
                    'delivery': 'direct',
                    'payment': 'payinstore'
                },
                'belcompany': {
                    'delivery': 'direct',
                    'payment': 'payinstore'
                },
                'indirect': {},
                'belcompany_webshop': {
                    'delivery': 'deliver',
                    'payment': 'cashondelivery'
                },
                'vodafone_traders': {
                    'delivery': 'deliver',
                    'payment': 'cashondelivery'
                }
            },
            defaultsplitdelivery: {
                'telesales': {
                    'delivery': 'billing_address',
                    'payment': 'cashondelivery'
                },
                'vodafone_webshop': {
                    'delivery': 'billing_address',
                    'payment': 'cashondelivery'
                },
                'retail': {
                    'delivery': 'direct',
                    'payment': 'payinstore'
                },
                'belcompany': {
                    'delivery': 'direct',
                    'payment': 'payinstore'
                },
                'indirect': {},
                'vodafone_traders': {
                    'delivery': 'billing_address',
                    'payment': 'cashondelivery'
                },
                'belcompany_webshop': {
                    'delivery': 'billing_address',
                    'payment': 'cashondelivery'
                }
            },
            'checkoutValidationRules': {
                 'id': {
                    // General fields
                    'customer[gender][1]': 'validate-one-required-by-name',
                    'customer[firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'customer[dob]': 'required-entry validate-date-de',
                    'customer[id_number]': 'required-entry validate-customer-id-number',
                    'customer[nationality]': 'required-entry validate-nationality',
                    'customer[other_identity][card_no]': 'required-entry validate-other-identity-card-number',
                    'customer[other_identity][exp_date]': 'required-entry validate-expiration-date-de',
                    'customer[address][postcode]': 'required-entry validate-postcode-de',
                    'customer[address][city]': 'required-entry',
                    'customer[address][district]': 'required-entry',
                    'customer[address][street]': 'required-entry',
                    'customer[address][houseno]': 'required-entry validate-number',
                    'customer[contact_person][name]': 'required-entry',
                    'customer[contact_person][gender][1]': 'validate-one-required-by-name',
                    'provider[other_address][postcode]': 'required-entry',
                    'provider[other_address][city]': 'required-entry',
                    'provider[other_address][street]': 'required-entry',
                    'provider[other_address][houseno]': 'required-entry',
                    'provider[other_owner][firstname]': 'required-entry',
                    'provider[other_owner][lastname]': 'required-entry',
                    'delivery[other_address][gender][1]': 'validate-one-required-by-name',
                    'delivery[other_address][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'delivery[other_address][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'delivery[other_address][company_name]': 'required-entry',
                    'delivery[other_address][co_name]': 'required-entry',
                    'delivery[other_address][postcode]': 'required-entry validate-postcode-de',
                    'delivery[other_address][city]': 'required-entry',
                    'delivery[other_address][district]': 'required-entry',
                    'delivery[other_address][street]': 'required-entry',
                    'delivery[other_address][houseno]': 'required-entry validate-number',
                    'billing[other_address][gender][1]': 'validate-one-required-by-name',
                    'billing[other_address][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'billing[other_address][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'billing[other_address][postcode]': 'required-entry validate-postcode-de',
                    'billing[other_address][city]': 'required-entry',
                    'billing[other_address][street]': 'required-entry',
                    'billing[other_address][houseno]': 'required-entry validate-number',
                    'billing[other_address][pobox]': 'required-entry',
                    'payment[monthly][direct_debit][account_holder]': 'required-entry',
                    'payment[monthly][direct_debit][iban]': 'required-entry',
                    'payment[monthly][alternative][gender][1]': 'validate-one-required-by-name',
                    'payment[monthly][alternative][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'payment[monthly][alternative][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'payment[monthly][alternative][postcode]': 'required-entry validate-postcode-de',
                    'payment[monthly][alternative][city]': 'required-entry',
                    'payment[monthly][alternative][street]': 'required-entry',
                    'payment[monthly][alternative][houseno]': 'required-entry validate-number',
                    'payment[monthly][self_transfer][gender][1]': 'validate-one-required-by-name',
                    'payment[monthly][self_transfer][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'payment[monthly][self_transfer][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'payment[monthly][self_transfer][postcode]': 'required-entry validate-postcode-de',
                    'payment[monthly][self_transfer][city]': 'required-entry',
                    'payment[monthly][self_transfer][street]': 'required-entry',
                    'payment[monthly][self_transfer][houseno]': 'required-entry validate-number',
                    'payment[one_time][cash][gender][1]': 'validate-one-required-by-name',
                    'payment[one_time][cash][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'payment[one_time][cash][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'payment[one_time][cash][postcode]': 'required-entry validate-postcode-de',
                    'payment[one_time][cash][city]': 'required-entry',
                    'payment[one_time][cash][street]': 'required-entry',
                    'payment[one_time][cash][houseno]': 'required-entry validate-number',
                    'other[phonebook_entries][1][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'other[phonebook_entries][2][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'other[phonebook_entries][3][firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'other[phonebook_entries][1][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'other[phonebook_entries][2][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'other[phonebook_entries][3][lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'other[prev_conn][prev_provider]': 'validate-other-previous-provider-de',
                    'other[conn_location][floor]': 'validate-other-floor-de',
                    'other[conn_location][position][3]': 'validate-one-required-by-name',
                    'other[student][matriculation_nr]': 'required-entry',
                    'other[student][institution_location]': 'required-entry',
                    // Soho fields
                    'customer[business][commercial_register][2]': 'validate-one-required-by-name',
                    'customer[address][commercial_register_type]': 'validate-business-commercial-register-type-de',
                    'customer[address][company_registration_number]': 'required-entry',
                    'customer[address][company_trade_location]': 'required-entry'
                },
                'class': {
                    'customer-contact-person-lastname': 'required-entry validate-name validate-length maximum-length-60',
                    'customer-contact-person-firstname': 'required-entry validate-name validate-length maximum-length-12',
                    'customer-contact-person-telephone': 'required-entry validate-foreign-number',
                    'customer-contact-person-gender[female]': 'validate-one-required-by-name',
                    'customer-contact-person-gender[male]': 'validate-one-required-by-name',
                    'provider[prefix_transfer]': 'required-entry',
                    'provider[phone_transfer]': 'required-entry'
                }
            },
            'generalValidationRules': {
                '-1': {
                    'address[otherAddress][postcode]': 'required-entry nl-postcode',
                    'address[otherAddress][houseno]': 'required-entry validate-number',
                    'address[otherAddress][city]': 'required-entry',
                    'address[foreignAddress][houseno]': 'validate-number',
                    'address[telephone]': 'validate-porting-number validate-foreign-number required-entry',
                    'address[account_no]': 'required-entry',
                    'address[account_holder]': 'required-entry validate-length maximum-length-40',
                    'additional[email][0]': 'required-entry validate-email validate-backend-email',
                    'additional[telephone][]': 'validate-porting-number validate-foreign-number required-entry'
                },
                '0': {
                    'customer[firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'customer[middlename]': 'validate-name validate-length maximum-length-100',
                    'customer[lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'customer[dob]': 'required-entry validate-date-nl validate-min-age validate-max-age',
                    'customer[id_type]': 'validate-select',
                    'customer[id_number]': 'required-entry',
                    'customer[valid_until]': 'required-entry validate-date-nl validate-date-until validate-date-expire-6-months',
                    'customer[issuing_country]': 'validate-select',
                    'customer[company_address][search][houseno]': 'validate-number',
                    'customer[foreignAddress][company_house_nr]': 'validate-number',
                    'customer[otherAddress][company_house_nr]': 'validate-number',
                    'customer[gender][2]': 'validate-one-required-by-name'
                },
                '1': {
                    'customer[company_vat_id]': 'validate-vat-nl',
                    'customer[company_coc]': 'required-entry',
                    'customer[company_date]': 'required-entry validate-date-nl validate-date-past',
                    'customer[company_name]': 'required-entry validate-name validate-length maximum-length-60',
                    'customer[company_legal_form]': 'validate-select',
                    'customer[contractant_firstname]': 'required-entry validate-name validate-length maximum-length-12',
                    'customer[contractant_middlename]': 'validate-name validate-length maximum-length-100',
                    'customer[contractant_lastname]': 'required-entry validate-name validate-length maximum-length-60',
                    'customer[contractant_dob]': 'required-entry validate-date-nl validate-min-age validate-max-age',
                    'customer[contractant_id_type]': 'validate-select',
                    'customer[contractant_id_number]': 'required-entry',
                    'customer[contractant_valid_until]': 'required-entry validate-date-nl validate-date-until validate-date-expire-6-months',
                    'customer[contractant_issuing_country]': 'validate-select',
                    'customer[contractant_gender][2]': 'validate-one-required-by-name'
                }
            },
            'portabilityValidationRules': {
                'portability[#][number_porting_type]': 'validate-select',
                'portability[#][type]': 'validate-select',
                'portability[#][mobile_number]': 'required-entry validate-porting-number',
                'portability[#][sim]': 'required-entry',
                'portability[#][contract]': 'required-entry',
                'portability[#][current_provider]': 'validate-select',
                'portability[#][end_date_contract]': 'validate-porting-date'
            }
        },

        lastStep: 'saveCustomer',

        initialize: function (options) {
            var self = this;
            this.options = $.extend(this.options, options);

            // Activate checkout steps
            $.each(this.options.activesteps, function (index, step) {
                self.activateStep(step);
                self.lastStep = step;
            });
        },

        activateStep: function (step) {
            var stepSection = $('#{0}'.format(step)).parents('.bs-docs-section');
            var to_enable = '#cart-left .bs-sidebar .nav li a[href="#' + stepSection.children('form').children('h4').first().attr('id') + '"]';

            stepSection.removeClass('hidden');
            $(to_enable).parent().removeClass('disabled');
            $(this.cartContainer).scrollspy('refresh');
        },

        checkOption: function (selectId, value) {
            var select = $('[id="{0}"]'.format(selectId));
            select.find('option[value="{0}"]'.format(value)).prop('selected', true).trigger('change');
        },

        showVodError: function (doShow, vodMessage, type, showRetry) {
            var modal = $(this.modalId);
            if (doShow == true && vodMessage != undefined) {
                if (!$(modal).is(':visible')) {
                    modal.appendTo('body').modal();
                }
                modal.find('.modal-vod-error-message').html(vodMessage);
                modal.find('.modal-title-processing').addClass('hidden');
                modal.find('.modal-title-error').removeClass('hidden');
                modal.find('#progress-bar').parent().addClass('hidden');
                modal.find('#vod-popup-error').removeClass('hidden');
                modal.find('#vod-popup-error h3.bolded:first-child').html(type == 0 ? 'Unify Error' : 'AXI Error');
                modal.find('#vod-popup-error').find('.error-img').addClass('hidden');
                modal.find('#vod-popup-error').find('.error-img.' + this.modalOptions[type]).removeClass('hidden');
                if (showRetry == 0) {
                    modal.find('#vod-popup-error').find('.btn.btn-warning').addClass('hidden');
                } else {
                    modal.find('#vod-popup-error').find('.btn.btn-warning').removeClass('hidden');
                }
            } else {
                modal.find('.modal-vod-error-message').html('');
                modal.find('.modal-title-processing').removeClass('hidden');
                modal.find('.modal-title-error').addClass('hidden');
                modal.find('#progress-bar').parent().removeClass('hidden');
                modal.find('#vod-popup-error').addClass('hidden');
            }
        }
    };
})(jQuery);

function disableAll() {
    checkoutDeliverPageDialog = false;
    jQuery('#cart-left li').addClass('disabled').removeClass('active');
    jQuery('#cart-left li a.post-order').attr('href', '');
    jQuery('#confirmation-menu').removeClass('hidden').removeClass('disabled').addClass('active');
    var all = jQuery('.bs-docs-section:not(:last)');
    var hiddenCount = 0;
    all.each(function (id, el) {
        var currentEl = jQuery(el);
        if (!currentEl.hasClass('hidden')) {
            currentEl.addClass('hidden');
            ++hiddenCount;
        }
    });

    window.canShowSpinner = false;
    jQuery.post(MAIN_URL + 'customerde/index/unloadCustomer', {}, function (response) {
        if (response.sticker) {
            jQuery('.col-left .sticker').html(response.sticker);
            jQuery('.guest').removeClass('guest');
        }
        return true;
    });
    window.canShowSpinner = true;

    if (hiddenCount > 0) {
        jQuery('#cart-content').scrollspy('refresh');
    }
}

function cloneField(field, prefix) {
    if (prefix === "email") {
        jQuery('.used-email-context.hide').removeClass('hide');
    }
    clonedFieldsIndex++;
    var fieldClone = jQuery(field).parent().children("input").first().clone().removeAttr('readonly').removeAttr('value').removeAttr('id').attr('name', 'additional[' + prefix + '][' + clonedFieldsIndex + ']').removeClass('validation-failed validation-passed').wrap("<div />").parent().html();
    var container = '<div class="input-group" style="margin-top:5px">' + fieldClone + '<span class="input-group-btn"><button class="btn btn-default remove-above" onclick="removeAbove(this)" type="button">x</button></span></div>';
    jQuery(field).prev().after(container);

    // Update the form data with the new field
    form.saveCustomer = new VarienForm('saveCustomer', false);
}

function removeAbove(field) {
    var parent = jQuery(field).parent().parent().parent();
    jQuery(field).parent().parent().remove();
    if (!parent.find('.input-group').length) {
        jQuery('.used-email-context').addClass('hide');
    }

    // Update the form data and remove the field
    form.saveCustomer = new VarienForm('saveCustomer', false);
}

function scrollAnimateTo(elem, container) {
    var scrollTo = jQuery(elem);
    if (scrollTo.length) {
        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() + 5
        });
    }
}

function scrollToSection(thisForm) {
    var nextSection = thisForm.parents('.bs-docs-section').nextAll('.bs-docs-section').first();

    if (nextSection.hasClass('skip')) {
        scrollToSection(nextSection.children('.cart'));
        return;
    }

    nextSection.removeClass('hidden');
    var to_enable = '#cart-left .bs-sidebar .nav li a[href="#' + nextSection.children('form').children('h3').first().attr('id') + '"]';
    jQuery(to_enable).parent().removeClass('disabled');
    jQuery('#cart-content').scrollspy('refresh');

    var elem = nextSection.find('.page-header:first'),
        container = jQuery('#cart-content');

    scrollAnimateTo(elem, container);
}

function processPreviousSteps(step) {
    if (step == 'saveOverview') {
        var inputs = jQuery('#saveCustomer').serializeObject();
        //setOverview(inputs);
        checkout.setCreditCheck(inputs);
    } else if (step == 'saveSuperOrder') {
        if (!jQuery('#saveSuperOrder #accept').hasClass('processed')) {
            // Check if Delivery address or Number Porting have any changes in edit order mode
            if ((undefined != form.saveDeliveryAddressData && form.saveDeliveryAddressData != jQuery('#saveDeliveryAddress').serialize())
                || (undefined != form.saveNumberPortingData && form.saveNumberPortingData != jQuery('#saveNumberPorting').serialize())) {
                // Enable the accept conditions button
                jQuery('#saveSuperOrder #accept').prop('checked', true).parent().removeClass('disabled');
                jQuery('#saveSuperOrder #submit').prop('disabled', false);
            }

            if ((undefined != form.saveDeliveryAddressData && form.saveDeliveryAddressData == jQuery('#saveDeliveryAddress').serialize())
                && (undefined != form.saveNumberPortingData && form.saveNumberPortingData == jQuery('#saveNumberPorting').serialize())) {
                // Disable the accept conditions button
                jQuery('#saveSuperOrder #accept').prop('checked', false).parent().addClass('disabled');
                jQuery('#saveSuperOrder #submit').prop('disabled', true);
                jQuery('#saveDeliveryAddress .validation-failed').removeClass('validation-failed');
                jQuery('#saveDeliveryAddress').find('.ajax-validation').fadeOut('slow', function () {
                    jQuery(this).remove();
                });
                jQuery('#saveDeliveryAddress').find('.cart-errors').remove();

                jQuery('#saveNumberPorting .validation-failed').removeClass('validation-failed');
                jQuery('#saveNumberPorting').find('.ajax-validation').fadeOut('slow', function () {
                    jQuery(this).remove();
                });
                jQuery('#saveNumberPorting').find('.cart-errors').remove();
            }
        }
    } else if (step == 'showOrderConfirmation') {
        disableAll();
        showOrderConfirmationPage();
    }
}

function showOrderConfirmationPage() {
    jQuery('#checkout-title').addClass("hidden");
    jQuery('#checkout-title').hide();
    jQuery('#order-submitted-title').removeClass("hidden");
    jQuery('#order-submitted-title').show();
    jQuery('#cart-left').addClass("hidden");
    jQuery('#cart-left').hide();
}
function scrollToExactSection(thisForm) {
    var nextSection = thisForm.parents('.bs-docs-section').first();
    var cartContent = jQuery('#cart-content');
    cartContent.scrollspy('refresh');
    var elem = nextSection.find('h3:first');

    scrollAnimateTo(elem, cartContent);
}

function continueWithoutSigning() {
    jQuery(location).attr('href', '/');
}

function submitSteps() {
    var ns = jQuery('#saveNumberSelection');
    var hn = jQuery('#saveNewNetherlands');
    var submit = false;
    if (ns.length > 0) {
        ns.submit();
        submit = true;
    }

    if (hn.length > 0) {
        hn.submit();
        submit = true;
    }
    if (submit) {
        jQuery(document).ajaxStop(function () {
            continueWithoutSigning();
        });
    } else {
        continueWithoutSigning();
    }
}

function redoCreditCheck(superorderId) {
    checkout.showVodError(false);
    if (superorderId) {
        var ccModal = Polling.show();

        var polling = new Polling('redoCreditCheck', 6120);
        polling.updateModal();
        jQuery.ajax({
            async: true,
            type: 'POST',
            url: MAIN_URL + 'checkout/index/redoCreditCheck',
            data: {'superorder_id': superorderId},
            success: function (data) {
                window.canShowSpinner = true;
                if (data.error) {
                    polling.pollingFailed(data.message);
                } else {
                    polling.updatePolling(data.polling_id);
                    ccModal.find('#polling-superorder-id').val(data.superorder_id);
                    ccModal.find('#polling-superorder-no').val(data.superorder_number);
                    updateContractIds(data.contract_data, data.superorder_number);
                    jQuery('#order-increment-id').text(data.superorder_number);
                    jQuery('#credit-check-cancel-button').attr('onclick', 'cancelOrder(' + data.superorder_number + ')');
                }
            }
        });
    }
}

function doCreditCheck(addresses, orderId, creditFailed) {
    checkout.showVodError(false);
    if (!addr || jQuery.isEmptyObject(addr)) {
        addr = addresses;
    }

    if (!cFailed) {
        cFailed = creditFailed;
    }

    jQuery.each(addresses, function (id, el) {
        if (jQuery('#credit-check-address-' + id).length != 0) {
            jQuery('#credit-check-address-' + id).html(el['street']);
            if (("houseno" in el))
                jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + ' ' + el['houseno']);
            if (("addition" in el))
                jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + ' ' + el['addition']);
            jQuery('#credit-check-address-' + id).html(jQuery('#credit-check-address-' + id).html() + '<br />' + el['postcode'] + ' ' + el['city']);
        }
        creditCheck[id] = {'id': id, 'checked': false};
    });
    jQuery('.bs-docs-section').addClass('hidden');
    jQuery('#creditcheck-waiting').removeClass('hidden');
    jQuery('#cart-left li').addClass('disabled');
    jQuery('#cart-left li a.pre-order').attr('href', '');
    jQuery('#credit-check-menu').removeClass('hidden').removeClass('disabled').addClass('active');
    if (orderId) {
        jQuery('#credit-check-cancel-button').attr('onclick', 'cancelOrder(' + orderId + ')');
        jQuery('#order-edit').attr('onclick', 'editOrder()');
        //jQuery('#redo-credit-check').attr('onclick', 'redoCreditCheck(' + orderId + ')');
    } else {
        jQuery('#credit-check-cancel-button').addClass('hidden');
    }
    jQuery('#cart-content').scrollspy('refresh');
    if (!creditFailed) {
        performCreditCheck(orderId);
        intervalID = setInterval(function () {
            performCreditCheck(orderId);
        }, 5000);
    } else {
        showCreditCheckFailed();
    }
}

function updateNumberSelectionList() {
    if (!localStorage.getItem('numberList')) {
        jQuery.ajax({
            async: true,
            type: 'POST',
            url: MAIN_URL + 'checkout/index/retrieveNumberList',
            success: function (data) {
                if (data.error) {
                    showModalError(data.message);
                    jQuery('#search-phone-number').modal('hide');
                } else {
                    localStorage.setItem('numberList', JSON.stringify(data.numbers));
                    // Clear the numbers after 10 minutes to refresh the list
                    setTimeout(function () {
                        localStorage.removeItem('numberList');
                    }, 10 * 60 * 1000);
                    updateNumberSelection();
                }
            },
            error: function () {
                console.log('Failed to get numbers list');
            }
        });
    } else {
        updateNumberSelection();
    }
}

function showNumberList(target) {
    updateNumberSelectionList();
    var modalDiv = jQuery('#search-phone-number');
    var phoneNo = jQuery('[id="{0}"]'.format(target)).val();

    modalDiv.data("target-input", target);
    modalDiv.find('#numbersFound input').filter('[data-phone-number="{0}"]'.format(phoneNo)).first().prop('checked', true);
    modalDiv.modal();
}

var deviceSelectionValidationCallback = function (result, elm) {
    if (result == false && jQuery(elm).parent().hasClass('input-group')) {
        var validationAdvice = jQuery(elm).siblings('.validation-advice');
        if (validationAdvice.length > 0) {
            jQuery.each(validationAdvice, function (index, value) {
                jQuery(value).parent().parent().append(jQuery(value));
            });
        }
    }
};

function updateNumberSelection(data) {
    if (data) {
        var test = jQuery(data).find('#numbers-list').parent().html();
        jQuery('#numbers-list').parent().html(test);
        jQuery('#numbers-list input').on('blur change', function (e) {
            var el = jQuery(this);
            el.removeClass('validation-failed');
            el.parent().find('.ajax-validation').fadeOut('slow', function () {
                jQuery(this).remove();
            });

            Validation.validate(this);
        });
    }

    if (localStorage.getItem('numberList')) {
        var numbers = jQuery.parseJSON(localStorage.getItem('numberList'));
        //Fill the modal with the received numbers
        var options = '';
        numbers.each(function (id) {
            options += '<tr><td><div class="radio"><label><input type="radio" name="phone-number" value="1" data-phone-number="' + id + '">' + id + '</label></div></td></tr>';
        });
        jQuery('#numbersFound').find('tbody').html(options);
    }
}

function formatPrice(price) {
    var test = /^([0-9]|\.|\,)+$/.test(price);
    var value = 0;
    if (test) {
        if (price.indexOf(',') != -1) {
            if (price.indexOf('.') != -1 && price.indexOf('.') > price.indexOf(',')) {
                value = price.replace(',', '');
            } else {
                value = price.replace('.', '').replace(',', '.');
            }
        } else {
            if ((("" + price).split(".").length - 1) < 2) {
                value = price;
            } else {
                value = price.replace('.', '');
            }
        }
    }
    return value;
}

function pad(price) {
    var split = ("" + price).split('.');
    var str = '';
    if (split.length - 1 == 1) {
        if (split[1].length < 2) {
            for (var i = split[1].length; i < 2; i++) {
                str += '0';
            }
        }
    } else {
        str = '.00';
    }

    return (price + str).replace('.', ',');
}


function updateContract(data) {
    if (data) {
        var test = jQuery(data).find('#contract-content').html();
        jQuery('#contract-content').html(test);
    }
}

function updateContractIds(data, superorder_id) {
    jQuery('#contract_page').find('[name="superorder_id"]').val(superorder_id);
    jQuery('a.print-contract-url').attr('href', data.contract_url);
    jQuery.each(data.increment_ids, function (index, value) {
        var orderClass = '#contract_page .order-' + index + ' h3';
        jQuery(orderClass).text(value);
    });
}

function checkIfAllChecked() {
    var checked = true;
    jQuery('[name^="contract[signed]"]').each(function (e, v) {
        if (!jQuery(v).is(':checked')) {
            checked = false;
            return false;
        }
    });

    if (checked) {
        jQuery('#contract-continue-button').removeClass('disabled');
    } else {
        jQuery('#contract-continue-button').addClass('disabled');
    }
}

function moveQuoteToNewCustomer() {
    jQuery(location).attr('href', MAIN_URL + 'customer/details/moveQuoteToNewCustomer');
}

function prefillBillingAddress(address) {
    jQuery('.billing-address-hidden').each(function () {
        var item = this;
        var packageId = jQuery(item).data('package-id');
        if (typeof packageId != 'undefined') {
            var targetDiv = jQuery('.billing-address[data-package-id=' + packageId + ']');
        } else {
            var targetDiv = jQuery('#billing-address');
        }

        jQuery(item).find('input[name*="street"]').val(address.street);
        jQuery(item).find('input[name*="houseno"]').val(address.houseno);
        jQuery(item).find('input[name*="addition"]').val(address.addition);
        jQuery(item).find('input[name*="postcode"]').val(address.postcode);
        jQuery(item).find('input[name*="city"]').val(address.city);

        if (jQuery.trim(address.street + address.houseno + address.addition + address.postcode + address.city) != '') {
            var html = address.street + ' ' + address.houseno + ' ' + address.addition + '<br />' + address.postcode + ', ' + address.city;
            targetDiv.html(html).removeClass('hidden');
        }
    });
}

function removeAboveRow(self) {
    var remove = jQuery(self).data('remove');
    jQuery(self).parents(remove).first().remove();
}

function addExtraAddressLine(self) {
    var cloned = jQuery(self).parent().find('.clone-template');
    var newLine = jQuery(cloned).clone().removeClass("clone-template");
    newLine.find('.form-group').addClass('input-group');
    newLine.css('margin-top', '5px').find('input').val('').parent().append('<span class="input-group-btn"><button class="btn btn-default input-sm remove-above" data-remove=".col-xs-12" onclick="removeAboveRow(this)" type="button">x</button></span>');
    jQuery(self).prev().after(newLine);
}

function cancelOrder(superOrderId) {
    if (superOrderId) {
        checkoutDeliverPageDialog = false;
        Polling.show();
        jQuery.ajax({
                type: "POST",
                url: MAIN_URL + 'checkout/index/cancelOrder',
                data: {'super_order': superOrderId},
                async: true
            })
            .done(function (response) {
                window.canShowSpinner = true;
                if (response.error) {
                    Polling.hide();
                    showModalError(response.message);
                } else {
                    Polling.start('cancelOrder', response.polling_id);
                }
            });
    }
}

function setPhoneNumber(self) {
    self = jQuery('#numbersFound').find('input[name=phone-number]').filter(':checked');
    if (self.length > 0) {
        var target = jQuery('#search-phone-number').data('target-input');
        jQuery('[id="' + target + '"]').val(self.data('phone-number'));
        jQuery('[id="' + target + '"]').trigger('change');
    }
    jQuery('#search-phone-number').modal('hide');
}

function checkoutDeliveryForce(self) {
    var parent = jQuery(self).parents('.modal');
    parent.modal('hide');
    functionName = parent.data('function');
    param1 = parent.data('param1');
    param2 = parent.data('param2');
    param3 = parent.data('param3');
    checkoutDeliverPageDialog = false;
    if (functionName.split('.').length == 1) {
        window[functionName.split('.')[0]](param1, param2, param3);
    } else {
        window[functionName.split('.')[0]][functionName.split('.')[1]](param1, param2, param3);
    }
}

function showProcessOrderModal(id) {
    // Deactivate previous steps and go to next available step
    disableAll();
    jQuery('.bs-docs-section').addClass('hidden');
    jQuery('#cart-left li').addClass('disabled');
    jQuery('#cart-left li a.pre-order').attr('href', '');
    scrollToSection(jQuery('#saveOverview'));

    // TODO: do we really need to start the polling on Telesales?!
    var polling = new Polling('saveOverview');
    polling.updatePolling(id);
}

function deliverOrder(orderId, packageId) {
    window.location.href = MAIN_URL + 'checkout/cart?orderId=' + orderId + '&packageId=' + packageId;
}

function showRefundModal() {
    if (checkout.validatePreviousSteps()) {
        jQuery('#order_refund_modal').appendTo('body').modal();
        var method = jQuery('.refund_method_select');

        if (method.length) {
            method.trigger('change');
        }
    }
}

function setDropdownDefault(dropdownClass, defaultValueText, packageId) {
    jQuery('[data-package-id=' + packageId + '] ' + '.' + dropdownClass).each(function (index) {
        if (jQuery(this).has('[selected]').length == 0) {
            jQuery(this)
                .prepend(jQuery("<option></option>")
                    .attr("value", "")
                    .attr("selected", "selected")
                    .text(defaultValueText));
        }
    });
}

function switchProvider(packageId) {
    var key = "portability[" + packageId + "][number_porting_type]";
    var keyDate = "portability[" + packageId + "][end_date_contract]";
    var sel = jQuery('[id="' + key + '"]').val();
    var key2 = "portability[" + packageId + "][contract]";
    var simKey = "portability[" + packageId + "][sim]";
    var currentProvider = "portability[" + packageId + "][current_provider]";
    var currentOperator = "portability[" + packageId + "][current_operator]";
    if (sel == 1) {
        jQuery('[id="' + key2 + '"]').val('');
        jQuery('[id="' + key2 + '"]').addClass('required-entry');
        jQuery('[id="' + key2 + '"]').parent('div').show();

        jQuery('[id="' + simKey + '"]').val('');
        Validation.validate(jQuery('[id="' + simKey + '"]').attr('name'));
        Validation.reset(jQuery('[id="' + simKey + '"]').attr('name'));
        jQuery('[id="' + simKey + '"]').next('.validation-advice').remove();
        jQuery('[id="' + currentProvider + '"]').val(undefined);
        Validation.reset(jQuery('[id="' + currentProvider + '"]').attr('name'));
        jQuery('[id="' + currentProvider + '"]').next('.validation-advice').remove();
        jQuery('[id="' + currentOperator + '"]').val(undefined);

        jQuery('[id="' + simKey + '"]').parent('div').hide();
        jQuery('[id="' + simKey + '"]').removeClass('required-entry');
    }
    else {
        jQuery('[id="' + key2 + '"]').val('');
        jQuery('[id="' + key2 + '"]').parent('div').hide();
        jQuery('[id="' + key2 + '"]').removeClass('required-entry');

        jQuery('[id="' + simKey + '"]').parent('div').show();
        jQuery('[id="' + simKey + '"]').addClass('required-entry');


        jQuery('[id="' + currentProvider + '"]').val(undefined);
        Validation.reset(jQuery('[id="' + currentProvider + '"]').attr('name'));
        jQuery('[id="' + currentProvider + '"]').next('.validation-advice').remove();
        jQuery('[id="' + currentOperator + '"]').val(undefined);
    }
}

function sortDropdown(dropdownClass, packageId) {
    var options = jQuery('[data-package-id=' + packageId + '] ' + '.' + dropdownClass + ' option');
    var selected = jQuery('[data-package-id=' + packageId + '] ' + '.' + dropdownClass + ' option:selected').val();
    var arr = options.map(function (_, o) {
        return {
            t: jQuery(o).text().trim(),
            k: jQuery(o).data('validate'),
            v: o.value.trim()
        };
    }).get();
    arr.sort(function (o1, o2) {
        return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
    });
    options.each(function (i, o) {
        var element = jQuery(o);
        o.value = arr[i].v;
        if (arr[i].k != undefined) {
            element.data('validate', arr[i].k);
        }
        element.text(arr[i].t);
        element.prop('selected', false);
        // Keep the selected value
        if (o.value == selected) {
            element.prop('selected', true);
        }
    });
}
/**
 * With the back button from the last step go to the first step if the checkout (the
 */
function backToFirstStep() {
    var elem = '#klantgegevens',
        container = jQuery('#cart-content');

    scrollAnimateTo(elem, container);
}

// Override createAdvice to show all error when customer is logged in
Object.extend(Validation, {
    createAdvice: function (name, elm, useTitle, customError) {
        var v = Validation.get(name);
        var errorMsg = useTitle ? ((elm && elm.title) ? elm.title : v.error) : v.error;
        if (customError) {
            errorMsg = customError;
        }
        try {
            if (Translator) {
                errorMsg = Translator.translate(errorMsg);
            }
        }
        catch (e) {}

        advice = '<div class="validation-advice" id="advice-' + name + '-' + Validation.getElmID(elm) + '" style="display:none">' + errorMsg + '</div>'

        Validation.insertAdvice(elm, advice);
        advice = Validation.getAdvice(name, elm);
        if ($(elm).hasClassName('absolute-advice')) {
            var dimensions = $(elm).getDimensions();
            var originalPosition = Position.cumulativeOffset(elm);

            advice._adviceTop = (originalPosition[1] + dimensions.height) + 'px';
            advice._adviceLeft = (originalPosition[0]) + 'px';
            advice._adviceWidth = (dimensions.width) + 'px';
            advice._adviceAbsolutize = true;
        }

        if (jQuery('[id="customer[logged]"]').val() == 'true' && jQuery(elm).is(":hidden")) {
            var fieldName = elm.name;
            var thisForm = jQuery(elm).parents('form').first();

            thisForm.append('<div class="cart-errors"><strong>' + '<i>' + fieldName + '</i>: ' + errorMsg + '</strong></div>');
        }

        return advice;
    }
});

jQuery(function ($) {

    /** START save_delivery_address.phtml **/

    // Main delivery settings for each type of delivery
    $('[name="delivery[deliver][address]"]').bind('change', function () {
        var packageId = "0"; // Default delivery package id
        var container = $('.search-address[data-package-id="' + packageId + '"]');

        if ($(this).val() != 'billing_address') {
            container.removeClass('hidden');
        } else {
            container.addClass('hidden');

            var streetField = container.find('.otherAddress input[id*="street"]');
            var housenoField = container.find('.otherAddress input[id*="houseno"]');
            var postcodeField = container.find('.otherAddress input[id*="postcode"]');
            var cityField = container.find('.otherAddress input[id*="city"]');

            streetField.parent().find('.ajax-validation').remove();

            housenoField.removeClass('required-entry validation-failed');
            streetField.removeClass('required-entry validation-failed');
            postcodeField.removeClass('required-entry nl-postcode validation-failed');
            cityField.removeClass('required-entry validation-failed');
        }
    });

    // Display each package settings in the split-delivery and split-payment sections
    $('#method-split .pakket select').change(function () {
        var opt = $(this).val();
        var parent = $(this).parent();
        parent.find('.extra').addClass('hidden');

        if (opt != '') {
            var find_elem = parent.find('.' + opt);
            if (find_elem.length) {
                find_elem.removeClass('hidden');
            }
        }
    });

    $('#search-stores form').on('submit', function (e) {
        e.preventDefault();
        checkout.searchStores();
    });

    $('#method-split .split-delivery-method select').change(function () {
        var opt = $(this).val();
        setSplitPaymentOptions(opt, $(this).data('pakket'));
        if (opt == 'store') {
            $('[id="delivery[pakket][{0}][store]"]'.format($(this).data('pakket'))).addClass('required-entry');
        } else {
            $('[id="delivery[pakket][{0}][store]"]'.format($(this).data('pakket'))).removeClass('required-entry');
        }
    });

    // Main delivery options settings
    $('#cart #saveDeliveryAddress .dropdown-menu li a').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var self = $(this);
        var value = self.data('value');
        var group = self.parents('.btn-group').first();
        var type = checkout.setDropdownOption(group, self, true);

        // Hide all payment options details
        $('#payment-type .payment-extra').addClass('hidden');

        // Delivery section
        $('#method-pickup input[type="text"]').removeClass('required-entry');

        if (type == 'delivery') {
            // Hide all delivery methods
            $('#cart .delivery-method').addClass('hidden');
            $('#method-' + value).removeClass('hidden');

            if (value == 'pickup') {
                $('#method-pickup input[type="text"]').addClass('required-entry');
            }

            setPaymentOptions(value);

            if (value == 'split') {
                $('#method-split .split-payment-method select').addClass('validate-select');
                $('#method-split .split-delivery-method select').trigger('change');
                $('#payment-type #payment_method_text').text(Translator.translate('Split payment'));
                $('#payment-type input[name="payment[method]"]').val('split-payment');
                $('#method-split input[data-store-id][readonly]:visible').addClass('required-entry');
            } else {
                $('#method-split input[data-store-id][readonly]').removeClass('required-entry');
                $('#method-split .split-payment-method select').removeClass('validate-select');
                var available_payment_options = $('#payment-type [id="payment[method]"] ul li').filter(':not(.hidden)');
                if (available_payment_options.length == 1) {
                    available_payment_options.children('a').trigger('click');
                } else {
                    $('#payment-type #payment_method_text').text(Translator.translate('Choose'));
                    $('#payment-type input[name="payment[method]"]').val('');
                }
            }
        } else {
            // Payment selection section
            if (value.search('split') != -1 && $('#' + value).length > 0) {
                $('#' + value).removeClass('hidden');
            } else {
                $('#payment-type .validate-payment-single').removeClass('validation-failed');
                if ($('#split-' + type).length > 0) {
                    $('#split-' + type).addClass('hidden');
                }
                if ($('#payment-type .payment-extra.' + value).length) {
                    $('#payment-type .payment-extra.' + value).removeClass('hidden').parent().find('.validation-advice').remove();
                }
            }
        }
    });

    if ($('[id="address[address]"]').val() == 'foreign_address') {
        toggleDeliveryBillingAddress($('[id="address[address]"]').val());
    }

    /** END save_delivery_address.phtml **/

    /** Checkout forms submit */
    $("body").on("submit", "form.cart", function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        // Validate the manual activation form
        if (window.cartForm && window.cartForm.validator && !window.cartForm.validator.validate()) {
            return false;
        }
        return checkout.submitCart($(this));
    });

    /** Customer lookup */
    $('.search-client').click(function (e) {
        e.preventDefault();
        $('#search-client').modal();

        setMaxHeightSearchModal();

        $(window).on("resize.searchModal", function () {
            setMaxHeightSearchModalWhenShown();
        });

        $('#search-client').on('hidden.bs.modal', function (e) {
            $(window).unbind("resize.searchModal");
        });
    });

    // height based on no search results showing
    function setMaxHeightSearchModal() {
        var windowHeight = $(window).height();
        var modalFooterHeight = $('.modal-footer-search').height();
        var modalFooterOffsetTop = Math.round($('.modal-footer-search').offset().top);
        var resultsHeight = windowHeight - modalFooterOffsetTop - modalFooterHeight - 30;
        $('#customer_search_results').css("max-height", resultsHeight);
    }

    // height based on search results showing
    function setMaxHeightSearchModalWhenShown() {
        var windowHeight = $(window).height();
        var modalFooterHeight = $('.modal-footer-search').height();
        var modalFooterOffsetTop = Math.round($('#customer_search_results').offset().top);
        var resultsHeight = windowHeight - modalFooterOffsetTop - modalFooterHeight - 30;
        console.log(modalFooterOffsetTop, modalFooterHeight, windowHeight);
        $('#customer_search_results').css("max-height", resultsHeight);
    }

    // hide the shoppincart when checkout visible
    $('.col-right').hide();
    $('.col-main').addClass('checkout');
    // minimize the left column
    if (checkout.orderEdit !== "0") {
        // leave the customer sidebar expanded.
    } else {
        $('.col-left').removeClass('bigView');
        $('.col-left').addClass('smallView');
        $('.checkout').addClass('bigView');
    }

    /** Scroll to specific checkout section on click on the left menu items */
    $('.bs-sidenav li').click(function (e) {
        e.preventDefault();
        var elem = $(this).find('a').attr('href'),
            container = $('#cart-content');

        scrollAnimateTo(elem, container);
    });

    /** Move input field data to overview and credit check */
    $("#cart-left").on('activate.bs.scrollspy', function (elem) {
        var step = $(elem.target).children('a').attr('name');
        processPreviousSteps(step);
    });

    // Trim sim number input as requested in DF-003962
    $('.sim-number').on('blur.number-selection change.number-selection', function () {
        var el = $(this);
        el.val(el.val().trim());
    });

    // Show / hide the main delivery and payment selection lists
    $('#cart .dropdown-toggle, #cart button.text').bind('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if (checkout.checkoutDisabled()) {
            checkout.toggleLockAction();
        } else {
            $(this).parents(".btn-group").toggleClass('open');
        }
    });

    $('#method-split .pakket select, #method-deliver .pakket .radio:not(.disabled), #delivery-payment input:not(.disabled), #saveNumberPorting select, #saveNumberPorting input, #saveNumberPorting .radio:not(.disabled)').bind('mousedown', function (e) {
        if (checkout.checkoutDisabled()) {
            e.preventDefault();
            e.stopPropagation();
            checkout.toggleLockAction();
        }
    });

    $('#delivery-payment input:not(.disabled)').on('focus', function (e) {
        if (checkout.checkoutDisabled()) {
            e.preventDefault();
            e.stopPropagation();
            checkout.toggleLockAction();
        }
    });

    $('#cart input').on('blur change', function (ev) {
        var el = $(this);
        if (el.attr('type') == 'radio') {
            el.parent().parent().find('.validation-failed').removeClass('validation-failed');
            el.parent().parent().find('.ajax-validation').remove();
            el.parent().parent().find('.validation-advice').remove();
        } else {
            if (ev.type == 'blur') {
                el.removeClass('validation-failed');
                el.next('.validation-advice').remove();
                el.parent().find('.ajax-validation').fadeOut('slow', function () {
                    $(this).remove();
                });
            }
            if (ev.type == 'change') {
                el.val(escapeHtml(el.val()));
            }
        }
    });

    function escapeHtml(str) {
        if ($.trim(str) != '') {
            var tmp = document.createElement("DIV");
            tmp.innerHTML = str;
            var result = tmp.innerText || tmp.textContent;

            $(tmp).remove();
            if (result != str) {
                return escapeHtml(result);
            } else {
                return result;
            }
        } else {
            return str;
        }
    }

    $('select').on('change', function () {
        var el = $(this);
        el.parent().find('.ajax-validation').fadeOut('slow', function () {
            $(this).remove();
        });
    });

    // BTW number is required in Indirect if company address is foreign
    $('#business-input-address [id="customer[company_address]"], #business-input-address [id="customer[foreignAddress][company_country_id]"]').bind('change', function () {
        Validation.test('validate-vat-nl', document.getElementById('customer[company_vat_id]'));
    });

    // Make sure to display the save button again if a change is made to the form, and to hide it if no change was made (RFC-160221)
    $('form').on('change', 'input, select, textarea', function () {
        var allowedSteps = ['saveNumberSelection', 'saveDeviceSelection', 'saveNewNetherlands'];
        var form = $(this).parents('form').first();
        if (allowedSteps.indexOf(form.attr('id')) != -1) {
            var data = form.serialize();
            if (data == checkout.form[form.attr('id') + "Data"]) {
                $('[data-step="' + form.attr('id') + '"]').addClass('hide');
            } else {
                $('[data-step="' + form.attr('id') + '"]').removeClass('hide');
            }
        }
    });

    $('a.print-validate').on('click', function (event) {
        var steps = [];
        $('.bs-docs-section:visible form.cart').each(function (id, el) {
            var form = $(el);
            var action = form.attr('action');
            if (action != 'saveContract') {
                steps.push(form);
            }
        });
        noErrors = true;
        checkout.waitForAsync = false;
        steps.each(function (form) {
            if (checkout.form[form.attr('action') + 'Data'] != $(form).serialize()) {
                noErrors = noErrors && checkout.form[form.attr('action')].validator.validate();
                checkout.waitForAsync = form;
            }
        });
        if (checkout.waitForAsync) {
            event.preventDefault();
            if (noErrors) {
                var current = window.name;
                var newWindow = window.open("", "_blank");
                window.open("", current).focus();
                noErrors = checkout.submitCart(checkout.waitForAsync, false);
                newWindow.location.href = $(this).attr('href');
            } else {
                return false;
            }
        }
        if (checkout.waitForAsync && !noErrors) {
            newWindow.close();
        }
    });

    // RFC-160064 - When no email is entered enable the dummy email checkbox and, if checked, disable the email input
    // if an email address is entered, disable the checkbox.
    $('[id="customer[dummy_email]"]').on('change', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            $('[id="additional[email][0]"]').removeClass('required-entry validate-email validate-backend-email validation-failed').prop('disabled', true);
            $('[id="additional[email][0]"]').parent().find('.validation-advice').remove();
            $('.add-address-email').addClass('hidden');
            $('.remove-above').each(function (a, el) {
                $(el).click()
            });
        } else {
            $('[id="additional[email][0]"]').addClass('required-entry validate-email validate-backend-email').prop('disabled', false);
            $('.add-address-email').removeClass('hidden');
        }
    });

    $('[id="additional[email][0]"]').on('change', function () {
        var empty = $(this).val().trim() == '';
        $('[id="customer[dummy_email]"]').prop('disabled', !empty);
    });
    // Calculate which steps are before the print contract step, and allow saving them, but only if the current step is the saveContract one
    $("body").on("click", "a.print-contract-url.print-validate", function (ev) {
        var checkoutAction = 'saveContract';
        if (checkout.currentCheckoutStep == checkoutAction) {
            var noErrors = true;
            var thisForm = $('#' + checkoutAction);
            var stepIndexes = [];
            var start = false;
            var end = false;
            checkout.stepsOrder.each(function (el) {
                if (el == 'saveContract') {
                    end = true;
                }
                if (start && !end) {
                    stepIndexes.push(el);
                }
                if (el == 'saveCreditCheck' || el == 'saveSuperOrder') {
                    start = true;
                }
            });
            stepIndexes.each(function (stepName) {
                checkout.savePreviousStep(noErrors, stepName, checkoutAction, thisForm);
            });
            if (!noErrors) {
                ev.preventDefault();
            }
        }
    });
});

// Override createAdvice to enable manipulation of where the advice is shown
Object.extend(Validation, {
    createAdvice: function (name, elm, useTitle, customError) {
        var v = Validation.get(name);
        var errorMsg = useTitle ? ((elm && elm.title) ? elm.title : v.error) : v.error;
        if (customError) {
            errorMsg = customError;
        }
        try {
            if (Translator) {
                errorMsg = Translator.translate(errorMsg);
            }
        }
        catch(e) {}

        // Special class to show the advice for the parent element, and make sure it is not repeated
        if ($(elm).hasClassName('parent-shown-advice')) {
            elm = $(elm).up().select('input').first();
            if (advice = Validation.getAdvice(name, elm)) {
                return advice;
            }
        }
        advice = '<div class="validation-advice" id="advice-' + name + '-' + Validation.getElmID(elm) + '" style="display:none">' + errorMsg + '</div>';

        Validation.insertAdvice(elm, advice);
        advice = Validation.getAdvice(name, elm);
        if ($(elm).hasClassName('absolute-advice')) {
            var dimensions = $(elm).getDimensions();
            var originalPosition = Position.cumulativeOffset(elm);

            advice._adviceTop = (originalPosition[1] + dimensions.height) + 'px';
            advice._adviceLeft = (originalPosition[0]) + 'px';
            advice._adviceWidth = (dimensions.width) + 'px';
            advice._adviceAbsolutize = true;
        }

        return advice;
    }
});
