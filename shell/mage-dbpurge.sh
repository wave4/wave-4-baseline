#!/bin/bash

CONFIG_FILE="../app/etc/local.xml"
TMP_FILE="../var/.tmp.local.xml"
DUMP_STRING=""

# Define list of excluded tables. The values can have wildcards(*) -starting with- or the exact table names
SKIP_TABLES=( "admin*" "cms_*" "core_*" "eav_*" "agent*" "customer_eav_attribute*" "customer_group" "catalog_category_entity*" "catalog_eav_attribute" "cataloginventory_stock" "tax_*" "directory_*" "api2_acl_role" "catalog_product_link_attribute" "catalog_product_link_type" "permission_block" "permission_variable" "sales_order_status" "sales_order_status_state" )

if [ ! -f "$CONFIG_FILE" ]; then
  echo "$CONFIG_FILE does not exist"
  exit
fi

grep -Pzo '(?s)<default_setup>.*</default_setup>' $CONFIG_FILE  | sed 's/\(<default_setup>\|<\/default_setup>\)//g' | tr '\n\r\t' $'\x01' | sed -e 's/\x01/ /g' | sed 's/ //g' > $TMP_FILE

function getParam()
{
	RETVAL=$(tr "\n" " " < $TMP_FILE | grep -Eo "<$2>(<!\[CDATA\[)?(.+?)(\]\]>)?</$2>" | sed "s#<$2><!\[CDATA\[##g;s#\]\]><\/$2>##g" | head -1)
	
	if [[ "$3" == "sanitise" ]]; then
	  RETVAL=$(echo "$RETVAL" | sed 's/"/\\\"/g')
	fi
echo -e "$RETVAL"
}

# Get database connection details
DBHOST=$(getParam "1" "host")
DBUSER=$(getParam "1" "username")
DBPASS=$(getParam "1" "password" "sanitise" )
DBNAME=$(getParam "1" "dbname")
TABLE_PREFIX=$(getParam "1" "table_prefix")

[ -f $TMP_FILE ] && rm $TMP_FILE

echo "The host that was selected is '$DBHOST' and the selected database is: '$DBNAME'"
echo -n "Are you sure you want to truncate all tables? [y/N]: "
read CONFIRM; if [[ ! "$CONFIRM" == "y" ]]; then exit; fi

TABLES=( $(mysql -Nsr -h$DBHOST -u$DBUSER -p"$DBPASS" $DBNAME -e 'show full tables where Table_Type != "VIEW"' | awk '{ print $1}' ) )
VIEWS=( $(mysql -Nsr -h$DBHOST -u$DBUSER -p"$DBPASS" $DBNAME -e 'show full tables where Table_Type = "VIEW"' | awk '{ print $1}' ) )

TABLE_DUMP_STRING="SET FOREIGN_KEY_CHECKS=0;"
VIEW_DUMP_STRING="SET FOREIGN_KEY_CHECKS=0;"

for TABLE in ${TABLES[@]}; do
    SKIP=false
    for element in ${SKIP_TABLES[@]}
    do
        if [[ $element == *"*"* ]]; then
            element=${element::-1}
            if [[ " ${TABLE} " =~ " ${element} "* ]]
                then
                SKIP=true
            fi
        else
            if [[ " ${TABLE} " == " ${element} "* ]]
                then
                SKIP=true
            fi
        fi
    done
    if (! $SKIP)
        then
        TABLE_DUMP_STRING="$TABLE_DUMP_STRING TRUNCATE TABLE $TABLE; "
    fi
done

for VIEW in ${VIEWS[@]}; do
  VIEW_DUMP_STRING="$VIEW_DUMP_STRING TRUNCATE VIEW $VIEW; "
done

mysql --force -h"$DBHOST" -u"$DBUSER" -p"$DBPASS" $DBNAME -e "$TABLE_DUMP_STRING SET FOREIGN_KEY_CHECKS=1"
mysql --force -h"$DBHOST" -u"$DBUSER" -p"$DBPASS" $DBNAME -e "$VIEW_DUMP_STRING SET FOREIGN_KEY_CHECKS=1"

# Special magento tables
mysql --force -h"$DBHOST" -u"$DBUSER" -p"$DBPASS" $DBNAME -e "DELETE FROM catalog_category_entity WHERE entity_id > 2; ALTER TABLE catalog_category_entity AUTO_INCREMENT = 3;"

mysql --force -h"$DBHOST" -u"$DBUSER" -p"$DBPASS" $DBNAME -e "DELETE FROM core_url_rewrite; ALTER TABLE core_url_rewrite AUTO_INCREMENT = 1;"


cat <<EOT
#######################################

 MYSQL DB TRUNCATE COMPLETE

#######################################
EOT
