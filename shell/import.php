<?php

require_once 'abstract.php';

/**
 * Class Dyna_Product_Visibility
 */
class Dyna_Import_Cli extends Mage_Shell_Abstract
{

    protected $types = [
        'cableProduct',
        'mobileProduct',
        'fixedProduct',
        'mixmatch',
        'multimapper',
        'categories',
        'dealerCampaigns',
        'mobileSalesRules',
        'productMatchRules',
        'fixedSalesRules'
    ];

    protected $_convertToUTF8;

    /**
     * Echo log message to console
     * @param $msg
     */
    private function _writeLine($msg)
    {
        echo $msg . PHP_EOL;
    }

    /**
     * Run appropriate import script based on the --type param
     *
     */
    public function run()
    {
        $type = $this->getArg('type');
        if (!$type || !in_array($type, $this->types)) {
            $this->_writeLine('Invalid type, please specify [' . implode('/', $this->types) . ']');

            return;
        }

        $file = $this->getArg('file');
        $path = Mage::getBaseDir('upload') . DIRECTORY_SEPARATOR . $file;
        if (!$file || !file_exists($path)) {
            $this->_writeLine('Import file not found, should be in ' . $path);

            return;
        }

        $contents = file_get_contents($path);
        $isUTF8Converted = false;
        $hasError = false;
        if (in_array(pathinfo($path, PATHINFO_EXTENSION), ['json', 'csv']) && ! mb_check_encoding($contents, 'UTF-8')) {
            $this->_writeLine('********************************************');
            $this->_writeLine('[ERROR] Import file is not encoded in UTF8.');
            $this->_writeLine('********************************************');
            $this->_writeLine('File will be converted to UTF8.');
            iconv(mb_detect_encoding($contents, mb_detect_order(), true), "UTF-8", $contents);
            file_put_contents($path . '_UTF8', $contents);
            $path = $path . '_UTF8';
            $isUTF8Converted = true;
            $this->_writeLine('File converted to UTF8.');
        }

        try {
            switch ($type) {
                case 'cableProduct':
                    $this->_importProductFile($path, 'cable');
                    break;
                case 'mobileProduct':
                    $this->_importProductFile($path, 'mobile');
                    break;
                case 'fixedProduct':
                    $this->_importProductFile($path, 'fixed');
                    break;
                case 'mixmatch':
                    $this->_importMixMatchFile($path);
                    break;
                case 'multimapper':
                    $fields = $this->getArg('fields');
                    $this->_importMultiMapperFile($path, $fields);
                    break;
                case 'categories':
                    $this->_importCategoriesFile($path);
                    break;
                case 'dealerCampaigns':
                    $this->_importDealerCampaignFile($path, 'dealerImportCampaign');
                    break;
                case 'mobileSalesRules':
                    $this->_importMobileSalesRules($path, 'importMobilePriceRules');
                    break;
                case 'productMatchRules':
                    $this->_importProductMatchRules($path);
                    break;
                case 'fixedSalesRules':
                    $this->_importFixedSalesRules($path, 'importFixedPriceRules');
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            $hasError = true;
            Mage::getSingleton('core/logger')->logException($e);
            Zend_Debug::dump($e->getMessage());
        }

        if ($isUTF8Converted && !$hasError)
        {
            $this->_writeLine('++++++++++++++++');
            $this->_writeLine('File was imported with auto convert to UTF8.');
            $this->_writeLine('++++++++++++++++');
        }
    }

    /**
     * @param $path
     * @param $type
     */
    protected function _importMobileSalesRules($path, $type)
    {
        /** @var Dyna_PriceRules_Model_ImportMobilePriceRules $simpleImport */
        $simpleImport = Mage::getModel("dyna_pricerules/" . $type);
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing mobile sales rules...');
        $file = fopen($path, 'r');
        $lineCnt = 0;

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = $simpleImport->formatKey($col);
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                $simpleImport->setHeader($header);
            }
        }
        $this->_writeLine('Finished importing mobile sale rules file...');
    }

    /**
     * @param $path
     * @param $type
     */
    protected function _importFixedSalesRules($path, $type)
    {
        /** @var Dyna_PriceRules_Model_ImportFixedPriceRules $simpleImport */
        $simpleImport = Mage::getModel("dyna_pricerules/" . $type);
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing fixed sales rules...');
        $file = fopen($path, 'r');
        $lineCnt = 0;

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = $simpleImport->formatKey($col);
                } else {
                    $data[$colCnt] = $col;
                }
            }

            if ($lineCnt != 1) {
                $simpleImport->process($data);
            } else {
                $simpleImport->setHeader($header);
            }
        }
        $this->_writeLine('Finished importing fixed sale rules file...');
    }

    /**
     * @param $path
     * @param $type
     */
    public function _importProductFile($path, $type)
    {
        /** OMNVFDE-133: Cable product data improvements */
        /** Importing cable products from json file */
        if ($type == 'cable' && pathinfo($path, PATHINFO_EXTENSION) == 'json') {
            $fileContent = file_get_contents($path);
            $allProducts = Mage::helper('core')->jsonDecode($fileContent);

            $this->_writeLine('Importing product file...');
            /** @var Dyna_Cable_Model_Import_CableProductJsonFile $simpleImport */
            $simpleImport = Mage::getModel('dyna_cable/import_cableProductJsonFile');
            $simpleImport->importProduct($allProducts);
            $this->_writeLine('Finished importing cable products from json file...');

            return;
        }

        /** @var Omnius_Import_Model_Import_Product_Abstract $simpleImport */
        $simpleImport = Mage::getModel("dyna_{$type}/import_{$type}Product");
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing product file...');
        $file = fopen($path, 'r');
        $lineCnt = 0;

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = $simpleImport->formatKey($col);
                } else {
                    $data[$header[$colCnt]] = $col;
                }
            }
            if ($lineCnt != 1) {
                $simpleImport->importProduct($data);
            }
        }
        $this->_writeLine('Finished importing product file...');
    }

    /**
     * @param $path
     * @param $type
     */
    public function _importDealerCampaignFile($path, $type)
    {
        /** @var Dyna_AgentDE_Model_DealerImportCampaign $simpleImport */
        $simpleImport = Mage::getModel("agentde/" . $type);
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing dealer assigned to campaigns file...');
        $file = fopen($path, 'r');
        $lineCnt = 0;

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            $data = array();
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = $simpleImport->formatKey($col);
                } else {
                    $data[$colCnt] = $col;
                }
            }

            $simpleImport->setCurrentLine($lineCnt);

            if ($lineCnt != 1) {
                $simpleImport->importDealer($data);
            } else {
                $simpleImport->mapHeaderToFields($header);
            }
        }
        $this->_writeLine('Finished importing dealer assigned to campaigns file...');
    }

    /**
     * @param $path
     */
    public function _importMixMatchFile($path)
    {
        /** Session not used, just initialised */
        $session = Mage::getSingleton('admin/session');
        $this->_writeLine('Importing mixmatch file...');
        $websiteIds = explode(',', $this->getArg('websites'));
        /**
         *@var Dyna_MixMatch_Model_Importer $importer
         */
        $importer = Mage::getModel('dyna_mixmatch/importer');
        $importer->importFile($path, $websiteIds);
        $this->_writeLine('Finished importing mixmatch file...');
    }

    /**
     * @param $path
     */
    public function _importMultiMapperFile($path, $fields)
    {
        $importer = Mage::getModel('multimapper/importer');
        $this->_writeLine('Importing MultiMapper...');
        $importer->import($path, $fields);
        $this->_writeLine('Finished importing MultiMapper...');
    }

    /**
     * @param $path
     */
    public function _importCategoriesFile($path)
    {
        /** @var Dyna_Mobile_Model_Import_MobileCategory $simpleImport */
        $simpleImport = Mage::getModel("dyna_mobile/import_mobileCategory");
        $simpleImport->setDebug(false);

        $this->_writeLine('Importing categories file...');
        $file = fopen($path, 'r');
        $lineCnt = 0;

        while (($line = fgetcsv($file, 0, $simpleImport->getCsvDelimiter())) !== false) {
            $line[0] = $this->removeBomUtf8($line[0]);
            $lineCnt++;
            $colCnt = 0;
            foreach ($line as $col) {
                $colCnt++;
                if ($lineCnt == 1) {
                    $header[$colCnt] = $simpleImport->formatkey($col);
                } else {
                    $data[$header[$colCnt]] = $col;
                }
            }
            if ($lineCnt != 1) {
                $simpleImport->importCategory($data);
            }
        }
        $this->_writeLine('Finished importing categories file.');
    }

    /**
     * @param $path
     */
    public function _importProductMatchRules($path)
    {
        /** @var Dyna_ProductMatchRule_Model_Importer $simpleImport */
        $simpleImport = Mage::getModel('productmatchrule/importer');

        $this->_writeLine('Importing product match rules file...');
        $simpleImport->import($path);
        $this->_writeLine('Finished importing product match rules file...');
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php file -- [options]

  help							This help
  file							Filename to import
  type							Import type [product/mixmatch/multimapper/categories/dealerCampaigns/mobileSalesRules/productMatchRules/fixedSalesRules]
  website						Websites to import

USAGE;
    }

    /**
     * Rremove BOM from string
     * @param $s
     * @return string
     */
    function removeBomUtf8($s){
        if(substr($s,0,3)==chr(hexdec('EF')).chr(hexdec('BB')).chr(hexdec('BF'))){
            return substr($s,3);
        }else{
            return $s;
        }
    }
}

// Example
//php import.php --type mobileProduct --file mobileHw.csv

$import = new Dyna_Import_Cli();
$import->run();
