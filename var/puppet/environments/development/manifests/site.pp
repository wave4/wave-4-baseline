class systempackages {

notice($operatingsystem)
if $operatingsystem == 'Ubuntu'
{
  exec { "pre-update":
    command => "/usr/bin/apt-get update"
  }
}
else
{
  exec { "pre-update":
    command => "/usr/bin/yum -y update"
  }
}

package { [ 'git', 'curl', 'acl', 'unattended-upgrades', 'vim', 'software-properties-common', 'mono-complete', 'unzip']:
    ensure  => "installed",
    require => Exec['pre-update'],
  }

}

################################

# System
Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

class { 'systempackages': }

if $operatingsystem == 'Ubuntu'
{
        # APT
        class { 'apt':
        update => {
            frequency => 'daily',
        },
    }

    apt::ppa { 'ppa:ondrej/php':
      before  => Package['php7.0-fpm'],
      require => Class['systempackages'],
    }->
    exec{ 'manual-after-add-ppa':
      command => '/usr/bin/apt-get update',
    }
}

# NGINX
class{ "nginxnaxsi":
  require => [
    Class['systempackages'],
    Class['varnish'],
  ],
}
nginxnaxsi::vhost { 'telesales-omnius-vf-de.dev.ynad.local' :
  docroot          => '/vagrant',
  port             => 80,
  template         => 'dynalean/nginx/vhost.conf.erb',
  template_options => {
    mage_run_code => 'telesales',
    mage_run_type => 'store',
    mage_env_code => 'BAU',
  },
}

# PHP
package{ 'php7.0-fpm':
  ensure  => 'installed',
  require => [
    Class['systempackages'],
  ],
}

service { 'php7.0-fpm':
  ensure     => 'running',
  name       => 'php7.0-fpm',
  enable     => true,
  require    => Package['php7.0-fpm'],
}

$modules = ['php7.0-gd', 'php7.0-curl', 'php7.0-intl', 'php-xdebug', 'php7.0-mysql', 'php7.0-xml', 'php7.0-mbstring', 'php7.0-soap', 'php7.0-zip']
package { $modules:
  ensure  => 'installed',
  require => Package['php7.0-fpm'],
}

# Update PHP.ini settings
exec{ 'update-fpm-timezone':
  command => "sed -i -e 's#;date.timezone.*#date.timezone = Europe/Amsterdam#g' /etc/php/7.0/fpm/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-cli-timezone':
  command => "sed -i -e 's#;date.timezone.*#date.timezone = Europe/Amsterdam#g' /etc/php/7.0/cli/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-fpm-memory-limit':
  command => "sed -i -e 's#memory_limit.*#memory_limit = 512M#g' /etc/php/7.0/fpm/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-cli-memory-limit':
  command => "sed -i -e 's#memory_limit.*#memory_limit = 512M#g' /etc/php/7.0/cli/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-fpm-max_execution_time':
  command => "sed -i -e 's#max_execution_time.*#max_execution_time = 18000#g' /etc/php/7.0/fpm/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-cli-max_execution_time':
  command => "sed -i -e 's#max_execution_time.*#max_execution_time = 18000#g' /etc/php/7.0/cli/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-fpm-max_input_vars':
  command => "sed -i -e 's#; max_input_vars.*#max_input_vars = 2000#g' /etc/php/7.0/fpm/php.ini",
  require => Package['php7.0-fpm'],
}->
exec{ 'update-cli-max_input_vars':
  command => "sed -i -e 's#; max_input_vars.*#max_input_vars = 2000#g' /etc/php/7.0/cli/php.ini",
  require => Package['php7.0-fpm'],
  notify  => Service['php7.0-fpm'],
}->
exec{ 'update-fpm-allow_url_include':
   command => "sed -i -e 's#allow_url_include.*#allow_url_include = On#g' /etc/php/7.0/fpm/php.ini",
   require => Package['php7.0-fpm'],
}->
exec{ 'update-cli-allow_url_include':
   command => "sed -i -e 's#allow_url_include.*#allow_url_include = On#g' /etc/php/7.0/cli/php.ini",
   require => Package['php7.0-fpm'],
   notify  => Service['php7.0-fpm'],
}

#xDebug configuration
file { '/etc/php/7.0/mods-available/xdebug.ini':
  source => 'puppet:///modules/dynalean/xdebug/xdebug.ini',
  require => Package['php-xdebug'],
}

# Varnish
class { 'varnish':
  varnish_listen_port  => 8080,
  varnish_storage_size => '1G',
  add_repo             => false,
  require              => [
    Class['systempackages'],
  ],
}
file { '/etc/varnish/default.vcl':
  ensure   => present,
  source   => 'puppet:///modules/dynalean/varnish/default.vcl',
  require  => Class['varnish'],
}

# Redis
class { "redis":
  bind        => '0.0.0.0',
  require     => Class['systempackages'],
  databases   => 100,
}

# MySQL
class { '::mysql::server':
  root_password => 'strongpassword',
  remove_default_accounts => true,
}
mysql::db { 'omnius_vfde_dev':
  user     => 'omnius_vfde_dev',
  password => 'password',
  host     => 'localhost',
  grant    => ['ALL'],
  sql      => '/vagrant/var/puppet/environments/development/modules/dynalean/files/mysql/seed.sql',
  import_timeout  => '0',
}

# PHPUnit
class{ 'phpunit':}

# Config
file { "/home/vagrant/.ssh/id_rsa":
  ensure => 'present',
  owner=> 'vagrant',
  group => 'vagrant',
  mode => '400',
  source => 'puppet:///modules/dynalean/ssh/id_rsa',
}
file { "/home/vagrant/.ssh/id_rsa.pub":
  ensure => 'present',
  owner=> 'vagrant',
  group => 'vagrant',
  mode => '400',
  source => 'puppet:///modules/dynalean/ssh/id_rsa.pub',
}
file { "/vagrant/app/etc/monolog.xml":
  ensure => 'present',
  source => 'puppet:///modules/dynalean/magento/aleron75/monolog.xml',
}
file { "/vagrant/app/etc/local.xml":
  ensure => 'present',
  source => 'puppet:///modules/dynalean/magento/omnius/local.xml',
}
file { "/vagrant/app/etc/sandbox.xml":
  ensure => 'present',
  source => 'puppet:///modules/dynalean/magento/omnius/sandbox.xml',
}
file { "/vagrant/app/etc/replicas.xml":
  ensure => 'present',
  source => 'puppet:///modules/dynalean/magento/omnius/replicas.xml',
}

# Rights
file { "/vagrant/var":
  ensure  => 'directory',
  require => [
	Class['composer'],
	Exec['composer-install-application']
  ],
  mode    => '0777',
}
file { "/vagrant/media/catalog":
  ensure  => 'directory',
  require => [
	Class['composer'],
	Exec['composer-install-application']
  ],
  mode    => '0777',
  recurse => true,
}
file { "/vagrant/app/code/local/Dyna/Proxy":
  ensure  => 'directory',
  require => [
	Class['composer'],
	Exec['composer-install-application']
  ],
  mode    => '0777',
  recurse => true,
}

# Cronjobs / but disabled
include cron

# Composer
class { 'composer':
  command_name => 'composer',
  target_dir   => '/usr/bin',
  require => [
    Package['php7.0-fpm'],
    File['/home/vagrant/.ssh/id_rsa'],
    File['/home/vagrant/.ssh/id_rsa.pub'],
  ],
}->
exec{ 'ssh-accept-bitbucket':
  logoutput => true,
  command => 'mkdir -p /home/vagrant/.ssh/ && ssh-keyscan bitbucket.org >> /home/vagrant/.ssh/known_hosts',
}->
exec{ 'composer-dir-oauth':
  command => 'mkdir -p /home/vagrant/.config/composer',
}->
file{ "/home/vagrant/.config/composer/auth.json":
  ensure => "present",
  source => "puppet:///modules/dynalean/composer/auth.json",
}->
exec{ 'composer-install-application':
  logoutput => true,
  timeout => 0,
  user => 'vagrant',
  environment => ["COMPOSER_HOME=/home/vagrant"],
  command => "/usr/bin/composer install --working-dir=/vagrant",
}

# Users
group { "development":
  ensure => "present",
  require => Class['nginxnaxsi'],
}
user { "www-data":
  groups     => ["development"],
}
user { "dynalean":
  ensure     => "present",
  managehome => true,
  password => '$6$uV.xnPDn$T/GiT8e1iiPlWXoquCxWH70ckQemuunYQevWZk6IidCv2KHhqA7aVVrAwB.OBvwlgpzK6XA82zmLMmeemM4MM.', # password
  shell      => '/bin/bash',
  groups     => ["development"],
}
class { 'sudo':
  purge               => false,
  config_file_replace => false,
}
sudo::conf { 'dynalean':
  priority => 10,
  content  => "%dynalean ALL=(ALL) ALL",
}
