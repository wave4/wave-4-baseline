# This is a basic VCL configuration file for varnish.  See the vcl(7)
# man page for details on VCL syntax and semantics.
# 
# Default backend definition.  Set this to point to your content
# server.
# 
backend default {
    .host = "127.0.0.1";
    .port = "8080";
    .connect_timeout = 600s;
    .first_byte_timeout = 600s;
    .between_bytes_timeout = 600s;
}

acl purge {
	"127.0.0.1";
}

#
# Below is a commented-out copy of the default VCL logic.  If you
# redefine any of these subroutines, the built-in logic will be
# appended to your code.
sub vcl_recv {

    # Handle compression correctly. Different browsers send different
    # "Accept-Encoding" headers, even though they mostly all support the same
    # compression mechanisms. By consolidating these compression headers into
    # a consistent format, we can reduce the size of the cache and get more hits.
    # @see: http:// varnish.projects.linpro.no/wiki/FAQ/Compression
    if (req.http.Accept-Encoding) {
      if (req.http.Accept-Encoding ~ "gzip") {
        # If the browser supports it, we'll use gzip.
        set req.http.Accept-Encoding = "gzip";
      }
      else if (req.http.Accept-Encoding ~ "deflate") {
        # Next, try deflate if it is supported.
        set req.http.Accept-Encoding = "deflate";
      }
      else {
        # Unknown algorithm. Remove it and send unencoded.
        unset req.http.Accept-Encoding;
      }
    }
    
    if (req.restarts == 0) {
	    if (req.http.x-forwarded-for) {
	        set req.http.X-Forwarded-For =
		    req.http.X-Forwarded-For + ", " + client.ip;
	    } else {
	        set req.http.X-Forwarded-For = client.ip;
	    }
    }

    if (req.request == "BAN") {
        if (!client.ip ~ purge) {
            error 405 "Not allowed.";
        }

        # This option is to clear any cached object containing the req.url
        if (req.http.x-ban-url) {
            ban("req.url ~ "+req.http.x-ban-url);
        } else {
            ban("req.url ~ "+req.url);
        }

        # This option is to clear any cached object matches the exact req.url
        # ban("req.url == "+req.url);

        # This option is to clear any cached object containing the req.url
        # AND matching the hostname.
        # ban("req.url ~ "+req.url+" && req.http.host == "+req.http.host);
        error 200 "Cached Cleared Successfully.";
    }

    if (req.request != "GET" &&
        req.request != "HEAD" &&
        req.request != "PUT" &&
        req.request != "POST" &&
        req.request != "TRACE" &&
        req.request != "OPTIONS" &&
        req.request != "DELETE") {
        /* Non-RFC2616 or CONNECT which is weird. */
        return (pipe);
    }
    if (req.request != "GET" && req.request != "HEAD") {
        /* We only deal with GET and HEAD by default */
        return (pass);
    }

    if (req.url ~ "^/configurator/init/getPrices"
    || req.url ~ "^/configurator/init/getConfiguratorOptions"
    || req.url ~ "^/configurator/init/getDeviceStock"
    || req.url ~ "^/configurator/init/getAccessoriesStock"
 	|| req.url ~ "^/configurator/init/getProductpopupdata"
 	|| req.url ~ "^/configurator/init/getProducts"
    || req.url ~ "^/configurator/cart/getRules"
    || req.url ~ "^/skin"
    || req.url ~ "^/js"
    || req.url ~ "^/media") {
        unset req.http.Cookie;
    }

    if (req.http.Authorization || req.http.Cookie) {
        /* Not cacheable by default */
        return (pass);
    }
    return (lookup);
}

sub vcl_pipe {
    # Note that only the first request to the backend will have
    # X-Forwarded-For set.  If you use X-Forwarded-For and want to
    # have it set for all requests, make sure to have:
    # set bereq.http.connection = "close";
    # here.  It is not set by default as it might break some broken web
    # applications, like IIS with NTLM authentication.
    return (pipe);
}

sub vcl_pass {
    return (pass);
}

sub vcl_hash {
    hash_data(req.url);
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }
    return (hash);
}

sub vcl_hit {
    return (deliver);
}

sub vcl_miss {
    return (fetch);
}

sub vcl_fetch {
    if (req.url ~ "^/configurator/init/getPrices"
    || req.url ~ "^/configurator/init/getConfiguratorOptions"
    || req.url ~ "^/configurator/init/getDeviceStock"
    || req.url ~ "^/configurator/init/getProductpopupdata"
    || req.url ~ "^/configurator/init/getAccessoriesStock"
    || req.url ~ "^/configurator/init/getProducts"
    || req.url ~ "^/configurator/cart/getRules"
    || req.url ~ "^/skin"
    || req.url ~ "^/js"
    || req.url ~ "~/media") {
        if(req.url ~ "~/media" || req.url ~ "^/js" || req.url ~ "^/skin"){
            # Handle custom assets cache
        } else{
            unset beresp.http.Cache-Control;
            unset beresp.http.Expires;
            unset beresp.http.Pragma;
        }
        unset beresp.http.Cache;
        unset beresp.http.Server;
        unset beresp.http.Set-Cookie;
        unset beresp.http.Age;
        set beresp.ttl = 21d;
        set beresp.http.X-Cacheable = "Cachable";
    }

    if (beresp.ttl <= 0s ||
        beresp.http.Set-Cookie ||
        beresp.http.Vary == "*") {
		/*
		 * Mark as "Hit-For-Pass" for the next 2 minutes
		 */
		set beresp.ttl = 120 s;
		return (hit_for_pass);
    }

    if (beresp.status != 200 && !(req.url ~ "^/media")) {
        return (hit_for_pass);
    }
    return (deliver);
}

sub vcl_deliver {
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
    } else {
        set resp.http.X-Cache = "MISS";
    }
    return (deliver);
}

sub vcl_error {
    set obj.http.Content-Type = "text/html; charset=utf-8";
    set obj.http.Retry-After = "5";
    synthetic {"
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>"} + obj.status + " " + obj.response + {"</title>
  </head>
  <body>
    <h1>Error "} + obj.status + " " + obj.response + {"</h1>
    <p>"} + obj.response + {"</p>
    <h3>Guru Meditation:</h3>
    <p>XID: "} + req.xid + {"</p>
    <hr>
    <p>Varnish cache server</p>
  </body>
</html>
"};
    return (deliver);
}

sub vcl_init {
	return (ok);
}

sub vcl_fini {
	return (ok);
}
