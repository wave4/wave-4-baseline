class phpunit {

  exec{'phpunit-download':
    logoutput => true,
    command => "/usr/bin/wget -q https://phar.phpunit.de/phpunit.phar",
    unless  => "/usr/bin/test -f /usr/local/bin/phpunit",
  }->
  exec{'phpunit-execrights':
    command => "/bin/chmod +x phpunit.phar",
  unless  => "/usr/bin/test -f /usr/local/bin/phpunit",
  }->
  exec{'phpunit-move-to-exec':
    command => "/bin/mv phpunit.phar /usr/local/bin/phpunit",
    unless  => "/usr/bin/test -f /usr/local/bin/phpunit",
  }
}