define nginxnaxsi::vhost (
  $docroot,
  $port           = '80',
  $template       = '',
  $priority       = '50',
  $serveraliases  = '',
  $create_docroot = true,
  $template_options = {},
) {

  include nginxnaxsi

  file { "${nginxnaxsi::vhost_dir}/${priority}-${name}.conf":
    content => template("${template}"),
    require => Package['nginx-naxsi'],
  }->
  file { "${nginxnaxsi::vhost_dir_enabled}/${priority}-${name}.conf":
    ensure  => 'link',
    target    => "${nginxnaxsi::vhost_dir}/${priority}-${name}.conf",
    require => Package['nginx-naxsi'],
    notify  => Service['nginx'],
  }

  if $create_docroot == true {
    file { "${nginxnaxsi::docroot}":
      ensure => directory,
      mode   => '0775',
    }
    file { $docroot:
      ensure => directory,
      mode   => '0775',
    }
  }
}
