class nginxnaxsi (
  $docroot = '/var/www',
  $config_dir = '/etc/nginx',
  $config_file = '/etc/nginx/nginx.conf',
  $vhost_dir = "/etc/nginx/sites-available",
  $vhost_dir_enabled = "/etc/nginx/sites-enabled",
){

  package { 'nginx-naxsi':
    ensure => 'present',
  }

  service { 'nginx':
    ensure     => 'running',
    name       => 'nginx',
    enable     => true,
    require    => Package['nginx-naxsi'],
  }

  file{'/etc/nginx/sites-enabled/default':
    ensure => 'absent',
    require => Package['nginx-naxsi'],
  }->
  file{'/etc/nginx/sites-available/default':
    ensure => 'absent',
    require => Package['nginx-naxsi'],
  }
}