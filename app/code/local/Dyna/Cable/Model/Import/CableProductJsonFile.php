<?php

/**
 * Class Dyna_Cable_Model_Import_CableProductJsonFile
 *
 * Parsing recursively through all individual product data
 * If json nodes are numeric, their values will be imploded and assigned to parent node
 * If json nodes are not numeric and not found in the attributes of a product and not mapped to certain attributes,
 * installer will try to parse them from camel case to snake case
 * and if still not found in the attributes list, these nodes will be logged
 */
class Dyna_Cable_Model_Import_CableProductJsonFile extends Omnius_Import_Model_Import_Product_Abstract
{
    protected $logFileName = "cable_product_import_json";
    protected $newProducts = 0;
    protected $updatedProducts = 0;
    protected $productAttributes;
    /** @var $currentProduct Mage_Catalog_Model_Product  */
    protected $currentProduct;
    protected $attributeSet = "KD_Cable_Products";
    protected $attributeSetId;
    protected $websites = "telesales";
    protected $websitesIds = array();
    protected $cachedAttributes = array();
    protected $specialNodesUsedForGrouping = array(
        'relations',
        'status',
        'properties',
        'display',
    );
    protected $specialCaseNodes = array(
        'tariffChange',
        'price',
        'products',
        'serviceCategory',
        'type',
        'selectable',
        'contractCode',
        'packageType',
        'packageSubtype',
        'premiumClass',
        'productSegment',
    );
    protected $mappedNodesToAttributes = array(
        'productId' => 'sku',
        'excludes' => 'relation_excludes',
        'upgrades' => 'relation_upgrades',
        'options' => 'relation_options',
        'preselectOptions' => 'relation_preselected_options',
        'inventoryPreselectedOptions' => 'relation_inv_pres_options',
        'mandatoryOptions' => 'relation_mandatory_options',
        'components' => 'relation_components',
        'devices' => 'relation_devices',
        'fees' => 'relation_fees',
        'minBound' => 'req_rate_min_bound',
        'maxBound' => 'req_rate_max_bound',
    );

    /**
     * Dyna_Cable_Model_Import_CableProductJsonFile constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->logFileName . '_' . date("Y-m-d") . '.log');

        /** Load all attributes defined for product entity */
        $product = Mage::getModel('catalog/product');

        /* @var $eavConfig Mage_Eav_Model_Config */
        $eavConfig = Mage::getModel('eav/config');
        $this->productAttributes = $eavConfig->getEntityAttributeCodes(Mage_Catalog_Model_Product::ENTITY, $product);

        /** Set attribute set id for current instance */
        $this->attributeSetId = Mage::getModel("eav/entity_attribute_set")->getCollection()
            ->addFieldToFilter("attribute_set_name", $this->attributeSet)
            ->getFirstItem()
            ->getId();

        /** Set allowed websites */
        $allowedWebsites = explode(",", $this->websites);
        $websites = Mage::getModel('core/website')->getCollection()
                    ->addFieldToFilter('code', array('in' => $allowedWebsites));
        foreach ($websites as $website) {
            $this->websitesIds[] = $website->getId();
        }
    }

    /**
     * Main import method called from shell/import.php for --type cableProduct --file *.json
     * @param array $allProducts
     * @return mixed|void
     */
    public function importProduct($allProducts)
    {
        foreach ($allProducts as $product) {
            if ($this->hasValidPackageDefined($product)) {
                /** Setting default product data */
                $this->saveProductData($product);
            }
        }

        $this->_log("Finished: Updated " . (int)$this->updatedProducts . " products");
        $this->_log("Finished: Added " . (int)$this->newProducts . " new products");
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;
        if (empty($productData['packageType']) || !in_array(strtolower($productData['packageType']), Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions())) {
            $productData['packageType'] = !empty($productData['packageType']) ? $productData['packageType'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['productId'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $productData['packageType'], "\033[0m (case insensitive)\n";
            $this->_log("(SKU " . $productData['productId'] . ") Skipped entire product because of invalid package type value: " . $productData['packageType'] . " (case insensitive)");
            $valid = false;
        }

        if (empty($productData['packageSubtype']) || !in_array($productData['packageSubtype'], Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())) {
            $productData['packageSubtype'] = !empty($productData['packageSubtype']) ? $productData['packageSubtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['productId'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['packageSubtype'], "\033[0m  (case sensitive)\n";
            $this->_log("(SKU " . $productData['productId'] . ") Skipped entire product because of invalid package subType value: " . $productData['packageSubtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }


    public function saveProductData($productData)
    {
        /** Try to load product by SKU (productId - unique identifier) */
        $this->currentProduct = Mage::getModel('catalog/product');
        $productId = $this->currentProduct->getIdBySku($productData['productId']);
        $new = false;
        if ($productId) {
            $this->currentProduct->load($productId);
            $this->_log('Updating existing product "' . $productData['productId'] . '" with ID ' . $this->currentProduct->getId());
            $this->updatedProducts++;
        } else {
            $new = true;
            $this->currentProduct->setData('sku', $productData['productId']);
            $this->_log('Saving new product <' . $this->currentProduct->getSku() . '>');
            $this->newProducts++;
        }

        /** Setting default product data */
        $this->currentProduct->setAttributeSetId($this->attributeSetId);
        $this->currentProduct->setWebsiteIds($this->websitesIds);
        $this->currentProduct->setStatus(1);

        $this->scanData($productData);
        $this->_setDefaults($this->currentProduct, $productData);
        if ($new) {
            $this->_createStockItem($this->currentProduct);
        }

        /** Check if product has a tax_class defined */
        if (!$this->currentProduct->getTaxClassId() && !$this->currentProduct->getTaxClass()) {
            $this->setTaxClassIdByName("Taxable Goods");
        }

        $this->currentProduct->save();

        unset($this->currentProduct);
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);

        // Add product_code and promotion_code from productId
        $parts = explode(":", $data['productId']);
        $product->setProductCode($parts[0]);

        if (isset($parts[1])) {
            $product->setPromotionCode($parts[1]);
        }

        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * @param $data
     * @return bool|string
     */
    protected function scanData($data)
    {
        /** Has nodes beneath */
        if (is_array($data)) {
            /** Has numeric nodes, that means values need to be imploded and assigned to parent node */
            if ($this->hasNumericKeys($data)) {
                return implode(",", $data);
            }

            /** Try to scan deeper through nodes */
            foreach ($data as $key => $value) {
                $newValue = false;

                /** Check if this key is in the special case list */
                if (in_array($key, $this->specialCaseNodes)) {
                    $this->parseSpecialCase($key, $value);
                } else {
                    /** Not a special case, scan deeper for json nodes */
                    $newValue = $this->scanData($value);
                }

                /** If value exists, we need to assign it to current key */
                if ($newValue) {
                    if ($newValue && in_array($key, $this->specialNodesUsedForGrouping)) {
                        $this->_log("(SKU " . $this->currentProduct->getSku() . ") Received value <" . $newValue . "> for a grouping node <" . $key . ">. Trying to map this key to an attribute. This grouping key should be changed.");
                    }

                    $assigned = false;
                    /** Check if this key is mapped to an attribute */
                    if (array_key_exists($key, $this->mappedNodesToAttributes)) {
                        $this->currentProduct->setData($this->mappedNodesToAttributes[$key], $newValue);
                        $assigned = true;
                    }

                    /** Check if attribute exists for this key */
                    if (!$assigned && in_array($key, $this->productAttributes)) {
                        $this->currentProduct->setData($key, $newValue);
                        $assigned = true;
                    }

                    /** Check if conversion from camel case to snake case results in an existing attribute */
                    if (!$assigned && in_array($this->_replaceUpper($key), $this->productAttributes)) {
                        $this->currentProduct->setData($this->_replaceUpper($key), $newValue);
                        $assigned = true;
                    }

                    /** If none of the above and node is not in used for grouping list, log notice that attribute does not exists */
                    if (!$assigned) {
                        $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipped adding value <" . $newValue . "> because of unknown attribute <" . $key . ">.");
                    }
                }
            }
        } else {
            return $data;
        }

        /** default return: no value */
        return false;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function parseSpecialCase($key, $value)
    {
        switch ($key) {
            case "tariffChange":
                $this->parseTariffChange($value);
                break;
            case "price":
                $this->parsePrice($value);
                break;
            case "products":
                $this->parseRequiredProducts($value);
                break;
            case "serviceCategory":
                $this->parseServiceCategory($value);
                break;
            case "type":
                $this->parseType($value);
                break;
            case "selectable":
                $this->parseSelectable($value);
                break;
            case "contractCode":
                $this->parseContractCode($value);
                break;
            case "packageType":
                $this->parsePackageType($value);
                break;
            case "premiumClass":
                $this->parsePremiumClass($value);
                break;
            case "productSegment":
                $value = ucfirst(strtolower(trim($value)));
                $attribute = "product_segment";
                $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$productSegmentOptions);
                break;
            case "packageSubtype":
                $value = trim($value);
                $attribute = "package_subtype";
                $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions());
                break;
            default :
                $this->_log("(SKU " . $this->currentProduct->getSku() . ") special case defined for node <" . $key . "> but not treated in parseSpecialCase method.");
                break;
        }
    }

    /**
     * @param $value
     */
    protected function parsePremiumClass($value)
    {
        $value = ucfirst(strtolower(trim($value)));
        $attribute = "premium_class";

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$hintsPremiumClassOptions);
    }

    /**
     * @param $value
     */
    protected function parsePackageType($value)
    {
        $value = trim($value);
        $attribute = "package_type";

        $this->parseSelectTypeAttribute($attribute, strtolower($value), Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions());
    }

    /**
     * @param $value
     */
    protected function parseBillingFrequency($value)
    {
        $attribute = "price_billing_frequency";

        if (empty($this->cachedAttributes[$attribute][$value])) {
            $attr = $this->currentProduct->getResource()->getAttribute($attribute);
            if ($attr->usesSource()) {
                $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
            } else {
                $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                return;
            }
        }
        $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);

    }

    /**
     * @param $value
     */
    protected function parseContractCode($value)
    {
        $attribute = "contract_code";
        $value = strtoupper($value);

        $this->parseSelectTypeAttribute(
            $attribute,
            $value,
            Dyna_Cable_Model_Product_Attribute_Option::$durationOptions
        );
    }

    /**
     * @param $value
     */
    protected function parseSelectable($value)
    {
        $values = Dyna_Cable_Model_Product_Attribute_Option::$portalStateInitialOptions;
        $attribute = "selectable";

        if ($value == true) {
            $value = $values[0];
            if (empty($this->cachedAttributes[$attribute][$value])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);
        } elseif ($value == false) {
            $value = $values[1];
            if (empty($this->cachedAttributes[$attribute][$value])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);
        } else {
            $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <selectable> because it contains the value <" . $value . "> other than true or false.");
        }
    }

    /**
     * @param $value
     */
    protected function parseType($value)
    {
        $attribute = "type";
        $value = ucfirst(strtolower($value));

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$typeOptions);
    }

    /**
     * @param $value
     */
    protected function parseServiceCategory($value)
    {
        $attribute = "service_category";
        $value = strtoupper($value);

        $this->parseSelectTypeAttribute($attribute, $value, Dyna_Cable_Model_Product_Attribute_Option::$serviceCategoryOptions);
    }

    /**
     * Special case for productsForContracts attribute (omnius: req_rate_prod_for_contracts)
     *
     * @param $value array
     */
    protected function parseRequiredProducts($value)
    {
        $newValue = Mage::helper('core')->jsonEncode($value);
        $this->currentProduct->setData('req_rate_prod_for_contracts', $newValue);
    }

    /**
     * Parsing tariffChange values
     * @param $value array
     */
    protected function parseTariffChange($value)
    {
        $value['action'] = strtoupper(trim($value['action']));
        $attribute = "tariff_change_action";

        if ($value['action'] == "REPLACE" && !empty($value['replaceBy'])) {
            if (empty($this->cachedAttributes[$attribute][$value['action']])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value['action']] = $attr->getSource()->getOptionId($value['action']);
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value['action']]);
            $this->currentProduct->setTariffChangeReplaceBy($value['replaceBy']);
        } elseif ($value['action'] == "KEEP") {
            if (empty($this->cachedAttributes[$attribute][$value['action']])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value['action']] = $attr->getSource()->getOptionId($value['action']);
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value['action']]);
        } else {
            $this->_log("(SKU " . $this->currentProduct->getSku() . ") do not know what to do with <tariffChange>, dumping array to logs:");
            $this->_log($value);
        }
    }

    /**
     * Parsing price combination values
     * @param $value array
     */
    protected function parsePrice($value)
    {
        $priceSliding = "";
        $priceRepeated = "";
        $priceOnce = "";
        $vat = 0;
        foreach ($value as $price) {
            if (!empty($price['billingFrequency'])) {
                $this->parseBillingFrequency($price['billingFrequency']);

                if (strtolower(trim($price['billingFrequency'])) == 'monthly') {
                    $priceRepeated .= !empty($priceRepeated) ? ":" . $price['amount'] : $price['amount'];
                    $period = !empty($price['periodStart']) && !empty($price['periodEnd']) ? $price['periodEnd'] - $price['periodStart'] + 1 : "";
                    $priceSliding .= !empty($priceSliding) ? ":" . $period : $period;
                } elseif (strtolower(trim($price['billingFrequency'])) == 'once') {
                    $priceOnce = $price['amount'];
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping price value: invalid billing frequency <" . $price['billingFrequency'] . ">");
                }

            } else {
                $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping price value: no billing frequency set.");
            }

            if (!empty($price['includedVat'])) {
                $vat = (int)str_replace("%", "", $price['includedVat']);
            }
        }

        $this->currentProduct->setData('price_sliding', $priceSliding);
        $this->currentProduct->setData('price_repeated', $priceRepeated);
        $this->currentProduct->setData('price_once', $priceOnce);
        /** OMNVFDE-345: Map priceOnce to price and log if priceOnce is not defined */
        if ($priceOnce) {
            $this->currentProduct->setData('price', (float)$priceOnce);
        } else {
            $this->currentProduct->setData('price', 0);
            $this->_log("(SKU " . $this->currentProduct->getSku() . ") No price_once received, setting price 0.");
        }
        $this->currentProduct->setData('vat', $vat);
    }

    /**
     * @param $attribute
     * @param $value
     * @param $allowedValues
     */
    protected function parseSelectTypeAttribute($attribute, $value, $allowedValues)
    {
        if (in_array($value, $allowedValues)) {
            if (empty($this->cachedAttributes[$attribute][$value])) {
                $attr = $this->currentProduct->getResource()->getAttribute($attribute);
                if ($attr->usesSource()) {
                    $this->cachedAttributes[$attribute][$value] = $attr->getSource()->getOptionId($value);
                } else {
                    $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because it is not a select or multiselect type.");
                    return;
                }
            }
            $this->currentProduct->setData($attribute, $this->cachedAttributes[$attribute][$value]);
        } else {
            $this->_log("(SKU " . $this->currentProduct->getSku() . ") Skipping attribute <" . $attribute . "> because value <" . $value . "> it is not in the defined list of values.");
        }
    }

    /**
     * Check if current array contains numeric keys, if yes it will further be imploded and resulting value will be assigned to parent node
     *
     * @param $array
     * @return bool
     */
    protected function hasNumericKeys($array)
    {
        $numeric = true;

        foreach ($array as $key => $value) {
            if (!is_numeric($key)) {
                return false;
            }
        }

        return $numeric;
    }

    /**
     * Set product class id based on the provided tax_class_id in the CSV file
     *
     * @param string $className
     * @return bool
     */
    protected function setTaxClassIdByName($className)
    {
        $productTaxClass = Mage::getModel('tax/class')
            ->getCollection()
            ->addFieldToFilter('class_name', $className)
            ->load()
            ->getFirstItem();
        if (!$productTaxClass || !$productTaxClass->getId()) {
            return false;
        }
        $this->currentProduct->setTaxClassId($productTaxClass->getId());

        return true;
    }
}
