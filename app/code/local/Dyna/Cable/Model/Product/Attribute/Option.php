<?php

/**
 * Class Dyna_Cable_Model_Product_Attribute_Option
 *
 * Define custom values for attribute options
 */
final class Dyna_Cable_Model_Product_Attribute_Option
{
    public static $typeOptions = [
        0 => 'Bundle',
        1 => 'Container',
        2 => 'Product',
        3 => 'Device',
        4 => 'Fee',
        5 => 'Option',
        6 => 'Skip',
        7 => 'Feature',
        8 => 'Info',
        9 => 'Memo',
        10 => 'Service',
        11 => 'Virtual',
        12 => 'Interest',
    ];

    public static $serviceCategoryOptions = [
        0 => 'KAI',
        1 => 'KAV',
        2 => 'KAD',
        3 => 'KAA',
        4 => 'KUD',
        5 => 'CMD',
        6 => 'VFV',
    ];

    public static $serviceTypeOptions = [
        0 => 'D',
        1 => 'I',
        2 => 'J',
        3 => 'M',
        4 => 'V',
    ];

    public static $productSegmentOptions = [
        0 => 'Privat',
        1 => 'Business',
        2 => 'Sonderkunde',
    ];

    public static $priceBillingFrequencyOptions = [
        0 => 'ONCE',
        1 => 'MONTHLY',
        2 => 'ANNUAL',
    ];

    public static $tariffChangeActionOptions = [
        0 => 'KEEP',
        1 => 'REPLACE',
    ];

    public static $hintsPremiumClassOptions = [
        0 => 'Standard',
        1 => 'Premium',
    ];

    public static $portalStateInitialOptions = [
        0 => '-',
        1 => 'x',
        2 => 'X (not selectable)',
        3 => '+',
    ];

    public static $durationOptions = [
        0 => 'HP1',
        1 => 'BUN',
        2 => 'VOD',
        3 => 'TRA',
        4 => 'D12',
        5 => 'D24',
        6 => 'A12',
        7 => 'D02',
        8 => '4K2',
        9 => 'A24',
        10 => 'HSN',
        11 => 'AN3',
        12 => 'D03',
        13 => 'T14',
        14 => '305',
        15 => 'A15',
        16 => 'IPS',
        17 => 'W12',
        18 => '4S1',
        19 => 'TRP',
        20 => 'KHW',
        21 => 'AD2',
    ];

    public static $licenseTypeOptions = [
        0 => 'HighSecureProfessional1',
        1 => 'HighSecureProfessional2',
        2 => 'HighSecureProfessional5',
        3 => 'OnlineBackup1',
    ];

    public static function packageSubtypeOptions()
    {
        $allPackageSubtypes = array_merge(
            Dyna_Catalog_Model_Type::$subscriptions,
            Dyna_Catalog_Model_Type::$devices,
            Dyna_Catalog_Model_Type::$accessories,
            Dyna_Catalog_Model_Type::$options,
            Dyna_Catalog_Model_Type::$promotions,
            Dyna_Catalog_Model_Type::$fees,
            Dyna_Catalog_Model_Type::$unknown
        );

        return $allPackageSubtypes;
    }

    public static function packageTypeOptions()
    {
        return Dyna_Catalog_Model_Type::getPackageTypes();
    }

//    public static $packageTypeOptions = [
//        1 => 'CABLE_INTERNET_PHONE',
//        2 => 'CABLE_INDEPENDENT',
//        3 => 'CABLE_TV',
//        4 => 'Mobile',
//        5 => 'Fixed',
//        6 => 'LTE',
//    ];

//    public static $packageSubtypeOptions = [
//        0 => 'Tariff',
//        1 => 'Hardware',
//        2 => 'Option',
//        3 => 'Accessory',
//        4 => 'Setting',
//        5 => 'Discount',
//        6 => 'Simcard',
//        7 => 'Subscription',
//        8 => 'Footnote',
//        // @TODO This will need to be removed after the demo (added for Miscellaneous POC)
//        9 => 'Miscellaneous',
//        10 => 'Promotion',
//    ];
}
