<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$attributes = [
    'marketable' => [
        'frontend_input' => 'boolean',
        'type' => 'int',
        'source_model' => 'eav/entity_attribute_source_boolean',
        'note' => 'Marketable == shown on the GUI; Non-marketable == not shown on the GUI.',
    ],
    'price_billing_frequency' => [
        'frontend_input' => 'select',
        'type' => 'int',
        'note' => '',
    ]
];

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');

    if ($attributeId) {
        $installer->updateAttribute($entityTypeId, $attributeId, $options);
    }
}

// Update price_billing_frequency attribute options
$attribute_code = 'price_billing_frequency';
$attribute = Mage::getModel('eav/config')->getAttribute($entityTypeId, $attribute_code);
$options = $attribute->getSource()->getAllOptions(false);

// Remove old values
$existingOption = [];
foreach ($options as $option) {
    if (!in_array($option['label'], $attributeValues)) {
        $existingOption['delete'][$option['value']] = true;
        $existingOption['value'][$option['value']] = true;
        $installer->addAttributeOption($existingOption);
    }
}

$installer->addAttributeOption($newOption);

// Add new values
$attributeValues = Dyna_Cable_Model_Product_Attribute_Option::$priceBillingFrequencyOptions;

$newOption = [
    'attribute_id' => $attributeId,
    'values' => $attributeValues,
];

$installer->addAttributeOption($newOption);

$installer->endSetup();
