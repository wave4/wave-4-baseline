<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Update attributes
$attributes = [
    'hints_is_multiple' => [
        'frontend_input' => 'text',
        'type' => 'text',
        'backend_model' => null,
        'backend_type' => 'text',
        'source' => null,
    ],
    'hints_is_phone_option' => [
        'frontend_input' => 'text',
        'type' => 'text',
        'backend_type' => 'text',
        'backend_model' => null,
        'source' => null,
    ],
];


foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttribute($entityTypeId, $code, 'attribute_id');
    if ($attributeId) {
        $installer->updateAttribute($entityTypeId, $attributeId, $options);
    }
}

$installer->endSetup();