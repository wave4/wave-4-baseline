<?php
$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$attributesToBeRemoved = [
    'portal_sequence_no',
    'hints_is_droppable',
    'portal_state_initial',
    'portal_channels_overview',
    'portal_hide_summary',
    'portal_hide_inventory',
    'portal_display_label',
    'portal_display_add_line',
    'hints_premium_class',
    'hints_req_bandwidth',
    'hints_contract_period',
    'hints_priority',
    'hints_contract_period_check',
    'hints_privat_hd_upsell',
    'hints_auto_privat_hd',
    'hints_ccb_auto',
    'hints_force_manual',
    'hints_has_erotic_option',
    'hints_is_digitaler_empfang',
    'hints_is_basic',
    'hints_is_bundle_part',
    'hints_is_droppable',
    'hints_is_filter',
    'hints_vod',
    'hints_is_hosting',
    'hints_is_backup',
    'hints_allows_customer_sla',
    'hints_contract_type',
    'hints_is_messaging',
    'hints_is_multiple',
    'hints_is_phone_option',
    'hints_is_upsell',
    'hints_skip_devices',
    'hints_stand_alone',
    'hints_placeholder',
    'hints_filter_by_osp',
    'hints_id_request',
    'hints_is_erotic',
    'hints_is_free',
    'hints_is_home',
    'hints_is_hd',
    'hints_is_hsp',
    'hints_is_international',
    'hints_is_internet_only',
    'hints_is_kd_plus',
    'hints_is_phone_only',
    'hints_is_selfinstall',
    'hints_is_usage_modifier',
    'hints_no_connection_fee',
    'hints_negativ_option',
    'hints_contract_exchange',
];

foreach ($attributesToBeRemoved as $attributeCode) {
    $installer->removeAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
}

$installer->endSetup();
