<?php

/**
 * Cable product Hints attributes
 */

$installer = $this;
/* @var $installer Dyna_Cable_Model_Resource_Setup */

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeSetName = 'KD_Cable_Products';

/** Portal */
$attributes = [
    'portal_state_initial' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal state initial',
        'input' => 'select',
        'type' => 'int',
        'backend' => 'eav/entity_attribute_backend_array',
        'option' => [
            'values' => Dyna_Cable_Model_Product_Attribute_Option::$portalStateInitialOptions,
        ],
        'note' => 'set initial status of a checkbox Values: -: selectable, not selected, x: not selectable, not selected, X:not selectable, selected, +: selectable, selected',
    ],
    'portal_hide_summary' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal hide summary',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'suppress the product in the order summary page',
    ],
    'portal_hide_inventory' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal hide inventory',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'suppress the product in inventory (in CSC existing customer display)',
    ],
    'portal_variable_price' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal variable price',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'defines a value check (priceRange) for products with variable prices',
    ],
    'portal_variable_billing_freq' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal variable billing frequency',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'note' => 'if field is set, then it is possible in Frontend to choose the billingFrequency',
    ],
    'portal_channels_overview' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal channels overview',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
    ],
    'display_frontend_category' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal display frontend category',
        'input' => 'text',
        'type' => 'text',
    ],
    'portal_group' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal group',
        'input' => 'text',
        'type' => 'text',
    ],
    'portal_sequence_no' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal sequence no',
        'input' => 'text',
        'type' => 'int',
        'note' => 'Number indicates that the product is active and number is the position between the group (for tv-device the number has 5 digits with codingInformation)',
    ],
    'portal_display_label' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal display label',
        'input' => 'text',
        'type' => 'text',
        'note' => 'The product description in the different portal',
    ],
    'portal_display_add_line' => [
        'attribute_set' => $attributeSetName,
        'group' => 'Cable',
        'label' => 'Portal display add line',
        'input' => 'text',
        'type' => 'text',
        'note' => 'the additional text for a product',
    ],
];

$createAttributeSets = [];
foreach ($attributes as $code => $options) {
    $attributeSet = $options['attribute_set'];
    $group = $options['group'];
    unset($options['group'], $options['attribute_set']);

    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (! $attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }

    $createAttributeSets[$attributeSet]['groups'][$group][] = $code;
}

$installer->assignAttributesToSet($createAttributeSets, $sortOrder);
unset($attributes, $createAttributeSets);

$installer->endSetup();
