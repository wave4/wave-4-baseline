<?php

/**
 * Class Dyna_Fixed_Model_Import_CableProduct
 */
class Dyna_Fixed_Model_Import_FixedProduct extends Omnius_Import_Model_Import_Product_Abstract
{
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "fixed_product_import";

    /**
     * Dyna_Import_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '_' . date("Y-m-d") . '.log');
        $this->_qties = 0;
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;

        $data = $this->_setDataMapping($data);

        if (!$this->hasValidPackageDefined($data)) {
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            return;
        }

        $data['sku'] = trim($data['sku']);

        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . $data['name']);
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product"' . $data['sku'] . '" with ID ' . $id);
                $product->load($id);
            }
            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setTaxClassIdByName($product, $data)) {
                $this->_logError('Unknown tax class "' . $data['tax_class_id'] . '" for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);

            $this->_setProductData($product, $data);

            if ($new) {
                $this->_createStockItem($product);
            }

            if (!$this->getDebug()) {
                $product->save();
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $productVisibilityInfo['groups']);
                } else {
                    //uncomment the following line if products that have no defined filters must be treated as available for all groups
                    //Mage::getModel('productfilter/filter')->createRule($product->getId(), Dyna_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null); //available for all dealer groups
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name']);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;

        $packageTypes = explode(',', $productData['package_type']);
        $validPackageTypes = Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions();
        $invalidPackageType = null;
        foreach ($packageTypes as $packageType) {
            if (!in_array(strtolower($packageType), $validPackageTypes)) {
                $invalidPackageType = $packageType;
                break;
            }
        }

        if (empty($productData['package_type']) || $invalidPackageType) {
            $productData['package_type'] = !empty($productData['package_type']) ? $productData['package_type'] : "none provided";

            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $invalidPackageType, "\033[0m (case insensitive)\n";
            $this->_log("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package type value: " . $invalidPackageType . " (case insensitive)");
            $valid = false;
        }

        if (empty($productData['package_subtype']) || !in_array($productData['package_subtype'], Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())) {
            $productData['package_subtype'] = !empty($productData['package_subtype']) ? $productData['package_subtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['package_subtype'], "\033[0m (case sensitive)\n";
            $this->_log("[ERROR] (SKU " . $productData['sku'] . ") Skipped entire product because of invalid package subType value: " . $productData['package_subtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }

    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {

        // Hardcoded values
        // todo: remove hardcoded 'telesales' website code
        $data['_product_websites'] = 'telesales';

        // Default value 0 for hide attributes
        if (!isset($data['hide_in_configurator'])) {
            $data['hide_in_configurator'] = 0;
        }
        if (!isset($data['hide_in_shopping_cart'])) {
            $data['hide_in_shopping_cart'] = 0;
        }
        if (!isset($data['hide_in_order_overview'])) {
            $data['hide_in_order_overview'] = 0;
        }

        // todo: determine product status
        $data['status'] = 1;

        // new name => old name
        $renamedFields = [
            'gui_name' => 'hardware_gui_name',
            'name' => 'internal_hardware_name',
            'fn_new_start_date_nc' => 'fn_new_start_sales_date_new_customer',
            'fn_new_end_date_nc' => 'fn_new_end_sales_date_new_customer',
            'fn_change_start_date_bc' => 'fn_change_start_sales_date_base_customer',
            'fn_change_end_date_bc' => 'fn_change_end_sales_date_base_customer',
            'isdn_network_capable_nt_spi' => 'isdn_network_capable(nt-split_integrated)',
            'hw_sub_type' => 'hw_sub-type',
            'monthly_price' => 'monthly_price(rental_or_surcharge)',
            'allowed_unsubsidized_sales' => 'allow_for_unsubsidized_sales',
            'vdsl2_vectoring' => 'vdsl2-vectoring',
            'vdsl2_legacy' => 'vdsl2-vectoring',
            'main_access_technology' => 'access_technology',
            'isdn_enabled' => 'isdn-enabled(s0)',
            'ipv6_enabled' => 'i_pv6enabled',
            'adsl2_plus' => 'adsl2+',
            'acs_enabled' => 'acs-enabled',
            'tax_class_id' => 'tax_class',
            'maf' => 'monthly_price',
        ];

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
                unset($data[$old]);
            }
        }

        if (empty($data['sku']) && !empty($data['hw_materianumer'])) {
            /** @done check with BA if HW needs to be prepended to sku to prevent sku collisions */
            /** nothing needed to be prepended to prevent sku collisions (see OMNVFDE-72) */
            // If sku empty, try to get it from hw_materianumer
            $data['sku'] = !empty($data['sku']) ? $data['sku'] : $data['hw_materianumer'];
        }

        if (empty($data['name']) && !empty($data['gui_name'])) {
            $data['name'] = $data['gui_name']; // None
        }

        if (empty($data['gui_name'])) {
            //If set, it will be preserved
            $data['gui_name'] = $data['name'];
        }

        if (!empty($data['fn_new_start_date_nc'])) {
            $data['fn_new_start_date_nc'] = $this->determineFnStartDate($data['fn_new_start_date_nc']);
        }

        if (!empty($data['fn_change_start_date_bc'])) {
            $data['fn_change_start_date_bc'] = $this->determineFnStartDate($data['fn_change_start_date_bc']);
        }

        /** Converting the remaining date fields to mysql friendly format, assuming that these do contain valid values */
        $fieldsKeys = array(
            "fn_new_start_date_nc",
            "fn_new_end_date_nc",
            "fn_change_start_date_bc",
            "fn_change_end_date_bc",
            "portfolio",
        );
        foreach ($fieldsKeys as $key) {
            if (!empty($data[$key])) {
                $data[$key] = date("Y-m-d", strtotime($data[$key]));
            }
        }

        if (!empty($data['price_subsidized'])) {
            $data['price_subsidized'] = str_replace(",", ".", $data['price_subsidized']);
        }

        if (!empty($data['price_unsubsidized'])) {
            $data['price_unsubsidized'] = str_replace(",", ".", $data['price_unsubsidized']);
        }

        if (!empty($data['monthly_price'])) {
            $data['monthly_price'] = str_replace(",", ".", $data['monthly_price']);
        }

        if (empty($data['attribute_set'])) {
            $data['attribute_set'] = $this->_getAttributeSet($data);
        }

        if (empty($data['price']) && !empty($data['ot_value_euro_brutto'])) {
            $data['price'] = str_replace("€", "", $data['ot_value_euro_brutto']);
        }

        return $data;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Return the appropriate attribute set to which the current product is assigned
     * The entire imported product info is passed for future improvements:
     * ex: determining the attribute_set from combination of product properties
     *
     * @readme to determine the Attribute Set to which a product is assigned, either use a column named hw_type with values from bellow switch
     * @readme of use a column named attribute_set which contains the attribute set name from magento
     *
     * @param $product array
     * @return string
     */
    protected function _getAttributeSet($product)
    {
        /** @done check with BA if the default attribute set for fixed line remains hardware for those products that do not have a defined hw_type attribute */
        /** new attribute set for empty HW Type column import (see OMNVFDE-72): FN_Unknown */
        $attributeSet = "FN_Unknown";

        /** custom checks are made because CSV columns differ from one imported file to another */
        if (isset($product['hw_type'])) {
            switch ($product['hw_type']) {
                case "IAD" :
                case "STB" :
                case "SIM" :
                    $attributeSet = "FN_Hardware";
                    break;
                case "ACC" :
                    $attributeSet = "FN_Accessory";
                    break;
                /** @done check with BA what attribute set should the products of hw_type bellow be related to */
                /** No attribute set should be used in this case (see OMNVFDE-72), but the import will fail so the Default_Set will be used */
                case "NTD" :
                case "VMO" :
                case "LMO" :
                    $attributeSet = "Default_Set";
                    break;
                default:
                    break;
            }
        }

        /** if attribute_set column exists, but has an empty values, we'll preserve the previous attributeSet value */
        if (isset($product['attribute_set'])) {
            $attributeSet = trim($product['attribute_set']) ?: $attributeSet;
        }

        return $attributeSet;
    }

    /**
     * Replace non date strings and strings values that have a date like string inside it's content
     *
     * @access private
     * @param string $startDate
     * @return string
     */
    private function determineFnStartDate($startDate)
    {
        /** fn_change_start_date_bc: Replacement for non date strings */
        $startDate = in_array(
            $startDate,
            array(
                "keine Vermarktung (GK Artikel)",
                "keine Vermarktung",
            )
        ) ? "" : $startDate;

        /** Replacement for string values having a date like string inside it's content */
        if (preg_match("/[0-9]{2}.[0-9]{2}.[0-9]{4}/", $startDate, $matches)) {
            $startDate = current($matches);
        }

        return $startDate;
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }
}
