<?php
/**
 * Installer for adding new attributes to Fixed products (OMNVFDE-343)
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->_generalGroupName = "Fixed";

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributes = [
    'FN_Options' => [
        'product_type' => [
            'label' => 'Product Type',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'maf' => [
            'label' => 'Maf',
            'input' => 'price',
            'type' => 'decimal',
        ],
    ],
    'FN_Promotion' => [
        'kassenzettel_relevant' => [
            'label' => 'Kassenzettel Relevant',
            'input' => 'boolean',
            'type' => 'int',
            'source' => 'eav/entity_attribute_source_boolean',
        ],
        'order_line_title' => [
            'label' => 'Order Line Title',
            'input' => 'text',
            'type' => 'varchar',
        ],
        'condition_service_type' => [
            'label' => 'Condition Service Type',
            'input' => 'text',
            'type' => 'varchar',
        ],
        'condition_service_name' => [
            'label' => 'Condition Service Name',
            'input' => 'text',
            'type' => 'varchar',
        ],
        'condition_service_price' => [
            'label' => 'PSM Product Name',
            'input' => 'price',
            'type' => 'decimal',
        ],
        'maf' => [
            'label' => 'Maf',
            'input' => 'price',
            'type' => 'decimal',
        ],
    ],
    'FN_SalesPackage' => [
        'product_type' => [
            'label' => 'Product Type',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'gross_package_price_brutto' => [
            'label' => 'Gross Package Price (Brutto)',
            'input' => 'price',
            'type' => 'decimal',
        ],
        'print_name1' => [
            'label' => 'Printname 1',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'print_name2' => [
            'label' => 'Printname 2',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'print_name3' => [
            'label' => 'Printname 3',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'backend_technology' => [
            'label' => 'Backend Technology',
            'input' => 'text',
            'type' => 'varchar'
        ],
        'bandwidth' => [
            'label' => 'Bandwidth',
            'input' => 'text',
            'type' => 'varchar',
        ],
        'minimum_contract_duration' => [
            'label' => 'Minimum Contract Duration',
            'input' => 'text',
            'type' => 'int',
        ],
        'minimum_contract_duration_unit' => [
            'label' => 'Minimum Contract Duration Unit',
            'input' => 'text',
            'type' => 'varchar',
        ],
    ],
    "FN_Hardware" => [
        'subvention_code' => [
            'label' => 'Subvention Code',
            'input' => 'boolean',
            'type' => 'int',
            'source' => 'eav/entity_attribute_source_boolean',
        ],
        'maf' => [
            'label' => 'Maf',
            'input' => 'price',
            'type' => 'decimal',
        ],
    ],
    "FN_Accessory" => [
        'subvention_code' => [
            'label' => 'Subvention Code',
            'input' => 'boolean',
            'type' => 'int',
            'source' => 'eav/entity_attribute_source_boolean',
        ],
    ],
];

foreach ($attributes as $attributeSetCode => $attributesList) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
    if (!$attributeSetId) {
        $installer->addAttributeSet($entityTypeId, $attributeSetCode);
        $attributeSetId = $installer->getAttributeSetId($entityTypeId, $attributeSetCode);
    }

    $attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, "Fixed");

    foreach ($attributesList as $attributeCode => $attributeData) {
        $attributeData['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $attributeCode, $attributeData);
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, $attributeCode);
    }
}

$installer->endSetup();
