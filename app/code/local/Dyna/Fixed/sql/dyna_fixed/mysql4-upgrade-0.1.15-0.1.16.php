<?php
/**
 * OMNVFDE-352
 */

/* @var $this Dyna_Fixed_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Remove obsolete attributes from FN_Accessory
$attributeCodes = [
    "internal_name",
    "print_name",
    "display_name",
    "special_legal_text",
    "special_legal_text_nr_porting",
    "special_legal_text_for_redone",
    "product_hint",
    "not_compatible_hint",
    "code",
    "service_item_type",
    "gui_name",
    "gui_hint_text",
    "print_text",
    "print_additional_text",
    "visibility",
];

$attributeSetCode = 'FN_Accessory';
$attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");

foreach ($attributeCodes as $attributeCode) {
    $attId = $installer->getAttribute($entityTypeId, $attributeCode, 'attribute_id');
    if ($attId && $attributeSetId) {
        try {
            Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attId, $attributeSetId);
        } catch (Exception $e) {
            // skip if error
        }
    }
}



// Assign correct attributes to FN_Accessory
$assignAttributes = [
    "internal_hardware_name",
    "hardware_print_name",
    "maf",
    "print_kassenzettel",
    "print_if_price_is_zero",
    "allowed_unsubsidized_sales",
];

foreach ($assignAttributes as $attributeCode) {
    if ($attributeSetId) {
        $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Fixed", $attributeCode);
    }
}

$installer->endSetup();
