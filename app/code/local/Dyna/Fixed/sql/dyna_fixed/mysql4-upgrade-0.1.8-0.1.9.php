<?php
/**
 * Set the attribute "monthly_price" to the attribute set "FN_Options" and to attribute group "Fixed"
 */

$installer = $this;

/* @var $installer Dyna_Mobile_Model_Resource_Setup */
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeSetId = $installer->getAttributeSetId($entityTypeId, 'FN_Options');

/** @var $attributeSetCollection Mage_Eav_Model_Resource_Entity_Attribute_Group_Collection */
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')->load();

foreach ($attributeSetCollection as $id => $attributeGroup) {
    if ($attributeGroup->getAttributeGroupName() == 'Fixed' && $attributeGroup->getAttributeSetId() == $attributeSetId) {
        $attributeGroupId = $attributeGroup->getAttributeGroupId();
    }
}
$attributeId = $installer->getAttributeId($entityTypeId, 'monthly_price');
$installer->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, 35);

$installer->endSetup();



