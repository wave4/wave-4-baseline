<?php
/**
 * Map effective_date and expiration_date to all Fixed attribute sets
 */

/* @var $this Dyna_Fixed_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Assign effective_date and expiration_date to all Fixed attribute sets
$attributeCodes = [
    "effective_date",
    "expiration_date",
];

$attributeSets = [
    "FN_Accessory",
    "FN_Hardware",
    "FN_Promotion",
    "FN_Options",
    "FN_SalesPackage",
];

foreach ($attributeCodes as $attributeCode) {
    foreach ($attributeSets as $attributeSetCode) {
        $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
        if ($attributeSetId) {
            $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Fixed", $attributeCode);
        }
    }
}

// Remove obsolete attributes
$obsoleteAttributes = [
    'sales_start_date',
    'sales_end_date',
];

foreach ($obsoleteAttributes as $attribute) {
    $installer->removeAttribute($entityTypeId, $attribute);
}

$installer->endSetup();
