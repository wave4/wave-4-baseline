<?php

class Dyna_Bundles_Helper_Data extends Omnius_Bundles_Helper_Data
{
    /**
     * Get all Bundles; optional parameter is the category
     *
     * @param $category
     * @return Omnius_Bundles_Model_Resource_Bundle_Collection
     */
    public function getAllBundles($category = null)
    {
        $bundles = Mage::getModel('bundles/bundle')->getCollection();
        if ($category) {
            // todo: implement category restriction
        }

        return $bundles;
    }

    /**
     * @return array
     */
    public function getCampaignsData()
    {
        $campaigns = [];

        // Customer allowed campaigns
        $customerAllowedCampaigns = Mage::getSingleton('customer/session')
                                    ->getCustomer()
                                    ->getAllowedCampaigns();

        $bundles = $this->getAllBundles();

        /** @var Omnius_Bundles_Model_Bundle $bundle */
        foreach ($bundles as $bundle) {
            $bundlePackages = $bundle->getPackages();

            // todo: check if we need to display bundles with more than one package
            if (count($bundlePackages) > 1) {
                continue;
            }

            /** @var Omnius_Bundles_Model_Package $bundlePackage */
            $bundlePackage = $bundlePackages->getFirstItem();

            $packageProduct = [];
            foreach ($bundlePackage->getItems() as $item) {
                $packageProduct[] = Mage::getModel('catalog/product')->getResource()->getIdBySku($item->getSku());
            }
            $packageProductList = implode(",", $packageProduct);

            $packageItems = $bundlePackage->getItemsForDisplay();
            /** @var  $category Omnius_Bundles_Model_Category */
            $category = $bundlePackage->getCategories()->getFirstItem();

            $data = [
                "id" => $bundlePackage->getId(),
                "contract_duration_2" => "13",
                // todo: calculate price of the bundle (auto or manual in backend)?
                "price_contract_duration_2" => "34,99",
                "name"=> $category->getName(),
                "identifier" => $category->getIdentifier(),
                "status" => in_array($category->getIdentifier(), $customerAllowedCampaigns) ? 1 : 0,
                "initConfiguratorData" => [
                    // The package type defined for this bundle package
                    'type' => strtolower($bundlePackage->getType()),
                    'packageId' => null,
                    'saleType' => 'acquisitie',
                    'ctn' => null,
                    //Only ony product as Campaigns by definition are Omnius Bundles with one Omnius Package that includes one product
                    'currentProducts' => $packageProductList,

                ],
            ];

            foreach ($packageItems as $key => $sku) {
                /** @var Dyna_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

                if ($product->isDevice()) {
                    $data['device'] = $product->getName();
                }
                if ($product->isSubscription()) {
                    $contractDuration = $product->getContractDuration();
                    if (is_array($contractDuration)) {
                        $data['contract_duration_1'] = array_shift($contractDuration);
                    }
                    $contractPeriod2 = $contractDuration ? array_shift($contractDuration) : null;
                    // todo second contract duration calculation .. it was hardcoded to 13 before 
                    $data['contract_duration_2'] = ! empty($contractPeriod2) ? $contractPeriod2 : '';

                    $data['subscription'] = $bundle->getName();

                    $data['price_contract_duration_1'] = Mage::helper('tax')->getPrice($product, $product->getMaf(), true);
                }
            }

            $campaigns[] = $data;
        }

        return ['campaigns' => $campaigns];
    }
}

