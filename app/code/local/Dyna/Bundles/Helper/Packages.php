<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Bundles_Helper_Packages
 */
class Dyna_Bundles_Helper_Packages extends Omnius_Bundles_Helper_Packages
{

    /**
     * Build array based on the backend defined types
     *
     * @return array
     */
    public function getTypesForDropdown()
    {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $result = [];
        foreach ($attribute->getSource()->getAllOptions(false) as $option) {
            $result[$option['label']] = $this->__($option['label']);
        }

        return $result;
    }
}
