<?php

class Dyna_Catalog_Block_Adminhtml_Delivery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                "id" => "edit_form",
                "action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
                "method" => "post",
                "enctype" => "multipart/form-data",
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $yesno = array(
            '0' => Mage::helper('catalog')->__('No'),
            '1' => Mage::helper('catalog')->__('Yes')
        );

        $packageTypes = array();
        foreach (Dyna_Catalog_Model_Type::getPackageTypes() as $type) {
            $packageTypes[$type] = $type;
        }

        $deliveryTypes = array();
        foreach(Dyna_Catalog_Model_Delivery::getDeliveryTypes() as $type) {
            $deliveryTypes[$type] = $type;
        }

        $fieldSet = $form->addFieldset("general",
            array(
                "legend" => "Delivery Options"
            )
        );

        $fieldSet->addField("name", "text",
            array(
                "name" => "name",
                "label" => "Name",
                "input" => "text",
                "required" => true
            )
        );

        $fieldSet->addField("package_type", "select",
            array(
                "name" => "package_type",
                "label" => "Package type",
                "input" => "text",
                "required" => true,
                "values" => $packageTypes
            )
        );

        $fieldSet->addField("price", "text",
            array(
                "name" => "price",
                "label" => "Price",
                "input" => "test",
                "required" => true,
                "class" => "validate-number"
            )
        );

        $fieldSet->addField("delivery_type", "select",
            array(
                "name" => "delivery_type",
                "label" => "Delivery Type",
                "input" => "text",
                "required" => true,
                "values" => $deliveryTypes
            )
        );

        $fieldSet->addField("is_new_customer", "select",
            array(
                "name" => "is_new_customer",
                "label" => "New Customer",
                "input" => "text",
                "required" => true,
                "values" => $yesno
            )
        );

        $deliveryModel = Mage::registry("catalog_delivery_option");
        if ($deliveryModel) {
            $form->addValues($deliveryModel->getData());
        }

        return parent::_prepareForm();
    }
}
