<?php

class Dyna_Catalog_Block_Adminhtml_Delivery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("deliveryGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("dyna_catalog/delivery")->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("id",
            array(
                "header" => Mage::helper("catalog")->__("Id"),
                "type" => "number",
                "index" => "id"
            )
        );

        $this->addColumn("name",
            array(
                "header" => Mage::helper("catalog")->__("Name"),
                "index" => "name"
            )
        );

        $this->addColumn("package_type",
            array(
                "header" => Mage::helper("catalog")->__("Package type"),
                "index" => "package_type"
            )
        );

        $this->addColumn("price",
            array(
                "header" => Mage::helper("catalog")->__("Price"),
                "index" => "price"
            )
        );

        $this->addColumn("delivery_type",
            array(
                "header" => Mage::helper("catalog")->__("Delivery type"),
                "index" => "delivery_type"
            )
        );

        $this->addColumn("is_new_customer",
            array(
                "header" => Mage::helper("catalog")->__("Is new customer"),
                "index" => "is_new_customer"
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

}
