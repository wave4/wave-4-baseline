<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Catalog_Helper_Data extends Omnius_Catalog_Helper_Data
{
    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @param $product
     * @return mixed
     */
    public function getProductsForStock($websiteId, $subtype, $cache, $product)
    {
        $productsCacheKey = 'catalog_products_' . $subtype . '_stock_call_' . $websiteId;
        if ($productsCached = $cache->load($productsCacheKey)) {
            $products = unserialize($productsCached);
        } else {
            $productsColl = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, array('eq' => $product));

            $products = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAssoc($productsColl->getSelect());
            unset($productsColl);
            $cache->save(serialize($products), $productsCacheKey, array(), $cache->getTtl());
            unset($productsCacheKey);
        }

        return $products;
    }

    /**
     * @param $websiteId
     * @param $subtype
     * @param $cache
     * @return mixed
     */
    public function getProductForStock($websiteId, $subtype, $cache)
    {
        $attributeCacheKey = $subtype . '_attribute_stock_admin_labels_' . $websiteId;
        if ($attributeCached = $cache->load($attributeCacheKey)) {
            $product = unserialize($attributeCached);
        } else {
            $attribute = Mage::getSingleton("eav/config")
                ->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
            $product = Mage::helper('omnius_catalog')->getAttributeAdminLabel(
                $attribute,
                null,
                $subtype
            );

            unset($attribute);
            $cache->save(serialize($product), $attributeCacheKey, array(), $cache->getTtl());
        }

        return $product;
    }
}
