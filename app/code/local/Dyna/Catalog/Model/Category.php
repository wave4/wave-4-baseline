<?php

class Dyna_Catalog_Model_Category extends Omnius_Catalog_Model_Category
{
    /**
     * Get a list of all categories that are family and mutually exclusive (if the current model has an id, that id is not returned)
     *
     * @return array
     */
    public function getAllExclusiveFamilyCategoryIds()
    {
        $key = md5(serialize([strtolower(__METHOD__), $this->getId()]));
        if (!($result = unserialize($this->getCache()->load($key)))) {
            $result = [];
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $this->getCollection()->addAttributeToFilter('is_family', 1)->load();
            foreach ($collection as $category) {
                if ($category->getId() != $this->getId()) {
                    $result[] = $category->getId();
                }
            }
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }
}
