<?php

/**
 * Class Dyna_Catalog_Model_Type
 */
class Dyna_Catalog_Model_Type extends Omnius_Catalog_Model_Type
{
    // FN package types
    const TYPE_FIXED_DSL = 'DSL';
    const TYPE_LTE = 'LTE';

    // Cable package types
    const TYPE_CABLE_TV = 'CABLE_TV';
    const TYPE_CABLE_INTERNET_PHONE = 'CABLE_INTERNET_PHONE';
    const TYPE_CABLE_INDEPENDENT = 'CABLE_INDEPENDENT';

    // Mobile package types
    const TYPE_MOBILE = 'Mobile';
    const TYPE_PREPAID = 'Prepaid';

    const SUBTYPE_SUBSCRIPTION = 'Tariff';
    const SUBTYPE_ADDON = 'Option';
    const SUBTYPE_GOODY = 'Goody';
    const SUBTYPE_SETTING = 'Setting';

    // Unknown package subtypes
    const SUBTYPE_CHECKOUTOPTION = 'CheckoutOption';
    const SUBTYPE_HIDDEN = 'hidden';
    const SUBTYPE_ADDITIONAL_SERVICES = 'Additional_services';
    const SUBTYPE_FOOTNOTE = 'Footnote';

    const SUBTYPE_FEE = 'Fee';
    const SUBTYPE_ACCESSORY = 'Accessory';
    const SUBTYPE_DEVICE = 'Hardware';

    const MOBILE_PREPAID_SUBSCRIPTION = 'Tariff';

    const MOBILE_SUBTYPE_SUBSCRIPTION = 'Tariff';
    const MOBILE_SUBTYPE_DEVICE = 'Device';
    const MOBILE_SUBTYPE_ADDON = "Option";
    const MOBILE_SUBTYPE_PROMOTION = 'Discount';
    const MOBILE_SUBTYPE_DISCOUNTS = 'Discounts';

    const DSL_SUBTYPE_SUBSCRIPTION= 'Tariff';
    const DSL_SUBTYPE_PROMOTION = 'Promotion';
    const DSL_SUBTYPE_DEVICE = 'Hardware';
    const DSL_SUBTYPE_ADDON = 'Option';  
    
    const LTE_SUBTYPE_SUBSCRIPTION= 'Tariff';
    const LTE_SUBTYPE_PROMOTION = 'Promotion';
    const LTE_SUBTYPE_DEVICE = 'Hardware';
    const LTE_SUBTYPE_ADDON = 'Option';

    const MOBILE_DISCOUNT_STUDENT_SKU = "KSTUDRAB_1";
    const MOBILE_DISCOUNT_YOUNG_SKU = "KJUGENDR_1";
    const MOBILE_DISCOUNT_DISABLED_SKU = "KBEHINDR_1";

    const COMFORT_CONNECTION_DSL_SKU = "20082580_sub";
    const COMFORT_CONNECTION_CABLE_SKU = "TAC:B03";

    const INSTALL_INSTRUCTIONS_CABLE_SKU = "6P0:DG1";

    // The two skus that require serial number input
    const OWN_RECEIVER_SKU = "RCOWN";
    const OWN_SMARTCARD_SKU = "SCOWN";

    // todo add real products sku if these producuts should be hardcoded also in the future; else remove these and add the functionality that will create them automatically
    const MIGRATE2CABLE = "migrateToCable";
    const MIGRATE2DSL = "migrateToDsl";
    const CABLE_INSTALLATION_TAX = "CABLE_TAX";

    public static function getOwnReceiverId()
    {
        return Mage::getModel('catalog/product')->getResource()->getIdBySku(self::OWN_RECEIVER_SKU);
    }

    public static function getOwnSmartcardId()
    {
        return Mage::getModel('catalog/product')->getResource()->getIdBySku(self::OWN_SMARTCARD_SKU);
    }

    public static $unknown = [
        self::SUBTYPE_CHECKOUTOPTION,
        self::SUBTYPE_HIDDEN,
        self::SUBTYPE_ADDITIONAL_SERVICES,
        self::SUBTYPE_FOOTNOTE
    ];

    public static $devices = [
        self::MOBILE_SUBTYPE_DEVICE,
        self::SUBTYPE_DEVICE,
    ];

    public static $subscriptions = [
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
    ];

    public static $accessories = [
        self::SUBTYPE_ACCESSORY,
    ];

    public static $options = [
        self::MOBILE_SUBTYPE_ADDON,
        self::DSL_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
    ];

    public static $promotions = [
        self::MOBILE_SUBTYPE_PROMOTION,
        self::SUBTYPE_GOODY,
        self::DSL_SUBTYPE_PROMOTION,
    ];

    public static $fees = [
        self::SUBTYPE_FEE,
    ];

    // todo the mobile prepaid special discounts have maf prices => but the subtype will not add them to this array;
    public static $hasMafPrices = [
        self::MOBILE_SUBTYPE_ADDON,
        self::DSL_SUBTYPE_ADDON,
        self::SUBTYPE_ADDON,
        self::SUBTYPE_GOODY,
        self::MOBILE_SUBTYPE_PROMOTION,
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
    ];

    public static $hasOTCPrices = [
        self::MOBILE_SUBTYPE_DEVICE,
        self::SUBTYPE_DEVICE,
        self::MOBILE_SUBTYPE_SUBSCRIPTION,
        self::SUBTYPE_SUBSCRIPTION,
        self::MOBILE_PREPAID_SUBSCRIPTION,
        self::SUBTYPE_FEE,
    ];

    public static function getMigrationCharges()
    {
        return [
            self::MIGRATE2CABLE,
            self::MIGRATE2DSL,
            self::CABLE_INSTALLATION_TAX,
        ];
    }

    public static function getSections($type = null)
    {
        $builtSections = [];
        $sections = [
            'mobile' => [
                strtolower(static::TYPE_MOBILE),
                strtolower(static::TYPE_PREPAID),
            ],
            'cable' => [
                strtolower(static::TYPE_CABLE_INTERNET_PHONE),
                strtolower(static::TYPE_CABLE_TV),
                strtolower(static::TYPE_CABLE_INDEPENDENT),
                strtolower(static::TYPE_PREPAID),
            ],
            'fixed' => [
                strtolower(static::TYPE_FIXED_DSL),
            ],
            'lte' => [
                strtolower(static::TYPE_LTE)
            ]
        ];

        if ($type = strtolower($type)) {
            if (empty($sections[$type])) {
                throw new Exception("Invalid section provided for fetching package types.");
            }

            foreach ($sections[$type] as $section) {
                $builtSections[$section] = $section;
            }
        } else {
            foreach ($sections as $sectionTypes) {
                foreach ($sectionTypes as $section) {
                    $builtSections[$section] = $section;
                }
            }
        }

        return $builtSections;
    }

    public static function getCablePackages()
    {
        return [
            strtolower(self::TYPE_CABLE_TV),
            strtolower(self::TYPE_CABLE_INTERNET_PHONE),
            strtolower(self::TYPE_CABLE_INDEPENDENT),
        ];
    }

    public static function getMobilePackages()
    {
        return [
            strtolower(self::TYPE_MOBILE),
            strtolower(self::TYPE_PREPAID),
        ];
    }

    public static function getFixedPackages()
    {
        return [
            strtolower(self::TYPE_FIXED_DSL),
            strtolower(self::TYPE_LTE),
        ];
    }

    public static function getLtePackages()
    {
        return [
            strtolower(self::TYPE_LTE),
        ];
    }

    public static function getDslPackages()
    {
        return [
            strtolower(self::TYPE_FIXED_DSL),
        ];
    }

    public static function getPackageTypes() {
        return [
            strtolower(self::TYPE_CABLE_TV),
            strtolower(self::TYPE_FIXED_DSL),
            strtolower(self::TYPE_LTE),
            strtolower(self::TYPE_CABLE_INTERNET_PHONE),
            strtolower(self::TYPE_CABLE_INDEPENDENT),
            strtolower(self::TYPE_PREPAID),
            strtolower(self::TYPE_MOBILE),
        ];
    }
}
