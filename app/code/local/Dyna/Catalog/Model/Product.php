<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Product
 */
class Dyna_Catalog_Model_Product extends Omnius_Catalog_Model_Product
{
    CONST CATALOG_PACKAGE_TYPE_ATTR = 'package_type';
    CONST CATALOG_PACKAGE_SUBTYPE_ATTR = 'package_subtype';

    public static $sections = [
        Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION],
        Dyna_Catalog_Model_Type::SUBTYPE_DEVICE => [Dyna_Catalog_Model_Type::SUBTYPE_DEVICE],
        Dyna_Catalog_Model_Type::SUBTYPE_ADDON => [Dyna_Catalog_Model_Type::SUBTYPE_ADDON],
        Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY => [Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY],

        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION],
        Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION => [Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION],

        Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION => [Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION],
    ];

    /** Hardcoded list of skus that represent the mobile postpaid addons */
    public static $mobileDiscounts = [
        'student' => Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_STUDENT_SKU,
        'disabled' => Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_DISABLED_SKU,
        'young' => Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_YOUNG_SKU,
    ];

    /**
     * @param bool $ids
     * @return array Returns an array with the product ids of the special mobile discount products
     */
    public static function getMobileDiscountsIds($ids = true)
    {
        $discounts = self::$mobileDiscounts;
        if (!$ids) {
            return $discounts;
        }

        $discounts = [];
        foreach (self::$mobileDiscounts as $sku) {
            $discounts[] = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        }

        return $discounts;
    }

    /**
     * Get regular maf price or maf price if exists
     */
    public function getMaf()
    {
        $attrSet = $this->getAttributeSet();
        // Special case for Cable products
        if ($attrSet == 'KD_Cable_Products') {
            $mafPrice = $this->getData('price_repeated');

            if ($mafPrice) {
                $priceParts = explode(':', $mafPrice);
                $price = array_shift($priceParts);

                return $price = str_replace(',', '.', $price) ?: 0;
            }
        }

        if (($price = $this->_getData('maf')) !== null) {
            return $price;
        }

        return $this->getPromotionalMaf() ?: ($this->getRegularMaf() ?: false);
    }

    /**
     * todo: check if this returns the correct contract duration for each product
     * @return mixed
     */
    public function getContractDuration()
    {
        return $this->getData('contract_duration');
    }


    public function isDevice()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$devices as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function isMobileSpecialDiscount()
    {
        $is = false;
        foreach (self::$mobileDiscounts as $sku) {
            if (!$is && $sku == $this->getSku()) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function hasMafPrice()
    {
        $has = false;
        foreach (Dyna_Catalog_Model_Type::$hasMafPrices as $subType) {
            if (!$has && in_array($subType, $this->getType())) {
                $has = true;
                break;
            }
        }
        if(!$has && $this->isMobileSpecialDiscount()) {
            $has = true;
        }

        return $has;
    }

    public function hasOTCPrice()
    {
        $has = false;
        foreach (Dyna_Catalog_Model_Type::$hasOTCPrices as $subType) {
            if (!$has && in_array($subType, $this->getType())) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    public function isPromotion()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$promotions as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function isFee()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$fees as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function isSubscription()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$subscriptions as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function isAccessory()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$accessories as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    public function isOption()
    {
        $is = false;
        foreach (Dyna_Catalog_Model_Type::$options as $subType) {
            if (!$is && in_array($subType, $this->getType())) {
                $is = true;
                break;
            }
        }

        return $is;
    }

    /**
     * Checks whether or not current product is migration charge
     * @return bool
     */
    public function isMigrationCharge()
    {
        return in_array($this->getSku(), Dyna_Catalog_Model_Type::getMigrationCharges());
    }

    /**
     * Retrieve assigned category Ids
     * @param int $websiteId
     * @param $subtype string Subtype of the product
     * @return array
     */
    public function getStockCall($websiteId, $subtype, $axiStoreId)
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $product = [];
        $products = [];
        if (is_array($subtype)) {
            foreach ($subtype as $identifier) {
                $productStock = Mage::helper('omnius_catalog')->getProductForStock($websiteId, $identifier, $cache);
                if ($productStock && is_array($productStock)) {
                    $product = array_merge($product, $productStock);
                }
                $productsStock = Mage::helper('omnius_catalog')->getProductsForStock($websiteId, $identifier, $cache, $product);
                if ($productsStock && is_array($productsStock)) {
                    $products = array_merge($products, $productsStock);
                }
            }
        } else {
            $product = Mage::helper('omnius_catalog')->getProductForStock($websiteId, $subtype, $cache);
            $products = Mage::helper('omnius_catalog')->getProductsForStock($websiteId, $subtype, $cache, $product);
        }
        $result = [];

        foreach ($products as $id => $item) {
            $result = Mage::helper('dyna_catalog')->buildStockItem($result, $id, $item, []);
        }

        return $result;
    }

    public function isOwnReceiver()
    {
        return (bool)($this->getSku() == Dyna_Catalog_Model_Type::OWN_RECEIVER_SKU);
    }

    public function isOwnSmartcard()
    {
        return (bool)($this->getSku() == Dyna_Catalog_Model_Type::OWN_SMARTCARD_SKU);
    }


    public function hasTagByName($tagName)
    {
        $tags = $this->getTags();

        if (! empty($tags)) {
            $tagsArray = explode(',', $tags);

            foreach ($tagsArray as $tag) {
                if ($tag == $tagName) {
                    return true;
                }
            }
        }

        return false;
    }
}
