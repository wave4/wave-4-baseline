<?php

class Dyna_Catalog_Adminhtml_DeliveryController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("catalog")->_addBreadcrumb(Mage::helper("adminhtml")->__("Delivery Options"), Mage::helper("adminhtml")->__("Delivery Options"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Delivery"));
        $this->_title($this->__("Manage Delivery"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dyna_catalog/delivery")->load($id);

        if ($model->getId()) {
            Mage::register("catalog_delivery_option", $model);

            $this->_title($this->__("Delivery"));
            $this->_title($this->__("Edit delivery option"));

            $this->_initAction();
            $this->renderLayout();
        } else {
            $this->_initAction();
            $this->renderLayout();
        }
    }

    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();
        $saveData = array();

        if ($postData) {
            $model = Mage::getModel("dyna_catalog/delivery");

            if ($this->getRequest()->getParam("id")) {
                $existingOption = $model->load($this->getRequest()->getParam("id"));
                $existingOption->addData($postData);

                $existingOption->save();
            } else {
                $model->setData($postData);
                $model->save();
            }
        }

        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id")) {
            $option = Mage::getModel("dyna_catalog/delivery")->load($this->getRequest()->getParam("id"));
            $option->delete();

            Mage::unregister("catalog_delivery_option");
        }

        $this->_redirect("*/*/");
    }
}
