<?php

/**
 * Cable product General attributes
 */

/* @var $installer Dyna_Cable_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$groupName = 'General';
$sortOrder = 10;

$createAttributeSets = [];

$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection') ->load();
foreach ($attributeSetCollection as $id=>$attributeSet) {
    $name = $attributeSet->getAttributeSetName();
    $createAttributeSets[$name]['groups'][$groupName][] = $name;
}

// Create attributes
$attributes = [
    'hide_in_config' => [
        'label'     => 'Hide in Configurator',
        'type'      => 'int',
        'input'     => 'boolean',
        'visible'   => true,
        'required'  => true,
        'default'   => 0,
        'position'  => 81,
        'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ],
    'hide_in_shopping_cart' => [
        'label'     => 'Hide in Shopping Cart',
        'type'      => 'int',
        'input'     => 'boolean',
        'visible'   => true,
        'required'  => true,
        'default'   => 0,
        'position'  => 82,
        'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ],
    'hide_in_order_overview' => [
        'label'     => 'Hide in Overview Page',
        'type'      => 'int',
        'input'     => 'boolean',
        'visible'   => true,
        'required'  => true,
        'default'   => 0,
        'position'  => 83,
        'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ],
];

// Create the attributes
foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (! $attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
    }

    foreach ($attributeSetCollection as $id=>$attributeSet) {
        $name = $attributeSet->getAttributeSetName();
        $createAttributeSets[$name]['groups'][$groupName][] = $code;
    }
}

foreach ($attributes as $code => $options) {
    $attributeId = $installer->getAttributeId($entityTypeId, $code);
    if (! $attributeId) {
        $options['user_defined'] = 1;
        $installer->addAttribute($entityTypeId, $code, $options);
        $attributeId = $installer->getAttributeId($entityTypeId, $code);
    }

    foreach ($attributeSetCollection as $id=>$attributeSet) {
        $name = $attributeSet->getAttributeSetName();
        $attrGroupId = $this->getAttributeGroup($entityTypeId, $id, $groupName, 'attribute_group_id');

        $this->addAttributeToSet($entityTypeId, $id, $attrGroupId, $attributeId, $sortOrder);
    }
}

unset($attributes, $createAttributeSets, $attributeSetCollection);

$installer->endSetup();
