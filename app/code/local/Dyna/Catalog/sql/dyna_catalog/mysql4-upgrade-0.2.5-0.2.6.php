<?php
$installer = $this;

$installer->startSetup();

$checkoutFieldsTable = new Varien_Db_Ddl_Table();
$checkoutFieldsTable->setName("catalog_delivery_options");

$checkoutFieldsTable
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, 10, [
        "unsigned" => true,
        "primary" => true,
        "auto_increment" => true,
    ], "Primary key for delivery options table")
    ->addColumn('name',Varien_Db_Ddl_Table::TYPE_TEXT, 200)
    ->addColumn('package_type',Varien_Db_Ddl_Table::TYPE_TEXT, 100)
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, [12,4])
    ->addColumn('delivery_type',Varien_Db_Ddl_Table::TYPE_TEXT, 200)
    ->addColumn('is_new_customer', Varien_Db_Ddl_Table::TYPE_BOOLEAN)
;

$this->getConnection()->createTable($checkoutFieldsTable);

$installer->endSetup();
