<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form
 */
class Dyna_MixMatch_Block_Adminhtml_Rules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("dyna_mixmatch_form", array("legend" => Mage::helper("dyna_mixmatch")->__("Item information")));

        $fieldset->addField("source_sku", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Source SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "source_sku",
        ));

        $fieldset->addField("target_sku", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Target SKU"),
            "class" => "required-entry",
            "required" => true,
            "name" => "target_sku",
        ));


        $fieldset->addField("subscriber_segment", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Subscriber segment"),
            "class" => "",
            "required" => false,
            "name" => "subscriber_segment",
        ));

        $fieldset->addField("void", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("VOID"),
            "class" => "",
            "required" => false,
            "name" => "void",
        ));

        $fieldset->addField("price", "text", array(
            "label" => Mage::helper("dyna_mixmatch")->__("Price"),
            "required" => false,
            "name" => "price",
        ));

        $fieldset->addField('website_id', 'multiselect', array(
            'label' => Mage::helper('dyna_mixmatch')->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
        ));

        if (Mage::getSingleton("adminhtml/session")->getDynaMixMatchData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getDynaMixMatchData());
            Mage::getSingleton("adminhtml/session")->setDynaMixMatchData(null);
        } elseif (Mage::registry("dyna_mixmatch_data")) {
            $form->setValues(Mage::registry("dyna_mixmatch_data")->getData());
        }

        return parent::_prepareForm();
    }
}
