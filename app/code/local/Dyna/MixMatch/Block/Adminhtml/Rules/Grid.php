<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * MixMatch attributes grid
 *
 * @category   Dyna
 * @package    Dyna_MixMatch
 */
class Dyna_MixMatch_Block_Adminhtml_Rules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_attributeSets = array();
    protected $_rulesArray = array();

    public function __construct()
    {
        //TODO these options must be dynamic
        $this->_attributeSets = array('mobile');

        parent::__construct();
        $this->setId('mixmatchRulesGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('mixmatch_rules_filter');

    }

    public function getExcelFile($sheetName = '', $websiteId = null)
    {
        $this->_prepareExistingRules();

        $objPHPExcel = new PHPExcel();

        $index = 0;
        foreach ($this->_attributeSets as $set) {
            $this->_buildWorksheet($objPHPExcel, $set, $index, $websiteId);
            $index++;
        }

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . $name . '.xlsx';

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($file);

        return array(
            'type' => 'filename',
            'value' => $file,
            'rm' => true,
            // can delete file after use
        );
    }

    protected function _prepareExistingRules()
    {
        $this->_prepareExportCollection();

        $collection = $this->getCollection();

        $this->_rulesArray = array();

        foreach ($collection as $item) {
            $this->_rulesArray[$item->getTargetSku()][$item->getSourceSku()][$item->getDeviceSubscriptionSku()] = $item->getPrice();
        }
    }

    /**
     * Prepare grid collection object
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareExportCollection()
    {
        $this->_isExport = true;
        $this->_prepareColumns();

        $collection = Mage::getResourceModel('omnius_mixmatch/price_collection');
        $this->setCollection($collection);

        if ($this->getCollection()) {

            $columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
            $dir = $this->getParam($this->getVarNameDir(), $this->_defaultDir);
            $filter = $this->getParam($this->getVarNameFilter(), null);

            if (is_null($filter)) {
                $filter = $this->_defaultFilter;
            }

            if (is_string($filter)) {
                $data = $this->helper('adminhtml')->prepareFilterString($filter);
                $this->_setFilterValues($data);
            } else {
                if ($filter && is_array($filter)) {
                    $this->_setFilterValues($filter);
                } else {
                    if (0 !== sizeof($this->_defaultFilter)) {
                        $this->_setFilterValues($this->_defaultFilter);
                    }
                }
            }

            if (isset($this->_columns[$columnId]) && $this->_columns[$columnId]->getIndex()) {
                $dir = (strtolower($dir) == 'desc') ? 'desc' : 'asc';
                $this->_columns[$columnId]->setDir($dir);
                $this->_setCollectionOrder($this->_columns[$columnId]);
            }
        }

        return $this;
    }

    /**
     * Prepare product attributes grid columns
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('ID'),
                'width' => '10px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('source_sku',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Source SKU'),
                'index' => 'source_sku',
            ));
        $this->addColumn('source_category',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Source Category'),
                'index' => 'source_category',
            ));
        $this->addColumn('target_sku',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Target SKU'),
                'index' => 'target_sku',
            ));
        $this->addColumn('target_category',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Target Category'),
                'index' => 'target_category',
            ));
        $this->addColumn('subscriber_segment',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Subscriber segment'),
                'index' => 'subscriber_segment',
            ));
        $this->addColumn('void',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('VOID'),
                'index' => 'void',
            ));
        $this->addColumn('uploaded_at',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Uploaded at'),
                'type' => 'datetime',
                'index' => 'uploaded_at',
            ));

        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('OT Price'),
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
                'renderer'  => 'dyna_mixmatch/adminhtml_rules_renderer_price',
            ));
        $this->addColumn('maf',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Maf'),
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'maf',
                'renderer'  => 'dyna_mixmatch/adminhtml_rules_renderer_maf',
            ));
        $this->addColumn('effective_date',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Effective date'),
                'type' => 'date',
                'index' => 'effective_date',
            ));
        $this->addColumn('expiration_date',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Expiration date'),
                'type' => 'date',
                'index' => 'expiration_date',
            ));
        $this->addColumn('stream',
            array(
                'header' => Mage::helper('dyna_mixmatch')->__('Stream'),
                'index' => 'stream',
            ));
        $this->addColumn('website_id',
            array(
                'header' => Mage::helper('catalog')->__('Website'),
                'index' => 'website_id',
                'type' => 'options',
                'options' => Mage::getModel('core/website')->getCollection()->toOptionHash(),
            ));

        return $this;
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);

        return Mage::app()->getStore($storeId);
    }

    /**
     * @param PHPExcel $objPHPExcel
     * @param $title
     * @param int $index
     * @param $websiteId
     * @return mixed
     */
    protected function _buildWorksheet($objPHPExcel, $title, $index, $websiteId)
    {
        if ($index == 0) {
            $ws = $objPHPExcel->getActiveSheet();
        } else {
            $ws = $objPHPExcel->createSheet();
        }
        $ws->setTitle($title);
        // Also retrieve disabled products(which have no price) for mixmatch
        Mage::register('mix_match_with_disabled_products', true);
        $devices = Mage::getModel('dyna_configurator/' . $title)->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, $websiteId);
        $subscriptions = Mage::getModel('dyna_configurator/' . $title)->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $websiteId);
        $deviceSubscriptions = [];
        $colIndex = 1;
        foreach ($subscriptions as $subscription) {
            $this->setCellValues($subscription, $deviceSubscriptions, $ws, $colIndex, $devices);
        }
        Mage::unregister('mix_match_with_disabled_products');

        return $ws;
    }

    /**
     * @param $subscription
     * @param $deviceSubscriptions
     * @param $ws
     * @param $colIndex
     * @param $devices
     */
    protected function setCellValues($subscription, $deviceSubscriptions, $ws, &$colIndex, $devices)
    {
        if ($subscription->getAikido()) {
            foreach ($deviceSubscriptions as $deviceSubscription) {
                $ws->setCellValueByColumnAndRow($colIndex, 1, $subscription->getSku());
                $ws->setCellValueByColumnAndRow($colIndex, 2, $deviceSubscription->getSku());

                $rowIndex = 3;
                foreach ($devices as $device) {
                    if ($colIndex == 1) {
                        $ws->setCellValueByColumnAndRow(0, $rowIndex, $device->getSku());
                    }

                    $value = (isset($this->_rulesArray[$device->getSku()][$subscription->getSku()][$deviceSubscription->getSku()]) && $this->_rulesArray[$device->getSku()][$subscription->getSku()][$deviceSubscription->getSku()]) ? $this->_rulesArray[$device->getSku()][$subscription->getSku()][$deviceSubscription->getSku()] : null;
                    $ws->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);

                    $rowIndex++;
                }
                $colIndex++;
            }
        } else {
            $ws->setCellValueByColumnAndRow($colIndex, 1, $subscription->getSku());
            $ws->setCellValueByColumnAndRow($colIndex, 2, '');

            $rowIndex = 3;
            foreach ($devices as $device) {
                if ($colIndex == 1) {
                    $ws->setCellValueByColumnAndRow(0, $rowIndex, $device->getSku());
                }

                $value = (isset($this->_rulesArray[$device->getSku()][$subscription->getSku()]['']) && $this->_rulesArray[$device->getSku()][$subscription->getSku()]['']) ? $this->_rulesArray[$device->getSku()][$subscription->getSku()][''] : null;
                $ws->setCellValueByColumnAndRow($colIndex, $rowIndex, $value);

                $rowIndex++;
            }
            $colIndex++;
        }
    }

    /**
     * Prepare product attributes grid collection object
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('omnius_mixmatch/price_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _exportCustomCollection(Varien_Data_Collection $collection, $objPHPExcel)
    {
        $worksheet = $objPHPExcel->setActiveSheetIndex(0);

        // build data matrix with existing rules
        foreach ($collection as $item) {
            $data[$item->getTargetSku()][$item->getSourceSku()] = $item->getPrice();
            $col_headers[] = $item->getSourceSku();
            $row_headers[] = $item->getTargetSku();
        }
        $col_headers = array_unique($col_headers);
        $row_headers = array_unique($row_headers);

        // write header
        $rowIndex = 1;
        foreach ($col_headers as $key => $subscription) {
            $worksheet->setCellValueByColumnAndRow($key + 1, $rowIndex, $subscription);
        }
        $rowIndex++;


        foreach ($row_headers as $device) {
            // write row head
            $columnIndex = 0;
            $worksheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $device);
            $columnIndex++;

            // fill in content
            foreach ($col_headers as $subscription) {
                $price = (isset($data[$device]) && isset($data[$device][$subscription]) && $data[$device][$subscription]) ? (float) $data[$device][$subscription] : null;

                $worksheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $price);
                $columnIndex++;
            }
            $rowIndex++;
        }

        return $objPHPExcel;
    }
}
