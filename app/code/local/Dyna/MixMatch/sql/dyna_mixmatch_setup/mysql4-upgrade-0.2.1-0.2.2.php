<?php
/**
 * Rename columns:
 * subscription_sku => source_sku
 * device_sku => target_sku
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
/** @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$table = $this->getTable('catalog_mixmatch');

if ($connection->tableColumnExists($table, 'subscription_sku')) {
    $connection->changeColumn($table, 'subscription_sku', 'source_sku', 'varchar(64) null');
}
if ($connection->tableColumnExists($table, 'device_sku')) {
    $connection->changeColumn($table, 'device_sku', 'target_sku', 'varchar(64) null');
}

$installer->endSetup();
