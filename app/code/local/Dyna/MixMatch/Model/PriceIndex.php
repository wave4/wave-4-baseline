<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Class  Dyna_MixMatch_Model_PriceIndex */
class Dyna_MixMatch_Model_PriceIndex extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("dyna_mixmatch/priceIndex");
    }

    /**
     * Return data of key for sql queries
     *
     * @param $key
     * @return mixed|string
     */
    public function getDataForSql($key)
    {
        if (parent::getData($key) == null) {
            return new Zend_Db_Expr('NULL');
        } else {
            return '"' . $this->getData($key) . '"';
        }
    }
}
