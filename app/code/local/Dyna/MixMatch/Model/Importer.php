<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Model_Importer
 */
class Dyna_MixMatch_Model_Importer extends Mage_Core_Model_Abstract
{
    const CONFIG_PATH = "dataimport_configuration/mixmatch/file_path";
    /** @var array */
    protected $_products = [];

    /**
     * @param string $path
     * @param  array $websiteIds
     * @return bool
     */
    public function importFile($path, $websiteIds)
    {
        $contents = file_get_contents($path);

        if (!$contents) {
            $this->_log('[ERROR] Cannot get content');

            $return = false;
        } else {
            //BOM deleting for UTF files
            if (ord($contents[0]) == 0xEF && ord($contents[1]) == 0xBB && ord($contents[2]) == 0xBF) {
                $contents = substr($contents, 3);
                if (!$contents) {
                    $this->_log('[ERROR] Cannot get content substring');

                    $return = false;
                }
                if (!file_put_contents($path, $contents)) {
                    $this->_log('[ERROR] Cannot delete bom ');

                    $return = false;
                }
            }
        }

        if (isset($return)) {
            return $return;
        }

        unset($contents);
        $startDate = gmdate('Y-m-d h:i:s');
        $userName = null;
        if (Mage::getSingleton('admin/session')->getUser()) {
            $userName = Mage::getSingleton('admin/session')->getUser()->getUsername();
        }
        try {
            /**
             * @var $objReader PHPExcel_Reader_Abstract
             * @var $objPHPExcel PHPExcel
             */
            $objReader = PHPExcel_IOFactory::createReaderForFile($path);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($path);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $excelValues = $this->getExcelNameValues($objWorksheet);

            foreach ($websiteIds as $websiteId) {
                if (empty($websiteId)) {
                    $this->importFileForWebsite($path, null, $excelValues, $startDate, $userName);
                } else {
                    $this->importFileForWebsite($path, $websiteId, $excelValues, $startDate, $userName);
                }
            }
            $objPHPExcel->disconnectWorksheets();
            $objPHPExcel->garbageCollect();
        } catch (PHPExcel_Reader_Exception $e) {
            $this->_log('[ERROR] Exception - Excel reader failed: ' . $e->getMessage());

            $return = false;
        } catch (Exception $e) {
            $this->_log('[ERROR] Exception - Failed importing: ' . $e->getMessage());

            $return = false;
        }

        return isset($return) ? $return : true;
    }

    protected function getExcelNameValues($objWorksheet)
    {
        /** @var PHPExcel_Worksheet $objWorksheet */
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
        $headingsArray = $headingsArray[1];
        $headingsArray = $this->mapOtherColumnsToHeadingsArray($headingsArray);

        $r = -1;
        $namedDataArray = [];
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
            ++$r;
            foreach ($headingsArray as $columnKey => $columnHeading) {
                $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
            }
        }

        return $namedDataArray;
    }

    /*
     * Just in case columns headers naming differs from the one that is needed by model
     */
    protected function mapOtherColumnsToHeadingsArray($headingArray)
    {
        return $headingArray;
    }

    /**
     * @param $path
     * @param $websiteId
     * @param $excelValues
     * @param $startDate
     * @param $userName
     * @throws PHPExcel_Exception
     * @throws bool
     */
    protected function importFileForWebsite($path, $websiteId, $excelValues, $startDate, $userName)
    {
        $counter = 0;
        $deviceSubscription = new Varien_Object();
        $cachedWebsitesIds = [];

        foreach ($excelValues as $valuesArray) {
            // skip empty row
            if (!array_filter($valuesArray)) {
                continue;
            }
            $ws = new Varien_Object();

            $ws->setData($valuesArray);

            /** @var Mage_Core_Model_Resource_Transaction $transaction */
            $transaction = Mage::getResourceModel('core/transaction');

            $sourceSku = trim($ws->getSourceSku());
            $targetSku = trim($ws->getTargetSku());
            $targetCategory = trim($ws->getTargetCategory());
            $sourceCategory = trim($ws->getSourceCategory());
            $subscriberSegment = $ws->getSubscriberSegment();
            if ($ws->getEffectiveDate()) {
                $effectiveDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getEffectiveDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $effectiveDate = date("Y-m-d", strtotime($effectiveDate));
            } else {
                $effectiveDate = "";
            }
            if ($ws->getExpirationDate()) {
                $expirationDate = PHPExcel_Style_NumberFormat::toFormattedString($ws->getExpirationDate(), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                $expirationDate = date("Y-m-d", strtotime($expirationDate));
            } else {
                $expirationDate = "";
            }
            $stream = $ws->getStream();
            $void = $ws->getVoid();
            $maf = $ws->getMaf();
            $price = (is_numeric($ws->getPrice()) && (float)$ws->getPrice() >= 0) ? (float)$ws->getPrice() : null;

            // if no sku on subscription column, proceed to next column
            if (!$sourceSku && !$sourceCategory) {
                $this->_log('Skipping row ' . $counter . ' because there is no source SKU and no source Category on this row');
                continue;
            }
            // if no sku on devices row, proceed to next row
            if (!$targetSku && !$targetCategory) {
                $this->_log('Skipping row ' . $counter . ' because there is no target SKU and no target Category on this row');
                continue;
            }

            if (!$websiteId && $ws['channel']) {
                $channel = 0;
                if (!empty($cachedWebsitesIds[$ws['channel']])) {
                    $channel = $cachedWebsitesIds[$ws['channel']]['website_id'];

                    /** Clear all website data at first load for the current stream and mark it as emptied */
                    if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                        $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                    }
                } else {
                    /** Load website and cache it's id */
                    $channel = Mage::getModel('core/website')->load(strtolower($ws['channel']), 'code')->getId();
                    $cachedWebsitesIds[$ws['channel']]['website_id'] = $channel;

                    /** Clear all website data at first load for the current stream and mark it as emptied */
                    if (empty($cachedWebsitesIds[$ws['channel']][$ws['stream']])) {
                        $cachedWebsitesIds[$ws['channel']][$ws['stream']] = $this->clearMixmatchData($channel, $ws['stream']);
                    }
                }

                if ($channel) {
                    $this->importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $void, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $channel, $counter, $effectiveDate, $expirationDate, $stream);
                } else {
                    $this->_log('Skipping row' . $counter . ' because the channel column contains an invalid website code');
                }
            } else {
                if (empty($cachedWebsitesIds[$websiteId])) {
                    $cachedWebsitesIds[$websiteId] = $websiteId;
                }

                /** Clear all website data at first load for the current stream and mark it as emptied */
                if (empty($cachedWebsitesIds[$websiteId][$ws['stream']])) {
                    $cachedWebsitesIds[$websiteId][$ws['stream']] = $this->clearMixmatchData($websiteId, $ws['stream']);
                }
                $this->importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $void, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $websiteId, $counter, $effectiveDate, $expirationDate, $stream);
            }

            try {
                $transaction->save();
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
            }
            unset($transaction);

            $counter++;
        }

        $this->setImportedFileName($path, $websiteId);
    }

    protected function clearMixmatchData($channel, $stream)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tableName = Mage::getResourceSingleton('omnius_mixmatch/price')->getMainTable();
        $connection->delete($tableName, ['website_id = ?' => (int)$channel, 'stream = ?' => $stream]);

        return "rules removed for this website and this channel";
    }

    /**
     * @param $msg
     */
    private function _log($msg)
    {
        Mage::log($msg, null, 'mixmatch_import_' . date('Y-m-d') . '.log');
    }

    protected function importProduct($targetSku, $sourceSku, $targetCategory, $sourceCategory, $subscriberSegment, $void, $deviceSubscription, $transaction, $price, $maf, $startDate, $userName, $websiteId, $counter, $effectiveDate, $expirationDate, $stream)
    {
        try {
            if (!empty($targetSku) && empty($targetCategory) && !$this->getProductIdBySku($targetSku)) {
                $this->_log(sprintf('Target product is not found: %s', $targetSku));

                return;
            }
            if (!empty($sourceSku) && empty($sourceCategory) && !$this->getProductIdBySku($sourceSku)) {
                $this->_log(sprintf('Source product is not found: %s', $sourceSku));

                return;
            }

            if (trim($deviceSubscription->getValue())
                && $this->getProductIdBySku($deviceSubscription->getValue())
            ) {
                $transaction->addObject(Mage::getModel('omnius_mixmatch/price')
                    ->setTargetSku($targetSku)
                    ->setSourceSku($sourceSku)
                    ->setDeviceSubscriptionSku($deviceSubscription->getValue())
                    ->setPrice($price)
                    ->setUploadedAt($startDate)
                    ->setUploadedBy($userName)
                    ->setWebsiteId($websiteId));
                $this->_log('Setting target ' . $targetSku . ' with source ' . $sourceSku . ' to price '
                    . $price . ' for website ' . $websiteId . ' using row ' . $counter);
                $this->_log(
                    sprintf(
                        'Setting device %s with subscription %s and subscription device %s to price %s for website %s using row %d',
                        $targetSku,
                        $sourceSku,
                        $deviceSubscription->getValue(),
                        $price,
                        $websiteId,
                        $counter
                    )
                );
            } else {
                $transaction->addObject(Mage::getModel('omnius_mixmatch/price')
                    ->setTargetSku($targetSku)
                    ->setSourceSku($sourceSku)
                    ->setTargetCategory($targetCategory)
                    ->setSourceCategory($sourceCategory)
                    ->setVoid($void)
                    ->setSubscriberSegment($subscriberSegment)
                    ->setPrice($price)
                    ->setMaf($maf)
                    ->setUploadedAt($startDate)
                    ->setUploadedBy($userName)
                    ->setWebsiteId($websiteId)
                    ->setEffectiveDate($effectiveDate)
                    ->setExpirationDate($expirationDate)
                    ->setStream($stream));

                $this->_log('Setting targetSku <' . $targetSku . '>, sourceSku <' . $sourceSku . '>, targetCategory <' . $targetCategory . '>, sourceCategory <' . $sourceCategory .
                    '>, subscriberSegment ' . $subscriberSegment . ', void ' . $void . ' to price ' . $price . ' for website ' . $websiteId . ' using row ' . $counter);
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
        }
    }

    /**
     * @param $sku
     * @return int
     */
    protected function getProductIdBySku($sku)
    {
        if (!isset($this->_products[$sku])) {
            return $this->_products[$sku] = Mage::getModel("catalog/product")->getIdBySku($sku);
        }

        return $this->_products[$sku];
    }

    protected function setImportedFileName($path, $websiteId)
    {
        $config = new Mage_Core_Model_Config();
        $config->saveConfig(self::CONFIG_PATH, $path, 'stores', Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId());
        Mage::getConfig()->cleanCache();
    }

    /**
     * @param array $skus
     * @return array
     */
    protected function getProductCollection(array $skus = [])
    {
        $collection = Mage::getResourceSingleton('catalog/product_collection')
            ->addAttributeToFilter('sku', ['in' => array_unique($skus)])
            ->load();
        $ids = [];
        foreach ($collection->getItems() as $item) {
            $ids[$item->getSku()] = $item->getId();
        }

        return $ids;
    }
}
