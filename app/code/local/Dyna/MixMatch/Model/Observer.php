<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Dyna_MixMatch_Model_Observer extends Mage_Core_Model_Abstract
{
    const BATCH_SIZE = 500;

    /**
     * @param $observer
     */
    public function calculateItemPrice($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // Ensure we have the parent item, if it has one
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);

        // if item is device, search for subscription and update price
        if (Mage::helper('dyna_catalog')->is([Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE], $item->getProduct())) {
            $this->_updateDevicePrice($item);
        }

        // if item is a subscription, search for devices and update prices
        if (Mage::helper('dyna_catalog')->is([Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION], $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }
    }

    /**
     * Recalculates device prices when a subscription is removed from the cart
     *
     * @param $observer
     */
    public function recalculatePrices($observer)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = $observer->getQuoteItem();

        // if item is a subscription, search for devices and update prices
        if (Mage::helper('dyna_catalog')->is([Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION], $item->getProduct())) {
            $this->_updateDevicesPrice($item);
        }
    }

    /**
     * Index MixMatches at the end with highest priority
     */
    public function indexMixMatches($observer)
    {
        /** @var Dyna_ProductMatchRule_Model_Indexer_Productmatch $productMatch */
        $productMatch = $observer->getProductMatch();
        $productIds = [];

        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $rows = $connection->fetchAll('SELECT distinct cp.entity_id,cp.sku FROM catalog_product_entity AS cp INNER JOIN dyna_mixmatch_flat AS cm ON cm.target_sku = cp.sku OR cm.source_sku=cp.sku');

        while (($row = array_pop($rows)) !== null) {
            $productIds[$row['sku']] = $row['entity_id'];
        }
        unset($row);
        unset($rows);

        $collection = Mage::getResourceModel('dyna_mixmatch/priceIndex_collection');
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();

        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection->getData() as $row) {
                if (isset($productIds[$row['target_sku']]) && isset($productIds[$row['source_sku']])) {
                    $productMatch->_addConditional($productIds[$row['target_sku']], $productIds[$row['source_sku']], $row['website_id']);
                }
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        unset($collection);
        unset($productIds);
        unset($rows);
    }

    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param $item
     * @return bool
     */
    protected function _updateDevicePrice($item)
    {
        $this->_updatePrice($item, $this->getPricing()->getDevicePrice($item));
    }

    /**
     * Overwrite the prices of all devices in cart based on the added subscription (item)
     * @param $item
     * @return bool
     */
    protected function _updateDevicesPrice($item)
    {
        $devices = $item->getQuote()->getPackageItems($item->getPackageId());
        foreach ($devices as $device) {
            // check if it is a device
            if (!Mage::helper('dyna_catalog')->is([Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE], $device->getProduct())) {
                continue;
                //skip items that are not devices
            }

            $this->_updatePrice($device, $this->getPricing()->getDevicePrice($device));
        }
    }

    /**
     * @param $item
     * @param $price
     */
    protected function _updatePrice($item, $price)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            //Enable super mode on the product.
            $item->getProduct()->setIsSuperMode(true);
            //Set the custom price
            $item->setCustomPrice($price)
                ->setOriginalCustomPrice($price)
                ->save();
        }
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('omnius_mixmatch/pricing');
    }

    public function addLastExportButtonInAdmin($observer)
    {
        /** @var Omnius_MixMatch_Block_Adminhtml_Rules $rulesForm */
        $rulesForm = $observer->getButtons();

        $rulesForm->addButton('lastexport', [
            'label' => Mage::helper('omnius_mixmatch')->__('Download last uploaded Mixmatch'),
            'onclick' => 'setLocation(\'' . $rulesForm->getLastExportUrl() . '\')',
            'class' => 'go',
        ]);

        /** @todo - export needs to be adjusted to VDFDE flow */
        $rulesForm->removeButton('export');
    }
}
