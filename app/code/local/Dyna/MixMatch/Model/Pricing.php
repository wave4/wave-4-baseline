<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/** Class  Dyna_MixMatch_Model_Pricing */
class Dyna_MixMatch_Model_Pricing extends Omnius_MixMatch_Model_Pricing
{

    /**
     * Overwrite the price of the added device (item) if there is a subscription in the cart
     * @param $item
     * @return bool
     */
    public function getDevicePrice(Mage_Sales_Model_Quote_Item $item)
    {

        $subscriptions = null;

        $siblings = $item->getQuote()->getPackageItems($item->getPackageId());

        foreach ($siblings as $sibling) {

            // check if there is a subscription
            if (!Mage::helper('dyna_catalog')->is([Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION],
                $sibling->getProduct())
            ) {
                continue;
            }

            $subscription = $sibling->getSku();
        }

        if (isset($subscription) && $subscription !== null) {
            $collection = $this->getMixMatchCollection($item->getSku(), $subscription, null);
            $mixmatch = $collection->getFirstItem();
        }

        if (isset($mixmatch) && $mixmatch->getPrice()) {
            $price = $mixmatch->getPrice();
        } else {
            if (!$item->getProduct()->getPurchasePrice()) {
                $price = null;
            } else {
                $price = $this->calculatePrice($item->getProduct()->getPurchasePrice());
            }
        }

        return $price;
    }

    /**
     * @param string $deviceSku
     * @param string $subscriptionSku
     * @param string $loanSku
     * @param int $websiteId
     * @return Omnius_MixMatch_Model_Resource_Price_Collection
     */
    public function getMixMatchCollection($deviceSku, $subscriptionSku, $loanSku, $websiteId = null)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        $key = serialize([
            __METHOD__,
            $deviceSku,
            $subscriptionSku,
            $websiteId
        ]);
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            /** @var Omnius_MixMatch_Model_Resource_Price_Collection $collection */
            $collection = Mage::getModel('omnius_mixmatch/price')->getCollection()
                ->addFieldToFilter('target_sku', $deviceSku)
                ->addFieldToFilter('source_sku', $subscriptionSku)
                ->addFieldToFilter('website_id', $websiteId);

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG],
                $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $devicePurchasePrice
     * @return float
     */
    public function calculatePrice($devicePurchasePrice, $taxRate = null)
    {
        $taxRate = $taxRate ?: Mage::helper('dyna_catalog')->getDefaultCountryTaxRate();

        return ((round(((((($devicePurchasePrice) * (1 + $taxRate)) - 4.99) * 2) / 10), 0) * 10) / 2) + 4.99;
    }
}
