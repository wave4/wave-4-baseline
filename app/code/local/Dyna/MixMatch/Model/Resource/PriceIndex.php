<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Model_Resource_PriceIndex
 */
class Dyna_MixMatch_Model_Resource_PriceIndex extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Model initialization
     */
    protected function _construct()
    {
        $this->_init('dyna_mixmatch/priceIndex', 'id');
    }
}
