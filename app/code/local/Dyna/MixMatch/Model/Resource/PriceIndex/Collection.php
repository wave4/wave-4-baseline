<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_MixMatch_Model_Resource_PriceIndex_Collection
 */
class Dyna_MixMatch_Model_Resource_PriceIndex_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Model initialization
     */
    protected function _construct()
    {
        $this->_init('dyna_mixmatch/priceIndex');
    }
}
