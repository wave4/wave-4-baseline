<?php

class Dyna_Package_Helper_Data extends Omnius_Package_Helper_Data
{
    /**
     * Checks whether or not the current quote contains a serviceability needed package
     * At this time, all packages that are not mobile, require serviceability check
     * @return bool
     */
    public function hasServiceAbilityRequiredPackages()
    {
        $hasAtLeastOne = false;

        /** @var Dyna_Checkout_Model_Sales_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /** @var Dyna_Package_Model_Package $package */
        foreach ($quote->getCartPackages() as $package) {
            if (!in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
                // Found one
                $hasAtLeastOne = true;
                break;
            }
        }

        return $hasAtLeastOne;
    }
}
