<?php

/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Package_Model_Package
 * @method string getType() Returns the package type (e.g. mobile, cable_tv, cable_internet_phone, prepaid etc)
 */
class Dyna_Package_Model_Package extends Omnius_Package_Model_Package
{
    /** @var  Dyna_Checkout_Model_Sales_Quote */
    protected $quote = null;

    /**
     * @return array
     *
     * Get default product order
     */
    public static function getDefaultProductOrder($simcard = false, $cartPackageType = Dyna_Catalog_Model_Type::TYPE_MOBILE)
    {
        switch (strtolower($cartPackageType)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE) :
            case "template-mobile" :
                $subtypes = [
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION,
                ];
                if ($simcard) {
                    return array_merge(
                        array_slice($subtypes, 0, 1, true),
                        [Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD],
                        array_slice($subtypes, 1, count($subtypes), true)
                    );
                }
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID) :
            case "template-prepaid" :
                $subtypes = [
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION,
                ];
                if ($simcard) {
                    return array_merge(
                        array_slice($subtypes, 0, 1, true),
                        [Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD],
                        array_slice($subtypes, 1, count($subtypes), true)
                    );
                }
                break;
            case "template-dsl" :
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL) :
                $subtypes = [
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_PROMOTION,
                ];
                break;
            case "template-lte" :
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE) :
                $subtypes = [
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_PROMOTION,
                ];
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT) :
            case "template-cable_independent" :
                $subtypes = [
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                ];
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE) :
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV) :
            case "template-cable_tv" :
            case "template-cable_internet_phone" :
            default :
                $subtypes = [
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::SUBTYPE_GOODY,
                ];
                break;
        }

        return $subtypes;
    }

    /**
     * Get title based on package type
     *
     * @return string
     */
    public function getTitle()
    {
        $packageType = strtolower($this->getType());

        switch ($packageType) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
                $title = Mage::helper('dyna_checkout')->__("Mobile");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
                $title = Mage::helper('dyna_checkout')->__("Mobile Prepaid");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
                $title = Mage::helper('dyna_checkout')->__("Cable");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
                $title = Mage::helper('dyna_checkout')->__("DSL");
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
                $title = Mage::helper('dyna_checkout')->__("LTE");
                break;
            default:
                $title = Mage::helper('dyna_checkout')->__("Package");
                break;
        }

        return $title;
    }

    /**
     * Get packages for extended shopping cart
     *
     * @return array
     */
    public function getPackagesProductsForSidebarSection($items)
    {
        $hardwareItems = $tariffItems = $optionItems = $discountItems = $feeItems = $other = [];

        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = $item->getData('product');
            $itemData = $item->getData();
            // get the parsed item data that should be displayed
            $parsedItemData = $this->getParsedItemData($itemData, $product);
            // group the items based on the type of the product
            if ($product->isDevice()) {
                $this->getParsedDeviceAdditionalItemData($parsedItemData, $itemData);
                $hardwareItems['items'][] = $parsedItemData;
            } else if ($product->isSubscription()) {
                $this->getParsedSubscriptionAdditionalItemData($parsedItemData, $product);
                $tariffItems['items'][] = $parsedItemData;
            } else if ($product->isOption()) {
                $optionItems['items'][] = $parsedItemData;
            } else if ($product->isPromotion() || $product->isMobileSpecialDiscount()) {
                $discountItems['items'][] = $parsedItemData;
            } else if ($product->isFee()) {
                $feeItems['items'][] = $parsedItemData;
            }
            else {
                $other['items'][] = $parsedItemData;
            }
        }

        $packageItemsBySections = [strtolower(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE) => $hardwareItems,
            strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) => $tariffItems,
            strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON) => $optionItems,
            strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION) => $discountItems,
            strtolower(Dyna_Catalog_Model_Type::SUBTYPE_FEE) => $feeItems,
            strtolower('other') => $other,
        ];
        return $packageItemsBySections;
    }

    /**
     * Return the fields from the item needed to be displayed in the extended cart
     *
     * @param  Dyna_Catalog_Model_Product $product
     * @param $itemData
     * @return array
     */
    private function getParsedItemData($itemData, $product)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');
        $parsedItemData['name'] = $itemData['name'];
        if($product->hasMafPrice()) {
            $parsedItemData['maf_row_total_incl_tax'] = $helper->showPriceWithoutCurrencySymbol($itemData['maf_row_total_incl_tax']);
        }
        if($product->hasOTCPrice()) {
            $parsedItemData['price_incl_tax'] = $helper->showPriceWithoutCurrencySymbol($itemData['price_incl_tax']);
        }

        return $parsedItemData;
    }

    /**
     * For subscription we have also a contract period (the period is stored either in the attribute initial_period for mobile
     * of in the attribute contract_period for cable)
     * For cable subscriptions we have also a price sliding per month that should be displayed
     *
     * @param $product
     * @param array $parsedItemData
     */
    private function getParsedSubscriptionAdditionalItemData(&$parsedItemData, $product)
    {
        $parsedItemData['period'] = '';
        // the period is stored for cable in the attribute "contract_period"
        if (isset($product['contract_period']) && $product['contract_period']) {
            $parsedItemData['period'] = $product['contract_period'];
        } else if (isset($product['initial_period']) && $product['initial_period']) {
            $parsedItemData['period'] = $product['initial_period'];
        }
        // for cable packages for subscription products we need to display the price sliding per months
        if (in_array($this->getType(), Dyna_Catalog_Model_Type::getCablePackages())) {
            $productPriceSliding = isset($product['price_sliding']) ? $product['price_sliding'] : '';
            $productPriceRepeated = isset($product['price_repeated']) ? $product['price_repeated'] : '';
            $parsedItemData['price_per_month'] = $this->getPriceSlidingPerMonth($productPriceRepeated, $productPriceSliding, $parsedItemData['period']);
        }
    }

    /**
     * For devices we should display also the original price
     *
     * @param array $parsedItemData
     * @param array $item
     */
    private function getParsedDeviceAdditionalItemData(&$parsedItemData, $item)
    {
        // the devices have an initial price before the match rules are applied
        $parsedItemData['original_custom_price'] = $item['original_custom_price'];
    }

    /**
     * Render slider
     *
     * @param $priceRepeated
     * @param $priceSliding
     * @param $contractPeriod
     * @return array
     */
    private function getPriceSlidingPerMonth($priceRepeated, $priceSliding, $contractPeriod)
    {
        $pricePerMonth = [];

        if (!$contractPeriod) {
            return $pricePerMonth;
        }

        if (!$priceSliding) {
            $pricePerMonth['1-' . (string)$contractPeriod] = $priceRepeated;
            return $pricePerMonth;
        }

        $priceMonthsSlidingArray = explode(":", $priceSliding);
        $priceSumsRepeated = explode(":", $priceRepeated);
        $monthsWhenThePriceChange = [];
        $mkey = 0;
        foreach ($priceMonthsSlidingArray as $key => $monthNo) {
            // for the first value from the sliding check if is the first month 1
            if ($key == 0) {
                if ($monthNo != '') {
                    if (1 != $monthNo) {
                        $monthsWhenThePriceChange[$mkey] = 1;
                        $mkey++;
                        $monthsWhenThePriceChange[$mkey] = $monthNo;
                        $pricePerMonth['1-' . (string)$monthNo] = $priceSumsRepeated[$key];
                    } else {
                        $monthsWhenThePriceChange[$mkey] = 1;
                        $pricePerMonth['1'] = $priceSumsRepeated[$key];
                    }
                } else {
                    $monthsWhenThePriceChange[$mkey] = 1;
                    $mkey++;
                    $monthsWhenThePriceChange[$mkey] = '...';
                    $pricePerMonth['1...'] = $priceSumsRepeated[$key];
                }
            } else {
                $monthsWhenThePriceChange[$mkey] = $monthsWhenThePriceChange[$mkey - 1] + 1;
                if ($monthNo != '') {
                    $nextValue = $monthsWhenThePriceChange[$mkey - 1] + $monthNo;
                    $pricePerMonth[(string)$monthsWhenThePriceChange[$mkey] . '-' . (string)$nextValue] = $priceSumsRepeated[$key];
                    $mkey++;
                    $monthsWhenThePriceChange[$mkey] = $nextValue;
                } else {
                    $nextValue = '...';
                    $pricePerMonth[(string)$monthsWhenThePriceChange[$mkey] . '-' . (string)$nextValue] = $priceSumsRepeated[$key];
                    $mkey++;
                    $monthsWhenThePriceChange[$mkey] = $nextValue;
                }
            }
            $mkey++;
        }

        return $pricePerMonth;
    }

    public function getPackagesProductsForOverviewSection($items)
    {
        $packageItemsBySections = [];
        $packageType = strtolower($this->getType());
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($items as $item) {
            $product = $item->getData('product');
            $subType = current($product->getType());
            $itemData = $item->getData();

            // for mobile there are 3 sections: products (with device and tariff subtypes), options and discounts&promotions (with discount subtype)
            if ($packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                $this->getParsedMobileProductsForOverview($packageItemsBySections, $subType, $itemData);
            } elseif ($packageType == strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID)) {
                $this->getParsedPrepaidProductsForOverview($packageItemsBySections, $subType, $itemData);
            } elseif (in_array($packageType, Dyna_Catalog_Model_Type::getCablePackages())) {
                $this->getParsedCableProductsForOverview($packageItemsBySections, $subType, $itemData, $product);
            } elseif (in_array(strtolower($packageType), Dyna_Catalog_Model_Type::getFixedPackages())) {
                $this->getParsedDslProductsForOverview($packageItemsBySections, $subType, $itemData);
            }
        }

        return $packageItemsBySections;
    }

    protected function getParsedPrepaidProductsForOverview(&$packageItemsBySections, $subType, $itemData)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        $parsedItemData['name'] = $itemData['name'];
        $parsedItemData['price'] = isset($itemData['price_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['price_incl_tax']) : '';
        $parsedItemData['maf'] = isset($itemData['maf_row_total_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['maf_row_total_incl_tax']) : '';

        $subType = strtolower($subType);

        if ($subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) ||
            $subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD)
        ) {
            if (!isset($packageItemsBySections['products']['items'])) {
                $packageItemsBySections['products']['items'] = [];
            }
            $packageItemsBySections['products']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['products']['title'])) {
                $packageItemsBySections['products']['title'] = $helper->__('Products') . "(" . $helper->__("MobileProduct") . ")";
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION)) {
            if (!isset($packageItemsBySections['tariff']['items'])) {
                $packageItemsBySections['tariff']['items'] = [];
            }
            $packageItemsBySections['tariff']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['tariff']['title'])) {
                $packageItemsBySections['tariff']['title'] = $helper->__('Tariff Prepaid');
            }
        }
    }

    protected function getParsedCableProductsForOverview(&$packageItemsBySections, $subType, $itemData, $product)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        $parsedItemData['name'] = $itemData['name'];
        $parsedItemData['price'] = isset($itemData['price_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['price_incl_tax']) : '0,00';
        $parsedItemData['maf'] = isset($itemData['maf_row_total_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['maf_row_total_incl_tax']) : '';

        $subType = strtolower($subType);

        if ($subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)) {
            $parsedItemData['contract_period'] = isset($product['contract_period']) ? $product['contract_period'] : '24';
            $parsedItemData['price_sliding'] = isset($product['price_sliding']) ? $product['price_sliding'] : '';
            $parsedItemData['price_repeated'] = isset($product['price_repeated']) ? $product['price_repeated'] : '';
            $parsedItemData['price_per_month'] = $this->getPriceSlidingPerMonth($parsedItemData['price_repeated'], $parsedItemData['price_sliding'], $parsedItemData['contract_period']);
        }

        if ($subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) ||
            $subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE)
        ) {
            if (!isset($packageItemsBySections['products']['items'])) {
                $packageItemsBySections['products']['items'] = [];
            }
            $packageItemsBySections['products']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['products']['title'])) {
                $packageItemsBySections['products']['title'] = $helper->__('Products') . "(" . $helper->__("CableProduct") . ")";
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON) ||
            $subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SETTING)
        ) {
            if (!isset($packageItemsBySections['options']['items'])) {
                $packageItemsBySections['options']['items'] = [];
            }
            $packageItemsBySections['options']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['options']['title'])) {
                $packageItemsBySections['options']['title'] = $helper->__('Options');
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::SUBTYPE_FEE)) {
            if (!isset($packageItemsBySections['fee']['items'])) {
                $packageItemsBySections['fee']['items'] = [];
            }
            $packageItemsBySections['fee']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['fee']['title'])) {
                $packageItemsBySections['fee']['title'] = $helper->__('Fee');
            }
        } else {
            if (!isset($packageItemsBySections['other']['items'])) {
                $packageItemsBySections['other']['items'] = [];
            }
            $packageItemsBySections['other']['items'][] = $parsedItemData;

            if (!isset($packageItemsBySections['other']['title'])) {
                $packageItemsBySections['other']['title'] = $helper->__('Other');
            }
        }
    }

    protected function getParsedMobileProductsForOverview(&$packageItemsBySections, $subType, $itemData)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        $parsedItemData['name'] = $itemData['name'];
        $parsedItemData['price'] = isset($itemData['price_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['price_incl_tax']) : '';
        $parsedItemData['maf'] = isset($itemData['maf_row_total_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['maf_row_total_incl_tax']) : '';

        $subType = strtolower($subType);

        if ($subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) ||
            $subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION)
        ) {
            if (!isset($packageItemsBySections['products']['items'])) {
                $packageItemsBySections['products']['items'] = [];
            }
            $packageItemsBySections['products']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['products']['title'])) {
                $packageItemsBySections['products']['title'] = $helper->__('Products') . " (" . $helper->__("MobileProduct") . ")";
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON)) {
            if (!isset($packageItemsBySections['options']['items'])) {
                $packageItemsBySections['options']['items'] = [];
            }
            $packageItemsBySections['options']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['options']['title'])) {
                $packageItemsBySections['options']['title'] = $helper->__('Options') . " (" . $helper->__("MobileProduct") . ")";
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION)) {
            if (!isset($packageItemsBySections['promotions']['items'])) {
                $packageItemsBySections['promotions']['items'] = [];
            }
            $packageItemsBySections['promotions']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['promotions']['title'])) {
                $packageItemsBySections['promotions']['title'] = $helper->__('Discounts & Promotions') . " (" . $helper->__("MobileProduct") . ")";
            }
        } else {
            if (!isset($packageItemsBySections['other']['items'])) {
                $packageItemsBySections['other']['items'] = [];
            }
            $packageItemsBySections['other']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['other']['title'])) {
                $packageItemsBySections['other']['title'] = $helper->__('Other') . " (" . $helper->__("MobileProduct") . ")";
            }
        }
    }

    protected function getParsedDslProductsForOverview(&$packageItemsBySections, $subType, $itemData)
    {
        /**
         * @var $helper Dyna_Checkout_Helper_Data
         */
        $helper = Mage::helper('dyna_checkout');

        $parsedItemData['name'] = $itemData['name'];
        $parsedItemData['price'] = isset($itemData['price_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['price_incl_tax']) : '';
        $parsedItemData['maf'] = isset($itemData['maf_row_total_incl_tax']) ? $helper->showPriceWithoutCurrencySymbol($itemData['maf_row_total_incl_tax']) : '';

        $subType = strtolower($subType);

        if ($subType == strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_SUBSCRIPTION) ||
            $subType == strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_DEVICE)
        ) {
            if (!isset($packageItemsBySections['products']['items'])) {
                $packageItemsBySections['products']['items'] = [];
            }
            $packageItemsBySections['products']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['products']['title'])) {
                $packageItemsBySections['products']['title'] = $helper->__('Products') . " (" . $helper->__("DSL, Internet & Phone") . ")";
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON)) {
            if (!isset($packageItemsBySections['options']['items'])) {
                $packageItemsBySections['options']['items'] = [];
            }
            $packageItemsBySections['options']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['options']['title'])) {
                $packageItemsBySections['options']['title'] = $helper->__('Options');
            }
        } elseif ($subType == strtolower(Dyna_Catalog_Model_Type::DSL_SUBTYPE_PROMOTION)) {
            if (!isset($packageItemsBySections['promotions']['items'])) {
                $packageItemsBySections['discount']['items'] = [];
            }
            $packageItemsBySections['discount']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['discount']['title'])) {
                $packageItemsBySections['discount']['title'] = $helper->__('Discount & Promotions');
            }
        } elseif ($subType == Dyna_Catalog_Model_Type::SUBTYPE_FEE) {
            if (!isset($packageItemsBySections['fee']['items'])) {
                $packageItemsBySections['fee']['items'] = [];
            }
            $packageItemsBySections['fee']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['fee']['title'])) {
                $packageItemsBySections['fee']['title'] = $helper->__('Fee');
            }
        } else {
            if (!isset($packageItemsBySections['other']['items'])) {
                $packageItemsBySections['other']['items'] = [];
            }
            $packageItemsBySections['other']['items'][] = $parsedItemData;
            if (!isset($packageItemsBySections['other']['title'])) {
                $packageItemsBySections['other']['title'] = $helper->__('Other');
            }
        }
    }

    /**
     * Returns the current package status (OPEN or COMPLETED), determined by the status of package items
     * Logic is moved to Configuration Model, preventing model from beying to heavy
     * @return Dyna_Package_Model_Configuration
     */
    public function getPackageConfiguration()
    {
        /** @var Dyna_Package_Model_Configuration $packageConfiguration */
        return $packageConfiguration = Mage::getModel('dyna_package/configuration')
            ->setPackage($this);
    }


    public function getPackageSection()
    {
        $type = $this->getType();

        if (in_array($type, Dyna_Catalog_Model_Type::getFixedPackages())) {
            return "fixed";
        }

        if (in_array($type, Dyna_Catalog_Model_Type::getMobilePackages())) {
            return "mobile";
        }

        if (in_array($type, Dyna_Catalog_Model_Type::getCablePackages())) {
            return "cable";
        }

        Mage::throwException("Got an invalid package type: " . $type);
        return "";
    }

    /**
     *
     * @param null $ofType
     * @return Dyna_Checkout_Model_Sales_Quote_Item[]
     */
    public function getItems($ofType = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $allItems */
        $allItems = $this->getQuote()->getPackageItems($this->getPackageId());

        if ($ofType) {
            // Get attribute option value
            $attributeCode = Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR;

            /** @var Dyna_Checkout_Model_Sales_Quote_Item[] $filteredItems */
            $filteredItems = [];
            foreach ($allItems as $item) {
                if (strtolower($item->getProduct()->getAttributeText($attributeCode)) == $ofType) {
                    $filteredItems[] = $item;
                }
            }

            return $filteredItems;
        } else {
            return $allItems;
        }
    }

    /**
     * Return the quote instance associated to this package
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    public function getQuote()
    {
        if (!$this->quote) {
            $this->quote = Mage::getModel('sales/quote')->load($this->getQuoteId());
        }

        return $this->quote;
    }

    /**
     * Set quote on current instance. Useful when loading package from quote instance to skip loading from db
     * @param Dyna_Checkout_Model_Sales_Quote $quote
     * @return $this
     */
    public function setQuote(Dyna_Checkout_Model_Sales_Quote $quote)
    {
        $this->quote = $quote;

        return $this;
    }
}
