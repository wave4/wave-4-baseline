<?php

class Dyna_Package_Model_Configuration extends Mage_Core_Model_Abstract
{
    /** @var $package Dyna_Package_Model_Package */
    protected $package;

    /**
     * Set the package model to witch current instance is attached
     * @param Dyna_Package_Model_Package $package
     * @return $this
     */
    public function setPackage(Dyna_Package_Model_Package $package)
    {
        $this->package = $package;

        return $this;
    }

    /*
     * Build status based on package configuration
     * @return string
     */
    public function buildPackageStatus()
    {
        return $this->isPackageCompleted()
            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN;
    }

    /**
     * Get configuration for all packages / an existing package
     * @param string $package
     * @return Mage_Core_Model_Config_Element
     */
    public function getConfiguration($section = null, $packageType = null)
    {
        /** @var Mage_Core_Model_Config_Element $config */
        $config = Mage::getConfig()
            ->loadModulesConfiguration('config.xml')
            ->getNode('default/packages');

        if (!isset($config->$section)) {
            Mage::throwException("Requested section type: " . $section . " does not exist in config.xml of Dyna_Package. Cannot continue building package status without this configuration ...");
        }

        /** Mage_Core_Model_Config_Element $toReturn */
        if ($section) {
            $toReturn = $config->$section;
        } else {
            $toReturn = $config;
        }

        // Get only the node for a certain package type
        if ($packageType) {
            /** @var Mage_Core_Model_Config_Element $configData */
            foreach ($toReturn->children() as $subTypeValue => $configData) {
                if (strtolower($packageType) == strtolower($subTypeValue)) {
                    $toReturn = $configData;
                }
            }
        }

        return $toReturn;
    }

    /**
     * Check whether or not a package can be updated to completed state after validating against it's configuration
     */
    public function isPackageCompleted()
    {
        // Check if package validation is enabled in config
        if (!Mage::getStoreConfig('omnius_general/package_validation/enabled')) {
            return true;
        }

        $packageConfig = $this->getConfiguration($this->package->getPackageSection(), $this->package->getType());
        /** @var Mage_Core_Model_Config_Element $packageConfigContent */
        $packageConfigContent = $packageConfig->subtypes->children();

        // Check if packages has all the package subtypes valid
        foreach ($packageConfigContent as $subtypeIdentifier => $subtypeConfig) {
            $packageItems = $this->package->getItems($subtypeIdentifier);
            $minItems = $this->getMinProducts($subtypeConfig->cardinality);
            $maxItems = $this->getMaxProducts($subtypeConfig->cardinality);
            if (count($packageItems) < $minItems) {
                if (Mage::getStoreConfig('omnius_general/package_validation/debugging')) {
                    Mage::log("Package " . $this->package->getId() . " does not have the minimum: " . $subtypeIdentifier, null, "packages-validation.log");
                }
                // Package doesn't meet the minimum items requirement
                return false;
            }

            // If max is not infinite
            if ($maxItems && count($packageItems) > $maxItems) {
                if (Mage::getStoreConfig('omnius_general/package_validation/debugging')) {
                    Mage::log("Package " . $this->package->getId() . " does not have the maximum: " . $subtypeIdentifier, null, "packages-validation.log");
                }
                // To much items, package cannot be completed
                return false;
            }
        }

        return true;
    }


    /**
     * Get the minimum number of items required
     * @param $cardinality 1..n
     * @return int
     */
    public function getMinProducts($cardinality) {
        $card = array_map(function ($string) {
            return trim($string, ".");
        }, explode(".", $cardinality));

        return $card[0];
    }

    /**
     * Get the maximum number of items required
     * @param $cardinality 1..n
     * @return int
     */
    public function getMaxProducts($cardinality) {
        $card = array_map(function ($string) {
            return trim($string, ".");
        }, explode(".", $cardinality));

        if (strtolower($card[1]) == "n") {
            return 0;
        }

        return (int)$card[1];
    }
}
