<?php

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

$packageTable = $this->getTable('package/package');

// Add service address id column
$this->getConnection()->addColumn($packageTable, 'service_address_id', [
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => '45',
    'after' => 'status',
    'comment' => 'The service address id',
]);

$this->endSetup();
