<?php

/**
 * Class Dyna_Customer_IndexController
 * CustomerDE actions
 */
class Dyna_Customer_IndexController extends Mage_Core_Controller_Front_Action
{
    /** @var Dyna_Customer_Model_Storage|null */
    protected $storage = null;

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get the potential links for a customer
     */
    public function getCustomerPotentialLinksAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the filters
            $response = $validationHelper->validateCustomerId($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }
            try {
                /**
                 * @var $helper Dyna_Customer_Helper_Search
                 */
                $helper = Mage::helper('dyna_customer/search');
                /** @var Dyna_Customer_Model_Client_PotentialLinksClient $customerClient */
                $customerClient = Mage::getModel('dyna_customer/client_potentialLinksClient');
                $linkDataResponse = $customerClient->executeGetPotentialLink($postData);
                // parse customer search results
                $linkData['potential_links'] = $helper->parseCustomerSearchResults($linkDataResponse);

                $customerId = $postData['customerId'];
                $linkData['customer'] = $this->getCustomerSession()->getSessionCustomer($customerId);

                $response = array("error" => false, "data" => $linkData);
                $this->setJsonResponse($response);
                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }
    
    public function getCustomerAuthAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                /** @var Dyna_Customer_Model_Client $customerSearchClient */
                $customerSearchClient = Mage::getModel('dyna_customer/client')->getClient();
                $data = $customerSearchClient->executeAuthenticateCustomer($postData);
                $result = array("error" => false, "data" => $data);

                $this->setJsonResponse($result);
                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    public function createPotentialLinkAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                $customerClient = Mage::getModel('dyna_customer/client_createOrAddLinkClient');

                $requestData["targetId"] = $postData['customer'];
                foreach ($postData['potentialCustomersLinks'] as $linkId) {
                    $requestData["customers"][] = $linkId;
                }

                /** @var Dyna_Customer_Model_Client_CreateOrAddLinkClient $customerClient */
                $data = $customerClient->executeCreateOrAddLink($requestData);

                $response = array(
                    "error" => $data["Status"]["StatusReasonCode"] != "OK" ? true : false,
                    "message" => $data["Status"]["StatusReason"],
                    "data" => $data["Account"]
                );
                $this->setJsonResponse($response);

                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    /**
     * @param $result
     */
    private function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    /**
     * Unload customer data from session (customer, quotes, orders, address ...)
     */
    public function unloadCustomerAction()
    {
        $this->getCustomerSession()->unsetCustomer();
        $this->getCustomerSession()->unsOrderEdit();
        $this->getCustomerSession()->unsOrderEditMode();
        $this->getCustomerSession()->unsNewEmail();
        $this->getCustomerSession()->unsWelcomeMessage();
        $this->getCustomerSession()->unsProspect();

        $this->getCheckoutSession()->unsIsEditMode();
        $this->getCheckoutSession()->setCustomerInfoSync(false);
        $this->getCheckoutSession()->clear();

        // Unload UTC data
        $this->getCustomerSession()->unsUctParams();

        /** Clear serviceability data from session when unloading customer */
        Mage::getSingleton('dyna_address/storage')
                ->clearAddress();

        $response = array(
            'sticker' => $this->getLayout()->createBlock('core/template')->setTemplate('customer/left_search.phtml')->toHtml(),
            'saveCartModal' => $this->getLayout()->createBlock('core/template')->setTemplate('page/html/save_cart_modal.phtml')->toHtml(),
        );

        $this->setJsonResponse($response);
    }

    /**
     *
     */
    public function validateIBANAction()
    {
        $IBAN = $this->getRequest()->get("iban");
        $addressExtra = $this->getRequest()->get("addressExtra");

        try {
            $IBANClient = Mage::getModel("dyna_checkout/client_convertAndValidateIBANClient");
            $isValid = $IBANClient->executeIsValidIBAN($IBAN);

            $cartHelper = Mage::helper("dyna_checkout/cart");
            $isCable = $cartHelper->hasCable();

            $response['valid'] = $isValid;
            $response['error'] = false;


            if($isCable) {

                $response['validAddress'] = false;
                $payerInfo = Mage::getModel("dyna_customer/client_getPayerAndPaymentInfoClient");
                $payerInfoResponse = $payerInfo->executeGetPayerAndPaymentInfo(['iban'=>$IBAN]);

                if(isset($payerInfoResponse['PayerAndPaymentInfo']['PartyLegalEntity']['RegistrationAddress'])){
                    $address = $payerInfoResponse['PayerAndPaymentInfo']['PartyLegalEntity']['RegistrationAddress'];

                    $quote = Mage::getSingleton('checkout/session')->getQuote();
                    /** @var Dyna_Checkout_Helper_Fields $fieldsHelper */
                    $fieldsHelper = Mage::helper("dyna_checkout/fields");
                    $customerAddress = $fieldsHelper->getCheckoutFieldsAsArray("save_customer", $quote->getId());

                    if($addressExtra) {
                        $addressComp = [
                            $addressExtra['delivery[iban_check']['postcode'] == $address['PostalZone'],
                            $addressExtra['delivery[iban_check']['city'] == $address['CityName'],
                            $addressExtra['delivery[iban_check']['street'] == $address['StreetName'],
                            $addressExtra['delivery[iban_check']['houseno'] == $address['BuildingNumber'],
                        ];
                    } else {
                        $addressComp = [
                            $customerAddress['customer[address][postcode]'] == $address['PostalZone'],
                            $customerAddress['customer[address][city]'] == $address['CityName'],
                            $customerAddress['customer[address][district]'] == $address['District'],
                            $customerAddress['customer[address][street]'] == $address['StreetName'],
                            $customerAddress['customer[address][houseno]'] == $address['BuildingNumber'],
                        ];
                    }


                    $isValidAddress = array_reduce($addressComp, function($result, $item){
                        return $result && $item;
                    }, true);

                    if($isValidAddress) {
                        $response['validAddress'] = true;
                    }
                }
            }

        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }
        $this->setJsonResponse($response);
    }

    /**
     * Generate and return bank account data
     */
    public function generateBankDataAction()
    {
        $params = [];
        $params['account_number'] = $this->getRequest()->getPost('account_number');
        $params['bank_code'] = $this->getRequest()->getPost('bank_code');
        $params['credit_provider'] = $this->getRequest()->getPost('credit_provider');
        $params['usage'] = $this->getRequest()->getPost('usage');

        try {
            $IBANClient = Mage::getModel("dyna_checkout/client_convertAndValidateIBANClient");
            $response = $IBANClient->executeConvertAndValidateByBankData($params['account_number'], $params['bank_code']);
            $response['error'] = false;
        } catch (Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage(),
            ];
        }

        $this->setJsonResponse($response);
    }
}
