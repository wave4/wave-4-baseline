<?php

/**
 * Class Dyna_Customer_DetailsController
 */
class Dyna_Customer_DetailsController extends Mage_Core_Controller_Front_Action
{
    /** @var Dyna_Customer_Model_Session|null */
    protected $storage = null;

    /**
     * Returns the customer details (address, name, id, phone) that are displayed in the left side of the page
     * This is the getCustomerDetailsMode1 request
     *
     * @IsGranted({"permissions":["SEARCH_CUSTOMER"]})
     */
    public function loadCustomerAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the filters
            $response = $validationHelper->validateCustomerId($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }

            $requestData = [
                "customerId" => $postData["customerId"],
            ];

            try {
                /** @var Dyna_Customer_Model_Client_RetrieveCustomerInfo $customerSearchClient */
                $customerSearchClient = Mage::getModel('dyna_customer/client_retrieveCustomerInfo')->getClient();
                $data = $customerSearchClient->executeRetrieveCustomerInfo($requestData);

                // if the service did not respond with success -> display a message
                if (!$data['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the customer loading. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }
                $response = $this->loadCustomerOnSession($data);
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    /**
     * Returns json response for customer left screen depending on the requested section (orders-content, products-content ... etc)
     */
    public function showCustomerPanelAction()
    {
        if (!$customer = Mage::getSingleton("dyna_customer/session")->getCustomer()) {
            $response = $this->parseError("Please select a customer.");

            $this->setJsonResponse($response);
            return;
        }

        if (!$section = $this->getRequest()->getParam('section')) {
            $response = $this->parseError("No valid section requested.");

            $this->setJsonResponse($response);
            return;
        }

        /**
         * @var $panelHelper Dyna_Customer_Helper_Panel
         */
        $panelHelper = Mage::helper('dyna_customer/panel');
        $response = $panelHelper->getPanelData($section);

        $this->setJsonResponse($response);
    }

    /**
     * Get the billing(monthly fee) information.
     * This action is possible only for mobile products
     */
    public function getBillingInformationAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the filters
            $response = $validationHelper->validateBan($postData);
            if (!empty($response['validationError'])) {
                $this->setJsonResponse($response);
                return;
            }

            try {
                /** @var Dyna_Customer_Model_Client $customerSearchClient */
                $customerSearchClient = Mage::getModel('dyna_customer/client')->getClient();
                $data = $customerSearchClient->executeGetBillingCustomerDetails($postData);
                // if the service did not respond with success -> display a message
                if (!$data['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the monthly fee details loading. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($data));
                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    /**
     * Make a retention check for multiple mobile accounts
     */
    public function prolongationCheckAction()
    {
        if ($postData = $this->getRequest()->getPost()) {

            if (!$customer = Mage::getSingleton("dyna_customer/session")->getCustomer()) {
                $response = $this->parseError("Please select a customer.");

                $this->setJsonResponse($response);
                return;
            }
            /**
             * @var $validationHelper Dyna_Customer_Helper_Validation
             */
            $validationHelper = Mage::helper('dyna_customer/validation');
            // parse and validate the filters
            $validationResponse = $validationHelper->validateAccountStatus($postData['accounts']);
            if (!empty($validationResponse['validationError'])) {
                $this->setJsonResponse($validationResponse);
                return;
            }

            $accountsToCheckForRetention = [];
            $results = [];
            if (!empty($validationResponse['error'])) {
                foreach ($validationResponse['message']['accounts'] as $contractNo => $message) {
                    foreach ($postData['accounts'] as $contractNoToCheck => $kias)
                        if ($contractNo != $contractNoToCheck) {
                            $accountsToCheckForRetention[] = $postData['accounts'][$contractNoToCheck];
                        } else {
                            $results[$contractNo] = $message;
                        }
                }
            }
            else {
                $accountsToCheckForRetention = $postData['accounts'];
            }


            try {
                /** @var Dyna_Customer_Model_Client $customerSearchClient */
                $customerSearchClient = Mage::getModel('dyna_customer/client_retrieveCustomerCreditProfileClient');
                $data = $customerSearchClient->executeRetrieveCustomerCreditProfile($postData);
                // if the service did not respond with success -> display a message
                if (!$data['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the retention check. Please try again!"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

//                foreach ($data['RetrieveCustomerCreditProfileResponse']['CustomerCreditProfile'] as $subscription) {
//                    foreach ($accountsToCheckForRetention as $contractNo) {
//                        if ($subscription['contract_no'] == $contractNo) {
//                            if (!empty($subscription['prolongation_time_per_type'])) {
//                                $results[$contractNo] = $subscription['prolongation_time_per_type'];
//                            }
//                        }
//                    }
//                }

                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json')
                    ->setBody(Mage::helper('core')->jsonEncode($results));
                return;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }

        }
    }


    /**
     * Set the show BTW
     */
    public function setCustomerTypeSohoAction()
    {
        /** @var Omnius_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        $state = $this->getRequest()->getParam('state', 0);
        $session->setIsSoho($state == 1 || $state == 'true');
    }

    /**
     * Set the show BTW
     */
    public function getCustomerTypeSohoAction()
    {
        /** @var Omnius_Customer_Model_Session $session */
        $session = Mage::getSingleton('customer/session');

        $this->getResponse()->setHeader('Content-Type', 'application/json')
            ->setBody(
                Mage::helper('core')->jsonEncode(
                    array(
                        'isSoho' => $session->getIsSoho(),
                    )
                )
            );
    }

    protected function loadCustomerOnSession($customerData)
    {
        /** @var Dyna_Customer_Model_Customer $customerObject */
        $customerObject = Mage::getModel("customer/customer")
            ->updateCustomerInfo($customerData);
        $this->getCustomerSession()->setCustomer($customerObject);

        // Calling helper to map services data to frontend data
        $customerData = Mage::helper('dyna_customer/info')->buildCustomerDetails($customerObject);

        return array(
            'sticker' => $this->loadLayout('page_three_columns')->getLayout()->getBlock('customer.details')->toHtml(),
            'customerData' => $customerData,
        );
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('customer/session');
        }

        return $this->storage;
    }

    /**
     * @param $result
     */
    private function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    private function parseError($message)
    {
        return [
            "error" => true,
            "message" => $this->__($message),
        ];
    }
}
