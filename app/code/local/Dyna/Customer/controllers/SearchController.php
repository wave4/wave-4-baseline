<?php

/**
 * Class Dyna_Customer_SearchController
 */
class Dyna_Customer_SearchController extends Mage_Core_Controller_Front_Action
{
    const MAX_SEARCH_RESULTS = 40;

    /** @var Dyna_Customer_Model_Session|null */
    protected $storage = null;

    /**
     *
     */
    public function getCustomerResultsLegacyAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                /**
                 * @var $helper Dyna_Customer_Helper_Search
                 */
                $helper = Mage::helper('dyna_customer/search');
                // parse and validate the filters
                $response = $helper->parseCustomerSearchParams($postData);
                // if there is a validation error => skip service call and display the error
                if (!empty($response['validationError'])) {
                    $this->setJsonResponse($response);
                    return;
                }
                $customerClient = Mage::getModel('dyna_customer/client_searchCustomerFromLegacyClient');
                // call the service to filter customers
                $customersResponse = $customerClient->executeSearchCustomerFromLegacy($response['params']);
                // if the service did not respond with success -> display a message
                if(!$customersResponse['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the service search customers. Please try again"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                // parse customer search results
                $customers = $helper->parseCustomerSearchResults($customersResponse);

                if (!empty($customers)) {
                    // Save customers to session
                    $this->getStorage()->setCustomers($customers);
                    $result = [
                        "error" => false,
                        "customers" => $customers,
                    ];

                    if (count($customers) == 1) {
                        $customer = current($customers);
                        $result = [
                            "error" => false,
                            "one_result" => true,
                            "data" => [
                                "customer" => $customer,
                            ]
                        ];

                    }

                } elseif (count($customers) > static::MAX_SEARCH_RESULTS) {
                    // More than 40 customers found
                    // todo: determine if error is received from service or not
                    $this->getStorage()->unsCustomers();
                    $result = [
                        "error" => true,
                        "message" => $this->__("More than 40 results found, please refine your search criteria")
                    ];
                } else {
                    // No customers found
                    $this->getStorage()->unsCustomers();
                    $result = [
                        "error" => true,
                        "message" => $this->__("0 matching results found. Please check the spelling or use more search criteria.")
                    ];
                }

                $this->setJsonResponse($result);

                return;
            } catch (\Exception $e) {
                $result = [
                    "error" => true,
                    "message" => $e->getMessage()
                ];

                $this->setJsonResponse($result);
            }
        }
    }
    /*
     * Get the customer results filtered
     *
     * If there is a single account retrieved (with or without other accounts linked to it) we will display directly the potential link accounts page
     * Else we display the list with all accounts.
     *
     * The list with accounts is parsed as follows in order to group the accounts that are linked together.
     * Foreach account we set 2 properties:
     * "linked" property will contain an array with all the accounts that are linked to this one
     * "isLinkedChild" property is a boolean that will be set to "true" if the account is a linked account not a parent account.
     * (in business logic all linked accounts are on the same level but we have decided that the first found will be considered as a "parent")
     * The accounts that have the same "Contact"."ID" are considered to be linked.
     */
    public function getCustomerResultsAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            try {
                /**
                 * @var $helper Dyna_Customer_Helper_Search
                 */
                $helper = Mage::helper('dyna_customer/search');
                // parse and validate the filters
                $response = $helper->parseCustomerSearchParams($postData);
                // if there is a validation error => skip service call and display the error
                if (!empty($response['validationError'])) {
                    $this->setJsonResponse($response);
                    return;
                }

                /** @var Dyna_Customer_Model_Client $customerClient */
                /** @var Dyna_Customer_Model_Client $customerClientModel */
                $customerClient = Mage::getModel('dyna_customer/client_searchCustomerClient');
                // call the service to filter customers
                $customersResponse = $customerClient->executeSearchCustomer($response['params']);
                // if the service did not respond with success -> display a message
                if(!$customersResponse['Success']) {
                    $result = [
                        "error" => true,
                        "message" => $this->__("There was a problem with the service search customers. Please try again"),
                    ];

                    $this->setJsonResponse($result);
                    return;
                }

                // parse customer search results
                $customersResult = $helper->parseCustomerSearchAndGroupLinkedAccounts($customersResponse);
                $customers = $customersResult['customers'];
                $countUnlinkedCustomers = $customersResult['countUnlinkedCustomers'];

                if (!empty($customers) && $countUnlinkedCustomers <= static::MAX_SEARCH_RESULTS) {
                    // Save customers to session
                    $this->getStorage()->setCustomers($customers);
                    $result = [
                        "error" => false,
                        "customers" => $customers,
                    ];

                    if ($countUnlinkedCustomers == 1) {
                        $customer = current($customers);
                        /** @var Dyna_Customer_Model_Client_PotentialLinksClient $customerLinkClient */
                        $customerLinkClient = Mage::getModel('dyna_customer/client_potentialLinksClient');
                        // One result case; Do not display customers list, Select Customer starts
                        $linkDataResponse = $customerLinkClient->executeGetPotentialLink(['customerId' => $customer['Role']['Person']['ID']]);
                        // parse customer search results
                        $linkData = $helper->parseCustomerSearchResults($linkDataResponse);

                        $result = [
                            "error" => false,
                            "one_result" => true,
                            "data" => [
                                "customer" => $customer,
                            ]
                        ];

                        if (count($linkData)) {
                            $result['data']['potential_links'] = $linkData;
                        }
                    }

                } elseif ($countUnlinkedCustomers > static::MAX_SEARCH_RESULTS) {
                    // More than 40 customers found
                    // todo: determine if error is received from service or not
                    $this->getStorage()->unsCustomers();
                    $result = [
                        "error" => true,
                        "message" => $this->__("More than 40 results found, please refine your search criteria")
                    ];
                } else {
                    // No customers found
                    $this->getStorage()->unsCustomers();
                    $result = [
                        "error" => true,
                        "message" => $this->__("0 matching results found. Please check the spelling or use more search criteria.")
                    ];
                }

                $this->setJsonResponse($result);

                return;
            } catch (\Exception $e) {
                $result = [
                    "error" => true,
                    "message" => $e->getMessage()
                ];

                $this->setJsonResponse($result);
            }
        }
    }

    /**
     * Get the customer results list with a ctn received from UCT that makes an URL ReceiveUCTLinkAPI call with parameter
     */
    public function getCustomerResultsWithUctAction()
    {
        if ($data = $this->getRequest()->getParams()) {
            // Save uct params to session
            $this->getStorage()->setUctParams($data);
        }

        $this->_redirect('/');
    }

    /**
     * @return Dyna_Customer_Model_Session
     */
    protected function getStorage()
    {
        if (!$this->storage) {
            $this->storage = Mage::getSingleton('dyna_customer/session');
        }

        return $this->storage;
    }

    /**
     * @param $result
     */
    private function setJsonResponse($result)
    {
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }
}
