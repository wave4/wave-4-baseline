<?php

class Dyna_Customer_Model_Client_RetrieveCustomerInfo extends Dyna_Service_Model_Client
{
    const STATUS_REASON_CODE = "OK";
    const CONFIG_PATH = "omnius_service/customer_info/";

    /**
     * @param $params
     * @param $mode
     * @return mixed
     */
    public function executeRetrieveCustomerInfo($params)
    {
        $this->mapRequestParams($params);
        $this->setRequestHeaderInfo($params);
        $response = $this->RetrieveCustomerInfo($params);

        $response["customer_number"] = $params["customerId"];

        // StatusReasonCode: Contains the result code (OK or error code) from the call to the OGW
        if (!empty($response["Status"]["StatusReasonCode"]) && $response["Status"]["StatusReasonCode"] == self::STATUS_REASON_CODE) {
            $response["Success"] = true;
        } else {
            $response["Success"] = false;
        }

        return $response;
    }

    protected function mapRequestParams(&$params)
    {
        $params["Customer"]["ID"] = $params["customerId"];
    }
}
