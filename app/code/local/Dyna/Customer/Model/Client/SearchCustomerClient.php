<?php

/**
 * Class Dyna_Customer_Model_Client_SearchCustomerClient
 */
class Dyna_Customer_Model_Client_SearchCustomerClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "customer_search/wsdl";
    const ENDPOINT_CONFIG_KEY = "customer_search/usage_url";
    protected $_forceUseStubs = false;

    /**
     * @param $params
     * @param $mode
     * @return mixed
     */
    public function executeSearchCustomer($params, $mode = null)
    {

        if ($mode) {
            $values = $this->{$mode}($params);
        } else {
            $searchParams = $this->mapCustomerSearchParametersToXMLNodes($params);
            $this->setRequestHeaderInfo($searchParams);
            $values = $this->SearchCustomer($searchParams, !empty($params['telephone_number']) ? $params['telephone_number'] : null);
        }

        return $values;
    }

    /**
     * Map the form search parameters for customer to an array with the structure needed for the XML that will be send to the service
     *
     * @access private
     * @param array $searchParams
     * @return array
     */
    private function mapCustomerSearchParametersToXMLNodes($searchParams)
    {
        $parametersMapping = [];
        foreach ($searchParams as $key => $value) {
            switch ($key) {
                case 'company_name':
                    $parametersMapping['SearchedAccount']['PartyName']['Name'] = $value;
                    break;
                case 'customer':
                    $parametersMapping['SearchedContact']['Role']['Person']['ID'] = $value;
                    break;
                case 'first_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FirstName'] = $value;
                    break;
                case 'last_name':
                    $parametersMapping['SearchedContact']['Role']['Person']['FamilyName'] = $value;
                    break;
                case 'birthday':
                    $parametersMapping['SearchedContact']['Role']['Person']['BirthDate'] = $value;
                    break;
                case 'telephone_number':
                    $parametersMapping['SearchedContact']['Role']['Person']['Contact']['Telephone'] = $value;
                    break;
                case 'email':
                    $parametersMapping['SearchedContact']['Role']['Person']['Contact']['ElectronicMail'] = $value;
                    break;
                case 'street':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['StreetName'] = $value;
                    break;
                case 'no':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['BuildingNumber'] = $value;
                    break;
                case 'city':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['CityName'] = $value;
                    break;
                case 'zipcode':
                    $parametersMapping['SearchedContact']['Role']['Person']['ResidenceAddress']['PostalZone'] = $value;
                    break;
                case 'device_id':
                    $parametersMapping['SearchedEquipment']['ManufacturersItemIdentification'] = function ($node) use ($value) {
                        foreach ($node[0]->children() as $name => $child) {
                            $node[0]->{$name} = $value;
                        }
                    };
                    break;
                default:
                    break;
            }
        }

        return $parametersMapping;
    }
}
