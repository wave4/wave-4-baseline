<?php

/**
 * Class Dyna_Customer_Model_Client_PotentialLinksClient
 */
class Dyna_Customer_Model_Client_PotentialLinksClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "potential_links/wsdl_potential_links";

    /**
     * @param $params
     * @return mixed
     */
    public function executeGetPotentialLink($params)
    {
        $searchParams = $this->mapCustomerSearchPotentialLinks($params);
        $this->setRequestHeaderInfo($searchParams);
        $values = $this->WSSearchPotentialLinks($searchParams);

        return $this->parseResponseData($values);
    }

    /**
     * @param $response
     * @return mixed
     */
    public function parseResponseData($response)
    {
        // Make sure that we always return an array of accounts
        if (!empty($response['Account']['PartyName'])) {
            $response['Account'] = [$response['Account']];
        }

        return $response;
    }

    /**
     * @param $searchParams
     * @return mixed
     */
    private function mapCustomerSearchPotentialLinks($searchParams)
    {
        $parametersMapping['Customer']['ID'] = $searchParams['customerId'];

        return $parametersMapping;
    }
}
