<?php

/**
 * Class Dyna_Customer_Model_Client_RetrieveCustomerCreditProfileClient
 */
class Dyna_Customer_Model_Client_RetrieveCustomerCreditProfileClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "customer_credit_profile/wsdl_customer_credit_profile";

    /**
     * @param $params
     * @return mixed
     */
    public function executeRetrieveCustomerCreditProfile($params)
    {
        $searchParams = $this->mapRetrieveCustomerCreditProfile($params);
        $this->setRequestHeaderInfo($searchParams);
        $values = $this->RetrieveCustomerCreditProfile($searchParams);
        return $values;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function mapRetrieveCustomerCreditProfile($params)
    {
        $customerId = $params['customerId'] ?? $params['linkId'];
        $parametersMapping['DealerCode']['ID'] = $params['dealerId'];
        $parametersMapping['CustomerAccount']['PartyIdentification']['ID'] = $customerId;
        foreach($params['accounts'] as $acc) {
            $parametersMapping['CustomerAccount']['Subscription'][]['Ctn'] = $acc['ctn'];
        }

        $parametersMapping['DealerCode']['ID'] = $params['dealerId'];

        return $parametersMapping;
    }
}
