<?php

/**
 * Class Dyna_Customer_Model_Client
 */
class Dyna_Customer_Model_Client extends Omnius_Service_Model_Client_Client
{
    const CONFIG_PATH = 'omnius_service/customer_search/';

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    /** @var string Unique client identifier */
    protected $_type = 'customer_search';

    /** @var array Client options */
    protected $_options = [];

    /** @var array */
    protected $_registry = [];

    /** @var bool */
    protected $_forceUseStubs = true;

    /** @var string */
    protected $_namespace;

    /**
     * Returns the customer details (address, name, id, phone) that are displayed in the left side of the page
     * This is the getCustomerDetailsMode1 request
     *
     * @param $params
     * @param string $mode
     * @return mixed
     */
    public function executeGetOnlyCustomerDetails($params, $mode = null)
    {
        if ($mode) {
            $values = $this->{$mode}($params);
        } else {
            $customerDetailsParams = $this->mapGetOnlyCustomerDetailsParametersToXMLNodes($params);
            $values = $this->getCustomerDetailsMode1($customerDetailsParams, $params['customerId']);
        }

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    /**
     * Returns the customer products
     * This is the getCustomerDetailsMode2 request
     *
     * @param $params
     * @param string $mode
     * @return mixed
     */
    public function executeGetProductsCustomerDetails($params, $mode = null)
    {
        if ($mode) {
            $values = $this->{$mode}($params, $params['LinkId']);
        } else {
            $values = $this->getCustomerDetailsMode2($params, $params['LinkId']);
        }

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    /**
     * Returns the customer billing details for a certain mobile product
     * This is the getCustomerDetailsMode3 request
     *
     * @param $params
     * @param string $mode
     * @return mixed
     */
    public function executeGetBillingCustomerDetails($params, $mode = null)
    {
        if ($mode) {
            $values = $this->{$mode}($params);
        } else {
            $customerDetailsParams = $this->mapGetBillingInfoCustomerDetailsParametersToXMLNodes($params);
            $values = $this->getCustomerDetailsMode3($customerDetailsParams);
        }

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    /**
     * Returns the prolongation check for a certain mobile account
     *
     * @param $params
     * @param string $mode
     * @return mixed
     */
    public function executeProlongationCheck($params, $mode = null)
    {
        if ($mode) {
            $values = $this->{$mode}($params);
        } else {
            $customerDetailsParams = $this->mapProlongationParametersToXMLNodes($params);
            $values = $this->getSubsidizedHandsetValidation($customerDetailsParams);
        }

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }


    /**
     * @param $params
     * @param string $mode
     * @return mixed
     */
    public function executeAuthenticateCustomer($params, $mode = "getAuthenticationData")
    {
        $values = $this->AuthenticateCustomer($params, $mode);

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    public function executeCreateOrAddPotentialLink($params)
    {
        $values = $this->CreateOrAddPotentialLink($params);

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    /**
     * @param $params
     * @return mixed
     */
    public function getBankData($params)
    {
        $values = $this->generateBankData($params);

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    /**
     * @return array
     */
    public function getDefaultOptions()
    {
        return [
            $this->_type => [
                'wsdl' => $this->getConfig('wsdl'),
                'endpoint' => $this->getConfig('usage_url'),
                'soap_version' => SOAP_1_1,
                'trace' => true,
                'connection_timeout' => $this->getHelper()->getGeneralConfig('timeout'),
                'keep_alive' => false,
            ]
        ];
    }

    /**
     * @param $functionality
     * @param $request
     * @return mixed
     */
    public function executeRequest($functionality, $request)
    {
        $params = array(
            'functionalityName' => $functionality,
            'loginName' => $this->getConfig('login'),
            'password' => $this->getConfig('password'),
            'request' => $request,
        );

        return $this->ExecuteRequestWithLogin($params);
    }

    /**
     * @param $obj
     * @return array
     */
    protected function objectToArray($obj)
    {
        if (is_object($obj)) {
            $obj = (array)$obj;
        }
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else {
            $new = $obj;
        }

        return $new;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Map the customer id paramers to the parameters requested for get only customer details request (getCustomerDetails mode1)
     *
     * @access private
     * @param array $params
     * @return array
     */
    private function mapGetOnlyCustomerDetailsParametersToXMLNodes($params)
    {
        $parametersMapping = [];
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'customerId':
                    $parametersMapping['Id'] = $value;
                    break;
                default:
                    break;
            }
        }

        return $parametersMapping;
    }

    /**
     * Map the customer id parameters to the parameters requested for get only customer details request (getCustomerDetails mode1)
     *
     * @access private
     * @param array $params
     * @return array
     */
    private function mapProlongationParametersToXMLNodes($params)
    {
        $parametersMapping = [];
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'customerId':
                    $parametersMapping['BanKias'] = $params['customerId'];
                    break;
                case 'linkId':
                    if(!empty( $params['linkId'])) {
                        $parametersMapping['LinkId'] = $params['linkId'];
                    }
                    break;
                default:
                    break;
            }
        }

        return $parametersMapping;
    }


    /**
     * Map the customer id parameters to the parameters requested for get only customer details request (getCustomerDetails mode1)
     *
     * @access private
     * @param array $params
     * @return array
     */
    private function mapGetBillingInfoCustomerDetailsParametersToXMLNodes($params)
    {
        $parametersMapping = [];
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'ban':
                    $parametersMapping['Ban'] = $value;
                    break;
                default:
                    break;
            }
        }

        return $parametersMapping;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getStubClient()
    {
        return Mage::getSingleton('dyna_service/multipleStubClient');
    }
}
