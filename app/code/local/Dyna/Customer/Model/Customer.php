<?php

class Dyna_Customer_Model_Customer extends Omnius_Customer_Model_Customer_Customer
{
    protected $hardwareSerials = [];
    /** @var  Varien_Object */
    protected $ownSmartcard = null;
    /** @var  Varien_Object */
    protected $ownReceiver = null;

    /** @var array  */
    // This is where we keep the data retrieved from services for current customer
    protected $retrievedCustomerInfoData = null;

    /** @var Varien_Object */
    protected $legalAddress = null;
    /** @var string */
    protected $phoneNumber = null;
    /** @var string */
    protected $phoneLocalAreaCode = null;
    /** @var string */
    protected $emailAddress = null;
    /** @var string */
    protected $serviceAddressId = null;

    /**
     * Check if customer is logged in
     */
    public function isCustomerLoggedIn()
    {
        return $this->getCustomerNumber();
    }

    /**
     * Get services customer id (customer number)
     * @return mixed
     */
    public function getCustomerNumber()
    {
        return $this->getData('customer_number');
    }
    //TODO - implement to use only this method for the entire cart display: do not use stub; also get the current customer carts only
    public function getShoppingCarts($includeOffers = false)
    {
        /** @var Mage_Sales_Model_Resource_Quote_Collection $carts */
        $carts = Mage::getModel('sales/quote')->getCollection();
        //todo leave this when the cart is saved per customer
       // $carts->addFieldToFilter('main_table.customer_id', Mage::getSingleton('customer/session')->getId()); //getSelectedCustomer
        $carts->getSelect()->where(
            '(
                -- saved shopping cart
                main_table.cart_status = \'' . Dyna_Checkout_Model_Sales_Quote::CART_STATUS_SAVED . '\'
                  AND main_table.is_offer = \'0\'
                ) 
                OR (
                  -- offer
                  main_table.cart_status IS NULL 
                  AND main_table.is_offer = \'1\'
                  AND main_table.store_id = ' . Mage::app()->getStore()->getId() . '
                )'
        );

        $carts->setOrder('main_table.created_at', 'desc');
        $carts->getSelect()->joinLeft(array('orders' => 'sales_flat_order'), 'main_table.entity_id = orders.quote_id',
            array('orders.entity_id as order_id'));
        $carts->getSelect()->group('main_table.entity_id');
        $cartsArray = array();
        $packages = array();

        foreach ($carts as $cart) {
            /** @var Dyna_Checkout_Model_Sales_Quote $cart */
            if ($cart->getOrderId()) {
                continue;
            }
            $tempPackages = $cart->getPackages();
            if (count($tempPackages) == 0) {
                continue;
            }
            unset($openPackages);
            $openPackages = [];
            foreach ($tempPackages as $i => $package) {
                $packageId = $package['package_id'];
                if (!count($package['items']) || $package['current_status'] != Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                    $openPackages[$packageId] = $package;
                    unset($tempPackages[$i]);
                    continue;
                }
                if (count($openPackages) > 0) {
                    foreach ($openPackages as $pack) {
                        array_push($tempPackages, $pack);
                    }
                }
                unset($openPackages);
            }

            foreach ($tempPackages as $id => $package) {
                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel = Mage::getModel('package/package')->getPackages(null, $cart->getId(), $package['package_id'])->getFirstItem();
                $packageModel->setData('items', $package['items']);
                $packages[] = $packageModel;
            }

            if($packages) {
                $cartsArray[$cart->getId()]['packages'] =  $packages;
                $cartsArray[$cart->getId()]['cart'] = $cart;
            }
        }

        return $cartsArray;
    }

//    /**
//     * Returns customer id (ban) that is not mapped to db entity_id
//     * return string
//     */
//    public function getId()
//    {
//        return $this->getData('ban');
//    }

    /**
     * Return translated salutation for customer
     * @return string
     */
    public function getTitle()
    {
        return $this->getData("title");
    }

    /**
     * Get company name for business type customer
     * @return mixed
     */
    public function getCompanyName()
    {
        if ($this->getCustomerNumber()) {
            return $this->getCompany()->getCompanyName();
        }

        return "";
    }

    /**
     * Get company additional name for business type customer
     * @return mixed
     */
    public function getCompanyAdditionalName()
    {
        return $this->getData('company_additional_name');
    }

    /**
     * Get street from customer address
     * @return mixed
     */
    public function getStreet()
    {
        return $this->getAddress()
            ->getStreet();
    }


    /**
     * Get house number from customer address
     * @return mixed
     */
    public function getHouseNo()
    {
        return $this->getAddress()
            ->getHouseNumber();
    }

    /**
     * Get postal code from customer address
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->getAddress()
            ->getPostcode();
    }

    /**
     * Get city from customer address
     * @return mixed
     */
    public function getCity()
    {
        return $this->getAddress()
            ->getCity();
    }

    /**
     * Get district from customer address
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->getAddress()
        ->getDistrict();
    }

    /**
     * Get state from customer address
     * @return string
     */
    public function getState()
    {
        return $this->getAddress()
            ->getState();
    }

    /**
     * Return the current customer country
     * @return string
     */
    public function getCountry()
    {
        return $this->getData('country') ?: "Deutschland";
    }

    /**
     * Get full address info for current customer in the following format:
     * Mr. Full Customer Name,
     * Street, HouseNo
     * Postal Code, City
     * Country
     * @return string
     */
    public function getFullAddressInfo($isSohoCustomer = false, $addDistrict = false, $addState = false)
    {
        if (!$this->isCustomerLoggedIn()) {
            return "";
        }

        $fullAddressInfo =  "";
        $fullAddressInfo = $fullAddressInfo
            . ($isSohoCustomer ? $this->getCompanyName() . " " . $this->getCompanyAdditionalName() . "<br>" : "" )
            . $this->getTitle()
            . " " . $this->getName() . "<br>"
            . $this->getStreet()
            . " " . $this->getHouseNo() . "," . "<br>"
            . $this->getPostalCode()
            . " " . $this->getCity()
            . ($addDistrict ? " " . $this->getDistrict() : "")
            . ($addState ? " " . $this->getState() : "")
        ;

        return $fullAddressInfo;
    }

    /**
     * Retrieving legal address from first of type legal found in subscription list
     * @todo clarify if there is another address received for current customer other than the ones set on subscriptions
     */
    public function getAddress()
    {
        if (!$this->getCustomerNumber()) {
            $this->legalAddress = new Varien_Object();
            return $this->legalAddress;
        }

        if (!$this->legalAddress) {
            /** @var Varien_Object $subscriptions */
            $subscriptions = $this->getContracts()->getSubscriptions();
            foreach ($subscriptions->getAddresses() as $address) {
                if ($address->getAddressType() == Dyna_Customer_Helper_Services::ADDRESS_TYPE_LEGAL) {
                    $this->legalAddress = $address;
                    break;
                }
            }
        }

        if (!$this->legalAddress) {
            // If no legal address found, set an empty Varien_Object on current customer
            $this->legalAddress = new Varien_Object();
        }

        return $this->legalAddress;
    }

    /**
     * Retrieving customer private number from contact phone number list
     * @todo clarify that this is the primarily phone number that the customer is identified with
     */
    public function getPhoneNumber()
    {
        if (!$this->phoneNumber && $this->getCustomerNumber()) {
            /** @var Varien_Object $contract */
            foreach ($this->getContactPhoneNumbers() as $phoneNumber) {
                if ($phoneNumber->getType() == Dyna_Customer_Helper_Services::PHONE_PRIVATE) {
                    $this->phoneNumber = $phoneNumber->getPhoneNumber();
                    break;
                }
            }
        }

        if (!$this->phoneNumber) {
            // If no private phone number found, return an empty Varien_Object
            $this->phoneNumber = "";
        }

        return $this->phoneNumber;
    }

    /**
     * Return the local area phone code from contact phone number list
     * @return string
     */
    public function getLocalAreaPhoneCode()
    {
        if (!$this->phoneLocalAreaCode && $this->getCustomerNumber()) {
            /** @var Varien_Object $contract */
            foreach ($this->getContactPhoneNumbers() as $phoneNumber) {
                if ($phoneNumber->getType() == Dyna_Customer_Helper_Services::PHONE_PRIVATE) {
                    $this->phoneLocalAreaCode = $phoneNumber->getLocalAreaCode();
                    break;
                }
            }
        }

        if (!$this->phoneLocalAreaCode) {
            // If no private phone number found, return an empty Varien_Object
            $this->phoneLocalAreaCode = "";
        }

        return $this->phoneLocalAreaCode;
    }

    /**
     * Return customer e-mail address as the first found contact_person from the contacts list of subscriptions
     * No reference to private person e-mail address found
     * @todo clarify if this is the correct e-mail address or additional info will be appended to service response for private customers
     * @return mixed
     */
    public function getEmailAddress()
    {
        // If no customer
        if (!$this->getCustomerNumber()) {
            $this->emailAddress = "";
            return $this->emailAddress;
        }

        if (!$this->emailAddress) {
            if ($this->getIsSoho()) {
                $this->emailAddress = $this->getCompany()->getContactEmail();
            } else {
                /** @var Varien_Object $subscriptionContact */
                foreach ($this->getContracts()->getSubscriptions()->getContacts() as $subscriptionContact) {
                    if ($contactPerson = $subscriptionContact->getContactPerson()) {
                        if ($hasEmail = $contactPerson->getEmail()) {
                            $this->emailAddress = $hasEmail;
                            break;
                        }
                    }
                }
            }
        }

        if (!$this->emailAddress) {
            $this->emailAddress = "";
        }

        return $this->emailAddress;
    }

    /**
     * Get customer (if exists) first name
     * @return string
     */
    public function getFirstName()
    {
        return $this->getData('firstname') ?: $this->getData('first_name');
    }

    /**
     * Get customer (if exists) last name
     * @return string
     */
    public function getLastName()
    {
        return $this->getData('lastname') ?: $this->getData('last_name');
    }

    /**
     * Return customer full name
     * @return mixed
     */
    public function getName()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    /**
     * Get current customer service address id
     * @return string
     */
    public function getServiceAddressId()
    {
        if (!$this->serviceAddressId && $this->getCustomerNumber()) {
            $this->serviceAddressId = $this->getAddress()->getId();
        }

        return $this->serviceAddressId;
    }

    /**
     * Get current customer billing bank account number
     * @return string
     */
    public function getBankAccountNumber()
    {
        if ($billing = $this->getBillingAccount()) {
            return $billing->getIban();
        }

        return "";
    }

    /**
     * Get current customer billing bank name
     * @return string
     */
    public function getBankName()
    {
        if ($billing = $this->getBillingAccount()) {
            return $billing->getBankName();
        }

        return "";
    }

    /**
     * Get current customer billing bank account owner name
     * @return string
     */
    public function getBankAccountOwnerName()
    {
        if ($billing = $this->getBillingAccount()) {
            return $billing->getAccountOwnerName();
        }

        return "";
    }

    /**
     * Get the last saved billing method for current customer. If none set in Omnius then it will be retrieved through RetrieveCustomerInfo service call
     * @return string
     */
    public function getBillingMethod()
    {
        if ($billing = $this->getBillingAccount()) {
            return $billing->getBillingMethod();
        }

        return "";
    }

    /**
     * Get customer birth data
     * @return string
     */
    public function getDob()
    {
        return $this->getData("dob");
    }

    /**
     * Get customer SOHO status
     * @return boolean
     */
    public function getIsSoho()
    {
        return $this->getPartyType() == Dyna_Customer_Helper_Services::PARTY_TYPE_SOHO;
    }

    /**
     * Returns the type of migration a customer might have
     * @return string|false
     */
    public function getInMigrationState()
    {
        return $this->getData('in_migration_state');
    }

    /**
     * Return an array with all campaigns customer is allowed to
     */
    public function getAllowedCampaigns()
    {
        return $this->getData('customerCampaigns') ?: [];
    }

    /**
     * Set serial number on current customer for an owned product
     * @param $productId
     * @param $productSerialNumber
     */
    public function setHardwareSerial($productId, $productSerialNumber)
    {
        $this->hardwareSerials[$productId] = $productSerialNumber;
    }

    /**
     * Retrieve previously set serial hardware for an owned product
     * @param $productId
     * @return string|null
     */
    public function getHardwareSerial($productId)
    {
        return !empty($this->hardwareSerials[$productId]) ? $this->hardwareSerials[$productId] : null;
    }

    /**
     * Set owned smart card on current customer
     * @return $this
     */
    public function setOwnSmartCard(Varien_Object $smartcard)
    {
        $this->ownSmartcard = $smartcard;

        return $this;
    }

    /**
     * Retrieve previously set owned smartcard
     */
    public function getOwnSmartCard()
    {
        return $this->ownSmartcard;
    }

    /**
     * Set owned receiver on current customer
     * @return $this
     */
    public function setOwnReceiver(Varien_Object $receiver)
    {
        $this->ownReceiver = $receiver;

        return $this;
    }

    /**
     * Retrieve previously set owned receiver
     *
     */
    public function getOwnReceiver()
    {
        return $this->ownReceiver;
    }

    /**
     * Clear own receiver data from customer
     * @return $this
     */
    public function clearOwnReceiver() {
        $this->ownReceiver = null;
        return $this;
    }

    /**
     * Clear own smartcard data from customer
     * @return $this
     */
    public function clearOwnSmartcard() {
        $this->ownSmartcard = null;
        return $this;
    }

    public function hasOwnHardware()
    {
        return (bool)(!empty($this->ownReceiver) || !empty($this->ownSmartcard));
    }

    /**
     * This method will set data retrieved from RetrieveCustomerInfo service call
     * Code has been moved to customer helper to avoid model heavy logic
     * @return $this
     */
    public function updateCustomerInfo($data)
    {
        $this->retrievedCustomerInfoData = $data;
        if (!$data['customer_number']) {
            Mage::throwException("No customer id received after executing RetrieveCustomerInfo service call (customer_id key changed?)");
        }

        $this->setData("customer_number", $data['customer_number'] ?: null);

        // Update this model data with details from services
        Mage::helper('dyna_customer/services')->mapRetrievedCustomerInfoToCustomer($this);

        return $this;
    }

    /**
     * Get customer info retrieved from services
     */
    public function getCustomerInfo()
    {
        return $this->retrievedCustomerInfoData;
    }

    /**
     * Show customer link icon in customer panel
     */
    public function showLinkIcon()
    {
        return $this->getLinkId() ? 1 : 0;
    }

    /**
     * Get link id for customer
     * @return mixed
     */
    public function getLinkId()
    {
        return $this->getData('link_id');
    }

    /**
     * Check whether or not a customer is prospect (has saved shopping cart)
     * @todo implement it - customer number needs to be mapped to a customer id
     * @return bool
     */
    public function isProspect()
    {
        return false;
    }

    /**
     * Return the Kvk number for current customer
     * @return mixed
     */
    public function getKvkNumber()
    {
        if (!$this->getCustomerNumber()) {
            return "";
        }

        return $this->getCompany()->getId();
    }
}
