<?php

/**
 * Class Dyna_Customer_Helper_Validation Helper
 */
class Dyna_Customer_Helper_Validation extends Mage_Core_Helper_Abstract
{
    private $errorMessage = [];
    private $error = 0;
    private $response = [];

    const FIRST_NAME_MAX_LENGTH = 8;
    const LAST_NAME_MAX_LENGTH = 30;
    const EMAIL_REGEX = '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';

    /**
     * @param $params
     * @return bool
     */
    public function validateCustomerLegacySearchParameters($params)
    {
        //@TODO
        $this->error = false;
        ($this->error) ? $this->setValidationErrorResponse() : $this->setSuccessResponse($params);
        return $this->response;
    }
    /**
     * @param $params
     * @return bool
     */
    public function validateCustomerSearchParameters($params)
    {
        if (!$this->validateAtLeastOneSearchFieldIsPresent($params)) {
            return $this->response;
        }
        $this->validateCustomerContractOrderNumber($params);
        $this->validatePhoneNumber($params);
        $this->validateCompanyName($params);
        $this->validateFirstName($params);
        $this->validateLastName($params);
        $this->validateBirthDate($params);
        $this->validateStreet($params);
        $this->validateHouseNo($params);
        $this->validateZipCode($params);
        $this->validateCity($params);
        $this->validateAddress($params);
        $this->validateDeviceId($params);
        $this->validateEmail($params);

        ($this->error) ? $this->setValidationErrorResponse() : $this->setSuccessResponse($params);
        return $this->response;
    }

    /**
     * Validate the customer id
     * @param array $param
     * @return array
     */
    public function validateCustomerId($param)
    {
        if(empty($param['customerId'])) {
            $this->errorMessage['customerId'] = $this->__("Customer Id could not be found");
            $this->setValidationErrorResponse();
        }
        else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }

    /**
     * Validate the BAN of a mobile product
     * @param $param
     * @return array
     */
    public function validateBan($param)
    {
        if(empty($param['ban'])) {
            $this->errorMessage['ban'] = $this->__("Please provide the BAN!");
            $this->setValidationErrorResponse();
        }
        else {
            $this->setSuccessResponse($param);
        }
        return $this->response;
    }


    /**
     * Validate the accounts to be active and to exist on the customer with the same kias
     * @param array $accounts
     * @return array
     */
    public function validateAccountStatus($accounts)
    {
        if(empty($accounts)) {
            $this->errorMessage['accounts'] = $this->__("Please provide accounts for retention check!");
            $this->setValidationErrorResponse();
            return $this->response;
        }

        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        $customerProducts = $customerStorage->getCustomerProducts();

        if (!empty($customerProducts) && !empty($customerProducts['Mobile'])) {
            foreach ($accounts as $contractNo => $data) {
                $kias = $data['kiasNo'];
                if(empty($customerProducts['Mobile'][$kias])) {
                    $this->errorMessage['accounts'][$contractNo] = $kias . " " . $this->__("kias number does not exist on this customer!");
                    $this->setValidationErrorResponse();
                    return $this->response;
                }
                else {
                    foreach ($customerProducts['Mobile'][$kias] as $mobileAccount) {
                        if($mobileAccount['contract_nr'] == $contractNo &&
                            $mobileAccount['product_status'] != Dyna_Customer_Helper_Customer::MOBILE_PRODUCT_STATUS_ACTIVE) {
                            $this->errorMessage['accounts'][$contractNo] = $this->__("Not possible: account suspended.");
                        }
                    }
                }
            }

            if(!empty($this->errorMessage)) {
                $this->setErrorResponse();
                return $this->response;
            }
            $this->setSuccessResponse($accounts);
            return $this->response;
        }
        else {
            $this->errorMessage['accounts'] = $this->__("No mobile accounts were found for this customer!");
            $this->setValidationErrorResponse();
            return $this->response;
        }
    }

    /**
     * At least one of the SearchAccount, SearchContact or SearchedEquipment is mandatory
     *
     * @param $params
     * @return bool
     */
    private function validateAtLeastOneSearchFieldIsPresent($params)
    {
        if (count($params) == 0) {
            $this->errorMessage['required'] = $this->__("0 matching results found. Please check the spelling or use more search criteria.");
            $this->setValidationErrorResponse();
            return false;
        }
        return true;
    }

    private function validateCustomerContractOrderNumber($params)
    {
        $this->error = false;
    }

    private function validateCompanyName($params)
    {
        $this->error = false;
    }

    private function validatePhoneNumber($params)
    {
        $this->error = false;
    }

    /**
     * Validate first name
     * - max length 8
     *
     * @param array $params
     */
    private function validateFirstName($params)
    {
        if(!empty($params['first_name']) && strlen($params['first_name']) > self::FIRST_NAME_MAX_LENGTH) {
            $this->errorMessage['first_name'] = $this->__("First name must not exceed 8 characters");
            $this->error = true;
        }
    }
    /**
     * Validate last name
     * - max length 30
     * s
     * @param array $params
     */
    private function validateLastName($params)
    {
        if(!empty($params['last_name']) && strlen($params['last_name']) > self::LAST_NAME_MAX_LENGTH) {
            $this->errorMessage['last_name'] = $this->__("Last name must not exceed 30 characters");
            $this->error = true;
        }
    }

    private function validateBirthDate($params)
    {
        $this->error = false;
    }

    private function validateStreet($params)
    {
        $this->error = false;
    }

    private function validateHouseNo($params)
    {
        $this->error = false;
    }

    private function validateZipCode($params)
    {
        $this->error = false;
    }

    private function validateCity($params)
    {
        $this->error = false;
    }

    private function validateAddress($params)
    {
        $this->error = false;
    }

    private function validateDeviceId($params)
    {
        $this->error = false;
    }

    private function validateEmail($params)
    {
        if(!empty($params['email']) && !preg_match(self::EMAIL_REGEX, $params['email']) ) {
            $this->errorMessage['email'] = $this->__("Email address must be valid");
            $this->error = true;
        }
        $this->error = false;
        
        // todo validate domain
    }

    private function setErrorResponse()
    {
        $this->response = ['validationError' => 0,
            'error' => 1,
            'message' => $this->errorMessage];
    }


    private function setValidationErrorResponse()
    {
        $this->response = ['validationError' => 1,
                            'error' => 1,
            'message' => $this->errorMessage];
    }

    private function setSuccessResponse($params)
    {
        $this->response = ['validationError' => 0,'error' => 0,'params' => $params];
    }
}
