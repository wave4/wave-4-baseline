<?php

/**
 * Class Dyna_Customer_Helper_Client Helper
 */
class Dyna_Customer_Helper_Client extends Mage_Core_Helper_Abstract
{
    const LEGACY_SYSTEM_KIAS = 'KIAS'; // mobile
    const LEGACY_SYSTEM_KDG = 'KDG'; // cable
    const LEGACY_SYSTEM_FN = 'FN'; // fixed
}
