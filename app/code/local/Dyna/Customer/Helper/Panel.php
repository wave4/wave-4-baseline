<?php

class Dyna_Customer_Helper_Panel extends Mage_Core_Helper_Abstract
{
    /**
     * Parses response for requested customer section
     * @param $section
     * @return array
     */
    public function getPanelData($section)
    {
        $result = [];

        switch ($section) {
            case "link-details" : {
                return $this->parseLinkDetails();
                break;
            }
            case "orders-content" : {
                return $this->parseOrdersContent();
                break;
            }
            case "carts-content" : {
                return $this->parseCartsContent();
                break;
            }
            case "products-content" : {
                return $this->getCustomerProducts();
                break;
            }
            default : {
                $result = [
                    "error" => true,
                    "message" => $this->__("No valid section requested for customer panel"),
                ];
                break;
            }
        }

        return $result;
    }

    /**
     * Parse left sidebar link details for a selected customer
     * No customer exists on session check as it is expected to be checked in controller
     * We assume the already linked accounts are part of the getCustomerDetails mode1
     */
    public function parseLinkDetails()
    {
        /** @var Dyna_Customer_Model_Customer  $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        // Calling helper to map services data to frontend data
        $customerData = Mage::helper('dyna_customer/info')->buildCustomerDetails($customer);
        $response['current_customer'] = $customerData;

        //todo when services are available check if the already linked accounts are part of the getCustomerDetails mode1

        /** Preparing response */
        if (!empty($customer['Account'])) {
            foreach ($customer['Account'] as $account) {
                $response['linkedAccounts'][] = [
                    "customer_data" => $account,
                ];
            }
        }
        return $response;
    }

    /**
     * Parse left sidebar orders for a selected customer
     * No customer exists on session check as it is expected to be checked in controller
     */
    public function parseOrdersContent()
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();

        /** @todo Orders will be retrieved from Magento db, so until order implement, we'll return a hardcoded array of orders */
        return [
            "orders" => [
                [
                    "creationDate" => "14-04-2016",
                    "orderNumber" => "1233456",
                    "orderStatus" => "Delivered",
                    "packages" => "1",
                    "KIAS" => "1",
                    "KDG" => "0",
                    "FN" => "1",
                    "salesChannel" => "Telesales"
                ],
                [
                    "creationDate" => "13-04-2016",
                    "orderNumber" => "1233455",
                    "orderStatus" => "Waiting confirmation",
                    "packages" => "1",
                    "KIAS" => "1",
                    "KDG" => "0",
                    "FN" => "0",
                    "salesChannel" => "Telesales"
                ],
                [
                    "creationDate" => "11-04-2015",
                    "orderNumber" => "1233454",
                    "orderStatus" => "Canceled",
                    "packages" => "2",
                    "KIAS" => "0",
                    "KDG" => "1",
                    "FN" => "1",
                    "salesChannel" => "Telesales"
                ],
            ],
        ];
    }

    /**
     * Parse customer panel shopping carts
     * @return array
     */
    public function parseCartsContent()
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('dyna_customer/session')->getCustomer();
        $dbShoppingCarts  = $customer->getShoppingCarts();
        $dbCartsParsed = [];

        foreach ($dbShoppingCarts as $key => $cart) {
            $packages = $cart['packages'];
            foreach ($packages as $keyPack => $package) {
                if($package->getPackageId() && $package->getData('items')) {
                    $packageItemsBySections = $package->getPackagesProductsForSidebarSection($package->getData('items'));
                    $dbCartsParsed[$key]['packages'][$keyPack]['packageItemsBySections'] = $packageItemsBySections;
                    $dbCartsParsed[$key]['packages'][$keyPack]['title'] = $package->getTitle();
                    $packageTotals = $cart['cart']->calculateTotalsForPackage($package->getPackageId());
                    $dbCartsParsed[$key]['packages'][$keyPack]['totals'] = $packageTotals;
                }
            }
        }

        /** @todo Carts will be retrieved from Magento db quotes, so until order implement, we'll return a hardcoded array of carts */
        return [
            "shoppingCarts" => $dbCartsParsed,
            "orders" => [
                [
                    "orderId" => "1233456",
                    "creationDate" => "14-04-2016",
                    "orderType" => "Saved shopping cart",
                    "channel" => "Telesales",
                    "agent" => "Agent Q",
                    "KIAS" => "0",
                    "KDG" => "1",
                    "FN" => "1",
                    "packages" => [
                        0 => [
                            "id" => "1",
                            "subscription_name" => "Red One",
                            "subscription_MSISDN" => "06123456789",
                            "subscription_monthly_price" => "46,00",
                            "subscription_one_time_price" => "-",
                            "device_name" => "iPhone 6s 64 GB Plus",
                            "device_sim" => "8931441234567890123",
                            "device_monthly_price" => "-",
                            "device_one_time_price" => "499,00",
                            "addon_name" => "3 GB Extra",
                            "addon_monthly_price" => "5,00",
                            "addon_one_time_price" => "-",
                        ],
                        1 => [
                            "id" => "2",
                            "subscription_name" => "Red Two",
                            "subscription_MSISDN" => "06123456789",
                            "subscription_monthly_price" => "19,00",
                            "subscription_one_time_price" => "-",
                        ]
                    ],
                    "totals" => [
                        "total_price_monthly_excl_val" => "55,00",
                        "total_price_one_time_excl_val" => "400,00",
                        "total_vat_monthly" => "20,00",
                        "total_vat_one_time" => "99,00",
                        "total_price_monthly" => "75,00",
                        "total_price_one_time" => "499,00",
                    ]
                ],
                [
                    "orderId" => "1233455",
                    "creationDate" => "13-04-2016",
                    "orderType" => "Offer",
                    "channel" => "Telesales",
                    "agent" => "Agent Z",
                    "KIAS" => "0",
                    "KDG" => "1",
                    "FN" => "1",
                ],
                [
                    "orderId" => "1233454",
                    "creationDate" => "11-04-2015",
                    "orderType" => "Saved shopping cart",
                    "channel" => "Telesales",
                    "agent" => "Agent T",
                    "KIAS" => "0",
                    "KDG" => "1",
                    "FN" => "1",
                    "packages" => [
                        0 => [
                            "id" => "1",
                            "subscription_name" => "Red One",
                            "subscription_MSISDN" => "06123456789",
                            "subscription_monthly_price" => "46,00",
                            "subscription_one_time_price" => "-",
                            "device_name" => "iPhone 6s 64 GB Plus",
                            "device_sim" => "8931441234567890123",
                            "device_monthly_price" => "-",
                            "device_one_time_price" => "499,00",
                            "addon_name" => "3 GB Extra",
                            "addon_monthly_price" => "5,00",
                            "addon_one_time_price" => "-",
                        ],
                        1 => [
                            "id" => "2",
                            "subscription_name" => "Red Two",
                            "subscription_MSISDN" => "06123456789",
                            "subscription_monthly_price" => "19,00",
                            "subscription_one_time_price" => "-",
                        ]
                    ],
                    "totals" => [
                        "total_price_monthly_excl_val" => "55,00",
                        "total_price_one_time_excl_val" => "400,00",
                        "total_vat_monthly" => "20,00",
                        "total_vat_one_time" => "99,00",
                        "total_price_monthly" => "75,00",
                        "total_price_one_time" => "499,00",
                    ]
                ],
            ],
        ];
    }

    /*
    * Return the customer products after a service call to getCustomerDetails Mode2
    */
    private function getCustomerProducts()
    {
        /** @var Dyna_Customer_Model_Session $customerStorage */
        $customerStorage = Mage::getSingleton('dyna_customer/session');
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $customerStorage->getCustomer();
        $customerProductsSearchParam = [];
        if ($customer->getLinkId()) {
            $customerProductsSearchParam["LinkId"] = $customer->getLinkId();
        } else {
            $customerProductsSearchParam["BanKias"] = $customer->getBanKias();
        }

        try {
            /** @var Dyna_Customer_Model_Client $customerSearchClient */
            $customerSearchClient = Mage::getModel('dyna_customer/client')->getClient();
            $data = $customerSearchClient->executeGetProductsCustomerDetails($customerProductsSearchParam);
            // if the service did not respond with success -> display a message
            if (!$data['Success']) {
                $result = [
                    "error" => true,
                    "message" => $this->__("There was a problem with the loading of customer products . Please try again!"),
                ];
                return $result;
            }

            $data = $this->parseProductsResults($data);
            $customerStorage->setCustomerProducts($data);
            return $data;

        } catch (\Exception $e) {

            $customerStorage->clearCustomerProducts();
            var_dump($e->getMessage());
            $result = [
                "error" => true,
                "message" => $this->__("There was a problem with the loading of customer products . Please try again!"),
            ];
            return $result;
        }
    }

    private function parseProductsResults($data)
    {
        $parsedCustomerProducts = [Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE => [],
            Dyna_Customer_Helper_Customer::ACCOUNT_CABLE => [],
            Dyna_Customer_Helper_Customer::ACCOUNT_FIXED => []
        ];

        foreach ($data['customer_products'] as $key => $product) {
            if (ucfirst($product['account']) == Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE) {
                if(empty($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE][$product['kias']])) {
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE][$product['kias']][0] = $product;
                }
                else {
                    $noAccountsForThisKias = count($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE][$product['kias']]);
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_MOBILE][$product['kias']][$noAccountsForThisKias] = $product;
                }
            }
            else if (ucfirst($product['account']) == Dyna_Customer_Helper_Customer::ACCOUNT_CABLE) {
                if(empty($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_CABLE][$product['kias']])) {
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_CABLE][$product['kias']][0] = $product;
                }
                else {
                    $noAccountsForThisKias = count($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_CABLE][$product['kias']]);
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_CABLE][$product['kias']][$noAccountsForThisKias] = $product;
                }
            }
            else {
                if(empty($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_FIXED][$product['kias']])) {
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_FIXED][$product['kias']][0] = $product;
                }
                else {
                    $noAccountsForThisKias = count($parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_FIXED][$product['kias']]);
                    $parsedCustomerProducts[Dyna_Customer_Helper_Customer::ACCOUNT_FIXED][$product['kias']][$noAccountsForThisKias] = $product;
                }
            }
        }
        
        return $parsedCustomerProducts;
    }
}
