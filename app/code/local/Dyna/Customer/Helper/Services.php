<?php

/**
 * Map customer data from RetrieveCustomerInfo service call to magento customer model / session model
 * Class Dyna_Customer_Helper_Services
 */

class Dyna_Customer_Helper_Services extends Mage_Core_Helper_Abstract
{
    CONST CODE_CABLE = "Cable";
    CONST CODE_DSL = "Dsl";
    CONST CODE_MOBILE = "Mobile";

    CONST DUNNING_IN = "IN_DUNNING";
    CONST DUNNING_NO = "NO_DUNNING";

    CONST ACCOUNT_CATEGORY_KIAS = "KIAS";
    CONST ACCOUNT_CATEGORY_FN = "FN";
    CONST ACCOUNT_CATEGORY_KD = "KD";
    CONST ACCOUNT_CATEGORY_ALL = "ALL";

    CONST PARTY_TYPE_PRIVATE = "PRIVATE";
    CONST PARTY_TYPE_SOHO = "SOHO";

    CONST ADDRESS_TYPE_LEGAL = "L";
    CONST ADDRESS_TYPE_BILLING = "B";
    CONST ADDRESS_TYPE_SHIPPING = "S";

    CONST PHONE_PRIVATE = "PRIVATE";
    CONST PHONE_WORK = "WORK";


    /**
     * Return an array containing valid dunning statuses for retrieve customer info service call
     * @return array
     */
    public static function validDunningStatuses()
    {
        return [
            self::DUNNING_NO,
            self::DUNNING_IN,
        ];
    }

    /**
     * Return an array containing valid account categories for retrieve customer info service call
     * @return array
     */
    public static function validAccountCategories()
    {
        return [
            self::ACCOUNT_CATEGORY_KIAS,
            self::ACCOUNT_CATEGORY_FN,
            self::ACCOUNT_CATEGORY_KD,
            self::ACCOUNT_CATEGORY_ALL,
        ];
    }

    public static function validPartyTypes()
    {
        return [
            self::PARTY_TYPE_PRIVATE,
            self::PARTY_TYPE_SOHO,
        ];
    }

    protected $flatArray = null;
    protected $validCustomerLifeCycleStatuses = [
        "AC", //Active
        "IN", //Inactive
        "CE", //Cancelled
        "T", //Tentative
        "EN", //Customer in creation
    ];
    protected $validCustomerStatuses = [
        "O", //Open
        "C", //Closed
        "S", //Suspended
        "N", //Cancelled
        "T", //Tentative
    ];
    protected $validCancellationNoticeUnits = [
        // @todo clarify what and when are these units used as they are not detailed in RetrieveCustomerInfo service definition
        "D", //Days?
        "W", //Weeks?
        "M", //Months?
        "Y", //Years?
    ];
    protected $validSubscriberValues = [
        // @todo clarify what valid subscriber values used for
        "R", //?
        "S", //?
        "G", //?
        "P", //?
        "T", //?
        "B", //?
    ];

    public function mapRetrievedCustomerInfoToCustomer(Dyna_Customer_Model_Customer $customer)
    {
        $servicesInfo = $customer->getCustomerInfo();

        // Customer data is set on the first key of <Customer> node
        $customerData = $servicesInfo["Customer"][0];

        // Make a one-dimension array
        $this->flatArray = $this->buildFlatArray($customerData);

        /**********************************************************
         * PARSING COMPANY DATA
         *********************************************************/
        $company = [];
        // Name of the company
        $company["registration_name"] = $this->getArrayValue('[PartyLegalEntity][RegistrationName]');
        // Mapping registration_name to company_name customer model attribute
        $company["company_name"] = $company["registration_name"];
        // The company registration number
        $company["id"] = $this->getArrayValue('[PartyLegalEntity][CompanyID]');
        // Type of registry where companies are registered
        $company["legal_form"] = $this->getArrayValue('[PartyLegalEntity][CompanyLegalForm]');
        // City of registration
        $company["city"] = $this->getArrayValue('[PartyLegalEntity][RegistrationAddress][CityName]');
        // Contact name (CompanyDetails.ContactPerson)
        $company["contact_name"] = $this->getArrayValue('[Contact][Name]');
        // Contact e-mail (CompanyDetails.Email)
        $company["contact_email"] = $this->getArrayValue('[Contact][ElectronicMail]');
        // Adding company data as a Varien_Object

        // Retrieve current customer company by calling $customerSession->getCustomer()->getCompany();
        $companyObject = new Varien_Object();
        $companyObject->addData($company);
        $customer->setData('company', $companyObject);
        /**********************************************************
         * EOF PARSING COMPANY DATA
         *********************************************************/

        /**********************************************************
         * PARSING CUSTOMER DATA
         *********************************************************/
        $customerMapping = [];
        $customerMapping["link_id"] = $this->getArrayValue('[PartyIdentification][ID]');
        // For user authentication purposes
        $customerMapping["password"] = $this->getArrayValue('[CustomerPassword]');
        // PrivateCustomerInfo.CustomerStatus
        $customerMapping["life_cycle_status"] = $this->getArrayValue('[CustomerLifeCycleStatus]');
        // Cardinality 0..1 - so check is made only against the returned value
        if ($customerMapping["life_cycle_status"] && !in_array($customerMapping["life_cycle_status"], $this->validCustomerLifeCycleStatuses)) {
            $this->logException("Invalid customer life cycle status <" . $customerMapping["life_cycle_status"] . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
        }
        // Cardinality 0..1 - so check is made only against the returned value
        $customerMapping["customer_status"] = $this->getArrayValue('[CustomerStatus]');
        if ($customerMapping["customer_status"] && !in_array($customerMapping["customer_status"], $this->validCustomerStatuses)) {
            $this->logException("Invalid customer status  <" . $customerMapping["customer_status"] . ">  received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
        }
        // Cardinality 0..2 - so check is made only against the returned value
        // @todo clarify DunningStatus response value because, as cardinality from definition document states, an array containing two statuses is presented which is abnormal
        $customerMapping["dunning_status"] = $this->getArrayValue('[DunningStatus]');
        if ($customerMapping["dunning_status"] && !in_array($customerMapping["dunning_status"], self::validDunningStatuses())) {
            $this->logException("Invalid dunning status <" . $customerMapping["dunning_status"] . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
        }
        // Customer account category is mandatory
        $customerMapping["account_category"] = $this->getArrayValue("[AccountCategory]");
        if (!in_array($customerMapping["account_category"], self::validAccountCategories())) {
            $this->logException("Invalid account category <" . $customerMapping["account_category"] ?: "none" . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
        }
        // Customer party type is mandatory
        $customerMapping["party_type"] = $this->getArrayValue("[PartyType]");
        if (!in_array($customerMapping["party_type"], self::validPartyTypes())) {
            $this->logException("Invalid party type <" . $customerMapping["party_type"] ?: "none" . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
        }
        // Customer might have multiple parent account numbers (cardinality 0..N)
        $customerMapping["parent_account_numbers"] = [];
        $parentAccountNumbers = $this->getCertainNode($customerData, "[ParentAccountNumber]");
        if (!empty($parentAccountNumbers)) {
            foreach ($parentAccountNumbers as $accountId => $accountNumber) {
                $customerMapping["parent_account_numbers"][$accountId] = $accountNumber;
            }
        }
        // Customer phone numbers
        $customerMapping["contact_phone_numbers"] = [];
        $contactPhoneNumbers = $this->getCertainNode($customerData, "[ContactPhoneNumber]");
        if (!empty($contactPhoneNumbers)) {
            foreach ($contactPhoneNumbers as $phoneNumber) {
                $phoneData["country_code"] = $phoneNumber["CountryCode"];
                $phoneData["local_area_code"] = $phoneNumber["LocalAreaCode"];
                // In case when CountryCode and AreaCode fields are empty this field contains FullPhoneNumber.
                $phoneData["phone_number"] = $phoneNumber["PhoneNumber"];
                $phoneData["type"] = $phoneNumber["Type"];
                $phone = new Varien_Object();
                $phone->addData($phoneData);
                $customerMapping["contact_phone_numbers"][] = $phone;
            }
        }

        // Role[Person] - Contains the private customer info - cardinality 1
        if (empty($this->getCertainNode($customerData, "[Role]"))) {
            $this->logException("No private customer details (role/person) received for customer: " . $servicesInfo["customer_number"]);
        }
        $customerMapping["firstname"] = $this->getArrayValue("[Role][Person][FirstName]");
        $customerMapping["lastname"] = $this->getArrayValue("[Role][Person][FamilyName]");
        $customerMapping["title"] = $this->getArrayValue("[Role][Person][Title]");
        $customerMapping["nationality_id"] = $this->getArrayValue("[Role][Person][NationalityID]");
        $customerMapping["dob"] = $this->getArrayValue("[Role][Person][BirthDate]");
        $customerMapping["salutation"] = $this->getArrayValue("[Role][Person][Salutation]");
        $customer->addData($customerMapping);
        /**********************************************************
         * EOF PARSING CUSTOMER DATA
         *********************************************************/

        /**********************************************************
         * PARSING CUSTOMER CONTRACTS
         *********************************************************/
        $customerContracts = [];
        $contractData = $this->getCertainNode($customerData, "[Contract]");
        //Expecting an array of contracts
        if (!isset($contractData[0])) {
            $contractData = [$contractData];
        }
        foreach ($contractData as $contract) {
            $parsedContractData = [];
            $flatContractData = $this->buildFlatArray($contract);
            // Parsing general contract data
            $parsedContractData["id"] = $this->getArrayValue("[ID]", $flatContractData);
            $parsedContractData["contract_type"] = $this->getArrayValue("[ContractType]", $flatContractData);
            // Contract Start Date
            $parsedContractData["start_date"] = $this->getArrayValue("[ValidityPeriod][StartDate]", $flatContractData);
            // Contract End Date
            $parsedContractData["end_date"] = $this->getArrayValue("[ValidityPeriod][EndDate]", $flatContractData);
            // First Activation Date
            $parsedContractData["activation_date"] = $this->getArrayValue("[ActivationDate]", $flatContractData);
            // Next Possible cancellation date
            $parsedContractData["possible_cancellation_date"] = $this->getArrayValue("[PossibleCancellationDate]", $flatContractData);
            // Cancellation Notice Period
            $parsedContractData["cancellation_notice_period"] = $this->getArrayValue("[CancellationNoticePeriod]", $flatContractData);
            // Cancellation Notice Unit (D,W,M,Y)
            $parsedContractData["cancellation_notice_unit"] = $this->getArrayValue("[CancellationNoticeUnit]", $flatContractData);
            if ($parsedContractData["cancellation_notice_unit"] && !in_array($parsedContractData["cancellation_notice_unit"], $this->validCancellationNoticeUnits)) {
                $this->logException("Invalid cancellation notice unit <" . $parsedContractData["cancellation_notice_unit"] ?: "none" . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
            }
            $parsedContractData["last_notification_date"] = $this->getArrayValue("[LastNotificationdate]", $flatContractData);

            // Adding subscription to current contract
            // @todo Example contains two subscription nodes - which still needs to be clarified as for a contract there should be only one subscription
            $subscription = $this->getCertainNode($contract, "[Subscription]");
            if (isset($subscription[0])) {
                $subscription = $subscription[0];
            }
            $subscriptionData = [];
            $subscriptionFlatData = $this->buildFlatArray($subscription);
            // Reference of the Customer ID in the backend.
            $subscriptionData["customer_id"] = $this->getArrayValue("[ID]", $subscriptionFlatData);
            // Reference of the Customer ID in the backend.
            $subscriptionData["cnt"] = $this->getArrayValue("[Ctn]", $subscriptionFlatData);
            // Subscriber Value (R,S,G,P,T,B)
            $subscriptionData["value_indicator"] = $this->getArrayValue("[Valueindicator]", $subscriptionFlatData);
            if ($subscriptionData["value_indicator"] && !in_array($subscriptionData["value_indicator"], $this->validSubscriberValues)) {
                $this->logException("Invalid value indicator <" . $subscriptionData["value_indicator"] ?: "none" . "> received on RetrieveCustomerInfo service call for customer: " . $servicesInfo["customer_number"]);
            }
            $subscriptionData["puk"] = $this->getArrayValue("[PUK]", $subscriptionFlatData);

            // Building addresses for current subscription
            $subscriptionData["addresses"] = [];
            $subscriptionAddresses = $this->getCertainNode($subscription, "[Address]");
            foreach ($subscriptionAddresses as $addressArray) {
                $addressArrayFlat = $this->buildFlatArray($addressArray);
                $addressData = [];
                // Unique address ID
                $addressData["id"] = $addressArray["ID"];
                // Type of address (Billing, Legal etc)
                $addressData["address_type"] = $this->getArrayValue("[AddressTypeCode]", $addressArrayFlat);
                $addressData["postcode"] = $this->getArrayValue("[PostalZone]", $addressArrayFlat);
                $addressData["post_box"] = $this->getArrayValue("[Postbox]", $addressArrayFlat);
                $addressData["street"] = $this->getArrayValue("[StreetName]", $addressArrayFlat);
                $addressData["house_number"] = $this->getArrayValue("[BuildingNumber]", $addressArrayFlat);
                $addressData["addition"] = $this->getArrayValue("[Addition]", $addressArrayFlat);
                $addressData["city"] = $this->getArrayValue("[City]", $addressArrayFlat);
                $addressData["district"] = $this->getArrayValue("[District]", $addressArrayFlat);
                $addressData["state"] = $this->getArrayValue("[CountrySubentity]", $addressArrayFlat);
                $addressData["country"] = $this->getArrayValue("[Country]", $addressArrayFlat);

                $addressObject = new Varien_Object();
                $addressObject->addData($addressData);
                $subscriptionData["addresses"][] = $addressObject;
            }
            // EOF building addresses for current subscription

            // Building products for current contract
            $subscriptionProducts = $this->getCertainNode($subscription, "[Product]");
            // Expecting an array of products as response
            if (!isset($subscriptionProducts[0])) {
                $subscriptionProducts = [$subscriptionProducts];
            }
            $subscriptionProductsData = [];
            $subscriptionProductsData[self::CODE_CABLE] = [];
            $subscriptionProductsData[self::CODE_DSL] = [];
            $subscriptionProductsData[self::CODE_MOBILE] = [];
            foreach ($subscriptionProducts as $product) {
                $productFlat = $this->buildFlatArray($product);
                $productData = [];
                $product["description"] = $this->getArrayValue("[Description]", $productFlat);
                $product["name"] = $this->getArrayValue("[Name]", $productFlat);
                // Product code, ID
                $product["code"] = $this->getArrayValue("[StandardItemIdentification][ID]", $productFlat);
                // Line of business: Cable, Mobile, DSL
                $product["nature_code"] = $this->getArrayValue("[CommodityClassification][NatureCode]", $productFlat);
                $product["information_content_provider_party"] = $this->getArrayValue("[InformationContentProviderParty][PartyIdentification][ID]", $productFlat);
                // Product status
                $product["status"] = $this->getArrayValue("[Status][Text]", $productFlat);
                // Component, expected to be only one
                $productComponent = $this->getCertainNode($product, "[Component]");
                // @todo clarify component status as in response example there are multiple components for a given product
                if (isset($productComponent[0])) {
                    $productComponent = $productComponent[0];
                }
                $productComponentFlat  = $this->buildFlatArray($productComponent);
                $productComponentData = [];
                $productComponentData["type"] = $this->getArrayValue("[Type]", $productComponentFlat);
                $productComponentData["tariff_option"] = $this->getArrayValue("[TariffOption]", $productComponentFlat);
                $productComponentData["tariff_option_description"] = $this->getArrayValue("[TariffOptionDescription]", $productComponentFlat);
                $productComponentData["tariff_option_name"] = $this->getArrayValue("[TariffOptionName]", $productComponentFlat);
                $productComponentData["tariff"] = $this->getArrayValue("[Tariff]", $productComponentFlat);
                $productComponentData["tariff_description"] = $this->getArrayValue("[TariffDescription]", $productComponentFlat);
                $productComponentData["agreement_soc"] = $this->getArrayValue("[AgreementSOC]", $productComponentFlat);
                $productComponentData["agreement_soc_description"] = $this->getArrayValue("[AgreementSOCDescription]", $productComponentFlat);
                $productComponentData["agreement_service_type"] = $this->getArrayValue("[AgreementServiceType]", $productComponentFlat);
                $productComponentData["agreement_effective_date"] = $this->getArrayValue("[AgreementEffectiveDate]", $productComponentFlat);
                $productComponentData["agreement_expiration_date"] = $this->getArrayValue("[AgreementExpirationDate]", $productComponentFlat);
                $productComponentData["agreement_tariff_option"] = $this->getArrayValue("[AgreementTariffOption]", $productComponentFlat);
                $productComponentData["contract_type"] = $this->getArrayValue("[ContractType]", $productComponentFlat);
                $productComponentData["commit_end_date"] = $this->getArrayValue("[CommitEndDate]", $productComponentFlat);
                $productComponentData["end_grace_period_date"] = $this->getArrayValue("[EndGracePeriodDate]", $productComponentFlat);
                $productComponentData["initial_period"] = $this->getArrayValue("[initialPeriod]", $productComponentFlat);
                $productComponentData["additional_period"] = $this->getArrayValue("[additionalPeriod]", $productComponentFlat);
                $productComponentData["initial_cnl_notice_unit"] = $this->getArrayValue("[initialCNLNoticeUnit]", $productComponentFlat);
                $productComponentData["initial_cnl_notice_period"] = $this->getArrayValue("[initialCNLNoticePeriod]", $productComponentFlat);
                $productComponentData["additional_cnl_notice_unit"] = $this->getArrayValue("[additionalCNLNoticeUnit]", $productComponentFlat);
                $productComponentData["additional_cnl_notice_period"] = $this->getArrayValue("[additionalCNLNoticePeriod]", $productComponentFlat);
                $productComponentData["notice_period"] = $this->getArrayValue("[noticePeriod]", $productComponentFlat);
                $productComponentData["extended_notice_period"] = $this->getArrayValue("[extendedNoticePeriod]", $productComponentFlat);
                $productComponentData["billing_frequency"] = $this->getArrayValue("[billingFrequency]", $productComponentFlat);
                $productComponentData["non_standard_rate"] = $this->getArrayValue("[nonStandardRate]", $productComponentFlat);
                $productComponentData["country"] = $this->getArrayValue("[Country]", $productComponentFlat);
                $productComponentData["device_id"] = $this->getArrayValue("[DeviceID]", $productComponentFlat);
                $productComponentData["tn"] = $this->getArrayValue("[TN]", $productComponentFlat);
                $productComObject = new Varien_Object();
                $productComObject->addData($productComponentData);
                // Add component data to the corresponding type node
                $subscriptionProductsData[$productComObject->getType()][] = $productComponentData;
            }
            $subscriptionData['products'] = $subscriptionProductsData;
            // EOF building products for current contract

            // Building contacts for current contract
            $subscriptionData['contacts'] = [];
            $subscriptionContacts = $this->getCertainNode($subscription, "[Contact]");
            // Ensure there is a list of contacts
            if (!isset($subscriptionContacts[0])) {
                $subscriptionContacts = [$subscriptionContacts];
            }
            foreach ($subscriptionContacts as $subscriptionContact) {
                $subscriptionContactFlat = $this->buildFlatArray($subscriptionContact);
                $subscriptionContactData = [];
                $subscriptionContactData["party_id"] = $this->getArrayValue("[PartyIdentification][ID]", $subscriptionContactFlat);
                $subscriptionContactData["party_type"] = $this->getArrayValue("[PartyType]", $subscriptionContactFlat);
                $subscriptionContactData["party_name"] = $this->getArrayValue("[PartyName]", $subscriptionContactFlat);
                $subscriptionContactData["party_legal_entity"] = $this->getArrayValue("[PartyLegalEntity][RegistrationName]", $subscriptionContactFlat);
                $subscriptionContactData["parent_account_number"] = $this->getArrayValue("[ParentAccountNumber][ID]", $subscriptionContactFlat);
                $subscriptionContactData["party_phone_number"] = $this->getArrayValue("[ContactPhoneNumber][PhoneNumber]", $subscriptionContactFlat);
                $subscriptionContactData["party_email"] = $this->getArrayValue("[Contact][ElectronicMail]", $subscriptionContactFlat);

                // Adding role (person) to current company
                if (isset($subscriptionContact["Role"]["Person"])) {
                    $subscriptionContactDataRole = $this->getCertainNode($subscriptionContact, "[Role][Person]");
                    $subscriptionContactDataRoleFlat = $this->buildFlatArray($subscriptionContactDataRole);
                    $subscriptionContactDataRoleData = [];
                    $subscriptionContactDataRoleData["firstname"] = $this->getArrayValue("[FirstName]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["lastname"] = $this->getArrayValue("[FamilyName]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["nationality_id"] = $this->getArrayValue("[NationalityID]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["dob"] = $this->getArrayValue("[BirthDate]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["email"] = $this->getArrayValue("[Contact][ElectronicMail]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["salutation"] = $this->getArrayValue("[Salutation]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleData["status"] = $this->getArrayValue("[Status][Text]", $subscriptionContactDataRoleFlat);
                    $subscriptionContactDataRoleObject = new Varien_Object();
                    $subscriptionContactDataRoleObject->addData($subscriptionContactDataRoleData);
                    $subscriptionContactData["contact_person"] = $subscriptionContactDataRoleObject;
                    // Eof adding role (person) to current company
                }

                $subscriptionContactObject = new Varien_Object();
                $subscriptionContactObject->addData($subscriptionContactData);
                $subscriptionData['contacts'][] = $subscriptionContactObject;
            }

            $subscriptionDataObject = new Varien_Object();
            $subscriptionDataObject->addData($subscriptionData);
            $customerContracts['subscriptions'] = $subscriptionDataObject;
            // EOF building contacts for current contract

            $contractsObject = new Varien_Object();
            $contractsObject->addData($customerContracts);

            $customer->setData('contracts', $contractsObject);
        }
        /**********************************************************
         * EOF PARSING CUSTOMER CONTRACTS
         *********************************************************/

        /**********************************************************
         * PARSING BILLING ACCOUNT FOR CURRENT CUSTOMER
         *********************************************************/
        $billingAccount = $this->getCertainNode($customerData, "[BillingAccount]");
        $billingAccountFlat = $this->buildFlatArray($billingAccount);
        $billingAccountData = [];
        $billingAccountData["iban"] = $this->getArrayValue("[ID]", $billingAccountFlat);
        $billingAccountData["bank_code"] = $this->getArrayValue("[FinancialInstitutionBranch][ID]", $billingAccountFlat);
        $billingAccountData["bank_name"] = $this->getArrayValue("[FinancialInstitutionBranch][Name]", $billingAccountFlat);
        $billingAccountData["legacy_id"] = $this->getArrayValue("[PartyIdentyfication][ID]", $billingAccountFlat);
        $billingAccountData["back_end_customer_id"] = $this->getArrayValue("[BackEndCustomerID]", $billingAccountFlat);
        $billingAccountData["account_owner_name"] = $this->getArrayValue("[AccountOwnerName]", $billingAccountFlat);
        $billingAccountData["payment_method"] = $this->getArrayValue("[PaymentMethod]", $billingAccountFlat);
        $billingAccountData["billing_method"] = $this->getArrayValue("[BillingType]", $billingAccountFlat);
        $billingAccountObject = new Varien_Object();
        $billingAccountObject->addData($billingAccountData);
        $customer->setData("billing_account", $billingAccountObject);
        /**********************************************************
         * EOF PARSING BILLING ACCOUNT FOR CURRENT CUSTOMER
         *********************************************************/

        /**********************************************************
         * PARSING BILLING DETAILS FOR CURRENT CUSTOMER
         *********************************************************/
        $billingDetails = $this->getCertainNode($customerData, "[BillingDetails]");
        $billingDetailsFlat = $this->buildFlatArray($billingDetails);
        $billingDetailsData = [];
        $billingDetailsData["last_billing_amount"] = $this->getArrayValue("[LastBillAmount]", $billingDetailsFlat);
        $billingDetailsData["last_billing_due_amount"] = $this->getArrayValue("[LastBillDueAmount]", $billingDetailsFlat);
        $billingDetailsData["last_billing_backout_data"] = $this->getArrayValue("[LastBillBackoutDate]", $billingDetailsFlat);
        $billingDetailsData["dunning_letter_amount"] = $this->getArrayValue("[DunningLetterAmount]", $billingDetailsFlat);
        $billingDetailsData["ar_balance"] = $this->getArrayValue("[arBalance]", $billingDetailsFlat);
        $billingDetailsObject = new Varien_Object();
        $billingDetailsObject->addData($billingDetailsData);
        $customer->setData("billing_details", $billingDetailsObject);
        /**********************************************************
         * EOF PARSING BILLING DETAILS FOR CURRENT CUSTOMER
         *********************************************************/



    }

    /**
     * Retrieve a certain value from a flattened keys array (Ex: $data["[Party][PartyName]"])
     * @param $key
     * @param null $dataArray
     * @return null
     */
    public function getArrayValue($key, $dataArray = null)
    {
        if ($dataArray) {
            if (isset($dataArray[$key])) {
                return $dataArray[$key];
            }
        } else {
            if (isset($this->flatArray[$key])) {
                return $this->flatArray[$key];
            }
        }

        return null;
    }

    /**
     * Retrieve all values from a certain node
     * @param $arrayData
     * @param $flatPath [Node1][Node2][Node3][etc]
     * @return mixed
     */
    public function getCertainNode($arrayData, $flatPath)
    {
        $stuff = preg_split("/[\[\]]/", $flatPath);
        $nodes = array_filter($stuff);

        if (is_array($nodes)) {
            foreach ($nodes as $key) {
                $key = str_replace(["[","]"], "", $key);
                //A chunk from arrayData has been retrieved
                if (isset($resultArray)) {
                    if (!isset($resultArray[$key])) {
                        // Node does not exist
                        unset ($resultArray);
                        break;
                    } else {
                        //Retrieving chunk from result array
                        $resultArray = $resultArray[$key];
                    }
                } else {
                    //Retrieving chunk from arrayData
                    $resultArray = $arrayData[$key];
                }
            }
        }

        //The return value will be last found node (which should not be null if found)
        if (isset($resultArray)) {
            return $resultArray;
        } else {
            return null;
        }
    }

    /**
     * Iterates through each array value and sub value and returns an array containing the variable path as key
     * and the fields value as value
     * @param $arrayData
     * @return array
     */
    public function buildFlatArray($arrayData, $currentKey = "", &$result = [])
    {
        if (!empty($arrayData)) {
            foreach ($arrayData as $key => $value) {
                //First key should not be encapsulated in brackets
                $extendedKey = !empty($currentKey) ? $currentKey . "[" . $key . "]" : "[" . $key . "]";
                //If it's an array, go deeper
                if (is_array($value)) {
                    $this->buildFlatArray($value, $extendedKey, $result);
                } else {
                    $result[$extendedKey] = $value;
                }
            }
        } else {
            // Current value might be an empty array
            $result[$currentKey] = "";
        }

        return $result;
    }

    protected function logException($message) {
        Mage::log($message);
    }
}
