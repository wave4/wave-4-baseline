<?php

/**
 * Class Dyna_Customer_Helper_Search Helper
 */
class Dyna_Customer_Helper_Search extends Mage_Core_Helper_Abstract
{

    public function parseCustomerSearchLegacyParams($params)
    {
        $searchParams = [];
        foreach ($params['data'] as $key => $parameter) {
            if (!empty(trim($parameter['value']))) {
                $searchParams[$parameter['name']] = $parameter['value'];
            }
        }

        /**
         * @var $validationHelper Dyna_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('dyna_customer/validation');
        return $validationHelper->validateCustomerLegacySearchParameters($searchParams);
    }
    /**
     * Parsing the form input search fields and validating them;
     * If the fields are not valid => the error messages will be returned
     *
     * @param array $params
     * @return array
     */
    public function parseCustomerSearchParams($params)
    {
        $searchParams = [];
        foreach ($params['data'] as $key => $parameter) {
            if (!empty(trim($parameter['value']))) {
                $searchParams[$parameter['name']] = $parameter['value'];
            }
        }

        /**
         * @var $validationHelper Dyna_Customer_Helper_Validation
         */
        $validationHelper = Mage::helper('dyna_customer/validation');
        // todo validate the parameters
        return $validationHelper->validateCustomerSearchParameters($searchParams);
    }

    /**
     * Parse the search customer results from the service and return only an array that contains the customer accounts
     * @param array $customersResult
     * @return array
     */
    public function parseCustomerSearchResults($customersResult)
    {
        $parsedCustomerResults = [];
        // even if the results is with a single account -> add it to an array for consistency
        if (!empty($customersResult['Success']) && $customersResult['Success']) {
            if (!empty($customersResult['Account']['PartyName'])) {
                $parsedCustomerResults[] = $customersResult['Account'];
            } else {
                $parsedCustomerResults = $customersResult['Account'];
            }
        }

        return $parsedCustomerResults;
    }

    public function parseCustomerSearchAndGroupLinkedAccounts($customerResult)
    {
        $parsedCustomerResults = $this->parseCustomerSearchResults($customerResult);
        $linkedAccounts = [];

        // set the linked accounts as a set of arrays in key ['linked'] on the first array that was found with a certain contact.id
        foreach ($parsedCustomerResults as $key => $customer) {
            if (!isset($linkedAccounts[$customer['Contact']['ID']])) {
                $linkedAccounts[$customer['Contact']['ID']] = ['linkedId' => $customer['Role']['Person']['ID'], 'accountKey' => $key];
                $parsedCustomerResults[$key]['isLinkedChild'] = false;
            } else {
                $keyLinkedAccounts = (!empty($parsedCustomerResults[$linkedAccounts[$customer['Contact']['ID']]['accountKey']]['linked'])) ?
                    count($parsedCustomerResults[$linkedAccounts[$customer['Contact']['ID']]['accountKey']]['linked']) : 0;
                $parsedCustomerResults[$linkedAccounts[$customer['Contact']['ID']]['accountKey']]['linked'][$keyLinkedAccounts] = $parsedCustomerResults[$key];
                $parsedCustomerResults[$key]['isLinkedChild'] = true;
            }
        }

        // sort the customers to have the linked customers ordered and grouped
        usort($parsedCustomerResults, function ($a, $b) {
            return $a['Contact']['ID'] > $b['Contact']['ID'];
        });

        return ['customers' => $parsedCustomerResults, 'countUnlinkedCustomers' => count($linkedAccounts)];
    }
}
