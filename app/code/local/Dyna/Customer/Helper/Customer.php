<?php

/**
 * Class Dyna_Customer_Helper_Customer Helper
 */
class Dyna_Customer_Helper_Customer extends Mage_Core_Helper_Abstract
{
    const MOBILE_PRODUCT_STATUS_ACTIVE = 'Active';
    const MOBILE_PRODUCT_STATUS_SUSPENDED = 'Suspended';
    const MOBILE_PRODUCT_STATUS_RESERVED = 'Reserved';
    const MOBILE_PRODUCT_STATUS_CANCELLED = 'Cancelled';

    const CABLE_PRODUCT_STATUS_ACTIVE = 'Active';
    const CABLE_PRODUCT_STATUS_INTERESTED = 'Interested';
    const CABLE_PRODUCT_STATUS_NEW = 'New';
    const CABLE_PRODUCT_STATUS_BARRED = 'Barred';
    const CABLE_PRODUCT_STATUS_INACTIVE = 'Inactive';

    const FIXED_PRODUCT_STATUS_ACTIVE = 'Active';
    const FIXED_PRODUCT_STATUS_SUSPENDED = 'Suspended';

    const MOBILE_PROLONGATION_TYPE_NEW_PHONE_EVERY_YEAR = "New phone every year";
    const MOBILE_PROLONGATION_TYPE_CONTRACT_EXTENSION_WITHOUT_PAYMENT = "Extension of the contract without payment";
    const MOBILE_PROLONGATION_TYPE_STARNDARD_CONTRACT_EXTENSION = "Standard contract extension";

    const MOBILE_CONTRACT_TYPE_PREPAID = "Prepaid";
    const MOBILE_CONTRACT_TYPE_POSTPAID = "Postpaid";

    const ACCOUNT_MOBILE = "Mobile";
    const ACCOUNT_CABLE = "Cable";
    const ACCOUNT_FIXED = "Fixed";

    const CUSTOMER_LINK_STATUS_DRAFT = "Draft";
    const CUSTOMER_LINK_STATUS_ACTIVE = "Active";

    const SUBSCRIPTION_NEW_PHONE_EVERY_YEAR = "NPEY";
    const SUBSCRIPTION_EXTENSION_OF_THE_CONTRACT_WITHOUT_PAYMENT = "ECWP";
    const SUBSCRIPTION_STANDARD_CONTRACT_EXTENSION = "SCE";

    // If the number of days until the next possible prolongation date is <= 30 days then the number of days will be displayed. Else >30 will be displayed.
    const OVER_30_DAYS = ">30";
}
