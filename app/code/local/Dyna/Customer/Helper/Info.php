<?php

/**
 * Get customer data from customer model and prepare it to be sent to frontend templates
 * Class Dyna_Customer_Helper_Info
 */

class Dyna_Customer_Helper_Info extends Mage_Core_Helper_Abstract
{
    /**
     * Return an array containing customer data needed for left_details.phtml template
     * Called from Customer/DetailController loadCustomerOnSession()
     */
    public function buildCustomerDetails(Dyna_Customer_Model_Customer $customer = null)
    {
        // If not customer sent as parameter, load the one from session
        if (!$customer) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
        }

        // Using Varien_Object data array
        $customerData = new Varien_Object();
        $customerData->setData("isSoho", $customer->getIsSoho());
        $customerData->setData("title", $customer->getTitle());
        $customerData->setData("showLinkIcon", $customer->showLinkIcon());
        $customerData->setData("isProspect", $customer->isProspect());
        $customerData->setData("first_name", $customer->getFirstName());
        $customerData->setData("last_name", $customer->getLastName());
        $customerData->setData("street", $customer->getStreet());
        $customerData->setData("no", $customer->getHouseNo());
        $customerData->setData("postal_code", $customer->getPostalCode());
        $customerData->setData("city", $customer->getCity());
        $customerData->setData("state", $customer->getState());
        $customerData->setData("contact_number_prefix", $customer->getLocalAreaPhoneCode());
        $customerData->setData("contact_number", $customer->getPhoneNumber());
        $customerData->setData("email", $customer->getEmailAddress());
        $customerData->setData("birth_date", $customer->getDob());
        $customerData->setData("kvk_number", $customer->getKvkNumber());
        $customerData->setData("link_id", $customer->getLinkId());
        $customerData->setData("service_address_id", $customer->getServiceAddressId());
        $customerData->setData("company_name", $customer->getCompanyName());
        $customerData->setData("contactPerson", [
            "title" => $customer->getTitle(),
            "first_name" => $customer->getFirstName(),
            "last_name" => $customer->getLastName(),
        ]);

        return $customerData->getData();
    }
}
