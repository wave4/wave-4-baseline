<?php

class Dyna_Configurator_Model_Catalog extends Omnius_Configurator_Model_Catalog
{
    /**
     * @param $packageType
     *
     * Get available configuration options based on package type
     * @return array
     */
    public static function getOptionsForPackageType($packageType)
    {
        switch (strtolower($packageType)) {
            case strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE):
            case "template-mobile":
                $result = array(
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION,
                );
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_PREPAID):
            case "template-prepaid":
                $result = array(
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION,
                );
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INTERNET_PHONE):
            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_TV):
            case "template-cable_tv":
            case "template-cable_internet_phone":
                $result = array(
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::SUBTYPE_GOODY,
                );
                break;
            case strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL):
            case "template-dsl":
                $result = array(
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::DSL_SUBTYPE_PROMOTION,
                );
                break;

            case strtolower(Dyna_Catalog_Model_Type::TYPE_LTE):
            case "template-lte":
                $result = array(
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_ADDON,
                    Dyna_Catalog_Model_Type::LTE_SUBTYPE_PROMOTION,
                );
                break;

            case strtolower(Dyna_Catalog_Model_Type::TYPE_CABLE_INDEPENDENT) :
            case "template-cable_independent" :
                $result = array(
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                );
                break;
            default:
                // do nothing
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * @param $productType
     * @param null $websiteId
     * @param array $filters
     * @return Mage_Eav_Model_Entity_Collection_Abstract|mixed|Varien_Data_Collection
     *
     * Get list of products of type device, subscription, addon or accessory
     */
    public function getProductsOfType($productType, $websiteId = null, array $filters = array(), $packageType = null, $consumerType = 0)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $key = serialize(array($packageType, $filters, $productType, $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId, implode(',', $dealerGroups), $consumerType, date('Y-m-d')));
        if (!($products = unserialize($this->getCache()->load($key)))) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);

            $collection = $this->getProductCollection($productType, Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId, $packageType, $consumerType, $filters)->load();

            $products = new Varien_Data_Collection();
            //only return the following properties to avoid large json objects with properties that are not used
            $keys = array(
                'sku',
                'thumbnail',
                'image',
                'entity_id',
                'name',
                'stock_status_class',
                'stock_status_text',
                'stock_status',
                'rel_promotional_maf',
                'rel_promotional_maf_with_tax',
                'rel_regular_maf',
                'rel_regular_maf_with_tax',
                'regular_maf',
                'regular_maf_with_tax',
                'price',
                'price_with_tax',
                'identifier_simcard_formfactor',
                'identifier_simcard_type',
                'identifier_simcard_allowed',
                'sim_type',
                'maf',
                'group_type',
                'is_discontinued',
                'sim_only',
                'prodspecs_afmetingen_unit_value',
                'prodspecs_besturingssysteem_value',
                'business_product',
                'include_btw',
                'contract_period',
                'available_stock_ind',
                'initial_period',
                'bandwidth',
                'minimum_contract_duration',
                'available_stock_ind',
                'additional_text',
                'has_maf',
                'has_price'
            );
            $keys = array_unique(array_merge($keys, $this->getFilterableAttributeCodes()));
            $attrHelper = Mage::helper('omnius_configurator/attribute');
            $mandatoryCategories = Mage::helper('omnius_configurator')->getMandatoryCategories();
            /** @var Dyna_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {

                // Skip Special Mobile discounts from promotions list
                if ($productType == Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION
                    && in_array($item->getSku(), Dyna_Catalog_Model_Product::getMobileDiscountsIds(false))
                ) {
                    continue;
                }

                $data = $this->parseProductDataForType($item, $attrHelper, $keys);

                // Building family information
                $mandatory = array_intersect($item->getCategoryIds(), $mandatoryCategories);
                $data['families'] = array_values($mandatory);

                $item->setData($data);
                $item->setData("maf", $item->getMaf());
                $item->setData("bandwidth", $item->getBandwidth() / 1000);
                $item->setData("available_stock_ind", (int) $item->getAvailableStockInd());
                $item->setData("has_maf", $item->getMaf() && ((int) $item->getMaf() >= 0) ? true : false);
                $item->setData("has_price", $item->getPrice() || ((int) $item->getPrice() >= 0) ? true : false);
                $item->setData("requires_serial", ($item->isOwnReceiver() || $item->isOwnSmartcard()) ? true : false);
                try {
                    $item->setData('thumbnail', (string) Mage::helper('catalog/image')->init($item, 'thumbnail')->resize(30));
                } catch (Exception $e) {
                    Mage::log(sprintf('Assigned image for product %s not found on disk.', $item->getSku()));
                }
                $products->addItem($item);
            }

            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;
    }

    /**
     * @param $packageSubType
     * @param string $type
     * @param $websiteId
     * @param $packageType
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection($packageSubType, $type, $websiteId, $packageType, $consumerType, $filters = [])
    {
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $subTypeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageSubType);

        //if website_id is provided in request param, overwrite it
        $websiteId = Mage::app()->getRequest()->getParam('website_id', $websiteId);
        /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->setStoreId(Mage::app()->getStore())
            ->addWebsiteFilter($websiteId)
            ->addTaxPercents()
            ->addPriceData(null, $websiteId)
            ->addAttributeToFilter('type_id', $type)
            ->addAttributeToFilter('is_deleted', array('neq' => 1))
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $subTypeValue);

        if (count($filters)) {
            $collection = $this->applyFilters($collection, $filters);
        }

        if ($packageType == Dyna_Catalog_Model_Type::TYPE_MOBILE || $packageType == Dyna_Catalog_Model_Type::TYPE_FIXED_DSL) {
            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'expiration_date', 'gteq' => date('Y-m-d')),
                    array('attribute' => 'expiration_date', 'null' => 'null'),
                )
            );
        }

        // Don't get the price for mixmatch as we also need disabled products
        if (!Mage::registry('mix_match_with_disabled_products')) {
            $collection
                ->addTaxPercents()
                ->addPriceData(null, $websiteId);
        }

        $collection->addAttributeToSort('priority', Mage_Eav_Model_Entity_Collection_Abstract::SORT_ORDER_DESC);
        /** No such attribute yet, commenting lines */
//        if ( $consumerType !== 0 ) {
//            $consValue = Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier($consumerType);
//            $collection->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('finset' => $consValue));
//        }

        if ($packageType) {
            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $typeValue = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            $collection->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, array("finset" => $typeValue));
        }

        /**
         * Apply agent groups visibility filters
         */

        // transform "customertype" attribute values to boolean
        /**************************************************************************************************
         * NO CUSTOMER TYPE YET FOR VDFDE
         ************************************************************************************************/
        /*
        $customerTypeAttribute = Mage::getResourceModel('catalog/product')->getAttribute('customertype');
        $customerTypeOptions = $customerTypeAttribute->getSource()->getAllOptions(true, true);
        $customerTypes = array();
        foreach ($customerTypeOptions as $key => $ct) {
            if ($ct['value']) {
                $customerTypes[$ct['value']] = ($ct['label'] == 'Business') ? true : false;
            }
        }
        */

        $showPriceWithBtw = Mage::getSingleton('customer/session')->showPriceWithBtw();
        $isBusiness = $this->getCustomer() instanceof Mage_Customer_Model_Customer && $this->getCustomer()->getIsBusiness();


        /** @var Dyna_Catalog_Model_Product $item */
        foreach ($collection as $item) {
            /**
             * Save if customer is business or not
             */
            $item->setData('is_business', $isBusiness);
            $item->setData('maf', $item->getMaf());
            $item->setData('regular_maf', $item->getMaf());
            $item->setData('group_type', $item->getType());
            $item->updateSimType();

            /** No such attributes in VDFDE [sim_only, prodspecs_afmetingen_unit_value, prodspecs_besturingssysteem_value, customertype] */
            //$item->setData('sim_only', $item->getSimOnly());
            //$item->setData('prodspecs_afmetingen_unit_value', $item->getUnitValue());
            //$item->setData('prodspecs_besturingssysteem_value', $item->getOperatingSystem());

            $customerTypeAttributeId = $item->getData('customertype');
            $item->setData('business_product', isset($customerTypes[$customerTypeAttributeId]) ? $customerTypes[$customerTypeAttributeId] : false);
            $item->setData('include_btw', $showPriceWithBtw);

//            $item->setData(
//                Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE,
//                $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE) ? explode(',', $item->getData(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE)) : array()
//            );

            $item->escapeData();
            /**
             * Prepare prices
             */
            $this->preparePrice($item);
        }

        return $collection;
    }

    /**
     * Applies tax on the prices if the current customer is business
     *
     * @param Mage_Catalog_Model_Product $product
     */
    protected function preparePrice(Mage_Catalog_Model_Product $product)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = $product->getStore();

        $allMafTypes = Dyna_Catalog_Model_Type::$hasMafPrices;

        if (Mage::helper('omnius_catalog')->is($allMafTypes, $product)
        ) {
            $priceValues = array(
                'promotional_maf',
                'regular_maf',
            );
            foreach ($priceValues as $priceValue) {
                if ($product->hasData($priceValue)) {
                    $_convertedPrice = $_store->roundPrice($_store->convertPrice($product->getData($priceValue)));
                    $priceWithTax = $_taxHelper->getPrice($product, $_convertedPrice, true);
                    $price = $_taxHelper->getPrice($product, $_convertedPrice, false);
                    /**
                     * Save both prices (with and without tax)
                     */
                    $product->setData(sprintf('%s_with_tax', $priceValue), $priceWithTax);
                    $product->setData($priceValue, $price);
                }
            }
        }

        /**
         * Save both prices (with and without tax)
         */
        $_convertedFinalPrice = $_store->roundPrice($_store->convertPrice($product->getPrice()));
        $priceWithTax = $_taxHelper->getPrice($product, $_convertedFinalPrice, true);
        $price = $_taxHelper->getPrice($product, $_convertedFinalPrice, false);
        $product->setPriceWithTax($priceWithTax);
        $product->setPrice($price);
    }

    public function getMobileDiscounts($productType, $packageType, $websiteId = null)
    {
        $key = serialize(array($productType, $packageType, __FUNCTION__, $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, $websiteId));
        if (!($products = unserialize($this->getCache()->load($key)))) {

            /** @var Omnius_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->setStoreId(Mage::app()->getStore())
                ->addFieldToFilter('sku', array('in' => Dyna_Catalog_Model_Product::getMobileDiscountsIds(false)))
                ->addWebsiteFilter($websiteId)
                ->addAttributeToFilter('is_deleted', array('neq' => 1))
                ->load();

            $products = new Varien_Data_Collection();

            //only return the following properties to avoid large json objects with properties that are not used
            $keys = array('sku', 'entity_id', 'name');
            $keys = array_unique(array_merge($keys, $this->getFilterableAttributeCodes()));
            $attrHelper = Mage::helper('omnius_configurator/attribute');

            /** @var Dyna_Catalog_Model_Product $item */
            foreach ($collection->getItems() as $item) {
                $data = $this->parseProductDataForType($item, $attrHelper, $keys);
                $item->setData($data);
                $products->addItem($item);
            }

            $this->getCache()->save(serialize($products), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());
        }

        return $products;

    }

    /**
     * @param $packageType
     * @return mixed|Varien_Data_Collection
     *
     * Get all items for a certain package type (mobile, prepaid, etc)
     * This relies on the "identifier_package_type" attribute meaning that only products that have this attribute set will be retrieved
     */
    public function getAllItems($packageType)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $key = serialize(array(strtolower(__METHOD__), $packageType, implode(',', $dealerGroups)));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR);
            $type = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $packageType);
            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId(Mage::app()->getStore())
                ->addWebsiteFilter()
                ->addAttributeToSelect('*')
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR, $type)
                ->load();

            $result = new Varien_Data_Collection();
            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }
            unset($collection);

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $tagName
     * @param array $filters TODO: Not implemented at this point, but it might be needed
     * @return mixed|Varien_Data_Collection
     */
    public function getProductsByTag($tagName, $filters = [])
    {
        $key = serialize([strtolower(__METHOD__), $tagName]);

        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $result = new Varien_Data_Collection();

            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter()
                ->addAttributeToFilter('tags', ['finset' => [$tagName]])
                ->load();

            foreach ($collection->getItems() as $item) {
                $result->addItem($item);
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $collection;
        }
    }

    /**
     * @param $attr string
     * @param $value string
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductsByAttribute($attr, $value)
    {
        return Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToFilter($attr, $value)
        ->load();
    }
}
