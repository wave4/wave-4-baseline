<?php

/**
 * Class Dyna_Configurator_Model_Client_InvalidProductsClient
 */
class Dyna_Configurator_Model_Client_InvalidProductsClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_invalid_products";

    protected $_forceUseStubs = true;

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetInvalidProducts($params = array())
    {
        $root = [
            'ROOT' => ['DATA' => $params]
        ];

        $values = $this->GetInvalidProducts($root);

        return $values;
    }
}
