<?php
class Dyna_Configurator_Model_Client_NonStandardRatesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_non_standard_rates";

    protected $_forceUseStubs = true;

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetNonStandardRates($params = array())
    {
        $filters = [];
        foreach ($params as $prodSku => $data) {
            foreach ($data as $contractCode => $products) {
                $filters[] = [
                    'ContractCode' => $contractCode,
                    'ProductCodes' => implode(', ', $products),
                ];
            }
        }

        $callParams['AddressId'] = Mage::getSingleton('dyna_address/storage')->getAddressId();
        $callParams['Filters'] = $filters;

        $values = $this->GetNonStandardRate($callParams);

        return $values['ROOT']['DATA'];
    }
}
