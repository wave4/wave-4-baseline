<?php
class Dyna_Configurator_Model_Client_TvEquipmentFeaturesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_tv_equipment_features";

    protected $_forceUseStubs = true;

    /**
     * GetSmartCardTypes
     * @param $params
     * @return mixed
     */
    public function executeGetTvEquipmentFeatures($params = array())
    {
        $values = $this->GetTvEquipmentFeatures($params);
        return $values;
    }
}