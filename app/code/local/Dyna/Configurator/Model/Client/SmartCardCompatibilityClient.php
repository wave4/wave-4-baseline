<?php

/**
 * Class Dyna_Configurator_Model_Client_SmartCardCompatibilityClient
 */
class Dyna_Configurator_Model_Client_SmartCardCompatibilityClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_smart_card_compatibility";

    protected $_forceUseStubs = true;

    /**
     * GetSmartCardCompatibility
     * @param $params
     * @return mixed
     */
    public function executeGetSmartCardCompatibility($params = array())
    {
        $values = $this->GetSmartCardCompatibility($params);

        return $values;
    }
}
