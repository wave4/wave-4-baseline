<?php
class Dyna_Configurator_Model_Client_IpEquipmentFeaturesClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_ip_equipment_features";

    protected $_forceUseStubs = true;

    /**
     * GetSmartCardTypes
     * @param $params
     * @return mixed
     */
    public function executeGetIpEquipmentFeatures($params = array())
    {
        $values = $this->GetIpEquipmentFeatures($params);
        return $values;
    }
}