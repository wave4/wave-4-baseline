<?php
class Dyna_Configurator_Model_Client_ConfigurationDataClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_configuration_data";

    protected $_forceUseStubs = true;

    /**
     * VFDEGetConfigurationDataRequest
     * @param $params
     * @return mixed
     */
    public function executeGetConfigurationData($params = array())
    {
        $values = $this->VFDEGetConfigurationData($params);
        return $values;
    }
}