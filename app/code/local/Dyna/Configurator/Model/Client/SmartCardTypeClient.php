<?php
class Dyna_Configurator_Model_Client_SmartCardTypeClient extends Dyna_Service_Model_Client
{
    const WSDL_CONFIG_KEY = "wsdl_smart_card_type";

    protected $_forceUseStubs = true;

    /**
     * GetSmartCardTypes
     * @param $params
     * @return mixed
     */
    public function executeGetSmartCardType($params = array())
    {
        $values = $this->GetSmartCardType($params);
        return $values;
    }
}