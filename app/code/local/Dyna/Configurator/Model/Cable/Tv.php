<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */


/**
 * Class Dyna_Configurator_Model_CABLE_TV
 */
class Dyna_Configurator_Model_Cable_Tv extends Dyna_Configurator_Model_Catalog
{
    /**
     * Retrieves all products for the mobile package type to be displayed in the configurator.
     * @param null $websiteId
     * @param array $filters
     * @param null $consumerType
     * @return array|mixed
     */
    public function getAll($websiteId = null, array $filters = array(), $consumerType = null)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        if ($consumerType === null) {
            $consumerType = 0;
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if ($customer && $customer->getId()) {
                $consumerType = ($customer->getIsBusiness()) ? 0 : Omnius_Catalog_Model_Product::IDENTIFIER_CONSUMER;
            }
        }

        $key = serialize(array(__METHOD__, $websiteId, $filters, implode(',', $dealerGroups), $consumerType));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $subscriptions = $this->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_CABLE_TV, $consumerType);
            $devices = $this->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_CABLE_TV);
            $addOns = $this->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_ADDON, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_CABLE_TV);
            $goodies = $this->getProductsOfType(Dyna_Catalog_Model_Type::SUBTYPE_GOODY, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_CABLE_TV);

            $filterableAttributes = $this->getFilterableAttributeCodes();

            $filterSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
            $filterDevices = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $devices->getColumnValues('entity_id')));
            $filterAddons = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $addOns->getColumnValues('entity_id')));
            $filterGoodies = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $goodies->getColumnValues('entity_id')));
            $filterConsumerSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
                //->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('in' => Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier()));

            $result = array(
                mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) => $subscriptions,
                mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE) => $devices,
                mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON) => $addOns,
                mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_GOODY) => $goodies,
                'filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON) => $this->getAttrHelper()->getAvailableFilters($filterAddons),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_GOODY) => $this->getAttrHelper()->getAvailableFilters($filterGoodies),
                ),
                'consumer_filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterConsumerSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON) => $this->getAttrHelper()->getAvailableFilters($filterAddons),
                    mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_GOODY) => $this->getAttrHelper()->getAvailableFilters($filterGoodies),
                )
            );

            $configuratorHelper = Mage::helper('omnius_configurator');
            $result['filters'][mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)] = array_merge(
                $result['filters'][mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)],
                array(
                    'mandatory' => array(
                    'label' => $configuratorHelper->__('Optional/Mandatory'),
                    'options' => array(
                        0 => $configuratorHelper->__('Optional'),
                        3 => $configuratorHelper->__('Mandatory')
                    ),
                    'position' => "3"
                )
                )
            );

            $result['consumer_filters'][mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)] = array_merge(
                $result['consumer_filters'][mb_strtolower(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)],
                array(
                    'mandatory' => array(
                        'label' => $configuratorHelper->__('Optional/Mandatory'),
                        'options' => array(
                            0 => $configuratorHelper->__('Optional'),
                            3 => $configuratorHelper->__('Mandatory')
                        ),
                        'position' => "3"
                    )
                )
            );

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return mixed|Varien_Data_Collection
     *
     * Get all products available on the mobile package
     */
    public function getAllProducts()
    {
        return parent::getAllItems(Dyna_Catalog_Model_Type::TYPE_CABLE_TV);
    }
}
