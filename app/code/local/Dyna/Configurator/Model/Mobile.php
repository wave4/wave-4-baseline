<?php

/**
 * Class Dyna_Configurator_Model_Mobile
 */
class Dyna_Configurator_Model_Mobile extends Dyna_Configurator_Model_Catalog
{
    const DEFAULT_24_MONTHS_INITIAL_PERIOD = 24;

    /**
     * Retrieves all products for the mobile package type to be displayed in the configurator.
     * @param null $websiteId
     * @param array $filters
     * @param null $consumerType
     * @return array|mixed
     */
    public function getAll($websiteId = null, array $filters = array(), $consumerType = null)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        if ($consumerType === null) {
            $consumerType = 0;
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if ($customer && $customer->getId()) {
                $consumerType = ($customer->getIsBusiness()) ? 0 : Omnius_Catalog_Model_Product::IDENTIFIER_CONSUMER;
            }
        }

        $key = serialize(array(__METHOD__, $websiteId, $filters, implode(',', $dealerGroups), $consumerType));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $subscriptions = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE, $consumerType);
            $devices = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE);
            $addons = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE);
            $promotions = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE);
            $discounts = $this->getMobileDiscounts(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION, Dyna_Catalog_Model_Type::TYPE_MOBILE, $websiteId);
            //$accessories = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ACCESSORY, $websiteId, $accessoriesFilter, Dyna_Catalog_Model_Type::TYPE_MOBILE);

            $filterableAttributes = $this->getFilterableAttributeCodes();

            $filterSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
            $filterDevices = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $devices->getColumnValues('entity_id')));
            $filterAddons = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $addons->getColumnValues('entity_id')));
            $filterPromotions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $promotions->getColumnValues('entity_id')));
//            $filterAccessories = Mage::getResourceModel('catalog/product_collection')
//                ->addAttributeToSelect($filterableAttributes)
//                ->addAttributeToFilter('entity_id', array('in' => $accessories->getColumnValues('entity_id')));
            $filterConsumerSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
            //->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('in' => Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier()));

            $result = array(
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION) => $subscriptions,
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $devices,
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON) => $addons,
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION) => $promotions,
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DISCOUNTS) => $discounts,
                //mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ACCESSORY) => $accessories,
                'filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON) => $this->getAttrHelper()->getAvailableFilters($filterAddons),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION) => $this->getAttrHelper()->getAvailableFilters($filterPromotions),
                    //mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ACCESSORY) => $this->getAttrHelper()->getAvailableFilters($filterAccessories),
                ),
                'consumer_filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterConsumerSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON) => $this->getAttrHelper()->getAvailableFilters($filterAddons),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_PROMOTION) => $this->getAttrHelper()->getAvailableFilters($filterPromotions),
                    //mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ACCESSORY) => $this->getAttrHelper()->getAvailableFilters($filterAccessories),
                )
            );

            $configuratorHelper = Mage::helper('omnius_configurator');
            $result['filters'][mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON)] = array_merge(
                $result['filters'][mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON)],
                array(
                    'mandatory' => array(
                        'label' => $configuratorHelper->__('Optional/Mandatory'),
                        'options' => array(
                            0 => $configuratorHelper->__('Optional'),
                            3 => $configuratorHelper->__('Mandatory')
                        ),
                        'position' => "3"
                    )
                )
            );

            $result['consumer_filters'][mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON)] = array_merge(
                $result['consumer_filters'][mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_ADDON)],
                array(
                    'mandatory' => array(
                        'label' => $configuratorHelper->__('Optional/Mandatory'),
                        'options' => array(
                            0 => $configuratorHelper->__('Optional'),
                            3 => $configuratorHelper->__('Mandatory')
                        ),
                        'position' => "3"
                    )
                )
            );

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return mixed|Varien_Data_Collection
     *
     * Get all products available on the mobile package
     */
    public function getAllProducts()
    {
        return parent::getAllItems(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }

    /**
     * Checks what addon families are available based on the package type, current products in cart and website.
     * @param $type
     * @param array $allProducts
     * @param null $websiteId
     * @return mixed
     */
    public function processFamilies($type, $allProducts = array(), $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        $productsKey = md5(serialize($allProducts));
        $key = serialize(array(__METHOD__, $websiteId, $type, $productsKey));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $mandatoryCategories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('is_family', 1)
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->setOrder('family_sort_order', Varien_Data_Collection::SORT_ORDER_ASC);

            $categories = new Varien_Data_Collection();

            // Special case Discounts Family for Mobile
            $discountsFamily = new Varien_Object();
            $discountsFamily->addData(
                [
                    'entity_id' => 'mobile_discounts',
                    'allow_mutual_products' => 1
                ]
            );
            $categories->addItem($discountsFamily);
            // END Special case

            foreach ($mandatoryCategories as $cat) {
                $categories->addItem($cat);
            }

            $result['families'] = $categories->toArray();
            $result['products'] = $allProducts;

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }
}
