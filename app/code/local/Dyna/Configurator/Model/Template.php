<?php

class Dyna_Configurator_Model_Template extends Omnius_Configurator_Model_Template
{
    protected $_sections = array(
        'tariff',
        'device',
        'hardware',
        'option',
        'accessory',
        'promotion',
        'discount',
        'goody',
    );

    /**
     * @param $type
     * @return array|mixed
     */
    public function getAllTemplates($type)
    {
        $key = sprintf('_cache_templates_%s_%s', $type, Mage::app()->getWebsite()->getCode());
        if ($templates = unserialize($this->getCache()->load($key))) {
            return $templates;
        } else {
            $templates = array();
            $block = new Mage_Page_Block_Html();
            $allowedSections = array_diff(
                Mage::getSingleton('dyna_configurator/catalog')->getOptionsForPackageType($type),
                Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            );
            $blockSections = array_intersect(
                $this->_sections,
                array_map('strtolower', $allowedSections)
            );
            foreach ($blockSections as $section) {
                $templates[$section] = $block
                    ->setTemplate(sprintf($this->_templatePath, $type, $section))
                    ->setSection($section)
                    ->setType($type)
                    ->toHtml();
            }
            $templates['partials'] = $this->getPartials($type);

            $this->getCache()->save(serialize($templates), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $templates;
        }
    }
}
