<?php

class Dyna_Configurator_Model_Observer
{
    /**
     * Special collector for Cable Rules
     * @param $observer
     * @return $this
     */
    public function collectTotalsAfter($observer)
    {
        /** @var Omnius_Checkout_Model_Sales_Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var Dyna_Package_Model_Package $activePackage */
        $activePackage = $quote->getPackageById($quote->getActivePackageId());

        if (in_array(strtolower($activePackage->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
            try {
                /** @var Dyna_Configurator_Helper_Rules $helper */
                $helper = Mage::helper('dyna_configurator/rules');
                $helper->executeCablePostRules($quote, $activePackage->getType());
            } catch (Exception $e) {
                Mage::logException($e);
            }

            if ($quote->getNeedsRecollectPostTotals() == true) {
                /** @var Dyna_Checkout_Model_Sales_Quote @quote */
                foreach ($quote->getAllAddresses() as $address) {
                    $address->unsetData('cached_items_nonnominal');
                    $address->save();
                }

                Mage::log('=== Re-execute Omnius Sales Rules ===', null, 'OmniusRulesTest.log', true);

                $quote->setTotalsCollectedFlag(false)
                    ->collectTotals()
                    ->setNeedsRecollectPostTotals(false)
                    ->save();
            }
        }

        return $this;
    }
}
