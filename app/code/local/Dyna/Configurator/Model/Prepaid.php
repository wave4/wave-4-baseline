<?php
class Dyna_Configurator_Model_Prepaid extends Dyna_Configurator_Model_Catalog
{
    /**
     * Retrieves all products for the mobile package type to be displayed in the configurator.
     * @param null $websiteId
     * @param array $filters
     * @param null $consumerType
     * @return array|mixed
     */
    public function getAll($websiteId = null, array $filters = array(), $consumerType = null)
    {
        $dealerGroups = Mage::helper('agent')->getCurrentDealerGroups();
        sort($dealerGroups);
        $websiteId = $websiteId ?: Mage::app()->getWebsite()->getId();
        if ($consumerType === null) {
            $consumerType = 0;
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if ($customer && $customer->getId()) {
                $consumerType = ($customer->getIsBusiness()) ? 0 : Omnius_Catalog_Model_Product::IDENTIFIER_CONSUMER;
            }
        }

        $key = serialize(array(__METHOD__, $websiteId, $filters, implode(',', $dealerGroups), $consumerType));
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {

            $filters = $filters + ['prepaid' => 1];
            $subscriptions = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE, $consumerType);
            $devices = $this->getProductsOfType(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE, $websiteId, $filters, Dyna_Catalog_Model_Type::TYPE_MOBILE);

            $filterableAttributes = $this->getFilterableAttributeCodes();

            $filterSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
            $filterDevices = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $devices->getColumnValues('entity_id')));
            $filterConsumerSubscriptions = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($filterableAttributes)
                ->addAttributeToFilter('entity_id', array('in' => $subscriptions->getColumnValues('entity_id')));
            //->addAttributeToFilter(Omnius_Catalog_Model_Product::CATALOG_CONSUMER_TYPE, array('in' => Mage::helper('omnius_configurator/attribute')->getSubscriptionIdentifier()));

            $result = array(
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION) => $subscriptions,
                mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $devices,
                'filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                ),
                'consumer_filters' => array(
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_PREPAID_SUBSCRIPTION) => $this->getAttrHelper()->getAvailableFilters($filterConsumerSubscriptions),
                    mb_strtolower(Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE) => $this->getAttrHelper()->getAvailableFilters($filterDevices),
                )
            );

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @return mixed|Varien_Data_Collection
     *
     * Get all products available on the mobile package
     */
    public function getAllProducts()
    {
        return parent::getAllItems(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }
}
