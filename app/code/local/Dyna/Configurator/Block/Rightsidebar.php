<?php

/**
 * CustomerDE block
 */
class Dyna_Configurator_Block_Rightsidebar extends Mage_Core_Block_Template
{

    public function getAgentStubData()
    {
        $agentDataArray = [
            'updateDate' => '4. August 2016',
            'updateTime' => '13.13.18',
            'agentName' => 'Becker Thomas',
            'storeName' => 'Vodafone Retail Store'
        ];

        $agentData = new Varien_Object();
        $agentData->setData($agentDataArray);

        return $agentData;
    }

    /**
     * Get the quote model
     *
     * @return Dyna_Checkout_Model_Sales_Quote
     */
    public function getQuoteData()
    {
        return Mage::getSingleton('checkout/cart')->getQuote();
    }

    /**
     * @return  Dyna_Checkout_Helper_Data
     */
    public function getDynaCheckoutHelper()
    {
        return Mage::helper('dyna_checkout');
    }

    /**
     * Retrieve the quote packages ordered by open/completed
     *
     * @return array
     */
    public function getPackages()
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
        $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();
        $tempPackages = $quoteModel->getPackages();
        $packages = [];
        if ($tempPackages) {
            unset($openPackages);
            $openPackages = [];
            foreach ($tempPackages as $i => $package) {
                $this->packageId = $package['package_id'];
                if (!count($package['items']) || $package['current_status'] != Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED) {
                    $openPackages[$this->packageId] = $package;
                    unset($tempPackages[$i]);
                    continue;
                }
            }
            if (count($openPackages) > 0) {
                foreach ($openPackages as $pack) {
                    array_push($tempPackages, $pack);
                }
            }
            unset($openPackages);
        }
        foreach ($tempPackages as $id => $package) {
            /** @var Dyna_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package')->getPackages(null, $quoteModel->getId(), $package['package_id'])->getFirstItem();
            $packageModel->setData('items', $package['items']);
            $packages[] = $packageModel;
        }

        return $packages;
    }

    public function getStandardNotes()
    {
        return [
            "Standard anmerkung 1",
            "Standard anmerkung 2",
            "Standard anmerkung 3",
            "Andere",
        ];
    }
}
