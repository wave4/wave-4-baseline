<?php
class Dyna_Configurator_TemplateController extends Mage_Core_Controller_Front_Action
{
    public function getAction()
    {
        $type = $this->getRequest()->getParam('type');
        $block = $this->getLayout()
            ->createBlock("core/template")
            ->setTemplate('configurator/postpaid.phtml')
            ->toHtml();

        $this->getResponse()
            ->setBody($block);
    }
}
