<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Omnius_Configurator_MobileController
 */

require_once Mage::getModuleDir('controllers', 'Omnius_Configurator').DS.'MobileController.php';

class Dyna_Configurator_MobileController extends Omnius_Configurator_MobileController
{
    protected function getModel()
    {
        preg_match('/^(.*)_(.*)Controller$/', get_called_class(), $matches);
        return Mage::getSingleton(sprintf('dyna_configurator/%s', lcfirst($matches[2])));
    }
}
