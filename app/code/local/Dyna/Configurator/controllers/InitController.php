<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Configurator') . DS . 'InitController.php';

/**
 * Class InitController
 */
class Dyna_Configurator_InitController extends Omnius_Configurator_InitController
{
    protected $coreHelper = null;

    public function mobileAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function prepaidAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function CABLE_INTERNET_PHONEAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function CABLE_INDEPENDENTAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function CABLE_TVAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function DSLAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    public function LTEAction()
    {
        $this->getActionData(__FUNCTION__);
    }

    /**
     * Action that parses the shopping cart and renders the extended shopping cart.
     */
    public function expandCartAction()
    {
        $html = Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')
            ->setTemplate('customer/right_sidebar.phtml')->toHtml();
        $this->getResponse()->setBody($html);
    }

    /**
     * Returns product information
     */
    public function getProductsAction()
    {
        try {
            $response = array();
            $helper = Mage::helper('dyna_configurator');
            $request = Mage::app()->getRequest();
            $websiteId = $request->get('website_id');
            $type = $request->get('package_type');

            $productModel = Mage::getModel(sprintf('dyna_configurator/%s', $type));
            $products = $productModel->getAll($websiteId);
            $productFilters = $products['filters'];
            $productFilters = isset($productFilters) && is_array($productFilters) && count($productFilters) ? $helper->toArray($productFilters) : new stdClass();
            unset($products['filters']);
            $allProducts = count($products) ? $helper->toArray($products) : new stdClass();
            $response[sprintf('prod_%s', $type)] = $allProducts;
            $response[sprintf('filter_%s', $type)] = $productFilters;

            $processedFamilies = $productModel->processFamilies($type, $allProducts);
            $response[sprintf('fam_%s', $type)] = $processedFamilies['families'];

            if (isset($products['consumer_filters'])) {
                $consumerProductFilters = $products['consumer_filters'];
                $consumerProductFilters = isset($consumerProductFilters) && is_array($consumerProductFilters) && count($consumerProductFilters) ? $helper->toArray($consumerProductFilters) : new stdClass();
                unset($products['consumer_filters']);
                $response[sprintf('filter_consumer_%s', $type)] = $consumerProductFilters;
            }


            $this->jsonResponse($response);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load products',
            ));
        }
    }

    /**
     *
     * Get configurator options that can be cached by Varnish
     */
    public function getConfiguratorOptionsAction()
    {
        $type = $this->getRequest()->getParam('type');
        $helper = Mage::helper('dyna_configurator');
        $productModel = Mage::getModel(sprintf('dyna_configurator/%s', $type));
        $products = $productModel->getAll();

        if (isset($products['filters'])) {
            unset($products['filters']);
        }

        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $simcard = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::SUBTYPE_SIMCARD);

        $sims = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addWebsiteFilter()
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $simcard)
            ->load()
            ->toArray();

        // Cart rules will return two nodes: rules(array of product ids) and mandatory(array of product ids)
        $cartRules = Mage::helper('dyna_configurator/rules')->getRules(array(), $type);
        $rules = $cartRules['rules'];
        /** @var Omnius_Configurator_Block_Wrapper $wrapper */
        try {
            $responseArray = array(
                'sim' => count($sims) ? $helper->toArray($sims) : new stdClass(),
                'allowedProducts' => array_values($rules)
            );
            Mage::getSingleton('core/session')->setCacheData($responseArray);
            $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load configurator options',
            ));
        }
    }

    /**
     * Get the updated products prices based on current selection
     */
    public function getPricesAction()
    {
        /** @var Dyna_Cache_Model_Cache $cache */
        $cache = Mage::getSingleton('dyna_cache/cache');
        $websiteId = $this->getRequest()->getParam('website_id');
        $isRetention = $this->getRequest()->getParam('type');
        $productIds = array();
        $cacheKey = 'configurator_get_prices_action_' . $websiteId . '_' . $isRetention;
        if ($products = $this->getRequest()->getParam('products', false)) {
            $productIds = array_filter(explode(',', $products), function($id) {
                return (int) $id;
            });
            sort($productIds);
            $cacheKey .= join('_', $productIds);
        }

        if ($cachedPrices = $cache->load($cacheKey)) {
            $prices = unserialize($cachedPrices);
        } else {
            $prices = Mage::helper('dyna_checkout')->getUpdatedPrices($productIds, $websiteId, $isRetention);
            $cache->save(serialize($prices), $cacheKey, array(Dyna_Cache_Model_Cache::PRODUCT_TAG), $cache->getTtl());
        }

        $this->jsonResponse(
            array(
                'prices' => $prices
            )
        );
    }

    /**
     * @param $section
     *
     * Get configuration for specific action
     */
    protected function getActionData($section)
    {
        $section = strtolower(str_replace('Action', '', $section));
        $config = array(
            'endpoints' => array(
                'products' => sprintf('configurator/%s/all', $section),
                'cart.addMulti' => 'configurator/cart/addMulti',
                'cart.delete' => 'configurator/cart/remove',
                'cart.list' => 'configurator/cart/list',
                'cart.empty' => 'configurator/cart/empty',
                'cart.getRules' => 'configurator/cart/getRules',
                'cart.getPrices' => 'configurator/init/getPrices',
                'cart.getPopup' => 'configurator/init/getProductpopupdata',
                'cart.getdeviceRules' => 'configurator/cart/getDeviceRules',
                'cart.getsubscriptionRules' => 'configurator/cart/getSubscriptionRules',
            ),
            'container' => '#config-wrapper',
            'sections' => array_map('strtolower', array_diff(
                Mage::getSingleton('dyna_configurator/catalog')->getOptionsForPackageType($section),
                Mage::getSingleton('dyna_configurator/catalog')->getOrderRestrictions(Mage::app()->getWebsite()->getCode())
            )),
            'type' => $section,
            'spinner' => 'skin/frontend/omnius/default/images/spinner.gif',
            'spinnerId' => '#spinner',
            'cache' => false,
            'include_btw' => Mage::getSingleton('customer/session')->showPriceWithBtw(),
            'hardwareOnlySwap' => Mage::getSingleton('customer/session')->getOrderEditMode()
        );

        $this->buildConfigurator($section, $config);
    }

    /**
     * Initializes the configurator based on the package type and sale type.
     * @param $type
     * @param array $config
     * @param string $saleType
     */
    protected function buildConfigurator($type, array $config = array(), $saleType = Omnius_Checkout_Model_Sales_Quote_Item::ACQUISITIE)
    {
        $packageId = $this->getRequest()->getParam('packageId');
        $currentProducts = $this->getRequest()->getParam('current_products');
        $migration = $this->getRequest()->getParam('migration');

        //If migration is in progress, set flag on customer model loaded to customer/session
        // todo refactor this: maybe the migration flag is not needed;
        if ($migration) {
            Mage::getModel('customer/session')
                ->getCustomer()
                ->setInMigrationState($migration);
        }
        // todo refactor this: check if the tax cable product should be added on all cable ordered products and if it is no other way of implementing this with rules
//        else if(in_array(strtolower($type), Dyna_Catalog_Model_Type::getSections('cable'))) {
//            Mage::getModel('customer/session')
//                ->getCustomer()
//                ->setInMigrationState(Dyna_Catalog_Model_Type::CABLE_INSTALLATION_TAX);
//        }

        $ctn = $this->getRequest()->getParam('ctn');
        $saleType = $this->getRequest()->getParam('saleType') ?: $saleType;
        $extraCTNPackages = array();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        try {
            if (!$packageId) {
                $ctns = explode(',', $ctn);
                $descriptions = Mage::getModel('ctn/ctn')->getAllCTNDetails($ctns);
                $getKey = function (array &$array, $key) {
                    return isset($array[$key]) ? $array[$key] : '';
                };

                $packageCTN = array_pop($ctns);
                // check if one-of-deal is active
                $isOneOfDeal = Mage::helper('dyna_configurator/cart')->isOneOfDealActive();
                $oneOfDealOptions = $isOneOfDeal && $packageCTN ? array('retainable' => false) : array();

                if ($packageCTN) {
                    $len = count($ctns);
                    for ($i = 0; $i < $len; ++$i) {
                        $extraCTNPackages[$ctns[$i]] = array(
                            'id' => Mage::helper('dyna_configurator/cart')->initNewPackage(
                                $type,
                                $saleType,
                                $ctns[$i],
                                $getKey($descriptions, $ctns[$i]),
                                $oneOfDealOptions
                            ),
                            'description' => $getKey($descriptions, $ctns[$i]),
                        );
                    }

                    // only store description for last package
                    $extraCTNPackages[$packageCTN] = array(
                        'description' => $getKey($descriptions, $packageCTN),
                    );
                }

                $packageId = Mage::helper('omnius_configurator/cart')->initNewPackage(
                    $type,
                    $saleType,
                    $packageCTN,
                    $currentProducts,
                    $oneOfDealOptions
                );
            } else {
                $quote->saveActivePackageId($packageId);
            }

            $initCart = Mage::helper('dyna_configurator/cart')->getCartItems($type, $packageId);

            $config['packageId'] = $packageId;
            $config['sale_type'] = $saleType;
            $config['current_products'] = $currentProducts;
            Mage::register('freeze_models', true, true);
            /** @var Omnius_Package_Model_Package $packageModel */
            $packageModel = Mage::getModel('package/package')->getPackages(null, $quote->getId(), $packageId)->getFirstItem();
            $config['old_sim'] = $packageModel->getId() && $packageModel->getOldSim();
            $responseArray = array(
                'error' => false,
                'initCart' => count($initCart) ? $initCart : new stdClass(),
                'config' => $config,
                'packageId' => $packageId,
                'promoRules' => Mage::helper('pricerules')->findApplicablePromoRules(true),
            );

            if (($incompleteSections = Mage::helper('omnius_checkout')->checkPackageStatus($packageId)) && is_array($incompleteSections)) {
                $responseArray['incomplete_sections'] = $incompleteSections;
            }

            if (!empty($extraCTNPackages)) {
                $responseArray['ctnPackages'] = $extraCTNPackages;
            }

            $this->jsonResponse($responseArray);
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => 'Could not load mobile configurator',
            ));
        }
    }

    /**
     * Fetches the stocks for the devices.
     */
    public function getDeviceStockAction()
    {
        /** @var Omnius_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $websiteId = $this->getRequest()->getParam('website_id');
        $axiStoreId = $this->getRequest()->getParam('axi_store_id', 0);
        $result = $productModel->getStockCall($websiteId, Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, $axiStoreId);
        $this->jsonResponse($result);
    }

    /**
     * Fetches the stocks for the accessories.
     */
    public function getAccessoriesStockAction()
    {
        /** @var Dyna_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product');
        $websiteId = $this->getRequest()->getParam('website_id');
        $axiStoreId = $this->getRequest()->getParam('axi_store_id', 0);
        $result = $productModel->getStockCall($websiteId, Dyna_Catalog_Model_Type::$accessories, $axiStoreId);
        $this->jsonResponse($result);
    }

    /**
     * Get html block containing all modals
     */
    public function getGeneralModalsAction()
    {
        $html = Mage::getSingleton('core/layout')->createBlock('core/template')
            ->setTemplate('page/html/global_modals.phtml')->toHtml();
        $this->getResponse()->setBody($html);
    }
}
