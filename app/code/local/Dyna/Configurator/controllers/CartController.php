<?php
require_once Mage::getModuleDir('controllers', 'Omnius_Configurator').DS.'CartController.php';

class Dyna_Configurator_CartController extends Omnius_Configurator_CartController
{
    /**
     * Get list of products that are visible in frontend based on current rules
     */
    public function getRulesAction()
    {
        $productIds = array();
        /** If there are current products in cart, forward them to rules processor */
        if ($products = Mage::app()->getRequest()->getParam('products', false)) {
            $productIds = explode(',', $products);
        }
        $type = $this->getRequest()->get('type');
        $websiteId = $this->getRequest()->get('websiteId', null);

        /** @var Dyna_Configurator_Helper_Rules $cartHelper */
        $cartHelper = Mage::helper('dyna_configurator/rules');
        $rules = $cartHelper->getRules($productIds, $type, $websiteId);

        $cablePackages = Dyna_Catalog_Model_Type::getCablePackages();
        // Cable specific service calls
        if (in_array(strtolower($type), $cablePackages)) {
            /** @var Dyna_Configurator_Helper_Services $servicesHelper */
            $servicesHelper = Mage::helper('dyna_configurator/services');

            // InvalidProducts for Cable
            $invalidProducts = $servicesHelper->getInvalidProducts($type, $rules['rules']);
            if (count($invalidProducts)) {
                $rules['invalid'] = $invalidProducts;
            }

            // NonStandardRates for Cable
            $nonStandardRates = $servicesHelper->getNonStandardRates($type, $rules['rules']);
            if (count($nonStandardRates)) {
                $rules['non_standard_rates'] = $nonStandardRates;
            }
        }

        $this->jsonResponse($rules);
    }

    /**
     * Method used for adding products to cart.
     */
    public function addMultiAction()
    {
        if (Mage::getSingleton('customer/session')->getOrderEditMode() && ! Mage::getSingleton('checkout/session')->getIsEditMode()) {
            $this->jsonResponse(array(
                'error' => true,
                'reload' => true,
                'message' => $this->__('Cannot add items to an already edited and saved quote'),
            ));
            return;
        }

        Mage::register('product_add_origin', 'addMulti');
        $priceRulesHelper = $this->_priceRulesHelper;
        $type = $this->getRequest()->getParam('type');
        $section = $this->getRequest()->getParam('section');
        $sim = $this->getRequest()->getParam('simType');
        $oldSim = $this->getRequest()->getParam('oldSim');
        $packageId = $this->getRequest()->getParam('packageId');
        $this->_getQuote()->saveActivePackageId($packageId);
        $this->_getQuote()->setCurrentStep(null);
        $removedDefaultFlag = false;

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Retrieve all the exclusive families
        $allExclusiveFamilyCategoryIds = Mage::getModel('catalog/category')->getAllExclusiveFamilyCategoryIds();
        // List of all the exclusive families that are contained in the package
        $exclusiveFamilyCategoryIds = [];
        $products = array_filter(
            array_filter(
                is_array($this->getRequest()->getParam('products')) ? $this->getRequest()->getParam('products') : array(), 'trim'
            )
        );

        // Create the new package for the quote or update if already created
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        if (!$packageModel->getId()) {
            $packageModel->setQuoteId($this->_getQuote()->getId())->setPackageId($packageId)->setType($type);
        }

        // If no products in cart, or own customer hardware has changed
        if ($customer->hasOwnHardware() && (empty($products) || !in_array(Dyna_Catalog_Model_Type::getOwnReceiverId(), $products) || !in_array(Dyna_Catalog_Model_Type::getOwnSmartcardId(), $products))) {
            if (empty($products)) {
                $customer->clearOwnReceiver()
                    ->clearOwnSmartcard();
                $packageModel->setSmartcardSerialNumber(NULL);
                $packageModel->setReceiverSerialNumber(NULL);
            } else {
                if (!in_array(Dyna_Catalog_Model_Type::getOwnReceiverId(), $products)) {
                    $packageModel->setReceiverSerialNumber(NULL);
                    $customer->clearOwnReceiver();
                } else {
                    $packageModel->setSmartcardSerialNumber(NULL);
                    $customer->clearOwnSmartcard();
                }
            }
            $packageModel->save();
        }

        // If customer has own hardware, set serial on package
        if ($customer->hasOwnHardware() && !empty($products)) {
            if (in_array(Dyna_Catalog_Model_Type::getOwnReceiverId(), $products)) {
                $packageModel->setReceiverSerialNumber(
                    $customer->getOwnReceiver()->getData("EquipmentSerialNumber")
                );
            }
            if (in_array(Dyna_Catalog_Model_Type::getOwnSmartcardId(), $products)) {
                $packageModel->setSmartcardSerialNumber(
                    $customer->getOwnSmartCard()->getData("SmartCardSerialNumber")
                );
            }
            $packageModel->save();
        }

        $added = [];
        $allItems = $this->_getQuote()->getAllItems();

        //If cart is empty and there is a migration in progress, add service migration provision charge product
        //todo refactor - these producs should be added based on a rule; do not add them manually
//        if (!count($allItems) && $migrationType = Mage::getSingleton('customer/session')->getCustomer()->getInMigrationState()) {
//            //There are two hardcoded products that have an sku identical to migration value
//            // (search Dyna_Catalog_Model_Type constants for migration values)
//            $productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($migrationType);
//            $serviceChargeProduct = Mage::getModel('catalog/product')->load($productId);
//            if ($serviceChargeProduct->getId()) {
//                $this->_getCart()->addProduct($serviceChargeProduct);
//            } else {
//                $this->jsonResponse(array(
//                    'error' => true,
//                    'message' => $this->__('You are trying to do a migration an I cannot find the service migration charge product!'),
//                ));
//            }
//        }

        if (!count($products)) {
            // Clear all
            return $this->removeAllItems($section, $packageId, $type, $packageModel);
        }

        $persistedProducts = [];

        $productsInCart = $appliedRulesArray = [];
        $deletedItems = [];
        try {
            /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
            foreach ($allItems as $itemKey => $item) {
                if (isset($deletedItems[$item->getId()])) {
                    continue;
                }
                if (
                    $item->getPackageId() == $packageId &&
                    (
                        $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION)
                        || $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE)
                        || $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_ACCESSORY)
                        || $item->getProduct()->is(Dyna_Catalog_Model_Type::SUBTYPE_ADDON))
                ) {
                    $productsInCart[] = $item->getProductId();
                }

                if (($categoriesExclusive = array_intersect($item->getProduct()->getCategoryIds(), $allExclusiveFamilyCategoryIds))
                    && ($item->getPackageId() == $packageId)
                ) {
                    foreach ($categoriesExclusive as $categoryExclusive) {
                        $exclusiveFamilyCategoryIds[$item->getProductId()] = $categoryExclusive;
                    }
                }

                if ($item->getPackageId() == $packageId
                    && $item->getProduct()->isInSection(strtolower($section))
                ) {
                    if (!in_array($item->getProductId(), $products)) {
                        $this->removeItemAndParseDefault($item, $productsInCart, $exclusiveFamilyCategoryIds, $packageModel, $allItems, $deletedItems, $itemKey, $removedDefaultFlag, $removedDefault);
                    } else {
                        $persistedProducts[] = $item->getProductId();
                    }
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__('Cannot add the item to shopping cart'),
            ));
            return;
        }

        try {
            $validProducts = Mage::helper('omnius_configurator/cart')->getCartRules($productsInCart);
            // todo: activate after demo
            //$this->validateProducts($productsInCart, $allItems, $packageId, $validProducts, $item);
        } catch (Exception $e) {
            $this->jsonResponse(array(
                'error' => true,
                'message' => $e->getMessage(),
            ));

            return;
        }

        // Preload the attributes needed for all the products to prevent loading each product
        $invalidProducts = array_diff($products, $validProducts);
        if ($invalidProducts) {
            $allProducts = Mage::getResourceModel('catalog/product_collection')
                ->addIdFilter($invalidProducts);
        }

        foreach ($products as $productId) {
            try {
                if (in_array($productId, $persistedProducts)) {
                    continue;
                }
                $this->parseProduct($packageId, $productId, $type, $appliedRulesArray, $added);
            } catch (RuntimeException $e) {
                continue;
            } catch (Mage_Core_Exception $e) {
                $messages = array_map(function ($el) {
                    return Mage::helper('core')->escapeHtml($el);
                }, array_unique(explode("\n", $e->getMessage())));
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => join(', ', $messages),
                ));
                $this->removeProductFromCart($added);
                $this->_getCart()->save();

                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/logger')->logException($e);
                $this->jsonResponse(array(
                    'error' => true,
                    'message' => $this->__('Cannot add the item to shopping cart'),
                ));
                $this->removeProductFromCart($added);
                $this->_getCart()->save();

                return;
            }
        }
        unset($allProducts, $invalidProducts);

        // Add sim if needed
        $simError = false;
        $simName = null;
        $package = $this->_getQuote()->getPackageById($packageId);

        $this->addSimProductToCart($sim, $package, $type, $oldSim, $simName, $simError);
        $finalProducts = array_merge($persistedProducts, $added);
        $addedDefaultItems = $this->parseDefaultedProducts($added, $packageModel, $finalProducts, $allExclusiveFamilyCategoryIds, $exclusiveFamilyCategoryIds, $type);

        $this->_getCart()->save();
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        // Update old_sim attribute to the current package if it is a retention package or if it is HN subscription

        if ($packageModel->getSaleType() == Omnius_Checkout_Model_Sales_Quote_Item::RETENTIE
        ) {
            $oldSimValue = ($oldSim == 'true') ? 1 : 0;
            $packageModel->setOldSim($oldSimValue)->save();
        }

        $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);

        // Update status
        $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->checkRemoveCurrentSim();
        $packageModel->save();

        $this->returnCombinations($simError, $simName, isset($addedDefaultItems) ? $addedDefaultItems : [], isset($removedDefault) ? $removedDefault : [], $packageStatus);
    }

    /**
     * The action used for the removal of packages from the configurator.
     */
    public function removePackageAction()
    {
        $accepted = $this->getRequest()->getParam('accepted', '0');
        try {
            $packageId = $this->getRequest()->getParam('packageId');
            $expanded = $this->getRequest()->getParam('expanded');

            // check if one of deal
            $isOneOfDeal = Mage::helper('omnius_configurator/cart')->isOneOfDealActive();

            $quote = $this->_getQuote();

            // Get all the packages
            $packagesModels = Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $quote->getId());

            // Find the current package
            foreach($packagesModels as $packagesModel) {
                if($packagesModel->getPackageId() == $packageId) {
                    $packageModel = $packagesModel;
                }
            }

            if(!isset($packageModel) || !$packageModel->getId()) {
                throw new Exception(sprintf($this->__('Could not find package %d for quote: %d'), $packageId, $quote->getId()));
            }

            $packageIds = array((int) $packageId);
            $removeRetainables = false;
            $couponsToDecrement = array();
            if ($isOneOfDeal && $packageModel->getRetainable()) {
                if ($accepted != '1') {
                    $this->jsonResponse(array(
                        'error'              => false,
                        'showOneOfDealPopup' => true
                    ));
                    return;
                }
                foreach($packagesModels as $packagesModel) {
                    if($packagesModel->getOneOfDeal()) {
                        $packageIds[] = $packagesModel->getPackageId();
                        $couponsToDecrement[] = $packageModel->getCoupon();
                        $packagesModel->delete();

                    }
                }

                $removeRetainables = true;
            }

            /** @var Mage_Sales_Model_Quote_Item $item */
            foreach ($quote->getAllItems() as $item) {
                if (in_array((int)$item->getPackageId(), $packageIds)) {
                    $item->isDeleted(true);
                }
            }

            $couponsToDecrement[] = $packageModel->getCoupon();
            // Remove the package from the packages table
            $packageModel->delete();
            $customerId = $quote->getCustomerId();

            foreach($couponsToDecrement as $coupon){
                Mage::helper('pricerules')->decrementCoupon($coupon, $customerId);
            }

            if (in_array((int) $quote->getActivePackageId(), $packageIds)) {
                $quote->setActivePackageId(false);
            }

            $quote->save();

            $this->_getCart()->save();

            $packagesIndex = Mage::helper('dyna_configurator/cart')->reindexPackagesIds($quote);

            $this->jsonResponse(array(
                'error'             => false,
                'totals'            => Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml(),
                'packagesIndex'     => $packagesIndex,
                'complete'          => Mage::helper('omnius_checkout')->canCompleteOrder(),
                'expanded'          => $expanded ? Mage::getSingleton('core/layout')->createBlock('dyna_configurator/rightsidebar')->setTemplate('customer/right_sidebar.phtml')->toHtml() : '',
                'removeRetainables' => $removeRetainables ? array_values($packageIds) : false,
            ));

        } catch (Exception $e) {
            Mage::getSingleton('core/logger')->logException($e);
            $this->jsonResponse(array(
                'error' => true,
                'message' => $this->__($e->getMessage())
            ));
        }
    }


    /**
     * Empties a product section from the configurator.
     * @param $section
     * @param $packageId
     * @param $type
     * @param $packageModel
     */
    protected function removeAllItems($section, $packageId, $type, $packageModel)
    {
        $removeSim = false;
        $removedDefaultFlag = false;
        $hasSimOnly = false;
        /** @var Omnius_Checkout_Model_Sales_Quote_Item $item */
        foreach ($this->_getQuote()->getItemsCollection() as $item) {
            if ($item->getPackageId() != $packageId || $item->isDeleted()) {
                continue;
            }
            if (strtolower($item->getPackageType()) == strtolower($type)
                && $item->getProduct()->isInSection(strtolower($section))
            ) {
                $this->doRemoveItemOfAll($packageModel, $item, $removedDefaultFlag, $removeSim, $removedDefault);
            }
        }

        /**
         * Needs second iteration to remove the sim products
         */
        if ($removeSim) {
            foreach ($this->_getQuote()->getItemsCollection() as $item) {
                if ($item->getPackageId() == $packageId && !$item->isDeleted() && $item->getProduct()->isSim()) {
                    $this->_getCart()->removeItem($item->getId());
                }
            }
        }
        $this->_getCart()->save();
        /** @var Omnius_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
            ->addFieldToFilter('package_id', $packageId)
            ->getFirstItem();
        $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);

        $packageModel->setCurrentStatus(Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED === $packageStatus
            ? Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED
            : Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_OPEN);
        $packageModel->updateDefaultedRemoved($removedDefaultFlag);
        $packageModel->save();

        $this->returnCombinations(false, null, false, [], isset($removedDefault) ? $removedDefault : [], $packageStatus);
    }

    /**
     * Returns list of products that are not allowed based on current selection together with package totals
     * @param bool|false $simError
     * @param null $simName
     * @param array $addedDefault
     * @param array $removedDefault
     * @param null $packageStatus
     */
    private function returnCombinations($simError = false, $simName = null, $addedDefault = [], $removedDefault = [], $packageStatus = null)
    {
        // From this point models can be cached
        Mage::register('freeze_models', true, true);
        $totalsUpdated = Mage::helper('omnius_configurator')->getActivePackageTotals();
        $totals = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
        $packageId = $this->_getQuote()->getActivePackageId();
        $rightBlock = Mage::getSingleton('core/layout')->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/partials/package_block.phtml')->setCurrentPackageId($packageId)->toHtml();
        $promoRules = $this->_priceRulesHelper->findApplicablePromoRules(true);

        $response = array(
            'error' => false,
            'activePackageId' => $packageId,
            'totalMaf' => $totalsUpdated['totalmaf'],
            'totalPrice' => $totalsUpdated['totalprice'],
            'totals' => $totals,
            'rightBlock' => $rightBlock,
            'promoRules' => $promoRules,
            'complete' => Mage::helper('omnius_checkout')->canCompleteOrder()
        );

        if ($packageStatus === null) {
            $packageStatus = Mage::helper('omnius_checkout')->checkPackageStatus($packageId);
        }
        if (($incompleteSections = $packageStatus) && is_array($incompleteSections)) {
            $response['incomplete_sections'] = $incompleteSections;
        }
        $closure = function ($el) {
            return ['product_id' => $el->getProductId(), 'type' => strtolower($el->getProduct()->getTypeText())];
        };
        if ($addedDefault) {
            $response['addedDefaultedItems'] = array_map($closure, $addedDefault);
        }
        if ($removedDefault) {
            $response['removedDefaultedItems'] = array_map($closure, $removedDefault);
        }
        if ($simError) {
            $response['simError'] = $simError;
        } else {
            $response['sim'] = $simName;
        }

        $this->jsonResponse($response);
    }

    /**
     * Validate serial number for hardware products that client has in its installed base
     */
    public function validateHardwareSerialAction()
    {
        $serialNumber = $this->getRequest()->getParam("serialNumber");
        $productId = $this->getRequest()->getParam("productId");

        // Get customer model
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Load product and check if it own smart card or own receiver
        /** @var Dyna_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($productId);

        // Initial response
        $response = [
            'error' => false,
            'message' => 'Success',
        ];

        // Service calls for smartcard
        if ($product->isOwnSmartcard()) {

            // Get smartcard type call
            $requestData = ["SmartCardSerialNumber" => $serialNumber];
            /** @var Dyna_Configurator_Model_Client_SmartCardTypeClient $client */
            $smartCardTypeClient = Mage::getModel('dyna_configurator/client_smartCardTypeClient')->getClient();
            $smartCardTypeData = $smartCardTypeClient->executeGetSmartCardType($requestData);

            if ($smartCardTypeData['ROOT']['DATA']['Type'] == 'UNKNOWN') {
                $response = $this->getErrorResponse($this->__("Type is unknown. SmartCard is not usable."));
                $this->jsonResponse($response);

                return;
            }

            // Set SmartCard on customer session
            $smartCard = new Varien_Object();
            $smartCard->addData($smartCardTypeData['ROOT']['DATA']);
            $smartCard->setSerialNumber($serialNumber);
            $customer->setOwnSmartCard($smartCard);

            $response = Mage::helper('dyna_configurator/services')->checkSmartcardReceiverCompatibility();

        // Service calls for own receiver
        } elseif ($product->isOwnReceiver()) {
            // Get tv equipment service call
            /** @var Dyna_Configurator_Model_Client_TvEquipmentFeaturesClient $tvEquipmentClient */
            $tvEquipmentClient = Mage::getModel('dyna_configurator/client_tvEquipmentFeaturesClient');
            $requestData = ["EquipmentSerialNumber" => $serialNumber];
            $responseData = $tvEquipmentClient->executeGetTvEquipmentFeatures($requestData);

            $receiver = new Varien_Object();
            $receiver->addData($responseData['ROOT']['DATA']);
            $receiver->setSerialNumber($serialNumber);
            $customer->setOwnReceiver($receiver);

            //Again, if customer has already added it's own smartcard, check compatibility with it
            $response = Mage::helper('dyna_configurator/services')->checkSmartcardReceiverCompatibility();
        } else {
            $response = $this->getErrorResponse($this->__("Selected a product that is not a smartcard nor a receiver."));
        }

        $this->jsonResponse($response);
    }

    /**
     * Generic method that returns an error response message
     * @param $message
     * @return array
     */
    protected function getErrorResponse($message)
    {
        return [
            'error' => true,
            'message' => $message,
        ];
    }

}
