<?php

class Dyna_Configurator_Helper_Cart extends Omnius_Configurator_Helper_Cart
{
    /**
     * @param array $productIds
     * @param array $allProducts
     * @param bool $websiteId
     * @param bool $cacheCollections
     * @return array
     */
    public function getCartRules($productIds, $allProducts = array(), $websiteId = null, $cacheCollections = false)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $key = sprintf('get_cart_new_rules_%s_%s_%s', md5(serialize($productIds)), md5(serialize($allProducts)), $websiteId);
        if (!($result = unserialize($this->getCache()->load($key)))) {
            if (count($productIds)) {
                $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
                $available = [];

                $storeId = $websiteId ? Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId() : Mage::app()->getStore()->getId();
                $catalogResource = Mage::getResourceModel('catalog/product');
                $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
                $typeDevice = [];
                foreach (Dyna_Catalog_Model_Type::$devices as $deviceIdentifier) {
                    $typeDevice[] = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $deviceIdentifier);
                }
                $typeSubscription = [];
                foreach (Dyna_Catalog_Model_Type::$subscriptions as $subscriptionIdentifier) {
                    $typeSubscription[] = Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, null, $subscriptionIdentifier);
                }
                $rawAttributesValues = [];

                if ($cacheCollections) {
                    if (!isset($this->_collectionsCache['device'])) {
                        $this->_collectionsCache['device'] = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => $typeDevice])->getSelectSql(true));
                    }
                    if (!isset($this->_collectionsCache['sub'])) {
                        $this->_collectionsCache['sub']  = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => $typeSubscription])->getSelectSql(true));
                    }

                    $deviceCollection = $this->_collectionsCache['device'];
                    $subsCollection = $this->_collectionsCache['sub'];
                } else {
                    $deviceCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => $typeDevice])->getSelectSql(true));
                    $subsCollection = $adapter->fetchCol(Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => $typeSubscription])->getSelectSql(true));
                }

                foreach ($productIds as $id) {
                    if (!empty($rule = $this->getRulesForId($websiteId, $id, $catalogResource, $storeId, $rawAttributesValues, $attribute, $deviceCollection, $subsCollection))) {
                        $available[] = $rule;
                    }
                }

                $result_new = [];
                if (!empty($available)) {
                    $result_new = array_pop($available);
                    foreach ($available as $items) {
                        $result_new = array_intersect($result_new, $items);
                    }
                    $result = array_values($result_new);
                } else {
                    $result = [];
                }

                if (!$result_new) {
                    return [];
                }

                $result = array_values($result_new);

            } else {
                $result = $this->getAllRulesForProducts($allProducts, $websiteId);
            }

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());
        }

        return $result;
    }

    /**
     * @param $websiteId
     * @param $id
     * @param $catalogResource
     * @param $storeId
     * @param $rawAttributesValues
     * @param $attribute
     * @param $deviceCollection
     * @param $subsCollection
     * @return array
     */
    protected function getRulesForId($websiteId, $id, $catalogResource, $storeId, &$rawAttributesValues, $attribute, $deviceCollection, $subsCollection)
    {
        $rulesSource = $this->getRuleById($id, $websiteId);

        /**
         * When choosing a device or subscription, normally the application would hide the rest of the devices or subscriptions
         * if there is no allowed rule between them. However, for devices and subscriptions, we must allow users to switch
         * between the allowed ones without hiding the other options.
         */
        $valueId = $catalogResource->getAttributeRawValue($id, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $storeId);
        if (isset($rawAttributesValues[$valueId])) {
            $productType = $rawAttributesValues[$valueId];
        } else {
            $productType = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $valueId), true);
            $rawAttributesValues[$valueId] = $productType;
        }

        $similarProductIds = array();

        //if device, also retrieve all other devices to simulate that subscriptions are allowed between themselves
        /** Using array intersect because devices are defined by multiple package subtypes */
        if (!empty(array_intersect(Dyna_Catalog_Model_Type::$devices, $productType))) {
            $similarProductIds = $deviceCollection;
        }

        //if subscription, also retrieve all other subscriptions to simulate that subscriptions are allowed between themselves
        /** Using array intersect because subscriptions are defined by multiple package subtypes */
        if (!empty(array_intersect(Dyna_Catalog_Model_Type::$subscriptions, $productType))) {
            $similarProductIds = array_merge($similarProductIds, $subsCollection);
        }

        $targetProductIds = [];
        $sourceProductIds = [];
        foreach ($rulesSource as $row) {
            $targetProductIds[$row['target_product_id']] = $row['target_product_id'];
            $sourceProductIds[$row['source_product_id']] = $row['source_product_id'];
        }

        $available = array_unique(
            array_merge(
                $similarProductIds,
                $targetProductIds,
                $sourceProductIds
            )
        );

        return $available;
    }

    /**
     * Returns the defaulted (auto added to cart) products for current cart content
     * @param $selectedProducts array with product ids
     * @param null $websiteId
     * @return array with product ids
     */
    public function getDefaultedProducts($selectedProducts, $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $key = sprintf('%s_product_defaulted_products__%s', md5(serialize($selectedProducts)), $websiteId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            /** Defaulted products are appended to the current list of products */
            $defaultedProducts = [];
            foreach ($selectedProducts as $productId) {
                // Retrieve all defaulted products for current product
                $defaultedProducts = Mage::getSingleton('productmatchrule/defaulted')->getDefaultedProductsFor($productId, $websiteId);
                foreach ($defaultedProducts as $defaultedProductId) {
                    if (!in_array($defaultedProductId, $defaultedProducts)) {
                        $defaultedProducts[] = $defaultedProductId;
                    }
                }
            }

            $result = $defaultedProducts;
            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * @param $type
     * @param $saleType
     * @param null $ctn
     * @param null $currentProducts
     * @param array $oneOffDealOptions
     * @return mixed
     */
    public function initNewPackage($type, $saleType, $ctn = null, $currentProducts = null, $oneOffDealOptions = array())
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $quoteId = $quote->getId();
        $newPackageId = Omnius_Configurator_Model_CartObserver::getBiggestPackageId($quote);

        if (!$quoteId) {
            $quote->setCustomerId(Mage::getSingleton('customer/session')->getId());
        }

        $quote->setActivePackageId($newPackageId);
        $quote->save();

        if (!$quoteId) {
            Mage::getSingleton('checkout/cart')->getCheckoutSession()->setQuoteId($quote->getId());
        }

        // Create the new package for the quote or update if already created
        /** @var Dyna_Package_Model_Package $packageModel */
        $packageModel = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $newPackageId)
            ->getFirstItem();

        if (!$packageModel->getId()) {
            // Cannot create new serviceability dependent package without a service address id
            $serviceAddressId = Mage::getSingleton('dyna_address/storage')->getAddressId();
            if (!in_array($type, Dyna_Catalog_Model_Type::getMobilePackages()) && !$serviceAddressId) {
                Mage::throwException("Trying to init a serviceability dependent package with no service address id");
            } elseif (!in_array($type, Dyna_Catalog_Model_Type::getMobilePackages())) {
                $packageModel->setServiceAddressId($serviceAddressId);
            }

            $packageModel->setQuoteId($this->_getQuote()->getId())
                ->setPackageId($newPackageId)
                ->setType($type)
                ->setSaleType($saleType)
                ->setCtn($ctn)
                ->setCurrentProducts($currentProducts);
        }

        if (!empty($oneOffDealOptions)) {
            $packageModel->setOneOfDeal(true);
            $quote->setItemsQty(count(explode(',', $ctn)));
            $packageModel->setRetainable($oneOffDealOptions['retainable']);
        }

        $packageModel->save();

        return $newPackageId;
    }
}
