<?php

class Dyna_Configurator_Helper_Services extends Mage_Core_Helper_Abstract
{

    /**
     * Check the compatibility between own SmartCard and own/new Receiver
     * @return array
     */
    public function checkSmartcardReceiverCompatibility()
    {
        // Get customer model
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        $response = ['error' => null];

        /** @var Dyna_Configurator_Model_Client_SmartCardCompatibilityClient $compatibilityClient */
        $compatibilityClient = Mage::getModel('dyna_configurator/client_smartCardCompatibilityClient');

        // Service call is performed only when own SmartCard
        if ($ownSmartCard = $customer->getOwnSmartCard()) {
            $serialNumber = $ownSmartCard->getSerialNumber();

            // If customer has already selected an own receiver
            if ($ownReceiver = $customer->getOwnReceiver()) {
                // Execute compatibility between own SmartCard and own Receiver
                $requestData = ["SmartCardSerialNumber" => $serialNumber, "EquipmentSerialNumber" => $ownReceiver->getSerialNumber()];
                $responseData = $compatibilityClient->executeGetSmartCardCompatibility($requestData);

                if (! $responseData['ROOT']['DATA']['Compatibility']) {
                    $response = $this->getErrorResponse($this->__("SmartCard is not compatible with your own receiver."));
                } else {
                    $response = [
                        'error' => false,
                        'data' => $responseData['ROOT']['DATA']
                    ];
                }

            } else {
                // Not own receiver case
                $receiverCode = null;

                /** @var Dyna_Checkout_Model_Sales_Quote $cart */
                $cart = Mage::getModel('checkout/cart')->getQuote();
                // Get all products in cart and check if they are receivers
                foreach ($cart->getAllItems() as $item) {
                    // Get first receiver in the package
                    if ($item->getPackageId() == $cart->getActivePackageId()
                        && $item->getProduct()->getSubType() == 'RC'
                    ) {
                        $receiverCode = $item->getProduct()->getProductCode();
                        break;
                    }
                }

                if ($receiverCode) {
                    // Execute GetSmartCardCompatibility between own SmartCard and selected Receiver
                    $requestData = ["SmartCardSerialNumber" => $serialNumber, "JobCode" => $receiverCode];
                    $responseData = $compatibilityClient->executeGetSmartCardCompatibility($requestData);

                    if (! $responseData['ROOT']['DATA']['Compatibility']) {
                        $response = $this->getErrorResponse($this->__("SmartCard is not compatible with the selected receiver."));
                    } else {
                        $response = [
                            'error' => false,
                            'data' => $responseData['ROOT']['DATA']
                        ];
                    }
                }
            }
        }

        return $response;
    }

    /**
     * GetInvalidProducts service call for cable products
     * @param $packageType
     * @param $availableProducts
     * @return array
     */
    public function getInvalidProducts($packageType, $availableProducts)
    {
        $invalidProducts = [];
        $ospProducts = [];

        // Get all products with tag filterByOSP (only when creating the package = no products selected)
        $filterByOSP = Mage::getModel('dyna_configurator/catalog')->getProductsByTag('filterByOSP');
        foreach ($filterByOSP as $ospProduct) {
            // Check if OSP product is available
            if ((strtolower($ospProduct->getData(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_TYPE_ATTR)) == strtolower($packageType))
                && in_array($ospProduct->getId(), $availableProducts)
            ) {
                $ospProducts[] = $ospProduct->getSku();
            }
        }

        if (count($ospProducts)) {
            /** @var Dyna_Configurator_Model_Client_InvalidProductsClient $client */
            $client = Mage::getModel('dyna_configurator/client_InvalidProductsClient');
            $addressId = Mage::getSingleton('dyna_address/storage')->getAddressId();

            $params = [
                'AddressId' => $addressId,
                'ProductCodes' => implode(',', $ospProducts),
            ];
            // Execute GetInvalidProducts service call
            $ospResponse = $client->executeGetInvalidProducts($params);

            if (! empty($ospResponse['ROOT']['DATA']['ProductCodes'])) {
                $tmpInvalidProducts = explode(',', $ospResponse['ROOT']['DATA']['ProductCodes']);
                $rulesHelper = Mage::helper('dyna_configurator/rules');

                foreach ($tmpInvalidProducts as $sku) {
                    $invalidProducts[$sku] =  $rulesHelper->getProductIdBySku($sku);
                }
            }
        }

        return $invalidProducts;
    }

    /**
     * GetNonStandardRates service call for cable products
     * @param $packageType
     * @param $availableProducts
     * @return mixed
     */
    public function getNonStandardRates($packageType, $availableProducts)
    {
        $params = [];

        $productsModel =  Mage::getModel('dyna_configurator/' . $packageType);
        $products = $productsModel->getAllProducts();

        foreach ($products as $product) {
            if (!empty($reqRates = $product->getData('req_rate_prod_for_contracts'))) {
                if (in_array($product->getId(), $availableProducts)) {
                    $sku = $product->getData('sku');
                    $params[$sku] = json_decode($reqRates);
                    $bounds[$sku] = [
                        'min' => $product->getData('req_rate_min_bound'),
                        'max' => $product->getData('req_rate_max_bound'),
                    ];
                }
            }
        }

        /** @var Dyna_Configurator_Model_Client_NonStandardRatesClient $client */
        $client = Mage::getModel('dyna_configurator/client_nonStandardRatesClient');
        $serviceResults = $client->executeGetNonStandardRates($params);

        if (!empty($serviceResults['NonStandardRates'])) {
            /** @var Mage_Tax_Helper_Data $_taxHelper */
            $_taxHelper = Mage::helper('tax');
            foreach ($serviceResults['NonStandardRates'] as $rate) {
                if ($rate['NonStandardRate'] >= $bounds[$sku]['min'] && $rate['NonStandardRate'] <= $bounds[$sku]['max']) {
                    $rulesHelper = Mage::helper('dyna_configurator/rules');
                    $prodId = $rulesHelper->getProductIdBySku($rate['ProductCode']);
                    // Load product model
                    $product = Mage::getModel('catalog/product')->load($prodId);

                    $result[$prodId] = [
                        'price' => $_taxHelper->getPrice($product, $rate['NonStandardRate'], false, null, null, null, null, false),
                        'price_with_tax' => $_taxHelper->getPrice($product, $rate['NonStandardRate'], true, null, null, null, null, false),
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * Generic method that returns an error response message
     * @param $message
     * @return array
     */
    protected function getErrorResponse($message)
    {
        return [
            'error' => true,
            'message' => $message,
        ];
    }
}
