<?php

class Dyna_Configurator_Helper_Rules extends Mage_Core_Helper_Abstract
{
    /** @var $productsMapping array */
    protected $productsMapping;
    /** @var $cache Dyna_Cache_Model_Cache */
    protected $cache;
    // Max iterations for cable rules
    protected static $maxIterations = 10;
    // Current iterations for post-condition rules
    protected $postIterations = 0;
    /** @var $cartHelper Dyna_Configurator_Helper_Cart */
    protected $cartHelper;

    // This is the gateway (helper) that implements getProductState, executePreconditionRules and executePostConditionRules methods
    /** @var  $gateWay Dyna_Configurator_Helper_Rules_Cable */
    protected $gateWay;
    protected $cartProducts = [];
    protected $packageType = null;
    protected $mandatoryProducts = [];
    protected $selectableAttributeData = [];
    protected $cableProducts = null;

    /**
     * Get all cable products based on the attribute set name: KD_Cable_Products
     * @return array
     */
    protected function getAllCableProducts($type)
    {
        /** @var Dyna_Configurator_Model_Cable_Tv $cableProductModel */
        $cableProductModel = Mage::getModel('dyna_configurator/' . $type);

        //Load product model collection filtered by attribute set id
        $cableProducts = $cableProductModel->getAllProducts();

        if (count($cableProducts)) {
            $this->cableProducts = Mage::helper('omnius_configurator')->toArray($cableProducts);
        } else {
            $this->cableProducts = [];
        }


        return $this->cableProducts;
    }

    /**
     * Return an array of orderable products based on current rules
     * @param array $selectedProducts  Product ids
     * @param string $type  Package type
     * @param int $websiteId
     * @return array
     */
    public function getRules($selectedProducts = [], $type = "cable_tv", $websiteId = 2)
    {
        $this->packageType = $type;
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $mandatory = $this->getCartHelper()->getMandatory($selectedProducts, $websiteId);

        $serviceProducts = [];
        // Get service expression compatibility rules
        if (Mage::getModel('customer/session')->getCustomer()) {
            $inputParams = [
                'customer' => Mage::getModel('customer/session')->getCustomer()
            ];

            /** @var Dyna_ProductMatchRule_Model_Rule $ruleModel */
            $ruleModel = Mage::getSingleton('dyna_productmatchrule/rule');
            $serviceRules = $ruleModel->loadServiceRules($inputParams);
            $serviceProducts = $ruleModel->getIndexedServiceRules($serviceRules, $websiteId);
        }

        // This will contain of list of compatible product ids retrieved from services and will be appended later to the allowed products list
        $ownHardwareRules = $this->getAvailableOwnHardware($selectedProducts);

        // Cable package flow
        if ($this->checkGateWay($type)) {
            $nowSelected = $selectedProducts;
            unset($selectedProducts);

            $currentIterationProducts = [];

            // Convert array of products to array of product states
            foreach ($nowSelected as $productId) {
                /** setting default states */;
                $visible = true;
                $selected = true;
                $changeable = true;
                $currentIterationProducts[] = $this->gateWay->getProductStateInstance($this->getProductSkuById($productId), $visible, $selected, $changeable);
            }

            // No products in cart, parse rules for all products
            $this->log('=== Executing precondition rules for all products of type: ' . $type . ' (nothing in cart) ===');

            $productsArray = $this->getAllCableProducts($type);

            // Check if any cable products are in the database
            if (empty($productsArray)) {
                $this->log("No " . $type . " products found in your database. Returning empty array.");

                return [];
            }

            // Convert array of products to array of product states
            foreach ($productsArray as $product) {
                if (in_array($product['entity_id'], $nowSelected)) {
                    continue;
                }

                // Setting default states from catalog for each product
                $visible = (int) !empty($product['marketable']);
                $selected = !empty($product['selectable']) ? $this->isProductSelectable((int) $product['selectable']) : 0;
                $changeable = isset($product['droppable']) ? !empty($product['droppable']) : 1;

                $currentIterationProducts[] = $this->gateWay->getProductStateInstance($product['sku'], $visible, $selected, $changeable);
            }

            if (empty($currentIterationProducts)) {
                return [
                    "error" => true,
                    "message" => $this->__("No products available for current store."),
                ];
            }

            $iterations = 1;
            do {
                $previousIterationProducts = $currentIterationProducts;

                // dump current selected products to log
                $this->log("****************************");
                $this->log("* STARTING NEW ITERATION: " . $iterations);
                $this->log("****************************");
                $this->dumpToLog($currentIterationProducts);

                // 1. Executing Cable Precondition Rules
                $currentIterationProducts = $this->executeCablePreconditionRules($currentIterationProducts);
                if (empty($currentIterationProducts)) {
                    $this->log('=== No remaining products available in current conditions ===');

                    return [
                        "error" => true,
                        "message" => $this->__("No products available for current store."),
                    ];
                }

                $this->log("****************************");
                $this->log("* RESULT FOR CablePreconditionRules ITERATION: " . $iterations);
                $this->log("****************************");
                // dump current selected products to log
                $this->dumpToLog($currentIterationProducts);

                // 2. Execute Omnius Compatibility Rules
                $currentIterationProducts = $this->executeOmniusCompatibilityRules($currentIterationProducts);
                if (empty($currentIterationProducts)) {
                    $this->log('=== No remaining products available in current conditions after executeOmniusCompatibilityRules ===');

                    return [
                        "error" => true,
                        "message" => $this->__("No products available for current store."),
                    ];
                }
                $this->log("****************************");
                $this->log("* RESULT FOR OmniusCompatibilityRules ITERATION: " . $iterations);
                $this->log("****************************");
                // dump current selected products to log
                $this->dumpToLog($currentIterationProducts);

                // 3. If states changed, execute (1) again
                if ($statesChanges = $this->areDifferent($previousIterationProducts, $currentIterationProducts)) {
                    $this->log('=== Found differences between input and output products. [Iteration: ' . $iterations . '] ===');
                } else {
                    $this->log('=== [RULES EXECUTION FINISHED] Got same result after applying Cable Precondition and Omnius Rules [Iteration: ' . $iterations . '] ===');
                }

                $iterations++;
            } while ($statesChanges && $iterations <= self::$maxIterations);

            $available = [];

            foreach ($currentIterationProducts as $product) {
                if ($product->isVisible()) {
                    $available[] = $this->getProductIdBySku($product->getProductId());
                }
            }
        } else {
            // Not a cable package; Get products compatibility rules
            $available = $this->executeOmniusCompatibilityRules($selectedProducts, true);
        }

        // Append products of type own to current rules and compatible smartcards and receiver if any found
        $allRules = array_merge($available, $serviceProducts, $ownHardwareRules);

        $result = [
            "rules" => $allRules,
            "mandatory" => $mandatory,
        ];

        return $result;
    }


    /**
     * Return a list of allowed products based on cart
     * @return array
     */
    public function getAvailableOwnHardware($selectedProducts)
    {
        $customAllowed = [];

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/session')->getCustomer();

        // Build allowed smart cards list if getTvEquipment service call has been done
        if ($receiver = $customer->getOwnReceiver()) {
            /** @var Dyna_Catalog_Model_Product $blankProduct */
            $blankProduct = Mage::getModel('catalog/product');
            if (!empty($smartSkus = $receiver->getData("SupportedSmartcardList"))) {
                foreach ($smartSkus as $sku) {
                    if ($productId = $blankProduct->getResource()->getIdBySku(trim($sku))) {
                        $customAllowed[] = $productId;
                    }
                }
            }
        }

        foreach ($selectedProducts as $productId) {
            /** @var Dyna_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')
                ->load($productId);

            // If customer has chosen own receiver, add own smartcard product as allowed
            if ($product->isOwnReceiver()) {
                $customAllowed[] = $product->getResource()->getIdBySku(Dyna_Catalog_Model_Type::OWN_SMARTCARD_SKU);
            }

            // If customer has chosen own smartcard, add own receiver to allowed list
            if ($product->isOwnSmartcard()) {
                $customAllowed[] = $product->getResource()->getIdBySku(Dyna_Catalog_Model_Type::OWN_RECEIVER_SKU);
            }
        }

        return $customAllowed;
    }

    /**
     * Needed for precondition and post condition rules processing
     * @param $productId
     * @return bool
     */
    public function isProductInCart($productId)
    {
        /** @todo Get products from actual car */

        return in_array($productId, $this->cartProducts);
    }

    /**
     * Add product to the list of cart products
     * @param $productId int
     */
    public function addProductInCart($productId)
    {
        if (!$this->isProductInCart($productId)) {
            $this->cartProducts[] = $productId;
        }
    }

    /**
     * Remove product from cart
     * @todo Update method to call configurator and remove product from cart
     * @param $productId
     */
    public function removeProductFromCart($productId)
    {
        if ($this->isProductInCart($productId)) {
            $position = array_search($productId, $this->cartProducts);
            unset($this->cartProducts[$position]);
        }
    }

    protected function isProductSelectable($selectableAttributeId)
    {
        if (!$this->selectableAttributeData) {
            $attribute = Mage::getModel('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'selectable');
            $allOptions = $attribute->getSource()->getAllOptions(true, true);
            foreach ($allOptions as $option) {
                if (!empty($option['value']) && !empty($option['label'])) {
                    $this->selectableAttributeData[$option['value']] = $option['label'];
                }
            }
        }

        return in_array(
            $this->selectableAttributeData[$selectableAttributeId],
            [
                "X (not selectable, selected)",
                "+'",
            ]);
    }

    /**
     * Update cart based on the result of rules processing
     * @param $productsList Omnius\RulesEnginePlugin\IProductState []
     */
    protected function updateCart($productList)
    {
        /** @todo Update method to access configurator and actually add product to cart */
        /** @var Omnius\RulesEnginePlugin\IProductState $productState */
        foreach ($productList as $productState) {
            if ($productState->isSelected()) {
                $this->addProductInCart(
                    $this->getProductIdBySku(
                        $productState->getProductId()
                    )
                );
            } else {
                $this->removeProductFromCart(
                    $this->getProductIdBySku(
                        $productState->getProductId()
                    )
                );
            }
        }
    }

    /*
     * Return mandatory products for current cart selection
     * @todo Implementation needed for mandatory products
     * @return array of product ids
     */
    public function getMandatory($productIds = [], $websiteId = null)
    {
        $cartHelper = $this->getCartHelper();
        $productIds = $cartHelper->getMandatory($productIds, $websiteId);

        foreach ($productIds as $productId) {
            if (!in_array($productId, $this->mandatoryProducts)) {
                $this->mandatoryProducts[] = $productId;
            }
        }
    }

    /**
     * Check whether or not a product is mandatory after executing Omnius compatibility rules
     */
    public function isProductMandatory($productId)
    {
        return true;
    }

    /**
     * Return product id for a given sku
     * @param $productSku string
     * @return $int
     */
    public function getProductIdBySku($productSku)
    {
        $mapping = $this->getProductMapping();
        return !empty($mapping[$productSku]) ? $mapping[$productSku] : $productSku;
    }

    /**
     * Return product sku for a given id
     * @param $productId int
     * @return $string
     */
    public function getProductSkuById($productId)
    {
        $sku = array_search($productId, $this->getProductMapping());
        return !empty($sku) ? $sku : $productId;
    }

    /**
     * @param $initialProducts Omnius\RulesEnginePlugin\IProductState[]
     * @param $processedProducts Omnius\RulesEnginePlugin\IProductState[]
     * @return boolean;
     */
    protected function areDifferent($initialProducts, $processedProducts)
    {
        if (count($initialProducts) != count($processedProducts)) {
            return true;
        }

        /** Building arrays for both input and output */
        $initialArray = [];
        foreach ($initialProducts as $productState) {
            $initialArray[$productState->getProductId()]['visible'] = $productState->isVisible();
            $initialArray[$productState->getProductId()]['changeable'] = $productState->isChangeable();
            $initialArray[$productState->getProductId()]['selected'] = $productState->isSelected();
        }

        /** Building arrays for both input and output */
        $processedArray = [];
        foreach ($processedProducts as $productState) {
            $processedArray[$productState->getProductId()]['visible'] = $productState->isVisible();
            $processedArray[$productState->getProductId()]['changeable'] = $productState->isChangeable();
            $processedArray[$productState->getProductId()]['selected'] = $productState->isSelected();
        }

        ksort($initialArray);
        ksort($processedArray);

        return !($initialArray == $processedArray);
    }

    /**
     * Execute Omnius Compatibility Rules
     * @param $productIds
     * @return array
     */
    protected function executeOmniusCompatibilityRules($productStates, $withIds = false)
    {
        if (! $withIds) {
            // SKIP Omnius Compatibility rules for Cable
            // TODO: clarify how can we handle this as the Compatibility rules return available products only if an ALLOWED rules exists between products
            // At this point there are no Compatibility rules for Cable products and this execution will return empty
            return $productStates;

            $productIds = $this->convertFromProductStateToIds($productStates);

            $this->log('=== Executing Omnius Compatibility Rules ===');
            $this->dumpToLog($productStates);
        } else {
            $productIds = $productStates;
        }

        $cartHelper = $this->getCartHelper();
        if (empty($availableProducts = $cartHelper->getCartRules($productIds))) {
            $this->log('=== Skipping Omnius Compatibility Rules because there are no products visible and returning back the original input ===');
            return $productStates;
        }

        if (! $withIds) {
            return $this->convertFromIdsToProductState($availableProducts);
        }

//        /** Updating cart products list to access it later for defaulted products */
//        $this->updateCart($selectedProducts);
//
//        /** Getting defaulted products for current cart selection */
//        $selectedProducts += $this->getDefaultedProducts($this->cartProducts);
//        $this->updateCart($selectedProducts);
//
//        /** @todo call getMandatory products an update array of mandatory products */
//        $this->getMandatory($productIds);

        return $availableProducts;
    }

    /**
     * Returns the defaulted (selected) products for current product selection
     * @param $currentProducts
     */
    protected function getDefaultedProducts($currentProductsInCart)
    {
        $cartHelper = $this->getCartHelper();
        $newProducts = $cartHelper->getDefaultedProducts($currentProductsInCart);
        /** Add them to cart */
        $this->cartProducts += $newProducts;
        $newSelectedProducts = $this->convertFromIdsToProductState($newProducts);

        return $newSelectedProducts;
    }

    /**
     * This method will call executePreconditionRules() set on gateWay
     * @todo implement needed input (IRulesInput) to be parsed to IOmniusRules->executePreConditionRules
     * @param $productsList Omnius\RulesEnginePlugin\IProductState []
     * @return array of product ids
     */
    protected function executeCablePreconditionRules($productsList)
    {
        /** @todo Implement setCurrentProductStates on IRules input */
        $this->gateWay->setCurrentProductStates($productsList);
        $preconditionResult = $this->gateWay->executePreconditionRules();

        $this->updateCart($preconditionResult);

        return $preconditionResult;
    }

    /**
     * @param $productList
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     */
    protected function executeCablePostconditionRules($productList)
    {
        /** @todo Implement setCurrentProductStates on IRules input */
        $this->gateWay->setCurrentProductStates($productList);
        $postconditionResult = $this->gateWay->executePostconditionRules();

        return $postconditionResult;
    }

    /**
     * Get configurator cart helper
     * @return Dyna_Configurator_Helper_Cart
     */
    protected function getCartHelper()
    {
        if (!$this->cartHelper) {
            $this->cartHelper = Mage::helper('dyna_configurator/cart');
        }

        return $this->cartHelper;
    }

    /*
     * Convert a list of product states to a list of ids
     * Method will ignore products not visible
     */
    protected function convertFromProductStateToIds($productStateList)
    {
        $result = [];
        /** @var Omnius\RulesEnginePlugin\IProductState $productState */
        foreach ($productStateList as $productState) {
            if ($productState->isVisible()) {
                $result[] = $this->getProductIdBySku($productState->getProductId());
            }
        }

        return $result;
    }

    /*
     * Convert a list of product ids to a list of productStates
     * Method will ignore products not visible
     */
    protected function convertFromIdsToProductState($productIds)
    {
        $result = [];
        /** @var Omnius\RulesEnginePlugin\IProductState $productState */
        foreach ($productIds as $productId) {
            $visible = true;
            $selected = (bool)$this->isProductInCart($productId);
            $changeable = (bool)$this->isProductMandatory($productId);
            $result[] = $this->gateWay->getProductStateInstance($this->getProductSkuById($productId), $visible, $selected, $changeable);
        }

        return $result;
    }

    /**
     * @param $products
     * @throws Exception
     */
    public function executePostRules($products)
    {
        throw new Exception('Not implemented');
    }

    /**
     * @param Omnius_Checkout_Model_Sales_Quote $quote
     */
    public function executeCablePostRules($quote, $packageType = null)
    {
        $this->packageType = $packageType;
        if (!$this->checkGateWay($packageType)) {
            return ;
        }

        $selectedItems = $quote->getAllItems();
        $selectedIds = [];
        foreach ($selectedItems as $selectedItem) {

                $selectedIds[] = $selectedItem->getProduct()->getId();

        }

        $this->cartProducts = $selectedIds;

        $productList = $this->convertFromIdsToProductState($selectedIds);

        $this->log('=== CablePostconditionRules input (selected products) ===');
        $this->dumpToLog($productList, false);

        $results = $this->executeCablePostconditionRules($productList);

        $this->log('=== CablePostconditionRules result ===');
        $this->dumpToLog($results, false);

        foreach ($results as $result) {
            if ($result->isSelected()) {
                $sel[] = $this->getProductIdBySku($result->getProductId());
            }
        }
        $this->postIterations++;

        if ($this->areDifferent($productList, $results) && $this->postIterations <= self::$maxIterations) {
            // Set all the new products as selected
            $this->cartProducts = $sel;

            // Remove all old products
            $items = $quote->getAllItems();
            foreach ($items as $item) {
                $item->isDeleted(1);
            }

            // Add all new products
            foreach ($sel as $addId) {
                $prod = Mage::getModel('catalog/product')->load($addId);
                $this->log('=== ADD PROD WITH SKU: ' . $prod->getSku() . ' ===');
                $quote->addProduct($prod, 1);
            }

            $items = $quote->getAllItems();

            // TODO: Only works with one package
            foreach ($items as $item) {
                $item->setPackageId(1)
                    ->save();
            }

            $quote->setNeedsRecollectPostTotals(true);
            $this->log('=== Processing continues ===');
        } else {
            $quote->setNeedsRecollectPostTotals(false);
            $this->log('=== Processing stops ===');
        }
    }

    /**
     * @param $type
     * @return bool
     */
    public function checkGateWay($type)
    {
        if (stripos($type, 'cable') !== false) {
            /** @var Dyna_Configurator_Helper_Rules_Cable $this->gateWay */
            $this->gateWay = Mage::helper('dyna_configurator/rules_cable');

            return true;
        }

        return false;
    }

    /**
     * Returns an array of $sku => $id for identifying products
     * @return array
     */
    protected function getProductMapping()
    {
        if (empty($this->productsMapping)) {
            /** Try loading mapping list from cache */
            $key = serialize([strtolower(__METHOD__), $this->packageType]);

            if ($list = unserialize($this->getCache()->load($key))) {
                /** save it on current instance from cache */
                $this->productsMapping = $list;
            } else {
                $result = [];
                /** @var array $allCableProducts */
                $cableProducts = $this->getAllCableProducts($this->packageType);
                foreach ($cableProducts as $product) {
                    $result[$product['sku']] = $product['entity_id'];
                }
                /** save it on current instance */
                $this->productsMapping = $result;
                /** cache it */
                $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG], $this->getCache()->getTtl());
            }
        }

        return $this->productsMapping;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->cache;
    }

    /**
     * @param $productStateList
     */
    protected function dumpToLog($productStateList, $onlyCount = true)
    {
        $this->log("Current available products: " . count($productStateList));

        $visible = $selected = $changeable = 0;

        foreach ($productStateList as $productState) {
            if ($productState->isVisible()) {
                $visible++;
            }
            if ($productState->isSelected()) {
                $selected++;
            }
            if ($productState->isChangeable()) {
                $changeable++;
            }

            if (! $onlyCount) {
                $text = '';
                $text .= "\t" . $productState->getProductId();
                $text .= " [" . (int) $productState->isVisible();
                $text .= " : " . (int) $productState->isSelected();
                $text .= " : " . (int) $productState->isChangeable();
                $text .= "]";
                $this->log($text);
            }
        }

        $this->log(">>> Visible: " . $visible);
        $this->log(">>> Selected: " . $selected);
        $this->log(">>> Changeable: " . $changeable);
    }

    /**
     * Log messages to custom file: var/log/OmniusRulesTest.log
     * @param $message
     */
    public function log($message)
    {
        Mage::log($message, null, 'CableOmniusRules.log', true);
    }

}
