<?php

use Mockery\MockInterface;
use CableRulesPlugin\CableRulesPluginFactory;
use CableRulesPlugin\RulesEngine\Context\RulesEngineDefaults\RulesEngineContextDefaults;
use Omnius\RulesEnginePlugin\Input\ICustomer;
use Omnius\RulesEnginePlugin\Input\IHardware;
use Omnius\RulesEnginePlugin\Input\IProductStateSelection;
use Omnius\RulesEnginePlugin\Input\IRulesInput;
use Omnius\RulesEnginePlugin\Input\IServiceAddress;
use Omnius\RulesEngineCode\ProductState;

class Dyna_Configurator_Helper_Rules_Cable extends Mage_Core_Helper_Abstract
{
    /** @var $iRulesInput MockInterface|IRulesInput */
    protected $iRulesInput;
    /** @var $customer ICustomer */
    protected $customer;
    /** @var $serviceAddress IServiceAddress */
    protected $serviceAddress;
    /** @var $currentProductStates array|IProductStateSelection */
    protected $currentProductStates = [];

    /**
     * Set current customer input for cable rules
     * @param Customer $customer
     * @return Dyna_Configurator_Helper_Rules_Cable
     */
    public function setCustomer(Customer $customer) : self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @param ServiceAddress $address
     * @return Dyna_Configurator_Helper_Rules_Cable
     */
    public function setServiceAddress(ServiceAddress $address) : self
    {
        $this->serviceAddress = $address;

        return $this;
    }

    /**
     * Set current product state selection
     * @return Dyna_Configurator_Helper_Rules_Cable
     */
    public function setCurrentProductStates($currentProductStates)
    {
        $this->currentProductStates = $currentProductStates;

        return $this;
    }

    /**
     * Return mock up of IOmniusRules
     * @return mixed
     */
    public function getProductStateInstance($productId, $visible = false, $selected = false, $changeable = false)
    {
        $productState = new ProductState($productId, $visible, $selected, $changeable);

        return $productState;
    }

    /**
     * This method calls for CableRulesPlugin\executePreConditionRules() parsing the current state products
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     * @throws Exception
     * @todo create setCurrentSelection method
     */
    public function executePreconditionRules() : array
    {
        if (!$this->currentProductStates) {
            throw new \Exception("No product state selection defined.");
        }

        // Setup IRulesInput
        $this->setUp();

        $productStates = $this->currentProductStates;

        if ($this->isArtifactEnabled()) {
            $cableRulesPlugin = CableRulesPluginFactory::createPlugin();
            $cableRulesPlugin->setLogger(Mage::getModel('aleron75_magemonolog/logger'));

            $productStates = $cableRulesPlugin->executePreConditionRules($this->iRulesInput);
        }

        return $productStates;
    }

    /**
     * This method calls for CableRulesPlugin\executePostConditionRules() parsing the current state products
     * @return array|\Omnius\RulesEnginePlugin\IProductState[]
     * @throws Exception
     */
    public function executePostconditionRules()
    {
        if (!$this->currentProductStates) {
            throw new \Exception("No product state selection defined.");
        }

        // Setup IRulesInput
        $this->setUp();

        $productStates = $this->currentProductStates;

        if ($this->isArtifactEnabled()) {
            $cableRulesPlugin = CableRulesPluginFactory::createPlugin();
            $cableRulesPlugin->setLogger(Mage::getModel('aleron75_magemonolog/logger'));

            $productStates = $cableRulesPlugin->executePostConditionRules($this->iRulesInput);
        }

        return $productStates;
    }

    /**
     * Check if the Cable Rules Artifact is enabled in System-Configuration-Omnius Settings-Default settings-Cable rules artifact settings
     * @return boolean
     */
    public function isArtifactEnabled()
    {
        return Mage::getStoreConfig('omnius_general/cable_rules/enabled');
    }

    /**
     * Setup Mock objects for IRulesInput
     * The only real data are the catalog products ($this->currentProductStates)) returned by getCurrentProductStates method
     * @todo: replace with real data from session
     */
    protected function setUp()
    {
        $this->iRulesInput = Mockery::mock(IRulesInput::class);
        $iCustomer = Mockery::mock(ICustomer::class);
        $iServiceAddress = Mockery::mock(IServiceAddress::class);
        $iProductStateSelection = Mockery::mock(IProductStateSelection::class);

        $this->iRulesInput->shouldReceive("getCustomer")->withNoArgs()->andReturn($iCustomer);
        $this->iRulesInput->shouldReceive("getServiceAddress")->withNoArgs()->andReturn($iServiceAddress);
        $this->iRulesInput->shouldReceive("getProductSelection")->withNoArgs()->andReturn($iProductStateSelection);

        $iCustomer->shouldReceive("getCustomerType")->withNoArgs()->andReturn(ICustomer::TYPE_PRIVATE_PERSON);

        $inputMarketabilityMap = [
            IServiceAddress::SERVICE_CATEGORY_KAD => IServiceAddress::MARKETABILITY_DIRECT_SALE,
            IServiceAddress::SERVICE_CATEGORY_KAA => IServiceAddress::MARKETABILITY_DIRECT_SALE,
            IServiceAddress::SERVICE_CATEGORY_KAI => IServiceAddress::MARKETABILITY_DIRECT_SALE,
            IServiceAddress::SERVICE_CATEGORY_KUD => IServiceAddress::MARKETABILITY_DIRECT_SALE,
        ];

        $iServiceAddress->shouldReceive("getServiceCategoryMarketability")->withNoArgs()->andReturn($inputMarketabilityMap);
        $iServiceAddress->shouldReceive("isMMDAvailable")->withNoArgs()->andReturn(true);
        $iServiceAddress->shouldReceive("isVODAvailable")->withNoArgs()->andReturn(true);
        $iServiceAddress->shouldReceive("isSelfInstallAvailable")->withNoArgs()->andReturn(true);
        $iServiceAddress->shouldReceive("isSatAvailable")->withNoArgs()->andReturn(true);
        $iServiceAddress->shouldReceive("getAvailableBandWidth")->withNoArgs()->andReturn(1000);
        $iServiceAddress->shouldReceive("getNetworkOperators")->withNoArgs()
            ->andReturn(RulesEngineContextDefaults::getDefaultNetworkOperatorsArray());

        $iProductStateSelection->shouldReceive("getHardware")->withNoArgs()->andReturn(Mockery::mock(IHardware::class));
        $iProductStateSelection->shouldReceive("getCurrentProductStates")->withNoArgs()->andReturn($this->currentProductStates);
        $iProductStateSelection->shouldReceive("technicianNeeded")->withNoArgs()->andReturn(false);
    }
}
