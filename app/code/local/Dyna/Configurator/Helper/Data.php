<?php

class Dyna_Configurator_Helper_Data extends Omnius_Configurator_Helper_Data
{
    /**
     * @param Omnius_Checkout_Model_Sales_Quote_Item $item
     * @return array
     */
    public function getBlockRowData(Omnius_Checkout_Model_Sales_Quote_Item $item)
    {
        /** @var Dyna_Catalog_Model_Product $product */
        $product = $item->getProduct();
        $originalItem = $item;
        $result = array(
            'name' => $product->getName(),
            'hashed' => false,
        );

        if ($product->isPromo()) {
            $result = $this->getPromoProductBlock($item, $product, $originalItem, $result);
        } elseif ($item->isPromo()) {
            $result['price'] = Mage::helper('tax')->getPrice($product, $item->getRowTotal(), false);
            $result['priceTax'] = Mage::helper('tax')->getPrice($product, $item->getRowTotalInclTax(), false);
            $result['maf'] = Mage::helper('tax')->getPrice($product, $item->getMaf(), false);
            $result['mafTax'] = Mage::helper('tax')->getPrice($product, $item->getMafInclTax(), true);
        }

        if ($product->isPromo() || $item->isPromo()) {
            return $result;
        }

        // add maf
        if (!(
            $product->isAccessory() ||
            $product->isDevice()
        )
        ) {
            $result['maf'] = (float) $item->getMaf();
            $result['mafTax'] = (float) $item->getMafInclTax();
        }


        $result['price'] = (float) $item->getPrice();
        $result['priceTax'] = (float) $item->getPriceInclTax();


        if (!($product->isOption() || $product->isPromo())
        ) {
            $result['price'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getRowTotalInclTax(), //todo something with attr price
                false
            );
            $result['priceTax'] = Mage::helper('tax')->getPrice(
                $product,
                $item->getHasMixMatch() ? ($product->getSpecialPrice() ?: $product->getPrice()) : $item->getRowTotalInclTax(),
                true
            );
        }

        if ($product->isAccessory()) {
            $result['discount'] = true;
        }

        return $result;
    }

    /**
     * Get customer SOHO status
     * return string
     */
    public function isCustomerSoho()
    {
        $session = Mage::getSingleton('customer/session');
        return $session->getIsSoho();
    }
}
