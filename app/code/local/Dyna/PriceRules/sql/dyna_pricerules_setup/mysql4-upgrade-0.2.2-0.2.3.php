<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->addAttribute("customer", "lifecycledetail", array(
    "type" => "text",
    "backend" => "",
    "label" => "Value",
    "input" => "multiselect",
    "source" => "dyna_pricerules/eav_entity_attribute_source_lifecycledetail",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    "note" => ""

));

$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "lifecycledetail");


$used_in_forms = array();

$used_in_forms[] = "adminhtml_customer";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_lifecycledetail", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100);
$attribute->save();


$installer->endSetup();

