<?php

/**
 * Installer for adding new fields to Shopping Cart Price Rules (OMNVFDE-383)
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable("salesrule/rule"), "maf_discount", [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'length' => '10,4',
    'comment' => 'Maf discount value (price)',
]);
$installer->getConnection()->addColumn($installer->getTable("salesrule/rule"), "maf_discount_percent", [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'length' => '10,4',
    'comment' => 'Maf discount percent',
]);
$installer->getConnection()->addColumn($installer->getTable("salesrule/rule"), "usage_discount", [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'length' => '10,4',
    'comment' => 'Usage discount value (price)',
]);
$installer->getConnection()->addColumn($installer->getTable("salesrule/rule"), "usage_discount_percent", [
    'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
    'length' => '10,4',
    'comment' => 'Usage discount percent',
]);

$installer->endSetup();
