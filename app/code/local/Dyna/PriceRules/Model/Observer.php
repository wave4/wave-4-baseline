<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_PriceRules_Model_Observer
 */
class Dyna_PriceRules_Model_Observer extends Omnius_PriceRules_Model_Observer
{
    /**
     * @param $observer
     * @return mixed
     */
    public function addConditionToSalesRule($observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array) $additional->getConditions();

        $conditions = array_merge_recursive($conditions, array(
            array('label' => Mage::helper('pricerules')->__('Channel'), 'value' => 'pricerules/condition_channel'),
            array('label' => Mage::helper('pricerules')->__('Dealer'), 'value' => 'pricerules/condition_dealer'),
            array('label' => Mage::helper('pricerules')->__('Dealer Group'), 'value' => 'pricerules/condition_dealergroup'),
            array('label' => Mage::helper('pricerules')->__('Value'), 'value' => 'pricerules/condition_value'),
            array('label' => Mage::helper('pricerules')->__('Campaign'), 'value' => 'pricerules/condition_campaign'),
            array('label' => Mage::helper('pricerules')->__('Customer segment'), 'value' => 'pricerules/condition_customersegment'),
            array('label' => Mage::helper('pricerules')->__('Paidcode'), 'value' => 'pricerules/condition_paidcode'),
            array('label' => Mage::helper('pricerules')->__('Lifecycle'), 'value' => 'pricerules/condition_lifecycle'),
            array('label' => Mage::helper('pricerules')->__('Lifecycle detail'), 'value' => 'dyna_pricerules/condition_lifecycledetail'),
            array('label' => Mage::helper('pricerules')->__('Brand'), 'value' => 'dyna_pricerules/condition_brand'),
            array('label' => Mage::helper('pricerules')->__('Dealer allowed campaigns'), 'value' => 'dyna_pricerules/condition_dealerCampaign'),

        ));

        $additional->setConditions($conditions);
        $observer->setAdditional($additional);

        return $observer;
    }

    /**
     * Process sales rule form creation
     * @param   Varien_Event_Observer $observer
     * @return  Dyna_PriceRules_Model_Observer
     */
    public function handleFormCreation($observer)
    {
        /** @var Varien_Data_Form_Element_Fieldset $fldSet */
        $fldSet = $observer->getForm()->getElement('action_fieldset');

        $fldSet->addField('discount_period_amount', 'text', array(
            'name' => 'discount_period_amount',
            'label' => Mage::helper('adminhtml')->__('Discount Period'),
            'note' => Mage::helper('adminhtml')->__('Period value for time related discount'),
        ),
            'discount_amount'
        );

        $fldSet->addField('discount_period', 'select', array(
            'name' => 'discount_period',
            'label' => Mage::helper('adminhtml')->__('Period type for the discount'),
            'note' => Mage::helper('adminhtml')->__('days/weeks/months'),
            'options' => [null => '', 'days' => 'days', 'weeks' => 'weeks', 'months' => 'months'],
        ),
            'discount_period_amount'
        );

        /** Adding another four fields for Shopping Cart Price Rules (OMNVFDE-383) */
        $fldSet->addField('maf_discount', 'text', array(
            'name' => 'maf_discount',
            'label' => Mage::helper('adminhtml')->__('Maf Discount'),
            'note' => Mage::helper('adminhtml')->__('Maf discount as price'),
        ),
            'maf_discount'
        );
        $fldSet->addField('maf_discount_percent', 'text', array(
            'name' => 'maf_discount_percent',
            'label' => Mage::helper('adminhtml')->__('Maf Discount Percent'),
            'note' => Mage::helper('adminhtml')->__('Maf discount as percent'),
        ),
            'maf_discount_percent'
        );
        $fldSet->addField('usage_discount', 'text', array(
            'name' => 'usage_discount',
            'label' => Mage::helper('adminhtml')->__('Usage Discount'),
            'note' => Mage::helper('adminhtml')->__('Usage discount as price'),
        ),
            'usage_discount'
        );
        $fldSet->addField('usage_discount_percent', 'text', array(
            'name' => 'usage_discount_percent',
            'label' => Mage::helper('adminhtml')->__('Usage Discount Percent'),
            'note' => Mage::helper('adminhtml')->__('Usage discount as percent'),
        ),
            'usage_discount_percent'
        );


        return $this;
    }
}
