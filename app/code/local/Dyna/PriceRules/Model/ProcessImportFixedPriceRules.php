<?php

class Dyna_PriceRules_Model_ProcessImportFixedPriceRules extends Varien_Object
{
    protected $_rawData;
    protected $_processRule;
    protected $_currentCondition = 1;
    protected $_currentAction = 1;
    protected $_helper;
    protected $_error;

    const CONDITION_ACTION_AND = "and";
    const CONDITION_ACTION_OR = "or";
    const CONDITION_ACTION_ONE_OF = "()";
    const CONDITION_ACTION_IS = "==";
    const SKU_DEMILIMITER_FOR_IS_ONE_OF = ",";

    /**
     * Dyna_PriceRules_Model_ProcessImportPriceRules constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setRawData($data)
    {
        $this->_rawData = $data;
    }

    public function setHelper($helper)
    {
        $this->_helper = $helper;
    }

    public function process()
    {
        $this->_processSkusConditionsRules();
        $this->_processCategoryConditionsRules();
        $this->_processDealerCampaigns();
        $this->_processLifeCycle();
        $this->_processLifeCycleDetail();
        $this->_processCustomerSegment();
        $this->_processDealer();
        $this->_processAction();
        $this->_processActionScopeProduct();
        $this->_processActionScopeCategory();

        return $this;
    }

    protected function _processSkusConditionsRules()
    {
        /** Adding product combination condition */
        if (isset($this->_rawData['based_on_a_product']) && !empty(trim($this->_rawData['based_on_a_product']))) {
            $basedOnAProduct = trim($this->_rawData['based_on_a_product']);
            if (stripos($basedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($this->_rawData['based_on_a_product']), true, true);
            } else {
                $this->setSimpleConditionProductRule($basedOnAProduct, self::CONDITION_ACTION_IS);
            }
        }

        return $this;
    }

    /**
     * Set Sku/Category Ids conditions that are based on multile skus/ids
     * The skus/ids separated by AND will be in conditions written on the same level in the tree;
     * The skus/ids separated by Or will be written in the same condition as "is one of A, B, C"
     * Example in the csv we have "I1208_YI003_v017K_LTE AND I1208_YI003_V018P_LTE"
     *
     * @param string $condition
     * @param bool $productCondition // determine if is a product condition or a catgeory condition
     * @param bool $conditionsSection // determine if is a condition from the action section or from the condition section
     */
    protected function _setRuleWithOrAnd($condition, $productCondition = true, $conditionsSection = true)
    {
        // find if the sku/ids are like A and B or C and D or F or E and H
        $itemsExplodedFromAnd = $this->explodeByAnd($condition);
        // if the sku/ids are with "and"
        if ($itemsExplodedFromAnd) {
            /**
             * for each exploded group delimited by "and" find if it contains a group delimited by "or"
             * for the initial group A and B or C and D or F or E and H => the following groups wil be exploded by and:
             * A
             * B or C
             * D or F or E
             * H
             */
            foreach ($itemsExplodedFromAnd as $itemExplodedFromAnd) {
                $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($itemExplodedFromAnd);
                $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
            }
        } // the skus can be in the form A or B or C or D
        else {
            $itemsToAddWithCondition = $this->findItemsToAddFromAPossibleOrGroup($condition);
            $this->addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition);
        }
    }

    private function addProductOrCategoryActionOrCondition($productCondition, $conditionsSection, $itemsToAddWithCondition)
    {
        if($productCondition) {
            if($conditionsSection) {
                $this->setSimpleConditionProductRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
            else {
                $this->setSimpleActionProductRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
        }
        else {
            if($conditionsSection) {
                $this->setSimpleConditionCategoryRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
            else {
                $this->setSimpleActionCategoryRule($itemsToAddWithCondition['itemsToAdd'], $itemsToAddWithCondition['condition']);
            }
        }
    }

    /**
     * Explode a string that may contain skus/ids separated by AND;
     * If is not a string with AND => return NULL;
     *
     * @param string $stringToExplode
     * @return array|null
     */
    private function explodeByAnd($stringToExplode)
    {
        $explodedArrayWithAnd = null;
        if (stripos($stringToExplode, self::CONDITION_ACTION_AND) !== false) {
            $explodedArrayWithAnd = preg_split("/".self::CONDITION_ACTION_AND."/i", trim($stringToExplode));
        }
        return $explodedArrayWithAnd;
    }

    /**
     * Find individual sku or category ID to add in a condition from a string that can contain a single id/sku OR
     * from a string that can contain multiple ids/skus separated by or
     * @param string $itemsGroup
     * @return array
     */
    private function findItemsToAddFromAPossibleOrGroup($itemsGroup)
    {
        /**
         * go through each group delimited previously by "and" to find if it contains items are delimited by "or";
         * if it contains replace the "or" with "," in order to add them in the condition as "one of A,B,C)
         * for the previous example the groups B or C will become "B,C" and the group "D or F or E" will become "D, F, E"
         */
        if(stripos($itemsGroup, self::CONDITION_ACTION_OR) !== false) {
            $itemsToAdd = str_ireplace(self::CONDITION_ACTION_OR, self::SKU_DEMILIMITER_FOR_IS_ONE_OF, $itemsGroup);
            $condition = self::CONDITION_ACTION_ONE_OF;
        }
        /**
         * if it not contains items delimited by "or" it means that is a single sku/id and we will trim the extra spaces;
         * for the previous example the group formed only from A will be added; and the group formed only from "H" will be added;
         */
        else {
            $itemsToAdd = trim($itemsGroup);
            $condition = self::CONDITION_ACTION_IS;
        }

        return ['itemsToAdd' => $itemsToAdd, 'condition' => $condition];
    }

    private function setSimpleConditionProductRule($skus, $condition = "()")
    {
        if (empty($this->_processRule['conditions'])) {
            $this->_processRule['conditions'][1] = array(
                'type' => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
            'type' => 'salesrule/rule_condition_product_found',
            'value' => 1,
            'aggregator' => 'all',
            'new_child' => '',
        );

        $this->_processRule['conditions']["1" . "--" . $this->_currentCondition . "--1"] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'sku',
            'operator' => $condition,
            'value' => $skus,
        );

        $this->_currentCondition++;
    }


    protected function _processCategoryConditionsRules()
    {
        /** Adding product combination condition */
        if (isset($this->_rawData['based_on_category']) && !empty(trim($this->_rawData['based_on_category']))) {
            $basedOnACategory = trim($this->_rawData['based_on_category']);
            if (stripos($basedOnACategory, self::CONDITION_ACTION_AND) !== false ||
                stripos($basedOnACategory, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($this->_rawData['based_on_category']), false, true);
            } else {
                $this->setSimpleConditionCategoryRule($basedOnACategory);
            }
        }

        return $this;
    }

    private function setSimpleConditionCategoryRule($category, $condition = self::CONDITION_ACTION_ONE_OF)
    {
        if (empty($this->_processRule['conditions'])) {
            $this->_processRule['conditions'][1] = array(
                'type' => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
            'type' => 'salesrule/rule_condition_product_found',
            'value' => 1,
            'aggregator' => 'all',
            'new_child' => '',
        );

        $this->_processRule['conditions']["1" . "--" . $this->_currentCondition . "--1"] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'category_ids',
            'operator' => $condition,
            'value' => $category,
        );

        $this->_currentCondition++;
    }

    protected function _processDealerCampaigns()
    {
        if (isset($this->_rawData['based_on_sales_force_id']) && !empty($this->_rawData['based_on_sales_force_id'])) {
            $this->_rawData['based_on_sales_force_id'] = trim($this->_rawData['based_on_sales_force_id']);
            $allowedCampaign = str_replace("VOID is allowed for campaign ", "", $this->_rawData['based_on_sales_force_id']);
            if (!empty($allowedCampaign)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_dealerCampaign',
                    'attribute' => 'dealer',
                    'operator' => '()',
                    'value' => $allowedCampaign,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processLifeCycle()
    {
        if (isset($this->_rawData['based_on_a_lifecycle']) && !empty($this->_rawData['based_on_a_lifecycle'])) {
            $lifeCycle = trim($this->_rawData['based_on_a_lifecycle']);
            if (!empty($lifeCycle)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_lifecycle',
                    'attribute' => 'lifecycle',
                    'operator' => '==',
                    'value' => $lifeCycle,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processLifeCycleDetail()
    {
        if (isset($this->_rawData['based_on_a_lifecycle_detail']) && !empty($this->_rawData['based_on_a_lifecycle_detail'])) {
            $lifeCycleDetail = trim($this->_rawData['based_on_a_lifecycle_detail']);
            if (!empty($lifeCycleDetail)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_lifecycledetail',
                    'attribute' => 'lifecycledetail',
                    'operator' => '==',
                    'value' => $lifeCycleDetail,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processDealer()
    {
        if (isset($this->_rawData['based_on_dealer_code']) && !empty($this->_rawData['based_on_dealer_code'])) {
            $dealerCode = trim($this->_rawData['based_on_dealer_code']);
            if (!empty($dealerCode)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_dealer',
                    'attribute' => 'dealer',
                    'operator' => '==',
                    'value' => $dealerCode,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processAction()
    {
        if (isset($this->_rawData['action']) && strpos($this->_rawData['action'], "Add product where sku") !== false) {
            $this->__addProduct();
        } elseif (strtolower($this->_rawData['action']) == "discount") {
            $this->_addDiscount();
        } else {
            /** Error */
            $this->_helper->log(new Exception("Unknown action type for: " . $this->_rawData['rule_name']));
            $this->_error = 1;
        }

        return $this;
    }

    protected function _processActionScopeProduct()
    {
        /** Adding product combination condition */
        if (isset($this->_rawData['action_scope_product']) && !empty(trim($this->_rawData['action_scope_product']))) {
            $actionBasedOnAProduct = trim($this->_rawData['action_scope_product']);
            if (stripos($actionBasedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($actionBasedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($this->_rawData['action_scope_product']), true, false);
            } else {
                $this->setSimpleActionProductRule($actionBasedOnAProduct);
            }
        }

        return $this;
    }

    protected function _processActionScopeCategory()
    {
        /** Adding product combination condition */
        if (isset($this->_rawData['action_scope_category']) && !empty(trim($this->_rawData['action_scope_category']))) {
            $actionBasedOnAProduct = trim($this->_rawData['action_scope_category']);
            if (stripos($actionBasedOnAProduct, self::CONDITION_ACTION_AND) !== false ||
                stripos($actionBasedOnAProduct, self::CONDITION_ACTION_OR) !== false
            ) {
                $this->_setRuleWithOrAnd(trim($this->_rawData['action_scope_category']), false, false);
            } else {
                $this->setSimpleActionCategoryRule($actionBasedOnAProduct);
            }
        }

        return $this;
    }

    private function setSimpleActionProductRule($product, $condition = self::CONDITION_ACTION_IS)
    {
        if (empty($this->_processRule['actions'])) {
            $this->_processRule['actions'][1] = array(
                'type' => 'salesrule/rule_condition_product_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        $this->_processRule['actions']["1--" . $this->_currentAction] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'sku',
            'operator' => $condition,
            'value' => $product,
        );

        $this->_currentAction++;
    }

    private function setSimpleActionCategoryRule($category, $condition = self::CONDITION_ACTION_IS)
    {
        if (empty($this->_processRule['actions'])) {
            $this->_processRule['actions'][1] = array(
                'type' => 'salesrule/rule_condition_product_combine',
                'aggregator' => 'all',
                'value' => 1,
                'new_child' => '',
            );
        }

        $this->_processRule['actions']["1--" . $this->_currentAction] = array(
            'type' => 'salesrule/rule_condition_product',
            'attribute' => 'category_ids',
            'operator' => $condition,
            'value' => $category,
        );

        $this->_currentAction++;
    }

    protected function _processCustomerSegment()
    {
        if (isset($this->_rawData['customer_segment']) && !empty(trim($this->_rawData['customer_segment']))) {
            $customerSegment = trim($this->_rawData['customer_segment']);
            $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                'type' => 'pricerules/condition_customersegment',
                'attribute' => 'customer_segment',
                'operator' => "==",
                'value' => $customerSegment,
            );

            $this->_currentCondition++;
        }

        return $this;
    }

    protected function __addProduct()
    {
        $sku = trim(str_replace("Add product where sku =", "", $this->_rawData['action']));

        $this->_processRule['simple_action'] = "ampromo_items";
        $this->_processRule['promo_sku'] = $sku;
    }

    protected function _addDiscount()
    {
        $discountPercent = str_replace("-", "", trim($this->_rawData['discount%']));
        $discountAmountNet = str_replace("-", "", trim($this->_rawData['discount_amount_net']));
        if ($discountPercent) {
            $this->_processRule['simple_action'] = 'by_percent';
            $this->_processRule['discount_amount'] = $discountPercent;
        } elseif ($discountAmountNet) {
            $this->_processRule['simple_action'] = 'by_fixed';
            $this->_processRule['discount_amount'] = $discountAmountNet;
        } else {
            /** Error */
            $this->_helper->log(new Exception("Both discount percent and discount amount net columns are empty: " . $this->_rawData['rule_name']));
            $this->_error = 1;
        }
    }

    public function getProcessedRules()
    {
        if (!$this->_error) {
            return $this->_processRule;
        } else {
            return false;
        }
    }
}
