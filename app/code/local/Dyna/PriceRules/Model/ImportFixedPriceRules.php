<?php

/**
 * Class Dyna_PriceRules_Model_ImportFixedPriceRules
 */
class Dyna_PriceRules_Model_ImportFixedPriceRules extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $_data;
    protected $_processor;
    protected $_headerColumns;
    protected $_csvDelimiter = ';';
    protected $_header;
    protected $_websites;
    protected $_logFileName;

    const ALL_WEBSITES = "*";

    /**
     * Dyna_PriceRules_Model_ImportFixedPriceRules constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_logFileName = "fixed_sales_rules_import";
        $this->_helper->setImportLogFile($this->_logFileName . '_' . date("Y-m-d") . '.log');
        $this->setDefaultWebsites();
    }

    /**
     * Reset rule information
     */
    public function clearData()
    {
        $this->_data = array(
            'rule_id' => null,
            'product_ids' => '',
            'name' => '',
            'description' => '',
            'is_active' => '',
            'website_ids' => '',
            'customer_group_ids' => array(1),
            'coupon_type' => '',
            'coupon_code' => '',
            'uses_per_coupon' => '',
            'uses_per_customer' => '',
            'from_date' => '',
            'to_date' => '',
            'sort_order' => '',
            'is_rss' => '',
            'conditions' => array(),
            'actions' => array(),
            'simple_action' => '',
            'discount_amount' => null,
            'discount_qty' => '',
            'discount_step' => '',
            'apply_to_shipping' => '',
            'simple_free_shipping' => '',
            'stop_rules_processing' => '',
            'store_labels' => array(),
            'discount_period_amount' => '',
            'discount_period' => '',
        );

        $this->_processor = null;
    }

    /**
     * @return $this
     */
    public function saveRule()
    {
        /** @var Mage_SalesRule_Model_Rule $model */
        $model = Mage::getModel('salesrule/rule');
        $data = $this->_data;

        $validateResult = $model->validateData(new Varien_Object($data));

        if ($validateResult == true) {
            if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
                && isset($data['discount_amount'])
            ) {
                $data['discount_amount'] = min(100, $data['discount_amount']);
            }

            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            }

            if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            }

            unset($data['rule']);

            $model->loadPost($data);

            $model->save();
        }

        return $this;
    }

    /**
     * @param $rawData
     * @return bool
     */
    public function process($rawData)
    {
        $rawData = array_combine($this->_header, $rawData);
        if (!array_filter($rawData)) {
            return false;
        }

        $ruleName = trim($rawData['rule_name']);
        $existing = Mage::getModel('salesrule/rule')->load($ruleName, 'name');

        if ($existing->getId()) {
            $this->_log("Updating rule: " . $rawData['rule_name']);
        } else {
            $this->_log("Saving rule: " . $rawData['rule_name']);
        }

        try {
            /** process basic data like name, start date, end date etc */
            $this->clearData();
            $this->_data['rule_id'] = $existing->getId() ?: null;
            $this->_data['name'] = $rawData['rule_name'];
            $this->_data['from_date'] = $this->formatDate($rawData['effective_date']);
            $this->_data['to_date'] = $this->formatDate($rawData['expiration_date']);
            $this->_data['is_active'] = 1;
            $this->_data['website_ids'] = $this->setWebsitesIds($rawData['channel']);
            if (!empty($rawData['is_promo'])) {
                $this->_data['is_promo'] = $this->setIsPromo($rawData['is_promo']);
            }
            if (!$this->_data['website_ids']) {
                return false;
            }
            if (!empty($rawData['discount_period'])) {
                $this->_data['discount_period'] = trim($rawData['discount_period']);
            }
            if (!empty($rawData['discount_period_amount'])) {
                $this->_data['discount_period_amount'] = (int) $rawData['discount_period_amount'];
            }
            if (!empty($rawData['description'])) {
                $this->_data['description'] = $rawData['description'];
            }

            /** leave rules processing to other method */
            $processedRule = $this->processRulesData($rawData);
            if(!$processedRule) {
                $this->_log("Skipping rule: " . $rawData['rule_name']);
            }
            else {
                $processedRule->saveRule();
            }
        } catch (Exception $e) {
            $this->_log($e->getMessage());
        }
    }

    /**
     * @param $rawData
     * @return bool|$this
     */
    public function processRulesData($rawData)
    {
        /** @var Dyna_PriceRules_Model_ProcessImportFixedPriceRules $this ->_processor */
        $this->_processor = Mage::getModel('dyna_pricerules/processImportFixedPriceRules');
        $this->_processor->setRawData($rawData);
        $this->_processor->setHelper($this->_helper);
        $conditionsAndActions = $this->_processor->process()->getProcessedRules();
        if(!$conditionsAndActions) {
            return false;
        }
        $this->_data = array_merge($this->_data, $conditionsAndActions);

        return $this;
    }

    /**
     * @param $date
     * @return bool|null|string
     */
    protected function formatDate($date)
    {
        /**
         * if date is set strip the character "-" from date
         * this is needed since the csv can contain instead of an empty cell the character "-"
         */
        if (trim($date) == "-" || !$date) {
            return null;
        }

        return date("Y-m-d", strtotime($date));
    }

    /**
     * @param $array
     * @param $dateFields
     * @return mixed
     */
    protected function _filterDates($array, $dateFields)
    {
        if (empty($dateFields)) {
            return $array;
        }
        $filterInput = new Zend_Filter_LocalizedToNormalized(array(
            'date_format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
        ));
        $filterInternal = new Zend_Filter_NormalizedToLocalized(array(
            'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
        ));

        foreach ($dateFields as $dateField) {
            if (array_key_exists($dateField, $array) && !empty($dateField)) {
                $array[$dateField] = $filterInput->filter($array[$dateField]);
                $array[$dateField] = $filterInternal->filter($array[$dateField]);
            }
        }
        return $array;
    }

    /**
     * @param $header
     */
    public function setHeader($header)
    {
        $this->_header = $header;
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided
     * (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @param $websites
     * @return bool
     */
    private function setWebsitesIds($websites)
    {
        $websitesData = explode(',', strtolower(trim($websites)));
        $websitesToAdd = [];
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*"
            // the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                return array_keys($this->_websites);
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->_websites)) {
                $websitesToAdd[$key] = $validWebsite;
            } elseif (array_key_exists($possibleWebsite, $this->_websites)) {
                // the website provided is id and we should store it
                $websitesToAdd[$key] = $possibleWebsite;
            } else {
                // not match can be found between the given website and a code or an id
                $this->_log('[ERROR] the website_id ' . print_r($websitesData, true) . ' is not present/valid. skipping row');

                return false;
            }
        }

        return $websitesToAdd;
    }

    /**
     * Get all DB websites and assign them to local variable
     */
    private function setDefaultWebsites()
    {
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $this->_websites[$website->getId()] = $website->getCode();
        }
    }

    /**
     * @param $isPromo
     * @return int
     */
    private function setIsPromo($isPromo)
    {
        switch (strtolower($isPromo)) {
            case 'ja':
            case 'y':
            case 'yes':
            case '1':
            case 'ok':
            case 'true':
                return 1;
            case 'nee':
            case 'n':
            case 'no':
            case '0':
            case 'x':
            case 'false':
                return 0;
            default:
                return 0;
        }
    }
}
