<?php

/**
 * Class Dyna_PriceRules_Model_Condition_DealerCampaign
 */
class Dyna_PriceRules_Model_Condition_DealerCampaign extends Omnius_PriceRules_Model_Condition_Abstract
{
    /**
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setupTextCondition('Dealer allowed campaigns', 'dealer');
        return $this;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Dyna_Agent_Model_Agent $agent */
        if ($agent = Mage::getSingleton('customer/session')->getAgent()) {
            /** @var Dyna_AgentDE_Model_Dealer $dealer */
            $dealer = $agent->getDealer();

            return $this->validateAttribute($dealer->getDealerCampaigns(false));
        }

        return false;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'grid';
    }
}
