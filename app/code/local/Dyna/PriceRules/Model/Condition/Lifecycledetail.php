<?php

/**
 * Class Dyna_PriceRules_Model_Condition_Lifecycledetail
 */
class Dyna_PriceRules_Model_Condition_Lifecycledetail extends Omnius_PriceRules_Model_Condition_Abstract
{
    const NEW_CUSTOMER_ACTIVATION = 'NewCustomerActivation';
    const DEBIT_TO_CREDIT_SWAP = 'DebitToCreditSwap';
    const IMPORTING = 'Importing';
    const CHANGE_TARIFF_OPTION = 'ChangeTariffOption';
    const CONTRACT_PROLONGATION = 'ContractProlongation';

    public function loadAttributeOptions()
    {
        $this->setupSelectCondition('Lifecycle detail', 'lifecycledetail');
        return $this;
    }

    public function getValueSelectOptions()
    {
        $segmentOptionsModel = Mage::getModel('dyna_pricerules/eav_entity_attribute_source_lifecycledetail');
        $options = $segmentOptionsModel->getAllOptions();
        return $this->setupOptions($options);
    }

    public function validate(Varien_Object $object)
    {
        $quote = $object->getQuote();
        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('package_id', $quote->getActivePackageId())
            ->getFirstItem();
        if ($package->getId()) {
            return $this->validateAttribute($package->getSaleType());
        }
        return false;
    }
}
