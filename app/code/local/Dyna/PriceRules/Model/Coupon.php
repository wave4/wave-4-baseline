<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_PriceRules_Model_Coupon extends Omnius_PriceRules_Model_Coupon
{
    /**
     * @param $options
     * @return bool
     */
    public function generateRule($options, &$count, &$countNotImpt)
    {
        $update = false;
        $rule = Mage::getModel('salesrule/rule')->getCollection()
            ->addFieldToFilter('name', $options['rule_name'])
            ->load();

        $ruleArr = $rule->getData();
        /** @var Mage_SalesRule_Model_Rule $rule */
        if (count($ruleArr) > 0 && !empty($ruleArr[0]['rule_id'])) {
            $update = true;
            $rule = Mage::getModel('salesrule/rule')->load($ruleArr[0]['rule_id']);
        } else {
            /** @var Mage_SalesRule_Model_Rule $rule */
            $rule = Mage::getModel('salesrule/rule');
            $rule->setName($options['rule_name']);
        }

        $rule->setDescription($options['description']);
        $rule->setFromDate($options['from_date']);//starting today
        if ($options['to_date'] != "") {
            $rule->setToDate($options['to_date']);//if you need an expiration date
        }

        $rule->setUsesPerCustomer($options['uses_per_customer']);//number of allowed uses for this coupon for each customer
        $rule->setIsActive($options['status']);

        $conditions = $this->_parseConditions($options);
        $actions = $this->_parseActions($options);

        $rule->setData('actions', $actions);
        $rule->setData('conditions', $conditions);

        $rule->setStopRulesProcessing($options['stop_rules_processing']);//set to 1 if you want all other rules after this to not be processed
        $rule->setIsAdvanced($options['is_advanced']);
        $rule->setProductIds($options['product_ids']);
        $rule->setSortOrder($options['sort_order']);// order in which the rules will be applied
        $rule->setSimpleAction($options['discount_type']);
        $rule->setDiscountAmount($options['discount_amount']);//the discount amount/percent. if SimpleAction is by_percent this value must be <= 100
        $rule->setDiscountQty($options['discount_qty']);//Maximum Qty Discount is Applied to
        $rule->setDiscountStep($options['discount_step']);//used for buy_x_get_y; This is X
        $rule->setSimpleFreeShipping($options['simple_free_shipping']);//set to 1 for Free shipping
        $rule->setApplyToShipping($options['apply_to_shipping']);//set to 0 if you don't want the rule to be applied to shipping
        $rule->setTimesUsed($options['times_used']);
        $rule->setIsRss($options['is_rss']);//set to 1 if you want this rule to be public in rss
        $rule->setCouponType($options['coupon_type']); // No Coupon=>1, Specific Coupon=>2, Auto=>3
        $rule->setUseAutoGeneration($options['use_auto_generation']);
        $rule->setUsesPerCoupon($options['user_per_coupon']);//number of allowed uses for this coupon

        $rule->setIsPromo($options['is_promo']);
        $rule->setPromoSku($options['promo_sku']);
        $rule->setLocked($options['locked']);

        $rule->setCouponCode($options['coupon_code']);
        $customerGroups = explode(',', $options['customer_group']);
        $rule->setCustomerGroupIds($customerGroups);//if you want only certain groups replace getAllCustomerGroups() with an array of desired ids

        $websites = explode(',', $options['website']);
        $rule->setWebsiteIds($websites);//if you want only certain websites replace getAllWbsites() with an array of desired ids

        // Set data to model
        $rule->loadPost($rule->getData());

        try {
            ($update) ? $countNotImpt ++ : $count++;
            return (bool)$rule->save();
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }
    }

    /**
     * @param $fp
     * @param $options
     * @param $count
     * @param $countNotImpt
     */
    protected function processFile($fp, &$count, &$countNotImpt)
    {
        $options = [];
        $i = 0;
        $fields = array_flip(fgetcsv($fp, 1024, ","));
        // import rules disregarding the status active or inactive
        while ($csv_line = fgetcsv($fp, 2048, ",")) {
            $options['rule_name'] = trim($csv_line[$fields['rule_name']]);
            $options['description'] = trim($csv_line[$fields['description']]);
            $options['from_date'] = $csv_line[$fields['from_date']];
            $options['to_date'] = $csv_line[$fields['to_date']];
            $options['uses_per_customer'] = (int)$csv_line[$fields['uses_per_customer']];
            $options['status'] = (int)$csv_line[$fields['status']];
            $options['conditions'] = $csv_line[$fields['conditions']] ? unserialize($csv_line[$fields['conditions']]) : [];
            $options['actions'] = $csv_line[$fields['actions']] ? unserialize($csv_line[$fields['actions']]) : [];
            $options['stop_rules_processing'] = (int)$csv_line[$fields['stop_rules_processing']];
            $options['is_advanced'] = (int)$csv_line[$fields['is_advanced']];
            $options['product_ids'] = trim($csv_line[$fields['product_ids']]);
            $options['sort_order'] = (int)$csv_line[$fields['sort_order']];
            $options['discount_type'] = $csv_line[$fields['discount_type']];
            $options['discount_amount'] = $csv_line[$fields['discount_amount']];
            $options['discount_qty'] = ($csv_line[$fields['discount_qty']] > 0) ? $csv_line[$fields['discount_qty']] : 0;
            $options['discount_step'] = (int)$csv_line[$fields['discount_step']];
            $options['simple_free_shipping'] = (int)$csv_line[$fields['simple_free_shipping']];
            $options['apply_to_shipping'] = (int)$csv_line[$fields['apply_to_shipping']];
            $options['times_used'] = (int)$csv_line[$fields['times_used']];
            $options['is_rss'] = (int)$csv_line[$fields['is_rss']];
            $options['coupon_type'] = (int)$csv_line[$fields['coupon_type']];
            $options['use_auto_generation'] = (int)$csv_line[$fields['use_auto_generation']];
            $options['user_per_coupon'] = $csv_line[$fields['user_per_coupon']];
            $options['is_promo'] = (bool)$csv_line[$fields['is_promo']];
            $options['promo_sku'] = trim($csv_line[$fields['promo_sku']]);
            $options['locked'] = (bool)$csv_line[$fields['locked']];

            $options['customer_group'] = $csv_line[$fields['customer_group']];
            $options['coupon_code'] = $csv_line[$fields['coupon_code']];
            $options['website'] = $csv_line[$fields['website']];

            $this->generateRule($options, $count, $countNotImpt);
            $i++;
        }
    }
}
