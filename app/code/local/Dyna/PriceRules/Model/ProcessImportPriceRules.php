<?php

class Dyna_PriceRules_Model_ProcessImportPriceRules extends Varien_Object
{
    protected $_rawData;
    protected $_processRule;
    protected $_currentCondition = 1;
    protected $_currentAction = 1;
    protected $_helper;
    protected $_error;
    const SKU_DEMILIMITER_FOR_IS_ONE_OF = ",";


    /**
     * Dyna_PriceRules_Model_ProcessImportPriceRules constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setRawData($data)
    {
        $this->_rawData = $data;
    }

    public function setHelper($helper)
    {
        $this->_helper = $helper;
    }

    public function process()
    {
        $this->_processSkus();
        $this->_processCategory();
        $this->_processDealerCampaigns();
        $this->_processLifeCycle();
        $this->_processLifeCycleDetail();
        $this->_processDealer();
        $this->_processAction();

        return $this;
    }

    protected function _processSkus()
    {
        /** Adding product combination condition */
        if (!empty($this->_rawData['based_on_a_product'])) {
            $skusList = $this->_processSkusCondition($this->_rawData['based_on_a_product'], $this->_currentCondition);

            if (!empty($skusList)) {
                foreach ($skusList as $skus) {
                    if (empty($this->_processRule['conditions'])) {
                        $this->_processRule['conditions'][1] = array(
                            'type' => 'salesrule/rule_condition_combine',
                            'aggregator' => 'all',
                            'value' => 1,
                            'new_child' => '',
                        );
                    }

                    $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                        'type' => 'salesrule/rule_condition_product_found',
                        'value' => 1,
                        'aggregator' => 'all',
                        'new_child' => '',
                    );
                    $operator = (strpos($skus, self::SKU_DEMILIMITER_FOR_IS_ONE_OF) !== false) ? "()" : "==";
                    $this->_processRule['conditions']["1" . "--" . $this->_currentCondition . "--1"] = array(
                        'type' => 'salesrule/rule_condition_product',
                        'attribute' => 'sku',
                        'operator' => $operator,
                        'value' => $skus,
                    );

                    $this->_currentCondition++;
                }
            }
        }

        return $this;
    }

    protected function _processSkusCondition($string, $ruleCount = 1)
    {
        $string = str_replace(" ", "", $string);
        $skuList = array();
        $skuListConditions = preg_split("/([sku:]|[andsku:])+/", $string, -1, PREG_SPLIT_NO_EMPTY);
        if (!empty($skuListConditions)) {
            foreach ($skuListConditions as $skus) {
                $skus = str_replace(["(", ")", "|"], ["", "", ", "], $skus);
                if ($skus != 'SKU') {
                    $skuList[] = $skus;
                }
            }
        }

        return $skuList;
    }

    protected function _processCategory()
    {
        if (!empty($this->_rawData['based_on_category'])) {
            $category = trim($this->_rawData['based_on_category']);
            if (!empty($category)) {
                if (empty($this->_processRule['conditions'])) {
                    $this->_processRule['conditions'][1] = array(
                        'type' => 'salesrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => 1,
                        'new_child' => '',
                    );
                }

                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'salesrule/rule_condition_product_found',
                    'value' => 1,
                    'aggregator' => 'all',
                    'new_child' => '',
                );

                $operator = (strpos($category, self::SKU_DEMILIMITER_FOR_IS_ONE_OF) !== false) ? "()" : "==";
                $this->_processRule['conditions']["1" . "--" . $this->_currentCondition . "--1"] = array(
                    'type' => 'salesrule/rule_condition_product',
                    'attribute' => 'category_ids',
                    'operator' => $operator,
                    'value' => $category,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processDealerCampaigns()
    {
        if (!empty($this->_rawData['based_on_sales_force_id'])) {
            $this->_rawData['based_on_sales_force_id'] = trim($this->_rawData['based_on_sales_force_id']);
            $allowedCampaign = str_replace("VOID is allowed for campaign ", "", $this->_rawData['based_on_sales_force_id']);
            if (! empty($allowedCampaign)) {
                $operator = (strpos($allowedCampaign, self::SKU_DEMILIMITER_FOR_IS_ONE_OF) !== false) ? "()" : "==";
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_dealerCampaign',
                    'attribute' => 'dealer',
                    'operator' => $operator,
                    'value' => $allowedCampaign,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processLifeCycle()
    {
        if (!empty($this->_rawData['based_on_a_lifecycle'])) {
            $lifeCycle = trim($this->_rawData['based_on_a_lifecycle']);
            if (!empty($lifeCycle)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_lifecycle',
                    'attribute' => 'lifecycle',
                    'operator' => '==',
                    'value' => $lifeCycle,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processLifeCycleDetail()
    {
        if (!empty($this->_rawData['based_on_a_lifecycle_detail'])) {
            $lifeCycleDetail = trim($this->_rawData['based_on_a_lifecycle_detail']);
            if (!empty($lifeCycleDetail)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'dyna_pricerules/condition_lifecycledetail',
                    'attribute' => 'lifecycledetail',
                    'operator' => '==',
                    'value' => $lifeCycleDetail,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processDealer()
    {
        if (!empty($this->_rawData['based_on_dealer_code'])) {
            $dealerCode = trim($this->_rawData['based_on_dealer_code']);
            if (!empty($dealerCode)) {
                $this->_processRule['conditions']["1--" . $this->_currentCondition] = array(
                    'type' => 'pricerules/condition_dealer',
                    'attribute' => 'dealer',
                    'operator' => '==',
                    'value' => $dealerCode,
                );

                $this->_currentCondition++;
            }
        }

        return $this;
    }

    protected function _processAction()
    {
        $this->_processRule['actions'][$this->_currentAction] = array(
            'type' => 'salesrule/rule_condition_product_combine',
            'aggregator' => 'all',
            'value' => 1,
            'new_child' => '',
        );

        if (strpos($this->_rawData['action'], "Add product where sku") !== false) {
            $this->__addProduct();
        } elseif (strtolower($this->_rawData['action']) == "discount") {
            $this->_addDiscount();
        } else {
            /** Error */
            $this->_helper->log(new Exception("Unknow action type for: " . $this->_rawData['rule_name']));
            $this->_error = 1;
        }

        return $this;
    }

    protected function __addProduct()
    {
        $sku = trim(str_replace("Add product where sku =", "", $this->_rawData['action']));

        $this->_processRule['simple_action'] = "ampromo_items";
        $this->_processRule['promo_sku'] = $sku;
    }

    protected function _addDiscount()
    {
        $discountPercent = str_replace("-", "", trim($this->_rawData['discount%']));
        $discountAmountNet = str_replace("-", "", trim($this->_rawData['discount_amount_net']));
        if ($discountPercent) {
            $this->_processRule['simple_action'] = 'by_percent';
            $this->_processRule['discount_amount'] = $discountPercent;
        } elseif ($discountAmountNet) {
            $this->_processRule['simple_action'] = 'by_fixed';
            $this->_processRule['discount_amount'] = $discountAmountNet;
        } else {
            /** Error */
            $this->_helper->log(new Exception("Both discount percent and discount amount net columns are empty: " . $this->_rawData['rule_name']));
            $this->_error = 1;
        }
    }

    public function getProcessedRules()
    {
        if (!$this->_error) {
            return $this->_processRule;
        } else {
            return [];
        }
    }
}
