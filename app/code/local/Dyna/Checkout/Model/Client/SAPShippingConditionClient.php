<?php

class Dyna_Checkout_Model_Client_SAPShippingConditionClient extends Dyna_Service_Model_Client
{
    protected $_forceUseStubs = false;

    const WSDL_CONFIG_KEY = "sap_shipping_condition/wsdl";
    const ENDPOINT_CONFIG_KEY = "sap_shipping_condition/usage_url";

    /**
     * @param array $params
     * @return array
     */
    public function executeCheckShippingCondition($params = []) : array
    {
        $params = $this->mapRequestFields($params);
        $this->setRequestHeaderInfo($params);

        $result = $this->CheckShippingCondtition($params);

        return $result;
    }

    /**
     * @param $params
     * @return array
     */
    private function mapRequestFields(array $params) : array
    {
        $parametersMapping = [
            'ShippingCondition' => [
                'Address' => [
                    'Postbox' => $params['postcode']
                ],
                'ShipmentStage' => [
                    'RequiredDeliveryDate' => $params['delivery_date']
                ]
            ]
        ];

        return $parametersMapping;
    }

    public function getPartyName()
    {
        return 'SAP';
    }
}
