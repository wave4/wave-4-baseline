<?php

class Dyna_Checkout_Model_ShippingFeeCalculateClient extends Dyna_Service_Model_Client
{
    protected $_forceUseStubs = false;

    const WSDL_CONFIG_KEY = "shipping_fee/wsdl";
    const ENDPOINT_CONFIG_KEY = "shipping_fee/usage_url";

    /**
     * @param array $params
     * @return array
     */
    public function executeShippingFeeCalculate($params = []) : array
    {
        $params = $this->mapRequestFields($params);
        $this->setRequestHeaderInfo($params);

        $result = $this->ShippingFeeCalculate($params);

        return $result;
    }

    /**
     * @param $params
     * @return array
     */
    private function mapRequestFields(array $params) : array
    {
        $parametersMapping =[];

        return $parametersMapping;
    }

    public function getPartyName()
    {
        return 'SAP';
    }
}