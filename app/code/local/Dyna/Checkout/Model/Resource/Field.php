<?php

class Dyna_Checkout_Model_Resource_Field extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_Field constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_checkout/fields', null);
    }
}
