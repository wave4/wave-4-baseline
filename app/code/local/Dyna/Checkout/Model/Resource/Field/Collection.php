<?php

class Dyna_Checkout_Model_Resource_Field_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Dyna_Checkout_Model_Resource_Field_Collection constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dyna_checkout/field');
    }

    /**
     * Returns all fields saved for current step
     * @param $currentStep
     * @param $quoteId
     * @return $this
     */
    public function getAllFieldsForStep($currentStep, $quoteId)
    {
        $this->addFieldToFilter("quote_id", ["eq" => $quoteId])
        ->addFieldToFilter("checkout_step", ["eq" => $currentStep]);

        return $this;
    }

    /**
     * Delete all fields for current step
     * @param $currentStep
     * @param $quoteId
     */
    public function deleteAllFieldsForStop($currentStep, $quoteId)
    {
        $connection = $this->getResource()
                    ->getReadConnection();
        $connection->query("DELETE FROM " . $this->getTable("dyna_checkout/fields") . " WHERE quote_id=:quoteId AND checkout_step=:checkoutStep", [
            "quoteId" => $quoteId,
            "checkoutStep" => $currentStep,
        ]);
    }
}
