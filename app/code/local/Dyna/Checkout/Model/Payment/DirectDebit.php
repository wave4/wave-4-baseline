<?php
class Dyna_Checkout_Model_Payment_DirectDebit extends Mage_Payment_Model_Method_Abstract {
	protected $_code = 'direct_debit';
	
	protected $_isInitializeNeeded      = true;
	protected $_canUseInternal          = true;
	protected $_canUseForMultishipping  = true;

}
