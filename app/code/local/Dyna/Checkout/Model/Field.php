<?php

class Dyna_Checkout_Model_Field extends Mage_Core_Model_Abstract
{
    /**
     * Dyna_Checkout_Model_Field constructor.
     */
    public function _construct()
    {
        $this->_init('dyna_checkout/field');
    }

    /**
     * Load current model data from combination of quote id, field name and checkout step
     * @return $this
     */
    public function loadByQuote($checkoutStep, $fieldName, $quoteId)
    {
        /** @var $collection  Dyna_Checkout_Model_Resource_Field_Collection */
        $oldItem = $this->getCollection()
            ->addFieldToFilter("quote_id", ["eq" => $quoteId])
            ->addFieldToFilter("checkout_step", ["eq" => $checkoutStep])
            ->addFieldToFilter("field_name", ["eq" => $fieldName])
            ->getFirstItem();

        $this->setData($oldItem->getData());

        return $this;
    }
}
