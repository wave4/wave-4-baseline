<?php

/**
 * Class Dyna_Checkout_Model_Sales_Quote
 */
class Dyna_Checkout_Model_Sales_Quote extends Omnius_Checkout_Model_Sales_Quote
{
    /**
     * Return all packages for current quote
     */
    public function getCartPackages()
    {
        // Note that packages collection will be returned not loaded - changes can be applied to it
        $packages = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('quote_id', $this->getId())
            ->setOrder('package_id', 'ASC');

        return $packages;
    }

    /**
     * Return a certain package or active package belonging to current quote
     * @param $packageId
     * @return Dyna_Package_Model_Package
     */
    public function getCartPackage($packageId = null)
    {
        $packageId = $packageId ?: $this->_getQuote()->getActivePackageId();
        $package = Mage::getModel('package/package')->getCollection()
            ->addFieldToFilter('package_id', $packageId)
            ->addFieldToFilter('quote_id', $this->getId())
            ->getFirstItem()
            ->setQuote($this);

        return $package;
    }

    /**
     * Retrieve customer model object
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            if ($customerId = $this->getCustomerId()) {
                $this->_customer->load($customerId);
                if (!$this->_customer->getId()) {
                    $this->_customer->setCustomerId(null);
                }
            } else {
                // If no customer && no customer id set on current quote, load the customer from session
                $this->_customer = Mage::getSingleton("customer/session")->getCustomer();
            }
        }
        return $this->_customer;
    }
}
