<?php

/**
 * Class Data
 */
class Dyna_Checkout_Helper_Data extends Omnius_Checkout_Helper_Data
{

    /**
     * todo: implement mixmatches for options as well
     * @param null $productsIds
     * @param null $websiteId
     * @param null $isRetention
     * @return array
     */
    public function getUpdatedPrices($productsIds = null, $websiteId = null, $isRetention = null)
    {
        $_taxHelper = Mage::helper('tax');
        $_store = Mage::app()->getStore();

        //if website is provided, get the store of the website
        if ($websiteId) {
            $_store = Mage::app()->getWebsite($websiteId)->getDefaultStore();
        }

        if ($isRetention == null) {
            $isRetention = 0;
        }

        $types = [];
        $attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::SUBTYPE_DEVICE);
        $types[] = Mage::helper('dyna_catalog')->getAttributeAdminLabel($attribute, null, Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE);

        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter(Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, ['in' => [$types]])
            ->addWebsiteFilter($websiteId)
            ->addAttributeToSelect('price')
            ->addTaxPercents()
            ->addAttributeToSelect('tax_class_id')
            ->load();

        /**
         * We must extract the subscription from the package
         * with the provided package ID
         */
        $subscription = null;
        $deviceRCSku = null;
        $device = null;

        // check selected products
        $selectedProducts = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('tax_class_id')
            ->addAttributeToFilter('entity_id', ['in' => $productsIds])
            ->load();
        foreach ($selectedProducts as $product) {
            if (!$subscription &&
                Mage::helper('dyna_catalog')->is([
                    Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_SUBSCRIPTION,
                ], $product)
            ) {
                $subscription = $product;
            } elseif (!$device &&
                Mage::helper('dyna_catalog')->is([
                    Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                    Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
                ], $product)
            ) {
                $device = $product;
            }

            if ($subscription && $device) {
                break;
            }
        }

        $mixMatches = $this->getMixMatchPrices(
            $subscription ? $subscription->getSku() : null,
            $device ? $device->getSku() : null,
            $deviceRCSku,
            $websiteId,
            $isRetention
        );

        $prices = [];
        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products->getItems() as $product) {
            // check if product is device
            $isDevice = Mage::helper('dyna_catalog')->is([
                Dyna_Catalog_Model_Type::SUBTYPE_DEVICE,
                Dyna_Catalog_Model_Type::MOBILE_SUBTYPE_DEVICE,
            ], $product);

            $priceStart = $_store->roundPrice($_store->convertPrice($product->getPrice()));
            $price = $_taxHelper->getPrice($product, $priceStart, false);
            $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);

            if (isset($mixMatches[$product->getSku()])) {
                $priceStart = $_store->roundPrice($_store->convertPrice($mixMatches[$product->getSku()]));
                $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);
                $price = $_taxHelper->getPrice($product, $priceStart, false);
            } elseif ($isDevice) {
                //if change, also change in the _afterSave event of the sales order as same formula is applied
                if ($subscription && $product->hasPurchasePrice()) {
                    $priceStart = $_store->roundPrice($_store->convertPrice($this->getPricing()->calculatePrice($product->getPurchasePrice(), $product->getTaxRate())));
                    $price = $_taxHelper->getPrice($product, $priceStart, false);
                    $priceWithTax = $_taxHelper->getPrice($product, $priceStart, true);
                }
            } else {
                $price = null;
                $priceWithTax = null;
            }

            $prices[$product->getId()]['rel_id'] = null;
            if ($isDevice && $subscription) {
                $prices[$product->getId()]['rel_id'] = (int)$subscription->getId();
            } elseif (!$isDevice && $device) {
                $prices[$product->getId()]['rel_id'] = (int)$device->getId();
            }

            $prices[$product->getId()]['price'] = $price;
            $prices[$product->getId()]['price_with_tax'] = $priceWithTax;
        }

        return $prices;
    }

    /**
     * @return Dyna_MixMatch_Model_Pricing
     */
    protected function getPricing()
    {
        return Mage::getSingleton('dyna_mixmatch/pricing');
    }

    /**
     * @param null $subscriptionSku
     * @param null $deviceSku
     * @param null $deviceRC
     * @param null $websiteId
     * @param null $isRetention
     * @return array|mixed
     */
    public function getMixMatchPrices(
        $sourceSku = null,
        $targetSku = null,
        $deviceRC = null,
        $websiteId = null,
        $isRetention = null
    )
    {
        $key = serialize([__METHOD__, $sourceSku, $targetSku, $deviceRC, $websiteId, $isRetention]);
        if ($result = unserialize($this->getCache()->load($key))) {
            return $result;
        } else {
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $result = [];

            if ($sourceSku) {
                // TODO CHANGE THIS MIXMATCH
                /** @var Omnius_MixMatch_Model_Resource_Price_Collection $mixMatches */
                $mixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                    ->addFieldToSelect(['source_sku', 'target_sku', 'price'])
                    ->addFieldToFilter('website_id', $websiteId ? $websiteId : Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('source_sku', $sourceSku);

                $mixMatches = $adapter->fetchAll($mixMatches->getSelect());

                foreach ($mixMatches as $mixMatch) {
                    $result[$mixMatch['target_sku']] = $mixMatch['price'];
                }
            }

            if ($targetSku) {
                /** @var Omnius_MixMatch_Model_Resource_Price_Collection $mixMatches */
                $deviceMixMatches = Mage::getResourceModel('omnius_mixmatch/price_collection')
                    ->addFieldToSelect(['source_sku', 'price'])
                    ->addFieldToFilter('website_id', $websiteId ? $websiteId : Mage::app()->getWebsite()->getId())
                    ->addFieldToFilter('target_sku', $targetSku);

                $deviceMixMatches = $adapter->fetchAll($deviceMixMatches->getSelect());
                foreach ($deviceMixMatches as $deviceMixMatch) {
                    $result[$deviceMixMatch['source_sku']] = $deviceMixMatch['price'];
                }
            }

            $this->getCache()->save(serialize($result), $key, [Dyna_Cache_Model_Cache::PRODUCT_TAG], $this->getCache()->getTtl());

            return $result;
        }
    }

    // TODO: implement backend setting
    public function getPrefixOptions()
    {
        $array = [
            "Dr.",
            "Dipl.-Ing.",
            "Dr.med.",
            "Prof. Dr.",
            "Dipl.-Betriebswirt",
            "Dipl.-Inform.",
            "Dipl.-Ing.(FH)",
            "Dipl.-Ing. von",
            "Dipl.-Kaufmann",
            "Dipl.-Med.",
            "Dipl.-Ökonom",
            "Dipl.-Phys.",
            "Dipl.-Psych.",
            "Dipl.-Volkswirt",
            "Dr. Dr.",
            "Dr. Freifrau",
            "Dr. Freiherr",
            "Dr. Graf",
            "Dr. Gräfin",
            "Dr.jur.",
            "Dr.med.dent.",
            "Dr.med.vet.",
            "Dr.med. von",
            "Dr.rer.nat.",
            "Dr.von",
            "Dr.-Ing.",
            "Freifrau",
            "Freifrau von",
            "Freiherr",
            "Freiherr von",
            "Graf",
            "Graf von",
            "Gräfin",
            "Gräfin von",
            "Ing.",
            "Prof.",
            "Prof. Dr.med.",
            "Prof. Dr.von",
            "Prof. Dr.-Ing.",
        ];

        return $array;
    }

    public function showNewFormattedPriced($price)
    {
        $priceWithoutSymbol = Mage::getModel('directory/currency')->format($price, ['display' => Zend_Currency::NO_SYMBOL], false);
        $currencySymbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $priceWithSymbolInFront = $currencySymbol . ' ' . $priceWithoutSymbol;

        return $priceWithSymbolInFront;
    }

    public function showPriceWithoutCurrencySymbol($price)
    {
        $priceWithoutSymbol = Mage::getModel('directory/currency')->format($price, ['display' => Zend_Currency::NO_SYMBOL], false);

        return $priceWithoutSymbol;
    }

    /**
     * Return current package possible status based on Dyna_Package_Model_Configuration the status will be completed if configuration is valid against requested package
     * The actual status field from package resource will be updated by controllers
     * @param null $packageId
     * @return string
     */
    public function checkPackageStatus($packageId = null)
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $currentQuote */
        $currentQuote = $this->_getQuote();
        /** @var Dyna_Package_Model_Package $currentPackage */
        $currentPackage = $currentQuote->getCartPackage($packageId);

        return $currentPackage
            ->getPackageConfiguration()
            ->buildPackageStatus();
    }

    /**
     * Ensure all packages are completed
     *
     * @return bool
     */
    public function hasAllPackagesCompleted()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $packages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                ->addFieldToFilter('current_status', Omnius_Configurator_Model_AttributeSet::PACKAGE_STATUS_COMPLETED)
                ->getSelect()
        );
        $allPackages = $conn->fetchAll(
            Mage::getModel('package/package')->getCollection()
                ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                ->getSelect()
        );

        return count($allPackages) > 0 && count($packages) == count($allPackages);
    }
}
