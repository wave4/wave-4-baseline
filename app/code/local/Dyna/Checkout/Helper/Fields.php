<?php

/**
 * Parser for the checkout steps fields
 * Class Dyna_Checkout_Helper_Fields
 */

class Dyna_Checkout_Helper_Fields extends Mage_Core_Helper_Data
{
    protected $checkoutData = null;

    /**
     * Iterates through each array value and sub value and returns an array containing the variable path as key
     * and the fields value as value
     * @param $arrayData
     * @return array
     */
    public function buildVariableName($arrayData, $currentKey = "", &$result = [])
    {
        foreach ($arrayData as $key => $value) {
            //First key should not be encapsulated in brackets
            $extendedKey = $currentKey ? $currentKey . "[" . $key . "]" : $key;
            //If it's an array, go deeper
            if (is_array($value)) {
                $this->buildVariableName($value, $extendedKey, $result);
            } else {
                $result[$extendedKey] = $value;
            }
        }

        return $result;
    }

    /**
     * Performs validations against customer data
     * @param $customerData
     * @return array
     * @todo implement it
     */
    public function validateCustomerData($customerData)
    {
        return [
            "error" => false,
//            "message" => "Invalid customer data fields",
//            "invalid_fields" => [
//                'customer[dob]' => $this->__("Only customers over 18 years old can order Iphone 7"),
//            ]
        ];
    }

    /**
     * Get previously saved checkout data for certain step and quote
     * @param $currentStep
     * @param $quoteId
     * @return array
     */
    public function getCheckoutFieldsAsArray($currentStep, $quoteId)
    {
        $checkoutData = [];

        /** @var Dyna_Checkout_Model_Resource_Field_Collection $checkoutFields */
        $checkoutFields = Mage::getModel("dyna_checkout/field")->getCollection()
            ->getAllFieldsForStep($currentStep, $quoteId);

        foreach ($checkoutFields as $field) {
            $checkoutData[$field->getFieldName()] = $field->getFieldValue();
        }

        return $checkoutData;
    }

    /**
     * Set checkout data on current instance
     * @param $currentStep string
     * @param $quoteId int
     * @return $this
     */
    public function setCheckoutData($currentStep, $quoteId)
    {
        $this->checkoutData = $this->getCheckoutFieldsAsArray($currentStep, $quoteId);

        return $this;
    }

    /**
     * Return checkout field value by field name and checkout data previously set by setCheckoutData
     * @param $fieldName
     * @return mixed|false
     */
    public function getFieldValue($fieldName, $defaultValue = null)
    {
        //If array key exists, than submit form included this field
        if (isset($this->checkoutData[$fieldName])) {
            return $this->checkoutData[$fieldName];
        }

        if ($defaultValue) {
            return $defaultValue;
        }

        return false;
    }

    /**
     * Return checkout field value by field name and checkout data previously set by setCheckoutData
     * @param string $fieldNameBeginning
     * @param null $defaultValue
     * @return array|false|string
     */
    public function getFieldValueBeginsWith($fieldNameBeginning, $defaultValue = null)
    {
        $foundFields = array();
        foreach ($this->checkoutData as $key => $value) {
            if (strpos($key, $fieldNameBeginning) === 0) {
                // we extract the key as from a string like provider[phone_transfer_list][telephone_prefix][0] ; the new key here we consider to be 0;
                $newKey = str_replace($fieldNameBeginning, "", $key);
                $newKey = str_replace("[", "", $newKey);
                $newKey = str_replace("]", "", $newKey);
                $foundFields[(int)$newKey] = $value;
            }
        }

        if(count($foundFields)) {
            return $foundFields;
        }

        if ($defaultValue) {
            return $defaultValue;
        }

        return false;
    }

    /*
     * Checks whether or not there are saved fields for current checkout step
     * @return bool
     */
    public function hasCheckoutData()
    {
        return !empty($this->checkoutData);
    }

    /**
     * Gets checkout data 
     * @return array
     */
    public function getCheckoutData()
    {
        return $this->checkoutData;
    }

    /**
     * Retrieve the checkout Billing Address of the customer
     * @return array
     */
    public function getBillingAddress()
    {
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $customerData = $this->getCheckoutFieldsAsArray('save_customer', $quoteId);

        $addressFields = [
            'postcode' => 'customer[address][postcode]',
            'city' => 'customer[address][city]',
            'district' => 'customer[address][district]',
            'street' => 'customer[address][street]',
            'houseno' => 'customer[address][houseno]',
            'addition' => 'customer[address][addition]',
        ];

        // Default empty address
        $address = [
            'postcode' => '',
            'city' => '',
            'district' => '',
            'street' => '',
            'houseno' => '',
            'addition' => '',
        ];

        foreach ($customerData as $name => $value) {
            if ($key = array_search($name, $addressFields)) {
                $address[$key] = $value;
            }
        }

        return $address;
    }
}
