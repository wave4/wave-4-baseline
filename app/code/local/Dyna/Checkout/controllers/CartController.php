<?php
require_once Mage::getModuleDir('controllers', 'Omnius_Checkout') . DS . 'CartController.php';

/**
 * Class Dyna_Checkout_CartController
 */
class Dyna_Checkout_CartController extends Omnius_Checkout_CartController
{
    /** @var  Omnius_Customer_Model_Session */
    protected $_customerSession;

    public function indexAction()
    {
        $complete = Mage::helper('omnius_checkout')->hasAllPackagesCompleted();
        if (!$complete) {
            Mage::getSingleton('core/session')->setGeneralError(Mage::helper('omnius_checkout')->__('You must have at least one complete package'));
            //redirect back to referer or home
            $this->_redirect('/');
        } else {
            $this->renderIndexLayout();
        }
    }

    public function successAction()
    {
        // Clear any packages on quote
        // Clear data from address search
        Mage::getSingleton('dyna_address/storage')->clearAddress();
        Mage::getSingleton('customer/session')->unsetCustomer();
        if (!$this->getCustomerSession()->getLastSuperOrderId()) {
            // If no order was recently finished, redirect to front page
            $this->_redirect('/');
        } else {
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $quote->setIsActive(false)->save();
            $this->renderIndexLayout();
        }
    }

    /**
     * Just render the layout for the indexAction
     */
    private function renderIndexLayout()
    {
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Cart'));
        $this->renderLayout();
    }

    /**
     * Get instance of the Omnius_Customer_Model_Session
     *
     * @return Omnius_Customer_Model_Session
     */
    protected function getCustomerSession()
    {
        if (!$this->_customerSession) {
            $this->_customerSession = Mage::getSingleton('customer/session');
        }

        return $this->_customerSession;
    }
}
