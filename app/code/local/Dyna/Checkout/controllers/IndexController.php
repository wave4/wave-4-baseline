<?php

require_once Mage::getModuleDir('controllers', 'Omnius_Checkout') . DS . 'IndexController.php';

class Dyna_Checkout_IndexController extends Omnius_Checkout_IndexController
{
    /** @var Dyna_Checkout_Helper_Fields */
    protected $checkoutFieldsHelper;

    protected $currentStep = "";

    /**
     * Allowed* actions for this controller
     * (*Verified in pre-dispatch)
     * @var array
     */
    protected $_classActions = array(
        'saveCustomer',
        'saveChangeProvider',
        'saveDeliveryAddress',
        'saveBilling',
        'savePayment',
        'saveOther',
        'saveOrderOverview',
        'saveOverview',
        'sendOffer',
    );

    /**
     * Save customer data step in checkout
     */
    public function saveCustomerAction()
    {
        $this->currentStep = "save_customer";
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                // Validate there is a quote
                $quote = $this->getQuote();
                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                $checkoutData = $request->getPost();

                if ('soho' == strtolower($checkoutData['customer']['type'])) {
                    $checkoutData['customer']['firstname'] = 'Soho Customer';
                    $checkoutData['customer']['lastname'] = 'Soho Customer';
                }

                /** @var Dyna_Customer_Model_Session $customerSession */
                $customerSession = $this->_getCustomerSession();

                // Check if customer is already logged in
                $customer = $customerSession->getCustomer();

                if (!$customer->getId()) {
                    // TODO: validate customer data
                    $data = $this->_buildAndValidateCustomerData($request);

                    $checkCustomer = 'invalid';
                    while ($checkCustomer != null) {
                        $email = microtime(true) . uniqid("", true) . Omnius_Customer_Model_Customer_Customer::DEFAULT_EMAIL_SUFFIX;
                        $customerModel = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId());
                        $checkCustomer = $customerModel->loadByEmail($email)->getId();
                    }

                    // Customer email address is an uniqid now
                    $data['email'] = $email;

                    // Save all customer and billing address information to the quote
                    // ToDo: Update customer firstname and lastname in order to validate billing for SOHO customer. Currently Dummy firstname and lastname are set
                    $result = $this->getOnepage()->saveBilling($data, null);
                    if ($result) {
                        throw new Exception(Mage::helper('core')->jsonEncode($result));
                    }

                } else {
                    // Validating existing customer data
                    $result = $this->getCheckoutFieldsHelper()
                        ->validateCustomerData($checkoutData['customer']);

                    // When customer is logged in check if it is hardware only for this customer
                    $customerData = $request->getPost('customer', array());
                    if ($customer->getIsProspect() || isset($customerData['hardware_only']) && $customerData['hardware_only'] == $customer->getId()) {
                        // Validate input data
                        $data = $this->_buildAndValidateCustomerData($request);
                        if ($customer->getIsProspect()) {
                            // Also save all customer and billing address information to the quote
                            $result = $this->getOnepage()->saveBilling($data, null);
                        }
                        foreach ($data as $key => $value) {
                            // Map data to the customer
                            $customer->setData($key, $value);
                        }
                        $customer->save();
                    }
                    $emailOverwrite = trim($request->getParam('email_overwrite'));
                    if ($emailOverwrite) {
                        if (Mage::helper('omnius_validators')->validateEmailSyntax($emailOverwrite)) {
                            $this->_getCustomerSession()->setNewEmail($emailOverwrite);
                        } else {
                            throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode(['email_overwrite' => $this->__('Please enter a valid email address. For example johndoe@domain.com.')]));
                        }
                    } else {
                        $this->_getCustomerSession()->setNewEmail(null);
                    }
                }

                //In case of error, send validation result directly to frontend
                //Checkout fields need to be updated
                if (isset($result['error']) && $result['error']) {
                    $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                        Mage::helper('core')->jsonEncode($result)
                    );

                    return;
                }

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                $quote->setCustomStatus(Omnius_Checkout_Model_Sales_Quote::VALIDATION);
                if ($request->getPost('current_step') && !$request->getParam('is_offer')) {
                    $quote->setCurrentStep($request->getPost('current_step'));
                }

                $quote->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                if ($this->getServiceHelper()->isDev()) {
                    $result['trace'] = $e->getTrace();
                }
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );

                return;
            }

        }
    }

    /**
     * @param Zend_Controller_Request_Http $request
     * @return mixed
     */
    protected function _buildAndValidateCustomerData($request)
    {
        //todo: Validating new customer data

        $customerData = $request->getPost('customer');

        // By default set the shipping address same as billing
        $customerData['use_for_shipping'] = 1;

        $addressData = $customerData['address'];

        $addressData['street'] = is_array($addressData['street']) ? $addressData['street'] : [$addressData['street']];
        $addressData['country_id'] = 'DE';

        if ('soho' == strtolower($customerData['type'])) {
            $customerData['firstname'] = 'Soho Customer';
            $customerData['lastname'] = 'Soho Customer';
        }

        return $customerData + $addressData;
    }

    /**
     * Persist delivery address data
     */
    public function saveDeliveryAddressAction()
    {
        $this->currentStep = "save_delivery";
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                $quote = $this->getQuote();

                if (!$quote->getId()) {
                    throw new Exception($this->__('Invalid quote'));
                }

                if ($currentStep = $request->getPost('current_step')) {
                    $quote->setCurrentStep($currentStep);
                }

                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                $delivery = $request->getPost('delivery', array());
                $data = array();
                if (!isset($delivery['method'])) {
                    throw new Exception($this->__('Missing shipment method'));
                }

                $errors = array();
                $showDeliverySteps = true;
                switch ($delivery['method']) {
                    case 'direct' :
                        $addressData = $this->_getCustomerSession()->getAgent(true)->getDealer()->getId();

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => 'store',
                            'store_id' => $addressData,
                        ));
                        if($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                            $errors += array('delivery[method][text]' => $this->__('Invalid store chosen'));
                        }
                        $showDeliverySteps = true;
                        break;
                    case 'pickup' :
                        /** @var Dyna_Agent_Model_Mysql4_Dealer_Collection $addressData */
                        if(!isset($delivery['pickup']['store_id']) || empty($delivery['pickup']['store_id'])) {
                            $errors += array('delivery[pickup][store]' => $this->__('Invalid store chosen'));
                        } else {
                            $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                                'address' => 'store',
                                'store_id' => $delivery['pickup']['store_id'],
                            ));
                            if($this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address'])) {
                                $errors += array('delivery[pickup][store]' => $this->__('Invalid store chosen'));
                            }
                        }
                        break;
                    case 'deliver' :
                        if (!isset($delivery['deliver']['address'])) {
                            throw new Exception($this->__('Missing shipping address info'));
                        }
                        $billingAddress = $this->getCheckoutFieldsHelper()->getBillingAddress();

                        $data['deliver']['address'] = $this->checkoutHelper->_getAddressByType(array(
                            'address' => $delivery['deliver']['address'],
                            'billingAddress' => $billingAddress,
                            'otherAddress' => isset($delivery['other_address']) ? $delivery['other_address'] : [],
                            'foreignAddress' => $request->getPost('foreignAddress', array()),
                        ));
                        $errors += $this->checkoutHelper->_validateDeliveryAddressData($data['deliver']['address']);
                        break;
                    case 'split' :
                        $splitData = $request->getPost('delivery', array());
                        $splitData = isset($splitData['pakket']) ? $splitData['pakket'] : array();
                        if (!$splitData) {
                            throw new Exception($this->__('Incomplete data'));
                        }

                        $cancelledPackages = Mage::helper('omnius_package')->getCancelledPackages();
                        $cancelledPackages = !empty($cancelledPackages) ? $cancelledPackages : [];
                        $cancelledPackagesNow = Mage::getSingleton('customer/session')->getCancelledPackagesNow();
                        $cancelledPackagesNow = !empty($cancelledPackagesNow) ? $cancelledPackagesNow : [];

                        foreach ($splitData as $packageId => $address) {
                            if (!isset($address['address']) || in_array($packageId, $cancelledPackages) || (!empty($cancelledPackagesNow[$quote->getId()]) && in_array($packageId, $cancelledPackagesNow[$quote->getId()]))) {
                                continue;
                            }

                            $data['pakket'][$packageId]['address'] = $this->checkoutHelper->_getAddressByType($address);

                            if($data['pakket'][$packageId]['address']['address'] == 'store' &&
                                $data['pakket'][$packageId]['address']['store_id'] ==  Mage::getSingleton('customer/session')->getAgent(true)->getDealer()->getId()) {
                                $showDeliverySteps = true;
                            }
                            $errors += $this->_validateSplitDeliveryAddressData($data['pakket'][$packageId]['address'], $packageId);
                        }
                        break;
                    default:
                        $errors += array('delivery[method]' => $this->__('Invalid delivery method'));
                }

                // TODO: separate monthly and one-time payments
                $paymentData = $request->getPost('payment', array());
                if (!isset($paymentData['one_time']['type']) && !isset($paymentData['monthly']['type'])) {
                    throw new Exception('Missing payment method');
                } else {
                    $paymentMethod = isset($paymentData['one_time']['type']) ? $paymentData['one_time']['type'] : $paymentData['monthly']['type'];
                }

                $payment = array();
                switch ($paymentMethod) {
                    case 'cashondelivery' :
                    case 'alreadypaid':
                    case 'checkmo':
                    case 'payinstore':
                    case 'direct_debit':
                    case 'banktransfer':
                        $payment = $paymentMethod;
                        break;
                    case 'split-payment':
                        foreach($paymentData['pakket'] as $packageId => $packageData) {
                            if (isset($packageData['type']) && $packageData['type']) {
                                $payment[$packageId] = $packageData['type'];
                            }
                        }
                        break;
                    default:
                        $errors += array('payment[method]' => $this->__('Invalid payment method'));
                }

                $data['payment'] = $payment;

                // Validate each delivery address has same payment method
                $errors += $this->_validateSplitPaymentAndDelivery($data);

                if ($errors) {
                    throw new Mage_Customer_Exception(Mage::helper('core')->jsonEncode($errors));
                }

                $this->_saveDeliveryAddressData($data);

                $quote->save();

                // Set successful response
                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                $result['delivery_steps'] = $showDeliverySteps;


                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Mage_Customer_Exception $e) {
                // Set fail response for validations
                $result['error'] = true;
                $result['message'] = $this->checkoutHelper->__("Please correct the following errors");
                $result['fields'] = Mage::helper('core')->jsonDecode($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
                return;
            }
        }
    }


    public function saveChangeProviderAction()
    {
        $this->currentStep = "save_change_provider";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }
    /**
     * Save billing data step in checkout
     */
    public function saveBillingAction()
    {
        $this->currentStep = "save_billing";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Save payment data step in checkout
     */
    public function savePaymentAction()
    {
        $this->currentStep = "save_payment";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Save other data step in checkout
     */
    public function saveOtherAction()
    {
        $this->currentStep = "save_other";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Save order overview data step in checkout
     */
    public function saveOrderOverviewAction()
    {
        $this->currentStep = "save_order_overview";
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $checkoutData = $this->getRequest()->getPost();

                //Fields are valid, proceed to saving
                $checkoutInfo = $this->getCheckoutFieldsHelper()
                    ->buildVariableName($checkoutData);
                $this->saveCheckoutFields($checkoutInfo);

                //Save current step on quote
                $this->getQuote()
                    ->setCurrentStep($checkoutData['current_step'])
                    ->save();

                $result['error'] = false;
                $result['message'] = "Step data successfully saved";
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }

            return;
        }
    }

    /**
     * Send offer based on checkout data
     */
    public function sendOfferAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            try {
                // todo: implement offer send functionality

                $result['error'] = false;
                $result['message'] = $this->__('Offer send successful');
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            } catch (Exception $e) {
                // Set fail response for other errors
                $result['error'] = true;
                $result['message'] = $this->__($e->getMessage());
                $this->getResponse()->setHeader('Content-Type', 'application/json')->setBody(
                    Mage::helper('core')->jsonEncode($result)
                );
            }
        }
    }

    /**
     * Returns the checkout cart helper
     * @return Dyna_Checkout_Helper_Fields|Mage_Core_Helper_Abstract
     */
    protected function getCheckoutFieldsHelper()
    {
        if (!$this->checkoutFieldsHelper) {
            $this->checkoutFieldsHelper = Mage::helper('dyna_checkout/fields');
        }

        return $this->checkoutFieldsHelper;
    }

    /**
     * Save current checkout fields on quote checkout
     * @param $checkoutInfo
     * @return $this
     */
    protected function saveCheckoutFields($checkoutInfo)
    {
        $quoteId = $this->getQuote()->getId();

        //Deleting all previous fields for current step
        Mage::getModel("dyna_checkout/field")
            ->getCollection()
            ->deleteAllFieldsForStop($this->currentStep, $quoteId);

        foreach ($checkoutInfo as $fieldName => $value) {
            if ($fieldName == 'current_step') {
                continue;
            }
            Mage::getModel("dyna_checkout/field")
                ->setQuoteId($quoteId)
                ->setCheckoutStep($this->currentStep)
                ->setFieldName($fieldName)
                ->setFieldValue($value)
                ->setDate(date("Y-m-d H:i:s"))
                ->save();
        }

        return $this;
    }

    /**
     * Contains the backend validations for the customer fields
     * @return array
     */
    protected function _getCustomerDataFields()
    {
        return [];
    }

    /**
     * Override createOrder to also call the processOrder call
     */
    protected function createOrder()
    {
        $superOrder = parent::createOrder();
        $client = Mage::getModel("dyna_superorder/client_processOrderRequestClient");
        $result = $client->executeProcessOrderRequest($superOrder);

        return $superOrder;
    }
}
