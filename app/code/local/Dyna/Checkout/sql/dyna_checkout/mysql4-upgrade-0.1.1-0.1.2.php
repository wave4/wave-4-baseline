<?php
/**
 * Installer that adds the customer_number column after customer_id
 */

/** @var Mage_Core_Model_Resource_Setup $this */

$this->startSetup();

// Add customer number on sales quote resource
$this->getConnection()->addColumn($this->getTable("sales/quote"), "customer_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_id",
    "comment" => "External customer number to which the current superorder maps to",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/quote"),
        $this->getIdxName($this->getTable("sales/quote"), "customer_number"),
        "customer_number");

// Add customer number on sales order resource
$this->getConnection()->addColumn($this->getTable("sales/order"), "customer_number", [
    "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
    "length" => 20,
    "after" => "customer_id",
    "comment" => "External customer number to which the current superorder maps to",
]);

$this->getConnection()
    ->addIndex(
        $this->getTable("sales/order"),
        $this->getIdxName($this->getTable("sales/order"), "customer_number"),
        "customer_number");

$this->endSetup();
