<?php

/**
 * Class Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider
 * available only for cable, dsl and lte
 */
class Dyna_Checkout_Block_Cart_Steps_SaveChangeProvider extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    //other provider address
    const OTHER_PROVIDER_ADDRESS = 1;

    // change provider values for the radio buttons
    const NO_CHANGE_PROVIDER = 0;
    const YES_CHANGE_PROVIDER = 1;
    const YES_CHANGE_PROVIDER_CONFIRMED = 2; // option only for cable

    // current provider options
    const CURRENT_AS_HOME_PHONE = 1;
    const CURRENT_AS_COMPANY_PHONE = 2;

    // no of phone transfers options
    const TRANSFER_ALL_PHONES = 1;
    const TRANSFER_1_PHONE = 2;
    const TRANSFER_MORE_THAN_1_PHONE = 3;
    const TRANSFER_NONE = 4; // only for cable and dsl

    //owner of terminal options
    const YES_ONE_OF_THE_TERMINAL_OWNERS = 0;
    const YES_OWNER_OF_TERMINAL = 1;
    const NO_OWNER_OF_TERMINAL = 2;

    // termination date not filled reasons
    const TERMINATION_DATE_NOT_FILLED_NOW = 1;
    const TERMINATION_DATE_NOT_FILLED_NO_PERIOD = 2;
    const TERMINATION_DATE_NOT_FILLED_CONTRACT_END = 3;

    // lte activation radio options: with new or old phone
    const ACTIVATE_WITH_NEW_PHONE = 0;
    const ACTIVATE_WITH_OLD_PHONE = 1;

    //dsl and cable desired date
    const NEW_CONNECTION_DATE_AS_SOON_AS_POSSIBLE = 0;
    const NEW_CONNECTION_DATE_SPECIFIC_DATE = 1;

    //dsl type of home
    const HOME_WITH_1_FAMILY = 0;
    const HOME_WITH_MULTIPLE_FAMILIES = 1;

    //dsl home entrance
    const HOME_ENTRANCE_VESTIBULE = 1;
    const HOME_ENTRANCE_ANNEX = 2;

    //dsl home floor
    const HOME_FLOOR_CELLAR = 1;
    const HOME_ENTRANCE_FLOOR = 2;
    const HOME_ENTRANCE_FLOOR_1 = 3;
    const HOME_ENTRANCE_FLOOR_2 = 4;
    const HOME_ENTRANCE_FLOOR_3 = 5;
    const HOME_ENTRANCE_FLOOR_4 = 6;
    const HOME_ENTRANCE_FLOOR_5 = 7;

    //dsl home location
    const AP_LOCATION_LEFT = 1;
    const AP_LOCATION_MIDDLE = 2;
    const AP_LOCATION_RIGHT = 3;

    public function getChangeProviderRadioOptions()
    {
        // for cable
        if($this->hasCable()) {
            return $this->getChangeProviderOptionsForCable();
        }
        // for dsl and lte
        else {
            return $this->getChangeProviderOptionsForFixed();
        }
    }

    public function getCurrentProvidersOptions()
    {
        return [
            self::CURRENT_AS_HOME_PHONE => $this->__("Home phone"),
            self::CURRENT_AS_COMPANY_PHONE => $this->__("Company phone"),
        ];
    }

    public function geNoOfPhoneTransfersOptions()
    {
        // for cable and dsl
        if($this->hasCable() || $this->hasDsl() ) {
            return $this->geNoOfPhoneTransfersOptionsForCableAndDsl();
        }
        // for lte
        else {
            return $this->geNoOfPhoneTransfersOptionsForLte();
        }
    }

    public function getOwnerOfTerminalOptions()
    {
        return [
            self::YES_ONE_OF_THE_TERMINAL_OWNERS => $this->__("Yes, one of the connection partners"),
            self::YES_OWNER_OF_TERMINAL => $this->__("Yes, I'm Connection Owner"),
            self::NO_OWNER_OF_TERMINAL =>  $this->__("No, not the terminal owner"),
        ];
    }


    public function getTerminationDateNotFilledReasons()
    {
        return [
            self::TERMINATION_DATE_NOT_FILLED_NOW => $this->__("We will fill it later"),
            self::TERMINATION_DATE_NOT_FILLED_NO_PERIOD =>  $this->__("No minimum contract period"),
            self::TERMINATION_DATE_NOT_FILLED_CONTRACT_END =>  $this->__("Agreement has terminated"),
        ];
    }

    public function getActivationWithPhoneOptionsForLte()
    {
        return [
            self::ACTIVATE_WITH_NEW_PHONE => $this->__("Activation immediately. You get new phones"),
            self::ACTIVATE_WITH_OLD_PHONE =>  $this->__("Enabling with old phone"),
        ];
    }

    public function getNewConnectionDesiredDatesOptionsForCableAndDsl()
    {
        return [
            self::NEW_CONNECTION_DATE_AS_SOON_AS_POSSIBLE => $this->__("As soon as possible"),
            self::NEW_CONNECTION_DATE_SPECIFIC_DATE =>  $this->__("Your date"),
        ];
    }

    public function getDslResidenceTypesOptions()
    {
        return [
            self::HOME_WITH_1_FAMILY => $this->__("House with one family"),
            self::HOME_WITH_MULTIPLE_FAMILIES => $this->__("Multi-family house"),
        ];
    }
    public function getDslHomeEntranceOptions()
    {
        return [
            self::HOME_ENTRANCE_VESTIBULE => $this->__("Vorhaus"),
            self::HOME_ENTRANCE_ANNEX => $this->__("Hinterhaus"),
        ];
    }
    public function getDslHomeFloorsOptions()
    {
        return [
            self::HOME_FLOOR_CELLAR => $this->__("Cellar"),
            self::HOME_ENTRANCE_FLOOR => $this->__("Floor."),
            self::HOME_ENTRANCE_FLOOR_1 => "1. ". $this->__("UpperFloor."),
            self::HOME_ENTRANCE_FLOOR_2 => "2. ". $this->__("UpperFloor."),
            self::HOME_ENTRANCE_FLOOR_3 => "3. ". $this->__("UpperFloor."),
            self::HOME_ENTRANCE_FLOOR_4 => "4. ". $this->__("UpperFloor."),
            self::HOME_ENTRANCE_FLOOR_5 => "5. ". $this->__("UpperFloor."),
        ];
    }

    public function getDslApartmentLocationOptions()
    {
        return [
            self::AP_LOCATION_LEFT => $this->__("Left"),
            self::AP_LOCATION_MIDDLE => $this->__("Center"),
            self::AP_LOCATION_RIGHT => $this->__("Right"),
        ];
    }

    private function getChangeProviderOptionsForCable()
    {
        $options = $this->getChangeProviderOptionsForFixed();
        $options[self::YES_CHANGE_PROVIDER_CONFIRMED] = $this->__("Yes, data is subsequently confirmed") ;
        return $options;
    }

    private function getChangeProviderOptionsForFixed()
    {
        return [
            self::NO_CHANGE_PROVIDER => $this->__('No'),
            self::YES_CHANGE_PROVIDER =>  $this->__('Yes')
        ];
    }


    private function geNoOfPhoneTransfersOptionsForLte()
    {
        return [
            self::TRANSFER_ALL_PHONES => $this->__("All phones"),
            self::TRANSFER_1_PHONE =>  $this->__("1 Phone"),
            self::TRANSFER_MORE_THAN_1_PHONE =>  $this->__("More than 1 Phone")
        ];
    }

    private function geNoOfPhoneTransfersOptionsForCableAndDsl()
    {
        $options = $this->geNoOfPhoneTransfersOptionsForLte();
        $options[self::TRANSFER_NONE] = $this->__("None") ;
        return $options;
    }

}
