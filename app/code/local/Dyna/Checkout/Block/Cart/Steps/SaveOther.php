<?php
/**
 * Class Dyna_Checkout_Block_Cart_Steps_SaveOther
 */
class Dyna_Checkout_Block_Cart_Steps_SaveOther extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    public function getTelephoneInfoOptions()
    {
        return $telephoneInfoOptions = [
            'None',
            'Only phone',
            'Full entry'
        ];
    }

    public function getTelephoneProviders()
    {
        /** @var $floorArray  Holds list of providers */
        return $telephoneProviderArray = [
            'Telephone provider 1',
            'Telephone provider 2',
            'Telephone provider 3',
            'Telephone provider 4',
            'Telephone provider 5',
        ];
    }

    public function getFloors()
    {
        /** @var $floorArray  Holds list of floors */
        $floorArray = [
            'K',
            'EG'
        ];
        for ($i=1; $i<=20; $i++) {
            $floorArray[] = $i;
        }

        return $floorArray;
    }

    // todo: get values from configuration data
    public function getNoteTypes()
    {
        /** @var $telephoneProviderArray  Holds list of telephone providers */
        return $noteTypeArray = [
            '-',
            'Standard note 1',
            'Standard note 2',
            'Standard note 3',
            'Other '
        ];
    }

    public function getInfoExchangeOptions()
    {
        /** @var $floorArray  Holds list of providers */
        return $infoExchangeOptions = [
            'Everything',
            'Information on the phone number',
            'No Information',
        ];
    }

    public function getKeywordOptions()
    {
        /** @var $floorArray  Holds list of providers */
        return $keywordOptions = [
            '-',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
            '[Defined entry]',
        ];
    }

    public function getPhoneEntryTypeOptions()
    {
        /** @var $floorArray  Holds list of providers */
        return $keywordOptions = [
            'Telephone',
            'Fax',
            'Telephone and Fax',
            'No entry',
        ];
    }

    /**
     * Check if there is at least a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasFixed()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type DSL in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_FIXED_DSL)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type LTE in the cart
     *
     * @return bool
     */
    public function hasLTE()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_LTE)) {
                return true;
            }
        }

        return false;
    }
}
