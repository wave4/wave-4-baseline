<?php

class Dyna_Checkout_Block_Cart_Steps_SaveDeliveryAddress extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    /**
     * Get selected package type as string
     * @return string
     */
    public function getPackageType()
    {
        return $this->getQuote()->getPackageType();
    }

    /**
     * Checks whether order contains cable / DSL packages
     * @return bool
     */
    public function hasCableTypePackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), array_merge(Dyna_Catalog_Model_Type::getCablePackages(), Dyna_Catalog_Model_Type::getFixedPackages()))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is atleast a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is atleast a package of type cable in the cart
     *
     * @return bool
     */
    public function hasCable()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether order contains mobile packages or a certain mobile package type
     * @param null $type
     * @return bool
     */
    public function hasMobileTypePackage($type = null)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($type) {
                if (strtolower($package->getType()) == strtolower($type)) {
                    return true;
                }
            } else {
                if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if there is atleast a package of type mobile (postpaid) in the cart
     *
     * @return bool
     */
    public function hasMobile()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }

    /**
     * Check if there is atleast a package of type prepaid in the cart
     *
     * @return bool
     */
    public function hasPrepaid()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_PREPAID);
    }

    public function getDeliveryAddress($addDistrict = false)
    {
        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        return $specifiedAddressRows = [
            $customer->getName(),
            $customer->getStreet() . " " . $customer->getHouseNo(),
            $customer->getPostalCode() . " " . $customer->getCity() . (($addDistrict) ? " " . $customer->getDistrict() : ""),
        ];
    }
}
