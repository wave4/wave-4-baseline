<?php

/**  */
class Dyna_Checkout_Block_Cart_Steps_SaveCustomer extends Omnius_Checkout_Block_Cart_Steps_SaveCustomer
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    protected $mobilePackages = [];

    public function __construct()
    {
        $this->mobilePackages = Dyna_Catalog_Model_Type::getMobilePackages();

        parent::__construct();
    }


    public function getTelephoneType()
    {
        return $telephone_type = ['landline',
            'mobile'];
    }

    public function getCommercialRegisterType()
    {
        // todo make this more readable if this is needed in this hardcoded form
        return $telephone_type = [
            'A.',
            'V.',
            'B.',
            'G.'
        ];
    }

    public function getServiceAddressAsString($addDistrict = false)
    {
        $serviceAddressParts = $this->getServiceAddressDetails();

        $return = '';
        $return = $return
            . $serviceAddressParts['street'] . ' ' . $serviceAddressParts['houseno'] . ', <br/> '
            . $serviceAddressParts['postcode'] . ' ' . $serviceAddressParts['city']
            . ($addDistrict ? ' ' . $serviceAddressParts['district'] : '') ;

        return $return;
    }

}
