<?php

/**
 * trait Dyna_Checkout_Block_Cart_Steps_Common
 */
trait Dyna_Checkout_Block_Cart_Steps_Common
{
    /** @var array */
    protected $packages = null;
    protected $customer;

    public function getCountries()
    {
        return $country_list = ['DE' => 'Deutschland',
            'NL' => 'die Niederlande',
            'CH' => 'die Schweiz',
            'SK' => 'die Slowakei',
            'TR' => 'die Türkei',
            'UA' => 'die Ukraine',
            'EE' => 'Estland',
            'FI' => 'Finnland',
            'FR' => 'Frankreich',
            'GR' => 'Griechenland',
            'UK' =>'Großbritanien',
            'IR' => 'Irland',
            'IS' => 'Island',
            'IT' => 'Italien',
            'HR' => 'Kroatien',
            'LV' => 'Lettland',
            'LT' => 'Litauen',
            'LU' => 'Luxemburg',
            'MT' => 'Malta',
            'MK' => 'Mazedonien',
            'MD' => 'Moldawien',
            'ME' => 'Montenegro',
            'NO' => 'Norwegen',
            'AT' => 'Österreich',
            'PL' => 'Polen',
            'PT' => 'Portugal',
            'RO' => 'Rumänien',
            'RU' => 'Russland',
            'SE' => 'Schweden',
            'RS' => 'Serbien',
            'SI' => 'Slowenien',
            'ES' => 'Spanien',
            'CZ' => 'Tschechien',
            'HU' => 'Ungarn',
            'BY' => 'Weißrussland',
            'CY' => 'Zypern'];
    }

    /**
     * Check if there is at least a package of type cable, dsl , lte in the cart
     *
     * @return bool
     */
    public function hasCableOrFixedTypePackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), array_merge(
                Dyna_Catalog_Model_Type::getCablePackages(),
                Dyna_Catalog_Model_Type::getFixedPackages()
            ))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasFixedTypePackage()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                return true;
            }
        }

        return false;
    }


    /**
     * Retrieve the quote packages
     *
     * @return array
     */
    public function getPackages()
    {
        // todo - clarify if the installed base pack from bundle should not be displayed in the checkout and exclude it if yes from this method

        if ($this->packages === null) {
            /** @var Dyna_Checkout_Model_Sales_Quote $quoteModel */
            $quoteModel = Mage::getSingleton('checkout/cart')->getQuote();
            $tempPackages = $quoteModel->getPackages();
            $this->packages = [];
            foreach ($tempPackages as $id => $package) {
                /** @var Dyna_Package_Model_Package $packageModel */
                $packageModel = Mage::getModel('package/package')->getPackages(null, $quoteModel->getId(), $id)->getFirstItem();
                $packageModel->setData('items', $package['items']);
                $this->packages[] = $packageModel;
            }
        }

        return $this->packages;
    }

    /**
     * Retrieve session loaded customer
     * @return Dyna_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (!$this->customer) {
            $this->customer = Mage::getModel('dyna_customer/session')->getCustomer();
        }

        return $this->customer;
    }

    /**
     * Get the storage model for serviceability details address
     * @return Dyna_Address_Model_Storage
     */
    public function getAddressStorage()
    {
        return Mage::getModel("dyna_address/storage");
    }

    /**
     * Get address from storage either as a string ($isFullAddress = true) or by parts
     * @param bool $isFullAddress
     * @return array
     */
    public function getServiceAddressDetails($isFullAddress = false)
    {
        if ($isFullAddress) {
            return $this->getAddressStorage()
                ->getServiceAddress(true);
        } else {
            return $this->getAddressStorage()
                ->getServiceAddressParts();
        }
    }

    // todo refactor for multiple packages
    public function getHasStudentDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_STUDENT_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    // todo refactor for multiple packages
    public function getHasDisabledDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_DISABLED_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    // todo refactor for multiple packages
    public function getHasYoungDiscount()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::MOBILE_DISCOUNT_YOUNG_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    // todo refactor for multiple packages
    public function getHasComfortConnection()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if (in_array($item->getData()['sku'], [Dyna_Catalog_Model_Type::COMFORT_CONNECTION_CABLE_SKU, Dyna_Catalog_Model_Type::COMFORT_CONNECTION_DSL_SKU])) {
                    return true;
                }
            }
        }

        return false;
    }

    // todo refactor for multiple packages
    public function getHasInstallationInstructions()
    {
        foreach ($this->getPackages() as $package) {
            $items = $package['items'];
            foreach ($items as $item) {
                if ($item->getData()['sku'] == Dyna_Catalog_Model_Type::INSTALL_INSTRUCTIONS_CABLE_SKU) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type fixed in the cart
     *
     * @return bool
     */
    public function hasFixed()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getFixedPackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type dsl in the cart
     *
     * @return bool
     */
    public function hasDsl()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getDslPackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is at least a package of type lte in the cart
     *
     * @return bool
     */
    public function hasLte()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getLtePackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is atleast a package of type cable in the cart
     *
     * @return bool
     */
    public function hasCable()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getCablePackages())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if there is atleast a package of type mobile (postpaid) in the cart
     *
     * @return bool
     */
    public function hasMobile()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_MOBILE);
    }


    /**
     * Check if there is atleast a package of type prepaid in the cart
     *
     * @return bool
     */
    public function hasPrepaid()
    {
        return $this->hasMobileTypePackage(Dyna_Catalog_Model_Type::TYPE_PREPAID);
    }

    /**
     * Checks whether order contains mobile packages or a certain mobile package type
     * @param null $type
     * @return bool
     */
    public function hasMobileTypePackage($type = null)
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if ($type) {
                if (strtolower($package->getType()) == strtolower($type)) {
                    return true;
                }
            } else {
                if (in_array(strtolower($package->getType()), Dyna_Catalog_Model_Type::getMobilePackages())) {
                    return true;
                }
            }
        }


        return false;
    }

    /**
     * @param string $step
     * @return array
     */
    public function getCustomerFields($step = "save_customer")
    {
        /** @var Dyna_Checkout_Model_Sales_Quote $quoteId */
        $quoteId = $this->getQuote()->getId();
        /** @var Dyna_Checkout_Helper_Fields $fieldsHelper */
        $fieldsHelper = Mage::helper("dyna_checkout/fields");
        $fields = $fieldsHelper->getCheckoutFieldsAsArray($step, $quoteId);

        return $fields;
    }
}
