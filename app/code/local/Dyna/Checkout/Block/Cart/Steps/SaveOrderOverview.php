<?php

/**
 * Class Dyna_Checkout_Block_Cart_Steps_SaveOverview
 */
class Dyna_Checkout_Block_Cart_Steps_SaveOrderOverview extends Omnius_Checkout_Block_Cart
{
    use Dyna_Checkout_Block_Cart_Steps_Common;

    /**
     * @return  Dyna_Checkout_Helper_Data
     */
    public function getDynaCheckoutHelper()
    {
        return Mage::helper('dyna_checkout');
    }

    /**
     * Check if there is atleast a package of type mobile in the cart
     *
     * @return bool
     */
    public function hasMobile()
    {
        /** @var Dyna_Package_Model_Package $package */
        foreach ($this->getPackages() as $package) {
            if (strtolower($package->getType()) == strtolower(Dyna_Catalog_Model_Type::TYPE_MOBILE)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $customerFields
     * @return array
     */
    public function getTelephoneNumbers(array $customerFields)
    {
        $prefixes = $phones = $types = [];
        foreach ($customerFields as $key => $value) {
            if(strpos($key, "phone_list") !==FALSE) {
                if(strpos($key, "[telephone_prefix]") !== FALSE) {
                    $prefixes[] = $value;
                }
                if(strpos($key, "[telephone]") !== FALSE) {
                    $phones[] = $value;
                }
                if(strpos($key, "[telephone_type]") !== FALSE) {
                    $types[] = $value;
                }
            }
        }
        return array_map(function($f, $t, $d) {
            return array('prefix'=>$f, 'phone'=>$t, 'type'=>$d);
            }, $prefixes, $phones, $types
        );
    }
}
