<?php
/**
 * Remove attribute gui_description if exists
 */

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;
$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'gui_description');
if($attributeModel->getId()) {
    $installer->removeAttribute($entityTypeId, 'gui_description');
}

$installer->endSetup();
