<?php
/**
 * Installer for adding new attribute maf to Mobile products
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->_generalGroupName = "Mobile";

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$attributeCode = "maf";
$mafAttributeData = [
    'label' => 'Maf',
    'input' => 'price',
    'type' => 'decimal',
    'user_defined' => 1,
];

$attributeSets = [
    "Mobile_Additional_Service",
    "Mobile_Data_Tariff",
    "Mobile_Prepaid_Tariff",
    "Mobile_Voice_Data_Tariff",
];

$installer->addAttribute($entityTypeId, $attributeCode, $mafAttributeData);

foreach ($attributeSets as $attributeSetCode) {
    $attributeSetId = $installer->getAttributeSet($entityTypeId, $attributeSetCode, "attribute_set_id");
    $installer->addAttributeToSet($entityTypeId, $attributeSetId, "Mobile", $attributeCode);
}

$installer->endSetup();
