<?php
/**
 * Map effective_date and expiration_date to all Mobile attribute sets
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$this->_generalGroupName = "Mobile";

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

// Remove obsolete attributes
$obsoleteAttributes = [
    'recurring_charge',
];

foreach ($obsoleteAttributes as $attribute) {
    $installer->removeAttribute($entityTypeId, $attribute);
}

$installer->endSetup();
