<?php
/**
 * Installer for adding <initial_credit> attribute (OMNVFDE-407)
 */

/* @var $this Dyna_Mobile_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$entityTypeId = Mage_Catalog_Model_Product::ENTITY;

$initialCreditAttributeCode = "initial_credit";
$initialCreditAttributeSet = "Mobile_Device";
$initialCreditAttributeData = [
    'label' => 'Initial Credit',
    'input' => 'text',
    'type' => 'int',
    'user_defined' => 1,
];

$installer->addAttribute($entityTypeId, $initialCreditAttributeCode, $initialCreditAttributeData);
$attributeSetId = $installer->getAttributeSet($entityTypeId, $initialCreditAttributeSet, "attribute_set_id");
$installer->addAttributeToSet($entityTypeId, $attributeSetId, "Device variation", $initialCreditAttributeCode);

$installer->endSetup();
