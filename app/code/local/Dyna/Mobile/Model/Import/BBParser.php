<?php
/*
 * Class creasted for parsing BB code
 */
class Dyna_Mobile_Model_Import_BBParser
{
    protected $_bbText;

    /*
     * Set the bb string data on this instance
     */
    public function setText($string)
    {
        $this->_bbText = trim($string);
    }

    public function parseText()
    {
        $modified = preg_replace($this->__getSearchArray(), $this->__getReplaceArray(), $this->_bbText);

        if ($modified == $this->_bbText) {

            return $this->_bbText;
        } else {

            $this->_bbText = $modified;
        }

        return $this->parseText();
    }

    protected function __getSearchArray()
    {
        return [
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[url\]((?:ftp|https?)://.*?)\[/url\]~s',
            '~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s',
            '~\[\*\](.*?)\[\*\]~s',
            '~\[\*\](.*?)\[/list\]~s',
            '~\[list\](.*?)\[/list\]~s',
        ];
    }

    protected function __getReplaceArray()
    {
        return [
            "<b>$1</b>",
            "<i>$1</i>",
            "<span style='text-decoration: underline;'>$1</span>",
            "<pre>$1</pre>",
            "<span style='font-size:$1px;'>$2</span>",
            "<span style='color: $1;'>$2</span>",
            "<a href='$1'>$1</a>",
            "<img src='$1' alt=''/>",
            "<li>$1</li>",
            "<li>$1</li>[/list]",
            "<ul>$1</ul>",
        ];
    }

}
