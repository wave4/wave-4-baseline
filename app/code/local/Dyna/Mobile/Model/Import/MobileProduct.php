<?php

/**
 * Class Dyna_Mobile_Model_Import_CableProduct
 */
class Dyna_Mobile_Model_Import_MobileProduct extends Omnius_Import_Model_Import_Product_Abstract
{
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = "|";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "mobile_product_import";

    /**
     * Dyna_Import_Model_Import_CableProduct constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '_' . date("Y-m-d") . '.log');
        $this->_qties = 0;
    }

    /**
     * Import cable product
     *
     * @param array $data
     * @return void
     */
    public function importProduct($data)
    {
        $skip = false;

        $data = $this->_setDataMapping($data);

        if (!$this->hasValidPackageDefined($data)) {
            return;
        }

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } elseif (!array_key_exists('name', $data) || empty($data['name'])) {
            $this->_logError('Skipping line without name');
            $skip = true;
        }

        if ($skip) {
            return;
        }

        $data['sku'] = trim($data['sku']);
        $this->_currentProductSku = $data['sku'];
        $this->_log('Start importing ' . trim($data['name']));
        $data = $this->checkPrice($data);

        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product');
            $id = $product->getIdBySku($data['sku']);
            $new = ($id == 0);
            if (!$new) {
                $this->_log('Loading existing product "' . $data['sku'] . '" with ID ' . $id);
                $product->load($id);
            }
            $skip = false;
            if (!$this->_setAttributeSetByCode($product, $data)) {
                $this->_logError('Unknown attribute set "' . $data['attribute_set'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif (!$this->_setWebsitesByCsvString($product, $data)) {
                $this->_logError('Unknown website "' . $data['_product_websites'] . '" for product ' . $data['sku']);
                $skip = true;
            } elseif ($data['tax_class_id'] === 0) {
                $product->setTaxClassId(0);
            } elseif (!$this->_setTaxClassIdByName($product, $data)) {
                $this->_logError('Unknown tax class "' . $data['tax_class_id'] . '" for product ' . $data['sku']);
                $skip = true;
            }

            if ($skip) {
                return;
            }

            $this->_setDefaults($product, $data);
            $this->_setStatus($product, $data);
            $this->_setProductData($product, $data);

            if ($new) {
                $this->_createStockItem($product);
            }

            if (!$this->getDebug()) {
                $product->save();
            }

            if (!$this->getDebug()) {
                $productVisibilityInfo = @json_decode($product->getProductVisibilityStrategy(), true);
                if ($productVisibilityInfo) {
                    Mage::getModel('productfilter/filter')->createRule($product->getId(), $productVisibilityInfo['strategy'], $productVisibilityInfo['groups']);
                } else {
                    //uncomment the following line if products that have no defined filters must be treated as available for all groups
                    //Mage::getModel('productfilter/filter')->createRule($product->getId(), Dyna_ProductFilter_Model_Filter::STRATEGY_INCLUDE_ALL, null); //available for all dealer groups
                }
            }

            unset($product);
            $this->_qties++;

            $this->_log('Finished importing ' . $data['name']);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Checks whether or not product data has both package type and package subtype valid
     * @return bool
     */
    public function hasValidPackageDefined($productData)
    {
        $valid = true;
        if (empty($productData['package_type']) || !in_array(strtolower($productData['package_type']), Dyna_Cable_Model_Product_Attribute_Option::packageTypeOptions())) {
            $productData['package_type'] = !empty($productData['package_type']) ? $productData['package_type'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33mtype\e[0m value: \e[1;31m" . $productData['package_type'], "\033[0m (case insensitive)\n";
            $this->_log("(SKU " . $productData['sku'] . ") Skipped entire product because of invalid package type value: " . $productData['package_type'] . " (case insensitive)");
            $valid = false;
        }

        if (empty($productData['package_subtype']) || !in_array($productData['package_subtype'], Dyna_Cable_Model_Product_Attribute_Option::packageSubtypeOptions())) {
            $productData['package_subtype'] = !empty($productData['package_subtype']) ? $productData['package_subtype'] : "none provided";
            echo "(SKU \e[1;32m" . $productData['sku'] . "\e[0m) Skipped entire product because of invalid package \e[1;33msubType\e[0m value: \e[1;31m" . $productData['package_subtype'], "\033[0m (case sensitive)\n";
            $this->_log("(SKU " . $productData['sku'] . ") Skipped entire product because of invalid package subType value: " . $productData['package_subtype'] . " (case sensitive)");
            $valid = false;
        }

        return $valid;
    }


    /**
     * Process custom data mapping for cable products
     *
     * @param array $data
     * @return array
     */
    protected function _setDataMapping($data)
    {
        // SKU format as specified in CAT-11_Product Catalogue HLD_version_2.4
        $data['package_type'] = 'Mobile';
        $data['package_subtype'] = $data['package_subtype'] == "add on" ? "Addon" : $data['package_subtype'];

        // Default telesales website
        $data['_product_websites'] = 'telesales';

        // Default value 0 for hide attributes
        if (!isset($data['hide_in_configurator'])) {
            $data['hide_in_configurator'] = 0;
        }
        if (!isset($data['hide_in_shopping_cart'])) {
            $data['hide_in_shopping_cart'] = 0;
        }
        if (!isset($data['hide_in_order_overview'])) {
            $data['hide_in_order_overview'] = 0;
        }

        // If tax class id is not provided, we set it to Taxable Goods by default
        if (! isset($data['tax_class_id'])) {
            $data['tax_class_id'] = 'Taxable Goods';
        } elseif ($data['tax_class_id'] == 'None') {
            $data['tax_class_id'] = 0;
        }

        // new name => old name
        $renamedFields = [
            'bank_collection_mandatory' => 'bank_collection',
            'sku' => 'kias_code',
        ];

        foreach ($renamedFields as $new => $old) {
            if (empty($data[$new]) && !empty($data[$old])) {
                $data[$new] = $data[$old];
            }
        }

        unset($vat);

        if (!empty($data['one_time_charge']) && empty($data['price'])) {
            $data['price'] = $data['one_time_charge'];
        }

        if (!empty($data['net_material_price'])) {
            $data['net_material_price'] = $this->getFormattedPrice($data['net_material_price']);
        }

        if (!empty($data['gross_material_price'])) {
            $data['gross_material_price'] = $this->getFormattedPrice($data['gross_material_price']);
            if (empty($data['price'])) {
                $data['price'] = $data['gross_material_price'];
            }
        }

        // determine the name based on the gui description or sku
        $data['name'] = $this->determineName($data);
        // if there is no gui description or sku -> unset the name
        if (!$data['name']) {
            unset($data['name']);
        }

        $dateTimeFields = array(
            'effective_date',
            'expiration_date',
        );

        foreach ($dateTimeFields as $dateTimeField) {
            if (!empty($data[$dateTimeField])) {
                $data[$dateTimeField] = date("Y-m-d H:i:s", strtotime($data[$dateTimeField]));
            }
        }
        // By default product status = enabled
        $data['status'] = 1;
        // determine if is prepaid based on attribute set
        $data['prepaid'] = $this->determineIsPrepaid($data);
        // if the attribute set does not meet the condition for prepaid -> unset the prepaid value
        if (! $data['prepaid']) {
            unset($data['prepaid']);
        }

        /** Text retrieved for service_description attribute needs to be parsed as BB code */
        $bbParser = new Dyna_Mobile_Model_Import_BBParser();
        if (!empty($data['service_description'])) {
            $bbParser->setText($data['service_description']);
            $data['service_description'] = $bbParser->parseText();
        }

        return $data;
    }

    /**
     * Returns the date as a string Y-m-d
     *
     * @param string $date
     * @return string
     */
    private function getDateFormatted($date)
    {
        return substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
    }

    /**
     * @param $price
     * @return string
     */
    private function getFormattedPrice($price)
    {
        return number_format($price, 4);
    }

    /**
     * Determine the name based on gui description or sku
     *
     * @param array $data
     * @return string
     */
    private function determineName($data)
    {
        $productName = null;
        if (!empty($data['name'])) {
            $productName = $data['name'];
        } elseif (isset($data['sku']) && $data['sku']) {
            $productName = $data['sku'];
        }

        return $productName;
    }

    /**
     * Determine if is prepaid based on the attribute set
     *
     * @param array $data
     * @return null|string
     */
    private function determineIsPrepaid($data)
    {
        $prepaid = null;
        if (!empty($data['prepaid'])) {
            $prepaid = $data['prepaid'];
        } elseif (isset($data['attribute_set'])
            && ($data['attribute_set'] == 'prepaid_package' || $data['attribute_set'] == 'mobile_prepaid_tariff')
        ) {
            $prepaid = 'Yes';
        }

        return $prepaid;
    }

    /**
     * Set product model default values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $data
     */
    protected function _setDefaults($product, $data)
    {
        $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        // This will format itself trough observer
        $product->setUrlKey($data['name']);
        $product->setProductVisibilityStrategy('all');
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setIsDeleted('0');
    }

    /**
     * Checks whether or not price is listed in import file (can be 0, but needs to exists)
     */
    protected function checkPrice($data)
    {
        if (!isset($data['price']) || $data['price'] === "") {
            $data['price'] = 0;
            $this->_log("[NOTICE] Did not receive any value for price column. Setting price to 0");
        }

        return $data;
    }
}
