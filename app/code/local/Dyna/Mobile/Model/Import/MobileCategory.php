<?php

/**
 * Class Dyna_Mobile_Model_Import_MobileCategory
 */
class Dyna_Mobile_Model_Import_MobileCategory extends Omnius_Import_Model_Import_Category_Abstract
{
    /** @var string $_multiselectSplit */
    protected $_multiselectSplit = "|";
    /** @var string $_csvDelimiter */
    protected $_csvDelimiter = ';';
    /** @var string $_logFileName */
    protected $_logFileName = "categories_import";

    private $categoriesAndSubcategories = [];
    private $parentPath;
    /** @var Mage_Catalog_Model_Product $product */
    private $product;
    private $categoryId;
    private $categoryPath;

    const DEFAULT_CATEGORY_PATH = '1/2';
    const CATEGORY_PATH_DELIMITER = '/';
    const TREE_CATEGORIES_DELIMITER = "->";
    const CATEGORY_DISPLAY_MODE = "PRODUCTS";
    const CATEGORY_IS_ACTIVE = 1;

    /**
     * Dyna_Mobile_Model_Import_Categories constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile($this->_logFileName . '_' . date("Y-m-d") . '.log');
        $this->_qties = 0;
    }

    /**
     * Import categories
     *
     * @param array $data
     * @return void
     */
    public function importCategory($data)
    {
        $skip = false;
        $this->parentPath = self::DEFAULT_CATEGORY_PATH;
        $this->product = null;
        $this->categoryId = null;

        if (!array_key_exists('sku', $data) || empty($data['sku'])) {
            $this->_logError('Skipping line without sku');
            $skip = true;
        } else if (!array_key_exists('category_tree', $data) || empty($data['category_tree'])) {
            $this->_logError('Skipping line without category tree');
            $skip = true;
        } else {
            $this->product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data['sku']);
            if (!$this->product || !$this->product->getId()) {
                $this->_logError('Product with sku  ' . $data['sku'] . ' does not exist');
                $skip = true;
            }
        }

        if ($skip) {
            return;
        }

        $this->_setDataMapping($data);
        $this->_log('Start importing the category tree ' . $data['category_tree'] . ' for product ' . $this->product->getSku());

        try {
            $this->createCategoryWithProducts();
            $this->_qties++;
            $this->_log('Finished importing the category tree ' . $data['category_tree']);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex->getMessage());
        }
    }

    /**
     * Map the data from csv with the categories that are exploded from the category tree;
     * @param array $data
     */
    protected function _setDataMapping($data)
    {
        // set the category and subcategories names that need to be created
        $categoriesAndSubcategories = explode(self::TREE_CATEGORIES_DELIMITER, $data["category_tree"]);
        $this->categoriesAndSubcategories = [];
        $level = 1;
        foreach ($categoriesAndSubcategories as $key => $name) {
            $level++;
            $this->categoriesAndSubcategories[$key]['name'] = $name;
            $this->categoriesAndSubcategories[$key]['description'] = isset($data["description"]) ? $data["description"] : '';
            $this->categoriesAndSubcategories[$key]['is_anchor'] = isset($data["is_anchor"]) ? $data["is_anchor"] : 0;
            $this->categoriesAndSubcategories[$key]['is_family'] = isset($data["is_family"]) ? $data["is_family"] : 0;
            $this->categoriesAndSubcategories[$key]['category_tree'] = $data["category_tree"];
            $this->categoriesAndSubcategories[$key]['level'] = $level;
        }
    }

    /**
     * Creates/Updates categories
     * And assigns the product sku
     */
    private function createCategoryWithProducts()
    {
        $this->categoryPath = self::DEFAULT_CATEGORY_PATH;
        $this->isLastNode = false;

        $length = count($this->categoriesAndSubcategories);
        $i = 0;
        /**
         * @var Mage_Catalog_Model_Category $category
         */
        // check if categories exist and also if the assigned subcategories exist
        foreach ($this->categoriesAndSubcategories as $key => $category) {
            $i++;
            /*
            * @var Mage_Catalog_Model_Category $categoryModel
            * @var Mage_Catalog_Model_Category $existentCategory
            */
            // check if categories exist and also if the assigned subcategories exist
            $categoryModel = Mage::getModel('catalog/category');
            $existentCategory = $categoryModel->loadByAttribute('name', $category['name']);
            if ($existentCategory && $existentCategory->getId()) {
                $this->categoryPath .= self::CATEGORY_PATH_DELIMITER . $existentCategory->getId();
                $this->categoriesAndSubcategories[$key]['path'] = $this->categoryPath;
                $this->categoriesAndSubcategories[$key]['existentCategory'] = $existentCategory;
                $this->_log('Existing category found with name: ' . $category['name'] . ' and path ' . $this->categoryPath);
                $this->parentPath = $this->categoryPath;

                $this->categoryId = $existentCategory->getId();
            } else {
                $this->_log('No existing category found with name: ' . $category['name']);
                $this->categoriesAndSubcategories[$key]['existentCategory'] = null;
                $this->categoriesAndSubcategories[$key]['path'] = $this->parentPath;
                if ($i == $length) {
                    $this->isLastNode = true;
                }
                $this->createCategory($this->categoriesAndSubcategories[$key]);
            }
        }

        $this->assignProductToCategory();
    }

    /**
     * Create or update a category
     *
     * @access private
     * @param array $categoryData (contains all category data: the category that has been found with the given name and the new data that
     * needs to be added for add and update)
     * @return void
     */
    private function createCategory($categoryData)
    {
        /**
         * @var Mage_Catalog_Model_Category $existentCategory
         * @var Mage_Catalog_Model_Category $categoryModel
         */
        $existentCategory = $categoryData['existentCategory'];

        /**
         * verify if the category exists:
         * - the category with the same name exists;
         * - has the same level;
         * - has the same path;
         * - is on the same website;
         * // todo: send website as parameter
         */

        if ($existentCategory
            && $existentCategory->getLevel() == $categoryData['level']
            && $existentCategory->getPath() == $categoryData['path']
            && in_array(Mage::app()->getStore('telesales')->getId(), $existentCategory->getStoreIds())
        ) {
            $this->_log('Category will be updated');
            // category exists => update category
            $this->saveCategory($categoryData, false);
            return;
        }

        $this->_log('Category will be added');
        $this->saveCategory($categoryData, true);
    }

    /**
     * Saves or updates a category
     * The update is done on the following fields: description, is_anchor and is_family
     * The save is done with the following fields: store, name, path, description, display mode, is active, is anchor, is family
     * All categories are added as active and with the display mode: products
     *
     * @access private
     * @param array $categoryData (contains the found category if is an update; also contains the data that needs to be updated or added)
     * @param bool $newCategory
     */
    private function saveCategory($categoryData, $newCategory = false)
    {

        $categoryModel = ($newCategory) ? Mage::getModel('catalog/category') : $categoryData['existentCategory'];
        $general = [];
        if ($newCategory) {
            $general['name'] = $categoryData['name'];
            $general['path'] = $this->parentPath;
        }
        $general['display_mode'] = self::CATEGORY_DISPLAY_MODE;
        $general['is_active'] = self::CATEGORY_IS_ACTIVE;
        $this->_log('is_anchor is determined:');
        $general['is_anchor'] = $this->_getYesNoData($categoryData['is_anchor']);

        if ($this->isLastNode) {
            $general['description'] = $categoryData['description'];
            $this->_log('is_family is determined:');
            $general['is_family'] = $this->_getYesNoData($categoryData['is_family']);
        }

        $categoryModel->addData($general);
        try {
            $categoryModel->save();
            $this->categoryId = $categoryModel->getId();
            $this->parentPath .= self::CATEGORY_PATH_DELIMITER . $this->categoryId;
            $this->_log('New category was added/updated with name <' . $categoryData['name'] . '> and path ' . $this->parentPath);
            unset($categoryModel);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }

    /**
     * Assign the product SKU to the category
     *
     * @access private
     * @return void
     */
    private function assignProductToCategory()
    {
//        $categories = array($this->categoryId); // reset all categories and add only the newest one
        // merge the existing categories of a product with the new categories from the import
        $categories = $this->product->getCategoryIds();
        $categories[count($categories)] = $this->categoryId;
        try {
            $this->product->setCategoryIds($categories);
            $this->product->save();
            $this->_log('Product with sku ' . $this->product->getSku() . ' was added to category id ' . $this->categoryId);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $this->_logEx($ex);
        }
    }
}
