<?php

class Dyna_Address_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Perform serviceability check for migration to dsl at another address
     * @param array $address
     * @return string
     */
    public function getMigrationAvailabilityForAddress(array $address)
    {
        //@todo remove hardcoded check and use services or stubs
        if ($address['city'] == "Berlin") {
            return [
                'cable' => 1,
                'dsl' => 1,
                'addressId' => 22355,
            ];
        } elseif ($address['city'] == "Frankfurt") {
            return [
                'cable' => 0,
                'dsl' => 1,
                'addressId' => 22356,
            ];
        } else {
            return [
                'cable' => 0,
                'dsl' => 0,
                'addressId' => 22352,
            ];
        }
    }
}
