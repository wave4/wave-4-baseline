<?php
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class Dyna_Address_Model_Client_ValidateAddressClient extends Dyna_Service_Model_Client
{
    const CONFIG_PATH = 'omnius_service/validate_address/';
    // Address Types
    const SERVICE_ADDRESS = "service_address";
    const DELIVERY_ADDRESS = "delivery_address";
    const BILLING_ADDRESS = "billing_address";
    const CUSTOMER_ADDRESS = "customer_address";
    const SEPA_ADDRESS = "sepa_address";

    protected $invalidAddress;

    public function __construct($wsdl, array $options =array())
    {
        parent::__construct($wsdl, $options);


        $this->invalidAddress = [
            'data["type"] =="'.self::SERVICE_ADDRESS.'"and data["country"] != "DEU" and data["addressnr"] > 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::SERVICE_ADDRESS.'"and data["country"] == "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::SERVICE_ADDRESS.'"and data["country"] != "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::DELIVERY_ADDRESS.'"and data["country"] == "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::BILLING_ADDRESS.'"and data["country"] == "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::CUSTOMER_ADDRESS.'"and data["country"] == "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::SEPA_ADDRESS.'"and data["country"] != "DEU" and data["addressnr"] > 0 and data["confirmation"] == false and data["status"] ==false',
            'data["type"] =="'.self::SEPA_ADDRESS.'"and data["country"] != "DEU" and data["addressnr"] == 0 and data["confirmation"] == false and data["status"] ==false',
        ];
    }

    /**
     * @param array $params
     * @return array
     */
    public function executeValidateAddress($params = array()) : array
    {
        $params = $this->mapAddressParams($params);
        $this->setRequestHeaderInfo($params);

        $results = $this->ValidateAddress($params);

        return $results;
    }

    /**
     * @param $params
     * @return bool
     */
    public function isValidAddress($params)
    {
        $result = $this->executeValidateAddress($params);

        $data['type'] = $params['type'];
        $data['confirmation'] = $result['Success'];
        $data['status'] = $result['Status']['Text'];
        $data['country'] = $result['ValidateAddress'][0]['Country']['Name'];
        $data['addressnr'] = $result['ReturnedNumberOfRecords'];

        $valid = true;
        foreach($this->invalidAddress as $exp) {
            $language = new ExpressionLanguage();
            $passed = $language->evaluate(
                $exp,
                ['data' => $data]
            );
            if($passed == true) {
                $valid = false;
                break;
            }
        }

        return $valid;
    }

    /**
     * @param $params
     */
    public function mapAddressParams($params)
    {
        $params['ValidateAddress']['CityName'] = $params['city'];
        $params['ValidateAddress']['PostalZone'] = $params['postcode'];
        $params['ValidateAddress']['StreetName'] = $params['street'];
        $params['ValidateAddress']['HouseNumber'] = $params['houseno'];
        $params['ValidateAddress']['District'] = !empty($params['district']) ? $params['district'] : "";
        $params['ValidateAddress']['HouseNumberAddition'] = !empty($params['addition']) ? $params['addition'] : "";

        return $params;
    }
}
