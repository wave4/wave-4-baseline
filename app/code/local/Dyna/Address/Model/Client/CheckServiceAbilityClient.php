<?php
class Dyna_Address_Model_Client_CheckServiceAbilityClient extends Dyna_Service_Model_Client
{
    const CONFIG_PATH = 'omnius_service/service_ability/';

    /** @var array Client options */
    protected $_options = [];

    /**
     * Perform serviceability check and retrieve technology from services
     * @param $params
     * @return array
     */
    public function executeCheckServiceAbility($params)
    {
        $this->setRequestHeaderInfo($params);
        $addressId = !empty($params['addressId']) ? $params['addressId'] : "";

        // We need an address id to perform serviceability check
        if (!$addressId) {
            Mage::throwException ("Invalid address id received as request parameter for CheckServiceAbility service call.");
        }

        $params = $this->mapAddressParams($params);
        $this->setRequestHeaderInfo($params);

        $addressDetails = $this->CheckServiceAbility($params);

        $result =  $this->parseCheckServiceAbilityResponse($addressDetails);
        $result['address_id'] = $addressId;

        // Save address on session
        /** @var Dyna_Address_Model_Storage $sessionAddress */
        $sessionAddress = Mage::getSingleton('dyna_address/storage');

        // Set serviceability on current address
        $sessionAddress->setServiceAbility($result);

        $validAddresses = $sessionAddress->getData('validAddresses');

        if (! empty($validAddresses)) {
            foreach ($validAddresses as $validAddress) {
                if ($validAddress['ID'] == $params['Address']['ID']) {
                    $sessionAddress->setData('address', $validAddress);
                }
            }
        }

        return $result;
    }

    public function mapAddressParams($params)
    {
        $params['Address']['ID'] = $params['addressId'];
        $params['Address']['StreetName'] = !empty($params['street']) ? $params['street'] : "";
        $params['Address']['BuildingNumber'] = !empty($params['buildingNr']) ? $params['buildingNr'] : "";
        $params['Address']['CityName'] = !empty($params['city']) ? $params['city'] : "";
        $params['Address']['PostalZone'] = !empty($params['postalcode']) ? $params['postalcode'] : "";

        return $params;
    }

    /**
     * Setup the Top Bar Available Services
     * @param $addressDetails
     * @return mixed
     */
    public function parseCheckServiceAbilityResponse($addressDetails)
    {
        /** @var Dyna_Configurator_Helper_Data $configuratorHelper */
        $configuratorHelper = Mage::helper('dyna_configurator');

        $services = [];
        foreach($addressDetails['LineItem'] as $serviceItems) {
            foreach($serviceItems['CatalogueLine'] as $serviceItem) {
                $serviceId = $serviceItem['ID'];
                $serviceStatus = $serviceItem['Item']['Status']['StatusReasonCode'];

                $color = $this->getMarketabilityColorByStatus($serviceStatus);

                $services[$serviceId]['bandwidth'] = (!empty($serviceItem['Item']['Bandwidth']['DownStream'])) ? $serviceItem['Item']['Bandwidth']['DownStream'] : 0;
                $services[$serviceId]['color'] = $color;
                $services[$serviceId]['tooltip'] = $configuratorHelper->__($serviceId);
            }
        }

        $response['top_bar_available_services'] = $services;
        $response['full_address'] = Mage::getSingleton('dyna_address/storage')->getServiceAddress(true);

        return $response + $addressDetails;
    }


    /**
     * There are certain marketability values (letters) that are returned from the CheckServiceAbility call which indicate what color should be displayed.
     * If the marketability is ‘A’ (reject any order), than the traffic lights will show red color
     * If the marketability is ‘B’ (allow any order), than the traffic lights will show green color.
     * If the marketability is ‘I’, than the traffic lights will show red color
     * If the marketability is ‘V’, than the traffic lights will show green color
     * If the marketability is ‘C’, than the traffic lights will show green color.
     *
     * @param string $status
     * @return string
     */
    protected function getMarketabilityColorByStatus($status)
    {
        switch ($status) {
            case 'A':
            case 'I':
                $color = 'red';
                break;
            case 'B':
            case 'V':
            case 'C':
                $color = 'green';
                break;
            default:
                $color = 'yellow';
        }

        return $color;
    }
}
