<?php
class Dyna_Address_Model_Client extends Dyna_Service_Model_Client
{
    const CONFIG_PATH = 'omnius_service/address_search/';

    /** @var string Unique client identifier */
    protected $_type = 'address_search';

    /** @var array Client options */
    protected $_options = [];

    public function getAddress($params)
    {
        $values = $this->CheckAddress($params);

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    public function getAddressList($params)
    {
        $values = $this->addressSearch($params);

        if ($this->getUseStubs()) {
            return $values;
        }

        // todo: implement service call, currently in stub mode
    }

    public function getStubClient()
    {
        return Mage::getSingleton('dyna_service/multipleStubClient');
    }
}
