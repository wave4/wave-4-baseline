<?php

/**
 * Class Dyna_Address_Model_Storage
 * @method setAddress($data)
 * @method getAddress()
 * @method setServiceAbility($data)
 * @method getServiceAbility()
 */
class Dyna_Address_Model_Storage extends Mage_Core_Model_Session_Abstract
{
    public function __construct()
    {
        $this->init('address_storage' . '_' . (Mage::app()->getStore()->getWebsite()->getCode()));
    }

    /**
     * Clear current loaded address
     */
    public function clearAddress()
    {
        $this->clear();
    }

    /**
     * Return the service address for current order in the following format if asString is set to false
     * array(
     *  "street, houseNo",
     *  "postalCode city"
     * )
     * @return array
     */
    public function getServiceAddress($asString = false)
    {
        //If model data is empty, than no serviceability check was performed
        if (empty($this->getData())) {
            if ($asString) {
                return "";
            }
            return [];
        }

        $address = $this->getData('address');

        if ($asString) {
            //One string containing entire address
            return $address['StreetName'] . " " . $address['BuildingNumber'] . ", <br/> " . $address['PostalZone'] . " " . $address['CityName'] . " " . $address['District'];
        } else {
            //An array containing street and houseNo in first key, postCode and city in the other key
            return [
                $address['StreetName'] . " " . $address['BuildingNumber'],
                $address['PostalZone'] . " " . $address['CityName'] . " " . $address['District'],
            ];
        }
    }

    /**
     * Return service address by components
     * @return array
     */
    public function getServiceAddressParts()
    {
        $address = $this->getData('address');

        $street = !empty($address['StreetName']) ? $address['StreetName'] : "";
        $houseNo = !empty($address['BuildingNumber']) ? $address['BuildingNumber'] : "";
        $postCode = !empty($address['PostalZone']) ? $address['PostalZone'] : "";
        $city = !empty($address['CityName']) ? $address['CityName'] : "";
        $addition = !empty($address['HouseNumberAddition']) ? $address['HouseNumberAddition'] : "";
        $district = !empty($address['District']) ? $address['District'] : "";

        /** @var Dyna_Customer_Model_Customer $customer */
        $customer = Mage::getSingleton('customer/session')
            ->getCustomer();

        return [
            "full_name" => $customer->getName(),
            "firstname" => $customer->getFirstName(),
            "lastname" => $customer->getLastName(),
            "title" => $customer->getTitle(),
            "street" => $street,
            "houseno" => $houseNo,
            "addition"  => $addition,
            "postcode" => $postCode,
            "city" => $city,
            "district" => $district,
            "foundAddress" => isset($address) ? true : false
        ];
    }

    /*
     * Returns whether or not the serviceability check has been performed for current order
     * return bool
     */
    public function isServiceAddressChecked()
    {
        return !empty($this->getServiceAbility());
    }

    /**
     * Return the current addressId
     * @return string|null
     */
    public function getAddressId()
    {
        $serviceAbility = $this->getServiceAbility();

        return !empty($serviceAbility['address_id']) ? $serviceAbility['address_id'] : null;
    }
}
