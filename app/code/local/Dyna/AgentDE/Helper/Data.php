<?php

/**
 * Class Dyna_AgentDE_Helper_Data
 * Helper class for Dyna_AgentDE module
 */
class Dyna_AgentDE_Helper_Data extends Mage_Core_Helper_Abstract
{

    /*
     * Get all active campaigns from database
     */
    public function getAvailableCampaigns()
    {
        $campaignsCollection = Mage::getModel('agentde/dealerCampaigns')->getCollection();

        $allCampaigns = array();
        foreach ($campaignsCollection as $campaign) {
            $allCampaigns[] = array('value' => $campaign->getId(), 'label' => $campaign->getName());
        }

        return $allCampaigns;
    }

    /*
     * Map campaign data to dealer edit values
     */
    public function mapCampaignData()
    {
        /** This empty will not work beneath php 5.5 */
        if (!empty($dealer = Mage::registry('dealer_data'))) {
            $dealerId = $dealer->getId();

            $dealerCampaigns = Mage::getModel('agentde/DealerCampaigns')->getCollection()->getCampaignsForDealer($dealerId, true);

            /** fetch allready set data on registry */
            $data = Mage::registry("dealer_data");

            /** if the multiselect id changes in Dyna_AgentDE_Block_Adminhtml_Dealer_Edit_Tab_Form it needs to be changed here too */
            $data['campaign_id'] = $dealerCampaigns;

            /** unregister and register data */
            Mage::unregister('dealer_data');
            Mage::register('dealer_data', $data);
        }
    }
}
