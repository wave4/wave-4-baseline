<?php

/**
 * Class Dyna_AgentDE_Model_Dealer
 * Extends and overrides Dyna_Agent_Model_Dealer _beforeSave method to map the dealer to it's allowed campaigns
 */
class Dyna_AgentDE_Model_Dealer extends Dyna_Agent_Model_Dealer
{
    /**
     * @return $this
     */
    public function _beforeSave()
    {
        if (!$dealerId = $this->getId()) {
            return $this;
        }

        /** Previously existing campaigns */
        $dealerHadCampaigns = Mage::getModel('agentde/DealerCampaigns')
            ->getCollection()
            ->getCampaignsForDealer($dealerId, true);

        if (empty($dealerHadCampaigns)) {
            $dealerHadCampaigns = [];
        }

        /** New campaigns */
        $dealerHasCampaigns = $this->getData('campaign_id');
        if (empty($dealerHasCampaigns)) {
            $dealerHasCampaigns = [];
        }

        $connection = Mage::getSingleton('core/resource')->getConnection('read');

        /** Delete the no longer available asigned campaigns */
        $toBeDeleted = array_diff($dealerHadCampaigns, $dealerHasCampaigns);
        if (!empty($toBeDeleted)) {
            $query = "delete from dealer_allowed_campaigns where dealer_id='" . $this->getId() . "' and campaign_id in (" . implode(',', $toBeDeleted) . ")";
            $connection->query($query);
        }

        /** Add the new assigned campaigns */
        $toBeAdded = array_diff($dealerHasCampaigns, $dealerHadCampaigns);
        foreach ($toBeAdded as $new) {
            $query = "insert into dealer_allowed_campaigns(dealer_id, campaign_id, allowed) values('" . $this->getId() . "', '" . $new . "', '1')";
            $connection->query($query);
        }

        return $this;
    }

    /**
     * Returns all campaigns to which a dealer is assigned
     * @return mixed
     */
    public function getDealerCampaigns($asArray = false)
    {
        return Mage::getModel('agentde/DealerCampaigns')
            ->getCollection()
            ->getCampaignsForDealer($this->getId(), $asArray);
    }
}
