<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Observer
 */
class Dyna_AgentDE_Model_Observer extends Dyna_Agent_Model_Observer
{
    /**
     * Listener for the preDispatch method of the Mage_Core_Controller_Front_Action
     * If the customers are logged in but no dealer is found on their session, redirect
     * them to the customer/account/dealer
     *
     * @param $observer
     */
    public function validateBlackListUrlsPreDispatch($observer)
    {
        /** @var Mage_Core_Controller_Front_Action $controller */
        $controller = $observer->getEvent()->getControllerAction();
        $request = $observer->getControllerAction()->getRequest();
        $this->validateBlackList($request, $controller);
    }

    /**
     * @param $request
     * @param $controller
     */
    protected function validateBlackList($request, $controller)
    {
        // Check for black listed routes - they will not be resolved
        $blackList = Mage::getConfig()->getNode('global/black_list_urls');

        if ($blackList) {
            $requestStr = $request->getRequestString();
            foreach ($blackList->children() as $url) {
                if (preg_match($url, $requestStr, $matches)) {
                    $controller->getResponse()->setRedirect(Mage::getUrl('/'))->sendHeaders();
                }
            }
        }
    }
}
