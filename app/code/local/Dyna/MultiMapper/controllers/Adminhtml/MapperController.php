<?php

/**
 * Class Dyna_MultiMapper_Adminhtml_MapperController
 */
class Dyna_MultiMapper_Adminhtml_MapperController extends Mage_Adminhtml_Controller_Action
{
    public function editAction()
    {
        $this->_title($this->__("MultiMapper"));
        $this->_title($this->__("Mapper"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("multimapper/mapper")->load($id);
        if ($model->getId()) {
            /** VDFDE specific - addons come from another collection */
            $addons = Mage::getModel("dyna_multimapper/addon")
                ->getCollection()
                ->addFieldToFilter('mapper_id', ["eq" => $model->getId()])
                ->load();

            $counter=1;
            foreach ($addons as $addon) {
                $model->setData("addon" . $counter . "_name", $addon->getAddonName());
                $model->setData("addon" . $counter . "_id", $addon->getId());
                $model->setData("addon" . $counter . "_desc", $addon->getAddonDesc());
                $model->setData("addon" . $counter . "_nature", $addon->getAddonNature());
                $model->setData("addon" . $counter . "_soc", $addon->getAddonSoc());
                $counter++;
            }

            Mage::register("mapper_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("multimapper/mapper");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Manager"), Mage::helper("adminhtml")->__("Mapper Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapper Description"), Mage::helper("adminhtml")->__("Mapper Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit"))->_addLeft($this->getLayout()->createBlock("multimapper/adminhtml_mapper_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("multimapper")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data) {
            try {
                /** Unlink addons from mapper data */
                /** @var $mapper Dyna_MultiMapper_Model_Mapper */
                if ($this->getRequest()->getParam("id")) {
                    $mapper = Mage::getModel("multimapper/mapper")
                        ->load($this->getRequest()->getParam("id"));
                } else {
                    $mapper = Mage::getModel("multimapper/mapper");
                }

                $oldAddons = $mapper->getAddons();
                $newAddons = [];

                $i=1;
                while (true) {
                    if (array_key_exists("addon" . $i . "_soc", $post_data)) {
                        $newAddons[$post_data["addon" . $i . "_soc"]] = [
                            /** cannot rely on combination of soc and nat, so adding id support */
                            "mapper_id" => $mapper->getId(),
                            "addon_id" => !empty($post_data["addon" . $i . "_id"]) ? (int)$post_data["addon" . $i . "_id"] : null,
                            "addon_name" => !empty($post_data["addon" . $i . "_name"]) ? $post_data["addon" . $i . "_name"] : "",
                            "addon_desc" => !empty($post_data["addon" . $i . "_desc"]) ? $post_data["addon" . $i . "_desc"] : "",
                            "addon_nature" => !empty($post_data["addon" . $i . "_nature"]) ? $post_data["addon" . $i . "_nature"] : "",
                            "addon_soc" => !empty($post_data["addon" . $i . "_soc"]) ? $post_data["addon" . $i . "_soc"] : "",
                        ];
                        unset($post_data["addon" . $i . "_name"]);
                        unset($post_data["addon" . $i . "_desc"]);
                        unset($post_data["addon" . $i . "_nature"]);
                        unset($post_data["addon" . $i . "_soc"]);
                        $i++;
                    } else {
                        break;
                    }
                }

                /** Adding product ids to mapper */
                if (!empty($post_data['commercial_sku'])) {
                    $post_data['commercial_id'] = Mage::getModel("catalog/product")->getIdBySku($post_data['commercial_sku']);
                }
                if (!empty($post_data['promo_sku'])) {
                    $post_data['promo_id'] = Mage::getModel("catalog/product")->getIdBySku($post_data['promo_sku']);
                }

                /** Saving multMapper model */
                $mapper->addData($post_data)
                    ->save();

                /** Going further to saving addons */
                foreach ($newAddons as $socCode => $newAddonData) {
                    $addon = Mage::getModel("dyna_multimapper/addon")
                        ->setId($newAddonData['addon_id']);
                    $addon->addData($newAddonData)
                        ->setMapperId($mapper->getId())
                        ->save();
                }

                /** Deleting old addons */
                /** @var Dyna_MultiMapper_Model_Addon $addon */
                foreach ($oldAddons as $addon) {
                    if (!array_key_exists($addon->getAddonSoc(), $newAddons)) {
                        $addon->delete();
                    }
                }

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Mapper was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setMapperData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $mapper->getId()));

                    return;
                }
                $this->_redirect("*/*/");

                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setMapperData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));

                return;
            }

        }
        $this->_redirect("*/*/");
    }
}
