<?php

/**
 * Class Dyna_MultiMapper_Model_Importer
 */
class Dyna_MultiMapper_Model_Importer extends Omnius_Import_Model_Import_ImportAbstract
{
    protected $header;
    /**
     * @var string
     */
    protected $_csvDelimiter = ";";

    /** example for header mapping */
    protected $mapping = [
        "CommercialID" => "commercial_id",
    ];

    /**
     * Dyna_MultiMapper_Model_Importer constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper->setImportLogFile('multimapper_import_' . date('Y-m-d') . '.log');
    }

    /**
     * @param $path
     * @return bool
     * @throws Exception
     */
    public function import($path)
    {
        $file = fopen($path, 'r');
        $lc = 0;
        $success = true;
        $combinations = [];

        while (($line = fgetcsv($file, null, $this->_csvDelimiter)) !== false) {
            $lc++;
            if ($lc == 1) {
                $this->prepareColumns($line);
                continue;
            }

            /** Parse addon like values */
            if (!$readyForImport = array_combine($this->header, $line)) {
                $this->_log("Inconsistency in import file as the first row (columns headers) columns count differs from the current line (" . $lc . ") columns count. Skipping row.");
                continue;
            }


            /** Retrieve addons codes */
            $addons = array_filter($readyForImport, [$this, "getAddons"], ARRAY_FILTER_USE_BOTH);
            $natureCodes = array_filter($readyForImport, [$this, "getNatureCodes"], ARRAY_FILTER_USE_BOTH);

            /** Retrieve multi mapper data */
            $multiMapperData = array_diff_key($readyForImport, $addons);

            /** Update data from commercial_id to commercial_sku */
            $multiMapperData['commercial_sku'] = $multiMapperData['commercial_id'];
            $multiMapperData['commercial_id'] = "";
            $multiMapperData['promo_sku'] = isset($multiMapperData['promo_id']) ? $multiMapperData['promo_id'] : null;
            $multiMapperData['promo_id'] = "";
            $commercialSku = $multiMapperData['commercial_sku'];
            $promoSku = $multiMapperData['promo_sku'];

            /** Parse special conditions */
            $multiMapperData = $this->parseMultiMapperData($multiMapperData);

            $commercialId = Mage::getModel("catalog/product")->getIdBySku($commercialSku);
            if (! $commercialId) {
                $success = false;
                $this->_log('Skipping row ' . print_r($line, true) . ' because the product could not be found using the SKU "' . $commercialSku . '"');

                continue;
            }

            $promoId = null;
            if ($promoSku) {
                $promoId = Mage::getModel("catalog/product")->getIdBySku($promoSku);
                if (!$promoId) {
                    $success = false;
                    $this->_log('Skipping row ' . print_r($line, true) . ' because the promotion could not be found using the SKU specified');

                    continue;
                }
            }

            if (isset($combinations[$commercialSku.'-'.$promoSku])) {
                $success = false;
                $combinations[$commercialSku.'-'.$promoSku]++;

                $this->_log(sprintf('The row with product `%s` and promo `%s` was found more than once in the import file. The database was updated with the last entry.', $commercialSku, $promoSku));
            } else {
                $combinations[$commercialSku.'-'.$promoSku] = 1;
            }

            /** @var Dyna_MultiMapper_Model_Mapper $multiMapper */
            $multiMapper = Mage::helper('multimapper')->getMappingInstanceBySku($multiMapperData['commercial_sku'], $multiMapperData['promo_sku']);
            $multiMapperData['commercial_id'] = $commercialId;
            $multiMapperData['promo_id'] = $promoId;
            $multiMapper->addData($multiMapperData)
                ->save();
            
            $oldAddons = $multiMapper->getAddons();

            /** Intersect existing addons with new ones and remove not existing ones */
            foreach ($addons as $key => $addonSoc) {
                $keyIndex = preg_replace("/[^0-9]/", "", $key);
                $addonNature = isset($natureCodes['nature_code' . $keyIndex]) ? $natureCodes['nature_code' . $keyIndex] : "";

                if ((empty($addonSoc) && (string)$addonSoc != '0') && (empty($addonNature) && $addonNature != '0')) {
                    $this->_log('Skipping addon with index ' . $keyIndex .'  since both soc and nature are empty (from csv line ' . $lc .')');
                    continue;
                }

                $addon = $multiMapper->getAddonInstanceBySoc($addonSoc, $addonNature);
                if (!$addon->getId()) {
                    $addon->setAddonSoc($addonSoc)
                        ->setMapperId($multiMapper->getId())
                        ->setAddonNature($addonNature)
                        ->save();
                }
            }

            /** Remove old addons from database */
            /** @var Dyna_MultiMapper_Model_Addon $addon */
            foreach ($oldAddons as $addon) {
                if (!in_array($addon->getAddonSoc(), $addons)) {
                    $addon->delete();
                }
            }

            $this->_log(sprintf('Successfully imported CSV line %d [product `%s` - promo `%s`] multimapper entry.', $lc, $commercialSku, $promoSku));
        }

        return $success;
    }

    /**
     * Checks keys for addon like entries
     * @param $value string
     * @param $key string
     * @return bool
     */
    protected function getAddons($value, $key) : bool
    {
        if (strpos($key, "technical_id") !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $value
     * @param $key
     * @return bool
     */
    protected function getNatureCodes($value, $key) : bool
    {
        if (strpos($key, "nature_code") !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return true or false if a string is empty
     * @param $value string
     * @return string
     */
    protected function cleanEmptyValues($value) : string
    {
        return str_replace("-", "", trim($value));
    }

    /**
     * @param $firstRow
     * @return Dyna_MultiMapper_Model_Importer
     */
    protected function prepareColumns($firstRow) : self
    {
        $firstRow = array_filter($firstRow, [$this, "cleanEmptyValues"]);

        foreach ($firstRow as $entry) {
            /** Checking whether or not this field is mapped to another one */
            if (array_key_exists($entry, $this->mapping)) {
                $this->header[] = $this->mapping[$entry];
            } else {
                /** Not a mapped field, then process field to snake case */
                $this->header[] = $this->formatKey($entry);
            }
        }

        return $this;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function parseMultiMapperData($data)
    {
        /** stop_execution */
        if (!empty($data['stop_execution'])) {
            if (strtolower($data['stop_execution']) == "no") {
                $data['stop_execution'] = 0;
            } else {
                $data['stop_execution'] = 1;
            }
        }

        return $data;
    }
}
