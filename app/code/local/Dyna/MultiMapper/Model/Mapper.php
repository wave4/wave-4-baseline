<?php

/**
 * Class Dyna_MultiMapper_Model_Mapper
 */
class Dyna_MultiMapper_Model_Mapper extends Omnius_MultiMapper_Model_Mapper
{
    /**
     * @param bool $justSocs
     * @return array
     */
    public function getAddons($justSocs = false): array
    {
        $return = [];
        $addonsCollection = Mage::getModel("dyna_multimapper/addon")
            ->getCollection()
            ->addFieldToFilter("mapper_id", $this->getId())
            ->load();
        foreach ($addonsCollection as $addon) {
            if ($justSocs) {
                $return[] = $addon->getAddonSoc();
            } else {
                $return[] = $addon;
            }
        }

        return $return;
    }

    /**
     * @param $addonSoc
     * @return Dyna_MultiMapper_Model_Addon
     */
    public function getAddonInstanceBySoc($addonSoc, $addonNature = "") : Dyna_MultiMapper_Model_Addon
    {
        // if the soc has value (0 or other value) and nature has value => find the addon with eq
        if ((!empty($addonSoc) || (string)$addonSoc == '0') && (!empty($addonNature) || (string)$addonNature == '0') ) {
            if ($addon = Mage::getModel("dyna_multimapper/addon")
                ->getCollection()
                ->addFieldToFilter("mapper_id", ["eq" => $this->getId()])
                ->addFieldToFilter("addon_soc", ["eq" => $addonSoc])
                ->addFieldToFilter("addon_nature", ["eq" => $addonNature])
                ->getFirstItem()) {
                return $addon;
            }
        }
        // if the soc is null and the nature has value => find the soc with is null
        if ((empty($addonSoc) && (string)$addonSoc != '0') && (!empty($addonNature) || (string)$addonNature == '0') ) {
            if ($addon = Mage::getModel("dyna_multimapper/addon")
                ->getCollection()
                ->addFieldToFilter("mapper_id", ["eq" => $this->getId()])
                ->addFieldToFilter("addon_soc", ["null" => true])
                ->addFieldToFilter("addon_nature", ["eq" => $addonNature])
                ->getFirstItem()) {
                return $addon;
            }
        }

        // if the soc is has value (0 or other value) and the nature is empty => find the nature with is null
        if ((!empty($addonSoc) || (string)$addonSoc == '0') && (empty($addonNature) || (string)$addonNature != '0') ) {
            if ($addon = Mage::getModel("dyna_multimapper/addon")
                ->getCollection()
                ->addFieldToFilter("mapper_id", ["eq" => $this->getId()])
                ->addFieldToFilter("addon_soc", ["eq" => $addonSoc])
                ->addFieldToFilter("addon_nature", ["null" => true])
                ->getFirstItem()) {
                return $addon;
            }
        }

        return Mage::getModel("dyna_multimapper/addon");
    }
}
