<?php

/**
 * Class Dyna_MultiMapper_Model_Addon
 */
class Dyna_MultiMapper_Model_Addon extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("dyna_multimapper/addon");
    }
}
