<?php
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class Dyna_MultiMapper_Helper_Data
 */
class Dyna_MultiMapper_Helper_Data extends Omnius_MultiMapper_Helper_Data
{
    /**
     * @param $priceplanSku
     * @param $promoSku
     * @return false|Mage_Core_Model_Abstract
     */
    public function getMappingInstanceBySku($priceplanSku, $promoSku)
    {
        return $this->_getMappingInstanceByField($priceplanSku, $promoSku, 'commercial_sku', 'promo_sku');
    }

    /**
     * Get all multi mapper rules that validate the service expression
     *
     * @param $inputParams
     * @return array $active
     */
    public function getMappingInstanceByService($inputParams)
    {
        $language = new ExpressionLanguage();

        $collection = Mage::getModel('multimapper/mapper')->getCollection()
            ->addFieldToFilter('service_expression', ['notnull' => true])
            ->load();

        $active = [];
        foreach ($collection as $instance) {
            // Evaluate each rule
            $passed = $language->evaluate(
                $instance->getServiceExpression(),
                $inputParams
            );

            if ($passed) {
                $active[] = $instance;
            }
        }

        return $active;
    }
}
