<?php

/**
 * Class Dyna_MultiMapper_Block_Adminhtml_Mapper
 */
class Dyna_MultiMapper_Block_Adminhtml_Mapper extends Omnius_MultiMapper_Block_Adminhtml_Mapper
{

    /**
     * Dyna_MultiMapper_Block_Adminhtml_Mapper constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Remove backend "Upload MultiMapper" button as the import should be executed from the cli:
        // ../shell$ php import.php --type multimapper --file [name of the file]
        $this->_removeButton('import');
    }
}
