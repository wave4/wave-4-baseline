<?php

/**
 * Class Dyna_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form
 */
class Dyna_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form extends Omnius_MultiMapper_Block_Adminhtml_Mapper_Edit_Tab_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        /** OMNVFDE-405: Add more than 10 addons */
        /** New button added at the base of the form which adds new inputs for a new addon */
        /** By default, the 11th is the next one, however, if there are more than 11 addons, the counter is automatically increased to one that is not found */
        $defaultAddonsCount = 11;
        if (!empty($postData = Mage::registry("mapper_data")->getData())) {
            $defaultAddonsCount = 0;
            while (true) {
                $defaultAddonsCount++;
                $addonCountName = 'addon' . $defaultAddonsCount . '_soc';
                /** addon data not found, setting counter here */
                if (!array_key_exists($addonCountName, $postData)) {
                    break;
                }
            }
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("multimapper_form", array("legend" => Mage::helper("multimapper")->__("Item information")));


        $fieldset->addField("commercial_sku", "text", array(
            "label" => Mage::helper("multimapper")->__("Commercial SKU"),
            "class" => "required-entry",
            "required" => false,
            "name" => "commercial_sku",
        ));

        $fieldset->addField("promo_sku", "text", array(
            "label" => Mage::helper("multimapper")->__("Promo SKU"),
            "name" => "promo_sku",
        ));

        $fieldset->addField("service_expression", "textarea", array(
            "label" => Mage::helper("multimapper")->__("Service Expression"),
            "name" => "service_expression",
        ));

        $fieldset->addField("priority", "text", array(
            "label" => Mage::helper("multimapper")->__("Priority"),
            "name" => "priority",
        ));

        $fieldset->addField("stop_execution", "select", array(
            "label" => Mage::helper("multimapper")->__("Stop execution"),
            "name" => "stop_execution",
            "values" => ["1" => "Yes", "0" => "No"],
        ));

        $fieldset->addField("comment", "textarea", array(
            "label" => Mage::helper("multimapper")->__("Comment"),
            "name" => "comment",
        ));

        /** pre render all addons found, data will later be mapped to this inputs */
        for ($i = 1; $i < $defaultAddonsCount; $i++) {
           /* $fieldset->addField("addon" . $i . "_name", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Name"),
                "name" => "addon" . $i . "_name",
            ));
            $fieldset->addField("addon" . $i . "_desc", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Description"),
                "name" => "addon" . $i . "_desc",
            ));*/

            // Display only addons SOC codes
            $fieldset->addField("addon" . $i . "_soc", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " SOC"),
                "name" => "addon" . $i . "_soc",
            ));

            $fieldset->addField("addon" . $i . "_nature", "textarea", array(
                "label" => Mage::helper("multimapper")->__("Addon " . $i . " Nature"),
                "name" => "addon" . $i . "_nature",
            ));

            /** preserve addon ids, as combination of soc and nat causes issues when soc is empty and nat is used in multiple addons for the same commercial sku */
            $fieldset->addField("addon" . $i . "_id", "hidden", array(
                "name" => "addon" . $i . "_id",
            ));
        }

        $addAddonButton = $fieldset->addField('add_addon', 'button', array(
                'name' => 'add_addon',
                'label' => $this->helper('dyna_multimapper')->__('Add another addon'),
                'value' => $this->helper('dyna_multimapper')->__('Add addon'),
                'class' => 'form-button',
                'onclick' => "multiMapperAppend(this)",
            )
        );

        /** The add new addon script with the following increment number */
        $addAddonButton->setAfterElementHtml('<script>
//< ![C
var counter = ' . $defaultAddonsCount . ';
multiMapperAppend = function (element)
{
    var prependedElement = element.parentNode.parentNode;
    var newChildSoc = document.createElement("tr");
    var newChildNature = document.createElement("tr");
    newChildNature.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_nature\'>Addon " + counter + " Nature</label></td>"
                         + "<td class=\'value\'><textarea id=\'addon" + counter + "_nature\' name=\'addon" + counter + "_nature\' rows=\'2\' cols=\'15\' class=\'textarea\'></textarea></td>";
    newChildSoc.innerHTML = "<td class=\'label\'><label for=\'addon" + counter + "_soc\'>Addon " + counter + " SOC</label></td>"
                         + "<td class=\'value\'><textarea id=\'addon" + counter + "_soc\' name=\'addon" + counter + "_soc\' rows=\'2\' cols=\'15\' class=\'textarea\'></textarea></td>";
    prependedElement.parentNode.insertBefore(newChildSoc, prependedElement)
    prependedElement.parentNode.insertBefore(newChildNature, prependedElement)
    counter++;
}
//]]>
</script>');

        if (Mage::getSingleton("adminhtml/session")->getMapperData()) {
            $form->addValues(Mage::getSingleton("adminhtml/session")->getMapperData());
            Mage::getSingleton("adminhtml/session")->setMapperData(null);
        } elseif (Mage::registry("mapper_data")) {
            $form->addValues(Mage::registry("mapper_data")->getData());
        }

        return Mage_Adminhtml_Block_Widget_Form::_prepareForm();
    }
}
