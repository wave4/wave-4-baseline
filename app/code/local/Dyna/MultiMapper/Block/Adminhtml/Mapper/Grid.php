<?php

/**
 * Class Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid
 */
class Dyna_MultiMapper_Block_Adminhtml_Mapper_Grid extends Omnius_MultiMapper_Block_Adminhtml_Mapper_Grid
{
    /**
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        if (!$this->_isExport) {
            $this->addColumn("entity_id", array(
                "header" => Mage::helper("multimapper")->__("ID"),
                "align" => "right",
                "width" => "50px",
                "type" => "number",
                "index" => "entity_id",
            ));
        }
        $this->addColumn("commercial_sku", array(
            "header" => Mage::helper("multimapper")->__("Commercial SKU"),
            "index" => "commercial_sku",
        ));
        $this->addColumn("promo_sku", array(
            "header" => Mage::helper("multimapper")->__("Promo SKU"),
            "index" => "promo_sku",
        ));
        $this->addColumn("service_expression", array(
            "header" => Mage::helper("multimapper")->__("Service expression"),
            "index" => "service_expression",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        //$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }
}
