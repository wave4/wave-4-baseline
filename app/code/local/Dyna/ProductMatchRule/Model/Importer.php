<?php

/**
 * Class Dyna_ProductMatchRule_Model_Importer
 */
class Dyna_ProductMatchRule_Model_Importer extends Mage_Core_Model_Abstract
{
    protected $data = [];
    protected $opType;
    protected $operationsTypes = [];
    protected $operationsWithValues = [];
    protected $operations = [];
    protected $line = [];
    protected $position = [];
    // $websites['websiteID' => 'websiteCode']
    protected $websites = [];
    protected $lifeCycles = [];
    protected $sourceCollection = [];
    protected $sourceType = [];
    protected $ruleOrigin = [];
    /** @var Dyna_ProductMatchRule_Helper_Data $helper */
    protected $helper;

    const ALL_WEBSITES = "*";
    const CSV_DELIMITER = ";";

    /**
     * @param $path
     * @return bool
     */
    public function import($path)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $this->helper = Mage::helper('productmatchrule');
        $file = fopen($path, 'r');
        $lc = 0;
        $success = false;

        $this->setDefaultOperationTypes();
        $this->operationsWithValues = $this->helper->getOperationsWithValues();
        $this->setDefaultOperations();
        $this->setDefaultLifeCycles();
        $this->setDefaultSourceCollections();
        $this->setDefaultSourceType();
        $this->setDefaultRuleOrigin();

        $this->setAllDefaultWebsites();
        $this->position = [];
        $noImports = 0;
        while (($this->line = fgetcsv($file, null, self::CSV_DELIMITER)) !== false) {
            if ($lc == 0) {
                $headers = [
                    'source' => 'left_id',
                    'operation' => 'operation',
                    'target' => 'right_id',
                    'operation_type' => 'operation_type',
                    'priority' => 'priority',
                    'website' => 'website_id',
                    'rule_title' => 'rule_title',
                    'rule_description' => 'rule_description',
                    'locked' => 'locked',
                    'unique_id' => 'unique_id',
                    'rule_origin' => 'rule_origin',
                    'lifecycle' => 'lifecycle',
                    'service_source_expression' => 'service_source_expression',
                    'source_collection' => 'source_collection',
                    'source_type' => 'source_type',
                    'store_view' => 'website_id'
                ];

                $this->line = $this->formatKey($this->line);
                foreach ($headers as $key => $value) {
                    if (in_array($key, $this->line)) {
                        $this->position[$value] = array_search($key, $this->line);
                    }
                }
            }

            $lc++;
            if ($lc == 1) {
                continue;
            }
            $this->opType = [];
            $this->data = [];
            $foundLeftIdOrSourceExpressionCollection = false;

            // get the rule title ; trim it
            if (isset($this->position['rule_title'])) {
                $this->data['rule_title'] = trim($this->line[$this->position['rule_title']]);
            }

            // determine the id of the operation type based on the string(PRODUCT-PRODUCT)
            if (isset($this->position['operation_type'])) {
                if (!$this->setOperationType()) {
                    $this->_log('[ERROR] operation type cannot be determined for rule title: '.  $this->data['rule_title'] . '. Skipping row no'.$lc);
                    continue;
                }
            }

            if (isset($this->position['operation'])) {
                // determine the id of the operation based on the string (allowed, min)
                if (!$this->setOperation()) {
                    $this->_log('[ERROR] operation cannot be determined for rule title: '.  $this->data['rule_title'] . '. Skipping row no '.$lc);
                    continue;
                }

                // determine the the operation value for operations min, max, equal
                if (!$this->setOperationValue()) {
                    $this->_log('[ERROR] Skipping row no '.$lc);
                    continue;
                }
            }

            // determine the the left id value for the given sku and op type
            if (isset($this->position['left_id']) && !empty($this->opType)) {
                if ($this->setLeftId()) {
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
            }

            // determine the source_type; must be a string that can be associated with an id or a valid id; else error
            if (isset($this->position['source_type'])) {
                if (!$this->setSourceType()) {
                    $this->_log('[ERROR] source type cannot be determined for rule title: '.  $this->data['rule_title'] . '. Skipping row no '.$lc);
                    continue;
                }
            }

            // determine the service_source_expression
            if (isset($this->position['service_source_expression'])) {
                $this->data['service_source_expression'] = trim($this->line[$this->position['service_source_expression']]);
                if ($this->data['service_source_expression']) {
                    $foundLeftIdOrSourceExpressionCollection = true;
                }
                // the service source expression is mandatory if the source type is service OMNVFDE-408
                if(!$this->data['service_source_expression'] && isset($this->data['source_type']) && $this->data['source_type']) {
                    $this->_log('[ERROR] service_source_expression cannot be determined and is mandatory when the source type is Service( rule tile = '.
                        $this->data['rule_title'] .') Skipping row no '. $lc);
                    continue;
                }
                $this->_log('service_source_expression ' . $this->line[$this->position['service_source_expression']] . ' is mapped to ' . $this->data['service_source_expression']);
            }

            // validate if the operation_type has the required values
            if (!$this->validateRequiredLeftIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection)) {
                $this->_log('[ERROR] Skipping row no '.$lc);
                continue;
            }

            // determine the the right id value for the given sku and op type
            if (isset($this->position['right_id']) && !empty($this->opType)) {
                if (!$this->setRightId()) {
                    $this->_log('[ERROR] Skipping row no '.$lc);
                    continue;
                }
            }

            // determine the rule description
            if (isset($this->position['rule_description'])) {
                $this->data['rule_description'] = trim($this->line[$this->position['rule_description']]);
                if (!$this->data['rule_description']) {
                    unset($this->data['rule_description']);
                }
            }

            // determine the priority
            if (isset($this->position['priority'])) {
                if (!$this->setPriority()) {
                    $this->_log('[ERROR] Skipping row no '.$lc);
                    continue;
                }
            }

            // determine if is locked or not; this is not a required field; it will be locked only when the value is locked or 1, else it will be unlocked
            if (isset($this->position['locked'])) {
                $this->setIsLocked();
            }

            // determine the website ids (if is provided a code it must be mapped to a possible id; if is an id it must be a valid one, else -> error)
            if (isset($this->position['website_id'])) {
                if (! $this->setWebsiteIds()) {
                    $this->_log('[ERROR] Skipping row no '.$lc);
                    continue;
                }
            } else {
                foreach (Mage::app()->getWebsites() as $website) {
                    $this->data['website_id'][] = $website->getId();
                }
            }

            // determine the rule origin, if is set we will use that value, else 1 = import
            if (isset($this->position['rule_origin'])) {
                $this->setRuleOrigin();
            } else {
                $this->_log('rule_origin was set to the default value for import 1');
                $this->data['rule_origin'] = 1;
            }

            // determine the lifecycle. if is a string it must be a valid string that can be mapped to an id, if is an id it must be a valid one, else -> error)
            if (isset($this->position['lifecycle'])) {
                if (!$this->setLifecycleId()) {
                    $this->_log('[ERROR] Skipping row no '.$lc);
                    continue;
                }
            }

            // determine the source_collection; must be a string that can be associated with an id or a valid id; else error
            // the source collection is mandatory only if source type is product selection OMNVFDE-408
            if (isset($this->position['source_collection'])) {
                if ((!$this->setSourceCollection() || $this->data['source_collection'] == 0 || !isset($this->data['left_id'])) && $this->data['source_type'] == 0) {
                    $this->_log('[ERROR] source_collection and left_id are required for Product source type( rule tile = '.
                        $this->data['rule_title'] .') Skipping row no '.$lc);
                    continue;
                }
            }

            $removed = [];



            $existingRule = Mage::getModel('productmatchrule/rule')->getCollection()
                ->addFieldToFilter('operation_type', $this->data['operation_type'])
                ->addFieldToFilter('right_id', $this->data['right_id'])
                ->addFieldToFilter('operation', $this->data['operation'])
                ->addFieldToFilter('priority', $this->data['priority']);

            if (empty($this->data['source_type'])) {
                $existingRule->addFieldToFilter('left_id', $this->data['left_id']);
            } else {
                $existingRule->addFieldToFilter('service_source_expression', $this->data['service_source_expression']);
            }

            $existingRule = $existingRule->getFirstItem();

            $baseId = ($existingRule->getId()) ? $existingRule->getId() : null;
            if ($baseId) {
                $this->_log('[INFO] Existing rule (rule tile = '. $this->data['rule_title'] .') Updating info for row '.$lc);
            }

            // set the left_id as a condition
            if (isset($this->data['sourceSku'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2'=> [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'sku',
                        'operator' => '==',
                        'value' => $this->data['sourceSku']
                    ]
                ];
            }
            if (isset($this->data['sourceCategoryId'])) {
                $this->data['conditions'] = [
                    '1' => [
                        'type' => 'catalogrule/rule_condition_combine',
                        'aggregator' => 'all',
                        'value' => '1',
                        'new_child' => '',
                    ],
                    '1--2' => [
                        'type' => 'catalogrule/rule_condition_product',
                        'attribute' => 'category_ids',
                        'operator' => '==',
                        'value' => $this->data['sourceCategoryId']
                    ]
                ];
            }

            // Parse POST data to Dyna_ProductMatchRule_Model_MatchRule model
            $response = $this->helper->setRuleData($this->data, $baseId, true);

            if (!is_array($response)) {
                $this->_log('[ERROR] ( rule tile = '. $this->data['rule_title'] .')' . $response);
                $this->_log('[ERROR] Skipping row no '.$lc);
                // Error occurred
               continue;
            }

            try {
                foreach ($response as $model) {
                    $model->save();
                    $model->setWebsiteIds($this->data['website_id']);
                }

                foreach ($removed as $model) {
                    $model->delete();
                }

                $success = true;
            } catch (Exception $e) {
                $this->_log('[ERROR] Exception ( rule tile = '.
                    $this->data['rule_title'] .')' . $e->getMessage());
                $this->_log('[ERROR] Skipping row no '.$lc);
                // Error occurred
                continue;
            }

            $noImports++;
            $this->_log('Imported row ' . print_r($this->line, true));
        }


        fclose($file);

        $this->_log('total number of product match rule imported = ' . $noImports);
        return $success;
    }

    /**
     * @param $type
     * @param $value
     * @return null
     */
    protected function _getEntity($type, $value)
    {
        switch (strtoupper($type)) {
            case 'PRODUCT':
                return Mage::getModel("catalog/product")->getIdBySku($value);
            case 'CATEGORY':
                return Mage::getModel('catalog/category')
                    ->getCollection()
                    // load the "$value" category
                    ->addAttributeToFilter('name', $value)
                    ->getFirstItem()
                    ->getId();
            default:
                // Unknown entity
                break;
        }

        return null;
    }

    protected function _log($msg)
    {
        Mage::log($msg, null, 'matchrules_import_' . date('Y-m-d') . '.log');
    }

    /**
     * Format CSV header columns to underscore format
     *
     * @param array $header
     * @return array
     */
    protected function formatKey($header)
    {
        foreach ($header as $key => $value) {
            $header[$key] = trim($header[$key]);
            $header[$key] = str_replace(' ', '_', $header[$key]);
            $header[$key] = strtolower($header[$key]);
        }

        return $header;
    }

    /**
     * For a given value try the following match:
     * - if the given value is numeric try to match it with an ID - if is not possible error, skip row; if is found the ID is retained to be used for the import
     * - if the given value is not numeric try to match it with the value of an attribute (as it appear on the front end);
     * here we will 2 types of matches: exact name or if the name is contained in one of the attributes values
     * (for example for lifecycle in the csv can be the value '*' that will be matched with '* (all)')
     * if a match is found (exact or partial, depending on the parameter $exactLookup) than the ID is retained to be used for the import
     *
     * @param string $keyValueToLookUp (this is the name of the key that will help us for the logs)
     * @param string $valueToLookUp (this is the value that we are trying to match)
     * @param $arrayWhereToLookUp (this is the array with possible values as ID->name where we will try to find the  $valueToLookUp)
     * @param bool $exactLookup (this specifies if we should do a strpos or an exact match of string for the name)
     * @return bool
     */
    protected function lookupInKeyOrValueOfArray($valueToLookUp, $keyValueToLookUp, $arrayWhereToLookUp, $exactLookup = true)
    {
        if (is_numeric($valueToLookUp)) {
            if (array_key_exists($valueToLookUp, $arrayWhereToLookUp)) {
                $this->_log($keyValueToLookUp . ' ' . $this->line[$this->position[$keyValueToLookUp]] . ' is mapped to ' . $valueToLookUp);
                $this->data[$keyValueToLookUp] = $valueToLookUp;
                $this->_log($keyValueToLookUp . ' ' . $valueToLookUp . ' is mapped to ' . $valueToLookUp);

                return true;
            } else {
                $this->_log('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = '. $this->data['rule_title'].'). skipping row');
                return false;
            }
        }

        foreach ($arrayWhereToLookUp as $key => $arrayValueWhereToLookUp) {
            if ($exactLookup) {
                if ($arrayValueWhereToLookUp == $valueToLookUp) {
                    $this->data[$keyValueToLookUp] = $key;
                    $this->_log($keyValueToLookUp . ' ' . $valueToLookUp . ' is mapped to ' . $key);
                    return true;
                }

            } else {
                if (strpos($arrayValueWhereToLookUp, $valueToLookUp) !== false) {
                    $this->data[$keyValueToLookUp] = $key;
                    $this->_log($keyValueToLookUp . ' ' . $valueToLookUp . ' is mapped to ' . $key);
                    return true;
                }
            }
        }

        $this->_log('[ERROR] the ' . $keyValueToLookUp . ' =  ' . $valueToLookUp . ' is not valid (rule title = '. $this->data['rule_title'].').  skipping row');
        $this->_log(var_export($arrayWhereToLookUp, true));
        return false;
    }

    /**
     * Set the lifecycle id
     * if it is a string it must be mapped to one of the possible strings for lifecycle (it string must be contained in one of the default lifecycle) ->
     * and from that sting we will used the id
     * if is an int it must be mapped to one of the possible ids for lifecycle
     * else -> skip row, error
     * @return bool
     */
    protected function setLifecycleId()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['lifecycle']]));
        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'lifecycle', $this->lifeCycles, true);
    }

    /**
     * Set operation
     * If the operation cannot be mapped to an ID => error, skip row
     *
     * @return bool
     */
    protected function setOperation()
    {
        // replace the value of the operation if is a min, max, eq operation with (n) in order to identify the operation
        $operation = strtolower(preg_replace('#\(.*?\)#s', '(n)', trim($this->line[$this->position['operation']])));
        return $this->lookupInKeyOrValueOfArray($operation, 'operation', $this->operations, true);
    }

    /**
     * Set operation type
     * If the type cannot be mapped to an ID (with the given name or the given id) => error, skip row
     *
     * @return bool
     */
    protected function setOperationType()
    {
        $this->opType = explode('-', trim(strtoupper($this->line[$this->position['operation_type']])));
        $valueToLookUp = strtolower(trim($this->line[$this->position['operation_type']]));
        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'operation_type', $this->operationsTypes, true);
    }

    /**
     * Set the operation value
     * The operation value is required and must be numeric for operations: min, max, equal;
     * If the operation is min,max, eqal and the operation value is not numeric or is not present => skip row, error
     * @return bool
     */
    protected function setOperationValue()
    {
        // determine also the operation value if the operation is min, max, equal
        if (in_array($this->data['operation'], $this->operationsWithValues)) {
            // the operation value is provided between parentheses; if in parentheses is a number that number is the value; else it will be 0
            preg_match('#\((.*?)\)#', $this->line[$this->position['operation']], $match);
            $this->_log('operation_value from ' . $this->line[$this->position['operation']] . ' is mapped to ' . $match[1]);
            // error skip row
            if (!is_numeric($match[1])) {
                $this->_log('[ERROR] the operation_value is not valid (rule title = '.$this->data['rule_title'].'). skipping row');
                return false;
            }
            $this->data['operation_value'] = $match[1];
        }

        return true;
    }

    /**
     * Set the left id based on the operation type and sku
     *
     * @return bool
     */
    protected function setLeftId()
    {
        $this->data['left_id'] = $this->_getEntity($this->opType[0], trim($this->line[$this->position['left_id']]));
        if (!$this->data['left_id']) {
            $this->_log('the left_id cannot be determined.');
            return false;
        }

        // determine the source sku or source category id that will be used to create the source condition
        switch ($this->opType[0]) {
            case 'PRODUCT':
                $this->data['sourceSku'] = trim($this->line[$this->position['left_id']]);
                break;
            case 'CATEGORY':
                $this->data['sourceCategoryId'] = $this->data['left_id'];
                break;
        }
        $this->_log('left_id from ' . $this->line[$this->position['left_id']] . ' with op type ' . $this->opType[0] . ' is mapped to ' . $this->data['left_id']);
        return true;
    }

    /**
     * Set the right id based on the operation type and sku
     * If it cannot be determined => error, skip row
     *
     * @return bool
     */
    protected function setRightId()
    {
        $this->data['right_id'] = $this->_getEntity($this->opType[1], trim($this->line[$this->position['right_id']]));
        $this->_log('right_id from ' . $this->line[$this->position['right_id']] . ' with op type ' . $this->opType[1] . ' is mapped to ' . $this->data['right_id']);
        if (!$this->data['right_id']) {
            $this->_log('[ERROR] the right_id is not valid (rule title = '. $this->data['rule_title'].'). skipping row');
            return false;
        }

        return true;
    }

    /**
     * Set the priority
     * If the priority is not determined => skip row, error
     *
     * @return bool
     */
    protected function setPriority()
    {
        $this->data['priority'] = (int)$this->line[$this->position['priority']];
        if (!isset($this->data['priority'])) {
            $this->_log('[ERROR] the priority is not present/valid (rule title = '. $this->data['rule_title'].'). skipping row');
            return false;
        }

        return true;
    }

    /**
     * Set locked
     * If the values = locked or 1 => than is locked (meaning 1)
     * Else is 0, meaning unlocked
     */
    protected function setIsLocked()
    {
        $lockedData = trim($this->line[$this->position['locked']]);
        if (is_numeric($lockedData)) {
            $this->data['locked'] = ($lockedData == 1) ? 1 : 0;
        } elseif (strtolower(trim($this->line[$this->position['locked']])) == 'locked') {
            $this->data['locked'] = 1;
        } else {
            $this->data['locked'] = 0;
        }

        $this->_log('locked from ' . $this->line[$this->position['locked']] . ' is mapped to ' . $this->data['locked']);
    }

    /**
     * Try to set the websites for the rule;
     * Go through each website id or website code provided (the list provided in the csv can be of the form "id1,id2" or "code1,code2"
     * and see if the code/id provided is in the list of websites codes/ids that are possible.
     * If one id/code is not found in the possible list => skip the row, error
     *
     * @return bool
     */
    protected function setWebsiteIds()
    {
        $websitesData = explode(',', strtolower(trim($this->line[$this->position['website_id']])));
        foreach ($websitesData as $key => $possibleWebsite) {
            // if one of the possible websites provided in the csv is "*" the rule will be available for all websites; skip the rest of the code
            if ($possibleWebsite == self::ALL_WEBSITES) {
                $this->data['website_id'] = array_keys($this->websites);
                $this->_log('website_id from ' . $this->line[$this->position['website_id']] . ' is mapped to ' . print_r($this->data['website_id'], true));
                return true;
            }
            // the website provided is the code and we should store the id
            if ($validWebsite = array_search($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $validWebsite;
            } // the website provided is id and we should store it
            elseif (array_key_exists($possibleWebsite, $this->websites)) {
                $this->data['website_id'][$key] = $possibleWebsite;
            } // not match can be found between the given website and a code or an id
            else {
                $this->_log('[ERROR] the website_id ' . print_r($websitesData, true) . ' is not present/valid (rule title = '. $this->data['rule_title'].'). skipping row');
                return false;
            }
        }
        $this->_log('website_id from ' . $this->line[$this->position['website_id']] . ' is mapped to ' . print_r($this->data['website_id'], true));

        return true;
    }

    /**
     * Set the websites that are available as an array of [websiteID => websiteCode]
     */
    protected function setAllDefaultWebsites()
    {
        $presentWebsites = Mage::app()->getWebsites();
        /**
         * @var Mage_Core_Model_Website $website
         */
        foreach ($presentWebsites as $website) {
            $this->websites[$website->getId()] = strtolower($website->getCode());
        }
    }

    /**
     * Set the source_collection id
     * if it is a string it must be mapped to one of the possible strings for source_collection (it string must be contained in one of the default source_collection) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_collection
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceCollection()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['source_collection']]));
        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'source_collection', $this->sourceCollection, true);
    }

    /**
     * Set the rule_origin data
     * @return bool
     */
    protected function setRuleOrigin()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['rule_origin']]));
        if (!$this->lookupInKeyOrValueOfArray($valueToLookUp, 'rule_origin', $this->ruleOrigin, true)) {
            $this->_log('rule_origin was set to the default value for import 1');
            $this->data['rule_origin'] = 1;
        }
    }

    /**
     * Set the source_type id
     * if it is a string it must be mapped to one of the possible strings for source_type (it string must be contained in one of the default source_type) ->
     * and from that string we will used the id
     * if is an int it must be mapped to one of the possible ids for source_type
     * else -> skip row, error
     * @return bool
     */
    protected function setSourceType()
    {
        $valueToLookUp = strtolower(trim($this->line[$this->position['source_type']]));
        return $this->lookupInKeyOrValueOfArray($valueToLookUp, 'source_type', $this->sourceType, true);
    }

    /**
     * Set the default operation types [id->name(strtolower)]
     */
    protected function setDefaultOperationTypes()
    {
        $defaultOperationTypes = $this->helper->getOperationTypes();
        foreach ($defaultOperationTypes as $id => $name) {
            $this->operationsTypes[$id] = strtolower($name);
        }
    }

    /**
     * Set the default operation [id->name(strtolower)]
     */
    protected function setDefaultOperations()
    {
        $defaultOperations = $this->helper->getOperations();
        foreach ($defaultOperations as $id => $name) {
            $this->operations[$id] = strtolower($name);
        }
    }

    /**
     * Set the default lifeCycles [id->name(strtolower)]
     */
    protected function setDefaultLifeCycles()
    {
        $defaultLifeCycles = $this->helper->getLifecycles();
        foreach ($defaultLifeCycles as $id => $name) {
            $this->lifeCycles[$id] = strtolower($name);
        }
    }

    /**
     * Set the default sourceCollections [id->name(strtolower)]
     */
    protected function setDefaultSourceCollections()
    {
        $defaultSourceCollections = $this->helper->getSourceCollections();
        foreach ($defaultSourceCollections as $id => $name) {
            $this->sourceCollection[$id] = strtolower($name);
        }
    }

    /**
     * Set the default rule_origin [id->name(strtolower)]
     */
    protected function setDefaultRuleOrigin()
    {
        $defaultRuleOrigin = $this->helper->getRuleOrigin();
        foreach ($defaultRuleOrigin as $id => $name) {
            $this->ruleOrigin[$id] = strtolower($name);
        }
    }

    /**
     * Set the default sourceTypes [id->name(strtolower)]
     */
    protected function setDefaultSourceType()
    {
        $defaultSourceTypes = $this->helper->getSourceTypes();
        foreach ($defaultSourceTypes as $id => $name) {
            $this->sourceType[$id] = strtolower($name);
        }
    }

    /**
     * Validate the presence of left_id or service_source_expression
     * If the operation_type is SERVICE-PRODUCT the service_source_expression must be present
     * If the operation_type is NOT SERVICE-PRODUCT the left_id must be present
     *
     * @param bool $foundLeftIdOrSourceExpressionCollection
     * @return bool
     */
    protected function validateRequiredLeftIdOrServiceSourceExpression($foundLeftIdOrSourceExpressionCollection)
    {
        // if no left_id nor service_source_expression => error, skip row
        if (!$foundLeftIdOrSourceExpressionCollection) {
            $this->_log('[ERROR] no left_id nor service_source_expression can be determined (rule title = '. $this->data['rule_title'].'). skipping row');
            return false;
        }

        if ($this->data['operation_type'] == Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P
            || $this->data['operation_type'] == Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C
        ) {
            if (!$this->data['service_source_expression']) {
                $this->_log('[ERROR] when the operation type is SERVICE-PRODUCT or SERVICE-CATEGORY the service_source_expression must be present(rule title = '. $this->data['rule_title'].'). skipping row');
                return false;
            }
        } else {
            if (!$this->data['left_id']) {
                $this->_log('[ERROR] when the operation type is not SERVICE-PRODUCT the left_id must be present (rule title = '. $this->data['rule_title'].'). skipping row');
                return false;
            }
        }
        return true;
    }
}
