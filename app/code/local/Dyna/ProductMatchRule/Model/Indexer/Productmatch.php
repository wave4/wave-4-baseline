<?php

/**
 * Class Dyna_ProductMatchRule_Model_Indexer_Productmatch
 */
class Dyna_ProductMatchRule_Model_Indexer_Productmatch extends Omnius_ProductMatchRule_Model_Indexer_Productmatch
{

    /** @var string */
    protected $_serviceTable;

    /** @var array */
    protected $_serviceMatches = array();

    /**
     * Initiate indexer
     */
    public function __construct()
    {
        parent::__construct();

        if (!@gc_enabled()) {
            @gc_enable();
        }

        $this->_table = (string)Mage::getResourceSingleton('productmatchrule/matchRule')->getMainTable();
        $this->_serviceTable = (string)Mage::getResourceSingleton('dyna_productmatchrule/service')->getMainTable();
        $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        /** convert memory limit to bytes */
        $value = trim(trim(ini_get('memory_limit')));
        if ($value == -1) {
            $value = $this->_getMachineMaxMemory();
        } else {
            $unit = strtolower(substr($value, -1, 1));
            $value = (int)$value;
            switch ($unit) {
                case 'g':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'm':
                    $value *= 1024;
                // no break (cumulative multiplier)
                case 'k':
                    $value *= 1024;
                default:
            }
        }
        //40% less than limit
        $this->_flushOnMemory = $value - (0.4 * $value);
        unset($value);
        unset($unit);

        $data = $this->_connection->fetchAll('SHOW VARIABLES;');
        foreach ($data as &$var) {
            if ($var['Variable_name'] == 'max_allowed_packet') {
                $limit = (int)$var['Value'];
                //20% less then limit
                $this->_maxPacketsLength = $limit - (0.20 * $limit);
                break;
            }
        }
        unset($var);
        unset($data);
        unset($limit);

        $this->_init('dyna_productmatchrule/indexer_productmatch');
    }

    /**
     * Empty index and generate whitelist based on rules
     */
    public function reindexAll()
    {
        $this->_connection->query(sprintf('TRUNCATE TABLE `%s`;', $this->_table));
        $this->_connection->query(sprintf('TRUNCATE TABLE `%s`;', $this->_serviceTable));
        $this->_reindexAll();
    }

    /**
     * Executed before processing the collection
     */
    public function start()
    {
        parent::start();
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {
        $websiteIds = implode(",", $rule->getWebsiteIds());

        switch ($rule['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                switch ($rule['operation']) {
                    case Dyna_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            $this->_addConditional($rule['left_id'], $rule['right_id'], $websiteId);
                        }
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            $this->_removeConditional($rule['left_id'], $rule['right_id'], $websiteId);
                        }
                        break;
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                switch ($rule['operation']) {
                    case Dyna_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                                foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                                    $this->_addConditional($rule['left_id'], $catProdId, $websiteId);
                                }
                                unset($catProdId);
                            }
                        }
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            if (isset($this->_productCategories[$websiteId][$rule['right_id']])) {
                                foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                                    $this->_removeConditional($rule['left_id'], $catProdId, $websiteId);
                                }
                                unset($catProdId);
                            }
                        }
                        break;
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                switch ($rule['operation']) {
                    case Dyna_ProductMatchRule_Model_Rule::OP_ALLOWED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED:
                    case Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                                foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                                    $this->_addConditional($rule['right_id'], $catProdId, $websiteId);
                                }
                                unset($catProdId);
                            }
                        }
                        break;
                    case Dyna_ProductMatchRule_Model_Rule::OP_NOTALLOWED:
                        foreach (explode(",", $websiteIds) as $websiteId) {
                            if (isset($this->_productCategories[$websiteId][$rule['left_id']])) {
                                foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                                    $this->_removeConditional($rule['right_id'], $catProdId, $websiteId);
                                }
                                unset($catProdId);
                            }
                        }
                        break;
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->handleConditionals($rule, $websiteId, $leftProdId, $rightProdId);
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                // Make sure that it is a service type rule and the product exists
                if ($rule['source_type'] == 0
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }

                $sourceHash = md5($rule['service_source_expression']);
                $rightId = $rule['right_id'];
                $priority = $rule['priority'];
                $operation = $rule['operation'];
                $operationValue = $rule['operation_value'];

                foreach (explode(",", $websiteIds) as $websiteId) {
                    $this->addServiceConditional(
                        $sourceHash,
                        $rightId,
                        $websiteId,
                        $priority,
                        $operation,
                        $operationValue
                    );
                }
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                // Make sure that it is a service type rule and the category exists
                $checkOk = true;
                foreach (explode(",", $websiteIds) as $websiteId) {
                    if ($rule['source_type'] == 0
                        || !isset($this->_productCategories[$websiteId][$rule['right_id']])
                    ) {
                        $checkOk = false;
                    }
                }

                if (!$checkOk) {
                    break;
                }

                foreach (explode(",", $websiteIds) as $websiteId) {
                    foreach ($this->_productCategories[$websiteId][$rule['right_id']] as &$catProdId) {
                        $sourceHash = md5($rule['service_source_expression']);
                        $priority = $rule['priority'];
                        $operation = $rule['operation'];
                        $operationValue = $rule['operation_value'];

                        $this->addServiceConditional(
                            $sourceHash,
                            $catProdId,
                            $websiteId,
                            $priority,
                            $operation,
                            $operationValue
                        );
                    }
                    unset($catProdId);
                }
                break;
        }
    }

    /**
     * Groups gathered items together by statement (INSERT/DELETE)
     * to decrease the number of statements executed on the database
     */
    protected function _applyChanges()
    {
        $this->_matches = array_reverse($this->_matches);
        while (($_row = array_pop($this->_matches)) !== null) {
            if (!count($this->_prev) || $_row[0] === $this->_prev[0][0]) {
                $this->_prev[] = $_row;
            } elseif ($this->_prev[0][0] === self::DELETE_ACTION) {
                $this->_removeMultiple();
                $this->_prev = array($_row);
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
                $this->_prev = array($_row);
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::DELETE_ACTION) {
                $this->_removeMultiple();
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultiple();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_matches = [];
        $this->_prev = [];


        $this->applyServiceChanges();
    }

    /**
     * Used to group and execute batches of insert statements
     *  to decrease the number of statements executed on the database
     */
    protected function applyServiceChanges()
    {
        $this->_serviceMatches = array_reverse($this->_serviceMatches);
        while (($_row = array_pop($this->_serviceMatches)) !== null) {
            if (!count($this->_prev) || $_row[0] === $this->_prev[0][0]) {
                $this->_prev[] = $_row;
            } elseif ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultipleService();
                $this->_prev = array($_row);
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        unset($_row);

        /**
         * If something remains unprocessed in the _prev array
         */
        if (count($this->_prev)) {
            if ($this->_prev[0][0] === self::ADD_ACTION) {
                $this->_insertMultipleService();
            } else {
                //should not happen
                throw new LogicException('Invalid action type for row');
            }
        }
        $this->_serviceMatches = [];
        $this->_prev = [];
    }

    /**
     * Builds INSERT statements and executes them
     * Iterates over the items withing the $_prev array
     * and builds the INSERT statements, always checking
     * if we approach the MySQL max_allowed_packet limit.
     * If we approach the limit too much, we execute the current
     * SQL statement and start building the statements for the remaining items
     */
    protected function _insertMultipleService()
    {
        $values = '';

        $add = array();
        $this->_prev = array_reverse($this->_prev);
        while (($_row = array_pop($this->_prev)) !== null) {
            $add[$_row[1][0]][] = array($_row[1][1], $_row[1][2], $_row[1][3], $_row[1][4], $_row[1][5]);
        }
        unset($_row);
        foreach ($add as $websiteId => &$combinations) {
            foreach ($combinations as $key => &$combination) {
                $values .= sprintf('("%s","%s","%s","%s","%s","%s"),', $websiteId, $combination[0], $combination[1], $combination[2], $combination[3], $combination[4]);

                if (strlen($values) >= $this->_maxPacketsLength) {
                    $sql = sprintf(
                        'INSERT INTO `%s` (`website_id`,`service_hash`,`target_product_id`,`priority`,`operation`,`operation_value`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`), `priority`=VALUES(`priority`), `operation`=VALUES(`operation`), `operation_value`=VALUES(`operation_value`);' . PHP_EOL,
                        $this->_serviceTable,
                        trim($values, ',')
                    );
                    $this->_connection->query($sql);
                    unset($sql);
                    $values = '';
                }
                unset($combinations[$key]);
            }
            unset($combination);
            unset($add[$websiteId]);
        }
        unset($combinations);
        unset($add);

        if ($values) {
            $sql = sprintf(
                'INSERT INTO `%s` (`website_id`,`service_hash`,`target_product_id`,`priority`,`operation`,`operation_value`) VALUES %s ON DUPLICATE KEY 
                        UPDATE `website_id`=VALUES(`website_id`), `priority`=VALUES(`priority`), `operation`=VALUES(`operation`), `operation_value`=VALUES(`operation_value`);' . PHP_EOL,
                $this->_serviceTable,
                trim($values, ',')
            );
            unset($values);
            $this->_connection->query($sql);
            unset($sql);
        }
    }

    /**
     * Method used to populate product_match_service_index table for service based rules
     * @param $left
     * @param $right
     * @param $websiteId
     */
    protected function addServiceConditional($left, $right, $websiteId, $priority, $operation, $operationValue)
    {
        if ($left != $right) {
            $this->_serviceMatches[] = array(1, array($websiteId, $left, $right, $priority, $operation, $operationValue));
            $this->_assertMemory();
        }
    }
}
