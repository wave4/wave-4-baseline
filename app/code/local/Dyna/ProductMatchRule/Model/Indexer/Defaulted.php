<?php

class Dyna_ProductMatchRule_Model_Indexer_Defaulted extends Omnius_ProductMatchRule_Model_Indexer_Defaulted
{
    /**
     * Reindex all
     */
    protected function _reindexAll()
    {
        /** @var Omnius_ProductMatchRule_Model_Resource_Rule_Collection $collection */
        $collection = Mage::getResourceModel('productmatchrule/rule_collection')
            ->addFieldToFilter('operation',
                array(
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED),
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED),
                    array('eq' => Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED)
                )
            );
        $collection->setPageSize(self::BATCH_SIZE);

        $currentPage = 1;
        $pages = $collection->getLastPageNumber();
        $this->start();

        /**
         * Process the rules within batches to remove
         * the risk using all the allocated memory
         * only to load the rule collections
         * This way, we never load the whole
         * collection into the current memory
         */
        do {
            $collection->setCurPage($currentPage);
            $collection->load();
            foreach ($collection as $rule) {
                $this->processItem($rule);
            }
            $currentPage++;
            $collection->clear();
        } while ($currentPage <= $pages);

        $this->end();
    }

    /**
     * @param $rule Dyna_ProductMatchRule_Model_Rule
     */
    protected function processItem($rule)
    {

        $websiteIds = $rule->getWebsiteIds();

        switch ($rule['operation_type']) {
            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allProductIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeP2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (!in_array($rule['right_id'], $this->_allProductIds)
                    || !in_array($rule['left_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeC2P($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (!in_array($rule['left_id'], $this->_allProductIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeP2C($rule, $websiteId);
                }
                break;

            case Omnius_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (!in_array($rule['left_id'], $this->_allCategoryIds)
                    || !in_array($rule['right_id'], $this->_allCategoryIds)
                ) {
                    break;
                }
                foreach ($websiteIds as $websiteId) {
                    $this->addConditionalCategoryOpTypeC2C($rule, $websiteId);
                }
                break;

            default:
                break;
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeP2P($rule, $websiteId)
    {
        if ($rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED
            || $rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED
        ) {
            $this->_addConditional($rule['left_id'], $rule['right_id'], $websiteId);
        }
    }

    /**
     * @param $rule
     * @param $websiteId
     */
    protected function addConditionalCategoryOpTypeC2P($rule, $websiteId)
    {
        if (($rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED
            || $rule['operation'] == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED_OBLIGATED)
            && isset($this->_productCategories[$websiteId][$rule['left_id']])
        ) {
            foreach ($this->_productCategories[$websiteId][$rule['left_id']] as &$catProdId) {
                $this->_addConditional($catProdId, $rule['right_id'], $websiteId);
            }
            unset($catProdId);
        }
    }
}
