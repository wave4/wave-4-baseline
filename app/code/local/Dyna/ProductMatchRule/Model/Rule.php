<?php
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class Dyna_ProductMatchRule_Model_Rule
 */
class Dyna_ProductMatchRule_Model_Rule extends Mage_CatalogRule_Model_Rule
{
    const OP_TYPE_P2P = 1;
    const OP_TYPE_P2C = 2;
    const OP_TYPE_C2C = 3;
    const OP_TYPE_C2P = 4;
    const OP_TYPE_S2P = 5;
    const OP_TYPE_S2C = 6;
    const OP_TYPE_M2P = 7;
    const OP_TYPE_M2C = 8;

    const OP_NOTALLOWED = 0;
    const OP_ALLOWED = 1;
    const OP_DEFAULTED = 2;
    const OP_OBLIGATED = 3;
    const OP_DEFAULTED_OBLIGATED = 4;
    const OP_MIN_N = 5;
    const OP_MAX_N = 6;
    const OP_EQL_N = 7;
    const OP_REMOVAL_ALLOWED = 8;
    const OP_REMOVAL_NOTALLOWED = 9;

    const ORIGIN_USER = 0;
    const ORIGIN_IMPORT = 1;

    // added to remove Dyna_Sandbox_Model_Sandbox dependency
    const UNIQUE_ID_REFERENCE = 'unique_id_replication_reference';

    protected $_maxPriority = 0;

    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    protected function _construct()
    {
        $this->_init('productmatchrule/rule');
    }

    /**
     * Fetches the highest priority from the product_match_rule table.
     * @return int
     */
    public function getMaxPriority()
    {
        if (!$this->_maxPriority) {
            $query = $this->getCollection()->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('MAX(priority) as max');
            $max = $query->query()->fetchAll();
            if (is_array($max) && count($max) > 0) {
                return $this->_maxPriority = (int) $max[0]['max'];
            } else {
                return $this->_maxPriority = 0;
            }
        }

        return $this->_maxPriority + 1;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }

        return $this->_cache;
    }

    /**
     * Gets a list of categories and associated products
     * @param int|null $websiteId
     * @return array
     */
    public static function getAllCategoryProducts($websiteId)
    {
        $productCategories = array();
        $categories = Mage::getModel('catalog/category')->getCollection();
        foreach ($categories as $category) {
            $categoryProductIds = Mage::getResourceModel('catalog/product_collection')
                ->addWebsiteFilter(array($websiteId))
                ->addCategoryFilter($category)
                ->getAllIds();
            $productCategories[$category->getId()] = $categoryProductIds;
        }

        return $productCategories;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if (!$this->getData('unique_id')) {
            $this->setData('unique_id', hash('sha256', (time() . uniqid(rand(0, 1000)) . __FILE__)));
        }

        return parent::_beforeSave();
    }

    /**
     * Processing object after save data
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        /**
         * Do not clean cache (heavy I/O) after each save
         */
        return $this;
    }

    /**
     *
     * @param int $id
     * @return Mage_Core_Model_Abstract
     */
    public function loadAll($id = null)
    {
        $id = $id ?: $this->getProductMatchRuleId();
        $model = $this->load($id);
        $ruleCollection = $this->getCollection()
            ->addFieldToFilter('operation_type', $model->getData('operation_type'))
            ->addFieldToFilter('right_id', $model->getData('right_id'))
            ->addFieldToFilter('operation', $model->getData('operation'))
            ->addFieldToFilter('priority', $model->getData('priority'));

        if ($model->getSourceType()) {
            // Service type expression
            $ruleCollection->addFieldToFilter('source_type', $model->getData('source_type'))
                ->addFieldToFilter('source_collection', $model->getData('source_collection'))
                ->addFieldToFilter('service_source_expression', $model->getData('service_source_expression'));
        } else {
            // Product selection
            $ruleCollection->addFieldToFilter('left_id', $model->getData('left_id'));
        }

        $ruleCollection->load();

        return $ruleCollection;
    }

    /**
     * @return int
     */
    public function getRuleOrigin()
    {
        return (int)parent::getRuleOrigin();
    }

    /**
     * Get all service expression applicable rules
     * @param array $inputParams
     * @return array
     * @todo Create cache mechanism
     */
    public function loadServiceRules($inputParams)
    {
        return array();//@todo - remove after implementing all service expressions
        $language = new ExpressionLanguage();

        $ruleCollection = $this->getCollection()
            ->addFieldToFilter('source_type', 1)
            ->addFieldToFilter('service_source_expression', array('notnull' => true))
            ->load();

        $activeRules = [];
        foreach ($ruleCollection as $rule) {
            $ssExpression = $rule->getServiceSourceExpression();
            // Evaluate each rule
            $passed = $language->evaluate(
                $ssExpression,
                $inputParams
            );

            if ($passed) {
                $ruleHash = md5($ssExpression);
                if (!in_array($ruleHash, $activeRules)) {
                    $activeRules[] = (string)$ruleHash;
                }
            }
        }

        return $activeRules;
    }

    /**
     * @param $hashes
     * @param $websiteId
     * @return array
     * @todo Create cache mechanism
     */
    public function getIndexedServiceRules($hashes, $websiteId)
    {
        $products = [];
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = 'product_match_service_index';

        if (count($hashes)) {
            $inCondition = "'" . implode("','", $hashes) . "'";

            $sql = "SELECT DISTINCT(target_product_id) 
                FROM " . $tableName . " 
                WHERE service_hash IN ($inCondition) AND website_id = :id";

            $products = $connection->fetchAll($sql, ['id' => $websiteId], Zend_Db::FETCH_COLUMN);
        }

        return $products;
    }

    /**
     * @return array
     */
    public function getWebsiteIds()
    {
        /** @var Zend_Db_Adapter_Abstract $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = 'product_match_rule_website';

        $sql = "SELECT website_id FROM `$tableName` WHERE rule_id = :id;";

        return array_column($connection->fetchAll($sql, ['id' => $this->getId()], Zend_Db::FETCH_NUM), 0);
    }

    /**
     * @param array $websiteIds
     * @return Dyna_ProductMatchRule_Model_Rule
     */
    public function setWebsiteIds($websiteIds)
    {
        $currentIds = $this->getWebsiteIds();
        $remove = array_diff($currentIds, $websiteIds);

        $add = array_diff($websiteIds, $currentIds);

        if ($remove || $add) {
            /** @var Zend_Db_Adapter_Abstract $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $tableName = 'product_match_rule_website';

            if ($remove) {
                $removeIds = implode(',', $remove);
                $sql = "DELETE FROM `$tableName` WHERE rule_id = :id AND website_id IN (:list)";
                $connection->query($sql, ["id" => $this->getId(), "list" => $removeIds]);
            }
            if ($add) {
                foreach ($add as $websiteId) {
                    $sql = "INSERT INTO `$tableName` VALUES (:id, :website_id)";
                    $connection->query($sql, ["id" => $this->getId(), "website_id" => $websiteId]);
                }
            }
        }

        return $this;
    }
}
