<?php

class Dyna_ProductMatchRule_Model_Resource_Service extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('dyna_productmatchrule/service', 'entity_id');
    }
}
