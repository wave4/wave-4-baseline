<?php

/**
 * Class Data
 */
class Dyna_ProductMatchRule_Helper_Data extends Omnius_ProductMatchRule_Helper_Data
{
    protected $_websites = [];
    protected $_ruleFromImpoter = false;
    const SOURCE_TYPE_PRODUCT_SELECTION = 'Product selection';
    const SOURCE_TYPE_SERVICE = 'Service';


    /** @var Dyna_Cache_Model_Cache */
    protected $_cache;

    public function getOperationTypes()
    {
        return array(
            1 => 'PRODUCT-PRODUCT',
            2 => 'PRODUCT-CATEGORY',
            3 => 'CATEGORY-CATEGORY',
            4 => 'CATEGORY-PRODUCT',
            5 => 'SERVICE-PRODUCT',
            6 => 'SERVICE-CATEGORY',
            7 => 'MIX-PRODUCT',
            8 => 'MIX-CATEGORY'
        );
    }

    public function getOperations()
    {
        return array(
            0 => 'Not Allowed',
            1 => 'Allowed',
            2 => 'Defaulted',
            3 => 'Obligated',
            4 => 'Defaulted-Obligated',
            5 => 'Min(n)',
            6 => 'Max(n)',
            7 => 'Eql(n)',
            8 => 'Removal Not Allowed',
        );
    }

    public function getCategoryInRightOperations()
    {
        $operationTypes = $this->getOperationTypes();

        return [
            array_search("PRODUCT-CATEGORY", $operationTypes),
            array_search("CATEGORY-CATEGORY", $operationTypes),
            array_search("SERVICE-CATEGORY", $operationTypes),
            array_search("MIX-CATEGORY", $operationTypes),
        ];
    }

    /**
     * Returns only the operations ids with values
     *
     * @access public
     * @return array
     */
    public function getOperationsWithValues()
    {
        return ['5', '6', '7'];
    }

    public function getSourceTypes()
    {
        return [
            0 => self::SOURCE_TYPE_PRODUCT_SELECTION,
            1 => self::SOURCE_TYPE_SERVICE,
        ];
    }


    public function getSourceCollections()
    {
        return [
            0 => '',
            1 => 'Current quote',
            2 => 'Installed base',
            3 => 'Quote selection delta',
        ];
    }

    public function getLifecycles()
    {
        return [
            0 => '*',
            1 => 'Acquisition',
            2 => 'Retention',
            3 => 'Inlife'
        ];
    }

    public function getRuleOrigin()
    {
        return [
            0 => 'Manual',
            1 => 'Import'
        ];
    }

    /**
     * @param $productIds
     * @param null $websiteId
     * @return array
     */
    public function getDefaultRules($productIds, $websiteId = null)
    {
        if ($websiteId == null) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }
        /** @var Dyna_ProductMatchRule_Model_Defaulted $defaultedModel */
        $defaultedModel = Mage::getModel('productmatchrule/defaulted');
        $result = [];
        foreach ($productIds as $prod) {
            $result[$prod] = $defaultedModel->getDefaultedProductsFor($prod, $websiteId);
        }

        return $result;
    }

    /**
     * @param array $data
     * @param int $baseId
     * @param bool $ruleFromImporter
     * @return array
     */
    public function setRuleData($data, $baseId = null, $ruleFromImporter = false)
    {
        $this->_ruleFromImpoter = $ruleFromImporter;
        $models = [];

        /**
         * @var Dyna_ProductMatchRule_Model_Rule $productMatchRuleModel
         * @var Dyna_ProductMatchRule_Model_Rule $oldModel
         * @var Dyna_ProductMatchRule_Model_Rule $model
         */
        $productMatchRuleModel = Mage::getModel('productmatchrule/rule');
        $model = $productMatchRuleModel;

        if ($baseId) {
            $oldModel = $productMatchRuleModel->load($baseId);
            if ($oldModel->getId()) {
                $model = $oldModel;
            }
        }
        $websitesIds = $data['website_id'];

        if (!empty($data['service_source_expression'])) {
            $data['service_source_expression'] = preg_replace('/\s+/', ' ', $data['service_source_expression']);
            $model->setServiceSourceExpression($data['service_source_expression']);
        }

        switch ($data['operation_type']) {
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                if (is_string($response = $this->validateRuleDataForOpTypeP2P($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                if (is_string($response = $this->validateRuleDataForOpTypeP2C($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                if (is_string($response = $this->validateRuleDataForOpTypeC2P($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                if (is_string($response = $this->validateRuleDataForOpTypeC2C($data, $websitesIds))) {
                    return $response;
                }
                /** Preserve left_id from condition */
                $data['left_id'] = $this->getLeftId($data);
                break;
            default:
                break;
        }

        unset($data['website_id']);
        $model->loadPost($data);
        //$model->setWebsiteId($websiteId);
        //$model->addData($data)->setWebsiteId($websiteId);
        $models[] = $model;

        return $models;
    }

    /**
     * Return left id from conditions ready to be saved on product match rule model
     * @param $data
     */
    protected function getLeftId($data)
    {
        $left_id = null;
        if (!empty($conditions = $data['conditions'])) {
            foreach ($conditions as $currentCondition) {
                switch ($currentCondition['type']) {
                    case "catalogrule/rule_condition_product" :
                        if (!empty($currentCondition['attribute']) && $currentCondition['attribute'] == 'sku') {
                            $left_id = Mage::getModel('catalog/product')
                                ->getResource()
                                ->getIdBySku($currentCondition['value']);
                        } elseif (!empty($currentCondition['attribute']) && $currentCondition['attribute'] == 'category_ids') {
                            $left_id = (int)($currentCondition['value']);
                        }
                        break;
                    default :
                    break;
                }
            }
        }

        return $left_id;
    }

    protected function validateRuleDataForOpTypeP2P($data, $websitesIds)
    {
        $firstProduct = Mage::getModel('catalog/product')->load($data['left_id']);
        $secondProduct = Mage::getModel('catalog/product')->load($data['right_id']);
        if (is_string($response = $this->validateObligatedOperationForOpTypesC2PAndP2P($data['operation']))) {
            return $response;
        }
        /**
         * @var Mage_Catalog_Model_Product $firstProduct
         * @var Mage_Catalog_Model_Product $secondProduct
         */
        $this->validate2ProductsBelongToSameWebsites($firstProduct, $secondProduct);
        $this->validateProductBelongsToWebsite($websitesIds, $firstProduct);
        $this->validateProductBelongsToWebsite($websitesIds, $secondProduct);
        //todo - clarify if this is still needed
//                if (is_string($response = $this->validateSecondProductOnOpTypeP2PAndC2P($data['operation'], $secondProduct))) {
//                    return $response;
//                }
        return true;
    }
    protected function validateRuleDataForOpTypeP2C($data, $websitesIds)
    {
        $product = Mage::getModel('catalog/product')->load($data['left_id']);
        if (is_string($response = $this->validateDefaultedOperationForOpTypesC2CAndP2C($data['operation']))) {
            return $response;
        }
        $this->validateProductBelongsToWebsite($websitesIds, $product);
        $this->validateCategoryProductsBelongToWebsite($data, 'right_id', $websitesIds);
        return true;
    }
    protected function validateRuleDataForOpTypeC2P($data, $websitesIds)
    {
        $product = Mage::getModel('catalog/product')->load($data['right_id']);
        $this->validateProductBelongsToWebsite($websitesIds, $product);
        //todo - clarify if this is still needed
//            if (is_string($response = $this->validateSecondProductOnOpTypeP2PAndC2P($data['operation'], $product))) {
//                return $response;
//            }
        if (is_string($response = $this->validateObligatedOperationForOpTypesC2PAndP2P($data['operation']))) {
            return $response;
        }
        $this->validateCategoryProductsBelongToWebsite($data, 'left_id', $websitesIds);

        return true;
    }

    protected function validateRuleDataForOpTypeC2C($data, $websitesIds)
    {
        if (is_string($response = $this->validateDefaultedOperationForOpTypesC2CAndP2C($data['operation']))) {
            return $response;
        }
        $this->validateCategoryProductsBelongToWebsite($data, 'left_id', $websitesIds);
        $this->validateCategoryProductsBelongToWebsite($data, 'right_id', $websitesIds);

        return true;
    }

    protected function validateObligatedOperationForOpTypesC2PAndP2P($operation)
    {
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_OBLIGATED) {
            return "Invalid operation: A product can't be set as mandatory.";
        }
        return true;
    }

    protected function validateDefaultedOperationForOpTypesC2CAndP2C($operation)
    {
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED) {
            return "Invalid operation: A category can't be set as defaulted.";
        }
        return true;
    }

    protected function validateSecondProductOnOpTypeP2PAndC2P($operation, $product)
    {
        // Only allow addons as the defaulted product
        if ($operation == Dyna_ProductMatchRule_Model_Rule::OP_DEFAULTED && !$product->is(Dyna_Catalog_Model_Type::SUBTYPE_ADDON)
        ) {
            return "The target product must be an Addon";
        }

        return true;
    }

    protected function validate2ProductsBelongToSameWebsites($firstProduct, $secondProduct)
    {
        $intersected = array_intersect($firstProduct->getWebsiteIds(), $secondProduct->getWebsiteIds());
        if (empty($intersected)) {
            $this->handleWarning('The products must belong at least to one common website.');
        }
    }


    protected function validateProductBelongsToWebsite($websitesIds, $product)
    {
        foreach ($websitesIds as $websiteId) {
            if (!in_array($websiteId, $product->getWebsiteIds())) {
                $this->handleWarning(sprintf('The website %s must include this product sku: %s', $this->getWebsiteById($websiteId), $product->getSku()));
            }
        }
    }

    protected function validateCategoryProductsBelongToWebsite($data, $dataKey, $websitesIds)
    {
        $categoryProductsIds = [];
        $productCount = [];

        //Checking if category product ids have been loaded
        if (!isset($productCount[$data[$dataKey]])) {
            //Loading category's products ids
            $categoryProductsIds = Mage::getModel('catalog/category')->getProductIdsAndSkus($data[$dataKey]);
            $productCount[$data[$dataKey]] = count($categoryProductsIds);
            if (!$productCount[$data[$dataKey]]) {
                /** @var Dyna_Catalog_Model_Category $category */
                $category = Mage::getModel('catalog/category')->load($data[$dataKey]);
                $this->handleWarning(sprintf('Category %s does not have products.', $category->getName()));
            }
        }
        //Continuing checking for each product visibility in focused store if category has at least one
        if ($productCount[$data[$dataKey]]) {
            foreach ($websitesIds as $websiteId) {
                //Loading website's products ids
                $websiteProductIds = Mage::getModel('catalog/category')->getWebsitesProductsId($websiteId);

                foreach ($categoryProductsIds as $productId => $sku) {
                    if (!isset($websiteProductIds[$productId])) {
                        //This product from focused category doesn't belong to this website
                        $this->handleWarning(sprintf('The website %s must include this product sku: %s', $this->getWebsiteById($websiteId), $sku));
                    }
                }
            }
        }
    }

    /**
     * Get the website name by Id
     *
     * @param $websiteId
     * @return string
     */
    public function getWebsiteById($websiteId)
    {
        if (!isset($this->_websites[$websiteId])) {
            $this->_websites[$websiteId] = Mage::app()->getWebsite($websiteId)->getName();
        }

        return $this->_websites[$websiteId];
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product name.
     * @return array
     */
    public function getProductsForDropdown()
    {
        $key = md5(__CLASS__ . 'products_for_dropdown');
        $options = unserialize($this->getCache()->load($key));
        if (empty($options)) {
            $products = Mage::helper('omnius_catalog')->getProductCollection();
            $options = array(-1 => 'Please select a product..');
            foreach ($products as $productId => $product) {
                $options[] = array(
                    'label' => $product["type_id"] == "configurable" ? sprintf("%s (configurable)", $product['name']) : $product["name"],
                    'value' => $productId
                );
            }
            unset($products);

            $this->getCache()->save(
                serialize($options),
                $key,
                [Dyna_Cache_Model_Cache::CACHE_TAG],
                $this->getCache()->getTtl()
            );
        }

        return $options;
    }

    /**
     * Fetches all simple products as an associative array where the
     * key is the product id and the value is the product sku.
     * @param bool $forJs
     * @return array
     */
    public function getProductSkusForDropdown($forJs = false)
    {
        $key = md5(__CLASS__ . 'products_for_dropdown_sku' . (int)$forJs);
        $options = unserialize($this->getCache()->load($key));

        if (empty($options)) {
            $products = Mage::helper('omnius_catalog')->getProductCollection();
            $options = [];
            $counter = 0;
            foreach ($products as $productId => $product) {
                $index = $forJs ? $counter : $productId;
                $options[$index] = array(
                    'value' => $product['sku'],
                    'data' => $productId
                );
                ++$counter;
            }
            unset($products);

            $this->getCache()
                ->save(
                    serialize($options),
                    $key,
                    [Dyna_Cache_Model_Cache::CACHE_TAG],
                    $this->getCache()->getTtl()
                );
        }

        return $options;
    }

    /**
     * Fetches all the categories as an associative array where the
     * key is the category id and the value is the category name.
     * @return array
     */
    public function getCategoriesForDropdown()
    {
        $categoriesCollection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name');
        $selectable_collection = array(-1 => 'Please select a category...');
        foreach ($categoriesCollection as $category) {
            $selectable_collection[$category->getId()] = $category->getName();
        }

        return $selectable_collection;
    }

    /**
     * @return Dyna_Cache_Model_Cache
     */
    protected function getCache()
    {
        if (!$this->_cache) {
            $this->_cache = Mage::getSingleton('dyna_cache/cache');
        }
        return $this->_cache;
    }


    public function getRulesForId($websiteId, $id, $catalogResource, $storeId, &$rawAttributesValues, $attribute, $deviceCollection, $subsCollection)
    {
        $rulesSource = $this->getRuleById($id, $websiteId);

        /**
         * When choosing a device or subscription, normally the application would hide the rest of the devices or subscriptions
         * if there is no allowed rule between them. However, for devices and subscriptions, we must allow users to switch
         * between the allowed ones without hiding the other options.
         */
        $valueId = $catalogResource->getAttributeRawValue($id, Dyna_Catalog_Model_Product::CATALOG_PACKAGE_SUBTYPE_ATTR, $storeId);
        if (isset($rawAttributesValues[$valueId])) {
            $productType = $rawAttributesValues[$valueId];
        } else {
            $productType = json_decode(Mage::helper('omnius_catalog')->getAttributeAdminLabel($attribute, $valueId), true);
            $rawAttributesValues[$valueId] = $productType;
        }

        $similarProductIds = array();

        //if device, also retrieve all other devices to simulate that subscriptions are allowed between themselves
        if (in_array(Dyna_Catalog_Model_Type::SUBTYPE_DEVICE, $productType)) {
            $similarProductIds = $deviceCollection;
        }

        //if subscription, also retrieve all other subscriptions to simulate that subscriptions are allowed between themselves
        if (in_array(Dyna_Catalog_Model_Type::SUBTYPE_SUBSCRIPTION, $productType)) {
            $similarProductIds = array_merge($similarProductIds, $subsCollection);
        }

        $targetProductIds = [];
        $sourceProductIds = [];
        foreach ($rulesSource as $row) {
            $targetProductIds[$row['target_product_id']] = $row['target_product_id'];
            $sourceProductIds[$row['source_product_id']] = $row['source_product_id'];
        }

        $available = array_unique(
            array_merge(
                $similarProductIds,
                $targetProductIds,
                $sourceProductIds
            )
        );

        return $available;
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function getRuleById($id, $websiteId = null)
    {
        $websiteId = $websiteId ?: Mage::app()->getStore()->getWebsiteId();
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $key = sprintf('%s_product_mix_match_white_rules_%s', $id, $websiteId);
        if ($result = $this->getCache()->load($key)) {
            return unserialize($result);
        } else {
            $sql = Mage::getResourceModel('productmatchrule/matchRule_collection')
                ->addFieldToSelect(array('source_product_id', 'target_product_id'))
                ->addFieldToFilter('website_id', $websiteId)
                ->addFieldToFilter(
                    array('source_product_id', 'target_product_id'),
                    array(
                        array('eq' => $id),
                        array('eq' => $id)
                    )
                )->getSelectSql(true);
            $result = $adapter->fetchAll($sql);

            $this->getCache()->save(serialize($result), $key, array(Dyna_Cache_Model_Cache::CACHE_TAG), $this->getCache()->getTtl());

            return $result;
        }
    }

    /**
     * Handle a warning display depending if the rule is imported from cli or if it is added manually
     */
    protected function handleWarning($message)
    {
        if ($this->_ruleFromImpoter) {
            $this->_importerLog($message);
        } else {
            Mage::getSingleton('adminhtml/session')->addWarning($message);
        }
    }

    /**
     * Write the errors/warnings to the a log for an imported rule
     * @param $msg
     */
    protected function _importerLog($msg)
    {
        Mage::log($msg, null, 'matchrules_import_' . date('Y-m-d') . '.log');
    }
}
