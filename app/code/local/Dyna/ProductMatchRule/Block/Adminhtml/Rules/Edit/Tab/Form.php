<?php

class Dyna_ProductMatchRule_Block_Adminhtml_Rules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $productsCollection = Mage::helper('productmatchrule')->getProductsForDropdown();
        $skusCollection = Mage::helper('productmatchrule')->getProductSkusForDropdown();
        $skusCollectionJs = Mage::helper('productmatchrule')->getProductSkusForDropdown(true);
        $categoriesCollection = Mage::helper('productmatchrule')->getCategoriesForDropdown();
        /** @var Dyna_ProductMatchRule_Model_Rule $data */
        $data = Mage::registry('rules_data');

        $form->setHtmlIdPrefix('rule_');
        $id = $data->getProductMatchRuleId();
        $edit = isset($id) && !empty($id);
        $this->buildSource($form, $data);
        //$this->buildLeftSide($data, $edit, $form, $productsCollection, $skusCollectionJs, $skusCollection, $categoriesCollection);
        $this->buildSourceConditions($form, $data);
        $this->buildRightSide($data, $edit, $form, $productsCollection, $skusCollectionJs, $skusCollection, $categoriesCollection);
        $this->buildOptions($form, $data);
        $this->buildLock($form, $data);

        return parent::_prepareForm();
    }


    /**
     * @param $form
     * @param $data
     */
    protected function buildSourceConditions($form, $data)
    {
        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('adminhtml/promo_quote/newConditionHtml/form/rule_conditions_fieldset'));

        $fieldset = $form->addFieldset('conditions_fieldset', array(
            'legend'=>Mage::helper('salesrule')->__('Source conditions')
        ))->setRenderer($renderer);

        $fieldset->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('salesrule')->__('Conditions'),
            'title' => Mage::helper('salesrule')->__('Conditions'),
        ))->setRule($data)->setRenderer(Mage::getBlockSingleton('rule/conditions'));
    }

    /**
     * @param $form Varien_Data_Form
     * @param $data
     */
    protected function buildSource($form, $data)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');

        $options_fieldset = $form->addFieldset(
            'source_properties_rules_form',
            array('legend' => $helper->__('Source Properties'))
        );

        $options_fieldset->addField('source_type', 'select', array(
            'label' => $helper->__('Source type'),
            'name' => 'source_type',
            'values' => $helper->getSourceTypes(),
            'value' => $data->getSourceType()
        ));

        $options_fieldset->addField('service_source_expression', 'textarea', array(
            'label' => $helper->__('Service source expression'),
            'name' => 'service_source_expression',
            'value' => $data->getServiceSourceExpression()
        ));

        $options_fieldset->addField('source_collection', 'select', array(
            'label' => $helper->__('Source collection'),
            'name' => 'source_collection',
            'values' => $helper->getSourceCollections(),
            'value' => $data->getSourceCollection()
        ));

        $options_fieldset->addField('lifecycle', 'select', array(
            'label' => $helper->__('Lifecycle'),
            'name' => 'lifecycle',
            'values' => $helper->getLifecycles(),
            'value' => $data->getLifecycle()
        ));

        $operationTypes = $helper->getOperationTypes();
        $categoryRightOperations = implode(",", $helper->getCategoryInRightOperations());

        $options_fieldset->addField('operation_type', 'select', array(
            'label' => $helper->__('Operation type'),
            'name' => 'operation_type',
            'values' => $operationTypes,
            'value' => $data->getOperationType(),
            'after_element_html' =>
                "<script type='text/javascript'>
                jQuery(document).ready(function() {
                    var rightCategories = [" . $categoryRightOperations . "];
                    if (rightCategories.indexOf(parseInt(jQuery('#rule_operation_type').val())) != -1) {
                        jQuery('#rule_right_category_visible').removeAttr('disabled');                    
                    }
    
                    jQuery('#rule_operation_type').on('change', function(){
                        var selectedValue = parseInt(jQuery(this).val());
                        if (rightCategories.indexOf(selectedValue) != -1) {
                            jQuery('#rule_right_category_visible').removeAttr('disabled');
                        } else {
                            jQuery('#rule_right_category_visible').attr('disabled', 'disabled');
                        }
                    });                    
                });
                </script>",
        ));

    }

    /**
     * @param $data
     * @param $edit
     * @param $form
     * @param $productsCollection
     * @param $skusCollectionJs
     * @param $skusCollection
     * @param $categoriesCollection
     */
    protected function buildLeftSide($data, $edit, $form, $productsCollection, $skusCollectionJs, &$skusCollection, $categoriesCollection)
    {
        $left_fieldset = $form->addFieldset('left_rules_form', array('legend' => Mage::helper('productmatchrule')->__('Source')));

        $cond21NotEdit = !$edit || in_array($data->getOperationType(), [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
            ]);

        $cond34NotEdit = !$edit || in_array($data->getOperationType(), [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
            ]);

        $left_fieldset->addField('left_product', 'select', array(
            'label' => Mage::helper('productmatchrule')->__('Product'),
            'name' => 'left_id[product]',
            'disabled' => !$cond21NotEdit,
            'values' => $productsCollection,
            'class' => 'left_product',
            'value' => $cond21NotEdit ? $data->getLeftId() : '',
            'after_element_html' => sprintf('<script>var skusAutocomplete = %s; var skusDropdown = %s;</script>', Mage::helper('core')->jsonEncode($skusCollectionJs), Mage::helper('core')->jsonEncode($skusCollection)),
        ));

        $left_fieldset->addField('left_product_sku', 'text', array(
            'label' => Mage::helper('productmatchrule')->__('Product sku'),
            'name' => 'left_id[product_sku]',
            'class' => 'left_product_sku',
            'disabled' => !$cond21NotEdit,
            'value' => $cond21NotEdit ? (isset($skusCollection[$data->getLeftId()]) ? $skusCollection[$data->getLeftId()]['value'] : '') : '',
            'after_element_html' => sprintf('<script>jQuery("#left_product_sku").autocomplete({source: skusAutocomplete});</script>'),
        ));


        $left_fieldset->addField('left_category_visible', 'select', array(
            'label' => Mage::helper('productmatchrule')->__('Category'),
            'name' => 'left_id[category_visible]',
            'disabled' => true,
            'class' => 'left_category_visible',
            'values' => $categoriesCollection,
            'value' => $cond34NotEdit ? $data->getLeftId() : '',
            'after_element_html' => sprintf('<button type="button" id="left_clear_button" class="scalable cancel productmatch"%s onclick="javascript:clearLeftCat()"><span>&nbsp;</span></button>', $cond34NotEdit ? '' : ' disabled')
        ));

        $left_fieldset->addField('left_category', 'hidden', array(
            'name' => 'left_id[category]',
            'class' => 'left_category',
            'value' => $cond34NotEdit ? $data->getLeftId() : '',
        ));

        $left_fieldset->addField('open_left_category', 'button', array(
                'name' => 'open_left_category',
                'label' => false,
                'value' => $this->helper('productmatchrule')->__('Open category tree'),
                'disabled' => !$cond34NotEdit,
                'class' => $cond34NotEdit ? 'form-button open_left_category' : 'form-button disabled open_left_category',
                'onclick' => "javascript:openThePopup('left')"
            )
        );

        $left_fieldset->addField('open_left_category_products', 'button', array(
                'name' => 'open_left_category_products',
                'label' => false,
                'value' => $this->helper('productmatchrule')->__('Open Source Category'),
                'disabled' => !$cond34NotEdit,
                'class' => $cond34NotEdit ? 'form-button open_left_category_products' : 'form-button disabled open_left_category_products',
                'onclick' => "javascript:openCategoryProducts('left')"
            )
        );
    }

    /**
     * @param $data
     * @param $edit
     * @param $form
     * @param $productsCollection
     * @param $skusCollection
     * @param $categoriesCollection
     */
    protected function buildRightSide($data, $edit, $form, $productsCollection, $skusCollectionJs, $skusCollection, $categoriesCollection)
    {
        $cond14NotEdit = !$edit || in_array($data->getOperationType(), [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2P,
            ]);
        $cond23NotEdit = !$edit || in_array($data->getOperationType(), [
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C,
                Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2C,
            ]);
        $right_fieldset = $form->addFieldset('right_rules_form', array('legend' => Mage::helper('productmatchrule')->__('Target')));
        $right_fieldset->addField('right_product', 'select', array(
            'label' => Mage::helper('productmatchrule')->__('Product'),
            'name' => 'right_id[product]',
            'class' => 'right_product',
            'disabled' => !$cond14NotEdit,
            'values' => $productsCollection,
            'value' => $cond14NotEdit ? $data->getRightId() : '',
        ));
        $right_fieldset->addField('right_product_sku', 'text', array(
            'label' => Mage::helper('productmatchrule')->__('Product sku'),
            'name' => 'right_id[product_sku]',
            'disabled' => !$cond14NotEdit,
            'class' => 'right_product_sku',
            'value' => $cond14NotEdit ? (isset($skusCollection[$data->getRightId()]) ? $skusCollection[$data->getRightId()]['value'] : '') : '',
            //'after_element_html' => sprintf('<script></script>'),
            'after_element_html' => sprintf('<script>var skusAutocomplete = %s;</script>', Mage::helper('core')->jsonEncode($skusCollectionJs)),
        ));
        $right_fieldset->addField('right_category_visible', 'select', array(
            'label' => Mage::helper('productmatchrule')->__('Category'),
            'name' => 'right_id[category_visible]',
            'disabled' => true,
            'class' => 'right_category_visible',
            'values' => $categoriesCollection,
            'value' => $cond23NotEdit ? $data->getRightId() : '',
            'after_element_html' => sprintf('<button type="button" id="right_clear_button" class="scalable cancel productmatch"%s onclick="javascript:clearRightCat()"><span>&nbsp;</span></button>', $cond23NotEdit ? '' : ' disabled')
        ));
        $right_fieldset->addField('right_category', 'hidden', array(
            'name' => 'right_id[category]',
            'value' => $cond23NotEdit ? $data->getRightId() : '',
            'class' => 'right_category',
        ));

        $right_fieldset->addField('open_right_category', 'button', array(
                'name' => 'open_right_category',
                'label' => false,
                'value' => $this->helper('productmatchrule')->__('Open category tree'),
                'disabled' => !$cond23NotEdit,
                'class' => $cond23NotEdit ? 'form-button open_right_category' : 'form-button open_right_category disabled',
                'onclick' => "javascript:openThePopup('right')",
            )
        );

        $right_fieldset->addField('open_right_category_products', 'button', array(
                'name' => 'open_right_category_products',
                'label' => false,
                'value' => $this->helper('productmatchrule')->__('Open Target Category'),
                'disabled' => !$cond23NotEdit,
                'class' => $cond23NotEdit ? 'form-button open_right_category_products' : 'form-button disabled open_right_category_products',
                'onclick' => "javascript:openCategoryProducts('right')"
            )
        );
    }

    /**
     * @param $form
     * @param $data
     */
    protected function buildOptions($form, $data)
    {
        /** @var Dyna_ProductMatchRule_Helper_Data $helper */
        $helper = Mage::helper('productmatchrule');

        $options_fieldset = $form->addFieldset(
            'properties_rules_form',
            array('legend' => $helper->__('Rule Properties'))
        );

        $typeOptions = array('0' => $helper->__('Manual'), '1' => $helper->__('Import'));
        $options_fieldset->addField('rule_origin', 'label', array(
            'label' => $helper->__('Type'),
            'name' => 'operation',
            'value' => (isset($typeOptions[$data->getRuleOrigin()])) ? $typeOptions[$data->getRuleOrigin()] : ''
        ));

        /** RFC-150019 added title and description column to Dyna_ProductMatchRule Module */
        $maxTitleLength = 250;
        $maxDescriptionLength = 250;
        $options_fieldset->addField('rule_title', 'text', array(
            'label' => $helper->__('Rule title') . " (max: 250)",
            'name' => 'rule_title',
            'after_element_html' => "<script type='text/javascript'>Event.observe('rule_rule_title', 'blur', function() {if ($(this).value.length>" . $maxTitleLength . ") {alert('" . sprintf($this->__("Please enter less than %s characters"),
                    $maxTitleLength) . "'); $(this).value = $(this).value.substring(0, " . $maxTitleLength . "); }})</script>",
            'value' => $data->getRuleTitle(),
        ));

        $options_fieldset->addField('rule_description', 'textarea', array(
            'label' => $helper->__('Rule description') . " (max: 250)",
            'name' => 'rule_description',
            'after_element_html' => "<script type='text/javascript'>Event.observe('rule_rule_description', 'blur', function() {if ($(this).value.length>" . $maxDescriptionLength . ") {alert('" . sprintf($this->__("Please enter less than %s characters"),
                    $maxDescriptionLength) . "'); $(this).value = $(this).value.substring(0, " . $maxDescriptionLength . "); }})</script>",
            'value' => $data->getRuleDescription(),
        ));

        $options_fieldset->addField('operation', 'select', array(
            'label' => $helper->__('Operation'),
            'name' => 'operation',
            'values' => $helper->getOperations(),
            'value' => $data->getOperation()
        ));

        $options_fieldset->addField('operation_value', 'text', array(
            'label' => $helper->__('Operation Value'),
            'name' => 'operation_value',
            "class" => "required-entry validate-digits",
            "required" => true,
            'value' => $data->getOperationValue(),
        ));

        // Append dependency javascript
        $this->setChild('form_after', $this->getLayout()
            ->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap('rule_operation', 'operation')
            ->addFieldMap('rule_operation_value', 'operation_value')
            ->addFieldMap('rule_source_type', 'source_type')
            ->addFieldMap('rule_service_source_expression', 'service_source_expression')
            ->addFieldDependence('operation_value', 'operation', $helper->getOperationsWithValues())
            ->addFieldDependence('service_source_expression', 'source_type', ['1'])
        );

        $model = Mage::getModel('productmatchrule/rule');
        $max_priority = $model->getMaxPriority() != null ? '<p class="note">The highest priority now is ' . $model->getMaxPriority() . '.</p>' : '';
        $options_fieldset->addField('priority', 'text', array(
            'label' => $helper->__('Priority'),
            'name' => 'priority',
            'value' => $data->getPriority(),
            'after_element_html' => $max_priority
        ));

        $options_fieldset->addField('website_id', 'multiselect', array(
            'label' => $helper->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'name' => 'website_id',
            "class" => "required-entry",
            "required" => true,
            'value' => $data->getWebsiteIds()
        ));
    }

    /**
     * @param $form
     * @param $data
     */
    protected function buildLock($form, $data)
    {
        $lock_fieldset = $form->addFieldset('lock_form', array('legend' => Mage::helper('productmatchrule')->__('Lock state')));

        $lock_fieldset->addField('locked', 'select', array(
            'label' => Mage::helper('productmatchrule')->__('Locked'),
            'values' => array(
                0 => 'Unlocked',
                1 => 'Locked',
            ),
            'name' => 'locked',
            "class" => "required-entry",
            "required" => true,
            'value' => (int) $data->getLocked()
        ));
    }

}
