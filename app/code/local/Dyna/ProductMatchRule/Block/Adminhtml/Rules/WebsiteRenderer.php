<?php

class Dyna_ProductMatchRule_Block_Adminhtml_Rules_WebsiteRenderer extends Omnius_ProductMatchRule_Block_Adminhtml_Rules_WebsiteRenderer
{
    protected $_isExport = false;

    public function render(Varien_Object $row)
    {
        $websitesNames = "";

        $websites = $row->getWebsiteIds();

        foreach ($websites as $website) {
            $website = Mage::getModel('core/website')->load($website);

            // When exporting, export the website id, not website name
            if ($this->_isExport) {
                if ($website->getId()) {
                    $ruleWebsite = $website->getId();
                    $websitesNames = $websitesNames ? $websitesNames . "," . $ruleWebsite : $ruleWebsite;
                }
            } else {
                if ($website->getName()) {
                    $ruleWebsite = $website->getName();
                    $websitesNames = $websitesNames ? $websitesNames . ",<br>" . $ruleWebsite : $ruleWebsite;
                }
            }

        }

        return $websitesNames;
    }
}
