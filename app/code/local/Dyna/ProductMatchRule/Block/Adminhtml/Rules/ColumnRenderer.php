<?php

class Dyna_ProductMatchRule_Block_Adminhtml_Rules_ColumnRenderer extends Omnius_ProductMatchRule_Block_Adminhtml_Rules_ColumnRenderer
{
    protected $_isExport = false;

    public function render(Varien_Object $row)
    {
        if ($this->getColumn()->getIndex() == 'left_id') {
            switch ($row->getOperationType()) {
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2C:
                    $model = 'product';
                    $exportField = 'sku';
                    break;
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2P:
                    $model = 'category';
                    $exportField = 'name';
                    break;
                default:
                    $model = null;
                    break;
            }

            if ($model) {
                $item = Mage::getModel('catalog/' . $model)->load($row->getLeftId());
                return $item->getData($exportField);
            }

            return $row->getServiceSourceExpression();

            //return $this->_isExport ? $item->getData($exportField) : ($item->getTypeId() == "configurable" ? sprintf("%s (configurable)",
            //    $item->getName()) : $item->getName());
        } else {
            switch ($row->getOperationType()) {
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2P:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2P:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2P:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2P:
                    $model = 'product';
                    $exportField = 'sku';
                    break;
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_P2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_C2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_S2C:
                case Dyna_ProductMatchRule_Model_Rule::OP_TYPE_M2C:
                    $model = 'category';
                    $exportField = 'name';
                    break;
                default:
                    break;

            }
            $item = Mage::getModel('catalog/' . $model)->load($row->getRightId());
            return $item->getData($exportField);

            //return $this->_isExport ? $item->getData($exportField) : ($item->getTypeId() == "configurable" ? sprintf("%s (configurable)",
            //    $item->getName()) : $item->getName());
        }
    }
}
