<?php
/**
 * Installer that adds the customer_number column on superorder resource
 * @var $this Mage_Core_Model_Resource_Setup
 */

$this->startSetup();

$this->getConnection()
        ->addColumn($this->getTable("superorder/superorder"), "customer_number", [
            "type" => Varien_Db_Ddl_Table::TYPE_TEXT,
            "length" => 20,
            "after" => "customer_id",
            "comment" => "External customer number to which the current superorder maps to",
        ]);

$this->getConnection()
        ->addIndex(
            $this->getTable("superorder/superorder"),
            $this->getIdxName($this->getTable("superorder/superorder"), "customer_number"),
            "customer_number");

$this->endSetup();
