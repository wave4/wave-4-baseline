<?php

class Dyna_Superorder_Model_Client_ProcessOrderRequestClient extends Dyna_Service_Model_Client
{
    protected $_forceUseStubs = false;

    const WSDL_CONFIG_KEY = "process_order/wsdl_process_order";

    /**
     * @param the Superorder $superorder
     * @return array
     */
    public function executeProcessOrderRequest($superorder) : array
    {
        $configurationModel = Mage::getModel('dyna_configurator/configuration');
        $clientCustomerModel = new Dyna_Superorder_Model_Client_ProcessOrderRequestClient_Customer();
        $clientPackageModel = new Dyna_Superorder_Model_Client_ProcessOrderRequestClient_Package();
        //var_dump((int)(bool)$configurationModel->getDataValue('root/data/marketingcodes/0/codex'));die;

        $saleOrderParams = array(
            'ID' => $superorder->getOrderNumber(),
            'Action' => $superorder->getActionType(),
            'DealerCode' => $configurationModel->getDataValue('root/event/dealercode'),
            'CampaignCode' => $configurationModel->getDataValue('root/data/campaigns/code'),
                'AdvertisingCode' => $configurationModel->getDataValue('root/data/advertisingcodes/0/code'),
            'MarketingCode' => $configurationModel->getDataValue('root/data/marketingcodes/0/code'),
            'MarketingFlags' => array(
                'Insert' => 1, //@todo - how is this field determined?
                'TelMarketing' => 1, //@todo - how is this field determined?
                'Advertising' => 1, //@todo - how is this field determined?
                'MailMarketing' => 1, //@todo - how is this field determined?
            ),
            'Comments' => $configurationModel->getDataValue('root/data/comments/0/text'),
            'OrderCommentText' => 'KD Comments', //@todo - how is this field determined?
            'OrderCommentId' => 'ID', //@todo - how is this field determined?
            'TechnicianNeeded' => 'true',
            'TechnicalInstallationComment' => 'Annotation',
            'BonusPromotor' => 'BonusPromotor',
            'BonusPromotorId' => 'ID',
            'BonusCode' => 'Code',
            'OrderCartVoucherCode' => 'VoucherCode',
            'Customer' => $clientCustomerModel->getXMLData(),
            'LogicalPackage' => $clientPackageModel->getXMLData(),
        );

        $response = $this->ProcessOrderRequest(['SalesOrder' => $saleOrderParams]);

        return $response;
    }
}