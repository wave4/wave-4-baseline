<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Superorder_Model_Client_ProcessOrderRequestClient_Package extends Mage_Core_Model_Abstract
{
    /**
     * Gets the data
     */
    public function getXMLData()
    {
        return array(
            'LogicalPackageID' => array(
                'ID' => 'LoremIpsum'
            ),
            'Action' => 'LoremIpsum2',
            'BundleID' => array(
                'ID' => 'LoremIpsum'
            ),
            'CustomerSequnceID' => array(
                'ID' => 'LoremIpsum'
            ),
            'DeliveryOrderID' => array(
                'ID' => 'LoremIpsum'
            ),
            'OGWOrderLineID' => array(
                'ID' => 'LoremIpsum'
            ),
            'Subscriber' => array(
                'Ctn' => 'LoremIpsum',
                'Address' => array(
                    'ID' => 'LoremIpsum',
                    'AddressTypeCode' => 'LoremIpsum',
                    'Postbox' => 'LoremIpsum',
                    'StreetName' => 'LoremIpsum',
                    'BuildingNumber' => 'LoremIpsum',
                    'CityName' => 'LoremIpsum',
                    'PostalZone' => 'LoremIpsum',
                    'CountrySubentity' => 'LoremIpsum',
                    'District' => 'LoremIpsum',
                    'Country' => array(
                        'Name' => 'LoremIpsum'
                    ),
                    'HouseNumberAddition' => 'LoremIpsum',
                    'Validated' => 'LoremIpsum',
                ),
                'Contact' => array(
                    'PartyIdentification' => array(
                        'ID' => 'LoremIpsum'
                    ),
                    'PartyName' => array(
                        'Name' => 'LoremIpsum'
                    ),
                    'PartyLegalEntity' => array(
                        'RegistrationName' => 'LoremIpsum'
                    ),
                    'Contact' => array(
                        'Name' => 'LoremIpsum',
                        'ElectronicMail' => 'LoremIpsum'
                    ),
                    'PartyType' => 'LoremIpsum',
                    'ContactPhoneNumber' => array(
                        'CountryCode' => 'LoremIpsum',
                        'LocalAreaCode' => 'LoremIpsum',
                        'PhoneNumber' => 'LoremIpsum',
                        'Type' => 'LoremIpsum'
                    ),
                    'Role' => array(
                        'Person' => array(
                            'FirstName' => 'LoremIpsum',
                            'FamilyName' => 'LoremIpsum',
                            'Title' => 'LoremIpsum',
                            'NationalityID' => 'LoremIpsum',
                            'BirthDate' => 'LoremIpsum',
                            'Contact' => array(
                                'ElectronicMail' => 'LoremIpsum'
                            ),
                            'Salutation' => 'LoremIpsum'
                        )
                    )
                )
            ),
            'ServiceAddress' => array(
                'StreetName' => 'LoremIpsum'
            ),
            'Component' => array(
                'Action' => 'LoremIpsum3',
                'TariffOption' => 'LoremIpsum',
                'TariffOptionDescription' => 'LoremIpsum',
                'TariffOptionName' => 'LoremIpsum',
                'AgreementSOC' => 'LoremIpsum'
            ),
            'BillingAccount' => array(
                'ID' => 'LoremIpsum',
                'FinancialInstitutionBranch' => array(
                    'ID' => 'LoremIpsum',
                    'Name' => 'LoremIpsum',
                    'FinancialInstitution' => array(
                        'ID' => 'LoremIpsum'
                    ),
                ),
                'PartyIdentyfication' => array(
                    'ID' => 'LoremIpsum'
                ),
                'AccountOwnerName' => 'LoremIpsum',
                'PaymentMethod' => 'LoremIpsum',
                'BillingType' => 'LoremIpsum',
                'CallDataEvaluationIndicator' => 'LoremIpsum',
                'CallDetailType' => 'LoremIpsum',
                'DigitsMaskED' => 'LoremIpsum',
                'DigitsMaskCD' => 'LoremIpsum',
                'BooIndicator' => 'LoremIpsum',
                'Purpose' => 'LoremIpsum',
                'IBANValidated' => 'LoremIpsum',
            )
        );
    }
}