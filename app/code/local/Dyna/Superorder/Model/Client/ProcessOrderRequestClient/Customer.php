<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

class Dyna_Superorder_Model_Client_ProcessOrderRequestClient_Customer extends Mage_Core_Model_Abstract
{
    /**
     * Builds the array of customer data that needs to be sent in the processOrder request
     * @return array
     */
    public function getXMLData()
    {
        return array(
            'PartyIdentification' => array(
                'ID' => 'Legacy Customer ID'
            ),
            'PartyLegalEntity' => array(
                'RegistrationName' => 'Company Name',
                'CompanyID' => 'Company Registration number',
                'CompanyLegalForm' => 'Company registration type',
                'RegistrationAddress' => array(
                    'CityName' => 'Company registration city'
                ),
            ),
            'SequenceID' => array(
                'ID' => '1'
            ),
            'Contact' => array(
                'Telephone' => 'LoremIpsum_Telephone',
                'Telefax' => 'Contact\'s fax number',
                'ElectronicMail' => 'Personal e-mail address',
                'FirstName' => 'Company contact first name',
                'LastName' => 'Company contact last name',
                'AdditionalName' => 'Company additional company name',
                'CallbackTime' => 'KD Additional customer data'
            ),
            'CustomerPassword' => 'Password',
            'AccountCategory' => 'Cable,Mobile_Prepaid,Mobile_Postpaid,DSL',
            'AccountClass' => 'KIAS AccountSubType',
            'PartyType' => 'SOHO',
            'PhonebookEntry' => array(
                'Address' => array(
                    'StreetName' => 'All address fields'
                ),
                'PersonalDetails' => array(
                    'FamilyName' => 'All personal detail fields'
                ),
                'Profession' => 'Lorem ipsum_Profession',
                'PrintListIndicator' => 'LoremIpsum_PrintListIndicator',
                'ElecListIndicator' => 'LoremIpsum',
                'InformationUse' => 'LoremIpsum',
                'NamePrefix' => 'LoremIpsum',
                'FaxDirectoryIndicator' => 'LoremIpsum',
                'VoiceDirectoryIndicator' => 'LoremIpsum',
                'NameSuffix' => 'LoremIpsum',
                'InverseSearch' => 'LoremIpsum',
                'PublishAddress' => 'LoremIpsum',
            ),
            'PortingInformation' => array(
                'HandInDataLater' => 'LoremIpsum',
                'CarrierId' => 'LoremIpsum',
                'CarrierCustomerId' => 'LoremIpsum',
                'CarrierContractCancel' => 'LoremIpsum',
                'PortingSubscriberAdress' => array(
                    'StreetName' => 'LoremIpsum'
                ),
                'PortingSubscriberContact' => array(
                    'Name' => 'LoremIpsum'
                ),
                'CarrierIdentificationPhoneNumber' => array(
                    'PhoneNumber' => 'LoremIpsum'
                ),
                'PortingPhoneNumber' => array(
                    'PhoneNumber' => 'LoremIpsum'
                ),
                'PortingDate' => 'LoremIpsum',
                'PortingOrderDate' => 'LoremIpsum',
                'PortAllNumbers' => 'LoremIpsum',
                'EndOfTermMode' => 'LoremIpsum',
                'EndOfTermDate' => 'LoremIpsum',
                'DonorAccNumber' => 'LoremIpsum',
                'DesiredDate' => 'LoremIpsum',
                'DesiredDateType' => 'LoremIpsum',
                'ContCnclDate' => 'LoremIpsum',
                'ContEndDate' => 'LoremIpsum',
                'ThirdName' => 'LoremIpsum',
                'MnpInfoNameFormat' => 'LoremIpsum',
                'LongNameIndicator' => 'LoremIpsum',
                'MnpInfoContactName' => 'LoremIpsum'
            )
        );
    }

}