<?php

class Dyna_Superorder_Model_Superorder extends Omnius_Superorder_Model_Superorder
{
    const SO_TYPE_NEW = 'New';
    const SO_TYPE_CHANGE_BEFORE = 'ChangeBefore';
    const SO_TYPE_CHANGE_AFTER = 'ChangeAfter';

    /**
     * Tries to determine if the superorder is a result of a create or change(before/after):
     * If it's updatedAt == createdAt , means is "New"
     * If it has children, it's a "changeAfter"
     * If none of the above, it's a "changeBefore"
     *
     * @return string Type of action
     */
    public function getActionType()
    {
        if(!$this->getUpdatedAt() || $this->getUpdatedAt() == $this->getCreatedAt()) {
            return self::SO_TYPE_NEW;
        }
        elseif($this->hasChildren()){
            return self::SO_TYPE_CHANGE_AFTER;
        }
        else{
            return self::SO_TYPE_CHANGE_BEFORE;
        }
    }
}
