<?php

/**
 * Class Dyna_Service_Model_Client
 */
class Dyna_Service_Model_Client extends Omnius_Service_Model_Client_Client
{
    const CONFIG_PATH = 'omnius_service/';
    const WSDL_CONFIG_KEY = "wsdl";
    const ENDPOINT_CONFIG_KEY = "usage_url";
    const CONFIG_HEADER_PATH = 'omnius_service/service_settings_header/';

    /**
     * Dyna_Service_Model_Client constructor.
     * @param null $wsdl
     * @param array $options
     */
    public function __construct($wsdl = null, $options = array())
    {
        $this->loadConfig();
        $this->setup();

        $defaultOptions = $this->getDefaultOptions();

        $options = array_merge(isset($options['options']) ? $options['options'] : array(), $defaultOptions);

        parent::__construct($wsdl, $options);
    }

    /**
     * @return Omnius_Service_Model_Normalizer
     */
    protected function getNormalizer()
    {
        if (!$this->_normalizer) {
            $this->_normalizer = Mage::getSingleton('dyna_service/normalizer');
        }

        return $this->_normalizer;
    }

    /**
     * Load config value for WSDL and Schema
     */
    public function loadConfig()
    {
        if (empty(static::WSDL_CONFIG_KEY)) {
            Mage::throwException(
                sprintf("Invalid WSDL_CONFIG_KEY in class %s", get_called_class())
            );
        }
        $this->_wsdl = $this->getConfig(static::WSDL_CONFIG_KEY);
        $path = Mage::getModuleDir('etc', $this->getNamespace()) . DS . 'schema';
        $requestBuilder = $this->_getRequestBuilder();
        $requestBuilder->setSchemaDir($path);
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        if (!$this->_namespace) {
            preg_match('/(.*)_Model_Client/', get_called_class(), $parts);
            if (isset($parts[1])) {
                $this->_namespace = $parts[1];
            } else {
                Mage::throwException(
                    sprintf('Client class (%s) does not follow naming convention', get_called_class())
                );
            }
        }

        return $this->_namespace;
    }

    public function setup()
    {
        // No login at this moment
    }

    /**
     * @return array
     */
    public function getDefaultOptions()
    {
        return [
            'endpoint' => $this->getConfig(static::ENDPOINT_CONFIG_KEY),
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'connection_timeout' => $this->getHelper()->getGeneralConfig('timeout'),
            'keep_alive' => false,
        ];
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getStubClient()
    {
        return Mage::getModel('dyna_service/multipleStubClient');
    }

    /**
     * @param $response
     * @throws Exception
     */
    public function validateResponseStatus($response)
    {
        if(!isset($response['Status'])) {
            throw new Exception("Invalid response structure, missing status.");
        }
        if($response['Status']['StatusReasonCode'] !== "OK") {
            throw new Exception($response['Status']['StatusReason']);
        }
    }

    /**
     * @param $searchParams
     */
    protected function setRequestHeaderInfo(&$searchParams)
    {
        $searchParams['HeaderInfo']['TransactionId'] = uniqid();
        $searchParams['HeaderInfo']['UserId'] = Mage::helper('agent')->getAgentId();
        $searchParams['HeaderInfo']['ApplicationId'] = $this->getApplicationId();
        $searchParams['HeaderInfo']['PartyName'] = $this->getPartyName();
    }

    /**
     * Get application id from the configurable input in Admin
     * @return bool
     */
    public function getApplicationId()
    {
        return $this->getServiceSettingsHeaderConfig('application_id');
    }

    /**
     * Get party_name from the configurable input in Admin
     * @return bool
     */
    public function getPartyName()
    {
        return $this->getServiceSettingsHeaderConfig('party_name');
    }

    /**
     * @param string $config
     * @return string|null
     */
    public function getServiceSettingsHeaderConfig($config)
    {
        return Mage::getStoreConfig(static::CONFIG_HEADER_PATH . $config);
    }
}
