<?php
class Dyna_Service_Model_MultipleStubClient extends Omnius_Service_Model_Client_StubClient
{
    const DEFAULT_STUB_FILE = "result.json";
    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    protected function getStubbedResponse($method, $arguments)
    {
        $customStub = !empty($arguments[1]) ? $arguments[1] : [];
        $arguments = array_shift($arguments);

        /** @var Omnius_Service_Model_DotAccessor $accessor */
        $accessor = Mage::getSingleton('omnius_service/dotAccessor');
        if (!$this->_gathered) {
            $this->gatherStubs();
        }

        if ('ExecuteRequestWithLogin' == $method && isset($arguments['functionalityName'])) {
            $method = $arguments['functionalityName'];
        }
        if ($stubbedResponse = $accessor->getValue($this->_stubs, $method)) {
            if($customStub) {
                foreach($stubbedResponse as $key => $value) {
                    if(strpos($key, $customStub) !==false) {
                        return $stubbedResponse[$key];
                    }
                }
            }
            if(isset($stubbedResponse[self::DEFAULT_STUB_FILE])) {
                return $stubbedResponse[self::DEFAULT_STUB_FILE];
            }
            return array_values($stubbedResponse)[0];
        }

        Mage::throwException(sprintf(
            'No stub found for the "%s" request. Please add one in %s directory or set the application to not use stubs.',
            $method,
            Mage::getModuleDir('etc', $this->getNamespace()) . DS . 'stubs' . DS . $method
        ));
    }
    /**
     *
     */
    protected function gatherStubs()
    {
        $namespace = $this->getNamespace();

        $path = Mage::getModuleDir('etc', $namespace) . DS . 'stubs';
        $iterator = new RecursiveDirectoryIterator(Mage::getModuleDir('etc', $namespace) . DS . 'stubs', FilesystemIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if (preg_match('/^\_/', $file->getFilename())) {
                //skip files starting with underscore
                continue;
            }

            $subPath = trim(str_replace($path, '', $file->getPathName()), DS);
            $parts = explode(DS, $subPath);
            $method = $parts[0];
            $methodPath = $path . DS . $method;
            if (is_dir($methodPath) && $file->isFile()) {
                $stub = @json_decode(file_get_contents($file->getRealPath()), true);
                if (JSON_ERROR_NONE === json_last_error()) {
                    $this->_stubs[$method][$file->getFilename()] = $stub;
                }
            }
        }
        $this->_gathered = true;
    }
}
