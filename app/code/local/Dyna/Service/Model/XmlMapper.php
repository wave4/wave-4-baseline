<?php
class Dyna_Service_Model_XmlMapper extends Omnius_Service_Model_XmlMapper
{
    public static $parent = null;
    public static $xpathNode = '*[local-name()="%s"][1]';

    /**
     * @param array $data
     * @param SimpleXMLElement $xml
     * @param bool $throw
     * @return SimpleXMLElement
     */
    public static function map(array $data = array(), SimpleXMLElement &$xml, $throw = false, $parent='', $elementNr = 1)
    {
        $callback = function ($paramName, $paramVal, &$el, $namespace = null, $parent = '', $originalXml) use ($throw) {
            $failed = true;
            if (is_callable($paramVal)) {
                $el[0] = $paramVal($el, $namespace);
                $failed = false;
            } elseif (is_array($paramVal)) {
                if(!count(array_filter(array_keys($paramVal), 'is_string'))){
                    //if array of data, first modify the xml schema to clone nodes and add them as siblings
                    $originalXml = self::duplicateNode($originalXml, $parent, count($paramVal));
                    foreach($paramVal as $key => $paramSingleVal){
                        self::$parent = $paramName;
                        $el[0] = self::map($paramSingleVal, $originalXml, false, $parent, $key+1);
                    }
                }
                else{
                    self::$parent = $paramName;
                    $el[0] = self::map($paramVal, $originalXml, false, $parent);
                }
                $failed = false;
            } elseif (is_scalar($paramVal)) {
                $failed = false;
                $el[0][0] = $paramVal;
            }
            if ($failed && $throw) {
                throw new InvalidArgumentException(sprintf('Invalid value given for key "%s". Expected callable, array or scalar, %s given.', $paramName, gettype($paramVal)));
            }
        };
        foreach ($data as $paramName => $paramVal) {
            if($elementNr>1){
                $parent = substr($parent, 0, strrpos($parent, '[')-strlen($parent)) . "[$elementNr]/";
            }
            $parent2 = $parent . sprintf(self::$xpathNode, $paramName);
            $xpath = sprintf("//%s", $parent2);
            if ($el = @$xml->xpath($xpath)) {
                $callback($paramName, $paramVal, $el, null, $parent2.'/', $xml);
            }
        }
        self::$parent = null;

        return $xml;
    }

    /**
     * Add a cloned node after itself
     * @param $xml The xml to be modified
     * @param $parent The node to be duplicated
     * @param $numberOfTimes number of siblings needed
     * @return mixed
     */
    private function duplicateNode($xml, $parent, $numberOfTimes)
    {
        //find the node that needs to be cloned
        $xpath = sprintf("//%s", substr($parent, 0, -1));
        $el = current(@$xml->xpath($xpath));

        //if found, duplicate it a given number of times
        if(!is_null($el)){
            while ($numberOfTimes > 1) {
                $el->insertAfterSelf($el->cloneNode(true));
                $numberOfTimes--;
            }
        }

        return $xml;
    }
}
