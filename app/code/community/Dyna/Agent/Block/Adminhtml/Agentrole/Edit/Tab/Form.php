<?php
/**
 * Copyright (c) 2016. Dynacommerce B.V.
 */

/**
 * Class Dyna_Agent_Block_Adminhtml_Agentrole_Edit_Tab_Form
 */
class Dyna_Agent_Block_Adminhtml_Agentrole_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("agent_form", array("legend" => Mage::helper("agent")->__("Agent Role Information")));

        $fieldset->addField("role", "text", array(
            "label" => Mage::helper("agent")->__("Role Name"),
            "name" => "role",
        ));

        $fieldset->addField("role_description", "text", array(
            "label" => Mage::helper("agent")->__("Role Description"),
            "name" => "role_description",
        ));

        $fieldset->addField("permission_ids", "multiselect", array(
            "label" => Mage::helper("agent")->__("Permission"),
            "name" => "permission_ids",
            "values" => $this->getPermissionsOptions(),
            "value" => array(2,3,4,5,6),
        ));

        if ($dealerRoleData = Mage::getSingleton("adminhtml/session")->getAgentRoleData()) {
            $form->setValues($dealerRoleData);
            Mage::getSingleton("adminhtml/session")->setAgentRoleData(null);
        } elseif (Mage::registry("agentrole_data")) {
            $form->setValues(Mage::registry("agentrole_data")->getData());
        }

        return parent::_prepareForm();
    }

    protected function getPermissionsOptions()
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $collection = Mage::getResourceModel('agent/permission_collection');
        $data = $conn->fetchAll($collection->getSelect());
        $options = array();
        foreach ($data as $perm) {
            $options[] = array(
                'label' => $perm['name'],
                'value' => $perm['entity_id'],
            );
        }

        return $options;
    }
}
