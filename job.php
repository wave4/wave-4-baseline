<?php

/**
 * Usage: GET: URL/job.php?id=JOB_ID
 * Response Ex.:
 *  {
 *      "success": true,
 *      "state": -1
 *  }
 */

date_default_timezone_set("Europe/Berlin");
 
if( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if( ! defined('CRLF')) {
        define('CRLF', sprintf('%s%s', chr(13), chr(10)));
    }
    static $redisClientSocket;

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    function getPost($key, $default = null) {
        return isset($_POST[$key]) && ! is_array($_POST[$key]) ? trim($_POST[$key]) : $default;
    }

    /**
     * @param $data
     * @param string $istr
     * @return string
     */
    function prettyJson($data, $istr='    ') {
        if (defined('JSON_PRETTY_PRINT')) {
            return json_encode($data, JSON_PRETTY_PRINT);
        } else {
            $json = json_encode($data);
            $result = '';
            for($p=$q=$i=0; isset($json[$p]); $p++)
            {
                $json[$p] == '"' && ($p>0?$json[$p-1]:'') != '\\' && $q=!$q;
                if(!$q && strchr(" \t\n", $json[$p])){continue;}
                if(strchr('}]', $json[$p]) && !$q && $i--)
                {
                    strchr('{[', $json[$p-1]) || $result .= "\n".str_repeat($istr, $i);
                }
                $result .= $json[$p];
                if(strchr(',{[', $json[$p]) && !$q)
                {
                    $i += strchr('{[', $json[$p])===FALSE?0:1;
                    strchr('}]', $json[$p+1]) || $result .= "\n".str_repeat($istr, $i);
                }
            }
            return $result;
        }
    }

    /**
     * @param $args
     * @return string
     */
    function _prepare_command($args) {
        return sprintf('*%d%s%s%s', count($args), CRLF, implode(array_map('_map', $args), CRLF), CRLF);
    }

    /**
     * @param $arg
     * @return string
     */
    function _map($arg) {
        return sprintf('$%d%s%s', strlen($arg), CRLF, $arg);
    }

    /**
     * @param $command
     * @throws Exception
     */
    function write_command($command)
    {
        global $redisClientSocket;
        // Reconnect on lost connection (Redis server "timeout" exceeded since last command)
        if(!is_resource($redisClientSocket) || (is_resource($redisClientSocket) && feof($redisClientSocket))) {
            close();
            connect();
        }

        $commandLen = strlen($command);
        for ($written = 0; $written < $commandLen; $written += $fwrite) {
            $fwrite = fwrite($redisClientSocket, substr($command, $written));
            if ($fwrite === FALSE || $fwrite == 0 ) {
                throw new Exception('Failed to write entire command to stream');
            }
        }
    }

    /**
     * @param string $name
     * @return array|int|string
     * @throws Exception
     */
    function read_reply($name = '')
    {
        global $redisClientSocket;
        $reply = fgets($redisClientSocket);
        if($reply === FALSE) {
            throw new Exception('Lost connection to Redis server.');
        }
        $reply = rtrim($reply, CRLF);
        #echo "> $name: $reply\n";
        $replyType = substr($reply, 0, 1);
        switch ($replyType) {
            /* Error reply */
            case '-':
                throw new Exception(substr($reply, 4));
            /* Inline reply */
            case '+':
                $response = substr($reply, 1);
                if($response == 'OK' || $response == 'QUEUED') {
                    return TRUE;
                }
                break;
            /* Bulk reply */
            case '$':
                if ($reply == '$-1') return FALSE;
                $size = (int) substr($reply, 1);
                $response = stream_get_contents($redisClientSocket, $size + 2);
                if( ! $response)
                    throw new Exception('Error reading reply.');
                $response = substr($response, 0, $size);
                break;
            /* Multi-bulk reply */
            case '*':
                $count = substr($reply, 1);
                if ($count == '-1') return FALSE;

                $response = array();
                for ($i = 0; $i < $count; $i++) {
                    $response[] = read_reply();
                }
                break;
            /* Integer reply */
            case ':':
                $response = intval(substr($reply, 1));
                break;
            default:
                throw new Exception('Invalid response: '.print_r($reply, TRUE));
                break;
        }

        // Smooth over differences between phpredis and standalone response
        switch($name)
        {
            case '': // Minor optimization for multi-bulk replies
                break;
            case 'config':
            case 'hgetall':
                $keys = $values = array();
                while($response) {
                    $keys[] = array_shift($response);
                    $values[] = array_shift($response);
                }
                $response = count($keys) ? array_combine($keys, $values) : array();
                break;
            case 'info':
                $lines = explode(CRLF, trim($response,CRLF));
                $response = array();
                foreach($lines as $line) {
                    if ( ! $line || substr($line, 0, 1) == '#') {
                        continue;
                    }
                    list($key, $value) = explode(':', $line, 2);
                    $response[$key] = $value;
                }
                break;
            case 'ttl':
                if($response === -1) {
                    $response = FALSE;
                }
                break;
        }

        return $response;
    }

    /**
     * @return bool
     */
    function close()
    {
        global $redisClientSocket;
        $result = true;
        if (is_resource($redisClientSocket)) {
            try {
                $result = false;
                if (is_resource($redisClientSocket)) {
                    $result = @fclose($redisClientSocket);
                }
            } catch (Exception $e) {
                ; // Ignore exceptions on close
            }
        }
        return $result;
    }

    /**
     * @return resource
     * @throws Exception
     */
    function connect() {
        global $redisClientSocket;
        if ( ! $redisClientSocket) {
            $config = new SimpleXMLElement(file_get_contents('./app/etc/jobs.xml'));
            $options = $config->xpath('jobs_config/queues/default/backend_options');


            $readTimeout = null;
            $server = (string) $options[0]->server;
            $port = (string) $options[0]->port;
            $database = (string) $options[0]->database;
            $password = (string) $options[0]->password;
            $connect_retries = (string) $options[0]->connect_retries;
            $persistent = (string) $options[0]->persistent;

            $flags = STREAM_CLIENT_CONNECT;
            $remote_socket = $port === NULL
                ? 'unix://'.$server
                : 'tcp://'.$server.':'.$port;

            if ($persistent) {
                if ($port === null) { // Unix socket
                    throw new Exception('Persistent connections to UNIX sockets are not supported in standalone mode.');
                }
                $remote_socket .= '/'.$persistent;
                $flags = $flags | STREAM_CLIENT_PERSISTENT;
            }

            $tries = 0;
            while ( ! $redisClientSocket = @stream_socket_client($remote_socket, $errno, $errstr, 2.5, $flags)) {
                if ($tries++ > $connect_retries) {
                    throw new Exception("Connection to Redis failed.");
                }
            }

            if ($readTimeout) {
                stream_set_timeout($redisClientSocket, (int) floor($readTimeout), ($readTimeout - floor($readTimeout)) * 1000000);
            }

            if ($password) {
                call('auth', array($password));
            }

            if ($database != 0) {
                call('select', array($database));
            }
        }
        return $redisClientSocket;
    }

    /**
     * @return array|int|null|string
     * @throws Exception
     */
    function call() {
        if ($arguments = func_get_args()) {
            $name = strtolower(array_shift($arguments));
            $args = $arguments;

            // Flatten arguments
            $argsFlat = NULL;
            foreach($args as $index => $arg) {
                if(is_array($arg)) {
                    if($argsFlat === NULL) {
                        $argsFlat = array_slice($args, 0, $index);
                    }
                    if($name == 'mset' || $name == 'msetnx' || $name == 'hmset') {
                        foreach($arg as $key => $value) {
                            $argsFlat[] = $key;
                            $argsFlat[] = $value;
                        }
                    } else {
                        $argsFlat = array_merge($argsFlat, $arg);
                    }
                } else if($argsFlat !== NULL) {
                    $argsFlat[] = $arg;
                }
            }
            if($argsFlat !== NULL) {
                $args = $argsFlat;
                $argsFlat = NULL;
            }

            // Non-pipeline mode
            array_unshift($args, $name);
            $command = _prepare_command($args);
            write_command($command);
            $response = read_reply($name);

            return $response;
        }
        return null;
    }

    /**
     * Log request to audit
     */
    function logRequest() {
        $logDir = __DIR__ . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array('var', 'log','security', 'audit')) . DIRECTORY_SEPARATOR; //we assure that this script is in the root of the project
        if ( ! @realpath($logDir)) {
            @mkdir($logDir, 0777, true);
        }
        $logFile = sprintf('%saudit_frontend_controller_%s.log', $logDir, date("dmY"));
        if ($h = @fopen($logFile, 'a+')) {
            $data = array(
                gmdate('Y-m-d H:i:s'),
                getPost('website_code'),
                getPost('agent_id'),
                getPost('dealer_id'),
                getPost('dealer_group_ids'),
                getPost('axi_store_id'),
                'job_status_script',
                json_encode($_POST),
                null,
            );
            @fputs($h, join('|', $data).PHP_EOL);
            @fclose($h);
            unset($data);
            unset($logFile);
            unset($logDir);
        }
    }

    /**
     * Handle the request
     */
    try {
        logRequest();
        $jobId = getPost('id');
        if ('undefined' === $jobId || '' === $jobId) { //special case
            $res = array(
                'error' => false,
                'state' => 1,
            );
        } elseif ( ! $job = @unserialize(call('get', $jobId))) {
            $res = array(
                'error' => false,
                'state' => -100, //missing/not present
            );
        } else {
            if ( ! is_array($job) || (is_array($job) && ! isset($job['status']))) {
                throw new Exception('Job has no "status" property. Corrupted data found.');
            }

            $res = array(
                'error' => false,
                'state' => $job['status'],
            );
        }
    } catch (Exception $e) {
        $res = array(
            'error' => true,
            'message' => $e->getMessage(),
        );
    }

    @close();

    /**
     * Output response
     */
    header('Content-Type: application/json');
    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
    header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
    echo prettyJson($res);
}
